#pragma once

//MetaProgrammingStuff
namespace MPS
{
	template<typename... Ts>
	class CTypeList
	{
	public:
		template <std::size_t Index>
		using Type = typename std::tuple_element<Index, std::tuple<Ts...>>::type;

		using TypeList = CTypeList<Ts...>;

		using AsTuple = typename std::tuple<Ts...>;

		template <typename T>
		using PushBack = typename CTypeList<Ts..., T>;

		template <typename T>
		using PushFront = typename CTypeList<T, Ts...>;

		template<typename T, typename Tuple>
		struct HasType;

		template<template <typename...> class T>
		using Rename = T<Ts...>;

	private:
		//HAS TYPE
		template <typename T>
		struct HasType<T, std::tuple<>> : std::false_type {};

		template <typename T, typename... Ts>
		struct HasType<T, std::tuple<T, Ts...>> : std::true_type {};

		template <typename T, typename U, typename... Ts>
		struct HasType<T, std::tuple<U, Ts...>> : HasType<T, std::tuple<Ts...>> {};
	};

	//Metaprogramming Type wrapper
	template <typename T>
	struct Type
	{
		using type = T;
	};

	//--------------------INDEX OF-----------------------
	template<typename, typename...>
	struct IndexOf;

	template <typename T, typename... Ts>
	struct IndexOf<T, std::tuple<T, Ts...>>
	{
		static const std::size_t value = 0;
	};

	template <typename T, typename U, typename... Ts>
	struct IndexOf<T, std::tuple<U, Ts...>>
	{
		static const std::size_t value = 1 + IndexOf<T, std::tuple<Ts...>>::value;
	};

	/*template <typename T, typename... Ts>
	struct IndexOf<T, T, Ts...> : std::integral_constant<std::size_t, 0>
	{
	};

	template <typename T, typename TOther, typename... Ts>
	struct IndexOf<T, TOther, Ts...> : std::integral_constant<std::size_t, 1 + IndexOf<T, Ts...>()>
	{
	};*/
	//---------------------------------------------------

	//------------------- REPEAT -------------------------
	template <typename, std::size_t, typename>
	struct RepeatHlpr;

	template <typename T, std::size_t Amount, typename... Args>
	struct RepeatHlpr<T, Amount, std::tuple<Args...>>
	{
		using Type =
			typename RepeatHlpr<T, Amount - 1, std::tuple<Args..., T>>::Type;
	};

	template <typename T, typename... Args>
	struct RepeatHlpr<T, 0, std::tuple<Args...>>
	{
		using Type = std::tuple<Args...>;
	};

	template <typename T, std::size_t Amount>
	struct Repeat
	{
		using Type = typename RepeatHlpr<T, Amount, std::tuple<>>::Type;
	};
	// ---------------------------------------------------------

	//-----------------------------FILTER ----------------------

	template <template <typename> class, typename, typename Result>
	struct FilterHlpr
	{
		using Type = Result;
	};
	template <template < typename> class FilterType, typename T, typename... Ts, typename Result>
	struct FilterHlpr<FilterType, CTypeList<T, Ts...>, Result>
	{
		using CurrentList = typename FilterHlpr<FilterType, CTypeList<Ts...>, Result>::Type;
		using AddedList = typename FilterHlpr<FilterType, CTypeList<Ts...>, typename Result::template PushBack<T>>::Type;
		//using Type = AddedList;
		static constexpr auto isFilterTrue = FilterType<T>::value;
		using Type = std::conditional_t<!isFilterTrue, CurrentList, AddedList>;
		//using Type = std::conditional < !FilterType<T>::value, CurrentList, AddedList > ;
		//using Type = std::conditional_t < !(FilterType<T>()), CurrentList, AddedList >;
	};

	//Deprecated (unfixed)
	//template <template <typename> class FilterType, typename... Ts>
	//using Filter =
	//	typename FilterHlpr<FilterType, CTypeList<Ts...>, CTypeList<>>::Type;

	template <template <typename> class FilterType, typename TypeListType>
	using FilterTemplateArguments =
		typename FilterHlpr<FilterType, typename TypeListType, CTypeList<>>::Type;
	//---------------------------------------------------------------

	//------------------------------FOR TYPES-----------------------

		//template<typename Lambda, typename T>
		//void ForTypes(Lambda& aLambda)
		//{
		//}

	// 	template<typename Lambda, typename T, typename... Tp>
	// 	void ForTypes(Lambda& aLambda)
	// 	{
	// 		std::cout << "last" << std::endl;
	// 		Type<T> t;
	// 		aLambda(t);
	// 		ForTypes<Lambda, std::tuple<Tp...>>(aLambda);
	// 	}

	template<typename Lambda, typename T>
	struct ForTypes;

	template<typename Lambda>
	struct ForTypes<Lambda, CTypeList<>>
	{
		ForTypes(Lambda&) { }
	};

	template<typename Lambda, typename T, typename... Tp>
	struct ForTypes<Lambda, CTypeList<T, Tp...>>
	{
		using NextForTypes = ForTypes<Lambda, CTypeList<Tp...>>;

		ForTypes(Lambda& aLambda)
		{
			Type<T> t;
			//std::cout << typeid(t).name() << std::endl;
			aLambda(t);
			NextForTypes next(aLambda);
		}

		ForTypes() = delete;
	};

	template<typename Lambda>
	struct ForTypes<Lambda, std::tuple<>>
	{
		ForTypes(Lambda&) { }
	};

	template<typename Lambda, typename T, typename... Tp>
	struct ForTypes<Lambda, std::tuple<T, Tp...>>
	{
		using NextForTypes = ForTypes<Lambda, std::tuple<Tp...>>;

		ForTypes(Lambda& aLambda)
		{
			Type<T> t;
			//std::cout << typeid(t).name() << std::endl;
			aLambda(t);
			NextForTypes next(aLambda);
		}

		ForTypes() = delete;
	};

	template<std::size_t TypeIndex = 0, typename Lambda, typename... Tuple>
	inline typename std::enable_if<TypeIndex == sizeof...(Tuple), void>::type
	ForTuple(std::tuple<Tuple...>&, Lambda&)
	{
	}

	template<std::size_t TypeIndex = 0, typename Lambda, typename... Tuple>
	inline typename std::enable_if<TypeIndex < sizeof...(Tuple), void>::type
	ForTuple(std::tuple<Tuple...>& aTuple, Lambda& aLambda)
	{
		aLambda(std::get<TypeIndex>(aTuple));

		ForTuple<TypeIndex + 1, Lambda, Tuple...>(aTuple, aLambda);
	}















	/*template<typename Lambda, typename T, typename... Tp>
	void ForTypes(Lambda& aLambda)
	{


		Type<T> t;
		std::cout << typeid(t).name() << std::endl;
		aLambda(t);
		ForTypes<Lambda, std::tuple<Tp...>>(aLambda);
	}*/

	// 	template<typename Lambda, typename T, typename U, typename... Tp>
	// 	void ForTypes(Lambda& aLambda)
	// 	{
	// 		std::cout << "other" << std::endl;
	// 		Type<T> t;
	// 		aLambda(t);
	// 		ForTypes<Lambda, U, std::tuple<Tp...>>();
	// 	}

	template<std::size_t I = 0, typename... Tp>
	inline typename std::enable_if<I == sizeof...(Tp), void>::type
		Print(std::tuple<Tp...>&)
	{}

	template<std::size_t I = 0, typename... Tp>
	inline typename std::enable_if < I < sizeof...(Tp), void>::type
		Print(std::tuple<Tp...>& aT)
	{
		std::cout << I/*std::get<I>(aT)*/ << std::endl;
		Print<I + 1, Tp...>(aT);
	}

	//---------------------------------RENAME----------------------------

	//Standardized implementation in most recent MPL library (� la Romeo)
	template <template <typename...> class Tuple, typename Typelist>
	using Rename = typename Typelist::template Rename<Tuple>;

	//-------------------------------------------------------------------
}
