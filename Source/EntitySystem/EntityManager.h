#pragma once
#include "ESSettings.h"
#include "Entity.h"
#include "Handle.h"
#include "../CommonUtilities/GrowingArray.h"
#include "ComponentStorage.h"

#include <new>

constexpr EntityIndex EntityCount = 10000;

template <typename ESSettings>
class CEntityManager
{
private:
	template <typename... Ts>
	friend struct ExpandCallHelper;

	using Settings = ESSettings;
	using ThisType = CEntityManager<Settings>;
	using Bitset = typename Settings::Bitset;
	using Entity = SEntity<Settings>;
	using SignatureBitsetsStorage = Matcher::SignatureBitsetsStorage<Settings>;
	using SignatureList = typename Settings::SignatureList;

	std::size_t mySize;
	std::size_t mySizeNext;

	mutable std::shared_mutex mySizeNextSharedMutex;

	CU::GrowingArray<Entity, std::size_t> myEntities;
	CU::GrowingArray<SHandleData, size_t> myHandleData;

	CComponentStorage<Settings> myComponentStorage;

	SignatureBitsetsStorage mySignatureBitsets;

	mutable std::shared_mutex myRefreshSharedLock;

	EntityIndex CreateIndex()
	{
		std::unique_lock<std::shared_mutex> sharedLockState(mySizeNextSharedMutex);
		assert(mySizeNext + 1 < EntityCount && "Created too many entities.");

		const EntityIndex freeIndex(mySizeNext++);
		sharedLockState.unlock();

		assert(!IsAlive(freeIndex) && "Added entity that was still alive.");

		Entity& e(myEntities[freeIndex]);
		std::unique_lock<std::shared_mutex> uniqueLock(e.mySharedMutex);
		e.myAlive = true;
		e.myBitset.reset();
		uniqueLock.unlock();

		return freeIndex;
	}

	inline Entity& GetEntity(const EntityIndex& aIndex) noexcept
	{
#ifndef _RETAIL
		std::shared_lock<std::shared_mutex> sharedLockState(mySizeNextSharedMutex);
		assert(mySizeNext > aIndex);
#endif // !_RETAIL

		return myEntities[aIndex];
	}

	inline const Entity& GetEntity(const EntityIndex& aIndex) const noexcept
	{
#ifndef _RETAIL
		std::shared_lock<std::shared_mutex> sharedLockState(mySizeNextSharedMutex);
		assert(mySizeNext > aIndex);
#endif // !_RETAIL

		return myEntities[aIndex];
	}

	inline Entity& GetEntityImpl(const EntityIndex& aIndex) noexcept
	{
		return myEntities[aIndex];
	}

	inline const Entity& GetEntityImpl(const EntityIndex& aIndex) const noexcept
	{
		return myEntities[aIndex];
	}

	inline SHandleData& GetHandleDataWithHandleDataIndex(const HandleDataIndex& aHandleIndex) noexcept
	{
		assert(myHandleData.Size() > aHandleIndex && "Handle index was out of bounds!");
		return myHandleData[aHandleIndex];
	}

	inline const SHandleData& GetHandleDataWithHandleDataIndex(const HandleDataIndex& aHandleIndex) const noexcept
	{
		assert(aHandleIndex != HandleDataIndexINVALID && "Handle index was invalid.");
		assert(myHandleData.Size() > aHandleIndex && "Handle index was out of bounds!");
		return myHandleData[aHandleIndex];
	}

	inline SHandleData& GetHandleDataWithEntityIndex(const EntityIndex& aEntityIndex) noexcept
	{
		return GetHandleDataWithHandleDataIndex(GetEntity(aEntityIndex).myHandleDataIndex);
	}

	inline const SHandleData& GetHandleDataWithEntityIndex(const EntityIndex& aEntityIndex) const noexcept
	{
		return GetHandleDataWithHandleDataIndex(GetEntity(aEntityIndex).myHandleDataIndex);
	}

	inline SHandleData& GetHandleData(const SEntityHandle& aHandle) noexcept
	{
		return GetHandleDataWithHandleDataIndex(aHandle.myHandleDataIndex);
	}

	inline const SHandleData& GetHandleData(const SEntityHandle& aHandle) const noexcept
	{
		return GetHandleDataWithHandleDataIndex(aHandle.myHandleDataIndex);
	}

	const inline bool IsHandleValidImpl(const SEntityHandle& aHandle) const noexcept
	{
		return aHandle.myCounter == GetHandleData(aHandle).myCounter;
	}

public:
	CEntityManager()
	{
		mySize = 0;
		mySizeNext = 0;

		Entity* entities(sce_newArray(Entity, EntityCount));
		myEntities.SetRawData(entities, EntityCount);

		SHandleData* handleData(sce_newArray(SHandleData, EntityCount));
		myHandleData.SetRawData(handleData, EntityCount);

		for (auto index(0); index < EntityCount; ++index)
		{
			auto& e(myEntities[index]);
			auto& h(myHandleData[index]);
			e.myDataIndex = index;
			e.myHandleDataIndex = index;
			e.myBitset.reset();
			e.myAlive = false;

			h.myCounter = 0;
			h.myEntityIndex = index;
		}

		myComponentStorage.InitializeCapacity(EntityCount);
	}

	~CEntityManager()
	{
		decltype(myEntities.GetRawData()) entityPointer(myEntities.GetRawData());
		sce_delete(entityPointer);
		myEntities.SetRawData(nullptr, 0);

		decltype(myHandleData.GetRawData()) handlePointer(myHandleData.GetRawData());
		sce_delete(handlePointer);
		myHandleData.SetRawData(nullptr, 0);
	}

	const inline bool IsHandleValid(const SEntityHandle& aHandle) const noexcept
	{
		std::shared_lock<std::shared_mutex> sharedLockRefresh(myRefreshSharedLock);
		return IsHandleValidImpl(aHandle);
	}

	const inline bool IsHandleValidNoAssert(const SEntityHandle& aHandle) const noexcept
	{
		if (aHandle.myHandleDataIndex == HandleDataIndexINVALID) return false;
		if (aHandle.myHandleDataIndex > myHandleData.Size()) return false;
		if (aHandle.myCounter != myHandleData[aHandle.myHandleDataIndex].myCounter) return false;

		return true;
	}

	const inline EntityIndex GetEntityIndex(const SEntityHandle& aHandle) const noexcept
	{
		std::shared_lock<std::shared_mutex> sharedLockRefresh(myRefreshSharedLock);
		assert(IsHandleValidImpl(aHandle) && "Tried to get entity index with invalid handle.");
		return GetHandleData(aHandle).myEntityIndex;
	}

	const inline EntityIndex GetEntityIndexImpl(const SEntityHandle& aHandle) const noexcept
	{
		assert(IsHandleValidImpl(aHandle) && "Tried to get entity index with invalid handle.");
		return GetHandleData(aHandle).myEntityIndex;
	}

	const inline bool IsAlive(const SEntityHandle& aHandle) const noexcept
	{
		std::shared_lock<std::shared_mutex> sharedLockRefresh(myRefreshSharedLock);
		if (!IsHandleValidImpl(aHandle)) return false;
		return IsAlive(GetHandleData(aHandle).myEntityIndex);
	}

	const inline bool IsAlive(EntityIndex aIndex) const noexcept
	{
		auto& entity(GetEntity(aIndex));
		std::shared_lock<std::shared_mutex> sharedLock(entity.mySharedMutex);
		return entity.myAlive;
	}

	const inline bool IsAliveImpl(EntityIndex aIndex) const noexcept
	{
		auto& entity(GetEntityImpl(aIndex));
		std::shared_lock<std::shared_mutex> sharedLock(entity.mySharedMutex);
		return entity.myAlive;
	}

	void Kill(EntityIndex aIndex) noexcept
	{
		auto& entity(GetEntity(aIndex));
		std::unique_lock<std::shared_mutex> uniqueLock(entity.mySharedMutex);
		entity.myAlive = false;
	}

	inline void Kill(const SEntityHandle& aHandle) noexcept
	{
		Kill(GetEntityIndex(aHandle));
	}

	inline void Kill_ThreadSafe(const SEntityHandle& aHandle) noexcept
	{
		std::shared_lock<std::shared_mutex> sharedLockRefresh(myRefreshSharedLock, std::defer_lock);
#ifndef _RETAIL
		std::shared_lock<std::shared_mutex> sharedLockState(mySizeNextSharedMutex, std::defer_lock);
		std::lock(sharedLockRefresh, sharedLockState);
#else
		sharedLockRefresh.lock();
#endif // !_RETAIL

		auto index(GetEntityIndexImpl(aHandle));
		assert(mySizeNext > index);
		auto& entity(GetEntityImpl(index));
		std::unique_lock<std::shared_mutex> uniqueLockEntity(entity.mySharedMutex);
		entity.myAlive = false;
	}

	template<typename T>
	const inline bool HasTag(EntityIndex aIndex) const noexcept
	{
		static_assert(Settings::template IsTag<T>(), "HasTag needs a tag struct as template argument.");
		return GetEntity(aIndex).myBitset[Settings::template TagBit<T>()];
	}

	template<typename T>
	const inline bool HasTag(const SEntityHandle& aHandle) const noexcept
	{
		return HasTag<T>(GetEntityIndex(aHandle));
	}

	template<typename T>
	void AddTag(const SEntityHandle& aHandle) noexcept
	{
		std::shared_lock<std::shared_mutex> sharedLockRefresh(myRefreshSharedLock);
		AddTag<T>(GetEntityIndex(aHandle));
	}

	template<typename T>
	void AddTag(EntityIndex aIndex) noexcept
	{
		static_assert(Settings::template IsTag<T>(), "AddTag needs a tag struct as template argument.");
		GetEntity(aIndex).myBitset[Settings::template TagBit<T>()] = true;
	}

	template<typename T>
	void DelTag(EntityIndex aIndex) noexcept
	{
		static_assert(Settings::template IsTag<T>(), "DelTag needs a tag struct as template argument.");
		GetEntity(aIndex).myBitset[Settings::template TagBit<T>()] = false;
	}

	template<typename T>
	const inline bool HasComponent(EntityIndex aIndex) const noexcept
	{
		static_assert(Settings::template IsComponent<T>(), "HasComponent needs a component struct as template argument.");
		return GetEntity(aIndex).myBitset[Settings::template ComponentBit<T>()];
	}

	template<typename T>
	const inline bool HasComponent(const SEntityHandle& aHandle) const noexcept
	{
		return HasComponent<T>(GetEntityIndex(aHandle));
	}

	template<typename T, typename... TArgs>
	auto& AddComponent(const SEntityHandle& aHandle, TArgs&&... aInitData) noexcept
	{
		std::shared_lock<std::shared_mutex> sharedLockRefresh(myRefreshSharedLock);
		return AddComponent<T>(GetEntityIndex(aHandle), std::forward<TArgs>(aInitData)...);
	}

	template<typename T, typename... TArgs>
	auto& AddComponent(EntityIndex aIndex, TArgs&&... aInitData) noexcept
	{
		static_assert(Settings::template IsComponent<T>(), "AddComponent needs a Component struct as template argument.");
		auto& e(GetEntity(aIndex));
		e.myBitset[Settings::template ComponentBit<T>()] = true;

		auto& c(myComponentStorage.template GetComponent<T>(e.myDataIndex));
		c.~T();
		new (&c) T(std::forward<TArgs>(aInitData)...);
		return c;
	}

	template <typename T>
	auto& GetComponent(const SEntityHandle& aHandle) noexcept
	{
		std::shared_lock<std::shared_mutex> sharedLockRefresh(myRefreshSharedLock);
		return GetComponent<T>(GetEntityIndex(aHandle));
	}

	template <typename T>
	auto& GetComponent(EntityIndex aIndex) noexcept
	{
		static_assert(Settings::template IsComponent<T>(), "GetComponent needs a Component struct as template argument.");
		assert(HasComponent<T>(aIndex) && "Entity does not own a component of this type");
		return myComponentStorage.template GetComponent<T>(GetEntity(aIndex).myDataIndex);
	}

	template <typename T>
	void DelComponent(const SEntityHandle& aHandle) noexcept
	{
		std::shared_lock<std::shared_mutex> sharedLockRefresh(myRefreshSharedLock);
		DelComponent<T>(GetEntityIndex(aHandle));
	}

	template<typename T>
	void DelComponent(EntityIndex aIndex) noexcept
	{
		static_assert(Settings::template IsComponent<T>(), "DelComponent needs a Component struct as template argument.");
		GetEntity(aIndex).myBitset[Settings::template ComponentBit<T>()] = false;
	}

	auto CreateHandle()
	{
		std::shared_lock<std::shared_mutex> sharedLockRefresh(myRefreshSharedLock);

		auto freeIndex(CreateIndex());

		auto& e(myEntities[freeIndex]);
		auto& hd(myHandleData[e.myHandleDataIndex]);

		hd.myEntityIndex = freeIndex;

		SEntityHandle handle;
		handle.myHandleDataIndex = e.myHandleDataIndex;

		handle.myCounter = hd.myCounter;

		assert(IsHandleValid(handle) && "Fucked up the handles somehow");
		AddComponent<CompHandle>(freeIndex, handle);
		return handle;
	}

	void ResetCounter(SEntityHandle& aHandle)
	{
		std::shared_lock<std::shared_mutex> sharedLockRefresh(myRefreshSharedLock);

		auto& hd(myHandleData[aHandle.myHandleDataIndex]);

		hd.myCounter = 0;
		aHandle.myCounter = 0;
	}

	void Clear() noexcept
	{
		for (auto index(0ull); index < EntityCount; ++index)
		{
			auto& e(myEntities[index]);
			auto& hd(myHandleData[index]);
			e.myDataIndex = index;
			e.myBitset.reset();
			e.myAlive = false;
			e.myHandleDataIndex = index;

			hd.myCounter = 0;
			hd.myEntityIndex = index;
		}

		mySize = 0;
		mySizeNext = 0;
	}

	void Refresh() noexcept
	{
		std::unique_lock<std::shared_mutex> uniqueLockRefresh(myRefreshSharedLock, std::defer_lock);
		std::unique_lock<std::shared_mutex> uniqueLockStateBuffer(mySizeNextSharedMutex, std::defer_lock);
		std::lock(uniqueLockRefresh, uniqueLockStateBuffer);

		if (mySizeNext == 0)
		{
			mySize = 0;
			return;
		}

		mySize = mySizeNext = RefreshImpl();
	}

	template<typename T>
	const inline bool MatchesSignature(EntityIndex aIndex) const noexcept
	{
		static_assert(Settings::template IsSignature<T>(), "Matches signature needs a signature as template argument.");

		const auto& entityBitset(GetEntity(aIndex).myBitset);
		const auto& signatureBitset(mySignatureBitsets.template GetSignatureBitset<T>());
		return (entityBitset & signatureBitset) == signatureBitset;
	}

	template<typename T>
	const inline bool MatchesSignatureImpl(EntityIndex aIndex) const noexcept
	{
		static_assert(Settings::template IsSignature<T>(), "Matches signature needs a signature as template argument.");

		const auto& entityBitset(GetEntityImpl(aIndex).myBitset);
		const auto& signatureBitset(mySignatureBitsets.template GetSignatureBitset<T>());
		return (entityBitset & signatureBitset) == signatureBitset;
	}

	template <typename Function>
	void ForAllEntities(Function&& aFunction)
	{
		for (EntityIndex i(0); i < mySize; ++i)
		{
			aFunction(i);
		}
	}

	template <typename Signature, typename Function>
	void ForAllEntitiesMatching(Function&& aFunction)
	{
		static_assert(Settings::template IsSignature<Signature>(), "ForEntitiesMatching needs a signature as template argument.");

		std::shared_lock<std::shared_mutex> sharedLockState(mySizeNextSharedMutex);
		ForAllEntities([this, &aFunction](auto index)
		{
			if (MatchesSignatureImpl<Signature>(index))
			{
				ExpandSignatureCallLocked<Signature>(index, aFunction, sharedLockState);
			}
		});
	}

	template <typename Signature, typename Function>
	void ForEntitiesMatching(Function&& aFunction, const CU::GrowingArray<UniqueIndexType>& aEntities)
	{
		static_assert(Settings::template IsSignature<Signature>(), "ForEntitiesMatching needs a signature as template argument.");

		std::shared_lock<std::shared_mutex> sharedLockState(mySizeNextSharedMutex);
		for (const auto& uniqueIndex : aEntities)
		{
			const auto index(GetEntityIndex(SEntityHandle(uniqueIndex)));
			if (MatchesSignatureImpl<Signature>(index))
			{
				ExpandSignatureCallLocked<Signature>(index, aFunction, sharedLockState);
			}
		}
	}

	template<typename Signature, typename Function>
	void RunIfMatching(const SEntityHandle& aHandle, Function&& aFunction)
	{
		if (aHandle.myUniqueIndex == EntityHandleINVALID.myUniqueIndex) return;

		std::shared_lock<std::shared_mutex> sharedLockRefresh(myRefreshSharedLock);
		RunIfMatching<Signature>(GetEntityIndex(aHandle), aFunction);
	}

	template<typename Signature, typename Function>
	void RunIfMatching(const EntityIndex& aEntityIndex, Function&& aFunction)
	{
		static_assert(Settings::template IsSignature<Signature>(), "RunIfMatching needs a signature as template argument.");
		if (MatchesSignature<Signature>(aEntityIndex))
		{
			ExpandSignatureCall<Signature>(aEntityIndex, aFunction);
		}
		else
		{
			assert(false && "RunIfMatching needs a signature that fits the entity its being run on.");
		}
	}

	//It was a cool idea, for sure.
	//void UpdateAllSignatures(const float aDeltaTime, CU::GrowingArray<std::function<void()>, size_t> aDuringUpdateFunctionList)
	//{
	//	auto signatureFunction = 
	//		[this, aDeltaTime]
	//		(auto& aSignature)
	//		{
	//			//decltype(aSignature)::Signature;
	//
	//			std::cout << typeid(decltype(aSignature.GetInvalidSignatureType())).name() << std::endl;
	//
	//			//ForEntitiesMatching<decltype(aSignature)::Signature(
	//			ForEntitiesMatching<decltype(aSignature.GetInvalidSignatureType())>(aSignature.myImplementation());
	//		};
	//	
	//	MPS::ForTuple(mySignatureCallers, signatureFunction);
	//}

private:
	template <typename T, typename Function>
	void ExpandSignatureCall(EntityIndex aIndex, Function&& aFunction)
	{
		static_assert(Settings::template IsSignature<T>(), "ExpandSignatureCall needs a signature as template argument.");

		using RequiredComponents = typename Settings::SignatureBitsets::template SignatureComponents<T>;

		using FunctionCallHelper = MPS::Rename<ExpandCallHelper, RequiredComponents>;

		FunctionCallHelper::Call(aIndex, *this, aFunction);
	}

	template <typename T, typename Function>
	void ExpandSignatureCallLocked(EntityIndex aIndex, Function&& aFunction, std::shared_lock<std::shared_mutex>& aSharedLock)
	{
		static_assert(Settings::template IsSignature<T>(), "ExpandSignatureCall needs a signature as template argument.");

		using RequiredComponents = typename Settings::SignatureBitsets::template SignatureComponents<T>;

		using FunctionCallHelper = MPS::Rename<ExpandCallHelper, RequiredComponents>;

		FunctionCallHelper::CallLocked(aIndex, *this, aFunction, aSharedLock);
	}

	template <typename... Ts>
	struct ExpandCallHelper
	{
		template <typename Function>
		static void Call(EntityIndex aIndex, ThisType& aManager, Function&& aFunction)
		{
			auto dataIndex(aManager.GetEntity(aIndex).myDataIndex);

			aFunction(aIndex, aManager.myComponentStorage.template GetComponent<Ts>(dataIndex)...);
		}

		template <typename Function>
		static void CallLocked(EntityIndex aIndex, ThisType& aManager, Function&& aFunction, std::shared_lock<std::shared_mutex>& aSharedLock)
		{
			auto dataIndex(aManager.GetEntityImpl(aIndex).myDataIndex);
			dataIndex;

			aSharedLock.unlock();
			aFunction(aIndex, aManager.myComponentStorage.template GetComponent<Ts>(dataIndex)...);
			aSharedLock.lock();
		}
	};

	void InvalidateHandle(EntityIndex aIndex) noexcept
	{
		auto& hd(myHandleData[myEntities[aIndex].myHandleDataIndex]);
		++hd.myCounter;
	}

	void RefreshHandle(EntityIndex aIndex) noexcept
	{
		auto& hd(myHandleData[myEntities[aIndex].myHandleDataIndex]);
		hd.myEntityIndex = aIndex;
	}

	std::size_t RefreshImpl() noexcept
	{
		EntityIndex indexDead{ 0 };
		EntityIndex indexAlive{ mySizeNext - 1 };

		while (true)
		{
			for (; true; ++indexDead)
			{
				if (indexDead > indexAlive)
					return indexDead;
				if (!myEntities[indexDead].myAlive)
					break;
			}

			for (; true; --indexAlive)
			{
				if (myEntities[indexAlive].myAlive)
					break;
				InvalidateHandle(indexAlive);
				if (indexAlive <= indexDead)
					return indexDead;
			}

			assert(myEntities[indexAlive].myAlive);
			assert(!myEntities[indexDead].myAlive);

			std::swap(myEntities[indexAlive], myEntities[indexDead]);
			//Alive entity's handle is refreshed
			RefreshHandle(indexDead);
			//Dead entity's handle is invalidated and refreshed
			InvalidateHandle(indexAlive);
			RefreshHandle(indexAlive);
			++indexDead; --indexAlive;
		}

		return indexDead;
	}

public:
	const inline size_t GetEntityCount() const noexcept
	{
		return mySize;
	}

	const inline size_t GetCapacity() const noexcept
	{
		return myEntities.Capacity();
	}

	void PrintState() const
	{
		std::cout
			<< "\nSize: " << mySize
			<< "\nSizeNext: " << mySizeNext
			<< "\n" << std::endl;

		for (auto index(0ull); index < mySizeNext; ++index)
		{
			auto& e(myEntities[index]);
			std::cout << (e.myAlive) ? "A" : "D";
		}

		std::cout << "\n\n" << std::endl;
	}

};