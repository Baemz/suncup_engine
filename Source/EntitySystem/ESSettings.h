#pragma once
#include "ESSettingsLight.h"

#include <tuple>
#include <type_traits>
#include <bitset>

//List types to use for the entity system, for modificability
//I'm well aware, Jacob(, probably.)

template
<
	typename ComponentListType,
	typename TagListType,
	typename SignatureListType
>
struct SESSettings
{
	using ComponentList = typename ComponentListType::AsTuple;
	using ComponentTypeList = typename ComponentListType::TypeList;
	using TagList = typename TagListType::AsTuple;
	using SignatureList = typename SignatureListType::AsTuple;
	using ThisType = SESSettings<ComponentListType, TagListType, SignatureListType>;

	using SignatureBitsets = Matcher::SignatureBitsets<ThisType>;
	using SignatureBitsetsStorage = Matcher::SignatureBitsetsStorage<ThisType>;

	template <typename T>
	static constexpr bool IsComponent() noexcept
	{
		return MPS::CTypeList<ComponentList>::template HasType<T, ComponentList>::value;
	}

	template <typename T>
	static constexpr bool IsTag() noexcept
	{
		return MPS::CTypeList<TagList>::template HasType<T, TagList>::value;
	}
	
	template <typename T>
	static constexpr bool IsSignature() noexcept
	{
		return MPS::CTypeList<SignatureList>::template HasType<T, SignatureList>::value;
	}

	static constexpr std::size_t ComponentCount() noexcept
	{
		return std::tuple_size<ComponentList>::value;
	}

	static constexpr std::size_t TagCount() noexcept
	{
		return std::tuple_size<TagList>::value;
	}

	static constexpr std::size_t SignatureCount() noexcept
	{
		return std::tuple_size<SignatureList>::value;
	}

	template <typename T>
	static constexpr std::size_t ComponentID() noexcept
	{
		return MPS::IndexOf<T, ComponentList>::value;
	}

	template <typename T>
	static constexpr std::size_t TagID() noexcept
	{
		return MPS::IndexOf<T, TagList>::value;
	}

	template <typename T>
	static constexpr std::size_t SignatureID() noexcept
	{
		return MPS::IndexOf<T, SignatureList>::value;
	}

	static constexpr std::size_t BitsetSize = ComponentCount() + TagCount();
	using Bitset = std::bitset<BitsetSize>;

	template <typename T>
	static constexpr std::size_t ComponentBit() noexcept
	{
		return ComponentID<T>();
	}

	template <typename T>
	static constexpr std::size_t TagBit() noexcept
	{
		//Tag bits stored at end of bitset
		return ComponentCount() + TagID<T>();
	}
};

namespace Matcher
{
	template <typename SettingsType>
	struct SignatureBitsets
	{
		using Settings = SettingsType;
		using ThisType = SignatureBitsets<Settings>;
		using SignatureList = typename Settings::SignatureList;
		using Bitset = typename Settings::Bitset;

		using BitsetRepeatedList = typename MPS::Repeat
		<
			Bitset,
			Settings::SignatureCount()
		>::Type;

		using BitsetStorage = std::tuple<BitsetRepeatedList>;

		template<typename T>
		using IsComponentFilter = std::integral_constant
		<
			bool, Settings::template IsComponent<T>()
		>;

		template<typename T>
		using IsTagFilter = std::integral_constant
		<
			bool, Settings::template IsTag<T>()
		>;

		template<typename SignatureType>
		using SignatureComponents = MPS::FilterTemplateArguments
		<
			IsComponentFilter,
			SignatureType
		>;
		
		template<typename SignatureType>
		using SignatureTags = MPS::FilterTemplateArguments
		<
			IsTagFilter,
			SignatureType
		>;
	};

	template <typename SettingsType>
	struct SignatureBitsetsStorage
	{
	private:
		using Settings = SettingsType;
		using SignatureBitsets = typename Settings::SignatureBitsets;
		using SignatureList = typename SignatureBitsets::SignatureList;
		using BitsetStorage = typename SignatureBitsets::BitsetStorage;

		BitsetStorage storage;

	public:
		template <typename T>
		auto& GetSignatureBitset() noexcept
		{
			static_assert(Settings::template IsSignature<T>(), "");

			constexpr std::size_t signatureID = Settings::template SignatureID<T>();
			return std::get<signatureID>(std::get<0>(storage));
		}

		template <typename T>
		const auto& GetSignatureBitset() const noexcept
		{
			static_assert(Settings::template IsSignature<T>(), "");

			constexpr std::size_t signatureID = Settings::template SignatureID<T>();
			return std::get<signatureID>(std::get<0>(storage));
		}

	private:
		template <typename T>
		void InitializeBitset() noexcept
		{
			auto& currentSigSet(this->GetSignatureBitset<T>());

			using SignatureComponents = typename SignatureBitsets::template SignatureComponents<T>;
			using SignatureTags =		typename SignatureBitsets::template SignatureTags<T>;
			
			auto componentLambda = [this, &currentSigSet](auto t)
			{
				t;
				currentSigSet[Settings::template ComponentBit<typename decltype(t)::type>()] = true;
			};
			auto tagLambda = [this, &currentSigSet](auto t)
			{
				t;
				currentSigSet[Settings::template TagBit<typename decltype(t)::type>()] = true;
			};

	  		MPS::ForTypes<decltype(componentLambda), SignatureComponents> forComponents(componentLambda);
	  		MPS::ForTypes<decltype(tagLambda), SignatureTags> forTags(tagLambda);
		}

	public:
		SignatureBitsetsStorage() noexcept
		{
			auto lambda = [this](auto t)
			{
				t;
				this->InitializeBitset<typename decltype(t)::type>();
			};

			MPS::ForTypes<decltype(lambda), SignatureList> forTypes(lambda);
		}
	};
}

//You expected comments ?