#pragma once
#include <cstddef>
#include <shared_mutex>

using DataIndex = typename std::size_t;
using EntityIndex = typename std::size_t;
using HandleDataIndex = typename int;
constexpr HandleDataIndex HandleDataIndexINVALID = static_cast<HandleDataIndex>(-1);
using UniqueIndexType = typename std::int64_t;

template <typename ESSettings>
class CEntityManager;

template <typename ESSettings>
struct SEntity
{
	friend CEntityManager<ESSettings>;

public:
	using Bitset = typename ESSettings::Bitset;

	SEntity() = default;

	SEntity(const SEntity& aOther)
		: myDataIndex(aOther.myDataIndex)
		, myHandleDataIndex(aOther.myHandleDataIndex)
		, myBitset(aOther.myBitset)
		, myAlive(aOther.myAlive)
		//, mySharedMutex <--- NO
	{}

	SEntity& operator=(const SEntity& aOther)
	{
		myDataIndex = aOther.myDataIndex;
		myHandleDataIndex = aOther.myHandleDataIndex;
		myBitset = aOther.myBitset;
		myAlive = aOther.myAlive;
		//mySharedMutex <--- NO

		return *this;
	}

	DataIndex myDataIndex;
	HandleDataIndex myHandleDataIndex;
	Bitset myBitset;

	bool myAlive;

private:
	mutable std::shared_mutex mySharedMutex;

};