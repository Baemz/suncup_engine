#pragma once
#include "Entity.h"
#include "ESSettingsLight.h"

struct SHandleData
{
	EntityIndex myEntityIndex;
	int myCounter;
};

struct SEntityHandle
{
	SEntityHandle()
		: myHandleDataIndex(HandleDataIndexINVALID)
		, myCounter(0)
	{}

	SEntityHandle(const SEntityHandle& aOther)
		: myUniqueIndex(aOther.myUniqueIndex)
	{}

	explicit SEntityHandle(const UniqueIndexType& aUniqueIndex)
		: myUniqueIndex(aUniqueIndex)
	{}

	SEntityHandle& operator=(const SEntityHandle& aOther)
	{
		myUniqueIndex = aOther.myUniqueIndex;
		return *this;
	}

	inline bool operator==(const SEntityHandle& aOther) const
	{
		return (myUniqueIndex == aOther.myUniqueIndex);
	}

	inline bool operator!=(const SEntityHandle& aOther) const
	{
		return !(*this == aOther);
	}

	union
#pragma warning(push)
#pragma warning(disable : 4201)
	{
		struct
		{
			HandleDataIndex myHandleDataIndex;
			int myCounter;
		};
		struct
		{
			UniqueIndexType myUniqueIndex;
		};
#pragma warning(pop)
	};
};

const SEntityHandle EntityHandleINVALID;
