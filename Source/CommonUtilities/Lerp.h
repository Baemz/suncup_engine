#pragma once

namespace CommonUtilities
{
	template <typename T, typename T2>
	__forceinline T Lerp(const T& aMin, const T& aMax, const T2& aCurrentValue)
	{
		return aMin + (aMax - aMin) * aCurrentValue;
	}
}

namespace CU = CommonUtilities;
