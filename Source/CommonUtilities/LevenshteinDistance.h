#pragma once
#include <string>
#include <numeric>
#include <algorithm>

#ifdef min
#pragma push_macro("min")
#undef min
#define MIN_UNDEFINED
#endif // min

namespace CommonUtilities
{
	inline std::string::size_type LevenshteinDistance(const std::string& aString0, const std::string& aString1)
	{
		const std::string::size_type string0Size(aString0.size());
		const std::string::size_type string1Size(aString1.size());

		decltype(string0Size) column_start(1);

		auto column(new std::remove_const_t<decltype(string0Size)>[string0Size + 1]);
		std::iota((column + column_start), (column + string0Size + 1), (column_start));

		for (auto x(column_start); x <= string1Size; ++x)
		{
			column[0] = x;
			auto last_diagonal(x - column_start);
			for (auto y(column_start); y <= string0Size; ++y)
			{
				const auto old_diagonal(column[y]);
				const auto possibilities =
				{
					column[y] + 1,
					column[y - 1] + 1,
					last_diagonal + (aString0[y - 1] == aString1[x - 1] ? 0 : 1),
				};
				column[y] = std::min(possibilities);
				last_diagonal = old_diagonal;
			}
		}

		const auto result(column[string0Size]);
		delete[] column;
		return result;
	}
}

namespace CU = CommonUtilities;

#ifdef MIN_UNDEFINED
#undef MIN_UNDEFINED
#pragma pop_macro("min")
#endif // MIN_UNDEFINED
