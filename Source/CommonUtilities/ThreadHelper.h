#pragma once
#include <assert.h>
#include <thread>
#include "Timer.h"

namespace CommonUtilities
{
	namespace ThreadHelper
	{
		// Set thread name on calling thread
		void SetThreadName(const std::string& aNewThreadName);

		// Set thread name with const thread id
		void SetThreadName(const std::thread::id& aThreadID, const std::string& aNewThreadName);

		// Set thread name with pointer to const thread
		inline void SetThreadName(const std::thread* aThread, const std::string& aNewThreadName)
		{
			assert("Tried to set thread name on nullptr" && (aThread != nullptr));
			SetThreadName(aThread->get_id(), aNewThreadName);
		}

		// Set thread name with reference to const thread
		inline void SetThreadName(const std::thread& aThread, const std::string& aNewThreadName)
		{
			SetThreadName(aThread.get_id(), aNewThreadName);
		}

		// Thread priorities
		enum class EThreadPriority : unsigned char
		{
			Lowest,
			Low,
			Normal,
			High,
			Highest,
			SIZE,
		};

		// Set thread priority on calling thread
		bool SetThreadPriority(const EThreadPriority& aThreadPriority, std::string& aErrorMessage);

		// Set thread priority with handle pointer(void*)
		bool SetThreadPriority(void* aThreadHandle, const EThreadPriority& aThreadPriority, std::string& aErrorMessage);

		// Set thread priority with const pointer to thread
		inline bool SetThreadPriority(std::thread* const aThread, const EThreadPriority& aThreadPriority, std::string& aErrorMessage)
		{
			assert("Tried to set thread priority on nullptr" && (aThread != nullptr));
			return SetThreadPriority(aThread->native_handle(), aThreadPriority, aErrorMessage);
		}

		// Set thread priority with reference to thread
		inline bool SetThreadPriority(std::thread& aThread, const EThreadPriority& aThreadPriority, std::string& aErrorMessage)
		{
			return SetThreadPriority(aThread.native_handle(), aThreadPriority, aErrorMessage);
		}

		// Get thread priority on calling thread
		bool GetThreadPriority(EThreadPriority& aThreadPriority, std::string& aErrorMessage);

		// Get thread priority with handle pointer(void*)
		bool GetThreadPriority(void* aThreadHandle, EThreadPriority& aThreadPriority, std::string& aErrorMessage);

		// Get thread priority with const pointer to thread
		inline bool GetThreadPriority(std::thread* const aThread, EThreadPriority& aThreadPriority, std::string& aErrorMessage)
		{
			assert("Tried to set thread priority on nullptr" && (aThread != nullptr));
			return GetThreadPriority(aThread->native_handle(), aThreadPriority, aErrorMessage);
		}

		// Get thread priority with reference to thread
		inline bool GetThreadPriority(std::thread& aThread, EThreadPriority& aThreadPriority, std::string& aErrorMessage)
		{
			return GetThreadPriority(aThread.native_handle(), aThreadPriority, aErrorMessage);
		}

		class CFramerateLimiter
		{
		public:
			CFramerateLimiter() : myMaxSeconds(-1.0f), myMinSecondsForSlowmotion(-1.0f) {};
			~CFramerateLimiter() = default;

			// Send in max fps.
			// 0.0f == No limit. 0.0f < Limit to.
			void Init(const float aMaxFPS, const float aMinFPSForSlowmotion);

			// Send in max fps, will sleep if actual fps is above it. Returns delta time.
			// -1.0f == Don't change since last set. 0.0f == No limit. 0.0f < Limit to.
			float Update(const float aMaxFPS = -1.0f, const float aMinFPSForSlowmotion = -1.0f);

			//
			double GetTotalTime() const;

			float GetMaxFPS() const;
			float GetMinFPSForSlowmotion() const;

		private:
			Timer myThreadTimer;
			float myMaxSeconds;
			float myMinSecondsForSlowmotion;

		};
	}
}

namespace CU = CommonUtilities;
