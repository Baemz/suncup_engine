#include "Intersection.h"
#include <assert.h>
#include "Macros.h"

#include <algorithm>

namespace CommonUtilities
{
	namespace Collision
	{
		bool PointInsideSphere(const Sphere& aSphere, const Vector3f aPoint)
		{
			return (aSphere.myCenter - aPoint).Length2() < aSphere.myRadius * aSphere.myRadius;
		}

		bool PointInsideAABB(const AABB3D& aAABB, const Vector3f& aPoint)
		{
			if (aPoint.x < aAABB.myMin.x)
			{
				return false;
			}
			if (aPoint.x > aAABB.myMax.x)
			{
				return false;
			}
			if (aPoint.y < aAABB.myMin.y)
			{
				return false;
			}
			if (aPoint.y > aAABB.myMax.y)
			{
				return false;
			}
			if (aPoint.z < aAABB.myMin.z)
			{
				return false;
			}
			if (aPoint.z > aAABB.myMax.z)
			{
				return false;
			}
			return true;
		}

		bool PointInsideAABB2D(const CU::Vector4f & aAABB, const Vector3f & aPoint)
		{
			if (aPoint.x > aAABB.x1)
			{
				if (aPoint.x < aAABB.x2)
				{
					if (aPoint.y < aAABB.y2)
					{
						if (aPoint.y > aAABB.y1)
						{
							return true;
						}
						return false;
					}
					return false;
				}
				return false;
			}
			return false;
		}

		bool IntersectionPlaneLine(const Plane& aPlane, const Line3D& aLine, Vector3f& aOurIntersectionPoint)
		{
			//Om linjen �r parallell med planet, returneras false och aOutIntersectionPoint �ndras inte
			//Om linjen inte �r parallell med planet skrivs sk�rningspunkten till aOutIntersectionPoint och true returneras
			
			float pointAndNormal = aLine.myDirection.Dot(aPlane.myNormal);

			if (pointAndNormal == 0.f)
			{
				return false;
			}

			float tDistance = (aPlane.myPoint.Dot(aPlane.myNormal) - aLine.myPoint.Dot(aPlane.myNormal)) / pointAndNormal;
			aOurIntersectionPoint = aLine.myPoint + aLine.myDirection * tDistance;

			return true;
		}

		bool IntersectionAABBLine(const AABB3D& aAABB, const Line3D& aLine)
		{
			const Vector3f& point = aLine.myPoint;
			const Vector3f& direction = aLine.myDirection;
			const Vector3f& min = aAABB.myMin;
			const Vector3f& max = aAABB.myMax;

			// IntersectionPoint
			Vector3f iPoint;

			iPoint = point;
			if (direction.x != 0.f)
			{
				iPoint = point + direction * ((min.x - point.x) / direction.x);
			}
			if (iPoint.x >= min.x && iPoint.x <= max.x && iPoint.y >= min.y && iPoint.y <= max.y && iPoint.z >= min.z && iPoint.z <= max.z)
			{
				return true;
			}
			if (direction.x != 0.f)
			{
				iPoint = point + direction * ((max.x - point.x) / direction.x);
			}
			if (iPoint.x >= min.x && iPoint.x <= max.x && iPoint.y >= min.y && iPoint.y <= max.y && iPoint.z >= min.z && iPoint.z <= max.z)
			{
				return true;
			}

			iPoint = point;
			if (direction.y != 0.f)
			{
				iPoint = point + direction * ((min.y - point.y) / direction.y);
			}
			if (iPoint.x >= min.x && iPoint.x <= max.x && iPoint.y >= min.y && iPoint.y <= max.y && iPoint.z >= min.z && iPoint.z <= max.z)
			{
				return true;
			}
			if (direction.y != 0.f)
			{
				iPoint = point + direction * ((max.y - point.y) / direction.y);
			}
			if (iPoint.x >= min.x && iPoint.x <= max.x && iPoint.y >= min.y && iPoint.y <= max.y && iPoint.z >= min.z && iPoint.z <= max.z)
			{
				return true;
			}

			iPoint = point;
			if (direction.z != 0.f)
			{
				iPoint = point + direction * ((min.z - point.z) / direction.z);
			}
			if (iPoint.x >= min.x && iPoint.x <= max.x && iPoint.y >= min.y && iPoint.y <= max.y && iPoint.z >= min.z && iPoint.z <= max.z)
			{
				return true;
			}
			if (direction.z != 0.f)
			{
				iPoint = point + direction * ((max.z - point.z) / direction.z);
			}
			if (iPoint.x >= min.x && iPoint.x <= max.x && iPoint.y >= min.y && iPoint.y <= max.y && iPoint.z >= min.z && iPoint.z <= max.z)
			{
				return true;
			}

			return false;
		}

		bool IntersectionSphereLine(const Sphere& aSphere, const Line3D& aLine)
		{
			const Vector3f& point = aLine.myPoint;
			const Vector3f& direction = aLine.myDirection.GetNormalized();
			const Vector3f& center = aSphere.myCenter;

			float length = (center - point).Dot(direction);
			Vector3f closestPoint = point + direction * length;
			Vector3f distance = aSphere.myCenter - closestPoint;
			return distance.Length2() < aSphere.myRadius * aSphere.myRadius;
		}

		bool IntersectionSphereSphere(const Sphere& aSphere1, const Sphere& aSphere2)
		{
			/*
			const Vector3f distance = aSphere1.center - aSphere2.center;
			const float distanceLength = distance.Length();
			*/

			return ((aSphere1.myCenter - aSphere2.myCenter).Length() < aSphere1.myRadius + aSphere2.myRadius);
		}
		
		bool IntersectionCircleCircle(const Circle& aCircle1, const Circle& aCircle2)
		{
			CU::Vector2f pos1 = CU::Vector2f(aCircle1.myCenter.x / (1080.0f / 1920.0f), aCircle1.myCenter.y);
			CU::Vector2f pos2 = CU::Vector2f(aCircle2.myCenter.x / (1080.0f / 1920.0f), aCircle2.myCenter.y);
			return ((pos1 - pos2).Length() < aCircle1.myRadius + aCircle2.myRadius);
		}


		bool IntersectionCircleVShape(const Circle& aCircle, const Line3D& aLLine,const Line3D& aRLine)
		{
			assert(aLLine.myPoint.x == aRLine.myPoint.x && aLLine.myPoint.y == aRLine.myPoint.y);

			CU::Vector2f circleCenter = CU::Vector2f(aCircle.myCenter.x, aCircle.myCenter.y);
			CU::Vector2f lineIntersectPoint = CU::Vector2f(aLLine.myPoint.x, aLLine.myPoint.y);
			CU::Vector2f lNormal = -1.f * CU::Vector2f( -aLLine.myDirection.y, aLLine.myDirection.x);
			CU::Vector2f rNormal = CU::Vector2f(-aRLine.myDirection.y, aRLine.myDirection.x);
			CU::Vector2f intersectionToCircle = circleCenter - lineIntersectPoint;
			return ((intersectionToCircle).Dot(lNormal) < 0.f && (intersectionToCircle).Dot(rNormal) < 0.f);
		}
		bool IntersectionAABBCircle(const AABB & aRect, const Circle & aCircle)
		{
			float deltaX = aCircle.myCenter.x - MAX(aRect.myMax.x, MIN(aCircle.myCenter.x, aRect.myMin.x + aRect.myWidth));
			float deltaY = aCircle.myCenter.y - MAX(aRect.myMax.y, MIN(aCircle.myCenter.y, aRect.myMin.y + aRect.myHeight));
			return (deltaX * deltaX + deltaY * deltaY) < (aCircle.myRadius * aCircle.myRadius);
		}
		bool IntersectionRayAABB(const Ray & aRay, const AABB3D & aAABB)
		{
			const Vector3f& origin = aRay.myOrigin;
			const Vector3f& direction = aRay.myDirection;
			const Vector3f& min = aAABB.myMin;
			const Vector3f& max = aAABB.myMax;

			// IntersectionPoint
			Vector3f iPoint;

			iPoint = origin;
			if (direction.x != 0.f)
			{
				iPoint = origin + direction * ((min.x - origin.x) / direction.x);
			}
			if (iPoint.x >= min.x && iPoint.x <= max.x && iPoint.y >= min.y && iPoint.y <= max.y && iPoint.z >= min.z && iPoint.z <= max.z)
			{
				return true;
			}
			if (direction.x != 0.f)
			{
				iPoint = origin + direction * ((max.x - origin.x) / direction.x);
			}
			if (iPoint.x >= min.x && iPoint.x <= max.x && iPoint.y >= min.y && iPoint.y <= max.y && iPoint.z >= min.z && iPoint.z <= max.z)
			{
				return true;
			}

			iPoint = origin;
			if (direction.y != 0.f)
			{
				iPoint = origin + direction * ((min.y - origin.y) / direction.y);
			}
			if (iPoint.x >= min.x && iPoint.x <= max.x && iPoint.y >= min.y && iPoint.y <= max.y && iPoint.z >= min.z && iPoint.z <= max.z)
			{
				return true;
			}
			if (direction.y != 0.f)
			{
				iPoint = origin + direction * ((max.y - origin.y) / direction.y);
			}
			if (iPoint.x >= min.x && iPoint.x <= max.x && iPoint.y >= min.y && iPoint.y <= max.y && iPoint.z >= min.z && iPoint.z <= max.z)
			{
				return true;
			}

			iPoint = origin;
			if (direction.z != 0.f)
			{
				iPoint = origin + direction * ((min.z - origin.z) / direction.z);
			}
			if (iPoint.x >= min.x && iPoint.x <= max.x && iPoint.y >= min.y && iPoint.y <= max.y && iPoint.z >= min.z && iPoint.z <= max.z)
			{
				return true;
			}
			if (direction.z != 0.f)
			{
				iPoint = origin + direction * ((max.z - origin.z) / direction.z);
			}
			if (iPoint.x >= min.x && iPoint.x <= max.x && iPoint.y >= min.y && iPoint.y <= max.y && iPoint.z >= min.z && iPoint.z <= max.z)
			{
				return true;
			}

			return false;
		}

		bool IntersectionRayAABB(const Ray & aRay, const AABB3D & aAABB, Vector3f & aIntersectionPoint)
		{
			const Vector3f& origin = aRay.myOrigin;
			const Vector3f& direction = aRay.myDirection;
			const Vector3f& min = aAABB.myMin;
			const Vector3f& max = aAABB.myMax;

			aIntersectionPoint = origin;
			if (direction.x != 0.f)
			{
				aIntersectionPoint = origin + direction * ((min.x - origin.x) / direction.x);
			}
			if (aIntersectionPoint.x >= min.x && aIntersectionPoint.x <= max.x && aIntersectionPoint.y >= min.y && aIntersectionPoint.y <= max.y && aIntersectionPoint.z >= min.z && aIntersectionPoint.z <= max.z)
			{
				return true;
			}
			if (direction.x != 0.f)
			{
				aIntersectionPoint = origin + direction * ((max.x - origin.x) / direction.x);
			}
			if (aIntersectionPoint.x >= min.x && aIntersectionPoint.x <= max.x && aIntersectionPoint.y >= min.y && aIntersectionPoint.y <= max.y && aIntersectionPoint.z >= min.z && aIntersectionPoint.z <= max.z)
			{
				return true;
			}

			aIntersectionPoint = origin;
			if (direction.y != 0.f)
			{
				aIntersectionPoint = origin + direction * ((min.y - origin.y) / direction.y);
			}
			if (aIntersectionPoint.x >= min.x && aIntersectionPoint.x <= max.x && aIntersectionPoint.y >= min.y && aIntersectionPoint.y <= max.y && aIntersectionPoint.z >= min.z && aIntersectionPoint.z <= max.z)
			{
				return true;
			}
			if (direction.y != 0.f)
			{
				aIntersectionPoint = origin + direction * ((max.y - origin.y) / direction.y);
			}
			if (aIntersectionPoint.x >= min.x && aIntersectionPoint.x <= max.x && aIntersectionPoint.y >= min.y && aIntersectionPoint.y <= max.y && aIntersectionPoint.z >= min.z && aIntersectionPoint.z <= max.z)
			{
				return true;
			}

			aIntersectionPoint = origin;
			if (direction.z != 0.f)
			{
				aIntersectionPoint = origin + direction * ((min.z - origin.z) / direction.z);
			}
			if (aIntersectionPoint.x >= min.x && aIntersectionPoint.x <= max.x && aIntersectionPoint.y >= min.y && aIntersectionPoint.y <= max.y && aIntersectionPoint.z >= min.z && aIntersectionPoint.z <= max.z)
			{
				return true;
			}
			if (direction.z != 0.f)
			{
				aIntersectionPoint = origin + direction * ((max.z - origin.z) / direction.z);
			}
			if (aIntersectionPoint.x >= min.x && aIntersectionPoint.x <= max.x && aIntersectionPoint.y >= min.y && aIntersectionPoint.y <= max.y && aIntersectionPoint.z >= min.z && aIntersectionPoint.z <= max.z)
			{
				return true;
			}

			return false;
		}

		bool IntersectionRayAABBPrecise(const Ray & aRay, const AABB3D & aAABB, Vector3f & aIntersectionPoint, float & aFlFraction)
		{
			float fLow = 0.0f;
			float fHigh = 1.0f;

			//Scale distance based on game scale
			auto clipLine = [&aAABB, l0 = aRay.myOrigin, l1 = aRay.myOrigin + aRay.myDirection * 1000.0f, &fLow, &fHigh](char aIndex)
			{
				float fDimLow = (aAABB.myMin[aIndex] - l0[aIndex]) / (l1[aIndex] - l0[aIndex]);
				float fDimHigh = (aAABB.myMax[aIndex] - l0[aIndex]) / (l1[aIndex] - l0[aIndex]);

				if (fDimHigh < fDimLow)
				{
					std::swap(fDimHigh, fDimLow);
				}

				if (fDimHigh < fLow)
				{
					return false;
				}

				if (fDimLow > fHigh)
				{
					return false;
				}

				fLow = std::max(fDimLow, fLow);
				fHigh = std::min(fDimHigh, fHigh);

				if (fLow > fHigh)
				{
					return false;
				}

				return true;
			};

			if (!clipLine(0))
			{
				return false;
			}

			if (!clipLine(1))
			{
				return false;
			}

			if (!clipLine(2))
			{
				return false;
			}

			CU::Vector3f distance = aRay.myDirection * 1000.0f;
			aIntersectionPoint = aRay.myOrigin + distance * fLow;

			aFlFraction = fLow;

			return true;
		}

		bool IntersectionRaySphere(const Ray & aRay, const Sphere & aSphere)
		{
			//Increase or decrease depending on precision and game scale. Formula requires line segment.
			constexpr float rayLength = 1000.0f;

			const CU::Vector3f& center(aSphere.myCenter);
			const float r(aSphere.myRadius);
			const CU::Vector3f& ray0(aRay.myOrigin);
			const CU::Vector3f ray1(aRay.myDirection * rayLength);

			const float a(std::pow((ray1.x - ray0.x), 2) + std::pow((ray1.y - ray0.y), 2) + std::pow((ray1.z - ray0.z), 2));
			const float b(2 * ((ray1.x - ray0.x) * (ray0.x - center.x) + (ray1.y - ray0.y) * (ray0.y - center.y) + (ray1.z - ray0.z) * (ray0.z - center.z)));
			const float c(std::pow((ray0.x - center.x), 2) + std::pow((ray0.y - center.y), 2) + std::pow((ray0.z - center.z), 2) - (r * r));
			
			const float delta((b * b) - (4 * a * c));

			if (delta >= 0)
			{
				return true;
			}

			return false;
		}

		bool IntersectionRayPlane(const Ray& aRay, const Plane& aPlane, Vector3f& aPointOfCollision)
		{
			float pointAndNormal = aRay.myDirection.Dot(aPlane.myNormal);

			if (std::abs(pointAndNormal) <= 1e-6f)
			{
				return false;
			}

			float tDistance = (aPlane.myPoint.Dot(aPlane.myNormal) - aRay.myOrigin.Dot(aPlane.myNormal)) / pointAndNormal;
			aPointOfCollision = aRay.myOrigin + aRay.myDirection * tDistance;

			return true;
		}

		bool IntersectionLineLine(const LineSegment& aLine1, const LineSegment& aLine2, Vector3f& aPointOfCollision)
		{
			//for (unsigned char i(0); i < 3; ++i)
			//{
			//	auto da = aLine1.myP1 - aLine1.myP0;
			//	auto db = aLine2.myP1 - aLine2.myP0;
			//	auto dc = aLine2.myP0 - aLine1.myP0;
			//	da[i] = 0.0f;
			//	db[i] = 0.0f;
			//	dc[i] = 0.0f;
			//
			//	const float i1(std::fabsf(dc.Dot(da.Cross(db))));
			//	if (i1 > 1e-3)
			//	{
			//		return false;
			//	}
			//
			//	const float i2(dc.Cross(db).Dot(da.Cross(db)));
			//	const float i3(da.Cross(db).Length2());
			//	const float s = i2 / i3;
			//
			//	const float i4(-(dc).Cross(da).Dot(db.Cross(da)));
			//	const float i5(da.Cross(db).Length2());
			//	const float t = i4 / i5;
			//
			//	if (s >= 0.0f && s < 1.0f && t >= 0.0f && t < 1.0f)
			//	{
			//		const float da_i = aLine1.myP1[i] - aLine1.myP0[i];
			//		const float db_i = aLine2.myP1[i] - aLine2.myP0[i];
			//
			//		if (da_i == db_i)
			//		{
			//			da[i] = da_i;
			//			aPointOfCollision = aLine1.myP0 + da * s;
			//			return true;
			//		}
			//	}
			//}

			auto da = aLine1.myP1 - aLine1.myP0;
			auto db = aLine2.myP1 - aLine2.myP0;
			auto dc = aLine2.myP0 - aLine1.myP0;
			da.y = 0.0f;
			db.y = 0.0f;
			dc.y = 0.0f;

			const float i1(std::fabsf(dc.Dot(da.Cross(db))));
			if (i1 > 1e-3)
			{
				return false;
			}

			const float i2(dc.Cross(db).Dot(da.Cross(db)));
			const float i3(da.Cross(db).Length2());
			const float s = i2 / i3;

			const float i4(-(dc).Cross(da).Dot(db.Cross(da)));
			const float i5(da.Cross(db).Length2());
			const float t = i4 / i5;

			if (s >= 0.0f && s < 1.0f && t >= 0.0f && t < 1.0f)
			{
				const float da_i = aLine1.myP1.y - aLine1.myP0.y;
				const float db_i = aLine2.myP1.y - aLine2.myP0.y;

				if (da_i == db_i)
				{
					da.y = da_i;
					aPointOfCollision = aLine1.myP0 + da * s;
					return true;
				}
			}

			return false;
		}

		bool IntersectionTrianglePoint(const Triangle& aTriangle, const Vector3f& aPoint)
		{
			const auto edge0 = aTriangle.myPoints[1] - aTriangle.myPoints[0];
			const auto edge1 = aTriangle.myPoints[2] - aTriangle.myPoints[1];
			const auto edge2 = aTriangle.myPoints[0] - aTriangle.myPoints[2];

			if (edge0.Cross(edge1).z > 0.0f)
			{
				if (edge0.Cross(aPoint - aTriangle.myPoints[0]).z > 0.0f &&
					edge1.Cross(aPoint - aTriangle.myPoints[1]).z > 0.0f &&
					edge2.Cross(aPoint - aTriangle.myPoints[2]).z > 0.0f)
				{
					return true;
				}
			}
			else
			{
				if (edge0.Cross(aPoint - aTriangle.myPoints[0]).z <= 0.0f &&
					edge1.Cross(aPoint - aTriangle.myPoints[1]).z <= 0.0f &&
					edge2.Cross(aPoint - aTriangle.myPoints[2]).z <= 0.0f)
				{
					return true;
				}
			}

			return false;
		}

		bool IntersectionTriangleRay(const Triangle& aTriangle, const Ray& aRay, CU::Vector3f& aIntersection_ref)
		{
			if (!IntersectionPlaneLine(Plane(aTriangle.myPoints[0], aTriangle.myNormal), Line3D(aRay.myOrigin, aRay.myDirection), aIntersection_ref))
			{
				return false;
			}

			unsigned char firstIndex(0);
			unsigned char secondIndex(0);
			for (unsigned char side(0); side < 3; ++side)
			{
				firstIndex = side;
				if (side == 2)
				{
					secondIndex = 0;
				}
				else
				{
					secondIndex = side + 1;
				}

				const CU::Vector3f V1(aTriangle.myPoints[firstIndex] - aIntersection_ref);
				const CU::Vector3f V2(aTriangle.myPoints[secondIndex] - aIntersection_ref);
				const CU::Vector3f N1(V2.Cross(V1).GetNormalized());

				const float D1((-aRay.myOrigin).Dot(N1));
				const float D2(aIntersection_ref.Dot(N1) + D1);

				if (D2 < 0.0f)
				{
					return false;
				}
			}

			return true;

			//CU::Vector3f barycentricCoordinates;
			//CU::Vector3f distanceVertex1ToStart = aTriangle.myPoints[1] - aTriangle.myPoints[0];
			//CU::Vector3f distanceVertex2ToStart = aTriangle.myPoints[2] - aTriangle.myPoints[0];
			//CU::Vector3f distancePointToStart = aIntersection_ref - aTriangle.myPoints[0];
			//
			//float d00(distanceVertex1ToStart.Dot(distanceVertex1ToStart));
			//float d01(distanceVertex1ToStart.Dot(distanceVertex2ToStart));
			//float d11(distanceVertex2ToStart.Dot(distanceVertex2ToStart));
			//float d20(distancePointToStart.Dot(distanceVertex1ToStart));
			//float d21(distancePointToStart.Dot(distanceVertex2ToStart));
			//
			//float denom((d00 * d11) - (d01 * d01));
			//barycentricCoordinates.x = ((d11 * d20) - (d01 * d21)) / denom;
			//barycentricCoordinates.y = ((d00 * d21) - (d01 * d20)) / denom;
			//barycentricCoordinates.z = 1.0f - barycentricCoordinates.x - barycentricCoordinates.y;
			//
			///* OLD CODE FOR PRECISION. IT IS HERE JUST IN CASE WE GET SOME PROBLEMS WITH PRECISION.
			//
			//aIntersection_ref = aRay.myOrigin + aRay.myDirection * t;
			//
			//	char cutValue[64];
			//	const std::string setting = "%." + std::to_string(aDecimalPrecision);
			//	CU::Vector3f cutIntersection_float;
			//
			//	sprintf_s(cutValue, 64, (setting + "aIntersection_ref.x\n").c_str(), aIntersection_ref.x);
			//	cutIntersection_float.x = (float)std::atof(cutValue);
			//
			//	sprintf_s(cutValue, 64, (setting + "aIntersection_ref.y\n").c_str(), aIntersection_ref.y);
			//	cutIntersection_float.y = (float)std::atof(cutValue);
			//
			//	sprintf_s(cutValue, 64, (setting + "aIntersection_ref.z\n").c_str(), aIntersection_ref.z);
			//	cutIntersection_float.z = (float)std::atof(cutValue);
			//
			//	aIntersection_ref = cutIntersection_float;
			//
			//*/
			//
			//return(
			//	0.0f <= barycentricCoordinates.x && barycentricCoordinates.x <= 1.0f &&
			//	0.0f <= barycentricCoordinates.y && barycentricCoordinates.y <= 1.0f &&
			//	0.0f <= barycentricCoordinates.z && barycentricCoordinates.z <= 1.0f);
		}
	}
}
