#include "KeyBinds.h"
#include <fstream>
#include <winuser.rh>

#include "../EngineCore/JSON/json.hpp"
#include "../EngineCore/Logger/Logger.h"
#include "../EngineCore/MemoryPool/MemoryPool.h"

using json = nlohmann::json;

CU::CKeyBinds* CU::CKeyBinds::myInstance = nullptr;
CU::CKeyBinds::SHotKeys CU::CKeyBinds::myHotKeys;

namespace CommonUtilities
{

	CKeyBinds::CKeyBinds(const char * aDataFilePath)
	{
		LoadKeyBindData(aDataFilePath);
	}

	const CKeyBinds::SHotKeys & CKeyBinds::GetHotKeys()
	{
		return myHotKeys;
	}

	const bool CKeyBinds::Create(const char * aDataFilePath)
	{
		if (myInstance != nullptr)
		{
			GAME_LOG("FAIL! Failed to create new instance of HotKeys because it was already created.");
			return false;
		}

		myInstance = sce_new(CKeyBinds(aDataFilePath));

		if (myInstance == nullptr)
		{
			GAME_LOG("FAIL! Failed to allocate memory for HotKeys.");
			return false;
		}

		return true;
	}

	void CKeyBinds::Destroy()
	{
		sce_delete(myInstance);
	}

	void CKeyBinds::LoadKeyBindData(const char * aDataFilePath)
	{

		std::ifstream playerDataFile(aDataFilePath);

		if (playerDataFile.good() == false)
		{
			GAME_LOG("ERROR: Hotkeys Filepath was wrong -> [ %s ] ! Defaulting to hardcoded values.", aDataFilePath);

			myHotKeys.myMove_FORWARD = 'W';
			myHotKeys.myMove_RIGHT = 'D';
			myHotKeys.myMove_BACKWARDS = 'S';
			myHotKeys.myMove_LEFT = 'A';
			myHotKeys.myMove_UP = ' ';
			myHotKeys.myMove_DOWN = VK_CONTROL;
			myHotKeys.myBoost = VK_SHIFT;
			myHotKeys.myRoll_RIGHT = 'E';
			myHotKeys.myRoll_LEFT = 'Q';
			myHotKeys.myShoot = VK_LBUTTON;
			myHotKeys.myMissile = VK_RBUTTON;
			myHotKeys.myPause = VK_ESCAPE;
			myHotKeys.myF1 = VK_F1;
			myHotKeys.myF2 = VK_F2;
			myHotKeys.myF3 = VK_F3;
			myHotKeys.myF4 = VK_F4;
			myHotKeys.myF5 = VK_F5;
			myHotKeys.myF6 = VK_F6;
			myHotKeys.myF7 = VK_F7;
			myHotKeys.myF8 = VK_F8;
			myHotKeys.myF9 = VK_F9;
			myHotKeys.myTilde = VK_OEM_5;
			myHotKeys.myEscape = VK_ESCAPE;
			myHotKeys.myEnterInput = VK_RETURN;
			myHotKeys.myArrowUp = VK_UP;
			myHotKeys.myArrowDown = VK_DOWN;
			myHotKeys.myArrowLeft = VK_LEFT;
			myHotKeys.myArrowRight = VK_RIGHT;
			myHotKeys.myTab = VK_TAB;
			myHotKeys.myH = 'H';
			myHotKeys.myNumPad1 = VK_NUMPAD1;
			myHotKeys.myNumPad2 = VK_NUMPAD2;
			myHotKeys.myNumPad3 = VK_NUMPAD3;
			myHotKeys.myNum1 = '1'; //0x31;
			myHotKeys.myNum2 = '2';// 0x32;
			myHotKeys.myNum3 = '3';// 0x33;
		}
		else
		{
			json fileReader;
			playerDataFile >> fileReader;
			playerDataFile.close();

			try
			{

				myHotKeys.myShoot = static_cast<unsigned char>(fileReader["Shoot"].get<int>());
				myHotKeys.myMissile = static_cast<unsigned char>(fileReader["Missile"].get<int>());
				myHotKeys.myMove_FORWARD = static_cast<unsigned char>(fileReader["Move_Forward"].get<int>());
				myHotKeys.myMove_BACKWARDS = static_cast<unsigned char>(fileReader["Move_Backward"].get<int>());
				myHotKeys.myMove_RIGHT = static_cast<unsigned char>(fileReader["Move_Right"].get<int>());
				myHotKeys.myMove_LEFT = static_cast<unsigned char>(fileReader["Move_Left"].get<int>());
				myHotKeys.myMove_UP = static_cast<unsigned char>(fileReader["Move_Up"].get<int>());
				myHotKeys.myMove_DOWN = static_cast<unsigned char>(fileReader["Move_Down"].get<int>());
				myHotKeys.myRoll_RIGHT = static_cast<unsigned char>(fileReader["Roll_Right"].get<int>());
				myHotKeys.myRoll_LEFT = static_cast<unsigned char>(fileReader["Roll_Left"].get<int>());
				myHotKeys.myBoost = static_cast<unsigned char>(fileReader["Boost"].get<int>());
				myHotKeys.myPause = static_cast<unsigned char>(fileReader["PauseMenu"].get<int>());
				myHotKeys.myF1 = VK_F1;
				myHotKeys.myF2 = VK_F2;
				myHotKeys.myF3 = VK_F3;
				myHotKeys.myF4 = VK_F4;
				myHotKeys.myF5 = VK_F5;
				myHotKeys.myF6 = VK_F6;
				myHotKeys.myF7 = VK_F7;
				myHotKeys.myF8 = VK_F8;
				myHotKeys.myF9 = VK_F9;
				myHotKeys.myTilde = VK_OEM_5;
				myHotKeys.myEscape = VK_ESCAPE;
				myHotKeys.myEnterInput = VK_RETURN;
				myHotKeys.myArrowUp = VK_UP;
				myHotKeys.myArrowDown = VK_DOWN;
				myHotKeys.myArrowLeft = VK_LEFT;
				myHotKeys.myArrowRight = VK_RIGHT;
				myHotKeys.myTab = VK_TAB;
				myHotKeys.myH = 'H';
				myHotKeys.myNumPad1 = VK_NUMPAD1;
				myHotKeys.myNumPad2 = VK_NUMPAD2;
				myHotKeys.myNumPad3 = VK_NUMPAD3;
				myHotKeys.myNum1 = '1'; //0x31;
				myHotKeys.myNum2 = '2';// 0x32;
				myHotKeys.myNum3 = '3';// 0x33;
			}
			catch (...)
			{
				GAME_LOG("ERROR: Wrong values in %s! Defaulting to hardcoded values.", aDataFilePath);

				myHotKeys.myMove_FORWARD = 'W';
				myHotKeys.myMove_RIGHT = 'D';
				myHotKeys.myMove_BACKWARDS = 'S';
				myHotKeys.myMove_LEFT = 'A';
				myHotKeys.myMove_UP = ' ';
				myHotKeys.myMove_DOWN = VK_CONTROL;
				myHotKeys.myBoost = VK_SHIFT;
				myHotKeys.myRoll_RIGHT = 'E';
				myHotKeys.myRoll_LEFT = 'Q';
				myHotKeys.myShoot = VK_LBUTTON;
				myHotKeys.myMissile = VK_RBUTTON;
				myHotKeys.myPause = VK_ESCAPE;
				myHotKeys.myF1 = VK_F1;
				myHotKeys.myF2 = VK_F2;
				myHotKeys.myF3 = VK_F3;
				myHotKeys.myF4 = VK_F4;
				myHotKeys.myF5 = VK_F5;
				myHotKeys.myF6 = VK_F6;
				myHotKeys.myF7 = VK_F7;
				myHotKeys.myF8 = VK_F8;
				myHotKeys.myF9 = VK_F9;
				myHotKeys.myTilde = VK_OEM_5;
				myHotKeys.myEscape = VK_ESCAPE;
				myHotKeys.myEnterInput = VK_RETURN;
				myHotKeys.myArrowUp = VK_UP;
				myHotKeys.myArrowDown = VK_DOWN;
				myHotKeys.myArrowLeft = VK_LEFT;
				myHotKeys.myArrowRight = VK_RIGHT;
				myHotKeys.myTab = VK_TAB;
				myHotKeys.myH = 'H';
				myHotKeys.myNumPad1 = VK_NUMPAD1;
				myHotKeys.myNumPad2 = VK_NUMPAD2;
				myHotKeys.myNumPad3 = VK_NUMPAD3;
				myHotKeys.myNum1 = '1'; //0x31;
				myHotKeys.myNum2 = '2';// 0x32;
				myHotKeys.myNum3 = '3';// 0x33;
			}
		}
	}
}