#pragma once
#include "Vector.h"
#include <cassert>
#ifdef _DEBUG
#include <xmmintrin.h>
#endif // _DEBUG

#define MATRIX44_SIZE 16

namespace CommonUtilities
{
	template <typename ObjectType>
	class Matrix44
	{
	public:
		union
		{
#pragma warning(push)
#pragma warning(disable : 4201)
			struct
			{
				ObjectType	m11, m12, m13, m14,
							m21, m22, m23, m24,
							m31, m32, m33, m34,
							m41, m42, m43, m44;
			};
			struct
			{
				ObjectType myMatrix[MATRIX44_SIZE];
			};
#ifdef _DEBUG
			struct // Used in Matrix44<float>
			{
				__m128 m128_1, m128_2, m128_3, m128_4;
			};
#endif // _DEBUG
#pragma warning(pop)
		};

		Matrix44();
		Matrix44(const Matrix44& aMatrix);
		Matrix44(const ObjectType a11, const ObjectType a12, const ObjectType a13, const ObjectType a14,
			const ObjectType a21, const ObjectType a22, const ObjectType a23, const ObjectType a24,
			const ObjectType a31, const ObjectType a32, const ObjectType a33, const ObjectType a34,
			const ObjectType a41, const ObjectType a42, const ObjectType a43, const ObjectType a44);
#ifdef _DEBUG
		Matrix44(const __m128 aRegister1, const __m128 aRegister2, const __m128 aRegister3, const __m128 aRegister4);
#endif // _DEBUG
		Matrix44(const CU::Vector4<ObjectType>& aRight, const CU::Vector4<ObjectType>& aUp, const CU::Vector4<ObjectType>& aLook, const CU::Vector4<ObjectType>& aPosition);
		~Matrix44();

		Matrix44 operator+(const Matrix44& aMatrix) const;
		Matrix44& operator+=(const Matrix44& aMatrix);
		Matrix44 operator-(const Matrix44& aMatrix) const;
		Matrix44& operator-=(const Matrix44& aMatrix);
		Matrix44 operator*(const Matrix44& aMatrix) const;
		Matrix44& operator*=(const Matrix44& aMatrix);
		bool operator==(const Matrix44& aMatrix) const;
		Matrix44& operator=(const Matrix44& aMatrix);
		ObjectType& operator[](const int aIndex);
		const ObjectType& operator[](const int aIndex) const;
		void SetTranslation(const Vector4<ObjectType>& aPoint);
		void SetRotation(const Matrix44& aMatrix);
		void SetPosition(const Vector3<ObjectType>& aPosition);

		const Vector3<ObjectType> GetPosition() const;
		const Vector3<ObjectType> GetFront() const;
		const Vector3<ObjectType> GetTop() const;

		const Vector3<ObjectType> GetScale() const;
		Vector4<ObjectType> GetTranslation() const;
		Matrix44<ObjectType> GetFastInverse() const;
		void ScaleBy(const Vector3<ObjectType>& aScale);

		///Call this bitch every frame and you'll get nothing but coal for christmas
		Matrix44<ObjectType> GetProperInverse() const;

		ObjectType GetMinor(unsigned int aRow, unsigned int aColumn) const;

		ObjectType GetTrace() const;

		Vector4<ObjectType> operator*(const Vector4<ObjectType>& aVector) const;
		Vector3<ObjectType> operator*(const Vector3<ObjectType>& aVector) const;

		static Matrix44 CreateRotateAroundX(ObjectType aAngleInRadius);
		static Matrix44 CreateRotateAroundY(ObjectType aAngleInRadius);
		static Matrix44 CreateRotateAroundZ(ObjectType aAngleInRadius);
		static Matrix44 CreateRotateAroundAngle(const Vector3<ObjectType>& aAxis, ObjectType aAngleInRadians);
		static Matrix44 CreateRotateAroundAngleGoldman(const Vector3<ObjectType>& aAxis, ObjectType aAngleInRadians);
		static Matrix44 CreateRotateAroundEulerAngles(const Vector3<ObjectType>& aEulerAngles);
		
		static Matrix44 Transpose(const Matrix44& aMatrixToTranspose);
		static Matrix44 CreateTranslation(const Vector4<ObjectType>& aPoint);
		static Matrix44<float> InverseSimple(const Matrix44<float>& aMatrix)
		{
			CU::Matrix44<float> inverse(aMatrix);

			CU::Vector4<float> translation = inverse.GetTranslation();
			inverse.SetTranslation(CU::Vector4<float>(0, 0, 0, 1.f));
			translation *= -1.f;
			translation.w = 1.f;
			inverse = Transpose(inverse);
			translation = translation * inverse;

			inverse.SetTranslation(translation);
			return inverse;
		}


		Matrix44<ObjectType> CreateProjectionMatrix(ObjectType aNearZ, ObjectType aFarZ, ObjectType aWidth, ObjectType aHeight, ObjectType aFovAngle, bool aIsLeftHanded = true)
		{
			aIsLeftHanded;
			Matrix44<ObjectType> temp;
			ObjectType aspectRatio = aHeight / aWidth;

			ObjectType SinFov;
			ObjectType CosFov;
			ObjectType Height;
			ObjectType Width;

			SinFov = sin(0.5f * aFovAngle);
			CosFov = cos(0.5f * aFovAngle);

			Width = CosFov / SinFov;
			Height = Width / aspectRatio;

			ObjectType scaling = aFarZ / (aFarZ - aNearZ);

			temp.myMatrix[0] = Width;
			temp.myMatrix[5] = Height;
			temp.myMatrix[10] = scaling;
			temp.myMatrix[11] = 1.0f;

			temp.myMatrix[14] = -scaling * aNearZ;
			temp.myMatrix[15] = 0.0f;
			return temp;
		}

		static const Matrix44 Identity, Zero;
	};

	template <typename ObjectType>
	const Matrix44<ObjectType> Matrix44<ObjectType>::Zero(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

	template <typename ObjectType>
	const Matrix44<ObjectType> Matrix44<ObjectType>::Identity;
	
	template<typename ObjectType>
	inline Matrix44<ObjectType>::Matrix44()
		: myMatrix{ 1, 0, 0, 0
				, 0, 1, 0, 0
				, 0, 0, 1, 0
				, 0, 0, 0, 1 }
	{
	}
	
	template<typename ObjectType>
	inline Matrix44<ObjectType>::Matrix44(const Matrix44& aMatrix)
		: myMatrix{ aMatrix[0], aMatrix[1], aMatrix[2], aMatrix[3]
				, aMatrix[4], aMatrix[5], aMatrix[6], aMatrix[7]
				, aMatrix[8], aMatrix[9], aMatrix[10], aMatrix[11]
				, aMatrix[12], aMatrix[13], aMatrix[14], aMatrix[15] }
	{
	}

#ifdef _DEBUG
	inline Matrix44<float>::Matrix44(const Matrix44& aMatrix)
	{
		m128_1 = aMatrix.m128_1;
		m128_2 = aMatrix.m128_2;
		m128_3 = aMatrix.m128_3;
		m128_4 = aMatrix.m128_4;
	}
#endif // _DEBUG

	template<typename ObjectType>
	inline Matrix44<ObjectType>::Matrix44(const ObjectType a11, const ObjectType a12, const ObjectType a13, const ObjectType a14,
		const ObjectType a21, const ObjectType a22, const ObjectType a23, const ObjectType a24,
		const ObjectType a31, const ObjectType a32, const ObjectType a33, const ObjectType a34,
		const ObjectType a41, const ObjectType a42, const ObjectType a43, const ObjectType a44)
		: myMatrix{ a11, a12, a13, a14
				, a21, a22, a23, a24
				, a31, a32, a33, a34
				, a41, a42, a43, a44 }
	{
	}

#ifdef _DEBUG
	template<typename ObjectType>
	inline Matrix44<ObjectType>::Matrix44(const __m128 aRegister1, const __m128 aRegister2, const __m128 aRegister3, const __m128 aRegister4) = delete;
	inline Matrix44<float>::Matrix44(const __m128 aRegister1, const __m128 aRegister2, const __m128 aRegister3, const __m128 aRegister4)
		: m128_1(aRegister1)
		, m128_2(aRegister2)
		, m128_3(aRegister3)
		, m128_4(aRegister4)
	{
	}
#endif // _DEBUG

	template<typename ObjectType>
	inline Matrix44<ObjectType>::Matrix44(const CU::Vector4<ObjectType>& aRight, const CU::Vector4<ObjectType>& aUp, const CU::Vector4<ObjectType>& aLook, const CU::Vector4<ObjectType>& aPosition)
		: myMatrix{ aRight.x, aRight.y, aRight.z, aRight.w
				, aUp.x, aUp.y, aUp.z, aUp.w
				, aLook.x, aLook.y, aLook.z, aLook.w
				, aPosition.x, aPosition.y, aPosition.z, aPosition.w }
	{
	}
	
	template<typename ObjectType>
	inline Matrix44<ObjectType>::~Matrix44()
	{
	}

	template<typename ObjectType>
	void Matrix44<ObjectType>::ScaleBy(const Vector3<ObjectType>& aScale)
	{
		Matrix44<ObjectType> scale;
		scale[0] = aScale.x;
		scale[5] = aScale.y;
		scale[10] = aScale.z;

		const CU::Vector3<ObjectType> pos(GetPosition());
		*this *= scale;
		SetPosition(pos);
	}

	template<typename ObjectType>
	inline Matrix44<ObjectType> Matrix44<ObjectType>::operator+(const Matrix44& aMatrix) const
	{
		Matrix44<ObjectType> newMatrix(*this);
		newMatrix += aMatrix;

		return newMatrix;
	}

	template<typename ObjectType>
	inline Matrix44<ObjectType>& Matrix44<ObjectType>::operator+=(const Matrix44& aMatrix)
	{
		myMatrix[0] += aMatrix.myMatrix[0];
		myMatrix[1] += aMatrix.myMatrix[1];
		myMatrix[2] += aMatrix.myMatrix[2];
		myMatrix[3] += aMatrix.myMatrix[3];

		myMatrix[4] += aMatrix.myMatrix[4];
		myMatrix[5] += aMatrix.myMatrix[5];
		myMatrix[6] += aMatrix.myMatrix[6];
		myMatrix[7] += aMatrix.myMatrix[7];

		myMatrix[8] += aMatrix.myMatrix[8];
		myMatrix[9] += aMatrix.myMatrix[9];
		myMatrix[10] += aMatrix.myMatrix[10];
		myMatrix[11] += aMatrix.myMatrix[11];

		myMatrix[12] += aMatrix.myMatrix[12];
		myMatrix[13] += aMatrix.myMatrix[13];
		myMatrix[14] += aMatrix.myMatrix[14];
		myMatrix[15] += aMatrix.myMatrix[15];

		return *(this);
	}

#ifdef _DEBUG
	inline Matrix44<float> Matrix44<float>::operator+(const Matrix44& aMatrix) const
	{
		return Matrix44<float>
		(
			_mm_add_ps(aMatrix.m128_1, m128_1)
			, _mm_add_ps(aMatrix.m128_2, m128_2)
			, _mm_add_ps(aMatrix.m128_3, m128_3)
			, _mm_add_ps(aMatrix.m128_4, m128_4)
		);
	}

	inline Matrix44<float>& Matrix44<float>::operator+=(const Matrix44& aMatrix)
	{
		m128_1 = _mm_add_ps(aMatrix.m128_1, m128_1);
		m128_2 = _mm_add_ps(aMatrix.m128_2, m128_2);
		m128_3 = _mm_add_ps(aMatrix.m128_3, m128_3);
		m128_4 = _mm_add_ps(aMatrix.m128_4, m128_4);

		return *(this);
	}
#endif // _DEBUG

	template<typename ObjectType>
	inline Matrix44<ObjectType> Matrix44<ObjectType>::operator-(const Matrix44& aMatrix) const
	{
		CommonUtilities::Matrix44<ObjectType> newMatrix(*this);
		newMatrix -= aMatrix;

		return newMatrix;
	}

	template<typename ObjectType>
	inline Matrix44<ObjectType>& Matrix44<ObjectType>::operator-=(const Matrix44& aMatrix)
	{
		myMatrix[0] -= aMatrix.myMatrix[0];
		myMatrix[1] -= aMatrix.myMatrix[1];
		myMatrix[2] -= aMatrix.myMatrix[2];
		myMatrix[3] -= aMatrix.myMatrix[3];

		myMatrix[4] -= aMatrix.myMatrix[4];
		myMatrix[5] -= aMatrix.myMatrix[5];
		myMatrix[6] -= aMatrix.myMatrix[6];
		myMatrix[7] -= aMatrix.myMatrix[7];

		myMatrix[8] -= aMatrix.myMatrix[8];
		myMatrix[9] -= aMatrix.myMatrix[9];
		myMatrix[10] -= aMatrix.myMatrix[10];
		myMatrix[11] -= aMatrix.myMatrix[11];

		myMatrix[12] -= aMatrix.myMatrix[12];
		myMatrix[13] -= aMatrix.myMatrix[13];
		myMatrix[14] -= aMatrix.myMatrix[14];
		myMatrix[15] -= aMatrix.myMatrix[15];

		return *(this);
	}

#ifdef _DEBUG
	inline Matrix44<float> Matrix44<float>::operator-(const Matrix44& aMatrix) const
	{
		return Matrix44<float>
		(
			_mm_min_ps(m128_1, aMatrix.m128_1)
			, _mm_min_ps(m128_2, aMatrix.m128_2)
			, _mm_min_ps(m128_3, aMatrix.m128_3)
			, _mm_min_ps(m128_4, aMatrix.m128_4)
		);
	}

	inline Matrix44<float>& Matrix44<float>::operator-=(const Matrix44& aMatrix)
	{
		m128_1 = _mm_min_ps(m128_1, aMatrix.m128_1);
		m128_2 = _mm_min_ps(m128_2, aMatrix.m128_2);
		m128_3 = _mm_min_ps(m128_3, aMatrix.m128_3);
		m128_4 = _mm_min_ps(m128_4, aMatrix.m128_4);

		return *(this);
	}
#endif // _DEBUG
	
	template<typename ObjectType>
	inline Matrix44<ObjectType> Matrix44<ObjectType>::CreateRotateAroundX(ObjectType aAngleInRadius)
	{
		ObjectType cosinus = static_cast<ObjectType>(std::cos(static_cast<double>(aAngleInRadius)));
		ObjectType sinus = static_cast<ObjectType>(std::sin(static_cast<double>(aAngleInRadius)));

		Matrix44<ObjectType> rotationMatrix;
		rotationMatrix.myMatrix[0] = 1;
		rotationMatrix.myMatrix[1] = 0;
		rotationMatrix.myMatrix[2] = 0;
		rotationMatrix.myMatrix[3] = 0;
		rotationMatrix.myMatrix[4] = 0;
		rotationMatrix.myMatrix[5] = cosinus;
		rotationMatrix.myMatrix[6] = sinus;
		rotationMatrix.myMatrix[7] = 0;
		rotationMatrix.myMatrix[8] = 0;
		rotationMatrix.myMatrix[9] = -sinus;
		rotationMatrix.myMatrix[10] = cosinus;
		rotationMatrix.myMatrix[11] = 0;
		rotationMatrix.myMatrix[12] = 0;
		rotationMatrix.myMatrix[13] = 0;
		rotationMatrix.myMatrix[14] = 0;
		rotationMatrix.myMatrix[15] = 1;

		return rotationMatrix;
	}
	
	template<typename ObjectType>
	inline Matrix44<ObjectType> Matrix44<ObjectType>::CreateRotateAroundY(ObjectType aAngleInRadius)
	{
		ObjectType cosinus = static_cast<ObjectType>(std::cos(static_cast<double>(aAngleInRadius)));
		ObjectType sinus = static_cast<ObjectType>(std::sin(static_cast<double>(aAngleInRadius)));

		Matrix44<ObjectType> rotationMatrix;
		rotationMatrix.myMatrix[0] = cosinus;
		rotationMatrix.myMatrix[1] = 0;
		rotationMatrix.myMatrix[2] = -sinus;
		rotationMatrix.myMatrix[3] = 0;
		rotationMatrix.myMatrix[4] = 0;
		rotationMatrix.myMatrix[5] = 1;
		rotationMatrix.myMatrix[6] = 0;
		rotationMatrix.myMatrix[7] = 0;
		rotationMatrix.myMatrix[8] = sinus;
		rotationMatrix.myMatrix[9] = 0;
		rotationMatrix.myMatrix[10] = cosinus;
		rotationMatrix.myMatrix[11] = 0;
		rotationMatrix.myMatrix[12] = 0;
		rotationMatrix.myMatrix[13] = 0;
		rotationMatrix.myMatrix[14] = 0;
		rotationMatrix.myMatrix[15] = 1;

		return rotationMatrix;
	}
	
	template<typename ObjectType>
	inline Matrix44<ObjectType> Matrix44<ObjectType>::CreateRotateAroundZ(ObjectType aAngleInRadius)
	{
		ObjectType cosinus = static_cast<ObjectType>(std::cos(static_cast<double>(aAngleInRadius)));
		ObjectType sinus = static_cast<ObjectType>(std::sin(static_cast<double>(aAngleInRadius)));

		Matrix44<ObjectType> rotationMatrix;
		rotationMatrix.myMatrix[0] = cosinus;
		rotationMatrix.myMatrix[1] = sinus;
		rotationMatrix.myMatrix[2] = 0;
		rotationMatrix.myMatrix[3] = 0;
		rotationMatrix.myMatrix[4] = -sinus;
		rotationMatrix.myMatrix[5] = cosinus;
		rotationMatrix.myMatrix[6] = 0;
		rotationMatrix.myMatrix[7] = 0;
		rotationMatrix.myMatrix[8] = 0;
		rotationMatrix.myMatrix[9] = 0;
		rotationMatrix.myMatrix[10] = 1;
		rotationMatrix.myMatrix[11] = 0;
		rotationMatrix.myMatrix[12] = 0;
		rotationMatrix.myMatrix[13] = 0;
		rotationMatrix.myMatrix[14] = 0;
		rotationMatrix.myMatrix[15] = 1;

		return rotationMatrix;
	}

	template<typename ObjectType>
	inline Matrix44<ObjectType> Matrix44<ObjectType>::CreateRotateAroundAngle(const Vector3<ObjectType>& aAxis, ObjectType aAngleInRadians)
	{
		CU::Matrix44<ObjectType> matrix;
		float length2 = aAxis.Length2();
		float length = sqrt(length2);
		float x2 = aAxis.x * aAxis.x;
		float y2 = aAxis.y * aAxis.y;
		float z2 = aAxis.z * aAxis.z;
		float cosAngle = cos(aAngleInRadians);
		float sinAngle = sin(aAngleInRadians);

		//Rows, columns
		matrix[0] = (x2 + (y2 + z2) * cosAngle) / length2;
		matrix[1] = (aAxis.x * aAxis.y * (1 - cosAngle) + aAxis.z * length * sinAngle) / length2;
		matrix[2] = (aAxis.x * aAxis.y * (1 - cosAngle) - aAxis.y * length * sinAngle) / length2;
		matrix[3] = 0;

		matrix[4] = (aAxis.x * aAxis.y * (1 - cosAngle) - aAxis.z * length * sinAngle) / length2;
		matrix[5] = (y2 + (x2 + z2) * cosAngle) / length2;
		matrix[6] = (aAxis.y * aAxis.z * (1 - cosAngle) + aAxis.x * length * sinAngle) / length2;
		matrix[7] = 0;

		matrix[8] = (aAxis.x * aAxis.z * (1 - cosAngle) + aAxis.y * length * sinAngle) / length2;
		matrix[9] = (aAxis.y * aAxis.z * (1 - cosAngle) - aAxis.x * length * sinAngle) / length2;
		matrix[10] = (z2 + (x2 + y2) * cosAngle) / length2;
		matrix[11] = 0;

		matrix[12] = 0;
		matrix[13] = 0;
		matrix[14] = 0;
		matrix[15] = 1;

		return matrix;
	}

	template<typename ObjectType>
	inline Matrix44<ObjectType> Matrix44<ObjectType>::CreateRotateAroundAngleGoldman(const Vector3<ObjectType>& aAxis, ObjectType aAngleInRadians)
	{
		//Goldman's handy dandy axis rotator!
		ObjectType cosAngle = static_cast<ObjectType>(std::cos(static_cast<double>(aAngleInRadians)));
		ObjectType sinAngle = static_cast<ObjectType>(std::sin(static_cast<double>(aAngleInRadians)));
		CU::Matrix44<ObjectType> matrix;

		matrix.myMatrix[0] = cosAngle + ((1 - cosAngle) * aAxis.x * aAxis.x);
		matrix.myMatrix[1] = ((1 - cosAngle) * aAxis.x * aAxis.y) - (aAxis.z * sinAngle);
		matrix.myMatrix[2] = ((1 - cosAngle) * aAxis.x * aAxis.z) + (aAxis.y * sinAngle);
		matrix.myMatrix[3] = 0;

		matrix.myMatrix[4] = ((1 - cosAngle) * aAxis.x * aAxis.y) + (aAxis.z * sinAngle);
		matrix.myMatrix[5] = cosAngle + ((1 - cosAngle) * aAxis.y * aAxis.y);
		matrix.myMatrix[6] = ((1 - cosAngle) * aAxis.y * aAxis.z) - (aAxis.x * sinAngle);
		matrix.myMatrix[7] = 0;

		matrix.myMatrix[8] = ((1 - cosAngle) * aAxis.x * aAxis.z) - (aAxis.y * sinAngle);
		matrix.myMatrix[9] = ((1 - cosAngle) * aAxis.y * aAxis.z) + (aAxis.x * sinAngle);
		matrix.myMatrix[10] = cosAngle + ((1 - cosAngle) * aAxis.z * aAxis.z);
		matrix.myMatrix[11] = 0;

		matrix.myMatrix[12] = 0;
		matrix.myMatrix[13] = 0;
		matrix.myMatrix[14] = 0;
		matrix.myMatrix[15] = 1;

		return matrix;
	}

	template<typename ObjectType>
	inline Matrix44<ObjectType> Matrix44<ObjectType>::CreateRotateAroundEulerAngles(const Vector3<ObjectType>& aEulerAngles)
	{
		//roll
		float sGamma = sin(aEulerAngles.z);
		float cGamma = cos(aEulerAngles.z);
		//pitch
		float sBeta = sin(aEulerAngles.x);
		float cBeta = cos(aEulerAngles.x);
		//yaw
		float sAlpha = sin(aEulerAngles.y);
		float cAlpha = cos(aEulerAngles.y);

		Matrix44<ObjectType> matrix =
		{
			cAlpha * cBeta,
			cAlpha * sBeta * sGamma - sAlpha * cGamma,
			cAlpha * sBeta * cGamma + sAlpha * sGamma,
			0,

			sAlpha * cBeta,
			sAlpha * sBeta * sGamma + cAlpha * cGamma,
			sAlpha * sBeta * cGamma - cAlpha * sGamma,
			0,

			-sBeta,
			cBeta * sGamma,
			cBeta * cGamma,
			0,

			0, 0, 0, 1
		};

		CreateRotateAroundX(aEulerAngles.x);

		return matrix;
	}
	
	template<typename ObjectType>
	inline Matrix44<ObjectType> Matrix44<ObjectType>::Transpose(const Matrix44 & aMatrixToTranspose)
	{
		CommonUtilities::Matrix44<ObjectType> transposedMatrix;

		transposedMatrix.myMatrix[0] = aMatrixToTranspose.myMatrix[0];
		transposedMatrix.myMatrix[1] = aMatrixToTranspose.myMatrix[4];
		transposedMatrix.myMatrix[2] = aMatrixToTranspose.myMatrix[8];
		transposedMatrix.myMatrix[3] = aMatrixToTranspose.myMatrix[12];
		transposedMatrix.myMatrix[4] = aMatrixToTranspose.myMatrix[1];
		transposedMatrix.myMatrix[5] = aMatrixToTranspose.myMatrix[5];
		transposedMatrix.myMatrix[6] = aMatrixToTranspose.myMatrix[9];
		transposedMatrix.myMatrix[7] = aMatrixToTranspose.myMatrix[13];
		transposedMatrix.myMatrix[8] = aMatrixToTranspose.myMatrix[2];
		transposedMatrix.myMatrix[9] = aMatrixToTranspose.myMatrix[6];
		transposedMatrix.myMatrix[10] = aMatrixToTranspose.myMatrix[10];
		transposedMatrix.myMatrix[11] = aMatrixToTranspose.myMatrix[14];
		transposedMatrix.myMatrix[12] = aMatrixToTranspose.myMatrix[3];
		transposedMatrix.myMatrix[13] = aMatrixToTranspose.myMatrix[7];
		transposedMatrix.myMatrix[14] = aMatrixToTranspose.myMatrix[11];
		transposedMatrix.myMatrix[15] = aMatrixToTranspose.myMatrix[15];

		return transposedMatrix;
	}

	template<typename ObjectType>
	inline Matrix44<ObjectType> Matrix44<ObjectType>::operator*(const Matrix44 & aMatrix) const
	{
		Matrix44<ObjectType> newMatrix;

		newMatrix[0] = myMatrix[0] * aMatrix[0] + myMatrix[1] * aMatrix[4] + myMatrix[2] * aMatrix[8] + myMatrix[3] * aMatrix[12];
		newMatrix[1] = myMatrix[0] * aMatrix[1] + myMatrix[1] * aMatrix[5] + myMatrix[2] * aMatrix[9] + myMatrix[3] * aMatrix[13];
		newMatrix[2] = myMatrix[0] * aMatrix[2] + myMatrix[1] * aMatrix[6] + myMatrix[2] * aMatrix[10] + myMatrix[3] * aMatrix[14];
		newMatrix[3] = myMatrix[0] * aMatrix[3] + myMatrix[1] * aMatrix[7] + myMatrix[2] * aMatrix[11] + myMatrix[3] * aMatrix[15];

		newMatrix[4] = myMatrix[4] * aMatrix[0] + myMatrix[5] * aMatrix[4] + myMatrix[6] * aMatrix[8] + myMatrix[7] * aMatrix[12];
		newMatrix[5] = myMatrix[4] * aMatrix[1] + myMatrix[5] * aMatrix[5] + myMatrix[6] * aMatrix[9] + myMatrix[7] * aMatrix[13];
		newMatrix[6] = myMatrix[4] * aMatrix[2] + myMatrix[5] * aMatrix[6] + myMatrix[6] * aMatrix[10] + myMatrix[7] * aMatrix[14];
		newMatrix[7] = myMatrix[4] * aMatrix[3] + myMatrix[5] * aMatrix[7] + myMatrix[6] * aMatrix[11] + myMatrix[7] * aMatrix[15];

		newMatrix[8] = myMatrix[8] * aMatrix[0] + myMatrix[9] * aMatrix[4] + myMatrix[10] * aMatrix[8] + myMatrix[11] * aMatrix[12];
		newMatrix[9] = myMatrix[8] * aMatrix[1] + myMatrix[9] * aMatrix[5] + myMatrix[10] * aMatrix[9] + myMatrix[11] * aMatrix[13];
		newMatrix[10] = myMatrix[8] * aMatrix[2] + myMatrix[9] * aMatrix[6] + myMatrix[10] * aMatrix[10] + myMatrix[11] * aMatrix[14];
		newMatrix[11] = myMatrix[8] * aMatrix[3] + myMatrix[9] * aMatrix[7] + myMatrix[10] * aMatrix[11] + myMatrix[11] * aMatrix[15];

		newMatrix[12] = myMatrix[12] * aMatrix[0] + myMatrix[13] * aMatrix[4] + myMatrix[14] * aMatrix[8] + myMatrix[15] * aMatrix[12];
		newMatrix[13] = myMatrix[12] * aMatrix[1] + myMatrix[13] * aMatrix[5] + myMatrix[14] * aMatrix[9] + myMatrix[15] * aMatrix[13];
		newMatrix[14] = myMatrix[12] * aMatrix[2] + myMatrix[13] * aMatrix[6] + myMatrix[14] * aMatrix[10] + myMatrix[15] * aMatrix[14];
		newMatrix[15] = myMatrix[12] * aMatrix[3] + myMatrix[13] * aMatrix[7] + myMatrix[14] * aMatrix[11] + myMatrix[15] * aMatrix[15];

		return newMatrix;
	}

	template<typename ObjectType>
	inline Matrix44<ObjectType>& Matrix44<ObjectType>::operator*=(const Matrix44 & aMatrix)
	{
		ObjectType tempMatrix[16]{ myMatrix[0], myMatrix[1], myMatrix[2], myMatrix[3]
								, myMatrix[4], myMatrix[5], myMatrix[6], myMatrix[7]
								, myMatrix[8], myMatrix[9], myMatrix[10], myMatrix[11]
								, myMatrix[12], myMatrix[13], myMatrix[14], myMatrix[15] };

		myMatrix[0] = (tempMatrix[0] * aMatrix[0]) + (tempMatrix[1] * aMatrix[4]) + (tempMatrix[2] * aMatrix[8]) + (tempMatrix[3] * aMatrix[12]);
		myMatrix[1] = (tempMatrix[0] * aMatrix[1]) + (tempMatrix[1] * aMatrix[5]) + (tempMatrix[2] * aMatrix[9]) + (tempMatrix[3] * aMatrix[13]);
		myMatrix[2] = (tempMatrix[0] * aMatrix[2]) + (tempMatrix[1] * aMatrix[6]) + (tempMatrix[2] * aMatrix[10]) + (tempMatrix[3] * aMatrix[14]);
		myMatrix[3] = (tempMatrix[0] * aMatrix[3]) + (tempMatrix[1] * aMatrix[7]) + (tempMatrix[2] * aMatrix[11]) + (tempMatrix[3] * aMatrix[15]);

		myMatrix[4] = (tempMatrix[4] * aMatrix[0]) + (tempMatrix[5] * aMatrix[4]) + (tempMatrix[6] * aMatrix[8]) + (tempMatrix[7] * aMatrix[12]);
		myMatrix[5] = (tempMatrix[4] * aMatrix[1]) + (tempMatrix[5] * aMatrix[5]) + (tempMatrix[6] * aMatrix[9]) + (tempMatrix[7] * aMatrix[13]);
		myMatrix[6] = (tempMatrix[4] * aMatrix[2]) + (tempMatrix[5] * aMatrix[6]) + (tempMatrix[6] * aMatrix[10]) + (tempMatrix[7] * aMatrix[14]);
		myMatrix[7] = (tempMatrix[4] * aMatrix[3]) + (tempMatrix[5] * aMatrix[7]) + (tempMatrix[6] * aMatrix[11]) + (tempMatrix[7] * aMatrix[15]);

		myMatrix[8] = (tempMatrix[8] * aMatrix[0]) + (tempMatrix[9] * aMatrix[4]) + (tempMatrix[10] * aMatrix[8]) + (tempMatrix[11] * aMatrix[12]);
		myMatrix[9] = (tempMatrix[8] * aMatrix[1]) + (tempMatrix[9] * aMatrix[5]) + (tempMatrix[10] * aMatrix[9]) + (tempMatrix[11] * aMatrix[13]);
		myMatrix[10] = (tempMatrix[8] * aMatrix[2]) + (tempMatrix[9] * aMatrix[6]) + (tempMatrix[10] * aMatrix[10]) + (tempMatrix[11] * aMatrix[14]);
		myMatrix[11] = (tempMatrix[8] * aMatrix[3]) + (tempMatrix[9] * aMatrix[7]) + (tempMatrix[10] * aMatrix[11]) + (tempMatrix[11] * aMatrix[15]);

		myMatrix[12] = (tempMatrix[12] * aMatrix[0]) + (tempMatrix[13] * aMatrix[4]) + (tempMatrix[14] * aMatrix[8]) + (tempMatrix[15] * aMatrix[12]);
		myMatrix[13] = (tempMatrix[12] * aMatrix[1]) + (tempMatrix[13] * aMatrix[5]) + (tempMatrix[14] * aMatrix[9]) + (tempMatrix[15] * aMatrix[13]);
		myMatrix[14] = (tempMatrix[12] * aMatrix[2]) + (tempMatrix[13] * aMatrix[6]) + (tempMatrix[14] * aMatrix[10]) + (tempMatrix[15] * aMatrix[14]);
		myMatrix[15] = (tempMatrix[12] * aMatrix[3]) + (tempMatrix[13] * aMatrix[7]) + (tempMatrix[14] * aMatrix[11]) + (tempMatrix[15] * aMatrix[15]);

		return *this;
	}

#ifdef _DEBUG
	inline Matrix44<float> Matrix44<float>::operator*(const Matrix44<float>& aMatrix) const
	{
		Matrix44<float> returnMatrix;

		const __m128 register0 = aMatrix.m128_1;
		const __m128 register1 = aMatrix.m128_2;
		const __m128 register2 = aMatrix.m128_3;
		const __m128 register3 = aMatrix.m128_4;

		__m128 tempRegister0(_mm_set1_ps(myMatrix[0]));
		__m128 tempRegister1(_mm_mul_ps(register0, tempRegister0));
		tempRegister0 = _mm_set1_ps(myMatrix[1]);
		tempRegister1 = _mm_add_ps(_mm_mul_ps(register1, tempRegister0), tempRegister1);
		tempRegister0 = _mm_set1_ps(myMatrix[2]);
		tempRegister1 = _mm_add_ps(_mm_mul_ps(register2, tempRegister0), tempRegister1);
		tempRegister0 = _mm_set1_ps(myMatrix[3]);
		tempRegister1 = _mm_add_ps(_mm_mul_ps(register3, tempRegister0), tempRegister1);

		returnMatrix.m128_1 = tempRegister1;

		tempRegister0 = _mm_set1_ps(myMatrix[4]);
		tempRegister1 = _mm_mul_ps(register0, tempRegister0);
		tempRegister0 = _mm_set1_ps(myMatrix[5]);
		tempRegister1 = _mm_add_ps(_mm_mul_ps(register1, tempRegister0), tempRegister1);
		tempRegister0 = _mm_set1_ps(myMatrix[6]);
		tempRegister1 = _mm_add_ps(_mm_mul_ps(register2, tempRegister0), tempRegister1);
		tempRegister0 = _mm_set1_ps(myMatrix[7]);
		tempRegister1 = _mm_add_ps(_mm_mul_ps(register3, tempRegister0), tempRegister1);

		returnMatrix.m128_2 = tempRegister1;

		tempRegister0 = _mm_set1_ps(myMatrix[8]);
		tempRegister1 = _mm_mul_ps(register0, tempRegister0);
		tempRegister0 = _mm_set1_ps(myMatrix[9]);
		tempRegister1 = _mm_add_ps(_mm_mul_ps(register1, tempRegister0), tempRegister1);
		tempRegister0 = _mm_set1_ps(myMatrix[10]);
		tempRegister1 = _mm_add_ps(_mm_mul_ps(register2, tempRegister0), tempRegister1);
		tempRegister0 = _mm_set1_ps(myMatrix[11]);
		tempRegister1 = _mm_add_ps(_mm_mul_ps(register3, tempRegister0), tempRegister1);

		returnMatrix.m128_3 = tempRegister1;

		tempRegister0 = _mm_set1_ps(myMatrix[12]);
		tempRegister1 = _mm_mul_ps(register0, tempRegister0);
		tempRegister0 = _mm_set1_ps(myMatrix[13]);
		tempRegister1 = _mm_add_ps(_mm_mul_ps(register1, tempRegister0), tempRegister1);
		tempRegister0 = _mm_set1_ps(myMatrix[14]);
		tempRegister1 = _mm_add_ps(_mm_mul_ps(register2, tempRegister0), tempRegister1);
		tempRegister0 = _mm_set1_ps(myMatrix[15]);
		tempRegister1 = _mm_add_ps(_mm_mul_ps(register3, tempRegister0), tempRegister1);

		returnMatrix.m128_4 = tempRegister1;

		return returnMatrix;
	}

	inline Matrix44<float>& Matrix44<float>::operator*=(const Matrix44<float>& aMatrix)
	{
		float tempMatrix[16]{ myMatrix[0], myMatrix[1], myMatrix[2], myMatrix[3]
			, myMatrix[4], myMatrix[5], myMatrix[6], myMatrix[7]
			, myMatrix[8], myMatrix[9], myMatrix[10], myMatrix[11]
			, myMatrix[12], myMatrix[13], myMatrix[14], myMatrix[15] };

		const __m128 register0 = aMatrix.m128_1;
		const __m128 register1 = aMatrix.m128_2;
		const __m128 register2 = aMatrix.m128_3;
		const __m128 register3 = aMatrix.m128_4;

		__m128 tempRegister0(_mm_set1_ps(tempMatrix[0]));
		__m128 tempRegister1(_mm_mul_ps(register0, tempRegister0));
		tempRegister0 = _mm_set1_ps(tempMatrix[1]);
		tempRegister1 = _mm_add_ps(_mm_mul_ps(register1, tempRegister0), tempRegister1);
		tempRegister0 = _mm_set1_ps(tempMatrix[2]);
		tempRegister1 = _mm_add_ps(_mm_mul_ps(register2, tempRegister0), tempRegister1);
		tempRegister0 = _mm_set1_ps(tempMatrix[3]);
		tempRegister1 = _mm_add_ps(_mm_mul_ps(register3, tempRegister0), tempRegister1);

		m128_1 = tempRegister1;

		tempRegister0 = _mm_set1_ps(tempMatrix[4]);
		tempRegister1 = _mm_mul_ps(register0, tempRegister0);
		tempRegister0 = _mm_set1_ps(tempMatrix[5]);
		tempRegister1 = _mm_add_ps(_mm_mul_ps(register1, tempRegister0), tempRegister1);
		tempRegister0 = _mm_set1_ps(tempMatrix[6]);
		tempRegister1 = _mm_add_ps(_mm_mul_ps(register2, tempRegister0), tempRegister1);
		tempRegister0 = _mm_set1_ps(tempMatrix[7]);
		tempRegister1 = _mm_add_ps(_mm_mul_ps(register3, tempRegister0), tempRegister1);

		m128_2 = tempRegister1;

		tempRegister0 = _mm_set1_ps(tempMatrix[8]);
		tempRegister1 = _mm_mul_ps(register0, tempRegister0);
		tempRegister0 = _mm_set1_ps(tempMatrix[9]);
		tempRegister1 = _mm_add_ps(_mm_mul_ps(register1, tempRegister0), tempRegister1);
		tempRegister0 = _mm_set1_ps(tempMatrix[10]);
		tempRegister1 = _mm_add_ps(_mm_mul_ps(register2, tempRegister0), tempRegister1);
		tempRegister0 = _mm_set1_ps(tempMatrix[11]);
		tempRegister1 = _mm_add_ps(_mm_mul_ps(register3, tempRegister0), tempRegister1);

		m128_3 = tempRegister1;

		tempRegister0 = _mm_set1_ps(tempMatrix[12]);
		tempRegister1 = _mm_mul_ps(register0, tempRegister0);
		tempRegister0 = _mm_set1_ps(tempMatrix[13]);
		tempRegister1 = _mm_add_ps(_mm_mul_ps(register1, tempRegister0), tempRegister1);
		tempRegister0 = _mm_set1_ps(tempMatrix[14]);
		tempRegister1 = _mm_add_ps(_mm_mul_ps(register2, tempRegister0), tempRegister1);
		tempRegister0 = _mm_set1_ps(tempMatrix[15]);
		tempRegister1 = _mm_add_ps(_mm_mul_ps(register3, tempRegister0), tempRegister1);

		m128_4 = tempRegister1;

		return *this;
	}
#endif // _DEBUG

	template<typename ObjectType>
	inline bool Matrix44<ObjectType>::operator==(const Matrix44& aMatrix) const
	{
#define MATRIX44_EQUAL_HELPER(aIndex) (myMatrix[aIndex] == aMatrix.myMatrix[aIndex])
		if (MATRIX44_EQUAL_HELPER(0) && MATRIX44_EQUAL_HELPER(1) && MATRIX44_EQUAL_HELPER(2) && MATRIX44_EQUAL_HELPER(3)
			&& MATRIX44_EQUAL_HELPER(4) && MATRIX44_EQUAL_HELPER(5) && MATRIX44_EQUAL_HELPER(6) && MATRIX44_EQUAL_HELPER(7)
			&& MATRIX44_EQUAL_HELPER(8) && MATRIX44_EQUAL_HELPER(9) && MATRIX44_EQUAL_HELPER(10) && MATRIX44_EQUAL_HELPER(11)
			&& MATRIX44_EQUAL_HELPER(12) && MATRIX44_EQUAL_HELPER(13) && MATRIX44_EQUAL_HELPER(14) && MATRIX44_EQUAL_HELPER(15))
		{
			return true;
		}
#undef MATRIX44_EQUAL_HELPER

		return false;
	}

#ifdef _DEBUG
	inline bool Matrix44<float>::operator==(const Matrix44& aMatrix) const
	{
		__m128 tempRegister(_mm_cmpeq_ps(m128_1, aMatrix.m128_1));

		if ((((long long*)(&tempRegister))[0] != 0xffffffffffffffffll) || (((long long*)(&tempRegister))[1] != 0xffffffffffffffffll))
		{
			return false;
		}
		if ((((long long*)(&(tempRegister = _mm_cmpeq_ps(m128_2, aMatrix.m128_2))))[0] != 0xffffffffffffffffll) || (((long long*)(&tempRegister))[1] != 0xffffffffffffffffll))
		{
			return false;
		}
		if ((((long long*)(&(tempRegister = _mm_cmpeq_ps(m128_3, aMatrix.m128_3))))[0] != 0xffffffffffffffffll) || (((long long*)(&tempRegister))[1] != 0xffffffffffffffffll))
		{
			return false;
		}
		if ((((long long*)(&(tempRegister = _mm_cmpeq_ps(m128_4, aMatrix.m128_4))))[0] != 0xffffffffffffffffll) || (((long long*)(&tempRegister))[1] != 0xffffffffffffffffll))
		{
			return false;
		}

		return true;
	}
#endif // _DEBUG

	template<typename ObjectType>
	inline Matrix44<ObjectType>& Matrix44<ObjectType>::operator=(const Matrix44& aMatrix)
	{
		myMatrix[0] = aMatrix.myMatrix[0];
		myMatrix[1] = aMatrix.myMatrix[1];
		myMatrix[2] = aMatrix.myMatrix[2];
		myMatrix[3] = aMatrix.myMatrix[3];

		myMatrix[4] = aMatrix.myMatrix[4];
		myMatrix[5] = aMatrix.myMatrix[5];
		myMatrix[6] = aMatrix.myMatrix[6];
		myMatrix[7] = aMatrix.myMatrix[7];

		myMatrix[8] = aMatrix.myMatrix[8];
		myMatrix[9] = aMatrix.myMatrix[9];
		myMatrix[10] = aMatrix.myMatrix[10];
		myMatrix[11] = aMatrix.myMatrix[11];

		myMatrix[12] = aMatrix.myMatrix[12];
		myMatrix[13] = aMatrix.myMatrix[13];
		myMatrix[14] = aMatrix.myMatrix[14];
		myMatrix[15] = aMatrix.myMatrix[15];

		return *(this);
	}

#ifdef _DEBUG
	inline Matrix44<float>& Matrix44<float>::operator=(const Matrix44 & aMatrix)
	{
		m128_1 = aMatrix.m128_1;
		m128_2 = aMatrix.m128_2;
		m128_3 = aMatrix.m128_3;
		m128_4 = aMatrix.m128_4;

		return *(this);
	}
#endif // _DEBUG

	template<typename ObjectType>
	inline ObjectType& Matrix44<ObjectType>::operator[](const int aIndex)
	{
		assert(aIndex >= 0 && aIndex < 16 && "Index out of bounds");
		return myMatrix[aIndex];
	}

	template<typename ObjectType>
	inline const ObjectType& Matrix44<ObjectType>::operator[](const int aIndex) const
	{
		assert(aIndex >= 0 && aIndex < 16 && "Index out of bounds");
		return myMatrix[aIndex];
	}

	template<typename ObjectType>
	inline Vector4<ObjectType> Matrix44<ObjectType>::operator*(const Vector4<ObjectType>& aVector4) const
	{
		Vector4<ObjectType> column1 = { myMatrix[0], myMatrix[4], myMatrix[8], myMatrix[12] };
		Vector4<ObjectType> column2 = { myMatrix[1], myMatrix[5], myMatrix[9], myMatrix[13] };
		Vector4<ObjectType> column3 = { myMatrix[2], myMatrix[6], myMatrix[10], myMatrix[14] };
		Vector4<ObjectType> column4 = { myMatrix[3], myMatrix[7], myMatrix[11], myMatrix[15] };

		Vector4<ObjectType> newVector(aVector4.x * column1.x + aVector4.y * column1.y + aVector4.z * column1.z + aVector4.w * column1.w
			, aVector4.x * column2.x + aVector4.y * column2.y + aVector4.z * column2.z + aVector4.w * column2.w
			, aVector4.x * column3.x + aVector4.y * column3.y + aVector4.z * column3.z + aVector4.w * column3.w
			, aVector4.x * column4.x + aVector4.y * column4.y + aVector4.z * column4.z + aVector4.w * column4.w);

		return newVector;
	}

	template<class ObjectType>
	inline Vector4<ObjectType> operator*(const Matrix44<ObjectType>& aMatrix44, const Vector4<ObjectType>& aVector4)
	{
		Vector4<ObjectType> row1 = { aMatrix44.myMatrix[0], aMatrix44.myMatrix[1], aMatrix44.myMatrix[2], aMatrix44.myMatrix[3] };
		Vector4<ObjectType> row2 = { aMatrix44.myMatrix[4], aMatrix44.myMatrix[5], aMatrix44.myMatrix[6], aMatrix44.myMatrix[7] };
		Vector4<ObjectType> row3 = { aMatrix44.myMatrix[8], aMatrix44.myMatrix[9], aMatrix44.myMatrix[10], aMatrix44.myMatrix[11] };
		Vector4<ObjectType> row4 = { aMatrix44.myMatrix[12], aMatrix44.myMatrix[13], aMatrix44.myMatrix[14], aMatrix44.myMatrix[15] };

		Vector4<ObjectType> newVector(aVector4.x * row1.x + aVector4.y * row1.y + aVector4.z * row1.z + aVector4.w * row1.w
			, aVector4.x * row2.x + aVector4.y * row2.y + aVector4.z * row2.z + aVector4.w * row2.w
			, aVector4.x * row3.x + aVector4.y * row3.y + aVector4.z * row3.z + aVector4.w * row3.w
			, aVector4.x * row4.x + aVector4.y * row4.y + aVector4.z * row4.z + aVector4.w * row4.w);

		return newVector;
	}

	template<typename ObjectType>
	inline Vector3<ObjectType> Matrix44<ObjectType>::operator*(const Vector3<ObjectType>& aVector3) const
	{
		Vector3<ObjectType> column1 = { myMatrix[0], myMatrix[4], myMatrix[8] };
		Vector3<ObjectType> column2 = { myMatrix[1], myMatrix[5], myMatrix[9] };
		Vector3<ObjectType> column3 = { myMatrix[2], myMatrix[6], myMatrix[10] };

		Vector3<ObjectType> newVector(aVector3.x * column1.x + aVector3.y * column1.y + aVector3.z * column1.z
			, aVector3.x * column2.x + aVector3.y * column2.y + aVector3.z * column2.z
			, aVector3.x * column3.x + aVector3.y * column3.y + aVector3.z * column3.z);

		return newVector;
	}

	template<class ObjectType>
	inline Vector4<ObjectType> operator*(const Vector4<ObjectType>& aVector4, const Matrix44<ObjectType>& aMatrix44)
	{
		Vector4<ObjectType> column1 = { aMatrix44.myMatrix[0], aMatrix44.myMatrix[4], aMatrix44.myMatrix[8], aMatrix44.myMatrix[12] };
		Vector4<ObjectType> column2 = { aMatrix44.myMatrix[1], aMatrix44.myMatrix[5], aMatrix44.myMatrix[9], aMatrix44.myMatrix[13] };
		Vector4<ObjectType> column3 = { aMatrix44.myMatrix[2], aMatrix44.myMatrix[6], aMatrix44.myMatrix[10], aMatrix44.myMatrix[14] };
		Vector4<ObjectType> column4 = { aMatrix44.myMatrix[3], aMatrix44.myMatrix[7], aMatrix44.myMatrix[11], aMatrix44.myMatrix[15] };

		Vector4<ObjectType> newVector(aVector4.x * column1.x + aVector4.y * column1.y + aVector4.z * column1.z + aVector4.w * column1.w
			, aVector4.x * column2.x + aVector4.y * column2.y + aVector4.z * column2.z + aVector4.w * column2.w
			, aVector4.x * column3.x + aVector4.y * column3.y + aVector4.z * column3.z + aVector4.w * column3.w
			, aVector4.x * column4.x + aVector4.y * column4.y + aVector4.z * column4.z + aVector4.w * column4.w);

		return newVector;
	}

	template<class ObjectType>
	inline Matrix44<ObjectType> Matrix44<ObjectType>::CreateTranslation(const Vector4<ObjectType>& aPoint)
	{
		return Matrix44<ObjectType>(
			1, 0, 0, 0,
			0, 1, 0, 0,
			0, 0, 1, 0,
			aPoint.x, aPoint.y, aPoint.z, aPoint.w
			);
	}

	template<class ObjectType>
	inline void Matrix44<ObjectType>::SetTranslation(const Vector4<ObjectType>& aPoint)
	{
		myMatrix[12] = aPoint.x;
		myMatrix[13] = aPoint.y;
		myMatrix[14] = aPoint.z;
		myMatrix[15] = aPoint.w;
	}

	template<typename ObjectType>
	inline void Matrix44<ObjectType>::SetRotation(const Matrix44& aMatrix)
	{
		myMatrix[0] = aMatrix[0];
		myMatrix[1] = aMatrix[1];
		myMatrix[2] = aMatrix[2];

		myMatrix[4] = aMatrix[4];
		myMatrix[5] = aMatrix[5];
		myMatrix[6] = aMatrix[6];

		myMatrix[8] = aMatrix[8];
		myMatrix[9] = aMatrix[9];
		myMatrix[10] = aMatrix[10];
	}

	template<typename ObjectType>
	inline void Matrix44<ObjectType>::SetPosition(const Vector3<ObjectType>& aPosition)
	{
		myMatrix[12] = aPosition.x;
		myMatrix[13] = aPosition.y;
		myMatrix[14] = aPosition.z;
	}

	template<class ObjectType>
	inline const Vector3<ObjectType> Matrix44<ObjectType>::GetPosition() const
	{
		return Vector3<ObjectType>(myMatrix[12], myMatrix[13], myMatrix[14]);
	}

	template<class ObjectType>
	inline const Vector3<ObjectType> Matrix44<ObjectType>::GetFront() const
	{
		return Vector3<ObjectType>(myMatrix[8], myMatrix[9], myMatrix[10]);
	}

	template<class ObjectType>
	inline const Vector3<ObjectType> Matrix44<ObjectType>::GetTop() const
	{
		return Vector3<ObjectType>(myMatrix[4], myMatrix[5], myMatrix[6]);
	}

	template<class ObjectType>
	inline const Vector3<ObjectType> Matrix44<ObjectType>::GetScale() const
	{
		return Vector3<ObjectType>(myMatrix[0], myMatrix[5], myMatrix[10]);
	}

	template<class ObjectType>
	inline Vector4<ObjectType> Matrix44<ObjectType>::GetTranslation() const
	{
		return Vector4<ObjectType>(myMatrix[12], myMatrix[13], myMatrix[14], myMatrix[15]);
	}

	template<class ObjectType>
	inline Matrix44<ObjectType> Matrix44<ObjectType>::GetFastInverse() const
	{
		return Matrix44<ObjectType>(

			myMatrix[0], myMatrix[4], myMatrix[8], myMatrix[3],
			myMatrix[1], myMatrix[5], myMatrix[9], myMatrix[7],
			myMatrix[2], myMatrix[6], myMatrix[10], myMatrix[11],

			(-myMatrix[12] * myMatrix[0]) + (-myMatrix[13] * myMatrix[1]) + (-myMatrix[14] * myMatrix[2]),
			(-myMatrix[12] * myMatrix[4]) + (-myMatrix[13] * myMatrix[5]) + (-myMatrix[14] * myMatrix[6]),
			(-myMatrix[12] * myMatrix[8]) + (-myMatrix[13] * myMatrix[9]) + (-myMatrix[14] * myMatrix[10]),

			myMatrix[15]
			);
	}

	template<typename ObjectType>
	inline Matrix44<ObjectType> Matrix44<ObjectType>::GetProperInverse() const 
	{
		Matrix44<ObjectType> matrix;

		for (int i = 0; i < 4; ++i)
		{
			for (int j = 0; j < 4; ++j)
			{
				matrix[j * 4 + i] = GetMinor(i, j);
			}
		}

		double determinant = 0;
		
		for (int k = 0; k < 4; ++k)
		{
			determinant += static_cast<double>(myMatrix[k]) * static_cast<double>(matrix[k * 4]);
		}

		if (determinant == 0)
		{
			assert(false && "Determinant of matrix is undefined");
			return Matrix44<ObjectType>();
		}

		determinant = static_cast<ObjectType>(1) / determinant;

		for (int i = 0; i < MATRIX44_SIZE; ++i)
		{
			matrix[i] *= static_cast<ObjectType>(determinant);
		}

		return matrix;
	}

	template<typename ObjectType>
	inline ObjectType Matrix44<ObjectType>::GetMinor(unsigned int aRow, unsigned int aColumn) const 
	{
		int offset = 2 + (aColumn - aRow);

		aRow += 4 + offset;
		aColumn += 4 - offset;

#define e(a, b) myMatrix[((aColumn + b)%4)*4 + ((aRow + a)%4)]

		ObjectType minor =
			+ e(+1, -1) * e(+0, +0) * e(-1, +1)
			+ e(+1, +1) * e(+0, -1) * e(-1, +0)
			+ e(-1, -1) * e(+1, +0) * e(+0, +1)
			- e(-1, -1) * e(+0, +0) * e(+1, +1)
			- e(-1, +1) * e(+0, -1) * e(+1, +0)
			- e(+1, -1) * e(-1, +0) * e(+0, +1);

		return (offset % 2 == 1) ? minor : -minor;

#undef e
	}

	template<typename ObjectType>
	inline ObjectType Matrix44<ObjectType>::GetTrace() const
	{
		return myMatrix[0] + myMatrix[5] + myMatrix[10] + myMatrix[15];
	}

	using Matrix44f = Matrix44<float>;
}