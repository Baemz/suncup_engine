#include "Timer.h"
#include <assert.h>

CommonUtilities::Timer::Timer()
{
	myDeltaTime = 0;
	myTotalTime = 0;
	myPreviousTime = clock.now();
	myStartTime = clock.now();
}


CommonUtilities::Timer::~Timer()
{
	
}

void CommonUtilities::Timer::Update()
{
	std::chrono::high_resolution_clock::time_point pointOne = clock.now();

	std::chrono::duration<float> deltaTime = std::chrono::duration_cast<std::chrono::duration<float>>(pointOne - myPreviousTime);
	std::chrono::duration<double> totalTime = std::chrono::duration_cast<std::chrono::duration<double>>(pointOne - myStartTime);

	myDeltaTime = deltaTime.count();
	myTotalTime = totalTime.count();

	myPreviousTime = pointOne;
}

float CommonUtilities::Timer::GetDeltaTime() const
{
	return myDeltaTime;
}

double CommonUtilities::Timer::GetTotalTime() const
{
	return myTotalTime;
}
