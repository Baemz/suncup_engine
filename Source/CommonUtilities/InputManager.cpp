#include "InputManager.h"
#include <string>
#include <windows.h>
#include "..\EngineCore\MemoryPool\MemoryPool.h"

#define ARRAY_SIZE(aArray) ((sizeof(aArray) / sizeof((aArray)[0])))

CommonUtilities::InputManager::InputManager(bool aUseRawMouseInput)
	: myCurrentFrameKeyStates{ false }
	, myPreviousFrameKeyStates{ false }
	, myCurrentFrameMouseButtonStates{ false }
	, myPreviousFrameMouseButtonStates{ false }
{
	POINT point;
	point.x = myCurrentMousePosition.x;
	point.y = myCurrentMousePosition.y;
	GetCursorPos(&point);
	myScrollMovementSinceLastFrame = 0;
	myCurrentScrollMovement = 0;
	myScrollPosition = 0;
	myRawMouseDeltaX = 0;
	myRawMouseDeltaY = 0;

	myActiveWindow = GetActiveWindow();

	if (aUseRawMouseInput == true)
	{
		RAWINPUTDEVICE Rid[1];

		Rid[0].usUsagePage = 0x01;
		Rid[0].usUsage = 0x02;
		Rid[0].dwFlags = 0;
		Rid[0].hwndTarget = 0; 

		if (RegisterRawInputDevices(Rid, 1, sizeof(Rid[0])) == FALSE)
		{
			//TODO: Mouse Not Registered
		}
	}

	myLostFocus = false;
}


CommonUtilities::InputManager::~InputManager()
{
}


void CommonUtilities::InputManager::Update()
{
	memcpy(myPreviousFrameKeyStates, myCurrentFrameKeyStates, ARRAY_SIZE(myCurrentFrameKeyStates));
	memcpy(myPreviousFrameMouseButtonStates, myCurrentFrameMouseButtonStates, ARRAY_SIZE(myCurrentFrameMouseButtonStates));

	if (myCurrentScrollMovement == 0)
	{
		myScrollMovementSinceLastFrame = 0;
	} 
	else
	{
		myScrollMovementSinceLastFrame = ((myCurrentScrollMovement) -(myScrollMovementSinceLastFrame));
		myScrollPosition += myScrollMovementSinceLastFrame;
	}

	myMouseMovementSinceLastFrame.x = (myCurrentMousePosition.x - (myPreviousMousePosition.x));
	myMouseMovementSinceLastFrame.y = (myCurrentMousePosition.y - (myPreviousMousePosition.y));
	myPreviousMousePosition = myCurrentMousePosition;
	myCurrentScrollMovement = 0;

	myRawMouseDeltaX = 0;
	myRawMouseDeltaY = 0;
}

void CommonUtilities::InputManager::ResetInput()
{
	for (auto& key : myCurrentFrameKeyStates)
	{
		key = false;
	}
	for (auto& button : myCurrentFrameMouseButtonStates)
	{
		button = false;
	}
}

void CommonUtilities::InputManager::CursorToScreen(void* aMsg)
{
	MSG msg = *(MSG*)aMsg;
	POINT point;
	GetCursorPos(&point);
	ScreenToClient(myActiveWindow, &point);

	myCurrentMousePosition.x = point.x;
	if (myCurrentMousePosition.x <= 0.f)
	{
		myCurrentMousePosition.x = 0;
	}
	myCurrentMousePosition.y = point.y;
}

void CommonUtilities::InputManager::SetKeyIsDown(const unsigned char aKey)
{
	myCurrentFrameKeyStates[aKey] = true;
}

void CommonUtilities::InputManager::SetKeyIsUp(const unsigned char aKey)
{
	myCurrentFrameKeyStates[aKey] = false;
}

bool CommonUtilities::InputManager::WasKeyJustPressed(const unsigned char aKey) const
{
	return (myCurrentFrameKeyStates[aKey] == true && myPreviousFrameKeyStates[aKey] == false);
}

bool CommonUtilities::InputManager::IsKeyDown(const unsigned char aKey) const
{
	return myCurrentFrameKeyStates[aKey];
}

bool CommonUtilities::InputManager::WasKeyJustReleased(const unsigned char aKey) const
{
	return (myCurrentFrameKeyStates[aKey] == false && myPreviousFrameKeyStates[aKey] == true);
}

bool CommonUtilities::InputManager::IsKeyUp(const unsigned char aKey) const
{
	return (myCurrentFrameKeyStates[aKey] == false);
}

int CommonUtilities::InputManager::GetMousePositionX() const
{
	return myCurrentMousePosition.x;
}

int CommonUtilities::InputManager::GetMousePositionY() const
{
	return myCurrentMousePosition.y;
}

void CommonUtilities::InputManager::SetMousePosition(const int aPositionX, const int aPositionY)
{
	POINT point;
	point.x = myCurrentMousePosition.x;
	point.y = myCurrentMousePosition.y;
	if (SetCursorPos(aPositionX, aPositionY) == false)
	{
		SetCursorPos(point.x, point.y);
	}
}

int CommonUtilities::InputManager::GetMouseXMovementSinceLastFrame() const
{
	return myMouseMovementSinceLastFrame.x;
}

int CommonUtilities::InputManager::GetMouseYMovementSinceLastFrame() const
{
	return myMouseMovementSinceLastFrame.y;
}

long CommonUtilities::InputManager::GetRawMouseXMovementSinceLastFrame() const
{
	return myRawMouseDeltaX;
}

long CommonUtilities::InputManager::GetRawMouseYMovementSinceLastFrame() const
{
	return myRawMouseDeltaY;
}

void CommonUtilities::InputManager::SetLeftMouseButtonDown()
{
	myCurrentFrameMouseButtonStates[0] = true;
}

void CommonUtilities::InputManager::SetLeftMouseButtonUp()
{
	myCurrentFrameMouseButtonStates[0] = false;
}

void CommonUtilities::InputManager::SetRightMouseButtonDown()
{
	myCurrentFrameMouseButtonStates[1] = true;
}

void CommonUtilities::InputManager::SetRightMouseButtonUp()
{
	myCurrentFrameMouseButtonStates[1] = false;
}

void CommonUtilities::InputManager::SetMiddleMouseButtonDown()
{
	myCurrentFrameMouseButtonStates[2] = true;
}

void CommonUtilities::InputManager::SetMiddleMouseButtonUp()
{
	myCurrentFrameMouseButtonStates[2] = false;
}

void CommonUtilities::InputManager::SetScrollMovement(const long aScrollMovement)
{
	myCurrentScrollMovement = aScrollMovement / WHEEL_DELTA;
}

long CommonUtilities::InputManager::GetScrollMovementSinceLastFrame() const
{
	return myScrollMovementSinceLastFrame;
}

long CommonUtilities::InputManager::GetScrollPosition() const
{
	return myScrollPosition;
}

bool CommonUtilities::InputManager::IsMouseButtonDown(const unsigned int aButton) const
{
	if (aButton > 0 && aButton <= ARRAY_SIZE(myCurrentFrameMouseButtonStates))
	{
		return myCurrentFrameMouseButtonStates[aButton - 1];
	} 
	else
	{
		return false;
	}
}

bool CommonUtilities::InputManager::WasMouseButtonJustPressed(const unsigned int aButton) const
{
	if (aButton > 0 && aButton <= ARRAY_SIZE(myCurrentFrameMouseButtonStates))
	{
		return (myCurrentFrameMouseButtonStates[aButton - 1] == true && myPreviousFrameMouseButtonStates[aButton - 1] == false);
	}
	else
	{
		return false;
	}
}

bool CommonUtilities::InputManager::WasMouseButtonJustReleased(const unsigned int aButton) const
{
	if (aButton > 0 && aButton <= ARRAY_SIZE(myCurrentFrameMouseButtonStates))
	{
		return (myCurrentFrameMouseButtonStates[aButton - 1] == false && myPreviousFrameMouseButtonStates[aButton - 1] == true);
	}
	else
	{
		return false;
	}
}

void CommonUtilities::InputManager::HandleInput(unsigned int message, unsigned long long wParam, long long lParam)
{
	switch (message)
	{
	case WM_MOUSEMOVE:
	{
		//Called when the cursor moves.
		this->CursorToScreen(&message);
		break;
	}
	case WM_RBUTTONDOWN:
	{
		this->SetRightMouseButtonDown();
		break;
	}
	case WM_RBUTTONUP:
	{
		this->SetRightMouseButtonUp();
		break;
	}
	case WM_LBUTTONDOWN:
	{
		this->SetLeftMouseButtonDown();
		break;
	}
	case WM_LBUTTONUP:
	{
		this->SetLeftMouseButtonUp();
		break;
	}
	case WM_MBUTTONDOWN:
	{
		this->SetMiddleMouseButtonDown();
		break;
	}
	case WM_MBUTTONUP:
	{
		this->SetMiddleMouseButtonUp();
		break;
	}
	case WM_MOUSEWHEEL:
	{
		this->SetScrollMovement(GET_WHEEL_DELTA_WPARAM(wParam));
		break;
	}
	case WM_KEYDOWN:
	{
		if (wParam < 256)
		{
			this->SetKeyIsDown(static_cast<uint8_t>(wParam));
		}
		break;
	}
	case WM_KEYUP:
	{
		if (wParam < 256)
		{
			this->SetKeyIsUp(static_cast<uint8_t>(wParam));
		}
		break;
	}
	case WM_INPUT:
	{
		UINT dwSize;

		GetRawInputData((HRAWINPUT)lParam, RID_INPUT, nullptr, &dwSize, sizeof(RAWINPUTHEADER));
		static LPBYTE lpb = new BYTE[dwSize];
		assert("Fix /Jacob" && (dwSize == 48));
		if (lpb == nullptr)
		{
			break;
		}

		if (GetRawInputData((HRAWINPUT)lParam, RID_INPUT, lpb, &dwSize, sizeof(RAWINPUTHEADER)) != dwSize)
		{
			OutputDebugString(TEXT("GetRawInputData does not return correct size !\n"));
		}

		RAWINPUT* raw = (RAWINPUT*)lpb;

		if (raw->header.dwType == RIM_TYPEMOUSE)
		{
			myRawMouseDeltaX = raw->data.mouse.lLastX;
			myRawMouseDeltaY = raw->data.mouse.lLastY;
		}

		break;
	}
	case WM_KILLFOCUS:
	{
		this->myLostFocus = true;
		for (auto& key : myCurrentFrameKeyStates)
		{
			key = false;
		}
		this->myCurrentScrollMovement = 0;
		for (auto& button : myCurrentFrameMouseButtonStates)
		{
			button = false;
		}
		break;
	}
	case WM_ACTIVATE:
	{
		if (wParam == WA_CLICKACTIVE)
		{
			for (auto& button : myCurrentFrameMouseButtonStates)
			{
				button = false;
			}
		}
		break;
	}
	case WM_SETFOCUS:
	{
		this->myLostFocus = false;
	}
	default:
		break;
	}

}

