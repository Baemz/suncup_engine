#pragma once
#include <cstring>
#include <assert.h>
namespace CommonUtilities
{
	template <typename T, int vectorSize, typename CountType = unsigned short, bool UseSafeModeFlag = true>
	class VectorOnStack
	{
	public:
		VectorOnStack();
		VectorOnStack(const VectorOnStack& aVectorOnStack);

		~VectorOnStack();

		VectorOnStack& operator=(const VectorOnStack & aVectorOnStack);

		inline const T& operator[](const CountType & aIndex) const;
		inline T& operator[](const CountType & aIndex);

		inline void Add(const T& aObject);
		inline void Insert(CountType aIndex, T& aObject);
		inline void DeleteCyclic(const T& aObject);
		inline void DeleteCyclicAtIndex(CountType aIndex);
		inline void RemoveCyclic(const T& aObject);
		inline void RemoveCyclicAtIndex(CountType aIndex);

		inline void Clear();
		inline void DeleteAll();

		inline const T& GetLast() const;
		inline CountType Size() const;

	private:
		CountType mySize;
		T myVector[vectorSize];
	};

	template<typename T, int vectorSize, typename CountType, bool UseSafeModeFlag>
	inline VectorOnStack<T, vectorSize, CountType, UseSafeModeFlag>::VectorOnStack()
	{
		mySize = 0;
	}
	
	template<typename T, int vectorSize, typename CountType, bool UseSafeModeFlag>
	inline VectorOnStack<T, vectorSize, CountType, UseSafeModeFlag>::VectorOnStack(const VectorOnStack& aVectorOnStack)
	{
		if (UseSafeModeFlag)
		{
			for (int i = 0; i < aVectorOnStack.Size(); i++)
			{
				myVector[i] = aVectorOnStack[i];
				mySize++;
			}
		} 
		else
		{
			std::memcpy(this, &aVectorOnStack, sizeof(aVectorOnStack));
		}
	}
	
	template<typename T, int vectorSize, typename CountType, bool UseSafeModeFlag>
	inline VectorOnStack<T, vectorSize, CountType, UseSafeModeFlag>::~VectorOnStack()
	{

		mySize = 0;
	}
	
	template<typename T, int vectorSize, typename CountType, bool UseSafeModeFlag>
	inline VectorOnStack<T, vectorSize, CountType, UseSafeModeFlag>& VectorOnStack<T, vectorSize, CountType, UseSafeModeFlag>::operator=(const VectorOnStack& aVectorOnStack)
 	{
		if (UseSafeModeFlag)
		{
			for (CountType i = static_cast<CountType>(0); i < aVectorOnStack.mySize; i++)
			{
				myVector[i] = aVectorOnStack[i];
				mySize++;
			}
		}
		else
		{
			std::memcpy(this, &aVectorOnStack, sizeof(aVectorOnStack));
		}
		return *(this);
 	}
	
	template<typename T, int vectorSize, typename CountType, bool UseSafeModeFlag>
	inline const T& VectorOnStack<T, vectorSize, CountType, UseSafeModeFlag>::operator[](const CountType& aIndex) const
	{
		assert((aIndex <= (mySize - 1)) && "Index out of bounds.");
		assert((aIndex >= 0) && "Index out of bounds.");
		return myVector[aIndex];
	}
	
	template<typename T, int vectorSize, typename CountType, bool UseSafeModeFlag>
	inline T& VectorOnStack<T, vectorSize, CountType, UseSafeModeFlag>::operator[](const CountType& aIndex)
	{
		assert((aIndex <= (mySize - 1)) && "Index out of bounds.");
		assert((aIndex >= 0) && "Index out of bounds.");
		return myVector[aIndex];
	}
	
	template<typename T, int vectorSize, typename CountType, bool UseSafeModeFlag>
	inline void VectorOnStack<T, vectorSize, CountType, UseSafeModeFlag>::Add(const T& aObject)
	{
		if (mySize >= vectorSize)
		{
			assert(false && "Vector is full.");
		}
		else
		{
			myVector[mySize] = aObject;
			mySize++;
		}
	}
	
	template<typename T, int vectorSize, typename CountType, bool UseSafeModeFlag>
	inline void VectorOnStack<T, vectorSize, CountType, UseSafeModeFlag>::Insert(CountType aIndex, T& aObject)
	{
		if (mySize >= vectorSize)
		{
			assert(false && "Vector is full.");
		}
		else if (aIndex > (mySize - 1) || (aIndex < 0))
		{
			assert(false && "Index out of bounds.");
		}
		else
		{
			T objectHolder = myVector[aIndex];
			myVector[aIndex] = aObject;

			for (int i = mySize + 1; i > aIndex; i--)
			{
				if (i == aIndex + 1)
				{
					myVector[i] = objectHolder;
				}
				else
				{
					myVector[i] = myVector[i - 1];
				}
			}
			mySize++;
		}
	}
	
	template<typename T, int vectorSize, typename CountType, bool UseSafeModeFlag>
	inline void VectorOnStack<T, vectorSize, CountType, UseSafeModeFlag>::DeleteCyclic(const T& aObject)
	{
		for (int i = 0; i < mySize; i++)
		{
			if (myVector[i] == aObject)
			{
				delete myVector[i];
				myVector[i] = myVector[mySize - 1];
				myVector[mySize - 1] = nullptr;
				mySize--;
			}
		}
	}
	
	template<typename T, int vectorSize, typename CountType, bool UseSafeModeFlag>
	inline void VectorOnStack<T, vectorSize, CountType, UseSafeModeFlag>::DeleteCyclicAtIndex(CountType aIndex)
	{
		assert((aIndex <= (mySize - 1)) && "Index out of bounds.");
		assert((aIndex >= 0) && "Index out of bounds."); 
		delete myVector[aIndex];
		myVector[aIndex] = myVector[mySize - 1]; 
		myVector[mySize - 1] = nullptr;
		mySize--;
	}
	
	template<typename T, int vectorSize, typename CountType, bool UseSafeModeFlag>
	inline void VectorOnStack<T, vectorSize, CountType, UseSafeModeFlag>::RemoveCyclic(const T& aObject)
	{
		for (int i = 0; i < mySize; i++)
		{
			while (myVector[i] == aObject && mySize > 0 && i < mySize)
			{
				myVector[i] = myVector[mySize - 1];
				mySize--;
			}
		}
	}
	
	template<typename T, int vectorSize, typename CountType, bool UseSafeModeFlag>
	inline void VectorOnStack<T, vectorSize, CountType, UseSafeModeFlag>::RemoveCyclicAtIndex(CountType aIndex)
	{
		assert((aIndex <= (mySize - 1)) && "Index out of bounds.");
		assert((aIndex >= 0) && "Index out of bounds.");
		myVector[aIndex] = myVector[mySize - 1];
		mySize--;
	}
	
	template<typename T, int vectorSize, typename CountType, bool UseSafeModeFlag>
	inline void VectorOnStack<T, vectorSize, CountType, UseSafeModeFlag>::Clear()
	{
		mySize = 0;
	}
	
	template<typename T, int vectorSize, typename CountType, bool UseSafeModeFlag>
	inline void VectorOnStack<T, vectorSize, CountType, UseSafeModeFlag>::DeleteAll()
	{
		for (int i = 0; i < mySize; i++)
		{
			delete myVector[i];
			myVector[i] = nullptr;
		}
		mySize = 0;
	}
	
	template<typename T, int vectorSize, typename CountType, bool UseSafeModeFlag>
	inline const T& VectorOnStack<T, vectorSize, CountType, UseSafeModeFlag>::GetLast() const
	{
		assert(mySize > 0 && "Array is empty!");
		return myVector[mySize - 1];
	}

	template<typename T, int vectorSize, typename CountType, bool UseSafeModeFlag>
	__forceinline CountType VectorOnStack<T, vectorSize, CountType, UseSafeModeFlag>::Size() const
	{
		return mySize;
	}
}
