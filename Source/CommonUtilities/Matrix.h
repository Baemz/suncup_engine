#pragma once
#include "Vector.h"
#include "Matrix33.h"
#include "Matrix44.h"

namespace CommonUtilities
{
	typedef Matrix44<float> Matrix44f;
	typedef Matrix44<int> Matrix44i;
	typedef Matrix44<unsigned int> Matrix44ui;

	typedef Matrix33<float> Matrix33f;
	typedef Matrix33<int> Matrix33i;
	typedef Matrix33<unsigned int> Matrix33ui;
}
namespace CU = CommonUtilities;