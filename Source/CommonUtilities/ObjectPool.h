#pragma once
#include "GrowingArray.h"

#ifdef max
#pragma push_macro("max")
#undef max
#define MAX_UNDEFINED
#endif // max

namespace CommonUtilities
{
	template <class ObjectType, unsigned int ObjectMaxCount>
	class ObjectPool
	{
	public:
		ObjectPool();
		~ObjectPool() = default;

		const unsigned int Add(const ObjectType& aObject);
		const unsigned int AddSpecificLowbound(const ObjectType& aObject, const unsigned int aID);
		bool Remove(const unsigned int aID);

		void Clear();

		bool IDIsUsed(const unsigned int aID) const;
		ObjectType& GetObject(const unsigned int aID);
		const ObjectType& GetObject(const unsigned int aID) const;

		const CU::GrowingArray<unsigned int, unsigned int>& GetUsedIDs() const;

	private:
		const unsigned int AddSpecific(const ObjectType& aObject, const unsigned int aID);

		template<typename Type>
		std::enable_if_t<std::is_pointer<Type>::value == true, void>
		SetNullptrIfPointerElseConstructor(Type& aType);

		template<typename Type>
		std::enable_if_t<std::is_pointer<Type>::value == false, void>
		SetNullptrIfPointerElseConstructor(Type& aType);

		ObjectType myObjects[ObjectMaxCount];
		CU::GrowingArray<unsigned int, unsigned int> myFreeIDs;
		CU::GrowingArray<unsigned int, unsigned int> myUsedIDs;
	};

	template<class ObjectType, unsigned int ObjectMaxCount>
	inline ObjectPool<ObjectType, ObjectMaxCount>::ObjectPool()
	{
		myFreeIDs.Init(ObjectMaxCount);
		myUsedIDs.Init(ObjectMaxCount);

		for (unsigned int i(0); i < ObjectMaxCount; ++i)
		{
			myFreeIDs.Add(i);
			SetNullptrIfPointerElseConstructor(myObjects[i]);
		}
	}

	template<class ObjectType, unsigned int ObjectMaxCount>
	inline const unsigned int ObjectPool<ObjectType, ObjectMaxCount>::Add(const ObjectType& aObject)
	{
		const unsigned int objectID(myFreeIDs.GetLast());

		return AddSpecific(aObject, objectID);
	}

	template<class ObjectType, unsigned int ObjectMaxCount>
	inline const unsigned int ObjectPool<ObjectType, ObjectMaxCount>::AddSpecificLowbound(const ObjectType& aObject, const unsigned int aID)
	{
		const unsigned int objectID((ObjectMaxCount - 1) - aID);

		return AddSpecific(aObject, objectID);
	}

	template<class ObjectType, unsigned int ObjectMaxCount>
	inline bool ObjectPool<ObjectType, ObjectMaxCount>::Remove(const unsigned int aID)
	{
		unsigned int usedIndex(myUsedIDs.Find(aID));
		if (usedIndex == myUsedIDs.FoundNone)
		{
			return false;
		}
		myUsedIDs.RemoveCyclicAtIndex(usedIndex);
		myFreeIDs.Add(aID);
		SetNullptrIfPointerElseConstructor(myObjects[aID]);
		return true;
	}

	template<class ObjectType, unsigned int ObjectMaxCount>
	inline void ObjectPool<ObjectType, ObjectMaxCount>::Clear()
	{
		myFreeIDs.RemoveAll();
		myUsedIDs.RemoveAll();

		for (unsigned int i(0); i < ObjectMaxCount; ++i)
		{
			myFreeIDs.Add(i);
			SetNullptrIfPointerElseConstructor(myObjects[i]);
		}
	}

	template<class ObjectType, unsigned int ObjectMaxCount>
	inline bool ObjectPool<ObjectType, ObjectMaxCount>::IDIsUsed(const unsigned int aID) const
	{
		return (myUsedIDs.Find(aID) != myUsedIDs.FoundNone);
	}

	template<class ObjectType, unsigned int ObjectMaxCount>
	inline ObjectType& ObjectPool<ObjectType, ObjectMaxCount>::GetObject(const unsigned int aID)
	{
		assert("Tried to get index out of bounds (ObjectPool)" && ((aID < ObjectMaxCount) && (aID >= 0)));
		assert("Tried to get unused object! (ObjectPool)" && (myUsedIDs.Find(aID) != myUsedIDs.FoundNone));
		return myObjects[aID];
	}

	template<class ObjectType, unsigned int ObjectMaxCount>
	inline const ObjectType& ObjectPool<ObjectType, ObjectMaxCount>::GetObject(const unsigned int aID) const
	{
		assert("Tried to get index out of bounds (ObjectPool)" && ((aID < ObjectMaxCount) && (aID >= 0)));
		assert("Tried to get unused object! (ObjectPool)" && (myUsedIDs.Find(aID) != myUsedIDs.FoundNone));
		return myObjects[aID];
	}

	template<class ObjectType, unsigned int ObjectMaxCount>
	inline const CU::GrowingArray<unsigned int, unsigned int>& ObjectPool<ObjectType, ObjectMaxCount>::GetUsedIDs() const
	{
		return myUsedIDs;
	}

	template<class ObjectType, unsigned int ObjectMaxCount>
	inline const unsigned int ObjectPool<ObjectType, ObjectMaxCount>::AddSpecific(const ObjectType& aObject, const unsigned int aID)
	{
		const unsigned int idToUse(aID);

		const unsigned int arrayIndex(myFreeIDs.Find(idToUse));

		assert("Couldn't find id in object pool" && (arrayIndex != myFreeIDs.FoundNone));

		myObjects[idToUse] = aObject;
		myUsedIDs.Add(idToUse);
		myFreeIDs.RemoveCyclicAtIndex(arrayIndex);
		return idToUse;
	}

	template<class ObjectType, unsigned int ObjectMaxCount>
	template<typename Type>
	inline std::enable_if_t<std::is_pointer<Type>::value == true, void>
		ObjectPool<ObjectType, ObjectMaxCount>::SetNullptrIfPointerElseConstructor(Type& aType)
	{
		aType = nullptr;
	}

	template<class ObjectType, unsigned int ObjectMaxCount>
	template<typename Type>
	inline std::enable_if_t<std::is_pointer<Type>::value == false, void>
		ObjectPool<ObjectType, ObjectMaxCount>::SetNullptrIfPointerElseConstructor(Type& aType)
	{
		aType = Type();
	}
}

#ifdef MAX_UNDEFINED
#undef MAX_UNDEFINED
#pragma pop_macro("max")
#endif // MAX_UNDEFINED

namespace CU = CommonUtilities;
