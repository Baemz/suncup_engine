#pragma once
#include "StringTypes.h"
#include <set>
#include <shared_mutex>

namespace CommonUtilities
{
	namespace String
	{
		class CStringHasher
		{
		public:
			inline static void HashString(const std::string& aString, HashType& aHashedStringValue);
			inline static bool Add(HashType& aHashedStringValue0, const HashType& aHashedStringValue1);
			inline static bool Add(std::string& aString, const HashType& aHashedStringValue);
			inline static bool Add(HashType& aHashedStringValue, const std::string& aString);
			inline static bool Add(std::string& aString0, const std::string& aString1);

			inline static bool HashExists(const HashType& aHashedStringValue);

			inline static bool GetString(const HashType& aHashedStringValue, std::string& aString);

		private:
			struct SStringHash
			{
				SStringHash() : myHashValue(HashTypeINVALID) {}
				explicit SStringHash(const HashType& aHashValue) : myHashValue(aHashValue) {}

				static __forceinline bool Hash(const SStringHash& aStringHash0, const SStringHash& aStringHash1)
				{
					return (aStringHash0 < aStringHash1);
				}

				inline bool operator==(const SStringHash& aStringHash) const
				{
					return (aStringHash.myHashValue == myHashValue);
				}

				inline bool operator<(const SStringHash& aStringHash) const
				{
					return (aStringHash.myHashValue < myHashValue);
				}

				std::string myString;
				HashType myHashValue;
			};

			CStringHasher() = delete;
			~CStringHasher() = delete;

			inline static void GetHashValue(const std::string& aString, SStringHash& aStringHash);
			inline static void HashStringImpl(const SStringHash& aStringHash);
			inline static void AddImpl(std::string& aString0, const std::string& aString1);

			inline static bool GetStringImpl(const HashType& aHashedStringValue, std::string& aString);

			static std::set<SStringHash, bool(*)(const SStringHash&, const SStringHash&)> ourHashedStrings;
			static std::shared_mutex ourHashedStringsMutex;

			inline static bool HashExistsImpl(const HashType& aHashedStringValue, decltype(ourHashedStrings)::iterator& aIterator);

		};

		inline void CStringHasher::HashString(const std::string& aString, HashType& aHashedStringValue)
		{
			SStringHash stringHash;
			GetHashValue(aString, stringHash);
			aHashedStringValue = stringHash.myHashValue;
			std::unique_lock<std::shared_mutex> uniqueLock(ourHashedStringsMutex);
			HashStringImpl(stringHash);
		}

		inline bool CStringHasher::Add(HashType& aHashedStringValue0, const HashType& aHashedStringValue1)
		{
			std::string string0;
			std::string string1;

			std::shared_lock<std::shared_mutex> sharedLock(ourHashedStringsMutex);
			const bool success0(GetStringImpl(aHashedStringValue0, string0));
			const bool success1(GetStringImpl(aHashedStringValue1, string1));
			sharedLock.unlock();

			if ((success0 == false) && (success1 == false))
			{
				return false;
			}

			AddImpl(string0, string1);

			SStringHash stringHash;
			GetHashValue(string0, stringHash);
			aHashedStringValue0 = stringHash.myHashValue;

			return true;
		}

		inline bool CStringHasher::Add(std::string& aString, const HashType& aHashedStringValue)
		{
			std::string string;

			std::shared_lock<std::shared_mutex> sharedLock(ourHashedStringsMutex);
			const bool success(GetStringImpl(aHashedStringValue, string));
			sharedLock.unlock();

			if (success == false)
			{
				return false;
			}

			AddImpl(aString, string);

			return true;
		}

		inline bool CStringHasher::Add(HashType& aHashedStringValue, const std::string& aString)
		{
			std::string string;

			std::shared_lock<std::shared_mutex> sharedLock(ourHashedStringsMutex);
			const bool success(GetStringImpl(aHashedStringValue, string));
			sharedLock.unlock();

			if (success == false)
			{
				return false;
			}

			AddImpl(string, aString);

			SStringHash stringHash;
			GetHashValue(string, stringHash);
			aHashedStringValue = stringHash.myHashValue;
			
			return true;
		}

		inline bool CStringHasher::Add(std::string& aString0, const std::string& aString1)
		{
			AddImpl(aString0, aString1);
			return true;
		}

		inline bool CStringHasher::HashExists(const HashType& aHashedStringValue)
		{
			decltype(ourHashedStrings)::iterator foundIterator;

			std::shared_lock<std::shared_mutex> sharedLock(ourHashedStringsMutex);
			return HashExistsImpl(aHashedStringValue, foundIterator);
		}

		inline bool CStringHasher::GetString(const HashType& aHashedStringValue, std::string& aString)
		{
			std::shared_lock<std::shared_mutex> sharedLock(ourHashedStringsMutex);
			return GetStringImpl(aHashedStringValue, aString);
		}

		inline void CStringHasher::GetHashValue(const std::string& aString, SStringHash& aStringHash)
		{
			aStringHash.myHashValue = std::hash<std::string>()(aString);

#ifndef _RETAIL
			aStringHash.myString = aString;
#endif // !_RETAIL
		}

		inline void CStringHasher::HashStringImpl(const SStringHash& aStringHash)
		{
			decltype(ourHashedStrings)::iterator foundIterator;

			if (HashExistsImpl(aStringHash.myHashValue, foundIterator) == false)
			{
				ourHashedStrings.insert(aStringHash);
			}
		}

		inline void CStringHasher::AddImpl(std::string& aString0, const std::string& aString1)
		{
			aString0 += aString0 + aString1;
			
			SStringHash stringHash;
			GetHashValue(aString0, stringHash);

			if (HashExists(stringHash.myHashValue) == false)
			{
				std::unique_lock<std::shared_mutex> uniqueLock(ourHashedStringsMutex);
				HashStringImpl(stringHash);
			}
		}

		inline bool CStringHasher::GetStringImpl(const HashType& aHashedStringValue, std::string& aString)
		{
			decltype(ourHashedStrings)::iterator foundIterator;

			const bool foundString(HashExistsImpl(aHashedStringValue, foundIterator));

			if (foundString == true)
			{
				aString = foundIterator->myString;
			}

			return foundString;
		}

		inline bool CStringHasher::HashExistsImpl(const HashType& aHashedStringValue, decltype(ourHashedStrings)::iterator& aIterator)
		{
			const SStringHash stringHash(aHashedStringValue);
			aIterator = ourHashedStrings.find(stringHash);
			if (aIterator != ourHashedStrings.end())
			{
				return true;
			}
			return false;
		}
	}
}

namespace CU = CommonUtilities;
