#pragma once
#include <string>
#include "StringTypes.h"

namespace CommonUtilities
{
	namespace String
	{
		class CHashedString
		{
			friend CHashedString operator+(const std::string& aString, const CHashedString& aHashedString);
			friend inline std::string& operator+=(std::string& aString, const CHashedString& aHashedString);

		public:
			CHashedString();
			CHashedString(const char& aChar);
			CHashedString(const char* aCharString);
			CHashedString(const std::string& aString);

			~CHashedString();

			inline bool operator==(const CHashedString& aHashedString) const;
			inline bool operator==(const char& aChar) const ;
			inline bool operator==(const char* aCharString) const;
			bool operator==(const std::string& aString) const;

			inline bool operator!=(const CHashedString& aHashedString) const;
			inline bool operator!=(const char& aChar) const;
			inline bool operator!=(const char* aCharString) const;
			bool operator!=(const std::string& aString) const;

			inline CHashedString& operator=(const CHashedString& aHashedString);
			inline CHashedString& operator=(const char& aChar);
			inline CHashedString& operator=(const char* aCharString);
			CHashedString& operator=(const std::string& aString);

			inline CHashedString& operator+=(const CHashedString& aHashedString);
			inline CHashedString& operator+=(const char& aChar);
			inline CHashedString& operator+=(const char* aCharString);
			inline CHashedString& operator+=(const std::string& aString);

			CHashedString operator+(const CHashedString& aHashedString) const;
			inline CHashedString operator+(const char& aChar) const;
			inline CHashedString operator+(const char* aCharString) const;
			CHashedString operator+(const std::string& aString) const;

			inline const std::string GetAsString() const;
			void GetAsString(std::string& aString) const;

		private:
#ifndef _RETAIL
			std::string myString;
#endif // !_RETAIL
			HashType myHashString;

		};

		inline bool CHashedString::operator==(const CHashedString& aHashedString) const
		{
			return (myHashString == aHashedString.myHashString);
		}

		inline bool CHashedString::operator==(const char& aChar) const
		{
			const char charAsArray[2]{ aChar, '\0' };
			return (*this == charAsArray);
		}

		inline bool CHashedString::operator==(const char* aCharString) const
		{
			return (*this == std::string(aCharString));
		}

		inline bool operator==(const std::string& aString, const CHashedString& aHashedString)
		{
			return (aHashedString == aString);
		}

		inline bool CHashedString::operator!=(const CHashedString& aHashedString) const
		{
			return (myHashString != aHashedString.myHashString);
		}

		inline bool CHashedString::operator!=(const char& aChar) const
		{
			const char charAsArray[2]{ aChar, '\0' };
			return (*this != charAsArray);
		}

		inline bool CHashedString::operator!=(const char* aCharString) const
		{
			return (*this != std::string(aCharString));
		}

		inline bool operator!=(const std::string& aString, const CHashedString& aHashedString)
		{
			return (aHashedString != aString);
		}

		inline CHashedString& CHashedString::operator=(const CHashedString& aHashedString)
		{
#ifndef _RETAIL
			myString = aHashedString.myString;
#endif // !_RETAIL
			myHashString = aHashedString.myHashString;
			return *this;
		}

		inline CHashedString& CHashedString::operator=(const char& aChar)
		{
			const char charAsArray[2]{ aChar, '\0' };
			*this = charAsArray;
			return *this;
		}

		inline CHashedString& CHashedString::operator=(const char* aCharString)
		{
			*this = std::string(aCharString);
			return *this;
		}

		inline CHashedString& CHashedString::operator+=(const CHashedString& aHashedString)
		{
			*this = *this + aHashedString;
			return *this;
		}

		inline CHashedString& CHashedString::operator+=(const char& aChar)
		{
			const char charAsArray[2]{ aChar, '\0' };
			*this += charAsArray;
			return *this;
		}

		inline CHashedString& CHashedString::operator+=(const char* aCharString)
		{
			*this += std::string(aCharString);
			return *this;
		}

		inline CHashedString& CHashedString::operator+=(const std::string& aString)
		{
			*this = *this + aString;
			return *this;
		}

		inline std::string& operator+=(std::string& aString, const CHashedString& aHashedString)
		{
			(aString + aHashedString).GetAsString(aString);
			return aString;
		}

		inline CHashedString CHashedString::operator+(const char& aChar) const
		{
			const char charAsArray[2]{ aChar, '\0' };
			return (*this + charAsArray);
		}

		inline CHashedString CHashedString::operator+(const char* aCharString) const
		{
			return (*this + std::string(aCharString));
		}

		CHashedString operator+(const std::string& aString, const CHashedString& aHashedString);

		inline const std::string CommonUtilities::String::CHashedString::GetAsString() const
		{
			std::string newString;
			GetAsString(newString);
			return newString;
		}
	}
}

namespace CU = CommonUtilities;
