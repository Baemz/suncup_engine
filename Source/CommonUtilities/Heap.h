#pragma once
#include <vector>
#include <cassert>

namespace CommonUtilities
{
	template <class T>
	class Heap
	{
	public:
		//Returnerar antal element i heapen
		int GetSize() const;

		//L�gger till elementet i heapen
		void Enqueue(const T& aElement);

		//Returnerar det st�rsta elementet i heapen
		const T& GetTop() const;

		//Tar bort det st�rsta elementet ur heapen och returnerar det
		T Dequeue();

	private:
		std::vector<T> myData;

		void MoveUp(const int aIndex);
		void MoveDown(const int aIndex);
	};

	template<class T>
	inline int Heap<T>::GetSize() const
	{
		return static_cast<int>(myData.size());
	}

	template<class T>
	inline void Heap<T>::Enqueue(const T& aElement)
	{
		myData.push_back(aElement);
		MoveUp(static_cast<int>(myData.size()) - 1);
		
	}

	template<class T>
	inline const T& Heap<T>::GetTop() const
	{
		return myData[0];
	}

	template<class T>
	inline T Heap<T>::Dequeue()
	{
		assert(myData.size() > 0 && "Heap is empty");

		const T dequeuedValue = myData[0];
		myData[0] = myData[static_cast<int>(myData.size() - 1)];
		myData.pop_back();
		MoveDown(0);

		return dequeuedValue;
	}
	template<class T>
	inline void Heap<T>::MoveUp(const int aIndex)
	{
		if (aIndex == 0)
		{
			return;
		}

		int parent = (aIndex - 1) / 2;
		if (myData[parent] < myData[aIndex])
		{
			T parentData = myData[parent];
			myData[parent] = myData[aIndex];
			myData[aIndex] = parentData;
			MoveUp(parent);
		}
	}
	template<class T>
	inline void Heap<T>::MoveDown(const int aIndex)
	{
		if (aIndex >= GetSize())
		{
			return;
		}

		int firstChild = aIndex * 2 + 1;
		int secondChild = aIndex * 2 + 2;

		if ((firstChild < GetSize()) && (secondChild < GetSize()))
		{
			if (myData[firstChild] < myData[secondChild])
			{
				if (myData[aIndex] < myData[secondChild])
				{
					T data = myData[aIndex];
					myData[aIndex] = myData[secondChild];
					myData[secondChild] = data;
					MoveDown(secondChild);
				}
			}
			else
			{
				if (myData[aIndex] < myData[firstChild])
				{
					T data = myData[aIndex];
					myData[aIndex] = myData[firstChild];
					myData[firstChild] = data;
					MoveDown(firstChild);
				}
			}
		}
		else if (firstChild < GetSize())
		{
			if (myData[aIndex] < myData[firstChild])
			{
				T data = myData[aIndex];
				myData[aIndex] = myData[firstChild];
				myData[firstChild] = data;
				MoveDown(firstChild);
			}
		}
	}
}