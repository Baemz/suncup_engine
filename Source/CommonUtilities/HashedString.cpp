#include "HashedString.h"
#include "StringHasher.h"
#include <assert.h>

namespace CommonUtilities
{
	namespace String
	{
		CHashedString::CHashedString()
			: myHashString(HashTypeINVALID)
		{
		}

		CHashedString::CHashedString(const char& aChar)
			: CHashedString(std::to_string(aChar))
		{
		}

		CHashedString::CHashedString(const char* aCharString)
			: CHashedString(std::string(aCharString))
		{
		}

		CHashedString::CHashedString(const std::string& aString)
#ifndef _RETAIL
			: myString(aString)
#endif // !_RETAIL
		{
			CStringHasher::HashString(aString, myHashString);
		}

		CHashedString::~CHashedString()
		{
		}

		bool CHashedString::operator==(const std::string& aString) const
		{
			HashType hashString(HashTypeINVALID);
			CStringHasher::HashString(aString, hashString);
			return (myHashString == hashString);
		}

		bool CHashedString::operator!=(const std::string& aString) const
		{
			HashType hashString(HashTypeINVALID);
			CStringHasher::HashString(aString, hashString);
			return (myHashString != hashString);
		}

		CHashedString& CHashedString::operator=(const std::string& aString)
		{
#ifndef _RETAIL
			myString = aString;
#endif // !_RETAIL
			CStringHasher::HashString(aString, myHashString);
			return *this;
		}

		CHashedString CHashedString::operator+(const CHashedString& aHashedString) const
		{
			CHashedString newHashedString(*this);
			const bool success(CStringHasher::Add(newHashedString.myHashString, aHashedString.myHashString));
			assert("CHashedString: Failed to add strings" && (success == true));
			return newHashedString;
		}

		CHashedString CHashedString::operator+(const std::string& aString) const
		{
			CHashedString newHashedString(*this);
			const bool success(CStringHasher::Add(newHashedString.myHashString, aString));
			assert("CHashedString: Failed to add strings" && (success == true));
			return newHashedString;
		}

		void CHashedString::GetAsString(std::string& aString) const
		{
			const bool foundString(CStringHasher::GetString(myHashString, aString));
			assert("Couldn't get CHashedString as string. Couldn't find the string" && (foundString == true));
		}

		CHashedString operator+(const std::string& aString, const CHashedString& aHashedString)
		{
			CHashedString newHashedString(aString);
			CStringHasher::Add(newHashedString.myHashString, aHashedString.myHashString);
			return newHashedString;
		}
	}
}
