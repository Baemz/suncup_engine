#pragma once

#define MIN(a, b)		\
(((a)<(b)) ? (a) : (b))

#define MAX(a, b)		\
(((a)>(b)) ? (a) : (b))

#define CLAMP(aValue, aMin, aMax)	\
(MIN(MAX(aValue, aMin), aMax))

#define CYCLIC_ERASE(aVector, index) \
aVector[index] = aVector[aVector.size()-1]; \
aVector.pop_back();

#define ARRAY_SIZE(aArray) (sizeof(aArray) / sizeof((aArray)[0]))
