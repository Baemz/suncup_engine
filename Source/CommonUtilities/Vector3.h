#pragma once
#include <cmath>
namespace CommonUtilities
{
	template <class T>
	class Vector4;

	template <class T>
	class Vector3
	{
	public:
		union
		{
			T x;
			T r;
		};
		union
		{
			T y;
			T g;
		};
		union
		{
			T z;
			T b;
		};

		//Creates a null-vector
		Vector3<T>();


		//Creates a vector (aX, aY, aZ)
		Vector3<T>(const T& aValue);
		Vector3<T>(const T& aX, const T& aY, const T& aZ);


		//Copy constructor (compiler generated)
		Vector3<T>(const Vector3<T>& aVector) = default;
		Vector3<T>(const Vector4<T>& aVector)
		{
			x = aVector.x;
			y = aVector.y;
			z = aVector.z;
		}


		//Assignment operator (compiler generated)
		Vector3<T>& operator=(const Vector3<T>& aVector3) = default;

		//Equals operator
		bool operator==(const Vector3<T>& aVector3) const;
		bool operator!=(const Vector3<T>& aVector3) const;

		//Negate operator
		Vector3<T> operator-() const;

		// Subtraction operator
		Vector3<T> operator-(const Vector3<T>& aVector3) const;

		//Multiplication operator
		Vector3<T> operator*(const Vector3<T>& aVector3) const;

		//Additive operator
		Vector3<T> operator+(const Vector3<T>& aVector3) const;

		//Subscript operator - returns X|Y|Z based on index % 3 - VERY NECCESARY!
		inline T& operator[](const unsigned char& aIndex);
		inline const T& operator[](const unsigned char& aIndex) const;

		//Destructor (compiler generated)
		~Vector3<T>() = default;

		//Returns the squared length of the vector
		T Length2() const;


		//Returns the length of the vector
		T Length() const;


		//Returns a normalized copy of this
		Vector3<T> GetNormalized() const;

		const T GetLargestComponent() const;


		//Normalizes the vector
		void Normalize();

		//Truncates to max length
		void Truncate(const T aMaxLength);

		const Vector3<T> Lerp(Vector3<T>& aToVector, const float t) const
		{
			return ((1 - t) * Vector3<T>(x, y, z)) + (t * aToVector);
		}

		// Gives values an absolute value
		void ConvertToDistance();

		//Returns the dot product of this and aVector
		T Dot(const Vector3<T>& aVector) const;


		//Returns the cross product of this and aVector
		Vector3<T> Cross(const Vector3<T>& aVector) const;

		T AngleFromCross() const;

		T Distance2(const Vector3<T>& aVector) const;

		static const Vector3<T> Zero;
		static const Vector3<T> Right;
		static const Vector3<T> Up;
		static const Vector3<T> Forward;

		static Vector3<T> Lerp(Vector3<T>& a, Vector3<T>& b, const float t)
		{
			return ((1 - t) * a) + (t * b);
		}

	};

	template<class T>
	const Vector3<T> Vector3<T>::Zero;
	template<class T>
	const Vector3<T> Vector3<T>::Right = CU::Vector3f(1.0f, 0.0f, 0.0f);
	template<class T>
	const Vector3<T> Vector3<T>::Up = CU::Vector3f(0.0f, 1.0f, 0.0f);
	template<class T>
	const Vector3<T> Vector3<T>::Forward = CU::Vector3f(0.0f, 0.0f, 1.0f);

	template<class T> inline Vector3<T>::Vector3()
	{
		x = static_cast<T>(0);
		y = static_cast<T>(0);
		z = static_cast<T>(0);
	}

	template<class T>
	inline Vector3<T>::Vector3(const T & aValue)
	{
		x = aValue;
		y = aValue;
		z = aValue;
	}

	template<class T> inline Vector3<T>::Vector3(const T& aX, const T& aY, const T& aZ)
	{
		x = aX;
		y = aY;
		z = aZ;
	}

	template<class T>
	inline bool Vector3<T>::operator==(const Vector3<T>& aVector3) const
	{
		return x == aVector3.x && y == aVector3.y && z == aVector3.z;
	}

	template<class T>
	inline bool Vector3<T>::operator!=(const Vector3<T>& aVector3) const
	{
		return !((*this) == aVector3);
	}

	template<class T>
	inline Vector3<T> Vector3<T>::operator-(const Vector3<T>& aVector3) const
	{
		return Vector3<T>(x - aVector3.x, y - aVector3.y, z - aVector3.z);
	}

	template<class T>
	inline Vector3<T> Vector3<T>::operator*(const Vector3<T>& aVector3) const
	{
		return Vector3<T>(x * aVector3.x, y * aVector3.y, z * aVector3.z);
	}

	template<class T>
	inline Vector3<T> Vector3<T>::operator+(const Vector3<T>& aVector3) const
	{
		return Vector3<T>(x + aVector3.x, y + aVector3.y, z + aVector3.z);
	}

	template<class T>
	inline Vector3<T> Vector3<T>::operator-() const
	{
		return Vector3<T>(-x, -y, -z);
	}

	template<class T> inline T Vector3<T>::Length2() const
	{
		return ((x*x) + (y*y) + (z*z));
	}

	template<class T> inline T Vector3<T>::Length() const
	{
		const T length2(Length2());
		if (length2 == static_cast<T>(0))
		{
			return static_cast<T>(0);
		}
		return sqrt(length2);
	}

	template<class T> inline const T Vector3<T>::GetLargestComponent() const
	{
		T val = (x > y) ? x : y;
		val = (val > z) ? val : z;
		return val;
	}

	template<class T> inline Vector3<T> Vector3<T>::GetNormalized() const
	{
		Vector3<T> newVector(*this);
		newVector.Normalize();
		return newVector;
	}

	template<class T> inline void Vector3<T>::Normalize()
	{
		T length = this->Length2();
		if (length == static_cast<T>(0))
		{
			return;
		}
		length = sqrt(length);
		x /= length;
		y /= length;
		z /= length;
	}

	template<class T>
	inline void Vector3<T>::Truncate(const T aMaxLength)
	{
		T length(Length());
		if (length < static_cast<T>(1e-3)) return;

		T scale(aMaxLength / length);
		scale = scale < static_cast<T>(1) ? scale : static_cast<T>(1);
		*this *= scale;
	}

	template<class T>
	inline void Vector3<T>::ConvertToDistance()
	{
		x = fabs(x);
		y = fabs(y);
		z = fabs(z);
	}

	template<class T> inline T Vector3<T>::Dot(const Vector3<T>& aVector) const
	{
		return ((x*aVector.x) + (y*aVector.y) + (z*aVector.z));
	}

	template<class T> inline Vector3<T> Vector3<T>::Cross(const Vector3<T>& aVector) const
	{
		return Vector3<T>(((y*aVector.z) - (z*aVector.y)), ((z*aVector.x) - (x*aVector.z)), ((x*aVector.y) - (y*aVector.x)));
	}

	template<class T>
	inline T Vector3<T>::AngleFromCross() const
	{
		T crossLength = Length();
		if (crossLength > 1.0f)
		{
			crossLength = 1;
		}
		else if (crossLength < 0.0f)
		{
			crossLength = 0;
		}
		return asin(crossLength);
	}

	template<class T>
	inline T Vector3<T>::Distance2(const Vector3<T>& aVector) const
	{
		return (aVector - (*this)).Length2();
	}

	template<class T>
	inline T& Vector3<T>::operator[](const unsigned char& aIndex)
	{
		return (aIndex % 3 == 0) ? x : (aIndex % 3 == 1) ? y : z;
	}

	template<class T>
	inline const T & Vector3<T>::operator[](const unsigned char & aIndex) const
	{
		return (aIndex % 3 == 0) ? x : (aIndex % 3 == 1) ? y : z;
	}

	//Returns the vector sum of aVector0 and aVector1
	template <class T> Vector3<T> operator+(const Vector3<T>& aVector0, const Vector3<T>& aVector1) 
	{
		return Vector3<T>((aVector0.x + aVector1.x), (aVector0.y + aVector1.y), (aVector0.z + aVector1.z));
	}

	//Returns the vector difference of aVector0 and aVector1
	template <class T> Vector3<T> operator-(const Vector3<T>& aVector0, const Vector3<T>& aVector1) 
	{
		return Vector3<T>((aVector0.x - aVector1.x), (aVector0.y - aVector1.y), (aVector0.z - aVector1.z));
	}

	//Returns the vector aVector multiplied by the scalar aScalar
	template <class T> Vector3<T> operator*(const Vector3<T>& aVector, const T& aScalar) 
	{
		return Vector3<T>((aVector.x * aScalar), (aVector.y * aScalar), (aVector.z * aScalar));
	}

	//Returns the vector aVector multiplied by the scalar aScalar
	template <class T> Vector3<T> operator*(const T& aScalar, const Vector3<T>& aVector) 
	{
		return Vector3<T>((aVector.x * aScalar), (aVector.y * aScalar), (aVector.z * aScalar));
	}

	//Returns the vector aVector divided by the scalar aScalar (equivalent to aVector multiplied by 1 / aScalar)
	template <class T> Vector3<T> operator/(const Vector3<T>& aVector, const T& aScalar) 
	{
		T scale = (1 / aScalar);
		return Vector3<T>((aVector.x * scale), (aVector.y * scale), (aVector.z * scale));
	}

	template <class T> Vector3<T> operator/(const T& aScalar, const Vector3<T>& aVector)
	{
		return Vector3<T>((aScalar * (1 / aVector.x))
			, (aScalar * (1 / aVector.y))
			, (aScalar * (1 / aVector.z)));
	}

	template <class T> Vector3<T> operator/(const Vector3<T>& aVector, const Vector3<T>& aVector2)
	{
		return Vector3<T>((aVector.x / aVector2.x), (aVector.y / aVector2.y), (aVector.z / aVector2.z));
	}

	//Equivalent to setting aVector0 to (aVector0 + aVector1)
	template <class T> void operator+=(Vector3<T>& aVector0, const Vector3<T>& aVector1) 
	{
		aVector0.x = (aVector0.x + aVector1.x);
		aVector0.y = (aVector0.y + aVector1.y);
		aVector0.z = (aVector0.z + aVector1.z);
	}


	//Equivalent to setting aVector0 to (aVector0 - aVector1)
	template <class T> void operator-=(Vector3<T>& aVector0, const Vector3<T>& aVector1) 
	{
		aVector0.x = (aVector0.x - aVector1.x);
		aVector0.y = (aVector0.y - aVector1.y);
		aVector0.z = (aVector0.z - aVector1.z);
	}


	//Equivalent to setting aVector to (aVector * aScalar)
	template <class T> void operator*=(Vector3<T>& aVector, const T& aScalar) 
	{
		aVector.x = (aVector.x * aScalar);
		aVector.y = (aVector.y * aScalar);
		aVector.z = (aVector.z * aScalar);
	}


	//Equivalent to setting aVector to (aVector / aScalar)
	template <class T> void operator/=(Vector3<T>& aVector, const T& aScalar) 
	{
		T scale = (1 / aScalar);
		aVector.x = (aVector.x * scale);
		aVector.y = (aVector.y * scale);
		aVector.z = (aVector.z * scale);
	}

	typedef Vector3<float> Vector3f;
	typedef Vector3<unsigned int> Vector3ui;
	typedef Vector3<int> Vector3i;
}

namespace CU = CommonUtilities;