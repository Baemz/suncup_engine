#pragma once
#include <array>
#include "Keys.h"

struct SPoint
{
	long x;
	long y;
};

#ifndef _WINDEF_
struct HWND__;
typedef HWND__* HWND;
#endif

namespace CommonUtilities
{
	class InputManager
	{
	public:
		InputManager(bool aUseRawMouseInput);
		~InputManager();

		// You MUST run Update() AFTER all of your desired input.
		void Update();
		void ResetInput();

		void CursorToScreen(void* aMsg);

		void SetKeyIsDown(const unsigned char aKey);
		void SetKeyIsUp(const unsigned char aKey);

		bool WasKeyJustPressed(const unsigned char aKey) const;
		bool IsKeyDown(const unsigned char aKey) const;
		
		bool WasKeyJustReleased(const unsigned char aKey) const;
		bool IsKeyUp(const unsigned char aKey) const;

		int GetMousePositionX() const;
		int GetMousePositionY() const;
		void SetMousePosition(const int aPositionX, const int aPositionY);

		int GetMouseXMovementSinceLastFrame() const;
		int GetMouseYMovementSinceLastFrame() const;

		long GetRawMouseXMovementSinceLastFrame() const;
		long GetRawMouseYMovementSinceLastFrame() const;

		void SetLeftMouseButtonDown();
		void SetLeftMouseButtonUp();

		void SetRightMouseButtonDown();
		void SetRightMouseButtonUp();

		void SetMiddleMouseButtonDown();
		void SetMiddleMouseButtonUp();

		void SetScrollMovement(const long aScrollMovement);
		long GetScrollMovementSinceLastFrame() const;
		long GetScrollPosition() const;

		bool IsMouseButtonDown(const unsigned int aButton) const;
		bool WasMouseButtonJustPressed(const unsigned int aButton) const;
		bool WasMouseButtonJustReleased(const unsigned int aButton) const;

		bool LostFocus() const { return myLostFocus; }
		
		void HandleInput(unsigned int message, unsigned long long wParam, long long lParam);

	private: 
		long myCurrentScrollMovement;
		long myScrollPosition;
		long myScrollMovementSinceLastFrame;
		long myRawMouseDeltaX;
		long myRawMouseDeltaY;
		SPoint myCurrentMousePosition;
		SPoint myPreviousMousePosition;
		SPoint myMouseMovementSinceLastFrame;
		HWND myActiveWindow;
		bool myPreviousFrameKeyStates[256];
		bool myCurrentFrameKeyStates[256];
		bool myCurrentFrameMouseButtonStates[3];
		bool myPreviousFrameMouseButtonStates[3];
		bool myLostFocus;
	};
}

