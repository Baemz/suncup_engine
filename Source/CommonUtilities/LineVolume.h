#pragma once
#include "Vector2.h"
#include "Line.h"
#include <vector>

namespace CommonUtilities
{
	template<typename T>
	class LineVolume
	{
	public:
		LineVolume();
		LineVolume(const std::vector<Line<T>>& aLineList);
		~LineVolume();

		void AddLine(const Line<T>& aLine);

		bool Inside(const Vector2<T>& aPosition);
		bool IntersectsWithCircle(const Vector2<T>& aPosition, const float aRadius) const;

		void ClearLines();

	private:
		std::vector<Line<T>> myLineList;
	};
	
	template<typename T>
	inline LineVolume<T>::LineVolume()
	{
	}
	
	template<typename T>
	inline LineVolume<T>::LineVolume(const std::vector<Line<T>>& aLineList)
	{
		myLineList = aLineList;
	}

	template<typename T>
	inline LineVolume<T>::~LineVolume()
	{
	}
	
	template<typename T>
	inline void LineVolume<T>::AddLine(const Line<T>& aLine)
	{
		myLineList.push_back(aLine);
	}

	template<typename T>
	inline bool LineVolume<T>::Inside(const Vector2<T>& aPosition)
	{
		for (int i = 0; i < static_cast<int>(myLineList.size()); i++)
		{
			if (!myLineList[i].Inside(aPosition))
			{
				return false;
			}
		}
		return true;
	}

	template<typename T>
	inline bool LineVolume<T>::IntersectsWithCircle(const Vector2<T>& aPosition, const float aRadius) const
	{
		for (int i = 0; i < static_cast<int>(myLineList.size()); i++)
		{
			if (!myLineList[i].IntersectsWithCircle(aPosition, aRadius))
			{
				return false;
			}
		}

		return true;
	}

	template<typename T>
	inline void LineVolume<T>::ClearLines()
	{
		myLineList.clear();
	}
}