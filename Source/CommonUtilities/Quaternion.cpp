#include "stdafx.h"
#include "Quaternion.h"
#include "Lerp.h"

namespace CommonUtilities
{
	Quaternion Quaternion::Identity = Quaternion();

	Quaternion::Quaternion(float aRotationX, float aRotationY, float aRotationZ, float aRotationAmount, bool aNormalize)
		: myRotation(aRotationX, aRotationY, aRotationZ), myRotationAmount(aRotationAmount)
	{
		if (aNormalize == true)
		{
			Normalize();
		}
	}

	Quaternion::Quaternion(const Vector3f &aRotation, float aRotationAmount /*= 1.0f*/, bool aNormalize)
		:Quaternion(aRotation.x, aRotation.y, aRotation.z, aRotationAmount, aNormalize)
	{

	}

	Quaternion::Quaternion(const Vector4f & aRotationAndAmount /*= Vector4f(0.0f, 0.0f, 0.0f, 1.0f)*/, bool aNormalize)
		: Quaternion(aRotationAndAmount.x, aRotationAndAmount.y, aRotationAndAmount.z, aRotationAndAmount.w, aNormalize)
	{

	}

	Quaternion::Quaternion(const Vector3f & aEulerAngle, bool aFromEuler)
	{
		if (aFromEuler)
		{
			float yaw = aEulerAngle.x;
			float pitch = aEulerAngle.y;
			float roll = aEulerAngle.z;

			float rollOver2 = roll * 0.5f;
			float sinRollOver2 = (float)std::sin((float)rollOver2);
			float cosRollOver2 = (float)std::cos((float)rollOver2);
			float pitchOver2 = pitch * 0.5f;
			float sinPitchOver2 = (float)std::sin((float)pitchOver2);
			float cosPitchOver2 = (float)std::cos((float)pitchOver2);
			float yawOver2 = yaw * 0.5f;
			float sinYawOver2 = (float)std::sin((float)yawOver2);
			float cosYawOver2 = (float)std::cos((float)yawOver2);

			myRotation.x = cosYawOver2 * cosPitchOver2 * cosRollOver2 + sinYawOver2 * sinPitchOver2 * sinRollOver2;
			myRotation.y = cosYawOver2 * cosPitchOver2 * sinRollOver2 - sinYawOver2 * sinPitchOver2 * cosRollOver2;
			myRotation.z = cosYawOver2 * sinPitchOver2 * cosRollOver2 + sinYawOver2 * cosPitchOver2 * sinRollOver2;
			myRotationAmount = sinYawOver2 * cosPitchOver2 * cosRollOver2 - cosYawOver2 * sinPitchOver2 * sinRollOver2;
		}
		else
		{
			*this = Quaternion(aEulerAngle, 1.0f, true);
		}
	}

	Quaternion::Quaternion(const Quaternion & aQuaternion)
	{
		myRotation = aQuaternion.myRotation;
		myRotationAmount = aQuaternion.myRotationAmount;
	}

	Quaternion::Quaternion(Matrix44f pm)
	{
		/*myRotationAmount =	( pm[0] + pm[5] + pm[10] + 1.0f) / 4.f;
		myRotation.x =		( pm[0] - pm[5] - pm[10] + 1.0f) / 4.f;
		myRotation.y =		(-pm[0] + pm[5] - pm[10] + 1.0f) / 4.f;
		myRotation.z =		(-pm[0] - pm[5] + pm[10] + 1.0f) / 4.f;

		if (myRotationAmount < 0.f)		{ myRotationAmount = 0.f; }
		if (myRotation.x < 0.f)			{ myRotation.x = 0.f; }
		if (myRotation.y < 0.f)			{ myRotation.y = 0.f; }
		if (myRotation.z < 0.f)			{ myRotation.z = 0.f; }

		myRotationAmount =	sqrt(myRotationAmount);
		myRotation.x =		sqrt(myRotation.x);
		myRotation.y =		sqrt(myRotation.y);
		myRotation.z =		sqrt(myRotation.z);

		if (myRotationAmount >= myRotation.x)
		{
		}*/

		float wsqrdminus1 = pm[0] + pm[5] + pm[10];
		float xsqrdminus1 = pm[0] - pm[5] - pm[10];
		float ysqrdminus1 = pm[5] - pm[0] - pm[10];
		float zsqrdminus1 = pm[10] - pm[0] - pm[5];
		int biggestindex = 0;
		float biggest = wsqrdminus1;
		if (xsqrdminus1 > biggest) {
			biggest = xsqrdminus1;
			biggestindex = 1;
		}
		if (ysqrdminus1 > biggest) {
			biggest = ysqrdminus1;
			biggestindex = 2;
		}
		if (zsqrdminus1 > biggest) {
			biggest = zsqrdminus1;
			biggestindex = 3;
		}
		float biggestval = sqrtf(biggest + 1.0f)*.5f;
		float mult = .25f / biggestval;
		switch (biggestindex) {
		case 0:
			myRotationAmount = biggestval;
			myRotation.x = (pm[6] - pm[9]) * mult;
			myRotation.y = (pm[8] - pm[2]) * mult;
			myRotation.z = (pm[1] - pm[4]) * mult;
			break;
		case 1:
			myRotation.x = biggestval;
			myRotationAmount = (pm[6] - pm[9]) * mult;
			myRotation.y = (pm[1] + pm[4]) * mult;
			myRotation.z = (pm[8] + pm[2]) * mult;
			break;
		case 2:
			myRotation.y = biggestval;
			myRotationAmount = (pm[8] - pm[2]) * mult;
			myRotation.x = (pm[1] + pm[4]) * mult;
			myRotation.z = (pm[6] + pm[9]) * mult;
			break;
		case 3:
			myRotation.z = biggestval;
			myRotationAmount = (pm[2] - pm[4]) * mult;
			myRotation.x = (pm[8] + pm[2]) * mult;
			myRotation.y = (pm[6] + pm[9]) * mult;
			break;
		};
	}

	void Quaternion::RotateTowardsPoint(const Vector3f & aCurrentPosition, const Vector3f & aTargetPosition)
	{
		bool angleIsPositive = false;
		const Vector3f CurrentDirection = GetForward().GetNormalized();
		const float sign = GetRight().GetNormalized().Dot(aTargetPosition - aCurrentPosition); //Sign is positive to the right, negative to the left
		Vector3f rotAxis;

		if (sign > 0.0001f) // right
		{
			rotAxis = CurrentDirection.Cross(aTargetPosition - aCurrentPosition);
			angleIsPositive = true;
		}
		else if (sign < 0.0001f) //left
		{
			rotAxis = (aTargetPosition - aCurrentPosition).Cross(CurrentDirection);
			angleIsPositive = false;
		}
		else
		{
			return;
		}

		if (isnan(rotAxis.z) == true || isnan(rotAxis.y) == true || isnan(rotAxis.x) == true || rotAxis == Vector3f::Zero)
		{
			return;
		}

		float angle = rotAxis.AngleFromCross();

		if (isnan(angle) == true || angle == 0.f)
		{
			return;
		}

		if (angleIsPositive == false)
		{
			angle *= -1.f;
		}

		RotateAlongAxis(rotAxis, angle);
	}

	void Quaternion::Normalize()
	{
		float magnitude = sqrtf(myRotation.Length2() + powf(myRotationAmount, 2));

		if (magnitude == 0.f)
		{
			return;
		}

		myRotation.x /= magnitude;
		myRotation.y /= magnitude;
		myRotation.z /= magnitude;
		myRotationAmount /= magnitude;
	}



	void Quaternion::RotateAlongAxis(const Vector3f &aAxis, float aRotationAmount)
	{
		Quaternion localRotation;
		localRotation.myRotation.x = aAxis.x * sinf(aRotationAmount / 2.f);
		localRotation.myRotation.y = aAxis.y * sinf(aRotationAmount / 2.f);
		localRotation.myRotation.z = aAxis.z * sinf(aRotationAmount / 2.f);
		localRotation.myRotationAmount = cosf(aRotationAmount / 2.f);

		*this = *this * localRotation;
	}

	void Quaternion::RotateWorld(const Vector3f& aRotationAmount)
	{
		Quaternion q;
		q.RotateWorldZ(aRotationAmount.z);
		*this = q * *this;

		q = Quaternion();
		q.RotateWorldX(aRotationAmount.x);
		*this = q * *this;
		
		q = Quaternion();
		q.RotateWorldY(aRotationAmount.y);
		*this = q * *this;
		
	}

	void Quaternion::RotateWorldX(float aRotationAmount)
	{
		RotateAlongAxis(Vector3f(1.0f, 0.0f, 0.0f), aRotationAmount);
	}

	void Quaternion::RotateWorldY(float aRotationAmount)
	{
		RotateAlongAxis(Vector3f(0.0f, 1.0f, 0.0f), aRotationAmount);
	}

	void Quaternion::RotateWorldZ(float aRotationAmount)
	{
		RotateAlongAxis(Vector3f(0.0f, 0.0f, 1.0f), aRotationAmount);
	}

	Quaternion Quaternion::CreateFromAxisAngle(const Vector3f &aAxis, const float anAngle)
	{
		float halfAngle = anAngle * .5f;
		float s = sinf(halfAngle);
		Quaternion q;
		q.myRotation.x = aAxis.x * s;
		q.myRotation.y = aAxis.y * s;
		q.myRotation.z = aAxis.z * s;
		q.myRotationAmount = cosf(halfAngle);
		return q;
	}

	void Quaternion::RotateLocal(const Vector3f& aRotationAmount)
	{
		RotateLocalX(aRotationAmount.x);
		RotateLocalY(aRotationAmount.y);
		RotateLocalZ(aRotationAmount.z);
	}

	void Quaternion::RotateLocalX(float aRotationAmount)
	{
		RotateAlongAxis(*this * GetRight(), aRotationAmount);
	}

	void Quaternion::RotateLocalY(float aRotationAmount)
	{
		RotateAlongAxis(*this * GetUp(), aRotationAmount);
	}

	void Quaternion::RotateLocalZ(float aRotationAmount)
	{
		RotateAlongAxis(*this * GetForward(), aRotationAmount);
	}


	Vector3f Quaternion::ToEuler()  const
	{
		float sp = -2.0f * (myRotation.y * myRotation.z - myRotationAmount*myRotation.x);// Extract sinf(pitch)

		float pitch, roll, yaw;

		if (fabs(sp) > 0.9999f) {// Check for Gimbel lock, giving slight tolerance for numerical imprecision
			pitch = (3.141592f / 2.f) * sp;// Looking straight up or down
			yaw = atan2(-myRotation.x*myRotation.z + myRotationAmount*myRotation.y, 0.5f - myRotation.y*myRotation.y - myRotation.z*myRotation.z);// Compute heading, slam roll to zero
			roll = 0.0f;
		}
		else {// Compute angles.  We don't have to use the "safe" asin function because we already checked for range errors when checking for Gimbel lock
			pitch = asinf(sp);
			yaw = atan2(myRotation.x*myRotation.z + myRotationAmount * myRotation.y, 0.5f - myRotation.x*myRotation.x - myRotation.y*myRotation.y);
			roll = atan2(myRotation.x*myRotation.y + myRotationAmount*myRotation.z, 0.5f - myRotation.x*myRotation.x - myRotation.z*myRotation.z);
		}

		return Vector3f(pitch, yaw, roll);
	}

	Vector3f Quaternion::GetRight() const
	{
		return *this * Vector3f(1.0f, 0.0f, 0.0f);
	}

	Vector3f Quaternion::GetLeft() const
	{
		return -GetRight();
	}

	Vector3f Quaternion::operator*(const Vector3f& aRight) const
	{
		Vector3f quaternionAsVector(myRotation.x, myRotation.y, myRotation.z);
		float scalar = myRotationAmount;

		Vector3f firstTest = quaternionAsVector * 2.0f * quaternionAsVector.Dot(aRight);
		Vector3f secondTest = aRight * (scalar*scalar - quaternionAsVector.Dot(quaternionAsVector));
		Vector3f thirdTest = quaternionAsVector.Cross(aRight) * scalar * 2.0f;

		return Vector3f(
			firstTest
			+ secondTest
			+ thirdTest);
	}

	Vector3f Quaternion::GetUp() const
	{
		return *this * Vector3f(0.0f, 1.0f, 0.0f);
	}

	Vector3f Quaternion::GetDown() const
	{
		return -GetUp();
	}

	Vector3f Quaternion::GetForward() const
	{
		return *this * Vector3f(0.0f, 0.0f, 1.0f);
	}

	Vector3f Quaternion::GetBackward() const
	{
		return -GetForward();
	}

	//	Roll = Z Rotation
	float Quaternion::GetRoll() const
	{
		return ToEuler().y;
		//return atan2f(2 * myRotation.y * myRotationAmount - 2 * myRotation.x * myRotation.z, 1 - 2 * myRotation.y * myRotation.y - 2 * myRotation.z * myRotation.z);
	}

	//	Pitch = X Rotation
	float Quaternion::GetPitch() const
	{
		return ToEuler().x;
	}

	//	Yaw = Y Rotation
	float Quaternion::GetYaw() const
	{
		return ToEuler().z;
	}

	Quaternion Quaternion::LookAt(const Vector3f &aStartPosition, const Vector3f &aEndPosition, const Vector3f &aUp)
	{
		Vector3f forwardVector = (aEndPosition - aStartPosition).GetNormalized();

		float dot = Vector3f(0.0f, 0.0f, 1.0f).Dot(forwardVector);

		if (std::abs(dot - (-1.0f)) < 0.000001f)
		{
			return Quaternion(aUp, 3.1415926535897932f);
		}
		if (std::abs(dot - (1.0f)) < 0.000001f)
		{
			return Quaternion();
		}

		float rotAngle = (float)std::acosf(dot);
		Vector3f rotAxis = Vector3f(0.0f, 0.0f, 1.0f).Cross(forwardVector);
		rotAxis.Normalize();

		Quaternion quaternion;
		quaternion.RotateAlongAxis(rotAxis, rotAngle);
		return CreateFromAxisAngle(rotAxis, rotAngle);
	}

	Quaternion Quaternion::FromToRotation(const Vector3f & aFrom, const Vector3f & aTo)
	{
		//Standard trigonometry
			//float cosTheta(aFrom.GetNormalized().Dot(aTo.GetNormalized()));
			//float angle(std::acos(cosTheta));
			//CU::Vector3f w(aFrom.Cross(aTo).GetNormalized());
			//return CU::Quaternion::CreateFromAxisAngle(w, angle);

		//Half-angle formulas
			//float cosTheta(aFrom.GetNormalized().Dot(aTo.GetNormalized()));
			//float halfCos(sqrt(0.5f * (1.0f + cosTheta)));
			//float halfSin(sqrt(0.5f * (1.0f - cosTheta)));
			//CU::Vector3f w(aFrom.Cross(aTo).GetNormalized());
			//return CU::Quaternion(halfSin * w.x, halfSin * w.y, halfSin * w.z, halfCos);

		//Less square roots
		float normUNormV(sqrt(aFrom.Length2() * aTo.Length2()));
		float cosTheta(aFrom.Dot(aTo) / normUNormV);
		float halfCos(sqrt(0.5f * (1.0f + cosTheta)));
		CU::Vector3f w(aFrom.Cross(aTo) / (normUNormV * 2.0f * halfCos));
		return CU::Quaternion(w.x, w.y, w.z, halfCos);
	}

	bool Quaternion::operator==(const Quaternion& aRight) const
	{
		return aRight.myRotation == myRotation && aRight.myRotationAmount == myRotationAmount;
	}

	bool Quaternion::operator!=(const Quaternion& aRight) const
	{
		return !(aRight == *this);
	}

	Quaternion Quaternion::operator*(const Quaternion& aRight) const
	{
		Quaternion returnValue;
		returnValue.myRotation.x = myRotationAmount * aRight.myRotation.x + myRotation.x * aRight.myRotationAmount + myRotation.y * aRight.myRotation.z - myRotation.z * aRight.myRotation.y;

		returnValue.myRotation.y = myRotationAmount * aRight.myRotation.y - myRotation.x * aRight.myRotation.z + myRotation.y * aRight.myRotationAmount + myRotation.z * aRight.myRotation.x;

		returnValue.myRotation.z = myRotationAmount * aRight.myRotation.z + myRotation.x * aRight.myRotation.y - myRotation.y * aRight.myRotation.x + myRotation.z * aRight.myRotationAmount;

		returnValue.myRotationAmount = myRotationAmount * aRight.myRotationAmount - myRotation.x * aRight.myRotation.x - myRotation.y * aRight.myRotation.y - myRotation.z * aRight.myRotation.z;

		return returnValue;
	}

	Quaternion& Quaternion::operator*=(const Quaternion& aRight)
	{
		*this = *this * aRight;
		return *this;
	}

	Quaternion Quaternion::operator/(const float aScale) const
	{
		return Quaternion(myRotation / aScale, myRotationAmount / aScale);
	}

	Quaternion Quaternion::operator*(const float aScale) const
	{
		return Quaternion(myRotation * aScale, myRotationAmount * aScale, false);
	}

	Quaternion Quaternion::operator+(const Quaternion& aRight) const
	{
		return Quaternion(myRotation + aRight.myRotation, myRotationAmount + aRight.myRotationAmount);
	}

	Quaternion Quaternion::operator-(const Quaternion& aRight) const
	{
		Quaternion temp;
		temp.myRotation = myRotation - aRight.myRotation;
		temp.myRotationAmount = myRotationAmount - aRight.myRotationAmount;
		return temp;
		//return Quaternion(myRotation - aRight.myRotation, myRotationAmount - aRight.myRotationAmount);
	}

	Quaternion& Quaternion::operator*=(const float aScale)
	{
		*this = *this * aScale;
		return *this;
	}

	Quaternion& Quaternion::operator+=(const Quaternion& aRight)
	{
		*this = *this + aRight;
		return *this;
	}

	Quaternion& Quaternion::operator-=(const Quaternion& aRight)
	{
		*this = *this - aRight;
		return *this;
	}

	Quaternion& Quaternion::operator/=(const float aScale)
	{
		*this = *this / aScale;
		return *this;
	}

	Matrix44f Quaternion::GenerateMatrix() const
	{
		float X = myRotation.x;
		float Y = myRotation.y;
		float Z = myRotation.z;
		float W = myRotationAmount;
		float xx = X * X;
		float xy = X * Y;
		float xz = X * Z;
		float xw = X * W;
		float yy = Y * Y;
		float yz = Y * Z;
		float yw = Y * W;
		float zz = Z * Z;
		float zw = Z * W;

		Matrix44f returnValue;
		returnValue[0] = 1 - 2 * (yy + zz);
		returnValue[1] = 2 * (xy - zw);
		returnValue[2] = 2 * (xz + yw);

		returnValue[4] = 2 * (xy + zw);
		returnValue[5] = 1 - 2 * (xx + zz);
		returnValue[6] = 2 * (yz - xw);

		returnValue[8] = 2 * (xz - yw);
		returnValue[9] = 2 * (yz + xw);
		returnValue[10] = 1 - 2 * (xx + yy);

		returnValue[3] = returnValue[7] = returnValue[11] = returnValue[12] = returnValue[13] = returnValue[14] = 0;
		returnValue[15] = 1;

		returnValue = Matrix44f::Transpose(returnValue);
		return returnValue;
	}

	Quaternion Quaternion::GetInverse(const Quaternion &aQuaternion)
	{
		Quaternion temp;
		temp.myRotationAmount = aQuaternion.myRotationAmount;
		temp.myRotation = aQuaternion.myRotation * -1.f;

		return temp;
	}

	float Quaternion::Dot(const Quaternion &aFirstQuaternion, const Quaternion &aSecondQuaternion)
	{
		return aFirstQuaternion.myRotation.Dot(aSecondQuaternion.myRotation) + aFirstQuaternion.myRotationAmount * aSecondQuaternion.myRotationAmount;
	}

	float Quaternion::Angle(const Quaternion & aFirstQuaternion, const Quaternion & aSecondQuaternion)
	{
		float f = Quaternion::Dot(aFirstQuaternion, aSecondQuaternion);
		return std::acosf(std::fminf(std::abs(f), 1.0f)) * 2.0f;
	}

	Quaternion Quaternion::SlerpNoInvert(const Quaternion& q1, const Quaternion& q2, float t)
	{
		float dot = Quaternion::Dot(q1, q2);

		if (dot > -0.95f && dot < 0.95f)
		{
			float angle = acosf(dot);
			return (q1*sinf(angle*(1 - t)) + q2*sinf(angle*t)) / sinf(angle);
		}
		else  // if the angle is small, use linear interpolation								
			return Lerp(q1, q2, t);
	}

	Quaternion Quaternion::Slerp(const Quaternion &aFirstQuaternion, const Quaternion &aSecondQuaternion, const float aProgress)
	{
		/*Quaternion first = aFirstQuaternion;
		Quaternion second = aSecondQuaternion;
		Quaternion third;

		float cosTheta = Quaternion::Dot(aFirstQuaternion, aSecondQuaternion);

		if (cosTheta < 0.f)
		{
			cosTheta = -cosTheta;
			second = second * -1.f;
		}
		if (cosTheta > 0.9995f)
		{
			return Lerp(first, second, aProgress);
		}


		third.myRotationAmount = second.myRotationAmount - first.myRotationAmount * cosTheta;
		third.myRotation = second.myRotation - first.myRotation * cosTheta;

		float sine = static_cast<float>(sqrtf(Quaternion::Dot(third, third)));

		float angle = atan2f(sine, cosTheta) * aProgress;
		float s = sinf(angle);
		float c = cosf(angle);

		Quaternion returnVal;

		returnVal.myRotationAmount = second.myRotationAmount * c + third.myRotationAmount * s / sine;
		returnVal.myRotation.x = second.myRotation.x * c + third.myRotation.x * s / sine;
		returnVal.myRotation.y = second.myRotation.y * c + third.myRotation.y * s / sine;
		returnVal.myRotation.z = second.myRotation.z * c + third.myRotation.z * s / sine;

		return returnVal;*/

		// If cosTheta < 0, the interpolation will take the long way around the sphere. 
		// To fix this, one quat must be negated.
		Quaternion qz = aSecondQuaternion;
		float cosTheta = Quaternion::Dot(aFirstQuaternion, aSecondQuaternion);

		if (cosTheta < 0.f)
		{
			qz = qz * -1.f;
			cosTheta = -cosTheta;
		}

		static const float dotThreshold = 0.95f;
		static const float dotThreshold2 = 1.05f;
		// Perform a linear interpolation when cosTheta is close to 1 to avoid side effect of sin(angle) becoming a zero denominator
		if (cosTheta > dotThreshold /* && cosTheta < dotThreshold2*/)
		{
			// Linear interpolation
			return Lerp(aFirstQuaternion, qz, aProgress);
		}
		else
		{
			// Essential Mathematics, page 467
			float angle = acos(cosTheta);
			return (aFirstQuaternion * sin((1.f - aProgress) * angle) + qz * sin(aProgress * angle)) / sin(angle);
		}
	}
	Quaternion Quaternion::RotateTowards(const Quaternion & aFirstQuaternion, const Quaternion & aSecondQuaternion, const float aMaxDegreesDelta)
	{
		float num = Quaternion::Angle(aFirstQuaternion, aSecondQuaternion);
		if (num == 0.0f)
		{
			return aSecondQuaternion;
		}
		float t = std::fminf(1.0f, aMaxDegreesDelta / num);
		return Quaternion::Slerp(aFirstQuaternion, aSecondQuaternion, t);
	}
}