#pragma once
#include "Vector.h"

namespace CommonUtilities
{
	template <typename T>
	class Line
	{
	public:
		Line();
		Line(const Vector2<T>& aPoint0, const Vector2<T>& aPoint1);

		void InitWith2Points(const Vector2<T>& aPoint0, const Vector2<T>& aPoint1);
		void InitWithPointAndDirection(const Vector2<T>& aPoint, const Vector2<T>& aDirection);

		bool Inside(const Vector2<T>& aPosition) const;
		bool Outside(const Vector2<T>& aPosition) const;
		bool OnLine(const Vector2<T>& aPosition) const;

		bool IntersectsWithCircle(const Vector2<T>& aPosition, const float aRadius) const;

	private:
		Vector2<T> myPoint;
		Vector2<T> myDirection;
		Vector2<T> myNormal;
	};

	template<typename T>
	inline Line<T>::Line()
	{
		myPoint = Vector2<T>(static_cast<T>(0), static_cast<T>(0));
		myDirection = Vector2<T>(static_cast<T>(0), static_cast<T>(0));
	}

	template<typename T>
	inline Line<T>::Line(const Vector2<T>& aPoint0, const Vector2<T>& aPoint1)
	{
		myPoint = aPoint0;
		myDirection = aPoint1 - aPoint0;
		myNormal = { -myDirection.y, myDirection.x };
		myNormal.Normalize();
	}

	template<typename T>
	inline void Line<T>::InitWith2Points(const Vector2<T>& aPoint0, const Vector2<T>& aPoint1)
	{
		myPoint = aPoint0;
		myDirection = aPoint1 - aPoint0;
		myNormal = { -myDirection.y, myDirection.x };
		myNormal.Normalize();
	}

	template<typename T>
	inline void Line<T>::InitWithPointAndDirection(const Vector2<T>& aPoint, const Vector2<T>& aDirection)
	{
		myPoint = aPoint;
		myDirection = aDirection;
		myNormal = { -myDirection.y, myDirection.x };
		myNormal.Normalize();
	}

	template<typename T>
	inline bool Line<T>::Inside(const Vector2<T>& aPosition) const
	{
		return myNormal.Dot(aPosition - myPoint) < static_cast<T>(0);
	}

	template<typename T>
	inline bool Line<T>::Outside(const Vector2<T>& aPosition) const
	{
		return myNormal.Dot(aPosition - myPoint) > static_cast<T>(0);
	}

	template<typename T>
	inline bool Line<T>::OnLine(const Vector2<T>& aPosition) const
	{
		return myNormal.Dot(aPosition - myPoint) == static_cast<T>(0);
	}

	template<typename T>
	inline bool Line<T>::IntersectsWithCircle(const Vector2<T>& aPosition, const float aRadius) const
	{
		return myNormal.Dot(aPosition - myPoint) - aRadius <= static_cast<T>(0);
	}
}

namespace CU = CommonUtilities;
