#include "Random.h"
#include <random>
#include <ctime>

void CommonUtilities::InitRandom()
{
	srand(static_cast<unsigned int>(time(0)));
}

void CommonUtilities::InitRandom(const unsigned int aSeed)
{
	srand(aSeed);
}

long long CommonUtilities::GetRandom()
{
	return static_cast<long long>(rand());
}

float CommonUtilities::GetRandomPercentage()
{
	return GetRandomInRange(0.0f, 100.0f);
}

float CommonUtilities::GetRandomInRange(const float aMinValue, const float aMaxValue)
{
	float random = static_cast<float>(rand()) / static_cast<float>(RAND_MAX);
	return (aMaxValue - aMinValue) * random + aMinValue;
}

int CommonUtilities::GetRandomInRange(const int aMinValue, const int aMaxValue)
{
	return aMinValue + rand() % (aMaxValue - aMinValue);
}
