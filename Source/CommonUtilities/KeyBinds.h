#pragma once
namespace CommonUtilities
{

	class CKeyBinds
	{
	public:
		struct SHotKeys
		{
			unsigned char myMove_FORWARD;
			unsigned char myMove_RIGHT;
			unsigned char myMove_BACKWARDS;
			unsigned char myMove_LEFT;
			unsigned char myMove_UP;
			unsigned char myMove_DOWN;
			unsigned char myRoll_RIGHT;
			unsigned char myRoll_LEFT;
			unsigned char myBoost;
			unsigned char myShoot;
			unsigned char myMissile;
			unsigned char myPause;
			unsigned char myF1;
			unsigned char myF2;
			unsigned char myF3;
			unsigned char myF4;
			unsigned char myF5;
			unsigned char myF6;
			unsigned char myF7;
			unsigned char myF8;
			unsigned char myF9;
			unsigned char myTilde;
			unsigned char myEscape;
			unsigned char myEnterInput;
			unsigned char myArrowUp;
			unsigned char myArrowDown;
			unsigned char myArrowLeft;
			unsigned char myArrowRight;
			unsigned char myTab;
			unsigned char myH;
			unsigned char myNumPad1;
			unsigned char myNumPad2;
			unsigned char myNumPad3;
			unsigned char myNum1;
			unsigned char myNum2;
			unsigned char myNum3;
		};

		~CKeyBinds() = default;
		static const SHotKeys& GetHotKeys();
		static const bool Create(const char * aDataFilePath);
		static void Destroy();

	private:
		CKeyBinds(const char * aDataFilePath);
		void LoadKeyBindData(const char * aDataFilePath);

		static CKeyBinds* myInstance;
		static SHotKeys myHotKeys;
	};

}
namespace CU = CommonUtilities;