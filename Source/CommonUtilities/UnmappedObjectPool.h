#pragma once
#include "GrowingArray.h"
#include <queue>

namespace CommonUtilities
{
	template <class ObjectType, unsigned int ObjectMaxCount>
	class UnmappedObjectPool
	{
	public:
		UnmappedObjectPool();
		~UnmappedObjectPool();

		//Ugly but might work
		void GetObject(ObjectType*& aObject);

		ObjectType& GetObject();
		void Recycle(ObjectType& aObject);

		std::size_t GetSize() const;

	private:

		ObjectType* myObjects;
		std::queue<ObjectType*> myFreeObjects;
	};

	template<class ObjectType, unsigned int ObjectMaxCount>
	inline UnmappedObjectPool<ObjectType, ObjectMaxCount>::UnmappedObjectPool()
	{
		myObjects = sce_newArray(ObjectType, ObjectMaxCount);

		for (unsigned int i = 0; i < ObjectMaxCount; ++i)
		{
			myFreeObjects.push(&myObjects[i]);
		}
	}

	template<class ObjectType, unsigned int ObjectMaxCount>
	inline UnmappedObjectPool<ObjectType, ObjectMaxCount>::~UnmappedObjectPool()
	{
		if (myFreeObjects.size() != ObjectMaxCount)
		{
			assert(false && "All allocated game objects have not been returned!");
		}

		sce_delete(myObjects);
	}

	template<class ObjectType, unsigned int ObjectMaxCount>
	inline void UnmappedObjectPool<ObjectType, ObjectMaxCount>::GetObject(ObjectType *& aObject)
	{
		aObject = myFreeObjects.front();
		myFreeObjects.pop();
	}

	template<class ObjectType, unsigned int ObjectMaxCount>
	inline ObjectType & UnmappedObjectPool<ObjectType, ObjectMaxCount>::GetObject()
	{
		ObjectType* object = myFreeObjects.front();
		myFreeObjects.pop();
		return *object;
	}

	template<class ObjectType, unsigned int ObjectMaxCount>
	inline void UnmappedObjectPool<ObjectType, ObjectMaxCount>::Recycle(ObjectType & aObject)
	{
		myFreeObjects.push(&aObject);
	}

	template<class ObjectType, unsigned int ObjectMaxCount>
	inline std::size_t UnmappedObjectPool<ObjectType, ObjectMaxCount>::GetSize() const
	{
		return myFreeObjects.size();
	}
}

namespace CU = CommonUtilities;