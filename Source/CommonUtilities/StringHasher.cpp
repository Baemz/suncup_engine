#include "StringHasher.h"

namespace CommonUtilities
{
	namespace String
	{
		decltype(CStringHasher::ourHashedStrings) CStringHasher::ourHashedStrings = decltype(CStringHasher::ourHashedStrings)(&CStringHasher::SStringHash::Hash);
		std::shared_mutex CStringHasher::ourHashedStringsMutex;
	}
}
