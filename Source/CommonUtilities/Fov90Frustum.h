#pragma once
#include "PlaneVolume.h"
#include "Matrix.h"
#include "GrowingArray.h"

namespace CommonUtilities
{
	namespace Intersection
	{
		class Fov90Frustum
		{
		public:
			Fov90Frustum(float aNear, float aFar);
			bool Inside(const Vector3<float>& aPos, float aRadius) const;

			void OnResize(float aNearPlane, float aFarPlane);

			CommonUtilities::PlaneVolume<float> myVolume;
			float myNear;
			float myFar;
		};
	}

	inline Intersection::Fov90Frustum::Fov90Frustum(float aNear, float aFar)
	{
		myNear = aNear;
		myFar = aFar;
		float rotated45 = sqrt(2.f) / 2.f;

		Plane<float> nearPlane(Vector3<float>(0, 0, aNear)
			, Vector3<float>(0, 0, -1));
		Plane<float> farPlane(Vector3<float>(0, 0, aFar)
			, Vector3<float>(0, 0, 1));

		Plane<float> right(Vector3<float>(0, 0, 0), Vector3<float>(rotated45, 0, -rotated45));
		Plane<float> left(Vector3<float>(0, 0, 0), Vector3<float>(-rotated45, 0, -rotated45));
		Plane<float> up(Vector3<float>(0, 0, 0), Vector3<float>(0, rotated45, -rotated45));
		Plane<float> down(Vector3<float>(0, 0, 0), Vector3<float>(0, -rotated45, -rotated45));

		myVolume.AddPlane(nearPlane);
		myVolume.AddPlane(farPlane);
		myVolume.AddPlane(right);
		myVolume.AddPlane(left);
		myVolume.AddPlane(up);
		myVolume.AddPlane(down);
	}

	inline bool CommonUtilities::Intersection::Fov90Frustum::Inside(const Vector3<float>& aPos, float aRadius) const
	{
		if (myVolume.Inside(aPos, aRadius) == true)
		{
			return true;
		}
		return false;
	}

	inline void CommonUtilities::Intersection::Fov90Frustum::OnResize(float aNearPlane, float aFarPlane)
	{
		myVolume.Clear();
		myNear = aNearPlane;
		myFar = aFarPlane;
		float rotated45 = sqrt(2.f) / 2.f;

		Plane<float> nearPlane(Vector3<float>(0, 0, aNearPlane)
			, Vector3<float>(0, 0, -1));
		Plane<float> farPlane(Vector3<float>(0, 0, aFarPlane)
			, Vector3<float>(0, 0, 1));

		Plane<float> right(Vector3<float>(0, 0, 0), Vector3<float>(rotated45, 0, -rotated45));
		Plane<float> left(Vector3<float>(0, 0, 0), Vector3<float>(-rotated45, 0, -rotated45));
		Plane<float> up(Vector3<float>(0, 0, 0), Vector3<float>(0, rotated45, -rotated45));
		Plane<float> down(Vector3<float>(0, 0, 0), Vector3<float>(0, -rotated45, -rotated45));

		myVolume.AddPlane(nearPlane);
		myVolume.AddPlane(farPlane);
		myVolume.AddPlane(right);
		myVolume.AddPlane(left);
		myVolume.AddPlane(up);
		myVolume.AddPlane(down);
	}
}
namespace CU = CommonUtilities;