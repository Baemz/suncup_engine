#pragma once
#include <assert.h>
#include <algorithm>

#ifdef max
#pragma push_macro("max")
#undef max
#define MAX_UNDEFINED
#endif // max

namespace CommonUtilities
{
	template<typename Type, typename CountType = unsigned short>
	class GrowingArray
	{
	public:
		using value_type = Type;
		using pointer = Type*;
		using const_pointer = const Type*;
		using reference = Type&;
		using const_reference = const Type&;
		using size_type = CountType;
		using size_t = size_type;
		using iterator = Type*;
		using const_iterator = const Type*;

		GrowingArray();
		explicit GrowingArray(CountType aNrOfRecommendedItems, bool aUseSafeModeFlag = true);
		GrowingArray(const GrowingArray& aGrowingArray);
		GrowingArray(GrowingArray&& aGrowingArray);

		~GrowingArray();

		GrowingArray& operator=(const GrowingArray& aGrowingArray);
		GrowingArray& operator=(GrowingArray&& aGrowingArray);

		void Init(CountType aNrOfRecomendedItems, bool aUseSafeModeFlag = true);
		void ReInit(CountType aNrOfRecomendedItems, bool aUseSafeModeFlag = true);

		inline Type& operator[](const CountType& aIndex);
		inline const Type& operator[](const CountType& aIndex) const;

		// __________________________________________________________________________________________________
		// STL compatibility
		
		__forceinline iterator begin() { return myData; };
		__forceinline const_iterator begin() const { return myData; };
		__forceinline iterator end() { return &myData[myAmountOfAddedElements]; };
		__forceinline const_iterator end() const { return &myData[myAmountOfAddedElements]; };

		__forceinline reference front() { return GetFirst(); };
		__forceinline const_reference front() const { return GetFirst(); };
		__forceinline reference back() { return GetLast(); };
		__forceinline const_reference back() const { return GetLast(); };

		__forceinline void clear() { myAmountOfAddedElements = 0; };

		__forceinline size_type size() const { return Size(); };
		__forceinline size_type capacity() const { return Capacity(); };
		__forceinline bool empty() const { return Empty(); };
		static __forceinline constexpr size_type max_size() { return std::numeric_limits<size_type>::max(); };

		__forceinline reference at(const CountType& aIndex) { return (*this)[aIndex]; };
		__forceinline const_reference at(const CountType& aIndex) const { return (*this)[aIndex]; };

		// STL compatibility
		// __________________________________________________________________________________________________

		inline void Add(const Type& aObject);
		inline void Add(const GrowingArray& aGrowingArray);
		inline bool AddUnique(const Type& aObject);
		inline bool AddUnique(const GrowingArray& aGrowingArray);
		inline void Insert(CountType aIndex, const Type& aObject);
		inline void DeleteCyclic(Type& aObject);
		inline void DeleteCyclicAtIndex(CountType aItemNumber);
		inline void RemoveCyclic(const Type& aObject);
		inline void RemoveCyclicAtIndex(CountType aItemNumber);
		inline CountType Find(const Type& aObject) const;

		inline Type& GetFirst();
		inline const Type& GetFirst() const;
		inline Type& GetLast();
		inline const Type& GetLast() const;

		static constexpr CountType FoundNone = static_cast<CountType>(-1);

		inline void RemoveAll();
		inline void DeleteAll();

		void Optimize();
		__forceinline CountType Size() const;
		__forceinline CountType GetLastIndex() const;
		__forceinline CountType Capacity() const;
		__forceinline bool Empty() const;
		__forceinline bool Full() const;
		inline void Resize(CountType aNewSize);
		inline void Resize(CountType aNewSize, const Type& aNewElementsValue);
		inline void Reserve(CountType aNewSize);

		// You should be REALLY careful with these.
		// Get a pointer to the internal data in the array.
		Type* GetRawData() { return myData; };
		// You should be REALLY careful with these.
		// Get invalid object of stored type.
		// Use with decltype, ex:
		// using ArrayType = decltype(myArray.GetInvalidTypeObject());
		Type GetInvalidTypeObject() { return *(Type*(nullptr))};
		// You should be REALLY careful with these.
		// Sets internal array pointer to aStartAddress and sets capacity and size to aAmount.
		// WARNING: Removes any growing/resizing functionality on this array for forever. Except the functions that starts with same comment as this one, or ReInit.
		void SetRawData(Type* aStartAddress, CountType aAmount) { SetRawDataReserve(aStartAddress, aAmount, aAmount); };
		// You should be REALLY careful with these.
		// Sets internal array pointer to aStartAddress and capacity to aAmount. Sets size to aAmountAlreadyAdded.
		// WARNING: Removes any growing/resizing functionality on this array for forever, except the functions that starts with same comment as this one, or ReInit.
		void SetRawDataReserve(Type* aStartAddress, CountType aAmount, CountType aAmountAlreadyAdded) { if (myDontDeleteAnything == false) { delete[] myData; } myData = aStartAddress; myCapacity = aAmount; myAmountOfAddedElements = aAmountAlreadyAdded; myDontDeleteAnything = true; };
		// You should be REALLY careful with these.
		// Sets internal array pointer to nullptr and sets capacity and size to 0.
		// Also resets array to normal state, can grow again.
		// WARNING: Memory leak if GetRawData() hasn't been called and returned pointer saved.
		void ReleaseOwnership() { myData = nullptr; myAmountOfAddedElements = 0; myCapacity = 0; myDontDeleteAnything = false; };

	private:
		Type* myData;
		CountType myAmountOfAddedElements;
		CountType myCapacity;
		bool mySafeModeFlag;
		bool myDontDeleteAnything;

		void ChangeArrayCapacityIfNeeded(const CountType& aIndex);

	};

	template<typename Type, typename CountType>
	inline GrowingArray<Type, CountType>::GrowingArray()
		: myData(nullptr)
		, myAmountOfAddedElements(0)
		, myCapacity(0)
		, mySafeModeFlag(true)
		, myDontDeleteAnything(false)
	{
	}

	template<typename Type, typename CountType>
	inline GrowingArray<Type, CountType>::GrowingArray(CountType aNrOfRecommendedItems, bool aUseSafeModeFlag)
		: myData(nullptr)
		, myAmountOfAddedElements(0)
		, myCapacity(0)
		, mySafeModeFlag(true)
		, myDontDeleteAnything(false)
	{
		Init(aNrOfRecommendedItems, aUseSafeModeFlag);
	}

	template<typename Type, typename CountType>
	inline GrowingArray<Type, CountType>::GrowingArray(const GrowingArray& aGrowingArray)
		: myData(nullptr)
		, myAmountOfAddedElements(0)
		, myCapacity(0)
		, mySafeModeFlag(true)
		, myDontDeleteAnything(false)
	{
 		this->Init(aGrowingArray.myCapacity, aGrowingArray.mySafeModeFlag);
		*this = aGrowingArray;
	}

	template<typename Type, typename CountType>
	inline GrowingArray<Type, CountType>::GrowingArray(GrowingArray&& aGrowingArray)
		: myData(aGrowingArray.myData)
		, myAmountOfAddedElements(aGrowingArray.myAmountOfAddedElements)
		, myCapacity(aGrowingArray.myCapacity)
		, mySafeModeFlag(aGrowingArray.mySafeModeFlag)
		, myDontDeleteAnything(aGrowingArray.myDontDeleteAnything)
	{
		aGrowingArray.myData = nullptr;
	}

	template<typename Type, typename CountType>
	inline GrowingArray<Type, CountType>::~GrowingArray()
	{
		if (myDontDeleteAnything == false)
		{
			delete[] myData;
			myData = nullptr;
		}
	}

	template<typename Type, typename CountType>
	inline GrowingArray<Type, CountType>& GrowingArray<Type, CountType>::operator=(const GrowingArray& aGrowingArray)
	{
		if (aGrowingArray.myCapacity != 0)
		{
			this->ChangeArrayCapacityIfNeeded(aGrowingArray.myCapacity);
		}

		myAmountOfAddedElements = aGrowingArray.myAmountOfAddedElements;
		mySafeModeFlag = aGrowingArray.mySafeModeFlag;
		myDontDeleteAnything = aGrowingArray.myDontDeleteAnything;

		if (aGrowingArray.mySafeModeFlag == true)
		{
			for (CountType dataIndex(static_cast<CountType>(0)); dataIndex < myAmountOfAddedElements; ++dataIndex)
			{
				myData[static_cast<int>(dataIndex)] = aGrowingArray.myData[static_cast<int>(dataIndex)];
			}
		}
		else
		{
			memcpy(this->myData, aGrowingArray.myData, (sizeof(Type) * aGrowingArray.myAmountOfAddedElements));

			myAmountOfAddedElements = aGrowingArray.myAmountOfAddedElements;
		}

		return *this;
	}

	template<typename Type, typename CountType>
	inline GrowingArray<Type, CountType>& GrowingArray<Type, CountType>::operator=(GrowingArray&& aGrowingArray)
	{
		myAmountOfAddedElements = aGrowingArray.myAmountOfAddedElements;
		myCapacity = aGrowingArray.myAmountOfAddedElements;
		mySafeModeFlag = aGrowingArray.mySafeModeFlag;
		myDontDeleteAnything = aGrowingArray.myDontDeleteAnything;

		myData = aGrowingArray.myData;
		aGrowingArray.myData = nullptr;
		
		return *this;
	}

	template<typename Type, typename CountType>
	inline void GrowingArray<Type, CountType>::Init(CountType aNrOfRecomendedItems, bool aUseSafeModeFlag)
	{
		ReInit(aNrOfRecomendedItems, aUseSafeModeFlag);
	}

	template<typename Type, typename CountType>
	inline void GrowingArray<Type, CountType>::ReInit(CountType aNrOfRecomendedItems, bool aUseSafeModeFlag)
	{
		assert("Tried to create an empty or negative GrowingArray!" && (aNrOfRecomendedItems > 0));

		if (myDontDeleteAnything == true)
		{
			assert("Tried to ReInit array with not released raw data" && (myData == nullptr));
		}
		else
		{
			delete[] myData;
		}
		myData = new Type[aNrOfRecomendedItems];

		myAmountOfAddedElements = 0;
		myCapacity = aNrOfRecomendedItems;
		mySafeModeFlag = aUseSafeModeFlag;
		myDontDeleteAnything = false;
	}

	template<typename Type, typename CountType>
	inline Type& GrowingArray<Type, CountType>::operator[](const CountType& aIndex)
	{
		assert("Trying to access invalid index!" && ((aIndex >= 0) && (myAmountOfAddedElements > aIndex)));
		return myData[aIndex];
	}

	template<typename Type, typename CountType>
	inline const Type& GrowingArray<Type, CountType>::operator[](const CountType& aIndex) const
	{
		assert("Trying to access invalid index!" && ((aIndex >= 0) && (myAmountOfAddedElements > aIndex)));
		return myData[aIndex];
	}

	template<typename Type, typename CountType>
	inline void GrowingArray<Type, CountType>::Add(const Type& aObject)
	{
		Insert(myAmountOfAddedElements, aObject);
	}

	template<typename Type, typename CountType>
	inline void GrowingArray<Type, CountType>::Add(const GrowingArray& aGrowingArray)
	{
		for (CountType dataIndex(0); dataIndex < aGrowingArray.Size(); ++dataIndex)
		{
			Add(aGrowingArray[dataIndex]);
		}
	}

	template<typename Type, typename CountType>
	inline bool GrowingArray<Type, CountType>::AddUnique(const Type& aObject)
	{
		if (Find(aObject) == FoundNone)
		{
			Add(aObject);
			return true;
		}
		return false;
	}

	template<typename Type, typename CountType>
	inline bool GrowingArray<Type, CountType>::AddUnique(const GrowingArray& aGrowingArray)
	{
		bool allElementsUnique(true);
		for (CountType dataIndex(0); dataIndex < aGrowingArray.Size(); ++dataIndex)
		{
			if (AddUnique(aGrowingArray[dataIndex]) == false)
			{
				allElementsUnique = false;
			}
		}
		return allElementsUnique;
	}

	template<typename Type, typename CountType>
	inline void GrowingArray<Type, CountType>::Insert(CountType aIndex, const Type& aObject)
	{
		assert("Trying to access invalid index!" && ((aIndex >= 0) && (myAmountOfAddedElements >= aIndex)));
		if (myDontDeleteAnything == true)
		{
			assert("Array with raw data is filled, can't insert another element" && (myAmountOfAddedElements < myCapacity));
		}

		if (myCapacity <= myAmountOfAddedElements)
		{
			constexpr CountType countTypeMaxValue(max_size());
			assert("Can't insert another element, GrowingArray is full!" && (myCapacity != countTypeMaxValue));

			CountType newCapacity(myCapacity * static_cast<CountType>(2));
			if (newCapacity < myCapacity)
			{
				newCapacity = countTypeMaxValue;
			}
			ChangeArrayCapacityIfNeeded(newCapacity);
		}

		for (CountType dataIndex(myAmountOfAddedElements); dataIndex > aIndex; --dataIndex)
		{
			myData[dataIndex] = myData[dataIndex - 1];
		}

		myData[aIndex] = aObject;
		++myAmountOfAddedElements;
	}

	template<typename Type, typename CountType>
	inline void GrowingArray<Type, CountType>::DeleteCyclic(Type& aObject)
	{
		CountType indexFoundAt(Find(aObject));

		if (indexFoundAt != FoundNone)
		{
			DeleteCyclicAtIndex(indexFoundAt);
		}
	}

	template<typename Type, typename CountType>
	inline void GrowingArray<Type, CountType>::DeleteCyclicAtIndex(CountType aItemNumber)
	{
		assert("Trying to access invalid index!" && ((aItemNumber >= 0) && (myAmountOfAddedElements > aItemNumber)));

		delete myData[aItemNumber];
		myData[aItemNumber] = myData[--myAmountOfAddedElements];
	}

	template<typename Type, typename CountType>
	inline void GrowingArray<Type, CountType>::RemoveCyclic(const Type& aObject)
	{
		CountType indexFoundAt(Find(aObject));

		if (indexFoundAt != FoundNone)
		{
			RemoveCyclicAtIndex(indexFoundAt);
		}
	}

	template<typename Type, typename CountType>
	inline void GrowingArray<Type, CountType>::RemoveCyclicAtIndex(CountType aItemNumber)
	{
		assert("Trying to access invalid index!" && ((aItemNumber >= 0) && (myAmountOfAddedElements > aItemNumber)));

		myData[aItemNumber] = myData[--myAmountOfAddedElements];
	}

	template<typename Type, typename CountType>
	inline CountType GrowingArray<Type, CountType>::Find(const Type& aObject) const
	{
		if ((myCapacity == 0) || (myAmountOfAddedElements == 0))
		{
			return FoundNone;
		}

		const std::size_t rootPointer((std::size_t)(myData));
		const std::size_t foundPointer((std::size_t)(std::find(begin(), end(), aObject)));

		const CountType foundIndex(static_cast<CountType>((foundPointer - rootPointer) / sizeof(value_type)));

		if (foundIndex == myAmountOfAddedElements)
		{
			return FoundNone;
		}

		return foundIndex;
	}

	template<typename Type, typename CountType>
	inline Type& GrowingArray<Type, CountType>::GetFirst()
	{
		assert("Trying to get first element in array when it's empty" && (myAmountOfAddedElements > 0));
		return myData[0];
	}

	template<typename Type, typename CountType>
	inline const Type& GrowingArray<Type, CountType>::GetFirst() const
	{
		assert("Trying to get first element in array when it's empty" && (myAmountOfAddedElements > 0));
		return myData[0];
	}

	template<typename Type, typename CountType>
	inline Type& GrowingArray<Type, CountType>::GetLast()
	{
		assert("Trying to get Last element in array when it's empty" && (myAmountOfAddedElements > 0));
		return myData[myAmountOfAddedElements - 1];
	}

	template<typename Type, typename CountType>
	inline const Type& GrowingArray<Type, CountType>::GetLast() const
	{
		assert("Trying to get Last element in array when it's empty" && (myAmountOfAddedElements > 0));
		return myData[myAmountOfAddedElements - 1];
	}

	template<typename Type, typename CountType>
	inline void GrowingArray<Type, CountType>::RemoveAll()
	{
		myAmountOfAddedElements = 0;
	}

	template<typename Type, typename CountType>
	inline void GrowingArray<Type, CountType>::DeleteAll()
	{
		for (CountType dataIndex(0); dataIndex < myAmountOfAddedElements; ++dataIndex)
		{
			delete myData[dataIndex];
			myData[dataIndex] = nullptr;
		}

		RemoveAll();
	}

	template<typename Type, typename CountType>
	inline void GrowingArray<Type, CountType>::Optimize()
	{
		if (myDontDeleteAnything == true)
		{
			return;
		}

		if (myCapacity == myAmountOfAddedElements)
		{
			return;
		}

		Type* allData(myData);
		myData = new Type[myAmountOfAddedElements];
		myCapacity = myAmountOfAddedElements;

		for (CountType dataIndex(0); dataIndex < myAmountOfAddedElements; ++dataIndex)
		{
			myData[dataIndex] = allData[dataIndex];
		}

		delete[] allData;
	}

	template<typename Type, typename CountType>
	inline CountType GrowingArray<Type, CountType>::Size() const
	{
		return myAmountOfAddedElements;
	}

	template<typename Type, typename CountType>
	inline CountType GrowingArray<Type, CountType>::GetLastIndex() const
	{
		if (myAmountOfAddedElements == 0)
		{
			return FoundNone;
		}
		return (myAmountOfAddedElements - 1);
	}
	
	template<typename Type, typename CountType>
	inline CountType GrowingArray<Type, CountType>::Capacity() const
	{
		return myCapacity;
	}

	template<typename Type, typename CountType>
	inline bool GrowingArray<Type, CountType>::Empty() const
	{
		return (myAmountOfAddedElements == 0);
	}

	template<typename Type, typename CountType>
	inline bool GrowingArray<Type, CountType>::Full() const
	{
		return (myAmountOfAddedElements == myCapacity);
	}

	template<typename Type, typename CountType>
	inline void GrowingArray<Type, CountType>::Resize(CountType aNewSize)
	{
		if (myDontDeleteAnything == true)
		{
			return;
		}

		if (aNewSize == 0)
		{
			return;
		}

		ChangeArrayCapacityIfNeeded(aNewSize);

		myAmountOfAddedElements = myCapacity;
	}

	template<typename Type, typename CountType>
	inline void GrowingArray<Type, CountType>::Resize(CountType aNewSize, const Type& aNewElementsValue)
	{
		CountType oldCapacity(myCapacity);
		
		Resize(aNewSize);

		if (oldCapacity < myCapacity)
		{
			for (CountType index(oldCapacity); index < myCapacity; ++index)
			{
				myData[index] = aNewElementsValue;
			}
		}
	}

	template<typename Type, typename CountType>
	inline void GrowingArray<Type, CountType>::Reserve(CountType aNewSize)
	{
		if (myDontDeleteAnything == true)
		{
			return;
		}

		if (aNewSize == 0)
		{
			return;
		}

		if (myCapacity < aNewSize)
		{
			Type* allData(myData);

			myData = new Type[aNewSize];

			myCapacity = aNewSize;

			for (CountType dataIndex(0); dataIndex < myAmountOfAddedElements; ++dataIndex)
			{
				myData[dataIndex] = allData[dataIndex];
			}

			delete[] allData;
		}
	}

	template<typename Type, typename CountType>
	inline void GrowingArray<Type, CountType>::ChangeArrayCapacityIfNeeded(const CountType& aIndex)
	{
		if (myDontDeleteAnything == true)
		{
			return;
		}

		if (myCapacity < aIndex)
		{
			Type* allData(myData);
			
			myData = new Type[aIndex];

			myCapacity = aIndex;

			for (CountType dataIndex(0); dataIndex < myAmountOfAddedElements; ++dataIndex)
			{
				myData[dataIndex] = allData[dataIndex];
			}

			delete[] allData;
		}
		else if (myCapacity > aIndex)
		{
			myAmountOfAddedElements = aIndex;
			myCapacity = aIndex;
		}
		else if (myCapacity == 0)
		{
			if (aIndex == 0)
			{
				myData = new Type[2];

				myCapacity = 2;
			}
			else
			{
				myData = new Type[aIndex];

				myCapacity = aIndex;
			}
		}
	}
};

namespace CU = CommonUtilities;

#ifdef MAX_UNDEFINED
#undef MAX_UNDEFINED
#pragma pop_macro("max")
#endif // MAX_UNDEFINED
