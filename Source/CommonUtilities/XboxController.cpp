#include "XboxController.h"

CommonUtilities::XButtonIDs XboxButtons;

CommonUtilities::XboxController::XboxController()
{
	myControllerNumber = 0; 
	
	for (int i = 0; i < ButtonCount; i++)
	{
		previousButtonStates[i] = false;
		currentButtonStates[i] = false;
		controllerButtonsDown[i] = false;
	}
}

CommonUtilities::XboxController::XboxController(int aControllerIndex)
{
	myControllerNumber = aControllerIndex;

	for (int i = 0; i < ButtonCount; i++)
	{
		previousButtonStates[i] = false;
		currentButtonStates[i] = false;
		controllerButtonsDown[i] = false;
	}
}


CommonUtilities::XboxController::~XboxController()
{
	this->Vibrate();
}

bool CommonUtilities::XboxController::IsXboxControllerConnected()
{
	memset(&myControllerState, 0, sizeof(XINPUT_STATE));

	DWORD result = XInputGetState(myControllerNumber, &myControllerState);

	if (result == ERROR_SUCCESS)
	{
		return true;
	}
	else
	{
		return false;
	}
}

void CommonUtilities::XboxController::Vibrate(float aLeftMotor, float aRightMotor)
{
	XINPUT_VIBRATION vibration;
	memset(&vibration, 0, sizeof(vibration));

	int leftVibration = static_cast<int>(aLeftMotor * 65535.0f);
	int rightVibration = static_cast<int>(aRightMotor * 65535.0f);

	vibration.wLeftMotorSpeed = static_cast<WORD>(leftVibration);
	vibration.wRightMotorSpeed = static_cast<WORD>(rightVibration);
	XInputSetState(static_cast<int>(myControllerNumber), &vibration);


// 		XINPUT_VIBRATION Vibration;
// 
// 		ZeroMemory(&Vibration, sizeof(XINPUT_VIBRATION));
// 
// 		// Set the Vibration Values
// 		Vibration.wLeftMotorSpeed = static_cast<WORD>(aLeftMotor * 65535);
// 		Vibration.wRightMotorSpeed = static_cast<WORD>(aRightMotor * 65535);
// 
// 		XInputSetState(myControllerNumber, &Vibration);
}

bool CommonUtilities::XboxController::IsButtonDown(const int aButton) const
{
	if (myControllerState.Gamepad.wButtons& XINPUT_Buttons[aButton])
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool CommonUtilities::XboxController::WasButtonPressed(const int aButton) const
{
	return controllerButtonsDown[aButton];
}

XINPUT_STATE CommonUtilities::XboxController::GetState()
{
	memset(&myControllerState, 0, sizeof(XINPUT_STATE));
	XInputGetState(myControllerNumber, &myControllerState);
	return myControllerState;
}

void CommonUtilities::XboxController::Update()
{
	myControllerState = GetState();

	for (int i = 0; i < ButtonCount; i++)
	{
		currentButtonStates[i] = (myControllerState.Gamepad.wButtons & XINPUT_Buttons[i]) == XINPUT_Buttons[i];
		controllerButtonsDown[i] = !previousButtonStates[i] && currentButtonStates[i];
	}
}

void CommonUtilities::XboxController::RefreshState()
{
	memcpy(previousButtonStates, currentButtonStates, sizeof(previousButtonStates));
}

bool CommonUtilities::XboxController::IsRightStickInDeadZone() const
{
	short xValue = myControllerState.Gamepad.sThumbRX;
	short yValue = myControllerState.Gamepad.sThumbRY;

	if (xValue > XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE || xValue < -XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE)
	{
		return false;
	}

	if (yValue > XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE || yValue < -XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE)
	{
		return false;
	}

	return true;
}

bool CommonUtilities::XboxController::IsLeftStickInDeadZone() const
{
	short xValue = myControllerState.Gamepad.sThumbLX;
	short yValue = myControllerState.Gamepad.sThumbLY;

	if (xValue > XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE || xValue < -XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE)
	{
		return false;
	}

	if (yValue > XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE || yValue < -XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE)
	{
		return false;
	}

	return true;
}

float CommonUtilities::XboxController::LeftStickX() const
{
	short value = myControllerState.Gamepad.sThumbLX;
	return (static_cast<float>(value) / 32768.0f);
}

float CommonUtilities::XboxController::LeftStickY() const
{
	short value = myControllerState.Gamepad.sThumbLY;
	return (static_cast<float>(value) / 32768.0f);
}

float CommonUtilities::XboxController::RightStickX() const
{
	short value = myControllerState.Gamepad.sThumbRX;
	return (static_cast<float>(value) / 32768.0f);
}

float CommonUtilities::XboxController::RightStickY() const
{
	short value = myControllerState.Gamepad.sThumbRY;
	return (static_cast<float>(value) / 32768.0f);
}

float CommonUtilities::XboxController::LeftTrigger()
{
	BYTE Trigger = myControllerState.Gamepad.bLeftTrigger;

	if (Trigger > XINPUT_GAMEPAD_TRIGGER_THRESHOLD)
	{
		return Trigger / 255.0f;
	}

	return 0.0f;
}

float CommonUtilities::XboxController::RightTrigger()
{
	BYTE Trigger = myControllerState.Gamepad.bRightTrigger;

	if (Trigger > XINPUT_GAMEPAD_TRIGGER_THRESHOLD)
	{
		return Trigger / 255.0f;
	}

	return 0.0f;
}

CommonUtilities::XButtonIDs::XButtonIDs()
{
	A = 0;
	B = 1;
	X = 2;
	Y = 3;

	DPad_Up = 4;
	DPad_Down = 5;
	DPad_Left = 6;
	DPad_Right = 7;

	L_Shoulder = 8;
	R_Shoulder = 9;

	L_Thumbstick = 10;
	R_Thumbstick = 11;

	Start = 12;
	Back = 13;
}
