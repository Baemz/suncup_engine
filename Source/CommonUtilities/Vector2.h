#pragma once
#include <cmath>
namespace CommonUtilities
{
	template <class T>
	class Vector2
	{
	public:
		T x;
		T y;

		Vector2<T>();
		Vector2<T>(const T& aValue);
		Vector2<T>(const T& aX, const T& aY);
		Vector2<T>(const Vector2<T>& aVector) = default;
		Vector2<T>& operator=(const Vector2<T>& aVector2) = default;
		bool operator==(const Vector2& aVector2) const;
		bool operator!=(const Vector2& aVector2) const;
		Vector2<T> operator-() const;
		~Vector2<T>() = default;
		void Vector2<T>::operator/=(const Vector2<T>& aVector);

		T Length2() const;
		T Length() const;
		Vector2<T> GetNormalized() const;
		void Normalize();
		T Dot(const Vector2<T>& aVector) const;
		Vector2<T> GetRotated(const T& aAngle) const;
		void Rotate(const T& aAngle);

		static const Vector2<T> Zero;
	};



	template<class T>
	const Vector2<T> Vector2<T>::Zero;

	template<class T> inline Vector2<T>::Vector2()
	{
		x = static_cast<T>(0);
		y = static_cast<T>(0);
	}

	template<class T>
	inline Vector2<T>::Vector2(const T & aValue)
	{
		x = aValue;
		y = aValue;
	}

	template<class T> inline Vector2<T>::Vector2(const T& aX, const T& aY)
	{
		x = aX;
		y = aY;
	}

	template<class T> inline T Vector2<T>::Length2() const
	{
		return ((x*x) + (y*y));
	}

	template<class T> inline T Vector2<T>::Length() const
	{
		return sqrt((x*x) + (y*y));
	}

	template<class T> inline Vector2<T> Vector2<T>::GetNormalized() const
	{
		Vector2<T> newVector(*this);
		newVector.Normalize();
		return newVector;
	}

	template<class T> inline void Vector2<T>::Normalize()
	{
		T length = this->Length2();
		if (length == static_cast<T>(0))
		{
			return;
		}
		length = sqrt(length);
		x /= length;
		y /= length;
	}

	template<class T> inline T Vector2<T>::Dot(const Vector2<T>& aVector) const
	{
		return ((x*aVector.x) + (y*aVector.y));
	}

	template<class T>
	inline Vector2<T> Vector2<T>::GetRotated(const T& aAngle) const
	{
		Vector2<T> newVector(*this);
		newVector.Rotate();
		return newVector;
	}

	template<class T>
	inline void Vector2<T>::Rotate(const T& aAngle)
	{
		const float cosTemp(cosf(aAngle));
		const float sinTemp(sinf(aAngle));

		CU::Vector2<T> tempVector;

		x = ((cosTemp * tempVector.x) - (sinTemp * tempVector.y));
		y = ((sinTemp * tempVector.x) + (cosTemp * tempVector.y));
	}

	template<class T>
	bool Vector2<T>::operator==(const Vector2<T>& aVector2) const
	{
		return x == aVector2.x && y == aVector2.y;
	}

	template<class T>
	bool Vector2<T>::operator!=(const Vector2<T>& aVector2) const
	{
		return !(*this == aVector2);
	}

	template <class T> Vector2<T> Vector2<T>::operator-() const
	{
		return Vector2<T>(-x, -y);
	}

	//Returns the vector sum of aVector0 and aVector1
	template <class T> Vector2<T> operator+(const Vector2<T>& aVector0, const Vector2<T>& aVector1)
	{
		return Vector2<T>((aVector0.x + aVector1.x), (aVector0.y + aVector1.y));
	}

	//Returns the vector difference of aVector0 and aVector1
	template <class T> Vector2<T> operator-(const Vector2<T>& aVector0, const Vector2<T>& aVector1)
	{
		return Vector2<T>((aVector0.x - aVector1.x), (aVector0.y - aVector1.y));
	}

	//Returns the vector aVector multiplied by the scalar aScalar
	template <class T> Vector2<T> operator*(const Vector2<T>& aVector, const T& aScalar)
	{
		return Vector2<T>((aVector.x * aScalar), (aVector.y * aScalar));
	}

	//Returns the vector aVector multiplied by the scalar aScalar
	template <class T> Vector2<T> operator*(const T& aScalar, const Vector2<T>& aVector)
	{
		return Vector2<T>((aVector.x * aScalar), (aVector.y * aScalar));
	}

	template <class T> Vector2<T> operator*(const Vector2<T>& aVector0, const Vector2<T>& aVector1)
	{
		return Vector2<T>((aVector0.x * aVector1.x), (aVector0.y * aVector1.y));
	}

	//Returns the vector aVector divided by the scalar aScalar (equivalent to aVector multiplied by 1 / aScalar)
	template <class T> Vector2<T> operator/(const Vector2<T>& aVector, const T& aScalar)
	{
		T scale = (1 / aScalar);
		return Vector2<T>((aVector.x * scale), (aVector.y * scale));
	}

	//Returns the vector aVector divided by the scalar aScalar (equivalent to aVector multiplied by 1 / aScalar)
	template <class T> Vector2<T> operator/(const T& aScalar, const Vector2<T>& aVector)
	{
		return Vector2<T>((aScalar / aVector.x), (aScalar / aVector.y));
	}

	template <class T> Vector2<T> operator/(const Vector2<T>& aVector0, const Vector2<T>& aVector1)
	{
		return Vector2<T>((aVector0.x / aVector1.x), (aVector0.y / aVector1.y));
	}

	//Equivalent to setting aVector0 to (aVector0 + aVector1)
	template <class T> void operator+=(Vector2<T>& aVector0, const Vector2<T>& aVector1)
	{
		aVector0.x = (aVector0.x + aVector1.x);
		aVector0.y = (aVector0.y + aVector1.y);
	}


	//Equivalent to setting aVector0 to (aVector0 - aVector1)
	template <class T> void operator-=(Vector2<T>& aVector0, const Vector2<T>& aVector1)
	{
		aVector0.x = (aVector0.x - aVector1.x);
		aVector0.y = (aVector0.y - aVector1.y);
	}


	//Equivalent to setting aVector to (aVector * aScalar)
	template <class T> void operator*=(Vector2<T>& aVector, const T& aScalar)
	{
		aVector.x = (aVector.x * aScalar);
		aVector.y = (aVector.y * aScalar);
	}


	//Equivalent to setting aVector to (aVector / aScalar)
	template <class T> void operator/=(Vector2<T>& aVector, const T& aScalar)
	{
		T scale = (1 / aScalar);
		aVector.x = (aVector.x * scale);
		aVector.y = (aVector.y * scale);
	}


	//Equivalent to setting aVector to (aVector / aScalar)
	template <class T> void Vector2<T>::operator/=(const Vector2<T>& aVector)
	{
		x /= aVector.x;
		y /= aVector.y;
	}

	using Vector2f = Vector2<float>;
	using Vector2i = Vector2<int>;
	using Vector2ui = Vector2<unsigned int>;
}
namespace CU = CommonUtilities;