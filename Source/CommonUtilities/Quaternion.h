#pragma once
#include "Matrix44.h"
#include "Vector3.h"
#include "Vector4.h"

#pragma warning(disable : 4201)

namespace CommonUtilities
{
	class Quaternion
	{
	public:
		Quaternion(float aRotationX = 0.0f, float aRotationY = 0.0f, float aRotationZ = 0.0f, float aRotationAmount = 1.0f, bool aNormalize = true);
		Quaternion(Matrix44f aMatrix);
		Quaternion(const Vector3f &aRotation, float aRotationAmount = 1.0f, bool aNormalize = true);
		Quaternion(const Vector4f & aRotationAndAmount, bool aNormalize = true);
		Quaternion(const Vector3f& aEulerAngle, bool aFromEuler);
		Quaternion(const Quaternion& aQuaternion);
		void Normalize();

		//dont breathe this
		//Quaternion CreateFromVectorsYlf(const Vector3f &aStartPosition, const Vector3f &aEndPosition, const Vector3f &aUp);


		void RotateAlongAxis(const Vector3f& aAxis, float aRotationAmount);
		void RotateWorld(const Vector3f& aRotationAmount);
		void RotateWorldX(float aRotationAmount);
		void RotateWorldY(float aRotationAmount);
		void RotateWorldZ(float aRotationAmount);

		static Quaternion CreateFromAxisAngle(const Vector3f &aAxis, const float anAngle);

		void RotateLocal(const Vector3f& aRotationAmount);
		void RotateLocalX(float aRotationAmount);
		void RotateLocalY(float aRotationAmount);
		void RotateLocalZ(float aRotationAmount);


		void RotateTowardsPoint(const Vector3f & aCurrentPosition, const Vector3f & aTargetPosition);
		Vector3f ToEuler() const;

		Vector3f GetRight() const;
		Vector3f GetLeft() const;
		Vector3f GetUp() const;
		Vector3f GetDown() const;
		Vector3f GetForward() const;
		Vector3f GetBackward() const;

		//Roll = X Rotation
		float GetRoll() const;

		//Pitch = Y Rotation
		float GetPitch() const;

		//Yaw = Z Rotation
		float GetYaw() const;

		static Quaternion LookAt(const Vector3f &aStartPosition, const Vector3f &aEndPosition, const Vector3f &aUp = Vector3f(0.0f, 1.0f, 0.0f));

		static Quaternion FromToRotation(const Vector3f& aFrom, const Vector3f& aTo);

		//note: (Q1*Q2) != (Q2*Q1)
		Quaternion operator *(const Quaternion& aRight) const;


		bool operator ==(const Quaternion& aRight) const;
		bool operator !=(const Quaternion& aRight) const;

		//note: (Q1*Q2) != (Q2*Q1)
		Quaternion& operator *=(const Quaternion& aRight);

		Quaternion operator / (const float aScale) const;
		Quaternion operator * (const float aScale) const;
		Quaternion operator + (const Quaternion& aRight) const;
		Quaternion operator - (const Quaternion& aRight) const;
		Vector3f operator * (const Vector3f &aRight) const;

		Quaternion& operator /= (const float aScale);
		Quaternion& operator *=(const float aScale);
		Quaternion& operator += (const Quaternion& aRight);
		Quaternion& operator -= (const Quaternion& aRight);

		//this assumes the quaternion is normalized
		Matrix44f GenerateMatrix() const;

		static Quaternion GetInverse(const Quaternion &aQuaternion);
		static float Dot(const Quaternion &aFirstQuaternion, const Quaternion& aSecondQuaternion);
		static float Angle(const Quaternion& aFirstQuaternion, const Quaternion& aSecondQuaternion);
		static Quaternion SlerpNoInvert(const Quaternion& q1, const Quaternion& q2, float t);
		static Quaternion Slerp(const Quaternion &aFirstQuaternion, const Quaternion &aSecondQuaternion, const float aProgress);
		static Quaternion RotateTowards(const Quaternion& aFirstQuaternion, const Quaternion& aSecondQuaternion, const float aMaxDegreesDelta);
		static Quaternion Identity;

		//private:
		union
		{
			struct
			{
				Vector3f myRotation;
				float myRotationAmount;
			};
			struct
			{
				float x, y, z, w;
			};
		};
	};
}


#pragma warning(default : 4201)







//#pragma once
//
//#include "Matrix44.h"
//
//#define E 2.7182818284590
//#define CLOSE_QUATERNIONS 0.05f
//
//namespace CommonUtilities
//{
//	template <typename T>
//	class Quaternion
//	{
//	public:
//		Vector3<T> myAxis;
//		T myReal;
//
//		Quaternion();
//		Quaternion(const Quaternion& aQuaternion);
//		Quaternion(const Vector3<T> aAxis, T aReal);
//		Quaternion(const Vector3<T> aEulerAngles);
//		Quaternion(const Matrix44<T>& aMatrix);
//		~Quaternion() = default;
//
//		Quaternion& operator=(const Quaternion& aQuaternion);
//
//		Quaternion operator+(const Quaternion& aQuaternion) const;
//		Quaternion& operator+=(const Quaternion& aQuaternion);
//		Quaternion operator-(const Quaternion& aQuaternion) const;
//		Quaternion& operator-=(const Quaternion& aQuaternion);
//		Quaternion operator*(const Quaternion& aQuaternion) const;
//		Quaternion& operator*=(const Quaternion& aQuaternion);
//		
//		Quaternion& operator*=(T aScalar);
//		Quaternion operator*(T aScalar) const;
//		Quaternion operator/(T aScalar) const;
//
//		Vector3<T> operator*(const Vector3<T>& aVector) const;
//
//		bool operator==(const Quaternion& aQuaternion);
//		bool operator!=(const Quaternion& aQuaternion);
//
//		T Dot(const Quaternion& aQuaternion);
//		//T GetAngleTo(const Quaternion& aQuaternion);
//
//		Quaternion GetConjugated() const;
//		T GetNorm() const;
//		T GetNorm2() const;
//		Quaternion GetInverse() const;
//		Quaternion GetNormalized() const;
//
//		void Normalize();
//
//		Vector3<T> GetLog() const;
//		Quaternion Pow(T aScalar);
//
//		static Quaternion CreateRotationQuaternion(const Vector3<T>& aAxis, T aAngle);
//
//		static Quaternion CreateRotationXYZ(const Vector3<T>& aRotation);
//
//		static Quaternion CreateRotationX(T aRadian);
//		static Quaternion CreateRotationY(T aRadian);
//		static Quaternion CreateRotationZ(T aRadian);
//
//		static Quaternion Slerp(const Quaternion& aFrom, const Quaternion& aTo, T aTime);
//
//		Vector3<T> ToEuler() const;
//		Vector3<T> GetLook() const;
//		Vector3<T> GetUp() const;
//		Vector3<T> GetRight() const;
//
//		Matrix44<T> GetMatrix() const;
//
//		static const Quaternion Identity;
//
//	private:
//
//		void SetRotationX(T aRadian);
//		void SetRotationY(T aRadian);
//		void SetRotationZ(T aRadian);
//
//		void SetRotationXYZ(const Vector3<T>& aRotation);
//
//		void RotateLocalX(T aRotationAmount);
//		void RotateLocalY(T aRotationAmount);
//		void RotateLocalZ(T aRotationAmount);
//
//		bool IsUnitQuaternion() const;
//		T GetHalfUnitAngle() const;
//	};
//
//	template <typename T>
//	const Quaternion<T> Quaternion<T>::Identity(CU::Vector3f(0), 1);
//
//	template<typename T>
//	inline Quaternion<T>::Quaternion()
//		: myAxis(CU::Vector3f(0))
//		, myReal(1)
//	{
//	}
//
//	template<typename T>
//	inline Quaternion<T>::Quaternion(const Quaternion & aQuaternion)
//		: myAxis(aQuaternion.myAxis)
//		, myReal(aQuaternion.myReal)
//	{
//	}
//
//	template<typename T>
//	inline Quaternion<T>::Quaternion(const Vector3<T> aAxis, T aReal)
//		: myAxis(aAxis)
//		, myReal(aReal)
//	{
//		assert(IsUnitQuaternion() && "Operation makes quaternion unnormalized");
//	}
//
//	template<typename T>
//	inline Quaternion<T>::Quaternion(const Vector3<T> aViewDirection)
//	{
//		SetRotationXYZ(aViewDirection);
//	}
//
//	template<typename T>
//	inline Quaternion<T>::Quaternion(const Matrix44<T>& aMatrix)
//	{
//		double wsqrdminus1 = aMatrix[0] + aMatrix[5] + aMatrix[10];
//		double xsqrdminus1 = aMatrix[0] - aMatrix[5] - aMatrix[10];
//		double ysqrdminus1 = aMatrix[5] - aMatrix[0] - aMatrix[10];
//		double zsqrdminus1 = aMatrix[10] - aMatrix[0] - aMatrix[5];
//		int biggestindex = 0;
//		double biggest = wsqrdminus1;
//		if (xsqrdminus1 > biggest) {
//			biggest = xsqrdminus1;
//			biggestindex = 1;
//		}
//		if (ysqrdminus1 > biggest) {
//			biggest = ysqrdminus1;
//			biggestindex = 2;
//		}
//		if (zsqrdminus1 > biggest) {
//			biggest = zsqrdminus1;
//			biggestindex = 3;
//		}
//		double biggestval = sqrtf(biggest+ 1.0f)*.5f;
//		double mult= .25f / biggestval;
//		switch (biggestindex) {
//		case 0:
//			myReal = biggestval;
//			myAxis.x = (aMatrix[6] - aMatrix[9]) * mult;
//			myAxis.y = (aMatrix[8] - aMatrix[2]) * mult;
//			myAxis.z = (aMatrix[1] - aMatrix[4]) * mult;
//			break;
//		case 1:
//			myAxis.x = biggestval;
//			myReal = (aMatrix[6] - aMatrix[9]) * mult;
//			myAxis.y = (aMatrix[1] + aMatrix[4]) * mult;
//			myAxis.z = (aMatrix[8] + aMatrix[2]) * mult;
//			break;
//		case 2:
//			myAxis.y = biggestval;
//			myReal = (aMatrix[8] - aMatrix[2]) * mult;
//			myAxis.x = (aMatrix[1] + aMatrix[4]) * mult;
//			myAxis.z = (aMatrix[6] + aMatrix[9]) * mult;
//			break;
//		case 3:
//			myAxis.z = biggestval;
//			myReal = (aMatrix[2] - aMatrix[4]) * mult;
//			myAxis.x = (aMatrix[8] + aMatrix[2]) * mult;
//			myAxis.y = (aMatrix[6] + aMatrix[9]) * mult;
//			break;
//		};
//	}
//
//	template<typename T>
//	inline Quaternion<T> & Quaternion<T>::operator=(const Quaternion & aQuaternion)
//	{
//		myAxis = aQuaternion.myAxis;
//		myReal = aQuaternion.myReal;
//
//		assert(IsUnitQuaternion() && "Cannot assign to a unnormalized quaternion");
//		return (*this);
//	}
//
//	template<typename T>
//	inline Quaternion<T> Quaternion<T>::operator+(const Quaternion & aQuaternion) const
//	{
//		Quaternion<T> quaternion;
//
//		quaternion.myAxis = myAxis + aQuaternion.myAxis;
//		quaternion.myReal = myReal + aQuaternion.myReal;
//
//		return quaternion;
//	}
//
//	template<typename T>
//	inline Quaternion<T> & Quaternion<T>::operator+=(const Quaternion & aQuaternion)
//	{
//		myAxis += aQuaternion.myAxis;
//		myReal += aQuaternion.myReal;
//
//		assert(IsUnitQuaternion() && "Operation makes quaternion unnormalized");
//
//		return (*this);
//	}
//
//	template<typename T>
//	inline Quaternion<T> Quaternion<T>::operator-(const Quaternion & aQuaternion) const
//	{
//		Quaternion<T> quaternion;
//
//		quaternion.myAxis = myAxis - aQuaternion.myAxis;
//		quaternion.myReal = myReal - aQuaternion.myReal;
//
//		return quaternion;
//	}
//
//	template<typename T>
//	inline Quaternion<T> & Quaternion<T>::operator-=(const Quaternion & aQuaternion)
//	{
//		myAxis -= aQuaternion.myAxis;
//		myReal -= aQuaternion.myReal;
//
//		assert(IsUnitQuaternion() && "Operation makes quaternion unnormalized");
//
//		return (*this);
//	}
//
//	template<typename T>
//	inline Quaternion<T> Quaternion<T>::operator*(const Quaternion & aQuaternion) const
//	{
//		Quaternion<T> quaternion;
//
//		quaternion.myAxis = myAxis.Cross(aQuaternion.myAxis) + aQuaternion.myReal * myAxis + myReal * aQuaternion.myAxis;
//		quaternion.myReal = myReal * aQuaternion.myReal - myAxis.Dot(aQuaternion.myAxis);
//
//		return quaternion;
//	}
//
//	template<typename T>
//	inline Quaternion<T> & Quaternion<T>::operator*=(const Quaternion & aQuaternion)
//	{
//		myAxis = myAxis.Cross(aQuaternion.myAxis) + aQuaternion.myReal * myAxis + myReal * aQuaternion.myAxis;
//		myReal = myReal * aQuaternion.myReal - myAxis.Dot(aQuaternion.myAxis);
//
//		assert(IsUnitQuaternion() && "Operation makes quaternion unnormalized");
//
//		return (*this);
//	}
//
//	template<typename T>
//	inline Quaternion<T> & Quaternion<T>::operator*=(T aScalar)
//	{
//		myAxis *= aScalar;
//		myReal *= aScalar;
//
//		assert(IsUnitQuaternion() && "Operation makes quaternion unnormalized");
//
//		return (*this);
//	}
//
//	template<typename T>
//	inline Quaternion<T> Quaternion<T>::operator*(T aScalar) const
//	{
//		Quaternion quaternion;
//
//		quaternion.myAxis = myAxis * aScalar;
//		quaternion.myReal = myReal * aScalar;
//
//		return quaternion;
//	}
//
//	template<typename T>
//	inline Quaternion<T> Quaternion<T>::operator/(T aScalar) const
//	{
//		Quaternion quaternion;
//
//		quaternion.myAxis = myAxis / aScalar;
//		quaternion.myReal = myReal / aScalar;
//
//		return quaternion;
//	}
//
//	template<typename T>
//	inline Vector3<T> Quaternion<T>::operator*(const Vector3<T>& aVector) const
//	{
//		Vector3<T> firstTest = myAxis * 2.0f * myAxis.Dot(aVector);
//		Vector3<T> secondTest = aVector * (myReal * myReal - myAxis.Dot(myAxis));
//		Vector3<T> thirdTest = myAxis.Cross(aVector) * myReal * 2.0f;
//
//		return Vector3<T>(
//			firstTest
//			+ secondTest
//			+ thirdTest);
//	}
//
//	template<typename T>
//	inline bool Quaternion<T>::operator==(const Quaternion & aQuaternion)
//	{
//		return myAxis == aQuaternion.myAxis && myReal == aQuaternion.myReal;
//	}
//
//	template<typename T>
//	inline bool Quaternion<T>::operator!=(const Quaternion & aQuaternion)
//	{
//		return !(*this == aQuaternion);
//	}
//
//	template<typename T>
//	inline T Quaternion<T>::Dot(const Quaternion & aQuaternion)
//	{			
//		return myAxis.Dot(aQuaternion.myAxis) + myReal * aQuaternion.myReal;
//	}
//
//	template<typename T>
//	inline Quaternion<T> Quaternion<T>::GetConjugated() const
//	{
//		Quaternion quaternion;
//
//		quaternion.myAxis = static_cast<T>(-1) * myAxis;
//		quaternion.myReal = myReal;
//
//		return quaternion;
//	}
//
//	template<typename T>
//	inline T Quaternion<T>::GetNorm() const
//	{
//		return sqrt(myAxis.x * myAxis.x + myAxis.y * myAxis.y + myAxis.z * myAxis.z + myReal * myReal);
//	}
//
//	template<typename T>
//	inline T Quaternion<T>::GetNorm2() const
//	{
//		return myAxis.x * myAxis.x + myAxis.y * myAxis.y + myAxis.z * myAxis.z + myReal * myReal;
//	}
//
//	template<typename T>
//	inline Quaternion<T> Quaternion<T>::GetInverse() const
//	{
//		return GetConjugated();
//	}
//
//	template<typename T>
//	inline Quaternion<T> Quaternion<T>::GetNormalized() const
//	{
//		Quaternion<T> quat(*this);
//		quat.Normalize();
//		return quat;
//	}
//
//	template<typename T>
//	inline void Quaternion<T>::Normalize()
//	{
//		T openAirQuotesLengthCloseAirQuotes = GetNorm();
//		myAxis /= openAirQuotesLengthCloseAirQuotes;
//		myReal /= openAirQuotesLengthCloseAirQuotes;
//	}
//
//	template<typename T>
//	inline Vector3<T> Quaternion<T>::GetLog() const
//	{
//		T lensQr = myAxis.Length2();
//		if (lensQr > 0.0f)
//		{
//			T length = sqrt(lensQr);
//			T angle = atan2(length, myReal) / length;
//			return myAxis * angle;
//		}
//
//		return CU::Vector3<T>(0);
//	}
//
//	template<typename T>
//	inline Quaternion<T> Quaternion<T>::Pow(T aScalar)
//	{
//		T angle = static_cast<T>(2) * GetHalfUnitAngle();
//		return Quaternion(sin(angle*aScalar) * myAxis, cos(angle * aScalar));
//	}
//
//	template<typename T>
//	inline Quaternion<T> Quaternion<T>::CreateRotationQuaternion(const Vector3<T>& aAxis, T aAngle)
//	{	
//		return Quaternion(sin(aAngle / static_cast<T>(2)) * aAxis, cos(aAngle / static_cast<T>(2)));
//	}
//
//	template<typename T>
//	inline Quaternion<T> Quaternion<T>::CreateRotationXYZ(const Vector3<T>& aRotation)
//	{
//		Quaternion<T> q;
//		q.SetRotationXYZ(aRotation);
//		return q;
//	}
//
//	template<typename T>
//	inline Quaternion<T> Quaternion<T>::CreateRotationX(T aRadian)
//	{
//		Quaternion<T> q;
//		q.SetRotationX(aRadian);
//		return q;
//	}
//
//	template<typename T>
//	inline Quaternion<T> Quaternion<T>::CreateRotationY(T aRadian)
//	{
//		Quaternion<T> q;
//		q.SetRotationY(aRadian);
//		return q;
//	}
//
//	template<typename T>
//	inline Quaternion<T> Quaternion<T>::CreateRotationZ(T aRadian)
//	{
//		Quaternion<T> q;
//		q.SetRotationZ(aRadian);
//		return q;
//	}
//
//	template<typename T>
//	inline Quaternion<T> Quaternion<T>::Slerp(const Quaternion & aFrom, const Quaternion & aTo, T aTime)
//	{
//		//Gucci Good Graphical Representations
//
//		T cosAngle = aFrom.myAxis.x * aTo.myAxis.x + aFrom.myAxis.y * aTo.myAxis.y + aFrom.myAxis.z * aTo.myAxis.z + aFrom.myReal * aTo.myReal;
//		cosAngle = (cosAngle < static_cast<T>(-1)) ? static_cast<T>(-1) : (cosAngle > static_cast<T>(1)) ? static_cast<T>(1) : cosAngle;
//		T angle = acos(cosAngle);
//		if (angle == 0)
//		{
//			return aTo;
//		}
//		
//		Quaternion<T> result = (aFrom * static_cast<T>(sin(angle * (1 - aTime)) / sin(angle))) + (aTo * static_cast<T>((sin(angle*aTime) / sin(angle))));
//		result.Normalize();
//
//		return result;
//	}
//
//	template<typename T>
//	inline Matrix44<T> Quaternion<T>::GetMatrix() const
//	{
//		Matrix44<T> matrix;
//
//// 		Vector3<T> v2 = myAxis + myAxis;
//// 		T xx = 1 - v2.x * myAxis.x;
//// 		T yy = v2.y * myAxis.y;
//// 		T xw = v2.x * myReal;
//// 		T xy = v2.y * myAxis.x;
//// 		T yz = v2.z * myAxis.y;
//// 		T yw = v2.y * myReal;
//// 		T xz = v2.z * myAxis.x;
//// 		T zz = v2.z * myAxis.z;
//// 		T zw = v2.z * myReal;
//// 		matrix[0] = 1 - yy - zz;
//// 		matrix[1] = xy - zw;
//// 		matrix[2] = xz + yw;
//// 		matrix[4] = xy + zw;
//// 		matrix[5] = xx - zz;
//// 		matrix[6] = yz - xw;
//// 		matrix[8] = xz - yw;
//// 		matrix[9] = yz + xw;
//// 		matrix[10] = xx - yy;
//
//		float wx, wy, wz, xx, yy, yz, xy, xz, zz, x2, y2, z2;
//
//		// calculate coefficients
//		x2 = myAxis.x + myAxis.x;
//		y2 = myAxis.y + myAxis.y;
//		z2 = myAxis.z + myAxis.z;
//
//		xx = myAxis.x * x2;
//		xy = myAxis.x * y2;
//		xz = myAxis.x * z2;
//
//		yy = myAxis.y * y2;
//		yz = myAxis.y * z2;
//
//		zz = myAxis.z * z2;
//
//		wx = myReal * x2;
//		wy = myReal * y2;
//		wz = myReal * z2;
//
//		matrix[0] = 1.0f - (yy + zz);
//		matrix[1] = xy - wz;
//		matrix[2] = xz + wy;
//		matrix[3] = 0.0f;
//
//		matrix[4] = xy + wz;
//		matrix[5] = 1.0f - (xx + zz);
//		matrix[6] = yz - wx;
//		matrix[7] = 0.0f;
//
//
//		matrix[8] = xz - wy;
//		matrix[9] = yz + wx;
//		matrix[10] = 1.0f - (xx + yy);
//		matrix[11] = 0.0f;
//
//
//		matrix[12] = 0.f;
//		matrix[13] = 0.f;
//		matrix[14] = 0.f;
//		matrix[15] = 1.f;
//
//		return matrix;
//	}
//
//	template<typename T>
//	inline void Quaternion<T>::SetRotationX(T aRadian)
//	{
//		T sinAngle, cosAngle;
//
//		cosAngle = cos(aRadian * static_cast<T>(0.5));
//		sinAngle = sin(aRadian * static_cast<T>(0.5));
//
//		myReal = cosAngle;
//		myAxis.x = sinAngle;
//		myAxis.y = 0;
//		myAxis.z = 0;
//	}
//
//	template<typename T>
//	inline void Quaternion<T>::SetRotationY(T aRadian)
//	{
//		T sinAngle, cosAngle;
//
//		cosAngle = cos(aRadian * static_cast<T>(0.5));
//		sinAngle = sin(aRadian * static_cast<T>(0.5));
//
//		myReal = cosAngle;
//		myAxis.x = 0;
//		myAxis.y = sinAngle;
//		myAxis.z = 0;
//	}
//
//	template<typename T>
//	inline void Quaternion<T>::SetRotationZ(T aRadian)
//	{
//		T sinAngle, cosAngle;
//
//		cosAngle = cos(aRadian * static_cast<T>(0.5));
//		sinAngle = sin(aRadian * static_cast<T>(0.5));
//
//		myReal = cosAngle;
//		myAxis.x = 0;
//		myAxis.y = 0;
//		myAxis.z = sinAngle;
//	}
//
//	template<typename T>
//	inline void Quaternion<T>::SetRotationXYZ(const Vector3<T> & aRotation)
//	{
//		T sinX = sin(aRotation.x * static_cast<T>(0.5));
//		T cosX = cos(aRotation.x * static_cast<T>(0.5));
//		T sinY = sin(aRotation.y * static_cast<T>(0.5));
//		T cosY = cos(aRotation.y * static_cast<T>(0.5));
//		T sinZ = sin(aRotation.z * static_cast<T>(0.5));
//		T cosZ = cos(aRotation.z * static_cast<T>(0.5));
//
//		T c = cosX;
//		T d = cosY;
//		T e = cosZ;
//		T f = sinX;
//		T g = sinY;
//		T h = sinZ;
//
//		myReal = c * d * e + f * g * h;
//		myAxis.x = f * d * e + c * g * h;
//		myAxis.y = c * g * e + f * d * h;
//		myAxis.z = c * d * h + f * g * e;
//
//		//YZX Order - quat calculator
//		//Up works
//		//Right gives same X
//		//myReal = c * d * e - f * g * h;
//		//myAxis.x = f * d * e + c * g * h;
//		//myAxis.y = c * g * e + f * d * h;
//		//myAxis.z = c * d * h - f * g * e;
//
//		//YXZ Order - quat calculator
//		//Up works
//		//Look gives same Z
//		//myReal = c * d * e + f * g * h;
//		//myAxis.x = f * d * e + c * g * h;
//		//myAxis.y = c * g * e - f * d * h;
//		//myAxis.z = c * d * h - f * g * e;
//
//		//ZXY Order - quat calculator
//		//Look works
//		//Up gives same Y
//		//myReal = c * d * e - f * g * h;
//		//myAxis.x = f * d * e - c * g * h;
//		//myAxis.y = c * g * e + f * d * h;
//		//myAxis.z = c * d * h + f * g * e;
//
//		//XZY - quat calculator
//		//Right works
//		//Up gives same Y
//		//myReal = c * d * e + f * g * h;
//		//myAxis.x = f * d * e - c * g * h;
//		//myAxis.y = c * g * e - f * d * h;
//		//myAxis.z = c * d * h + f * g * e;
//
//		//ZYX Order - quat calculator
//		//Look works
//		//Right gives same X
//		//myReal = c * d * e + f * g * h;
//		//myAxis.x = f * d * e - c * g * h;
//		//myAxis.y = c * g * e + f * d * h;
//		//myAxis.z = c * d * h - f * g * e;
//
//		//XYZ Order - quat calculator
//		//Right works
//		//Look gives same Z
//		//myReal = c * d * e - f * g * h;
//		//myAxis.x = f * d * e + c * g * h;
//		//myAxis.y = c * g * e - f * d * h;
//		//myAxis.z = c * d * h + f * g * e;
//
//		//ZYX Order - Cry engine
//		//Look works
//		//Right gives same X
//		//myReal = cosX * cosY * cosZ + sinX * sinY * sinZ;
//		//myAxis.x = cosZ * cosY * sinX - sinZ * sinY * cosX;
//		//myAxis.y = cosZ * sinY * cosX + sinZ * cosY * sinX;
//		//myAxis.z = sinZ * cosY * cosX - cosZ * sinY * sinX;
//	}
//
//	template<typename T>
//	inline Vector3<T> Quaternion<T>::ToEuler() const
//	{
//		double sp = -2.0f * (myAxis.y * myAxis.z - myReal*myAxis.x);// Extract sinf(pitch)
//
//		double pitch, roll, yaw;
//
//		if (fabs(sp) > 0.9999f) {// Check for Gimbel lock, giving slight tolerance for numerical imprecision
//			pitch = (3.141592f / 2.f) * sp;// Looking straight up or down
//			yaw = atan2(-myAxis.x*myAxis.z + myReal*myAxis.y, 0.5f - myAxis.y*myAxis.y - myAxis.z*myAxis.z);// Compute heading, slam roll to zero
//			roll = 0.0f;
//		}
//		else {// Compute angles.  We don'THICC have to use the "safe" asin function because we already checked for range errors when checking for Gimbel lock
//			pitch = asinf(sp);
//			yaw = atan2(myAxis.x*myAxis.z + myReal * myAxis.y, 0.5f - myAxis.x*myAxis.x - myAxis.y*myAxis.y);
//			roll = atan2(myAxis.x*myAxis.y + myReal*myAxis.z, 0.5f - myAxis.x*myAxis.x - myAxis.z*myAxis.z);
//		}
//
//		return Vector3f(static_cast<T>(pitch), static_cast<T>(yaw), static_cast<T>(roll));
//	}
//
//	template<typename T>
//	inline Vector3<T> Quaternion<T>::GetLook() const
//	{
//		return (*this) * Vector3<T>(0.0, 0.0, 1.0);
//	}
//
//	template<typename T>
//	inline Vector3<T> Quaternion<T>::GetUp() const
//	{
//		return (*this) * Vector3<T>(0.0, 1.0, 0.0);
//	}
//
//	template<typename T>
//	inline Vector3<T> Quaternion<T>::GetRight() const
//	{
//		return (*this) * Vector3<T>(1.0, 0.0, 0.0);
//	}
//
//	template<typename T>
//	inline void Quaternion<T>::RotateLocalX(T aRotationAmount)
//	{
//		//(*this) = (*this) * (CreateRotationQuaternion((*this) * GetRight(), aRotationAmount)).GetNormalized();
//		(*this) = (CreateRotationQuaternion((*this) * GetRight(), aRotationAmount)).GetNormalized() * (*this);
//	}
//
//	template<typename T>
//	inline void Quaternion<T>::RotateLocalY(T aRotationAmount)
//	{
//		//(*this) = (*this) * (CreateRotationQuaternion((*this) * GetUp(), aRotationAmount)).GetNormalized();
//		(*this) = (CreateRotationQuaternion((*this) * GetUp(), aRotationAmount)).GetNormalized() * (*this);
//	}
//
//	template<typename T>
//	inline void Quaternion<T>::RotateLocalZ(T aRotationAmount)
//	{
//		//(*this) = (*this) * (CreateRotationQuaternion((*this) * GetLook(), aRotationAmount)).GetNormalized();
//		(*this) = (CreateRotationQuaternion((*this) * GetLook(), aRotationAmount)).GetNormalized() * (*this);
//	}
//
//	template<typename T>
//	inline bool Quaternion<T>::IsUnitQuaternion() const
//	{
//		return std::abs(GetNorm2()) - 1.0f < 0.025f;
//	}
//
//	template<typename T>
//	inline T Quaternion<T>::GetHalfUnitAngle() const
//	{
//		return asin(myAxis.Length());
//	}
//
//
//	typedef Quaternion<float> QuaternionF;
//}