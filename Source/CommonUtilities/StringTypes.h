#pragma once

namespace CommonUtilities
{
	namespace String
	{
		using HashType = unsigned long long;

		constexpr HashType HashTypeINVALID = static_cast<HashType>(-1);
	}
}
