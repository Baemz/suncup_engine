#pragma once
#include "PlaneVolume.h"
#include "Matrix.h"
#include "GrowingArray.h"
#include "CommonMath.h"
#include "Matrix44.h"

namespace CommonUtilities
{
	namespace Intersection
	{
		class FovFrustum
		{
		public:
			FovFrustum(float aNear, float aFar, float aFov);
			bool Inside(const Vector3<float>& aPos, float aRadius) const;

			void OnResize(float aNearPlane, float aFarPlane);

			CU::PlaneVolume<float> myVolume;
			float myNear;
			float myFar;
		};
	}

	inline Intersection::FovFrustum::FovFrustum(float aNear, float aFar, float aFov)
	{
		myNear = aNear;
		myFar = aFar;
		float angle = CU::ToRadians(aFov / 2.f);

		Plane<float> nearPlane(Vector3<float>(0, 0, aNear), Vector3<float>(0, 0, -1));
		Plane<float> farPlane(Vector3<float>(0, 0, aFar), Vector3<float>(0, 0, 1));
		Plane<float> right(Vector3<float>(0, 0, 0), Vector3<float>(angle, 0, -angle));
		Plane<float> left(Vector3<float>(0, 0, 0), Vector3<float>(-angle, 0, -angle));
		Plane<float> up(Vector3<float>(0, 0, 0), Vector3<float>(0, angle, -angle));
		Plane<float> down(Vector3<float>(0, 0, 0), Vector3<float>(0, -angle, -angle));

		nearPlane.Normalize();
		farPlane.Normalize();
		right.Normalize();
		left.Normalize();
		up.Normalize();
		down.Normalize();

		myVolume.AddPlane(nearPlane);
		myVolume.AddPlane(farPlane);
		myVolume.AddPlane(right);
		myVolume.AddPlane(left);
		myVolume.AddPlane(up);
		myVolume.AddPlane(down);
	}

	inline bool CommonUtilities::Intersection::FovFrustum::Inside(const Vector3<float>& aPos, float aRadius) const
	{
		return myVolume.Inside(aPos, aRadius);
	}

	inline void CommonUtilities::Intersection::FovFrustum::OnResize(float aNearPlane, float aFarPlane)
	{
		myVolume.Clear();
		myNear = aNearPlane;
		myFar = aFarPlane;
		float rotated45 = sqrt(2.f) / 2.f;

		Plane<float> nearPlane(Vector3<float>(0, 0, aNearPlane), Vector3<float>(0, 0, -1));
		Plane<float> farPlane(Vector3<float>(0, 0, aFarPlane), Vector3<float>(0, 0, 1));
		Plane<float> right(Vector3<float>(0, 0, 0), Vector3<float>(rotated45, 0, -rotated45));
		Plane<float> left(Vector3<float>(0, 0, 0), Vector3<float>(-rotated45, 0, -rotated45));
		Plane<float> up(Vector3<float>(0, 0, 0), Vector3<float>(0, rotated45, -rotated45));
		Plane<float> down(Vector3<float>(0, 0, 0), Vector3<float>(0, -rotated45, -rotated45));

		nearPlane.Normalize();
		farPlane.Normalize();
		right.Normalize();
		left.Normalize();
		up.Normalize();
		down.Normalize();

		myVolume.AddPlane(nearPlane);
		myVolume.AddPlane(farPlane);
		myVolume.AddPlane(right);
		myVolume.AddPlane(left);
		myVolume.AddPlane(up);
		myVolume.AddPlane(down);
	}
}
namespace CU = CommonUtilities;