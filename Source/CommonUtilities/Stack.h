#pragma once
#include "GrowingArray.h"
#include <assert.h>

namespace CommonUtilities
{
	template <class T>
	class Stack
	{
	public:
		//Skapar en tom stack
		Stack();

		//Returnerar antal element i stacken
		int GetSize() const;

		//Returnerar det �versta elementet i stacken. Kraschar med en assert om
		//stacken �r tom.
		const T& GetTop() const;

		//Returnerar det �versta elementet i stacken. Kraschar med en assert om
		//stacken �r tom.
		T& GetTop();

		//L�gger in ett nytt element �verst p� stacken
		void Push(const T &aValue);

		//Tar bort det �versta elementet fr�n stacken och returnerar det. Kraschar
		//med en assert om stacken �r tom.
		T Pop();

	private:
		GrowingArray<T> myData;
	};

	template<class T>
	inline Stack<T>::Stack()
	{
		myData.Init(10);
	}

	template<class T>
	inline int Stack<T>::GetSize() const
	{
		return myData.Size();
	}

	template<class T>
	inline const T& Stack<T>::GetTop() const
	{
		assert(myData.Size() > 0 && "Stack is empty!");
		return myData.GetLast();
	}

	template<class T>
	inline T& Stack<T>::GetTop()
	{
		assert(myData.Size() > 0 && "Stack is empty!");
		return myData.GetLast();
	}

	template<class T>
	inline void Stack<T>::Push(const T& aValue)
	{
		myData.Add(aValue);
	}

	template<class T>
	inline T Stack<T>::Pop()
	{
		assert(myData.Size() > 0 && "Stack is empty!");
		T element = myData.GetLast();
		myData.RemoveAtIndex(myData.Size() - 1);
		return element;
	}
}
namespace CU = CommonUtilities;
