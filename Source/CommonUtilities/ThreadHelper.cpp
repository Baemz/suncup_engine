#include "ThreadHelper.h"
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <sstream>

namespace CommonUtilities
{
	namespace ThreadHelper
	{
		//  
		// Usage: SetThreadName ((DWORD)-1, "MainThread");  
		//  
		static const DWORD MS_VC_EXCEPTION = 0x406D1388;
#pragma pack(push,8)  
		typedef struct tagTHREADNAME_INFO
		{
			DWORD dwType; // Must be 0x1000.  
			LPCSTR szName; // Pointer to name (in user addr space).  
			DWORD dwThreadID; // Thread ID (-1=caller thread).  
			DWORD dwFlags; // Reserved for future use, must be zero.  
		} THREADNAME_INFO;
#pragma pack(pop)  
		static void SetThreadNameWindowsFunction(DWORD dwThreadID, const char* threadName) {
			THREADNAME_INFO info;
			info.dwType = 0x1000;
			info.szName = threadName;
			info.dwThreadID = dwThreadID;
			info.dwFlags = 0;
#pragma warning(push)  
#pragma warning(disable: 6320 6322)  
			__try {
				RaiseException(MS_VC_EXCEPTION, 0, sizeof(info) / sizeof(ULONG_PTR), (ULONG_PTR*)&info);
			}
			__except (EXCEPTION_EXECUTE_HANDLER) {
			}
#pragma warning(pop)  
		}

		void SetThreadName(const std::string& aNewThreadName)
		{
			SetThreadNameWindowsFunction(GetCurrentThreadId(), aNewThreadName.c_str());
		}

		void SetThreadName(const std::thread::id& aThreadID, const std::string& aNewThreadName)
		{
			std::stringstream threadId;
			threadId << aThreadID;
			SetThreadNameWindowsFunction(std::stoul(threadId.str()), aNewThreadName.c_str());
		}

		bool SetThreadPriority(const EThreadPriority& aThreadPriority, std::string& aErrorMessage)
		{
			return SetThreadPriority(GetCurrentThread(), aThreadPriority, aErrorMessage);
		}

		bool SetThreadPriority(void* aThreadHandle, const EThreadPriority& aThreadPriority, std::string& aErrorMessage)
		{
			assert("Tried to set thread priority on thread handle that is nullptr" && (aThreadHandle != nullptr));

			static_assert((static_cast<std::size_t>(EThreadPriority::SIZE) == 5), "Thread priority enum size changed, fix the mapping too");
			static constexpr int priorityMapping[static_cast<std::size_t>(EThreadPriority::SIZE)]
			{
				-2,
				-1,
				0,
				1,
				2
			};

			// Windows thread priority function, in global namespace
			BOOL result(::SetThreadPriority(reinterpret_cast<HANDLE>(aThreadHandle), priorityMapping[static_cast<std::size_t>(aThreadPriority)]));

			if (result == FALSE)
			{
				aErrorMessage = "";

				int error(GetLastError());
				char buffer[512];
				if (FormatMessageA(FORMAT_MESSAGE_IGNORE_INSERTS | FORMAT_MESSAGE_FROM_SYSTEM,
					nullptr, error, 0, buffer, sizeof(buffer) / sizeof(char), nullptr) == 0)
				{
					aErrorMessage += "Failed to get error message";
				}
				else
				{
					aErrorMessage += buffer;
				}

				return false;
			}

			return true;
		}

		bool GetThreadPriority(EThreadPriority& aThreadPriority, std::string& aErrorMessage)
		{
			return GetThreadPriority(GetCurrentThread(), aThreadPriority, aErrorMessage);
		}

		bool GetThreadPriority(void* aThreadHandle, EThreadPriority& aThreadPriority, std::string& aErrorMessage)
		{
			assert("Tried to set thread priority on thread handle that is nullptr" && (aThreadHandle != nullptr));

			static_assert((static_cast<std::size_t>(EThreadPriority::SIZE) == 5), "Thread priority enum size changed, fix the mapping too");
			static constexpr int priorityMapping[static_cast<std::size_t>(EThreadPriority::SIZE)]
			{
				-2,
				-1,
				0,
				1,
				2
			};

			// Windows thread priority function, in global namespace
			const int threadPriority(::GetThreadPriority(reinterpret_cast<HANDLE>(aThreadHandle)));

			if (threadPriority == THREAD_PRIORITY_ERROR_RETURN)
			{
				aErrorMessage = "";

				int error(GetLastError());
				char buffer[512];
				if (FormatMessageA(FORMAT_MESSAGE_IGNORE_INSERTS | FORMAT_MESSAGE_FROM_SYSTEM,
					nullptr, error, 0, buffer, sizeof(buffer) / sizeof(char), nullptr) == 0)
				{
					aErrorMessage += "Failed to get error message";
				}
				else
				{
					aErrorMessage += buffer;
				}

				return false;
			}

			int foundValue(static_cast<std::size_t>(EThreadPriority::SIZE));
			for (auto mappingIndex(0); mappingIndex < static_cast<std::size_t>(EThreadPriority::SIZE); ++mappingIndex)
			{
				if (priorityMapping[mappingIndex] == threadPriority)
				{
					foundValue = mappingIndex;
				}
			}

			if (foundValue != static_cast<std::size_t>(EThreadPriority::SIZE))
			{
				aThreadPriority = static_cast<EThreadPriority>(foundValue);
			}
			else
			{
				assert("Priority not mapped, add it");
				return false;
			}

			return true;
		}

		void CFramerateLimiter::Init(const float aMaxFPS, const float aMinFPSForSlowmotion)
		{
			assert("No negative values are allowed in CFramerateLimiter::Init" && (aMaxFPS >= 0.0f));

			myThreadTimer.Update();

			if (aMaxFPS == 0.0f)
			{
				myMaxSeconds = 0.0f;
			}
			else
			{
				myMaxSeconds = (1.0f / aMaxFPS);
			}

			if (aMinFPSForSlowmotion == 0.0f)
			{
				myMinSecondsForSlowmotion = 0.0f;
			}
			else
			{
				myMinSecondsForSlowmotion = (1.0f / aMinFPSForSlowmotion);
			}
		}

		float CFramerateLimiter::Update(const float aMaxFPS, const float aMinFPSForSlowmotion)
		{
			myThreadTimer.Update();

			if (aMaxFPS == 0.0f)
			{
				myMaxSeconds = 0.0f;
			}
			else if (aMaxFPS > 0.0f)
			{
				myMaxSeconds = (1.0f / aMaxFPS);
			}

			if (aMinFPSForSlowmotion == 0.0f)
			{
				myMinSecondsForSlowmotion = 0.0f;
			}
			else if (aMinFPSForSlowmotion > 0.0f)
			{
				myMinSecondsForSlowmotion = (1.0f / aMinFPSForSlowmotion);
			}

			float deltaTime(myThreadTimer.GetDeltaTime());

			if (0.0f < myMinSecondsForSlowmotion)
			{
				if (deltaTime > myMinSecondsForSlowmotion)
				{
					deltaTime = myMinSecondsForSlowmotion;
				}
			}

			if (0.0f < myMaxSeconds)
			{
				if (deltaTime < myMaxSeconds)
				{
					std::this_thread::sleep_for(std::chrono::microseconds(static_cast<unsigned short>((myMaxSeconds - deltaTime) * 1000000.0f)));
					myThreadTimer.Update();
					deltaTime += myThreadTimer.GetDeltaTime();
				}
			}

			return deltaTime;
		}

		double CFramerateLimiter::GetTotalTime() const
		{
			return myThreadTimer.GetTotalTime();
		}

		float CFramerateLimiter::GetMaxFPS() const
		{
			return (1.0f / myMaxSeconds);
		}
		
		float CFramerateLimiter::GetMinFPSForSlowmotion() const
		{
			return (1.0f / myMinSecondsForSlowmotion);
		}
	}
}
