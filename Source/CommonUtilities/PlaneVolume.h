#pragma once
#include "Plane.h"
#include "Vector3.h"
#include "GrowingArray.h"

namespace CommonUtilities
{
	template<typename T>
	class PlaneVolume
	{
	public:
		PlaneVolume();
		PlaneVolume(const CU::GrowingArray<Plane<T>, unsigned int>& aPlaneList);
		~PlaneVolume();

		void InitVectorWithSize(const unsigned int aSize);
		void AddPlane(const Plane<T>& aPlane);
		bool Inside(const Vector3<T>& aPosition) const;
		bool Inside(Vector3<T> aPosition, T aRadius) const;
		void Clear();

		inline Plane<T>& operator[](const unsigned int& aIndex);

	private:
		CU::GrowingArray<Plane<T>, unsigned int> myPlaneList;
	};

	template<typename T>
	inline Plane<T>& PlaneVolume<T>::operator[](const unsigned int& aIndex)
	{
		return myPlaneList[aIndex];
	}

	template<typename T>
	inline PlaneVolume<T>::PlaneVolume()
	{
	}

	template<typename T>
	inline PlaneVolume<T>::PlaneVolume(const CU::GrowingArray<Plane<T>, unsigned int>& aPlaneList)
	{
		myPlaneList = aPlaneList;
	}

	template<typename T>
	inline PlaneVolume<T>::~PlaneVolume()
	{
	}

	template<typename T>
	inline void PlaneVolume<T>::InitVectorWithSize(const unsigned int aSize)
	{
		myPlaneList.Resize(aSize);
	}

	template<typename T>
	inline void PlaneVolume<T>::AddPlane(const Plane<T>& aPlane)
	{
		myPlaneList.Add(aPlane);
	}

	template<typename T>
	inline bool PlaneVolume<T>::Inside(const Vector3<T>& aPosition) const
	{
		bool isInside = true;

		for (const auto& plane : myPlaneList)
		{
			if (!plane.Inside(aPosition))
			{
				isInside = false;
			}
		}

		return isInside;
	}

	template <typename T>
	bool PlaneVolume<T>::Inside(Vector3<T> aPosition, T aRadius) const
	{
		bool result = true;
		for (const auto& plane : myPlaneList)
		{
			if (plane.ClassifySpherePlane(aPosition, aRadius) > 0)
			{
				result= false;
			}
		}
		return result;
	}

	template<typename T>
	inline void PlaneVolume<T>::Clear()
	{
		myPlaneList.clear();
	}

}
namespace CU = CommonUtilities;