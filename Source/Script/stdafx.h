#pragma once

// STD-includes
#include <string>
#include <functional>
#include <cassert>

#include "..\EngineCore\MemoryPool\MemoryPool.h"

// CommonUtilities-includes
#include "../CommonUtilities/GrowingArray.h"
#include "../CommonUtilities/KeyBinds.h"
#include "..\CommonUtilities\CommonMath.h"

//For logging purposes
#include "../EngineCore/Logger/Logger.h"
