#include "stdafx.h"
#include "ScriptManager.h"
#include "..\EngineCore\FileWatcher\FileWatcherWrapper.h"
#include "..\CommonUtilities\ThreadHelper.h"
#include <future>
#include "..\CommonUtilities\Timer.h"

#include "..\CommonUtilities\LevenshteinDistance.h"
#include "..\CommonUtilities\Macros.h"

#ifdef max
#pragma push_macro("max")
#undef max
#define MAX_UNDEFINED
#endif // max

#ifdef GetObject
#undef GetObject
#endif // GetObject

CScriptManager* CScriptManager::ourInstance = nullptr;

void CScriptManager::Create()
{
	assert("ScriptManager already created!" && (ourInstance == nullptr));

	if (ourInstance == nullptr)
	{
		ourInstance = sce_new(CScriptManager);
	}
}

void CScriptManager::Destroy()
{
	sce_delete(ourInstance);
}

bool CScriptManager::Init(const std::function<bool(CScriptManager& aScriptManager)>& aRegisterFunctionsFunction, const std::string& aFileName, StateIndexType& aStateIndexToSet, const OnReloadCallback& aReloadCallback)
{
	myCallLuaFromCPPLuaState = luaL_newstate();
	bool successfullyInititated(true);

	if (myCallLuaFromCPPLuaState == nullptr)
	{
		RESOURCE_LOG("[ScriptManager] ERROR! Couldn't create C++ -> Lua state, for the console");
		successfullyInititated = false;
	}

#ifndef _RETAIL
	std::ofstream outputFile(ourDocumentationFilePath, std::ofstream::out | std::ofstream::trunc);
	if (outputFile.good() == false)
	{
		RESOURCE_LOG("[ScriptManager] ERROR! Couldn't open the documentation file \"%s\""
			, ourDocumentationFilePath);

		successfullyInititated = false;
	}
	outputFile.close();
#endif
	luaL_openlibs(myCallLuaFromCPPLuaState);

	if (NewState(aFileName, aStateIndexToSet, aReloadCallback))
	{
		std::shared_lock<std::shared_mutex> sharedLock(myLuaStatesMutex);
		auto& newLuaState(myLuaStates.GetObject(aStateIndexToSet));
		sharedLock.unlock();

		if ((aRegisterFunctionsFunction == nullptr)
			|| (aRegisterFunctionsFunction(*this) == false))
		{
			newLuaState.myStateIsValid = false;
			successfullyInititated = false;
		}
	}
	else
	{
		RESOURCE_LOG("[ScriptManager] ERROR! Init failed to create default lua state");
		successfullyInititated = false;
	}

	constexpr decltype(myQueuedCallsToScriptWrite)::size_t initialFunctionBuffersSize(256);
	myQueuedCallsToScriptWrite.ReInit(initialFunctionBuffersSize);
	myQueuedCallsToScriptRead.ReInit(initialFunctionBuffersSize);

	myForceQuitThread = false;
	myUpdateQueuedCallsThread = std::thread(&CScriptManager::UpdateQueuedCallsThread, this);
	std::string threadPriorityErrorMessage;
	if (CU::ThreadHelper::SetThreadPriority(myUpdateQueuedCallsThread, CU::ThreadHelper::EThreadPriority::High, threadPriorityErrorMessage) == false)
	{
		ENGINE_LOG("ERROR! Failed to set script manager thread priority. Error: %s"
			, threadPriorityErrorMessage.c_str());
	}
	CU::ThreadHelper::SetThreadName(myUpdateQueuedCallsThread, "ScriptManager");

	return successfullyInititated;
}

bool CScriptManager::NewState(const std::string& aFileToOpen, StateIndexType& aStateIndexToSet, const OnReloadCallback& aReloadCallback)
{
	std::unique_lock<std::shared_mutex> uniqueLock(myLuaStatesMutex);
	aStateIndexToSet = myLuaStates.Add(SLuaState());
	auto& luaState(myLuaStates.GetObject(aStateIndexToSet));
	luaState.myReloadCallback = aReloadCallback;
	uniqueLock.unlock();

	if (InitState(aFileToOpen, luaState, aStateIndexToSet) == false)
	{
		// 		std::unique_lock<std::shared_mutex> removeStateLock(myLuaStatesMutex);
		// 		myLuaStates.Remove(aStateIndexToSet);
		// 		removeStateLock.unlock();
		return false;
	}

	return true;
}

bool CScriptManager::RemoveState(StateIndexType& aStateIndex)
{
	if (myLuaStates.IDIsUsed(aStateIndex) == false)
	{
		return false;
	}

	std::shared_lock<std::shared_mutex> sharedLock(myLuaStatesMutex);
	auto& luaState(myLuaStates.GetObject(aStateIndex));
	sharedLock.unlock();

	std::unique_lock<std::shared_mutex> uniqueLockStates(myLuaStatesMutex, std::defer_lock);
	std::unique_lock<std::mutex> uniqueLockRemoveState(luaState.myMutex, std::defer_lock);
	std::lock(uniqueLockStates, uniqueLockRemoveState);

	if (myLuaStates.IDIsUsed(aStateIndex) == false)
	{
		return false;
	}

	luaState.myForceQuitLuaCall = true;

	if (luaState.myIsAddedToFilewatch == true)
	{
		for (const auto& filePath : luaState.myFilePaths)
		{
			sce::CFileWatcherWrapper::RemoveFileToWatch(&luaState, filePath.c_str(), &CScriptManager::LoadedFileChanged);
		}
		luaState.myFilePaths.RemoveAll();
	}

	lua_close(luaState.myLuaState);
	luaState = nullptr;

	if (myLuaStates.Remove(aStateIndex) == false)
	{
		return false;
	}

	aStateIndex = InvalidStateIndex;

	return true;
}

void CScriptManager::QueueRemoveState(StateIndexType& aStateIndex)
{
	const StateIndexType stateIndexToQueue(aStateIndex);
	aStateIndex = InvalidStateIndex;

	std::unique_lock<std::mutex> queueRemoveMutex(myQueueRemoveStatesMutex);
	myQueuedStatesToRemove.Add(stateIndexToQueue);
}

void CScriptManager::WaitUntilDone()
{
	std::string errorMessage;
	CU::ThreadHelper::EThreadPriority priorityBefore;
	CU::ThreadHelper::GetThreadPriority(priorityBefore, errorMessage);
	CU::ThreadHelper::SetThreadPriority(CU::ThreadHelper::EThreadPriority::High, errorMessage);

	std::unique_lock<std::mutex> queueLg(myQueueCallsToScriptMutex);
	while ((myAnyScriptsQueued == true) || (myQueuedCallsToScriptWrite.Empty() == false))
	{
		queueLg.unlock();
		std::this_thread::yield();
		queueLg.lock();
	}
	queueLg.unlock();

	CU::ThreadHelper::SetThreadPriority(priorityBefore, errorMessage);

	std::unique_lock<std::mutex> removeStatesLg(myQueueRemoveStatesMutex);
	for (auto& currentState : myQueuedStatesToRemove)
	{
		if (RemoveState(currentState) == false)
		{
			GAME_LOG("ERROR! Failed to remove state(%u) from ScriptManager!"
				, currentState);
		}
	}
	myQueuedStatesToRemove.RemoveAll();
	removeStatesLg.unlock();
}

auto CScriptManager::GetArgsAsTuple(const std::string & aFunctionName, bool& aFoundFunction)
{
	aFunctionName;
	aFoundFunction;
	//Implement something similar to Apply, later on.

// 	auto functionIterator = std::find_if(myRegisteredFunctions.begin(), myRegisteredFunctions.end(), [&aFunctionName](SScriptFunctionCallableBase* aFunction)
// 	{
// 		if (aFunction->myAlsoExposeToConsole)
// 		{
// 			if (aFunctionName == aFunction->myFunctionName)
// 			{
// 				return true;
// 			}
// 		}
// 		return false;
// 	});
// 
// 	if (functionIterator == myRegisteredFunctions.end())
// 	{
// 		aFoundFunction = false;
// 	}

	//functionIterator->second->myFunctionWrapper.

	//CU::GrowingArray<
	return std::tuple<float>();
}

#define GET_REGISTERED_FUNCTIONS(aCondition)											\
CU::GrowingArray<std::string, std::size_t> functionNames(myRegisteredFunctions.size());	\
																						\
for (const auto& functionPair : myRegisteredFunctions)									\
{																						\
	if (aCondition)																		\
	{																					\
		functionNames.Add(functionPair.first);											\
	}																					\
}																						\
return functionNames

CU::GrowingArray<std::string, std::size_t> CScriptManager::GetRegisteredFunctionNamesAll() const
{
	GET_REGISTERED_FUNCTIONS(true);
}

CU::GrowingArray<std::string, std::size_t> CScriptManager::GetRegisteredFunctionNamesOnlyConsole() const
{
	GET_REGISTERED_FUNCTIONS(functionPair.second->myAlsoExposeToConsole == true);
}

CU::GrowingArray<std::string, std::size_t> CScriptManager::GetRegisteredFunctionNamesOnlyNotConsole() const
{
	GET_REGISTERED_FUNCTIONS(functionPair.second->myAlsoExposeToConsole == false);
}

bool CScriptManager::CallCPPFunctionAsIfFromLuaSTRING(const std::string& aFullFunctionCall, SFunctionAsIfFromLuaResult& aResult)
{
	const int stackPositionBeforeCall(lua_gettop(myCallLuaFromCPPLuaState));

	if (CheckLUAError(*myCallLuaFromCPPLuaState, luaL_loadstring(myCallLuaFromCPPLuaState, aFullFunctionCall.c_str())) == false)
	{
		RESOURCE_LOG("[ScriptManager] ERROR! Failed to load C++ function as if lua");
		return false;
	}
	
	if (CheckLUAError(*myCallLuaFromCPPLuaState, lua_pcall(myCallLuaFromCPPLuaState, 0, LUA_MULTRET, 0)) == false)
	{
		RESOURCE_LOG("[ScriptManager] ERROR! Failed to call C++ function as if lua");
		return false;
	}

	const int stackPositionAfterCall(lua_gettop(myCallLuaFromCPPLuaState));
	
	int amountOfResults(stackPositionAfterCall - stackPositionBeforeCall);

	assert("CScriptManager::CallCPPFunctionAsIfFromLuaSTRING only supports 0 or 1 results from functions" && ((amountOfResults == 0) || (amountOfResults == 1)));
	amountOfResults = CLAMP(amountOfResults, 0, 1);

	for (int resultIndex(0); resultIndex < amountOfResults; ++resultIndex)
	{
		if (lua_isboolean(myCallLuaFromCPPLuaState, -1) == true)
		{
			aResult.myBool = !!lua_toboolean(myCallLuaFromCPPLuaState, -1);
			aResult.myType = SFunctionAsIfFromLuaResult::EType::Bool;
		}
		else if (lua_isnumber(myCallLuaFromCPPLuaState, -1) != 0)
		{
			aResult.myNumber = lua_tonumber(myCallLuaFromCPPLuaState, -1);
			aResult.myType = SFunctionAsIfFromLuaResult::EType::Number;
		}
		else if (lua_isstring(myCallLuaFromCPPLuaState, -1) != 0)
		{
			const char* const string(lua_tostring(myCallLuaFromCPPLuaState, -1));
			const std::size_t stringLength(strlen(string));
			if (stringLength < ARRAY_SIZE(aResult.myString))
			{
				assert("Too long string in CScriptManager::CallCPPFunctionAsIfFromLuaSTRING" && (false));
			}
			else
			{
				std::size_t charIndex(0);
				for (; charIndex < stringLength; ++charIndex)
				{
					aResult.myString[charIndex] = string[charIndex];
				}
				aResult.myString[charIndex] = '\0';
				aResult.myType = SFunctionAsIfFromLuaResult::EType::String;
			}
		}
		else if (lua_islightuserdata(myCallLuaFromCPPLuaState, -1) == true)
		{
			aResult.myPointer = lua_touserdata(myCallLuaFromCPPLuaState, -1);
			aResult.myType = SFunctionAsIfFromLuaResult::EType::Pointer;
		}
		else if (lua_isnil(myCallLuaFromCPPLuaState, -1))
		{
		}
		else
		{
			assert("Return type not supported in CScriptManager::CallCPPFunctionAsIfFromLuaSTRING" && (false));
		}

		lua_pop(myCallLuaFromCPPLuaState, -1);
	}

	return true;
}

int CScriptManager::FromLUA(lua_State* aState)
{
	SScriptFunctionCallableBase::SFunctionWrapper* function(reinterpret_cast<SScriptFunctionCallableBase::SFunctionWrapper*>(lua_touserdata(aState, lua_upvalueindex(1))));

	int amountReturned(0);
	if (function != nullptr)
	{
		amountReturned = (*function)(aState);
	}
	else
	{
		RESOURCE_LOG("[ScriptManager] ERROR! Nullptr function in FromLUA");
	}

	return amountReturned;
}

void CScriptManager::HookLua(lua_State* aState, lua_Debug* aLuaDebug)
{
	if (aLuaDebug->event == LUA_HOOKCOUNT)
	{
		const int getTableIndex1(lua_rawgeti(aState, LUA_REGISTRYINDEX, ourMagicTableIndex));
		const int getUserDataIndex(lua_rawgeti(aState, -1, ourMagicUserDataIndex));

		SLuaState* customLuaState(reinterpret_cast<SLuaState*>(lua_touserdata(aState, -1)));

		lua_pop(aState, 2);

		if (customLuaState == nullptr)
		{
			luaL_error(aState, "HOOK don't have a valid SLuaState* in REGISTRY");
			return;
		}

		if (customLuaState->myForceQuitLuaCall == true)
		{
			customLuaState->myForceQuitLuaCall = false;
			luaL_error(aState, "HOOK called for abort");
			return;
		}
	}
}

bool CScriptManager::CheckLUAError(lua_State& aState, int aResult)
{
	CScriptManager* scriptManager(CScriptManager::Get());

	if (scriptManager == nullptr)
	{
		return false;
	}

	return scriptManager->CheckLUAErrorImpl(aState, aResult);
}

bool CScriptManager::CheckLUAErrorImpl(lua_State & aState, int aResult)
{
	if (aResult != LUA_OK)
	{
		std::string message(lua_tostring(&aState, -1));
		std::string completeMessage("[ScriptManager] ERROR! ");
		lua_pop(&aState, 1);

		if (message.find("attempt to call") != completeMessage.npos)
		{
			constexpr const char* functionNamePrefixStringToSearchFor("(global '");

			std::string::size_type functionNameIndex(message.find(functionNamePrefixStringToSearchFor));
			if (functionNameIndex != message.npos)
			{
				functionNameIndex += sizeof(functionNamePrefixStringToSearchFor);

				const std::string calledName(message.substr((functionNameIndex + 1), (message.size() - 2 - functionNameIndex - 1)));

				std::string closestName("NOT FOUND");
				std::string::size_type leastDistance(InvalidStateIndex);
				for (const auto& functionPair : myRegisteredFunctions)
				{
					const std::string::size_type distance(CU::LevenshteinDistance(calledName, functionPair.first));
					if (distance < leastDistance)
					{
						leastDistance = distance;
						closestName = functionPair.first;
					}
				}

				const std::string::size_type afterFileIndex(functionNameIndex - 36);

				message = message.substr(0, afterFileIndex);
				message += "Couldn't find callable \"" + calledName + "\", did you mean \"" + closestName + "\" instead?";
			}
		}

		completeMessage += message;
		RESOURCE_LOG(completeMessage.c_str());

		return false;
	}

	return true;
}

bool CScriptManager::LoadedFileChanged(void* aLuaState, const char* aFileChangedName)
{
	if (ourInstance == nullptr)
	{
		return false;
	}

	SLuaState* luaStatePointer(reinterpret_cast<SLuaState*>(aLuaState));
	if (luaStatePointer == nullptr)
	{
		return false;
	}

	const std::string fileChangedName(aFileChangedName);
	bool validFilePath(false);
	for (const auto& filePath : luaStatePointer->myFilePaths)
	{
		if (filePath == fileChangedName)
		{
			validFilePath = true;
			break;
		}
	}

	if (validFilePath == false)
	{
		return false;
	}

	return ourInstance->LoadedFileChangedImpl(*luaStatePointer, luaStatePointer->myFilePaths[0].c_str());
}

bool CScriptManager::LoadedFileChangedImpl(SLuaState& aLuaState, const char* aFileChangedName)
{
	RESOURCE_LOG("[ScriptManager] File \"%s\" changed, reloading it"
		, aFileChangedName);

	std::unique_lock<std::mutex> lg(aLuaState.myMutex);

	if (aLuaState.IsValid())
	{
		lua_close(aLuaState.myLuaState);
	}

	bool stateChanged(InitState(aFileChangedName, aLuaState, aLuaState.myIndexInObjectPool));

	if (stateChanged == true)
	{
		aLuaState.myReloadCallback(aLuaState.myIndexInObjectPool);
	}

	RESOURCE_LOG("[ScriptManager] SUCCESS! File \"%s\" reloaded"
		, aFileChangedName);

	return true;
}

bool CScriptManager::InitState(const std::string& aFileToOpen, SLuaState& aState, StateIndexType& aStateIndexToSet)
{
	std::unique_lock<std::shared_mutex> uniqueLock(myLuaStatesMutex, std::defer_lock);

	aState.myIndexInObjectPool = aStateIndexToSet;

	aState.myLuaState = luaL_newstate();

	if (aState.myLuaState == nullptr)
	{
		uniqueLock.lock();
		myLuaStates.Remove(aStateIndexToSet);
		uniqueLock.unlock();
		aStateIndexToSet = InvalidStateIndex;
		return false;
	}

	aState.myStateIsValid = true;

	luaL_openlibs(aState.myLuaState);

	if (OpenFile(aStateIndexToSet, aFileToOpen.c_str()) == false)
	{
		lua_close(aState.myLuaState);
		aState = nullptr;
		return false;
	}

	lua_newtable(aState.myLuaState);

	const int luaTableRef(luaL_ref(aState.myLuaState, LUA_REGISTRYINDEX));
	assert("Failed to create table for SLuaState* in Lua" && (luaTableRef == ourMagicTableIndex));

	const int getTableIndex0(lua_rawgeti(aState.myLuaState, LUA_REGISTRYINDEX, ourMagicTableIndex));

	lua_pushlightuserdata(aState.myLuaState, &aState);

	const int luaUserDataRef(luaL_ref(aState.myLuaState, -2));
	assert("Failed to bind SLuaState* to table in Lua" && (luaUserDataRef == ourMagicUserDataIndex));

	lua_pop(aState.myLuaState, 1);

	constexpr int processorInstructionsCountBeforeCheckIfTimeToAbortScript(1000000 / 32);
	lua_sethook(aState.myLuaState, &CScriptManager::HookLua, LUA_MASKCOUNT, processorInstructionsCountBeforeCheckIfTimeToAbortScript);

	return true;
}

bool CScriptManager::OpenFile(const StateIndexType& aStateIndex, const std::string& aFileToOpen)
{
	std::shared_lock<std::shared_mutex> sharedLock(myLuaStatesMutex);
	auto& luaState(myLuaStates.GetObject(aStateIndex));
	sharedLock.unlock();

	if (luaState.myIsAddedToFilewatch == false)
	{
		luaState.myIsAddedToFilewatch = true;

#ifndef _RETAIL
		luaState.myFilePaths.Add(aFileToOpen);

		if (sce::CFileWatcherWrapper::AddFileToWatch(&luaState, aFileToOpen.c_str(), &CScriptManager::LoadedFileChanged, true) == false)
		{
			RESOURCE_LOG("[ScriptManager] ERROR! Failed to add file \"%s\" to file watcher"
				, aFileToOpen.c_str());
			luaState.myStateIsValid = false;
			return false;
		}

		std::ifstream file(aFileToOpen);
		if (file.good() == false)
		{
			RESOURCE_LOG("[ScriptManager] ERROR! File \"%s\" can't be opened for script includes check"
				, aFileToOpen.c_str());
			return false;
		}

		const std::string includeText("require \"");
		const std::string::size_type pathStartIndex(includeText.size());
		for (std::string currentLine; std::getline(file, currentLine);)
		{
			if ((currentLine.size() > pathStartIndex) && (currentLine.substr(0, pathStartIndex) == includeText))
			{
				const std::string::size_type pathEndIndex(currentLine.find_last_of('\"'));
				if (pathEndIndex != currentLine.npos)
				{
					std::string path(currentLine.substr(pathStartIndex, (pathEndIndex - pathStartIndex)));

					for (auto charIterator(path.begin()); charIterator != path.end(); ++charIterator)
					{
						if (*charIterator == '.')
						{
							*charIterator = '\\';
						}
						else if (*charIterator == '\\')
						{
							charIterator = path.erase(charIterator);
						}
					}

					path += ".lua";

					luaState.myFilePaths.Add(path);

					if (sce::CFileWatcherWrapper::AddFileToWatch(&luaState, path.c_str(), &CScriptManager::LoadedFileChanged, true) == false)
					{
						RESOURCE_LOG("[ScriptManager] ERROR! Failed to add included file \"%s\" to file watcher"
							, path.c_str());
						luaState.myStateIsValid = false;
						return false;
					}
				}
			}
		}
#endif // !_RETAIL
	}

	if (CheckLUAError(*luaState.myLuaState, luaL_loadfile(luaState.myLuaState, aFileToOpen.c_str())) == false)
	{
		RESOURCE_LOG("[ScriptManager] ERROR! File \"%s\" doesn't exist or isn't a valid lua file"
			, aFileToOpen.c_str());
		luaState.myStateIsValid = false;
		return false;
	}

	if (CheckLUAError(*luaState.myLuaState, lua_pcall(luaState.myLuaState, 0, LUA_MULTRET, 0)) == false)
	{
		RESOURCE_LOG("[ScriptManager] ERROR! File \"%s\" isn't a valid lua file"
			, aFileToOpen.c_str());
		luaState.myStateIsValid = false;
		return false;
	}

#ifndef _RETAIL
	{
		std::ifstream file(aFileToOpen);
		if (file.good() == false)
		{
			RESOURCE_LOG("[ScriptManager] ERROR! File \"%s\" can't be opened for line count check"
				, aFileToOpen.c_str());
			return false;
		}

		file.unsetf(std::ios_base::skipws);

		const unsigned long long amountOfLines(1 + std::count(std::istreambuf_iterator<char>(file), std::istreambuf_iterator<char>(), '\n'));

		if (amountOfLines > ourAmountOfLinesToWarnAbout)
		{
			RESOURCE_LOG("[ScriptManager] WARNING! File \"%s\" is above %llu amount of lines (%llu), please split to individual, smaller, files"
				, aFileToOpen.c_str(), ourAmountOfLinesToWarnAbout, amountOfLines);
		}
	}
#endif // !_RETAIL

	luaState.myStateIsValid = true;

	const auto size = myRegisteredFunctions.size();
	for (auto& functionPair : myRegisteredFunctions)
	{
		RegisterFunctionForState(luaState, *functionPair.second);
	}

	return true;
}

#define REGISTER_FUNCTION(aState)								\
lua_pushlightuserdata(aState, &aFunction.myFunctionWrapper);	\
lua_pushcclosure(aState, &CScriptManager::FromLUA, 1);			\
lua_setglobal(aState, aFunction.myFunctionName.c_str());

void CScriptManager::RegisterFunctionForState(SLuaState& aLuaState, SScriptFunctionCallableBase& aFunction)
{
	if (aLuaState.IsValid())
	{
		REGISTER_FUNCTION(aLuaState.myLuaState);
	}
}

void CScriptManager::RegisterFunctionForFakeState(SScriptFunctionCallableBase& aFunction)
{
	REGISTER_FUNCTION(myCallLuaFromCPPLuaState);
}

#undef REGISTER_FUNCTION

CScriptManager::CScriptManager()
	: myForceQuitThread(false)
	, myAnyScriptsQueued(false)
	, myCallLuaFromCPPLuaState(nullptr)
{
	myQueuedStatesToRemove.Reserve(8);
}

CScriptManager::~CScriptManager()
{
	myForceQuitThread = true;
	myUpdateQueuedCallsThread.join();

	DeleteFunctionsInBuffer(myQueuedCallsToScriptWrite);
	DeleteFunctionsInBuffer(myQueuedCallsToScriptRead);

	myQueuedCallsToScriptWrite.RemoveAll();
	myQueuedCallsToScriptRead.RemoveAll();

	std::unique_lock<std::shared_mutex> uniqueLock(myLuaStatesMutex);

	const auto& usedStatesIndices(myLuaStates.GetUsedIDs());
	for (const auto& currentStateIndex : usedStatesIndices)
	{
		auto& currentState(myLuaStates.GetObject(currentStateIndex));

		if (currentState.myLuaState != nullptr)
		{
			lua_close(currentState.myLuaState);
			currentState = nullptr;
		}
		currentState.myStateIsValid = false;
	}

	myLuaStates.Clear();
	uniqueLock.unlock();

	if (myCallLuaFromCPPLuaState != nullptr)
	{
		lua_close(myCallLuaFromCPPLuaState);
		myCallLuaFromCPPLuaState = nullptr;
	}

	const auto size = myRegisteredFunctions.size();
	for (auto& functionPair : myRegisteredFunctions)
	{
		sce_delete(functionPair.second);
	}
	myRegisteredFunctions.clear();
}

void CScriptManager::UpdateQueuedCallsThread()
{
	volatile bool functionResult(false);
	volatile bool functionReady(false);
	SCallScriptBase* functionReadyPointer(nullptr);
	CU::Timer functionTimer;

	auto callerThreadFunction(
		[this, &functionReady, &functionReadyPointer, &functionResult]()
	{
		while (myForceQuitThread == false)
		{
			if (functionReady == true)
			{
				functionResult = (*functionReadyPointer)();
				functionReady = false;
			}

			std::this_thread::yield();
		}
	}
	);

	std::thread callerThread(callerThreadFunction);
	std::string threadPriorityErrorMessage;
	if (CU::ThreadHelper::SetThreadPriority(callerThread, CU::ThreadHelper::EThreadPriority::High, threadPriorityErrorMessage) == false)
	{
		ENGINE_LOG("ERROR! Failed to set script manager helper thread priority. Error: %s"
			, threadPriorityErrorMessage.c_str());
	}
	CU::ThreadHelper::SetThreadName(callerThread, "ScriptManager Caller Helper");

	while (myForceQuitThread == false)
	{
		myQueuedCallsToScriptRead.RemoveAll();

		std::unique_lock<std::mutex> queueLg(myQueueCallsToScriptMutex);
		myQueuedCallsToScriptRead.Add(myQueuedCallsToScriptWrite);
		myQueuedCallsToScriptWrite.RemoveAll();
		queueLg.unlock();

		if (myQueuedCallsToScriptRead.Empty() == false)
		{
			myAnyScriptsQueued = true;
		}
		else
		{
			myAnyScriptsQueued = false;
		}

		for (decltype(myQueuedCallsToScriptRead)::size_t functionIndex(0); functionIndex < myQueuedCallsToScriptRead.Size(); ++functionIndex)
		{
			if (myForceQuitThread == true)
			{
				break;
			}

			SCallScriptBase* currentFunction(myQueuedCallsToScriptRead[functionIndex]);

			functionReadyPointer = currentFunction;
			functionTimer.Update();
			const double timeWhenStarted(functionTimer.GetTotalTime());
			functionReady = true;

			bool shownTooLongWarning(false);
			bool shownAbortWarning(false);
			while ((functionReady == true) && (myForceQuitThread == false))
			{
				functionTimer.Update();
				const double timeSinceStarted(functionTimer.GetTotalTime() - timeWhenStarted);

				if ((shownTooLongWarning == false)
					&& (timeSinceStarted > ourTimeWhenTooLongScriptExecutionInSeconds))
				{
					RESOURCE_LOG("[ScriptManager] WARNING! Script \"%s\", function \"%s\", is taking a long time to run, above %f seconds"
						, currentFunction->myLuaState.myFilePaths[0].c_str(), currentFunction->myFunctionName.c_str(), ourTimeWhenTooLongScriptExecutionInSeconds);
					shownTooLongWarning = true;
				}
				else if ((shownAbortWarning == false)
					&& (timeSinceStarted > ourTimeWhenAbortScriptExecutionInSeconds))
				{
					RESOURCE_LOG("[ScriptManager] ERROR!Script \"%s\", function \"%s\", is taking too long time, above %f seconds. Aborting it"
						, currentFunction->myLuaState.myFilePaths[0].c_str(), currentFunction->myFunctionName.c_str(), ourTimeWhenAbortScriptExecutionInSeconds);
					shownAbortWarning = true;
					currentFunction->myLuaState.myForceQuitLuaCall = true;
				}

				std::this_thread::yield();
			}

			if (myForceQuitThread == true)
			{
				break;
			}

			if (shownTooLongWarning == true)
			{
				functionTimer.Update();
				const double timeSinceStarted(functionTimer.GetTotalTime() - timeWhenStarted);

				RESOURCE_LOG("[ScriptManager] WARNING! Script \"%s\", function \"%s\", took %f seconds to run"
					, currentFunction->myLuaState.myFilePaths[0].c_str(), currentFunction->myFunctionName.c_str(), timeSinceStarted);
			}

			if (functionResult == false)
			{
				// TODO: Log here? There are loggs inside operator()
			}

			sce_delete(currentFunction);
		}

		std::this_thread::yield();
	}

	callerThread.join();
}

void CScriptManager::DeleteFunctionsInBuffer(CU::GrowingArray<SCallScriptBase*>& aBuffer)
{
	for (unsigned short aFunctionIndex = 0; aFunctionIndex < aBuffer.Size(); ++aFunctionIndex)
	{
		sce_delete(aBuffer[aFunctionIndex]);
	}
}
