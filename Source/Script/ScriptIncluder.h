#pragma once

extern "C"
{
#pragma warning (push)
#pragma warning (disable : 4244)
#pragma warning (disable : 4310)
#pragma warning (disable : 4324)
#pragma warning (disable : 4702)
#include "LUA/lua.h"
#include "LUA/lauxlib.h"
#include "LUA/lualib.h"
#pragma warning (pop)
}
