#pragma once
#include "ScriptIncluder.h"
#include <shared_mutex>
#include <fstream>
#include "..\CommonUtilities\ObjectPool.h"
#include "..\CommonUtilities\Vector3.h"
#include "..\CommonUtilities\Vector4.h"

namespace sce
{
	class CMemoryPool;
}

class CScriptManager
{
	friend sce::CMemoryPool;

public:
	using StateIndexType = unsigned int;
	using OnReloadCallback = std::function<void(const StateIndexType&)>;
	using OnFunctionCalledCallback = std::function<void(const StateIndexType&)>;

	struct SFunctionAsIfFromLuaResult
	{
		enum class EType : unsigned char
		{
			None,
			Bool,
			Number,
			String,
			Pointer,
		};

		SFunctionAsIfFromLuaResult() : myType(EType::None) {};
		~SFunctionAsIfFromLuaResult() {};

		union
		{
			bool myBool;
			double myNumber;
			char myString[1024];
			void* myPointer;
		};
		EType myType;
	};

	static constexpr StateIndexType InvalidStateIndex = static_cast<StateIndexType>(-1);

	static void Create();
	static void Destroy();

	static inline CScriptManager* Get() { return ourInstance; };

	bool Init(const std::function<bool(CScriptManager& aScriptManager)>& aRegisterFunctionsFunction, const std::string& aFileName, StateIndexType& aStateIndexToSet, const OnReloadCallback& aReloadCallback);

	bool NewState(const std::string& aFileToOpen, StateIndexType& aStateIndexToSet, const OnReloadCallback& aReloadCallback);
	bool RemoveState(StateIndexType& aStateIndex);

	void QueueRemoveState(StateIndexType& aStateIndex);

	void WaitUntilDone();

	auto GetArgsAsTuple(const std::string& aFunctionName, bool& aFoundFunction);

	CU::GrowingArray<std::string, std::size_t> GetRegisteredFunctionNamesAll() const;
	CU::GrowingArray<std::string, std::size_t> GetRegisteredFunctionNamesOnlyConsole() const;
	CU::GrowingArray<std::string, std::size_t> GetRegisteredFunctionNamesOnlyNotConsole() const;

	template<typename FunctionType>
	bool RegisterFunction(const std::string& aFunctionName, const std::function<FunctionType>& aFunction, const std::string& aArgumentsText, const std::string& aDocumentationText, const bool aAlsoExposeToConsole = false);
	template<typename FunctionType>
	bool RegisterFunctionCallWithStateIndex(const std::string& aFunctionName, const std::function<FunctionType>& aFunction, const std::string& aArgumentsText, const std::string& aDocumentationText, const bool aAlsoExposeToConsole = false);

	template<typename... Args>
	bool CallFunction(const StateIndexType& aStateIndex, const std::string& aFunctionName, Args&&... aArgs);
	template<typename... Args>
	bool CallFunction(const StateIndexType& aStateIndex, const OnFunctionCalledCallback& aFunctionCalledCallback, const std::string& aFunctionName, Args&&... aArgs);

// 	template<typename... Args>
// 	bool CallCPPFunctionAsIfFromLua(const std::string& aFunctionName, Args&&... aArgs);

	bool CallCPPFunctionAsIfFromLuaSTRING(const std::string& aFullFunctionCall, SFunctionAsIfFromLuaResult& aResult);

private:
#pragma region Function structs
	struct SScriptFunctionCallableBase
	{
		struct SFunctionWrapper
		{
			int operator()(lua_State* aState)
			{
				return myFunctionWrapper(aState);
			}

			std::function<int(lua_State*)> myFunctionWrapper;
		};

		SScriptFunctionCallableBase()
			: myAlsoExposeToConsole(false)
		{};
		~SScriptFunctionCallableBase() = default;

		SFunctionWrapper myFunctionWrapper;
		std::string myFunctionName;
		bool myAlsoExposeToConsole;

#ifndef _RETAIL
		std::string myDocumentationText;
#endif // !_RETAIL
	};
	template<typename FunctionType>
	class SScriptFunctionCallable : public SScriptFunctionCallableBase
	{
		friend CScriptManager;

	public:
		~SScriptFunctionCallable() = default;

	private:
		SScriptFunctionCallable(std::function<FunctionType> aFunction)
			: myFunction(aFunction)
		{};

		std::function<FunctionType> myFunction;

	};

	struct SLuaState;

	struct SCallScriptBase
	{
		friend CScriptManager;

	public:
		~SCallScriptBase() = default;

		virtual bool operator()() = 0;

	private:
		SCallScriptBase(SLuaState& aLuaState, const std::string& aFunctionName, const OnFunctionCalledCallback& aFunctionCallback)
			: myLuaState(aLuaState)
			, myFunctionName(aFunctionName)
			, myOnFunctionCalledCallback(aFunctionCallback)
		{};

		static constexpr int ourLuaStackBottomIndex = 1;

		SLuaState& myLuaState;
		std::string myFunctionName;
		OnFunctionCalledCallback myOnFunctionCalledCallback;

	};
	template<typename TupleType>
	class SCallScriptLua : public SCallScriptBase
	{
		friend CScriptManager;

	public:
		~SCallScriptLua() = default;

		bool operator()() override
		{
			std::unique_lock<std::mutex> lg(myLuaState.myMutex);

			if (myLuaState.IsValid() == false)
			{
				RESOURCE_LOG("[ScriptManager] ERROR! \"%s\" doesn't have a valid lua state"
					, myLuaState.myFilePaths[0].c_str());
				return false;
			}

			lua_settop(myLuaState.myLuaState, ourLuaStackBottomIndex);
			if (lua_getglobal(myLuaState.myLuaState, myFunctionName.c_str()) == 0)
			{
				lua_settop(myLuaState.myLuaState, ourLuaStackBottomIndex);
				RESOURCE_LOG("[ScriptManager] ERROR! \"%s\" doesn't exist in file \"%s\", getting as function"
					, myFunctionName.c_str(), myLuaState.myFilePaths[0].c_str());
				return false;
			}

			if (lua_isfunction(myLuaState.myLuaState, -1) == false)
			{
				RESOURCE_LOG("[ScriptManager] ERROR! \"%s\" isn't a function in file \"%s\""
					, myFunctionName.c_str(), myLuaState.myFilePaths[0].c_str());
				return false;
			}

			const int amountOfArguments(PushCustomTypeForTuple(*myLuaState.myLuaState, myTuple));

			if (CheckLUAError(*myLuaState.myLuaState, lua_pcall(myLuaState.myLuaState, amountOfArguments, 0, 0)) == false)
			{
				RESOURCE_LOG("[ScriptManager] ERROR! Something went wrong when calling function \"%s\" in file \"%s\""
					, myFunctionName.c_str(), myLuaState.myFilePaths[0].c_str());
				return false;
			}

			lg.unlock();

			if (myOnFunctionCalledCallback != nullptr)
			{
				myOnFunctionCalledCallback(myLuaState.myIndexInObjectPool);
			}

			return true;
		}

	private:
		SCallScriptLua(SLuaState& aLuaState, const TupleType& aTuple, const std::string aFunctionName, const OnFunctionCalledCallback& aFunctionCallback)
			: SCallScriptBase(aLuaState, aFunctionName, aFunctionCallback)
			, myTuple(aTuple)
		{};

		TupleType myTuple;

	};

#pragma endregion

#pragma region Tuple helpers
	template <typename T>
	struct function_traits
		: public function_traits<decltype(&T::operator())>
	{};

	template <typename ClassType, typename ReturnType, typename... Args>
	struct function_traits<ReturnType(ClassType::*)(Args...) const>
	{
		enum { AmountOfArgs = sizeof...(Args) };

		using tuple_type = std::tuple<Args...>;

		using result_type = ReturnType;

		template <size_t i>
		struct arg
		{
			using type = typename std::tuple_element<i, std::tuple<Args...>>::type;
		};
	};

	template<std::size_t TypeIndex = 0, typename... Tuple>
	static inline typename std::enable_if_t<TypeIndex == sizeof...(Tuple), std::size_t>
		GetCustomTypeForTuple(lua_State&, std::tuple<Tuple...>&)
	{
		constexpr std::size_t tupleSize(sizeof...(Tuple));
		return tupleSize;
	}

	template<std::size_t TypeIndex = 0, typename... Tuple>
	static inline typename std::enable_if_t < TypeIndex < sizeof...(Tuple), std::size_t>
		GetCustomTypeForTuple(lua_State& aLuaState, std::tuple<Tuple...>& aTuple)
	{
		constexpr std::size_t argIndex((sizeof...(Tuple)-1) - TypeIndex);
		if (GetCustomType(aLuaState, std::get<argIndex>(aTuple)) == false)
		{
			return argIndex;
		}

		return GetCustomTypeForTuple<TypeIndex + 1, Tuple...>(aLuaState, aTuple);
	}

	template<std::size_t TypeIndex = 0, typename... Tuple>
	static inline typename std::enable_if_t<TypeIndex == sizeof...(Tuple), int>
		PushCustomTypeForTuple(lua_State&, std::tuple<Tuple...>&)
	{
		return 0;
	}

	template<std::size_t TypeIndex = 0, typename... Tuple>
	static inline typename std::enable_if_t < TypeIndex < sizeof...(Tuple), int>
		PushCustomTypeForTuple(lua_State& aLuaState, std::tuple<Tuple...>& aTuple)
	{
		const int amountOfReturnTypes(PushCustomType(aLuaState, std::get<TypeIndex>(aTuple)));

		return (amountOfReturnTypes + PushCustomTypeForTuple<TypeIndex + 1, Tuple...>(aLuaState, aTuple));
	}

	// _____________________________________________
	// Replace with std::apply when moved to C++17
	template<typename Function, typename Tuple, size_t... I>
	static auto ApplyImpl(Function& aFunction, Tuple& aTuple, std::index_sequence<I ...>)
	{
		aTuple; // Compile warning without this if the tuple is empty
		return aFunction(std::get<I>(aTuple)...);
	}

	template<typename Function, typename Tuple>
	static auto ApplyImpl(Function& aFunction, Tuple& aTuple)
	{
		static constexpr auto size(std::tuple_size<Tuple>::value);
		return ApplyImpl(aFunction, aTuple, std::make_index_sequence<size>{});
	}
	// _____________________________________________

	template<typename ReturnType, typename Function, typename Tuple>
	static std::enable_if_t<std::is_same<ReturnType, void>::value == false, int>
		Apply(lua_State& aLuaState, Function& aFunction, Tuple& aTuple)
	{
		static constexpr auto size = std::tuple_size<Tuple>::value;
		return PushCustomType(aLuaState, ApplyImpl(aFunction, aTuple, std::make_index_sequence<size>{}));
	}

	template<typename ReturnType, typename Function, typename Tuple>
	static std::enable_if_t<std::is_same<ReturnType, void>::value == true, int>
		Apply(lua_State&, Function& aFunction, Tuple& aTuple)
	{
		static constexpr auto size = std::tuple_size<Tuple>::value;
		ApplyImpl(aFunction, aTuple, std::make_index_sequence<size>{});
		return 0;
	}

	static void GetArgsAsString(std::string&)
	{
	}

	template<typename Arg0, typename... Args>
	static void GetArgsAsString(std::string& aStringToFill, Arg0&& aArg0, Args&&... aArgs)
	{
		StringifyCustomType(aStringToFill, aArg0);
		GetArgsAsString(aStringToFill, aArgs...);
	}

	template<typename... Args>
	static auto GetTupleTailType(std::tuple<Args...>)
	{
		return std::tuple<Args...>();
	}

	template<typename Arg0, typename... Args>
	static auto GetTupleTailType(std::tuple<Arg0, Args...>)
	{
		return std::tuple<Args...>();
	}

#pragma endregion

	struct SLuaState
	{
		SLuaState()
			: myReloadCallback(nullptr)
			, myLuaState(nullptr)
			, myStateIsValid(false)
			, myForceQuitLuaCall(false)
			, myIsAddedToFilewatch(false)
			, myIndexInObjectPool(InvalidStateIndex)
		{};

		bool IsValid() const
		{
			return ((myLuaState != nullptr) && (myStateIsValid == true));
		}

		SLuaState& operator=(const SLuaState& aOther)
		{
			myReloadCallback = aOther.myReloadCallback;
			myLuaState = aOther.myLuaState;
			myStateIsValid = aOther.myStateIsValid;
			// myMutex = aOther.myMutex; // Mutex can't be assigned/moved
			myForceQuitLuaCall = aOther.myForceQuitLuaCall;
			myIsAddedToFilewatch = aOther.myIsAddedToFilewatch;
			myIndexInObjectPool = aOther.myIndexInObjectPool;
			myFilePaths = aOther.myFilePaths;

			return *this;
		}

		SLuaState& operator=(const std::nullptr_t& aNullptr)
		{
			myLuaState = aNullptr;
			myStateIsValid = !!aNullptr;

			return *this;
		}

		OnReloadCallback myReloadCallback;
		lua_State* myLuaState;
		bool myStateIsValid;
		std::mutex myMutex;
		volatile bool myForceQuitLuaCall;
		bool myIsAddedToFilewatch;
		StateIndexType myIndexInObjectPool;
		CU::GrowingArray<std::string, unsigned char> myFilePaths;
	};

	static int FromLUA(lua_State* aState);
	static void HookLua(lua_State* aState, lua_Debug* aLuaDebug);

	static bool CheckLUAError(lua_State& aState, int aResult);
	bool CheckLUAErrorImpl(lua_State& aState, int aResult);

	static bool LoadedFileChanged(void* aLuaState, const char* aFileChangedName);
	bool LoadedFileChangedImpl(SLuaState& aLuaState, const char* aFileChangedName);

	bool InitState(const std::string& aFileToOpen, SLuaState& aState, StateIndexType& aStateIndexToSet);
	bool OpenFile(const StateIndexType& aStateIndex, const std::string& aFileToOpen);

	template<typename ApplyFunctionType, typename FunctionType>
	bool RegisterFunctionImpl(const std::function<ApplyFunctionType>& aApplyFunction, const std::string& aFunctionName, const std::function<FunctionType>& aFunction, const std::string& aArgumentsText, const std::string& aDocumentationText, const bool aAlsoExposeToConsole, const unsigned short aAmountOfArgsLess);

	void RegisterFunctionForState(SLuaState& aLuaState, SScriptFunctionCallableBase& aFunction);
	void RegisterFunctionForFakeState(SScriptFunctionCallableBase& aFunction);

#pragma region Get/Push and stringify types
	template<typename Type>
	static bool GetCustomType(lua_State& aLuaState, Type& aTypeReference);

	static int PushCustomType(lua_State& aLuaState, const double& aTypeReference);

	static int PushCustomType(lua_State& aLuaState, const unsigned int& aTypeReference);

	static int PushCustomType(lua_State& aLuaState, const unsigned long long& aTypeReference);

	static int PushCustomType(lua_State& aLuaState, const int& aTypeReference);

	static int PushCustomType(lua_State& aLuaState, const char* aTypeReference);

	static int PushCustomType(lua_State& aLuaState, const std::string& aTypeReference);

	static int PushCustomType(lua_State& aLuaState, const bool& aTypeReference);
	
	static int PushCustomType(lua_State& aLuaState, const long long& aTypeReference);

	static int PushCustomType(lua_State& aLuaState, const CU::Vector3f& aTypeReference);

	static int PushCustomType(lua_State& aLuaState, const CU::Vector4f& aTypeReference);

	template<typename Type>
	static void StringifyCustomType(std::string& aStringToAddTo, Type& aTypeReference);

	template<std::size_t ArraySize>
	static void StringifyCustomType(std::string& aStringToAddTo, const char(&aTypeReference)[ArraySize]);

#pragma endregion

	CScriptManager();
	~CScriptManager();

	void UpdateQueuedCallsThread();

	void DeleteFunctionsInBuffer(CU::GrowingArray<SCallScriptBase*>& aBuffer);

	static CScriptManager* ourInstance;

	static constexpr const char* ourDocumentationFilePath = "Data/Scripts/exposedScriptFunctions.txt";
	static constexpr StateIndexType ourMaxAmountOfStates = 1024;

	static constexpr unsigned long long ourAmountOfLinesToWarnAbout = 500;

	static constexpr double ourTimeWhenTooLongScriptExecutionInSeconds = 0.1;
	static constexpr double ourTimeWhenAbortScriptExecutionInSeconds = 1.0;

	static constexpr int ourMagicTableIndex = 3;
	static constexpr int ourMagicUserDataIndex = 1;

	std::unordered_map<std::string, SScriptFunctionCallableBase*> myRegisteredFunctions;

	std::thread myUpdateQueuedCallsThread;
	volatile bool myForceQuitThread;
	volatile bool myAnyScriptsQueued;

	CU::GrowingArray<SCallScriptBase*> myQueuedCallsToScriptWrite;
	CU::GrowingArray<SCallScriptBase*> myQueuedCallsToScriptRead;
	CU::GrowingArray<StateIndexType> myQueuedStatesToRemove;
	std::mutex myQueueCallsToScriptMutex;

	CU::ObjectPool<SLuaState, ourMaxAmountOfStates> myLuaStates;
	std::shared_mutex myLuaStatesMutex;
	std::mutex myQueueRemoveStatesMutex;
	lua_State* myCallLuaFromCPPLuaState;
};

template<typename FunctionType>
inline bool CScriptManager::RegisterFunction(const std::string& aFunctionName, const std::function<FunctionType>& aFunction, const std::string& aArgumentsText, const std::string& aDocumentationText, const bool aAlsoExposeToConsole)
{
	constexpr unsigned short amountOfArgumentsLess(0);

	using functionTypes = function_traits<decltype(std::function<FunctionType>())>;

	std::function<int(lua_State*, const SScriptFunctionCallable<FunctionType>&)> applyFunction(
		[amountOfArgumentsLess](lua_State* aLuaState, const SScriptFunctionCallable<FunctionType>& aFunction) -> int
	{
		functionTypes::tuple_type argsTuple;
		const std::size_t argExitedAt(GetCustomTypeForTuple(*aLuaState, argsTuple));
		if (argExitedAt != (functionTypes::AmountOfArgs - amountOfArgumentsLess))
		{
			lua_settop(aLuaState, SCallScriptBase::ourLuaStackBottomIndex);
			RESOURCE_LOG("[ScriptManager] ERROR! Argument type at %llu passed to \"%s\" is wrong"
				, argExitedAt, aFunction.myFunctionName.c_str());
			return 0;
		}

		return Apply<functionTypes::result_type>(*aLuaState, aFunction.myFunction, argsTuple);
	});

	return RegisterFunctionImpl(applyFunction, aFunctionName, aFunction, aArgumentsText, aDocumentationText, aAlsoExposeToConsole, amountOfArgumentsLess);
}

template<typename FunctionType>
inline bool CScriptManager::RegisterFunctionCallWithStateIndex(const std::string& aFunctionName, const std::function<FunctionType>& aFunction, const std::string& aArgumentsText, const std::string& aDocumentationText, const bool aAlsoExposeToConsole)
{
	constexpr unsigned short amountOfArgumentsLess(1);

	using functionTypes = function_traits<decltype(std::function<FunctionType>())>;

	std::function<int(lua_State*, const SScriptFunctionCallable<FunctionType>&)> applyFunction(
		[amountOfArgumentsLess](lua_State* aLuaState, const SScriptFunctionCallable<FunctionType>& aFunction) -> int
	{
		const int getTableIndex1(lua_rawgeti(aLuaState, LUA_REGISTRYINDEX, ourMagicTableIndex));
		const int getUserDataIndex(lua_rawgeti(aLuaState, -1, ourMagicUserDataIndex));

		SLuaState* customLuaState(reinterpret_cast<SLuaState*>(lua_touserdata(aLuaState, -1)));

		lua_pop(aLuaState, 2);

		if (customLuaState == nullptr)
		{
			// TODO: error
			return 0;
		}

		functionTypes::tuple_type argsTupleTemp;
		auto argsTuple(GetTupleTailType(argsTupleTemp));
		const std::size_t argExitedAt(GetCustomTypeForTuple(*aLuaState, argsTuple));
		if (argExitedAt != (functionTypes::AmountOfArgs - amountOfArgumentsLess))
		{
			lua_settop(aLuaState, SCallScriptBase::ourLuaStackBottomIndex);
			RESOURCE_LOG("[ScriptManager] ERROR! Argument type at %llu passed to \"%s\" is wrong"
				, argExitedAt, aFunction.myFunctionName.c_str());
			return 0;
		}

		auto newTuple(std::tuple_cat(std::tuple<decltype(customLuaState->myIndexInObjectPool)>(customLuaState->myIndexInObjectPool), argsTuple));

		return Apply<functionTypes::result_type>(*aLuaState, aFunction.myFunction, newTuple);
	});

	return RegisterFunctionImpl(applyFunction, aFunctionName, aFunction, aArgumentsText, aDocumentationText, aAlsoExposeToConsole, amountOfArgumentsLess);
}

template<typename ApplyFunctionType, typename FunctionType>
inline bool CScriptManager::RegisterFunctionImpl(const std::function<ApplyFunctionType>& aApplyFunction, const std::string& aFunctionName, const std::function<FunctionType>& aFunction, const std::string& aArgumentsText, const std::string& aDocumentationText, const bool aAlsoExposeToConsole, const unsigned short aAmountOfArgsLess)
{
	aArgumentsText;
	aDocumentationText;
	assert("Tried to register unnamed function to ScriptManager" && (aFunctionName != ""));
	assert("You forgot your documentation text! (Registering function in ScriptManager)" && (aDocumentationText.empty() == false));
	assert("Your documentation text is probably not complete! Make it more descriptive. (Registering function in ScriptManager)" && (aDocumentationText.size() > 10));

	SScriptFunctionCallable<FunctionType>* newFunction(sce_new(SScriptFunctionCallable<FunctionType>(aFunction)));

	newFunction->myFunctionName = aFunctionName;
	newFunction->myAlsoExposeToConsole = aAlsoExposeToConsole;
#ifndef _RETAIL
	newFunction->myDocumentationText = aDocumentationText;
#endif // !_RETAIL

	using functionTypes = function_traits<decltype(std::function<FunctionType>())>;

	SScriptFunctionCallable<FunctionType>& function(*newFunction);
	newFunction->myFunctionWrapper.myFunctionWrapper =
		[this, aApplyFunction, function, aAmountOfArgsLess](lua_State* aLuaState) -> int
	{
		if (aLuaState == nullptr)
		{
			RESOURCE_LOG("[ScriptManager] ERROR! Invalid lua state in RegisterFunction lambda, probably memory corruption somewhere");
			return 0;
		}

		const int amountOfArgumentsPassed(lua_gettop(aLuaState));
		const std::size_t amountOfArgumentsWanted(functionTypes::AmountOfArgs - aAmountOfArgsLess);

		if (amountOfArgumentsPassed != amountOfArgumentsWanted)
		{
			lua_settop(aLuaState, SCallScriptBase::ourLuaStackBottomIndex);
			RESOURCE_LOG("[ScriptManager] ERROR! Wrong amount of arguments passed to \"%s\". Got %i, expected %llu"
				, function.myFunctionName.c_str(), amountOfArgumentsPassed, amountOfArgumentsWanted);
			return 0;
		}

		const int amountOfReturnTypes(aApplyFunction(aLuaState, function));

		return amountOfReturnTypes;
	};

	std::shared_lock<std::shared_mutex> sharedLock(myLuaStatesMutex);
	const auto& usedStatesIndices(myLuaStates.GetUsedIDs());
	for (const auto& currentStateIndex : usedStatesIndices)
	{
		auto& currentState(myLuaStates.GetObject(currentStateIndex));
		std::unique_lock<std::mutex> uniqueLock(currentState.myMutex);
		RegisterFunctionForState(currentState, function);
		uniqueLock.unlock();
	}
	sharedLock.unlock();

	RegisterFunctionForFakeState(function);
#ifndef _RETAIL


	std::ofstream outputFile(ourDocumentationFilePath, std::ofstream::out | std::ofstream::app);
	if (outputFile.good() == false)
	{
		RESOURCE_LOG("[ScriptManager] ERROR! Couldn't open the documentation file \"%s\""
			, ourDocumentationFilePath);
		sce_delete(newFunction);
		return false;
	}
	outputFile << "Name:" << std::endl;
	outputFile << "\t\"" << aFunctionName << "\"" << std::endl;
	outputFile << "ARGS:" << std::endl;
	outputFile << "\t[" << aArgumentsText << "]" << std::endl;
	outputFile << "Description:" << std::endl;
	outputFile << "\t" << aDocumentationText << std::endl;
	outputFile << std::endl;
	outputFile.close();
#endif

	assert("A function with this name has already been registered."
		&& (myRegisteredFunctions.find(aFunctionName) == myRegisteredFunctions.end()));
	myRegisteredFunctions.insert({ aFunctionName, newFunction });

	return true;
}

template<typename... Args>
inline bool CScriptManager::CallFunction(const StateIndexType& aStateIndex, const std::string& aFunctionName, Args&&... aArgs)
{
	return CallFunction(aStateIndex, nullptr, aFunctionName, std::forward<Args>(aArgs)...);
}

template<typename... Args>
inline bool CScriptManager::CallFunction(const StateIndexType& aStateIndex, const OnFunctionCalledCallback& aFunctionCalledCallback, const std::string& aFunctionName, Args&&... aArgs)
{
	std::shared_lock<std::shared_mutex> sharedLock(myLuaStatesMutex);
	if (myLuaStates.IDIsUsed(aStateIndex) == false)
	{
		RESOURCE_LOG("[ScriptManager] ERROR! Lua state ID (%u) isn't used, can't call function \"%s\""
			, aStateIndex, aFunctionName.c_str());
		return false;
	}

	SLuaState& luaState(myLuaStates.GetObject(aStateIndex));
	sharedLock.unlock();

	auto arguments(std::make_tuple(std::move(aArgs)...));

#define NEW_CALL_SCRIPT_LUA SCallScriptLua<decltype(arguments)>(luaState, arguments, aFunctionName, aFunctionCalledCallback)
	SCallScriptLua<decltype(arguments)>* newFunction(sce_new(NEW_CALL_SCRIPT_LUA));
#undef NEW_CALL_SCRIPT_LUA

	std::unique_lock<std::mutex> queueLg(myQueueCallsToScriptMutex);
	if (myQueuedCallsToScriptWrite.Size() >= 16000)
	{
		queueLg.unlock();

		RESOURCE_LOG("[ScriptManager] WARNING! There are too many scripts queued! All queues will wait for free buffer space");

		bool queueCanBeFilled(false);
		while (queueCanBeFilled == false)
		{
			queueLg.lock();
			queueCanBeFilled = myQueuedCallsToScriptWrite.Size() < 16000;
			queueLg.unlock();
			std::this_thread::yield();
		}
		queueLg.lock();
	}
	myQueuedCallsToScriptWrite.Add(newFunction);
	queueLg.unlock();

	return true;
}

// template<typename... Args>
// inline bool CScriptManager::CallCPPFunctionAsIfFromLua(const std::string& aFunctionName, Args&&... aArgs)
// {
// 	if (myCallLuaFromCPPLuaState == nullptr)
// 	{
// 		RESOURCE_LOG("[ScriptManager] ERROR! Lua state for C++ -> Lua is nullptr");
// 		return false;
// 	}
// 
// 	std::string stringFunction(aFunctionName + "(");
// 
// 	GetArgsAsString(stringFunction, aArgs...);
// 
// 	return CallCPPFunctionAsIfFromLua(stringFunction);
// 
// 	//return true;
// }

#define LUA_GET_CUSTOM_TYPE(aType, aValue, aCast)	\
if (lua_is##aType(&aLuaState, -1))					\
{													\
	aValue = aCast(lua_to##aType(&aLuaState, -1));	\
	lua_pop(&aLuaState, 1);							\
	return true;									\
}													\
													\
return false;

template<typename Type>
inline bool CScriptManager::GetCustomType(lua_State& aLuaState, Type& aTypeReference)
{
	static_assert(false, "One of your functions arguments isn't supported");
	// Compile error here?
	// Add it!
	return false;
}

template<>
inline bool CScriptManager::GetCustomType(lua_State& aLuaState, const char*& aTypeReference)
{
	LUA_GET_CUSTOM_TYPE(string, aTypeReference, );
}

template<>
inline bool CScriptManager::GetCustomType(lua_State& aLuaState, int& aTypeReference)
{
	LUA_GET_CUSTOM_TYPE(number, aTypeReference, static_cast<int>);
}

template<>
inline bool CScriptManager::GetCustomType(lua_State& aLuaState, unsigned int& aTypeReference)
{
	LUA_GET_CUSTOM_TYPE(number, aTypeReference, static_cast<unsigned int>);
}

template<>
inline bool CScriptManager::GetCustomType(lua_State& aLuaState, double& aTypeReference)
{
	LUA_GET_CUSTOM_TYPE(number, aTypeReference, );
}

template<>
inline bool CScriptManager::GetCustomType(lua_State& aLuaState, float& aTypeReference)
{
	LUA_GET_CUSTOM_TYPE(number, aTypeReference, static_cast<float>);
}

template<>
inline bool CScriptManager::GetCustomType(lua_State& aLuaState, bool& aTypeReference)
{
	LUA_GET_CUSTOM_TYPE(boolean, aTypeReference, !!);
}

template<>
inline bool CScriptManager::GetCustomType(lua_State& aLuaState, unsigned char& aTypeReference)
{
	LUA_GET_CUSTOM_TYPE(number, aTypeReference, static_cast<unsigned char>);
}

template<>
inline bool CScriptManager::GetCustomType(lua_State& aLuaState, std::string& aTypeReference)
{
	LUA_GET_CUSTOM_TYPE(string, aTypeReference, );
}

template<>
inline bool CScriptManager::GetCustomType(lua_State& aLuaState, unsigned long long& aTypeReference)
{
	LUA_GET_CUSTOM_TYPE(number, aTypeReference, static_cast<unsigned long long>);
}

template<>
inline bool CScriptManager::GetCustomType(lua_State& aLuaState, long long& aTypeReference)
{
	if (lua_isnumber(&aLuaState, -1))
	{
		const double tempDouble(lua_tonumber(&aLuaState, -1));
		aTypeReference = *reinterpret_cast<const long long*>(&tempDouble);
		lua_pop(&aLuaState, 1);
		return true;
	}
	return false;
}

#undef LUA_GET_CUSTOM_TYPE

inline int CScriptManager::PushCustomType(lua_State& aLuaState, const double& aTypeReference)
{
	lua_pushnumber(&aLuaState, aTypeReference);
	return 1;
}

inline int CScriptManager::PushCustomType(lua_State& aLuaState, const unsigned int& aTypeReference)
{
	lua_pushnumber(&aLuaState, static_cast<double>(aTypeReference));
	return 1;
}

inline int CScriptManager::PushCustomType(lua_State& aLuaState, const unsigned long long& aTypeReference)
{
	lua_pushnumber(&aLuaState, static_cast<double>(aTypeReference));
	return 1;
}

inline int CScriptManager::PushCustomType(lua_State& aLuaState, const int& aTypeReference)
{
	lua_pushnumber(&aLuaState, static_cast<double>(aTypeReference));
	return 1;
}

inline int CScriptManager::PushCustomType(lua_State& aLuaState, const char* aTypeReference)
{
	lua_pushstring(&aLuaState, aTypeReference);
	return 1;
}

inline int CScriptManager::PushCustomType(lua_State& aLuaState, const std::string& aTypeReference)
{
	lua_pushstring(&aLuaState, aTypeReference.c_str());
	return 1;
}

inline int CScriptManager::PushCustomType(lua_State& aLuaState, const bool& aTypeReference)
{
	lua_pushboolean(&aLuaState, aTypeReference);
	return 1;
}

inline int CScriptManager::PushCustomType(lua_State& aLuaState, const long long& aTypeReference)
{
	lua_pushnumber(&aLuaState, *reinterpret_cast<const double*>(&aTypeReference));
	return 1;
}

inline int CScriptManager::PushCustomType(lua_State& aLuaState, const CU::Vector3f& aTypeReference)
{
	lua_newtable(&aLuaState);

	lua_pushstring(&aLuaState, "x");
	lua_pushnumber(&aLuaState, aTypeReference.x);
	lua_rawset(&aLuaState, -3);
	
	lua_pushstring(&aLuaState, "y");
	lua_pushnumber(&aLuaState, aTypeReference.y);
	lua_rawset(&aLuaState, -3);
	
	lua_pushstring(&aLuaState, "z");
	lua_pushnumber(&aLuaState, aTypeReference.z);
	lua_rawset(&aLuaState, -3);

	return 1;
}

inline int CScriptManager::PushCustomType(lua_State& aLuaState, const CU::Vector4f& aTypeReference)
{
	lua_newtable(&aLuaState);

	lua_pushstring(&aLuaState, "x");
	lua_pushnumber(&aLuaState, aTypeReference.x);
	lua_rawset(&aLuaState, -3);

	lua_pushstring(&aLuaState, "y");
	lua_pushnumber(&aLuaState, aTypeReference.y);
	lua_rawset(&aLuaState, -3);

	lua_pushstring(&aLuaState, "z");
	lua_pushnumber(&aLuaState, aTypeReference.z);
	lua_rawset(&aLuaState, -3);

	lua_pushstring(&aLuaState, "w");
	lua_pushnumber(&aLuaState, aTypeReference.w);
	lua_rawset(&aLuaState, -3);

	return 1;
}

template<typename Type>
inline void CScriptManager::StringifyCustomType(std::string& aStringToAddTo, Type& aTypeReference)
{
	aStringToAddTo += std::to_string(aTypeReference) + ',';
	// Compile error here?
	// Add it as a specific below!
}

template<std::size_t ArraySize>
inline void CScriptManager::StringifyCustomType(std::string& aStringToAddTo, const char(&aTypeReference)[ArraySize])
{
	const std::string arrayAsString(aTypeReference);
	std::string escapedString;
	escapedString.reserve(arrayAsString.size());

	for (unsigned char currentChar : arrayAsString)
	{
		if ((' ' <= currentChar) && (currentChar <= '~') && (currentChar != '\\') && (currentChar != '"'))
		{
			escapedString += currentChar;
		}
		else
		{
			escapedString += '\\';
			switch (currentChar)
			{
			case '\"':
			{
				escapedString += '\"';
				break;
			}
			case '\\':
			{
				escapedString += '\\';
				break;
			}
			case '\t':
			{
				escapedString += 't';
				break;
			}
			case '\r':
			{
				escapedString += 'r';
				break;
			}
			case '\n':
			{
				escapedString += 'n';
				break;
			}
			default:
			{
				constexpr char* hexdig = "0123456789ABCDEF";
				escapedString += 'x';
				escapedString += hexdig[currentChar >> 4];
				escapedString += hexdig[currentChar & 0xF];
			}
			}
		}
	}

	aStringToAddTo += "\"" + escapedString + "\",";
}
