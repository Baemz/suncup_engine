#pragma once

namespace sce {
	namespace gfx {

		class CShaderEffect
		{
			friend class CShaderEffectFactory;
			friend class CDeferredRenderer;
			friend class CFullscreenRenderer;
		public:
			~CShaderEffect() = default;

		private:
			CShaderEffect();

			unsigned int myConstantBufferID;
			unsigned int myTextureID;
			unsigned int myPixelShaderID;
			unsigned char myConstantBufferSlot;
		};

	}
}