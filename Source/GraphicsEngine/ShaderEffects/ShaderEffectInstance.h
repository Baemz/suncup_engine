#pragma once
#include "ShaderEffectStructs.h"

namespace sce {
	namespace gfx {

		class CShaderEffectInstance
		{
			friend class CFullscreenRenderer;
			friend class CDeferredRenderer;
			friend class CScene;

		public:
			CShaderEffectInstance();
			~CShaderEffectInstance() = default;

			void InitGlitchScreen(const SGlitchScreenData& aInitData);
			void InitStatic(const SStaticScreenData& aInitData);

			void Render();
			const unsigned int GetEffectID() const { return myShaderEffectID; };

			SGlitchScreenData& GetGlitchScreenData();
			SStaticScreenData& GetStaticData();

		private:

			void Init(const char* aEntryPoint, const char* aDefineName);
			
			enum : char
			{
				eNone,
				eGlitchScreen,
				eStatic,
			} myEffect;

			union
			{
				SGlitchScreenData myGlitchData;
				SStaticScreenData myStaticData;
				SNoneEffectData myNoneEffectData;
			};

			float myElapsedTime;
			unsigned int myShaderEffectID;
		};
	}
}