#include "stdafx.h"
#include "ShaderEffectInstance.h"
#include "GraphicsEngineInterface.h"
#include "ShaderEffectFactory.h"

namespace sce
{
	namespace gfx
	{
		CShaderEffectInstance::CShaderEffectInstance()
			: myShaderEffectID(UINT_MAX)
			, myEffect(eNone)
		{
		}

		void CShaderEffectInstance::Init(const char * aEntryPoint, const char * aDefineName)
		{
			if (myEffect == eNone)
			{
				myNoneEffectData = SNoneEffectData();
			}

			if (CShaderEffectFactory::Get() != nullptr)
			{
				myShaderEffectID = CShaderEffectFactory::Get()->CreateShaderEffect(aEntryPoint, aDefineName);
			}
			else
			{
				ENGINE_LOG("WARNING! No shadereffect factory created!");
			}
		}

		void CShaderEffectInstance::InitGlitchScreen(const SGlitchScreenData & aInitData)
		{
			myEffect = eGlitchScreen;
			myGlitchData = aInitData;
			Init("PS_GlitchScreen01", "GLITCHSCREEN01");
		}

		void CShaderEffectInstance::InitStatic(const SStaticScreenData & aInitData)
		{
			myEffect = eStatic;
			myStaticData = aInitData;
			Init("PS_Static", "STATIC");
		}

		void CShaderEffectInstance::Render()
		{
			if (myEffect == eNone)
			{
				ENGINE_LOG("ShaderEffect with ID [%i] has not been initialized", myShaderEffectID);
				return;
			}
			CGraphicsEngineInterface::AddToScene(*this);
		}
		
		SGlitchScreenData & CShaderEffectInstance::GetGlitchScreenData()
		{
			if (myEffect != eGlitchScreen)
			{
				assert(false && "Getting wrong shader struct from shader effect with ID %i.");
				RESOURCE_LOG("Getting wrong shader struct from shader effect with ID %i.", myShaderEffectID);
			}
			return myGlitchData;
		}
		SStaticScreenData & CShaderEffectInstance::GetStaticData()
		{
			if (myEffect != eStatic)
			{
				assert(false && "Getting wrong shader struct from shader effect with ID %i.");
				RESOURCE_LOG("Getting wrong shader struct from shader effect with ID %i.", myShaderEffectID);
			}
			return myStaticData;
		}
	}
}


