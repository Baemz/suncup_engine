#pragma once
#define MAX_SHADEREFFECTS 10

namespace sce {
	namespace gfx {

		class CShaderEffect;
		class CShaderEffectFactory
		{
		public:
			CShaderEffectFactory();
			~CShaderEffectFactory();

			static void Create();
			static void Destroy();
			static CShaderEffectFactory* Get() { return ourInstance; };

			unsigned int CreateShaderEffect(const char* aEntryPoint, const char* aDefineName);

			CShaderEffect& GetEffectWithID(unsigned int aID);
		private: 
			static CShaderEffectFactory* ourInstance;

			//bool LoadShaderData(const char* aDataPath);

			bool ShaderEffectExists(const std::string& aIdentifier);
			unsigned int GetShaderEffectID(const std::string& aIdentifier);
			unsigned int myCurrentlyFreeShaderEffectID;
			std::map<std::string, unsigned int> myAssignedShaderEffectIDs;
			CShaderEffect* myShaderEffects;
		};

	}
}