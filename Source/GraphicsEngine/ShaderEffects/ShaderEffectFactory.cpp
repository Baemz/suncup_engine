#include "stdafx.h"
#include "ShaderEffectFactory.h"
#include "ShaderEffect.h"
#include "../ResourceManager/ResourceManager.h"
#include "../DirectXFramework/API/DX11Shader.h"
#include "../DirectXFramework/API/DX11ConstantBuffer.h"
#include "../DirectXFramework/API/DX11Texture2D.h"
#include "ShaderEffectStructs.h"

namespace sce
{
	namespace gfx
	{
		CShaderEffectFactory* CShaderEffectFactory::ourInstance = nullptr;

		CShaderEffectFactory::CShaderEffectFactory()
			: myCurrentlyFreeShaderEffectID(0)
		{
			myShaderEffects = sce_newArray(CShaderEffect, MAX_SHADEREFFECTS);
		}

		CShaderEffectFactory::~CShaderEffectFactory()
		{
			sce_delete(myShaderEffects);
		}

		void CShaderEffectFactory::Create()
		{
			if (ourInstance == nullptr)
			{
				ourInstance = sce_new(CShaderEffectFactory());
			}

			return;
		}

		void CShaderEffectFactory::Destroy()
		{
			sce_delete(ourInstance);
		}

		unsigned int CShaderEffectFactory::CreateShaderEffect(const char * aEntryPoint, const char * aDefineName)
		{
			std::string identifier = aEntryPoint;
			if (ShaderEffectExists(identifier))
			{
				unsigned int effectID = GetShaderEffectID(identifier);
				return effectID;
			}

			unsigned int effectID = GetShaderEffectID(identifier);
			if (effectID == UINT_MAX)
			{
				return effectID;
			}

			CResourceManager* resources(CResourceManager::Get());
			CShaderEffect& shaderEffect(myShaderEffects[effectID]);

			unsigned int shaderID = UINT_MAX;
			if (resources->ShaderExists(aEntryPoint))
			{
				shaderID = resources->GetShaderID(aEntryPoint);
			}
			else
			{
				shaderID = resources->GetShaderID(aEntryPoint);
				CDX11Shader& shader(resources->GetShaderWithID(shaderID));
				const D3D_SHADER_MACRO define[] = 
				{
					{ aDefineName, "1" },
					{nullptr, nullptr}
				};
				shader.InitPixel("Data/Shaders/ShaderEffects.hlsl", aEntryPoint, define);
			}
			shaderEffect.myPixelShaderID = shaderID;

			unsigned int cBufferID = resources->GetConstantBufferID(aEntryPoint);
			shaderEffect.myConstantBufferID = cBufferID;
			shaderEffect.myConstantBufferSlot = (unsigned char)myCurrentlyFreeShaderEffectID - 1;

			unsigned int textureID = UINT_MAX;
			if (resources->TextureExists("Data/Shaders/shader_noise.dds"))
			{
				textureID = resources->GetTextureID("Data/Shaders/shader_noise.dds");
			}
			else
			{
				textureID = resources->GetTextureID("Data/Shaders/shader_noise.dds");
				CDX11Texture& texture(resources->GetTextureWithID(textureID));
				texture.CreateTexture("Data/Shaders/shader_noise.dds", ETextureType::ShaderEffect);
			}
			shaderEffect.myTextureID = textureID;

			return effectID;
		}

		CShaderEffect & CShaderEffectFactory::GetEffectWithID(unsigned int aID)
		{
			return myShaderEffects[aID];
		}

		bool CShaderEffectFactory::ShaderEffectExists(const std::string & aIdentifier)
		{
			if (myAssignedShaderEffectIDs.find(aIdentifier) != myAssignedShaderEffectIDs.end())
			{
				return true;
			}
			return false;
		}

		unsigned int CShaderEffectFactory::GetShaderEffectID(const std::string & aIdentifier)
		{
			if (myAssignedShaderEffectIDs.find(aIdentifier) != myAssignedShaderEffectIDs.end())
			{
				return myAssignedShaderEffectIDs[aIdentifier];
			}
			else
			{
				if (myCurrentlyFreeShaderEffectID > (MAX_SHADEREFFECTS - 1))
				{
					assert(false && "Too many shader effects created!");
					RESOURCE_LOG("ERROR! Too many shader effects created!");
					return UINT_MAX;
				}
				myAssignedShaderEffectIDs[aIdentifier] = myCurrentlyFreeShaderEffectID;
				return myCurrentlyFreeShaderEffectID++;
			}
		}
	}
}