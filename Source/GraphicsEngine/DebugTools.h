#pragma once
#ifndef _RETAIL
#define USE_DEBUG_TOOLS
#endif // !_RETAIL

#include "Utilities\VertexStructs.h"
#include "..\CommonUtilities\GrowingArray.h"

#ifndef USE_DEBUG_TOOLS
#define DT_DRAW_LINE_2_ARGS(aStartPos, aEndPos)
#define DT_DRAW_LINE_3_ARGS(aStartPos, aEndPos, aStartColorRGB)
#define DT_DRAW_LINE_4_ARGS(aStartPos, aEndPos, aStartColorRGB, aEndColorRGB)
#define DT_DRAW_LINE_5_ARGS(aStartPos, aEndPos, aStartColorRGB, aEndColorRGB, aStartThickness)
#define DT_DRAW_LINE_6_ARGS(aStartPos, aEndPos, aStartColorRGB, aEndColorRGB, aStartThickness, aEndThickness) 

#define DT_DRAW_SPHERE_2_ARGS(aPos, aColor)
#define DT_DRAW_SPHERE_3_ARGS(aPos, aColor, aRadius)
#define DT_DRAW_SPHERE_4_ARGS(aPos, aColor, aRadius, aRotation)
#define DT_DRAW_SPHERE_5_ARGS(aPos, aColor, aRadius, aRotation, aRingCount)
#define DT_DRAW_SPHERE_6_ARGS(aPos, aColor, aRadius, aRotation, aRingCount, aSliceCount)

#define DT_DRAW_SPHERE_DEF_COLOR_1_ARGS(aPos)
#define DT_DRAW_SPHERE_DEF_COLOR_2_ARGS(aPos, aRadius)
#define DT_DRAW_SPHERE_DEF_COLOR_3_ARGS(aPos, aRadius, aRotation)
#define DT_DRAW_SPHERE_DEF_COLOR_4_ARGS(aPos, aRadius, aRotation, aRingCount)
#define DT_DRAW_SPHERE_DEF_COLOR_5_ARGS(aPos, aRadius, aRotation, aRingCount, aSliceCount)
#define DRAW_SPHERE_WITH_A_FREAKING_COLOR(aPos, aRadius, aColor)

#define DT_DRAW_CUBE_1_ARGS(aPos)
#define DT_DRAW_CUBE_2_ARGS(aPos, aSize)
#define DT_DRAW_CUBE_3_ARGS(aPos, aSize, aColor)
#else
#include <shared_mutex>

#define DT_DRAW_LINE_2_ARGS(aStartPos, aEndPos) sce::gfx::CDebugTools::Get()->DrawLine(aStartPos, aEndPos);
#define DT_DRAW_LINE_3_ARGS(aStartPos, aEndPos, aStartColorRGB) sce::gfx::CDebugTools::Get()->DrawLine(aStartPos, aEndPos, aStartColorRGB, aStartColorRGB);
#define DT_DRAW_LINE_4_ARGS(aStartPos, aEndPos, aStartColorRGB, aEndColorRGB) sce::gfx::CDebugTools::Get()->DrawLine(aStartPos, aEndPos, aStartColorRGB, aEndColorRGB);
#define DT_DRAW_LINE_5_ARGS(aStartPos, aEndPos, aStartColorRGB, aEndColorRGB, aStartThickness) sce::gfx::CDebugTools::Get()->DrawLine(aStartPos, aEndPos, aStartColorRGB, aEndColorRGB, aStartThickness);
#define DT_DRAW_LINE_6_ARGS(aStartPos, aEndPos, aStartColorRGB, aEndColorRGB, aStartThickness, aEndThickness) sce::gfx::CDebugTools::Get()->DrawLine(aStartPos, aEndPos, aStartColorRGB, aEndColorRGB, aStartThickness, aEndThickness);

#define DT_DRAW_SPHERE_2_ARGS(aPos, aColor) sce::gfx::CDebugTools::Get()->DrawSphere(aPos, aColor);
#define DT_DRAW_SPHERE_3_ARGS(aPos, aColor, aRadius) sce::gfx::CDebugTools::Get()->DrawSphere(aPos, aColor, { 0.0f }, aRadius);
#define DT_DRAW_SPHERE_4_ARGS(aPos, aColor, aRadius, aRotation) sce::gfx::CDebugTools::Get()->DrawSphere(aPos, aColor, aRotation, aRadius);
#define DT_DRAW_SPHERE_5_ARGS(aPos, aColor, aRadius, aRotation, aRingCount) sce::gfx::CDebugTools::Get()->DrawSphere(aPos, aColor, aRotation, aRadius, aRingCount);
#define DT_DRAW_SPHERE_6_ARGS(aPos, aColor, aRadius, aRotation, aRingCount, aSliceCount) sce::gfx::CDebugTools::Get()->DrawSphere(aPos, aColor, aRotation, aRadius, aRingCount, aSliceCount);

#define DT_DRAW_SPHERE_DEF_COLOR_1_ARGS(aPos) sce::gfx::CDebugTools::Get()->DrawSphere(aPos);
#define DT_DRAW_SPHERE_DEF_COLOR_2_ARGS(aPos, aRadius) sce::gfx::CDebugTools::Get()->DrawSphere(aPos, aRadius);
#define DT_DRAW_SPHERE_DEF_COLOR_3_ARGS(aPos, aRadius, aRotation) sce::gfx::CDebugTools::Get()->DrawSphere(aPos, aRadius, { -1.0f }, aRotation);
#define DT_DRAW_SPHERE_DEF_COLOR_4_ARGS(aPos, aRadius, aRotation, aRingCount) sce::gfx::CDebugTools::Get()->DrawSphere(aPos, aRadius, { -1.0f }, aRotation, aRingCount);
#define DT_DRAW_SPHERE_DEF_COLOR_5_ARGS(aPos, aRadius, aRotation, aRingCount, aSliceCount) sce::gfx::CDebugTools::Get()->DrawSphere(aPos, aRadius,{ -1.0f }, aRotation, aRingCount, aSliceCount);
#define DRAW_SPHERE_WITH_A_FREAKING_COLOR(aPos, aRadius, aColor) sce::gfx::CDebugTools::Get()->DrawSphere(aPos, aRadius, aColor);

#define DT_DRAW_CUBE_1_ARGS(aPos) sce::gfx::CDebugTools::Get()->DrawCube(aPos);
#define DT_DRAW_CUBE_2_ARGS(aPos, aSize) sce::gfx::CDebugTools::Get()->DrawCube(aPos, aSize);
#define DT_DRAW_CUBE_3_ARGS(aPos, aSize, aColor) sce::gfx::CDebugTools::Get()->DrawCube(aPos, aSize, aColor);
#endif // !USE_DEBUG_TOOLS

namespace sce { namespace gfx {

	class CModelInstance;

	class CDebugTools
	{
#ifndef USE_DEBUG_TOOLS
	public:
		~CDebugTools() = default;

		static void Create();
		static void Destroy();
		static CDebugTools* Get();

		void DrawLine(CU::Vector3f, CU::Vector3f, CU::Vector3f = { 1.f, 1.f, 1.f }, CU::Vector3f = { 1.f, 1.f, 1.f },
			float = 1.0f, float = 1.0f) {};
		void DrawLine(const float, const float, const float
			, const float, const float, const float
			, const unsigned char = 255, const unsigned char = 255, const unsigned char = 255
			, const unsigned char = 255, const unsigned char = 255, const unsigned char = 255
			, float = 1.0f, float = 1.0f) {};
		void DrawLine(const float, const float, const float
			, const float, const float, const float
			, const float = 1.0f, const float = 1.0f, const float = 1.0f
			, const float = 1.0f, const float = 1.0f, const float = 1.0f
			, float = 1.0f, float = 1.0f) {};

		void DrawLine2D(CU::Vector2f, CU::Vector2f, CU::Vector3f = { 1.f, 1.f, 1.f }, CU::Vector3f = { 1.f, 1.f, 1.f }) {};
		void DrawLine2D(const float, const float
			, const float, const float
			, const unsigned char = 255, const unsigned char = 255, const unsigned char = 255
			, const unsigned char = 255, const unsigned char = 255, const unsigned char = 255) {};
		void DrawLine2D(const float, const float
			, const float, const float
			, const float = 1.0f, const float = 1.0f, const float = 1.0f
			, const float = 1.0f, const float = 1.0f, const float = 1.0f) {};

		void DrawLinePlane(const CU::Vector4f&, const CU::Vector3f = { 1.0f, 1.0f, 1.0f }) {};

		void DrawCube(const CU::Vector3f, const float = 1.f, const CU::Vector3f = { 1.f, 1.f, 1.f }) {};
		void DrawCube(const float, const float, const float
			, const float = 1.0f
			, const unsigned char = 255, const unsigned char = 255, const unsigned char = 255) {};
		void DrawCube(const float, const float, const float
			, const float = 1.0f
			, const float = 1.0f, const float = 1.0f, const float = 1.0f) {};

		void DrawRectangle3D(const CU::Vector3f, const CU::Vector3f = { 1.f, 1.f, 1.f }, const CU::Vector3f = { 1.f, 1.f, 1.f }) {};
		void DrawRectangle3D(const float, const float, const float
			, const float = 1.0f, const float = 1.0f, const float = 1.0f
			, const unsigned char = 255, const unsigned char = 255, const unsigned char = 255) {};
		void DrawRectangle3D(const float, const float, const float
			, const float = 1.0f, const float = 1.0f, const float = 1.0f
			, const float = 1.0f, const float = 1.0f, const float = 1.0f) {};

		void DrawText2D(const std::string&, const CU::Vector2f& = CU::Vector2f(0.0f, 0.0f), float = 24.0f, const CU::Vector3f& = CU::Vector3f(1.0f, 1.0f, 1.0f)) {};
		void DrawText2D(const std::string
			, const float = 0.0f, const float = 0.0f
			, float = 24.0f
			, const unsigned char = 255, const unsigned char = 255, const unsigned char = 255) {};
		void DrawText2D(const std::string&
			, const float = 0.0f, const float = 0.0f
			, float = 24.0f
			, const float = 1.0f, const float = 1.0f, const float = 1.0f) {};

		void DrawSphere(const CU::Vector3f&, const CU::Vector3f&, const CU::Vector3f& = CU::Vector3f(),
			const float = 1.0f, const unsigned int = 10, const unsigned int = 10) {};

		const CU::GrowingArray<SSimpleDebugVertex, unsigned int>& GetDebugLineData() { return mySimpleDebugVertexArray; };
		const CU::GrowingArray<SSimpleDebugVertex, unsigned int>& GetDebug2DLineData() { return mySimpleDebugVertexArray; };
		const CU::GrowingArray<SSimpleDebugVertex, unsigned int>& GetDebugCubeData() { return mySimpleDebugVertexArray; };
		const CU::GrowingArray<CModelInstance, unsigned int>& GetDebugSphereData() { return myModelInstanceArray; };
		const CU::GrowingArray<SSimpleDebugVertex, unsigned int>& GetRectangle3DData() { return mySimpleDebugVertexArray; };

		void ChangeUpdateBuffer() {};
		void ChangeRenderBuffer() {};
	
	private:
		CDebugTools() = default;
		static CDebugTools* ourInstance;

		CU::GrowingArray<SSimpleDebugVertex, unsigned int>	mySimpleDebugVertexArray;
		CU::GrowingArray<CModelInstance, unsigned int> myModelInstanceArray;

#else
	public:
		~CDebugTools();

		static void Create();
		static void Destroy();
		static CDebugTools* Get();

		void DrawLine(CU::Vector3f aStartPos, CU::Vector3f aEndPos, CU::Vector3f aStartColorRGB = { 1.f, 1.f, 1.f }, CU::Vector3f aEndColorRGB = { 1.f, 1.f, 1.f },
			float aStartThickness = 1.0f, float aEndThickness = 1.0f);
		void DrawLine(const float aStartPosX, const float aStartPosY, const float aStartPosZ
			, const float aEndPosX, const float aEndPosY, const float aEndPosZ
			, const unsigned char aStartColorR = 255, const unsigned char aStartColorG = 255, const unsigned char aStartColorB = 255
			, const unsigned char aEndColorR = 255, const unsigned char aEndColorG = 255, const unsigned char aEndColorB = 255
			, float aStartThickness = 1.0f, float aEndThickness = 1.0f);
		void DrawLine(const float aStartPosX, const float aStartPosY, const float aStartPosZ
			, const float aEndPosX, const float aEndPosY, const float aEndPosZ
			, const float aStartColorR = 1.0f, const float aStartColorG = 1.0f, const float aStartColorB = 1.0f
			, const float aEndColorR = 1.0f, const float aEndColorG = 1.0f, const float aEndColorB = 1.0f
			, float aStartThickness = 1.0f, float aEndThickness = 1.0f);

		void DrawLine2D(CU::Vector2f aStartScreenPos, CU::Vector2f aEndScreenPos, CU::Vector3f aStartColorRGB = { 1.f, 1.f, 1.f }, CU::Vector3f aEndColorRGB = { 1.f, 1.f, 1.f });
		void DrawLine2D(const float aStartScreenPosX, const float aStartScreenPosY
			, const float aEndScreenPosX, const float aEndScreenPosY
			, const unsigned char aStartColorR = 255, const unsigned char aStartColorG = 255, const unsigned char aStartColorB = 255
			, const unsigned char aEndColorR = 255, const unsigned char aEndColorG = 255, const unsigned char aEndColorB = 255);
		void DrawLine2D(const float aStartScreenPosX, const float aStartScreenPosY
			, const float aEndScreenPosX, const float aEndScreenPosY
			, const float aStartColorR = 1.0f, const float aStartColorG = 1.0f, const float aStartColorB = 1.0f
			, const float aEndColorR = 1.0f, const float aEndColorG = 1.0f, const float aEndColorB = 1.0f);

		void DrawLinePlane(const CU::Vector4f& aAABBWithYZero, const CU::Vector3f aColor = { 1.0f, 1.0f, 1.0f });

		void DrawCube(const CU::Vector3f aPos, const float aSize = 1.f, const CU::Vector3f aCubeRGB = { 1.f, 1.f, 1.f });
		void DrawCube(const float aPosX, const float aPosY, const float aPosZ
			, const float aSize = 1.0f
			, const unsigned char aR = 255, const unsigned char aG = 255, const unsigned char aB = 255);
		void DrawCube(const float aPosX, const float aPosY, const float aPosZ
			, const float aSize = 1.0f
			, const float aR = 1.0f, const float aG = 1.0f, const float aB = 1.0f);

		void DrawRectangle3D(const CU::Vector3f aPos, const CU::Vector3f aSizes = { 1.f, 1.f, 1.f }, const CU::Vector3f aCubeRGB = { 1.f, 1.f, 1.f });
		void DrawRectangle3D(const float aPosX, const float aPosY, const float aPosZ
			, const float aSizeX = 1.0f, const float aSizeY = 1.0f, const float aSizeZ = 1.0f
			, const unsigned char aR = 255, const unsigned char aG = 255, const unsigned char aB = 255);
		void DrawRectangle3D(const float aPosX, const float aPosY, const float aPosZ
			, const float aSizeX = 1.0f, const float aSizeY = 1.0f, const float aSizeZ = 1.0f
			, const float aR = 1.0f, const float aG = 1.0f, const float aB = 1.0f);

		void DrawText2D(const std::string& aText, const CU::Vector2f& aPosition = CU::Vector2f(0.0f, 0.0f), float aSize = 24.0f, const CU::Vector3f& aColor = CU::Vector3f(1.0f, 1.0f, 1.0f));
		void DrawText2D(const std::string aText
			, const float aPositionX = 0.0f, const float aPositionY = 0.0f
			, float aSize = 24.0f
			, const unsigned char aR = 255, const unsigned char aG = 255, const unsigned char aB = 255);
		void DrawText2D(const std::string& aText
			, const float aPositionX = 0.0f, const float aPositionY = 0.0f
			, float aSize = 24.0f
			, const float aR = 1.0f, const float aG = 1.0f, const float aB = 1.0f);

		void DrawSphere(const CU::Vector3f& aPos, const float aRadius = 1.0f, const CU::Vector3f& aColor = CU::Vector3f(-1.0f), const CU::Vector3f& aRotation = CU::Vector3f(),
			const unsigned int aRingCount = 10, const unsigned int aSliceCount = 10);

		const CU::GrowingArray<SSimpleDebugVertex, unsigned int>& GetDebugLineData();
		const CU::GrowingArray<SSimpleDebugVertex, unsigned int>& GetDebug2DLineData();
		const CU::GrowingArray<SSimpleDebugVertex, unsigned int>& GetDebugCubeData();
		const CU::GrowingArray<CModelInstance, unsigned int>& GetDebugSphereData();
		const CU::GrowingArray<SSimpleDebugVertex, unsigned int>& GetRectangle3DData();

		void ChangeUpdateBuffer();
		void ChangeRenderBuffer();

	private:
		struct SDebugDataBuffer
		{
			template<typename DataType>
			struct SArrayWithMutex
			{
				CU::GrowingArray<DataType, unsigned int> myData;
				std::mutex myMutex;
			};

			SArrayWithMutex<SSimpleDebugVertex> myDebugLineData;
			SArrayWithMutex<SSimpleDebugVertex> myDebugCubeData;
			SArrayWithMutex<SSimpleDebugVertex> myDebug2DLineData;
			SArrayWithMutex<SSimpleDebugVertex> myDebugRectangle3DData;
			SArrayWithMutex<CModelInstance> myDebugSphereData;

			bool myIsRead = false;

			void RemoveAll()
			{
				myDebugLineData.myData.RemoveAll();
				myDebugCubeData.myData.RemoveAll();
				myDebug2DLineData.myData.RemoveAll();
				myDebugRectangle3DData.myData.RemoveAll();
				myDebugSphereData.myData.RemoveAll();
			}
		};

		CDebugTools();
		static CDebugTools* ourInstance;
		SDebugDataBuffer myDebugDataBuffers[3];

		unsigned char myFreeIndex;
		unsigned char myReadIndex;
		unsigned char myWriteIndex;

		std::shared_mutex myBufferLock;

#endif // !USE_DEBUG_TOOLS
	};
}}