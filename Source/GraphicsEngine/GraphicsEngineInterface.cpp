#include "stdafx.h"
#include "GraphicsEngineInterface.h"
#include "GraphicsEngine.h"
#include "Scene/Scene.h"
#include "WindowHandler/WindowHandler.h"

using namespace sce::gfx;
CGraphicsEngine* CGraphicsEngineInterface::myGraphicsEngine = nullptr;
const CU::Vector2f CGraphicsEngineInterface::myTargetResolution = { 1920.f, 1080.f };

void CGraphicsEngineInterface::AddToScene(const CModelInstance& aInstance)
{
	if (myGraphicsEngine)
	{
		myGraphicsEngine->myScene.AddModelInstance(aInstance);
	}
}

void sce::gfx::CGraphicsEngineInterface::AddToScene(const CCameraInstance& aCameraInstance)
{
	if (myGraphicsEngine)
	{
		myGraphicsEngine->myScene.SetCamera(aCameraInstance);
	}
}

void sce::gfx::CGraphicsEngineInterface::AddToScene(const CSprite& aSprite)
{
	if (myGraphicsEngine)
	{
		myGraphicsEngine->myScene.AddSprite(aSprite);
	}
}

void sce::gfx::CGraphicsEngineInterface::AddToScene(const CSprite3D & a3DSprite)
{
	if (myGraphicsEngine)
	{
		myGraphicsEngine->myScene.AddSprite3D(a3DSprite);
	}
}

void sce::gfx::CGraphicsEngineInterface::AddToScene(const CPointLightInstance& aPLInstance)
{
	if (myGraphicsEngine)
	{
		myGraphicsEngine->myScene.AddPointLightInstance(aPLInstance);
	}
}

void sce::gfx::CGraphicsEngineInterface::AddToScene(const CSpotLightInstance& aSLInstance)
{
	if (myGraphicsEngine)
	{
		myGraphicsEngine->myScene.AddSpotLightInstance(aSLInstance);
	}
}

void sce::gfx::CGraphicsEngineInterface::AddToScene(const CText& aText)
{
	if (myGraphicsEngine)
	{
		myGraphicsEngine->myScene.AddText(aText);
	}
}

void sce::gfx::CGraphicsEngineInterface::AddToScene(const CShaderEffectInstance & aShaderEffectInstance)
{
	if (myGraphicsEngine)
	{
		myGraphicsEngine->myScene.AddShaderEffectInstance(aShaderEffectInstance);
	}
}

void sce::gfx::CGraphicsEngineInterface::UseForRendering(const CEnvironmentalLight& aEnvLight)
{
	if (myGraphicsEngine)
	{
		myGraphicsEngine->myScene.SetEnvironmentalLight(aEnvLight);
	}
}

void sce::gfx::CGraphicsEngineInterface::UseForRendering(const CDirectionalLight& aDirLight)
{
	if (myGraphicsEngine)
	{
		myGraphicsEngine->myScene.SetDirectionalLight(aDirLight);
	}
}

void sce::gfx::CGraphicsEngineInterface::EnableCursor()
{
	if (myGraphicsEngine)
	{
		myGraphicsEngine->myWindowHandler->EnableCursor();
	}
}

void sce::gfx::CGraphicsEngineInterface::DisableCursor()
{
	if (myGraphicsEngine)
	{
		myGraphicsEngine->myWindowHandler->DisableCursor();
	}
}

bool sce::gfx::CGraphicsEngineInterface::LoadCustomCursor(const unsigned char& aIDToUse, const std::string& aCursorPath)
{
	constexpr char* preErrorMessage("ERROR! Failed to load custom cursor. Message: ");
	preErrorMessage;
	std::string errorMessage;

	if (myGraphicsEngine)
	{
		if (myGraphicsEngine->myWindowHandler->LoadCustomCursor(aIDToUse, aCursorPath, errorMessage) == true)
		{
			return true;
		}
	}
	else
	{
		errorMessage = "Graphics engine not set";
	}

	ENGINE_LOG((preErrorMessage + errorMessage).c_str());
	return false;
}

bool sce::gfx::CGraphicsEngineInterface::UnloadCustomCursor(const unsigned char& aID)
{
	constexpr char* preErrorMessage("ERROR! Failed to unload custom cursor. Message: ");
	preErrorMessage;
	std::string errorMessage;

	if (myGraphicsEngine)
	{
		if (myGraphicsEngine->myWindowHandler->UnloadCustomCursor(aID, errorMessage) == true)
		{
			return true;
		}
	}
	else
	{
		errorMessage = "Graphics engine not set";
	}

	ENGINE_LOG((preErrorMessage + errorMessage).c_str());
	return false;
}

bool sce::gfx::CGraphicsEngineInterface::SetCustomCursor(const unsigned char& aID)
{
	constexpr char* preErrorMessage("ERROR! Failed to set custom cursor. Message: ");
	preErrorMessage;
	std::string errorMessage;

	if (myGraphicsEngine)
	{
		if (myGraphicsEngine->myWindowHandler->SetCustomCursor(aID, errorMessage) == true)
		{
			return true;
		}
	}
	else
	{
		errorMessage = "Graphics engine not set";
	}

	ENGINE_LOG((preErrorMessage + errorMessage).c_str());
	return false;
}

const bool sce::gfx::CGraphicsEngineInterface::IsFullscreen()
{
	if (myGraphicsEngine)
	{
		return myGraphicsEngine->myWindowHandler->IsFullscreen();
	}

	return false;
}

void sce::gfx::CGraphicsEngineInterface::ToggleFullscreen()
{
	if (myGraphicsEngine)
	{
		myGraphicsEngine->ToggleFullscreen();
	}
}

void sce::gfx::CGraphicsEngineInterface::TakeScreenshot(const wchar_t* aDestinationPath)
{
	if (myGraphicsEngine)
	{
		myGraphicsEngine->TakeScreenshot(aDestinationPath);
	}
}

const CU::Vector2f sce::gfx::CGraphicsEngineInterface::GetResolution()
{
	if (myGraphicsEngine)
	{
		return myGraphicsEngine->GetResolution();
	}
	else
	{
		return CU::Vector2f();
	}
}

const CU::Vector2f sce::gfx::CGraphicsEngineInterface::GetFullResolution()
{
	if (myGraphicsEngine)
	{
		return myGraphicsEngine->myWindowHandler->GetFullResolution();
	}
	else
	{
		return CU::Vector2f();
	}
}

const CU::Vector2f sce::gfx::CGraphicsEngineInterface::GetWindowedResolution()
{
	if (myGraphicsEngine)
	{
		return myGraphicsEngine->myWindowHandler->GetWindowedResolution();
	}
	else
	{
		return CU::Vector2f();
	}
}

const CU::Vector2f sce::gfx::CGraphicsEngineInterface::GetFullWindowedResolution()
{
	if (myGraphicsEngine)
	{
		return myGraphicsEngine->myWindowHandler->GetFullWindowedResolution();
	}
	else
	{
		return CU::Vector2f();
	}
}

const CU::Vector2f& sce::gfx::CGraphicsEngineInterface::GetTargetResolution()
{
	return myTargetResolution;
}

const float sce::gfx::CGraphicsEngineInterface::GetRatio()
{
	if (myGraphicsEngine)
	{
		return myGraphicsEngine->GetRatio();
	}
	else
	{
		return 0.0f;
	}
}

void sce::gfx::CGraphicsEngineInterface::ToggleSSAO()
{
	if (myGraphicsEngine)
	{
		myGraphicsEngine->ToggleSSAO();
	}
}
void sce::gfx::CGraphicsEngineInterface::ToggleColorGrading()
{
	//if (myGraphicsEngine)
	//{
	//	myGraphicsEngine->ToggleColorGrading();
	//}
}
void sce::gfx::CGraphicsEngineInterface::ToggleLinearFog()
{
	if (myGraphicsEngine)
	{
		myGraphicsEngine->ToggleLinearFog();
	}
}
void sce::gfx::CGraphicsEngineInterface::ToggleFXAA()
{
	if (myGraphicsEngine)
	{
		myGraphicsEngine->ToggleFXAA();
	}
}
void sce::gfx::CGraphicsEngineInterface::ToggleBloom()
{
	if (myGraphicsEngine)
	{
		myGraphicsEngine->ToggleBloom();
	}
}

void sce::gfx::CGraphicsEngineInterface::SetVSync(const bool aUseVsync)
{
	if (myGraphicsEngine)
	{
		myGraphicsEngine->SetVsync(aUseVsync);
	}
}

void sce::gfx::CGraphicsEngineInterface::SetSSAO(const bool aBool)
{
	if (myGraphicsEngine)
	{
		myGraphicsEngine->SetSSAO(aBool);
	}
}
void sce::gfx::CGraphicsEngineInterface::SetLinearFog(const bool aBool)
{
	if (myGraphicsEngine)
	{
		myGraphicsEngine->SetLinearFog(aBool);
	}
}
void sce::gfx::CGraphicsEngineInterface::SetBloom(const bool aBool)
{
	if (myGraphicsEngine)
	{
		myGraphicsEngine->SetBloom(aBool);
	}
}
void sce::gfx::CGraphicsEngineInterface::SetFXAA(const bool aBool)
{
	if (myGraphicsEngine)
	{
		myGraphicsEngine->SetFXAA(aBool);
	}
}
void sce::gfx::CGraphicsEngineInterface::SetConsoleRenderCallback(std::function<void()> aFunc)
{
	if (myGraphicsEngine)
	{
		myGraphicsEngine->SetConsoleRenderCallback(aFunc);
	}
}
void sce::gfx::CGraphicsEngineInterface::SetResolution(const CU::Vector2f& aNewResolution)
{
	if (myGraphicsEngine)
	{
		myGraphicsEngine->SetResolution(aNewResolution);
	}
}

const bool sce::gfx::CGraphicsEngineInterface::IsVSyncEnabled()
{
	if (myGraphicsEngine)
	{
		return myGraphicsEngine->IsVsyncEnabled();
	}

	return false;
}

const bool sce::gfx::CGraphicsEngineInterface::IsSSAOEnabled()
{
	if (myGraphicsEngine)
	{
		return myGraphicsEngine->IsSSAOEnabled();
	}

	return false;
}

const bool sce::gfx::CGraphicsEngineInterface::IsColorGradingEnabled()
{
	if (myGraphicsEngine)
	{
		return myGraphicsEngine->IsColorGradingEnabled();
	}

	return false;
}

const bool sce::gfx::CGraphicsEngineInterface::IsLinearFogEnabled()
{
	if (myGraphicsEngine)
	{
		return myGraphicsEngine->IsLinearFogEnabled();
	}

	return false;
}

const bool sce::gfx::CGraphicsEngineInterface::IsBloomEnabled()
{
	if (myGraphicsEngine)
	{
		return myGraphicsEngine->IsBloomEnabled();
	}

	return false;
}

const bool sce::gfx::CGraphicsEngineInterface::IsFXAAEnabled()
{
	if (myGraphicsEngine)
	{
		return myGraphicsEngine->IsFXAAEnabled();
	}

	return false;
}

void sce::gfx::CGraphicsEngineInterface::ToggleVSync()
{
	if (myGraphicsEngine)
	{
		myGraphicsEngine->ToggleVsync();
	}
}
