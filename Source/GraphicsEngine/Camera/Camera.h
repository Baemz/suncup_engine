#pragma once
#include "Frustum/Frustum.h"

namespace sce { namespace gfx {

	class CFrustumCollider;
	class CCameraInstance;
	class CCamera
	{
		friend class CCameraFactory;
		friend class CForwardRenderer;
		friend class CDeferredRenderer;
		friend class CDeferredRendererGI;
		friend class CSpriteRenderer3D;
		friend class CParticleRenderer;
		friend class CDebugRenderer;
		friend class CSkyboxRenderer;
		friend class CInputComponent;
		friend class CScene;

	public:
		CCamera();
		~CCamera();

		bool Init();
		void UpdateBuffers(const CU::Matrix44f& aCameraOrientation);

		const CU::Matrix44f& GetProjection() { return myCBufferData.myProjection; }
		const CU::Matrix44f& GetInverseProjection() { return myInvProjection; }
		std::array<CU::Vector3f, 8> GetCorners();
		const float GetFar() const;
		const float GetNear() const;
		const float GetFov() const;

	private:
		struct SCameraBufferData
		{
			CU::Matrix44f myCameraOrientation;
			CU::Matrix44f myView;
			CU::Matrix44f myProjection;
			CU::Vector4f myCameraPosition;
		};
	private:
		SCameraBufferData myCBufferData;
		CFrustum myFrustum;
		CU::Matrix44f myInvProjection;
		unsigned int myCBufferID;
		float myFOV;
		float myNear;
		float myFar;

		void CreateProjection(const float aNearPlane = 0.001f, const float aFarPlane = 1000.f);
		bool Intersects(CFrustumCollider& aCollider); // Overload with the different types of colliders.
		bool Intersects(const CU::Vector3f aPosition, const float aRadius);
	};

}}