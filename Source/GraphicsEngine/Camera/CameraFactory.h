#pragma once
#include <vector>
#include "../CommonUtilities/Vector.h"

namespace sce { namespace gfx {

	class CCameraInstance;
	class CScene;
	class CCameraFactory
	{
	public:
		~CCameraFactory();

		static void Create(CScene* aScene);
		static void Destroy();
		static CCameraFactory* Get() { return ourInstance; };

		// Currently only creates one type of camera.
		CCameraInstance CreateCameraAtPosition(const CU::Vector3f& aPos, float aFOV = 90.f);

	private:
		CCameraFactory(CScene* aScene);
		static CCameraFactory* ourInstance;

		CScene* myScene;
	};

}}
