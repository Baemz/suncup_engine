#include "stdafx.h"
#include "Camera.h"
#include "../Framework/Framework.h"
#include "../GraphicsEngineInterface.h"
#include "../DirectXFramework/CDirectXMathHelper.h"

#define PIf       3.14159265358979323846
#define DEGREES_TO_RADIANS static_cast<float>(PIf / 180.0f)
#define CAM_NEAR 0.01f
#define CAM_FAR 1000.f

using namespace sce::gfx;

CCamera::CCamera()
	: myFrustum(myCBufferData.myCameraOrientation, myCBufferData.myProjection, CAM_NEAR, CAM_FAR)
	, myNear(CAM_NEAR)
	, myFar(CAM_FAR)
{
}


CCamera::~CCamera()
{
}

bool sce::gfx::CCamera::Init()
{
	CreateProjection(CAM_NEAR, CAM_FAR);
	myFrustum.SetFov(myFOV);
	myFrustum.Update();
	myInvProjection = myCBufferData.myProjection.GetProperInverse();
	return true;
}

void sce::gfx::CCamera::UpdateBuffers(const CU::Matrix44f& aCameraOrientation)
{
	myCBufferData.myCameraOrientation = aCameraOrientation;
	myCBufferData.myView = myCBufferData.myCameraOrientation.GetProperInverse();
	CU::Vector3f pos = aCameraOrientation.GetPosition();
	myCBufferData.myCameraPosition = { pos.x, pos.y, pos.z, 1.0f };
	myFrustum.Update();
}

std::array<CU::Vector3f, 8> sce::gfx::CCamera::GetCorners()
{
	std::array<CU::Vector3f, 8> corners;

	CU::Vector3f topLeftNear = myFrustum.GetMinCorner();
	CU::Vector3f bottomRightFar = myFrustum.GetMaxCorner();

	CU::Vector3f topRightNear = { bottomRightFar.x, topLeftNear.y, topLeftNear.z };
	CU::Vector3f bottomLeftNear = { topLeftNear.x, bottomRightFar.y, topLeftNear.z };
	CU::Vector3f bottomRightNear = { topLeftNear.x, bottomRightFar.y, topLeftNear.z };

	CU::Vector3f topRightFar = { bottomRightFar.x, topLeftNear.y, bottomRightFar.z };
	CU::Vector3f bottomLeftFar = { topLeftNear.x, bottomRightFar.y, bottomRightFar.z };
	CU::Vector3f topLeftFar = { topLeftNear.x, topLeftNear.y, bottomRightFar.z };

	corners[0] = topLeftNear;
	corners[1] = topRightNear;
	corners[2] = bottomLeftNear;
	corners[3] = bottomRightNear;
	corners[4] = topLeftFar;
	corners[5] = topRightFar;
	corners[6] = bottomLeftFar;
	corners[7] = bottomRightFar;

	return corners;
}

const float sce::gfx::CCamera::GetFar() const
{
	return myFar;
}

const float sce::gfx::CCamera::GetNear() const
{
	return myNear;
}

const float sce::gfx::CCamera::GetFov() const
{
	return myFOV;
}

void sce::gfx::CCamera::CreateProjection(const float aNearPlane, const float aFarPlane)
{
	if (CFramework::GetRenderAPI() == eRenderAPI::DirectX11)
	{
		CU::Vector2f resolution = CGraphicsEngineInterface::GetResolution();
		myCBufferData.myProjection = myCBufferData.myProjection.CreateProjectionMatrix(aNearPlane, aFarPlane, resolution.x, resolution.y, myFOV * DEGREES_TO_RADIANS);
	}
}

bool sce::gfx::CCamera::Intersects(CFrustumCollider& aCollider)
{
	return myFrustum.Intersects(aCollider);
}

bool sce::gfx::CCamera::Intersects(const CU::Vector3f aPosition, const float aRadius)
{
	return myFrustum.Intersects(aPosition, aRadius);
}
