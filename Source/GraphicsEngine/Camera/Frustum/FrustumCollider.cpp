#include "stdafx.h"
#include "FrustumCollider.h"

sce::gfx::CFrustumCollider::CFrustumCollider()
	: myCenter(0.f, 0.f, 0.f)
	, myRadius(1000.f)
{
}

sce::gfx::CFrustumCollider::~CFrustumCollider()
{
}

sce::gfx::CFrustumCollider::CFrustumCollider(const CU::Vector3f& aPostion, const float aRadius)
{
	myCenter = aPostion;
	myRadius = aRadius;
}

void sce::gfx::CFrustumCollider::SetRadius(const float aRadius)
{
	myRadius = aRadius;
}

void sce::gfx::CFrustumCollider::SetPosition(const CU::Matrix44f& aPostion)
{
	CU::Vector3f pos(aPostion.GetPosition());
	CU::Vector3f change(aPostion * myCenterOffset);

	myCenter = pos + change;
}

void sce::gfx::CFrustumCollider::SetOffset(const CU::Vector3f& aPostion)
{
	myCenterOffset = aPostion;
}

const float sce::gfx::CFrustumCollider::GetRadius() const
{
	return myRadius;
}

const CU::Vector3f & sce::gfx::CFrustumCollider::GetPosition() const
{
	return myCenter;
}
