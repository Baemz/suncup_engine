#pragma once
#include "../../../CommonUtilities/Matrix44.h"
#include "../../../CommonUtilities/FovFrustum.h"

namespace sce { namespace gfx {
	class CFrustumCollider;
	class CFrustum
	{
		friend class CCamera;
	public:
		~CFrustum();

		void Update();
		void SetFov(const float aFov);

		bool Intersects(CFrustumCollider& aCollider); // Overload with different types of bounding-colliders
		bool Intersects(const CU::Vector3f& aPosition, const float aRadius);

		const CU::Vector3f GetMinCorner() const;
		const CU::Vector3f GetMaxCorner() const;

	private:
		const CU::Vector3f CalcMinCorner() const;
		const CU::Vector3f CalcMaxCorner() const;

	private:
		CFrustum() = delete;
		CFrustum(const CommonUtilities::Matrix44<float>& aCameraOrientation, const CommonUtilities::Matrix44<float>& aProjection, float aNear, float aFar);
		const CommonUtilities::Matrix44<float>& myOrientation;
		const CommonUtilities::Matrix44<float>& myProjection;
		CommonUtilities::Matrix44<float> myOrientationInverse;
		CommonUtilities::Matrix44<float> myProjectionInverse;
		CU::Intersection::FovFrustum myFrustum90; 
		CU::Vector4f myUpLeft;
		CU::Vector4f myUpRight;
		CU::Vector4f myDownLeft;
		CU::Vector4f myDownRight;
		CU::Vector3f myCornerMin;
		CU::Vector3f myCornerMax;
		float myFar;
		float myNear;
	};

}}