#pragma once
#include "..\CommonUtilities\Vector.h"

namespace sce { namespace gfx {

	class CFrustumCollider
	{
		friend class CFrustum; 
	public:
		CFrustumCollider();
		~CFrustumCollider();
		CFrustumCollider(const CU::Vector3f& aPostion, const float aRadius);

		void SetRadius(const float aRadius);
		void SetPosition(const CU::Matrix44f& aPostion);
		void SetOffset(const CU::Vector3f& aPostion);

		const float GetRadius() const;
		const CU::Vector3f& GetPosition() const;

	private:
		CU::Vector3f myCenterOffset;
		CU::Vector3f myCenter;
		float myRadius;
	};
}}