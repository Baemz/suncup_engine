#include "stdafx.h"
#include "Frustum.h"
#include "../../../CommonUtilities/Matrix44.h"
#include "FrustumCollider.h"

namespace sce { namespace gfx {

	CFrustum::CFrustum(const CommonUtilities::Matrix44<float>& aCameraOrientation, const CommonUtilities::Matrix44<float>& aProjection, float aNear, float aFar)
		: myOrientation(aCameraOrientation)
		, myProjection(aProjection)
		, myFrustum90(aNear, aFar, 90.f)
		, myFar(aFar)
		, myNear(aNear)
	{
		myUpLeft = CU::Vector4f(-aFar, +aFar, +aFar, 1.f);
		myUpRight = CU::Vector4f(+aFar, +aFar, +aFar, 1.f);
		myDownLeft = CU::Vector4f(-aFar, -aFar, +aFar, 1.f);
		myDownRight = CU::Vector4f(+aFar, -aFar, +aFar, 1.f);
	}

	CFrustum::~CFrustum()
	{
	}

	void CFrustum::Update()
	{
		myOrientationInverse = myOrientation.GetFastInverse();
		myProjectionInverse = myProjection.GetFastInverse();
		myCornerMin = CalcMinCorner();
		myCornerMax = CalcMaxCorner();
	}

	void CFrustum::SetFov(const float aFov)
	{
		myFrustum90 = CU::Intersection::FovFrustum(myNear, myFar, aFov);
	}

	bool CFrustum::Intersects(CFrustumCollider& aCollider)
	{
		CU::Vector4<float> position = CU::Vector4<float>(aCollider.myCenter.x, aCollider.myCenter.y, aCollider.myCenter.z, 1.f) * myOrientationInverse * myProjectionInverse;
		return myFrustum90.Inside({ position.x, position.y, position.z }, aCollider.myRadius);
	}

	bool CFrustum::Intersects(const CU::Vector3f& aPosition, const float aRadius)
	{
		CU::Vector4<float> position = CU::Vector4<float>(aPosition.x, aPosition.y, aPosition.z, 1.f) * myOrientationInverse * myProjectionInverse;
		return myFrustum90.Inside({ position.x, position.y, position.z }, aRadius);
	}

	const CU::Vector3f CFrustum::GetMinCorner() const
	{
		return myCornerMin;
	}

	const CU::Vector3f CFrustum::GetMaxCorner() const
	{
		return myCornerMax;
	}

	const CU::Vector3f CFrustum::CalcMinCorner() const
	{
		CU::Vector4f result = myOrientation.GetTranslation();

		result.x = fminf(result.x, (myUpLeft * myOrientation).x);
		result.y = fminf(result.y, (myUpLeft * myOrientation).y);
		result.z = fminf(result.z, (myUpLeft * myOrientation).z);

		result.x = fminf(result.x, (myUpRight * myOrientation).x);
		result.y = fminf(result.y, (myUpRight * myOrientation).y);
		result.z = fminf(result.z, (myUpRight * myOrientation).z);

		result.x = fminf(result.x, (myDownLeft * myOrientation).x);
		result.y = fminf(result.y, (myDownLeft * myOrientation).y);
		result.z = fminf(result.z, (myDownLeft * myOrientation).z);

		result.x = fminf(result.x, (myDownRight * myOrientation).x);
		result.y = fminf(result.y, (myDownRight * myOrientation).y);
		result.z = fminf(result.z, (myDownRight * myOrientation).z);

		return result;
	}

	const CU::Vector3f CFrustum::CalcMaxCorner() const
	{
		CU::Vector4f result = myOrientation.GetTranslation();

		result.x = fmaxf(result.x, (myUpLeft * myOrientation).x);
		result.y = fmaxf(result.y, (myUpLeft * myOrientation).y);
		result.z = fmaxf(result.z, (myUpLeft * myOrientation).z);

		result.x = fmaxf(result.x, (myUpRight * myOrientation).x);
		result.y = fmaxf(result.y, (myUpRight * myOrientation).y);
		result.z = fmaxf(result.z, (myUpRight * myOrientation).z);

		result.x = fmaxf(result.x, (myDownLeft * myOrientation).x);
		result.y = fmaxf(result.y, (myDownLeft * myOrientation).y);
		result.z = fmaxf(result.z, (myDownLeft * myOrientation).z);

		result.x = fmaxf(result.x, (myDownRight * myOrientation).x);
		result.y = fmaxf(result.y, (myDownRight * myOrientation).y);
		result.z = fmaxf(result.z, (myDownRight * myOrientation).z);

		return result;
	}
}}
