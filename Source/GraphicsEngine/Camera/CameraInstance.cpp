#include "stdafx.h"
#include "CameraInstance.h"
#include "GraphicsEngineInterface.h"

#include <iostream>

// For listener position update
#include "../Game/AudioWrapper.h"
#include "DebugTools.h"

//For screenpos purposes
#include "Camera.h"
#include "ResourceManager/ResourceManager.h"

//For shake
#include "../CommonUtilities/Random.h"

using namespace sce::gfx;

CCameraInstance::CCameraInstance()
	: myCameraID(UINT_MAX)
	, mySpeed(0.01f)
	, myScreenShakeTimer(0.0f)
	, myHasResetPostShake(true)
{
	myRight.x = myMatrixOrientation[0];
	myRight.y = myMatrixOrientation[1];
	myRight.z = myMatrixOrientation[2];
	myUp.x = myMatrixOrientation[4];
	myUp.y = myMatrixOrientation[5];
	myUp.z = myMatrixOrientation[6];
	myLook.x = myMatrixOrientation[8];
	myLook.y = myMatrixOrientation[9];
	myLook.z = myMatrixOrientation[10];
}


CCameraInstance::~CCameraInstance()
{
}

void sce::gfx::CCameraInstance::SetPosition(const CU::Vector3f& aPosition)
{
	myMatrixOrientation.SetPosition(aPosition);
	UpdateListenerPosition();
}

void sce::gfx::CCameraInstance::Move(const CU::Vector3f & aAxis, float aSpeed)
{
	myMatrixOrientation.SetPosition(myMatrixOrientation.GetPosition() + (aAxis * aSpeed));
	UpdateListenerPosition();
}

void sce::gfx::CCameraInstance::RotateWorld(const CU::Vector3f & aRotation)
{
	CU::Quaternion quatPreTransformation;
	quatPreTransformation.RotateWorld(aRotation);
	myQuatOrientation *= quatPreTransformation;
	AdjustRotation();
	UpdateListenerPosition();
}

void sce::gfx::CCameraInstance::RotateLocal(const CU::Vector3f & aRotation)
{
	CU::Quaternion quatPreTransformation;
	quatPreTransformation.RotateLocal(aRotation);
	myQuatOrientation *= quatPreTransformation;
	AdjustRotation();
	UpdateListenerPosition();
}

void sce::gfx::CCameraInstance::SlerpRotation(const CU::Quaternion & aStart, const CU::Quaternion & aEnd, const float aPercentage)
{
	myQuatOrientation = CU::Quaternion::Slerp(aStart, aEnd, aPercentage);
	AdjustRotation();
	UpdateListenerPosition();
}

void sce::gfx::CCameraInstance::LookAt(const CU::Vector3f & aStart, const CU::Vector3f & aEnd)
{
	myQuatOrientation = CU::Quaternion::LookAt(aStart, aEnd);
	AdjustRotation();
	UpdateListenerPosition();
}

void sce::gfx::CCameraInstance::SetOrientation(const CU::Quaternion & aOrientation)
{
	myQuatOrientation = aOrientation;
	AdjustRotation();
	UpdateListenerPosition();
}

void sce::gfx::CCameraInstance::ShakeOverTime(const float aTime, const CU::Vector3f& aIntensity)
{
	myScreenShakeTimer = aTime;
	myShakeIntensity = aIntensity;
	myShakeOffset = { 0.0f };
	myHasResetPostShake = false;
}

void sce::gfx::CCameraInstance::StopShake()
{
	myScreenShakeTimer = 0.0f;
	myShakeOffset = { 0.0f };
}

/* PRIVATE FUNCTIONS */

void sce::gfx::CCameraInstance::UpdateListenerPosition() const
{
#ifndef _MATEDIT
	CAudioWrapper::SetListenerPositionAndOrientation(myMatrixOrientation.GetPosition(), myMatrixOrientation.GetFront(), myMatrixOrientation.GetTop());
#endif
}

void sce::gfx::CCameraInstance::UpdateScreenShake(const float aDeltaTime)
{
	if (myScreenShakeTimer > 0.0f)
	{
		myScreenShakeTimer -= aDeltaTime;
		float quakeAmtX = (CU::GetRandomPercentage() / 100.0f) * std::sin(myShakeIntensity.x) * 2 - myShakeIntensity.x;
		float quakeAmtY = (CU::GetRandomPercentage() / 100.0f) * std::sin(myShakeIntensity.y) * 2 - myShakeIntensity.y;
		float quakeAmtZ = (CU::GetRandomPercentage() / 100.0f) * std::sin(myShakeIntensity.z) * 2 - myShakeIntensity.z;
		myShakeOffset += myRight * quakeAmtX;
		myShakeOffset += myUp * quakeAmtY;
		myShakeOffset += myLook * quakeAmtZ;
	}
	else if (!myHasResetPostShake)
	{
		StopShake();
	}
}

void sce::gfx::CCameraInstance::AdjustRotation()
{
	myMatrixOrientation.SetRotation(myQuatOrientation.GenerateMatrix());

	//Reset local basis vectors
	myRight.x = myMatrixOrientation[0];
	myRight.y = myMatrixOrientation[1];
	myRight.z = myMatrixOrientation[2];
	myUp.x = myMatrixOrientation[4];
	myUp.y = myMatrixOrientation[5];
	myUp.z = myMatrixOrientation[6];
	myLook.x = myMatrixOrientation[8];
	myLook.y = myMatrixOrientation[9];
	myLook.z = myMatrixOrientation[10];
}

void sce::gfx::CCameraInstance::Update(const float aDeltaTime)
{
	UpdateScreenShake(aDeltaTime);

	//Local basis vectors
	myRight.x = myMatrixOrientation[0];
	myRight.y = myMatrixOrientation[1];
	myRight.z = myMatrixOrientation[2];
	myRight.Normalize();
	myUp.x = myMatrixOrientation[4];
	myUp.y = myMatrixOrientation[5];
	myUp.z = myMatrixOrientation[6];
	myUp.Normalize();
	myLook.x = myMatrixOrientation[8];
	myLook.y = myMatrixOrientation[9];
	myLook.z = myMatrixOrientation[10];
	myLook.Normalize();
}

void sce::gfx::CCameraInstance::UseForRendering()
{
	//Quick fix for sure, but at least the camera's position remains constant.
	myMatrixOrientation.SetPosition(myMatrixOrientation.GetPosition() + myShakeOffset);
	CGraphicsEngineInterface::AddToScene(*this);
	myMatrixOrientation.SetPosition(myMatrixOrientation.GetPosition() - myShakeOffset);
}

const CU::Vector2f sce::gfx::CCameraInstance::GetScreenPosition(const CU::Vector3f& aWorldPosition) const
{
	CU::Vector4f viewPos = myMatrixOrientation.GetFastInverse() * CU::Vector4f(aWorldPosition, 1.0f);
	CU::Vector4f projectionPos = GetProjection() * viewPos;
	projectionPos.x /= projectionPos.w;
	projectionPos.y /= projectionPos.w;
	return { projectionPos.x, projectionPos.y };
}

const CU::Vector3f sce::gfx::CCameraInstance::GetPosition() const
{
	return myMatrixOrientation.GetPosition();
}

const CU::Vector3f& sce::gfx::CCameraInstance::GetLook() const
{
	return myLook;
}

const CU::Vector3f& sce::gfx::CCameraInstance::GetRight() const
{
	return myRight;
}

const CU::Vector3f& sce::gfx::CCameraInstance::GetUp() const
{
	return myUp;
}

const CU::Matrix44f& sce::gfx::CCameraInstance::GetOrientation() const
{
	return myMatrixOrientation;
}

const CU::Matrix44f& sce::gfx::CCameraInstance::GetProjection() const
{
	return CResourceManager::Get()->GetCameraWithID(myCameraID).GetProjection();
}
