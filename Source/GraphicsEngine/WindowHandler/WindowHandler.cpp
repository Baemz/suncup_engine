#include "stdafx.h"
#include "WindowHandler.h"
#include "WindowData.h"

#include "../GraphicsEngineInterface.h"
#include "../Launcher/resource.h"

using namespace sce::gfx;

CWindowHandler::CWindowHandler()
{
}


CWindowHandler::~CWindowHandler()
{
	std::string errorMessage;
	for (auto& cursor : myCursors)
	{
		if (UnloadCustomCursorImpl(cursor.second, errorMessage) == false)
		{
// 			for (auto& currentChar : errorMessage)
// 			{
// 				if ((currentChar == '�') || (currentChar == '�'))
// 				{
// 					currentChar = '*';
// 				}
// 				else if ((currentChar == '�') || (currentChar == '�'))
// 				{
// 					currentChar = '�';
// 				}
// 				else if ((currentChar == '�') || (currentChar == '�'))
// 				{
// 					currentChar = '"';
// 				}
// 			}
// 
// 			ENGINE_LOG("ERROR! Failed to destroy cursor ID (%u). Message: %s"
// 				, cursor.first, errorMessage.c_str());
		}
	}

	myCursors.clear();
}


LRESULT CALLBACK CWindowHandler::WinProc(HWND aWindowHandle, UINT aWinMessage, WPARAM aWPARAM, LPARAM aLPARAM)
{
	if (aWinMessage == WM_DESTROY || aWinMessage == WM_CLOSE)
	{
		PostQuitMessage(0);
		return 0;
	}
	else if (aWinMessage == WM_SIZE || aWinMessage == WM_MOVE)
	{
		return 0;
	}
	return DefWindowProc(aWindowHandle, aWinMessage, aWPARAM, aLPARAM);
}

bool CWindowHandler::Init(const SWindowData& someWindowData)
{
	WNDCLASS windowClass = {};
	windowClass.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
	windowClass.lpfnWndProc = CWindowHandler::WinProc;
	//windowClass.hCursor = LoadCursor(nullptr, IDC_ARROW);
	HINSTANCE hInstance(GetModuleHandleW(nullptr));
	if (hInstance != nullptr)
	{
		windowClass.hIcon = (HICON)LoadImageW(hInstance, MAKEINTRESOURCEW(IDI_ICON1), IMAGE_ICON, 0, 0, LR_DEFAULTSIZE);
	}
	ShowCursor(FALSE);
	windowClass.lpszClassName = "SCEEngine";
	RegisterClass(&windowClass);

	DWORD dwStyle(0);
	// Overlapped
	dwStyle = WS_OVERLAPPED | WS_SYSMENU | WS_MINIMIZEBOX | WS_VISIBLE;
	myIsBorderless = false;

	myIsFullscreen = someWindowData.myStartInFullScreen;

	HWND hwnd = nullptr;
	hwnd = reinterpret_cast<HWND>(someWindowData.myHWND);

	if (hwnd == nullptr)
	{
		myHWND = CreateWindow("SCEEngine",
			someWindowData.myWindowName.c_str(),
			dwStyle,
			0,
			0,
			someWindowData.myWidth,
			someWindowData.myHeight,
			nullptr, nullptr, nullptr, nullptr);
	}
	else
	{
		myHWND = hwnd;
	}
	

	SetResolution(CU::Vector2ui(someWindowData.myWidth, someWindowData.myHeight));

	if (myIsFullscreen == true)
	{
		RECT clipy;
		GetWindowRect(myHWND, &clipy);
		ClipCursor(&clipy);
	}

	ShowWindow(myHWND, SW_SHOW);
	SetForegroundWindow(myHWND);
	SetFocus(myHWND);

	return true;
}

void CWindowHandler::SetResolution(CU::Vector2ui aResolution)
{
	::SetWindowPos(myHWND, nullptr, 0, 0, (int)aResolution.x, (int)aResolution.y, SWP_NOMOVE | SWP_NOOWNERZORDER | SWP_NOZORDER);
	myFullscreenResolution.x = (float)GetSystemMetrics(SM_CXSCREEN);
	myFullscreenResolution.y = (float)GetSystemMetrics(SM_CYSCREEN);

	if (myIsBorderless)
	{
		myWindowedResolution = myFullscreenResolution;
	}
	else 
	{
		RECT r;
		GetClientRect(myHWND, &r);
		const int horizontal(r.right - r.left);
		const int vertical(r.bottom - r.top);

		myWindowedResolution.x = (float)horizontal;
		myWindowedResolution.y = (float)vertical;
	}

	//myWidth = (float)aResolution.x;
	//myHeight = (float)aResolution.y;
	myFullWindowedResolution.x = (float)aResolution.x;
	myFullWindowedResolution.y = (float)aResolution.y;
}

HWND CWindowHandler::GetWindowHandle()
{
	return myHWND;
}

const CU::Vector2f& sce::gfx::CWindowHandler::GetValidResolution() const
{
	if (myIsFullscreen) return myFullscreenResolution;
	else return myWindowedResolution;
}

const CU::Vector2f& sce::gfx::CWindowHandler::GetFullResolution() const
{
	return myFullscreenResolution;
}

const CU::Vector2f& sce::gfx::CWindowHandler::GetFullWindowedResolution() const
{
	return myFullWindowedResolution;
}

const CU::Vector2f& sce::gfx::CWindowHandler::GetWindowedResolution() const
{
	return myWindowedResolution;
}

void sce::gfx::CWindowHandler::SetFullscreenState(const bool aIsFullscreen)
{
	myIsFullscreen = aIsFullscreen;

	if (myIsFullscreen == true)
	{
		RECT clipy;
		GetWindowRect(myHWND, &clipy);
		ClipCursor(&clipy);
	}
	else
	{
		ClipCursor(nullptr);
	}
}

void sce::gfx::CWindowHandler::EnableCursor() const
{
	while (ShowCursor(TRUE) < 0) continue;
}

void sce::gfx::CWindowHandler::DisableCursor() const
{
	while (ShowCursor(FALSE) >= 0) continue;
}

bool sce::gfx::CWindowHandler::LoadCustomCursor(const unsigned char& aIDToUse, const std::string& aCursorPath, std::string& aErrorMessage)
{
	if (myCursors.find(aIDToUse) == myCursors.end())
	{
		HANDLE cursorHandle(LoadImageA(nullptr, aCursorPath.c_str(), IMAGE_CURSOR, 0, 0, LR_LOADFROMFILE | LR_DEFAULTSIZE));
		if (cursorHandle == nullptr)
		{
			aErrorMessage = "";

			int error(GetLastError());
			char buffer[512];
			if (FormatMessageA(FORMAT_MESSAGE_IGNORE_INSERTS | FORMAT_MESSAGE_FROM_SYSTEM,
				nullptr, error, 0, buffer, sizeof(buffer) / sizeof(char), nullptr) == 0)
			{
				aErrorMessage += "Failed to get error message";
			}
			else
			{
				aErrorMessage += buffer;
			}

			return false;
		}

		myCursors.insert({ aIDToUse, static_cast<HCURSOR>(cursorHandle) });

		return true;
	}
	else
	{
		aErrorMessage = "Cursor ID(" + std::to_string(aIDToUse) + ") already loaded";
		return false;
	}
}

bool sce::gfx::CWindowHandler::UnloadCustomCursor(const unsigned char& aID, std::string& aErrorMessage)
{
	auto cursorIterator(myCursors.find(aID));

	if (cursorIterator != myCursors.end())
	{
		UnloadCustomCursorImpl(cursorIterator->second, aErrorMessage);
		return true;
	}
	else
	{
		aErrorMessage = "Cursor ID(" + std::to_string(aID) + ") not loaded";
		return false;
	}
}

bool sce::gfx::CWindowHandler::SetCustomCursor(const unsigned char& aID, std::string& aErrorMessage)
{
	auto cursorIterator(myCursors.find(aID));

	if (cursorIterator != myCursors.end())
	{
		SetCursor(cursorIterator->second);
		SetClassLongPtr(myHWND, GCLP_HCURSOR, reinterpret_cast<LONG_PTR>(cursorIterator->second));
		return true;
	}
	else
	{
		aErrorMessage = "Cursor ID(" + std::to_string(aID) + ") not loaded";
		return false;
	}
}

bool sce::gfx::CWindowHandler::UnloadCustomCursorImpl(HCURSOR& aCursorHandle, std::string& aErrorMessage)
{
	if (DestroyCursor(aCursorHandle) == FALSE)
	{
		aErrorMessage;

		int error(GetLastError());
		char buffer[512];
		if (FormatMessageA(FORMAT_MESSAGE_IGNORE_INSERTS | FORMAT_MESSAGE_FROM_SYSTEM,
			nullptr, error, 0, buffer, sizeof(buffer) / sizeof(char), nullptr) == 0)
		{
			aErrorMessage += "Failed to get error message";
		}
		else
		{
			aErrorMessage += buffer;
		}

		return false;
	}

	aCursorHandle = nullptr;
	return true;
}
