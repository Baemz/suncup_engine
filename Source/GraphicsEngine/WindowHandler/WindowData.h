#pragma once
#include <string>

namespace sce {namespace gfx {
	struct SWindowData
	{
		std::string myWindowName;
		unsigned int myWidth;
		unsigned int myHeight;
		bool myStartInFullScreen; 
		bool myUseBloom;
		bool myUseFXAA;
		bool myUseSSAO;
		bool myUseVSync;
		void* myHWND = nullptr;
	};
}}