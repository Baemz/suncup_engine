#pragma once
#include "ParticleStructs.h"
namespace sce { namespace gfx {

	class CDX11VertexBuffer;
	class CParticleEmitter
	{
		friend class CParticleFactory;
		friend class CParticleRenderer;
	public:
		CParticleEmitter();
		~CParticleEmitter();

		const unsigned int GetMaterialID() const { return myMaterialID; };

	private:
		SParticleJsonData myJsonData;
		unsigned int myGeometryShaderID;
		unsigned int myMaterialID;
		CDX11VertexBuffer* myVBuffer;
	};

}}