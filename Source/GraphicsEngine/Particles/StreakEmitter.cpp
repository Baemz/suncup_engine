#include "stdafx.h"
#include "StreakEmitter.h"
#include "..\DirectXFramework\API\DX11VertexBuffer.h"

namespace sce { namespace gfx {

	CStreakEmitter::CStreakEmitter()
		: myTextureID(UINT_MAX)
		, myVertexShaderID(UINT_MAX)
		, myGeometryShaderID(UINT_MAX)
		, myPixelShaderID(UINT_MAX)
	{
		myVBuffer = sce_new(CDX11VertexBuffer());
	}


	CStreakEmitter::~CStreakEmitter()
	{
		sce_delete(myVBuffer);
	}
}}