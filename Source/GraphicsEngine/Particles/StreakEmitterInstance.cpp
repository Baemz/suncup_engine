#include "stdafx.h"
#include "StreakEmitterInstance.h"
#include "ParticleFactory.h"
#include "..\GraphicsEngineInterface.h"

namespace sce { namespace gfx {

		CStreakEmitterInstance::CStreakEmitterInstance()
			: myEmitterID(UINT_MAX)
			, mySpawnTimer(0.f)
		{
		}


		CStreakEmitterInstance::~CStreakEmitterInstance()
		{
		}

		void CStreakEmitterInstance::Init(const char* aTexturePath, const float aMaxLifeTime, const float aSpawnRate)
		{
			if (CParticleFactory::Get() != nullptr)
			{
				myEmitterID = CParticleFactory::Get()->CreateStreakEmittor(aTexturePath, aMaxLifeTime, aSpawnRate);

				myFrustumCollider.SetPosition(myOrientation);
				myFrustumCollider.SetRadius(1.f);

				myMaxLifeTime = aMaxLifeTime;
				mySpawnRate = aSpawnRate;
				myPoints.Init(static_cast<unsigned int>(myMaxLifeTime * mySpawnRate));
			}
			else
			{
				ENGINE_LOG("WARNING! No ParticleFactory created.");
			}
		}

		void CStreakEmitterInstance::Update(const float aDeltaTime)
		{
			bool spawned(false);
			while (mySpawnTimer >= (1 / mySpawnRate))
			{
				spawned = true;
				CU::Vector3f direction;
				direction.x = 0.f;// fmodf(rand() / 3.14f, rand() * 4.0f / 3.14f);
				direction.y = 0.f;// fmodf(rand() / 3.14f, rand() * 2.0f / 3.14f);
				direction.z = 1.f;// fmodf(rand() / 3.14f, rand() * 5.0f / 3.14f);
				direction.Normalize();

				CU::Vector3f pos(myOrientation.GetPosition());
				//GAME_LOG("Particle: %f %f %f", myOrientation.GetPosition().x, myOrientation.GetPosition().y, myOrientation.GetPosition().z);
				myPoints.Add(SStreak({ pos.x, pos.y, pos.z, 1.0f }, { 0.4f, 1.0f, 0.7f, 1.0f }, direction, 0.5f));
				mySpawnTimer -= (1 / mySpawnRate);
			}
			if (spawned)
			{
				mySpawnTimer = 0.f;
			}

			mySpawnTimer += aDeltaTime;

			for (unsigned int i = myPoints.Size(); i-- > 0;)
			{
				if (myPoints[i].myCurrentLifeTime >= myMaxLifeTime)
				{
					myPoints.RemoveCyclicAtIndex(i);
				}
				else
				{
					myPoints[i].myPosition += CU::Vector4f(myPoints[i].myDirection, 1.0f) * mySpawnRate * aDeltaTime;
					myPoints[i].myColor.w = 1.0f - (myPoints[i].myCurrentLifeTime / myMaxLifeTime);
					myPoints[i].mySize = 0.5f - (myPoints[i].myCurrentLifeTime / myMaxLifeTime);
					myPoints[i].myCurrentLifeTime += aDeltaTime;
				}
			}
		}

		void CStreakEmitterInstance::SetPosition(const CU::Vector3f& aPosition)
		{
			myOrientation.SetPosition(aPosition);
		}

		const CU::Vector3f CStreakEmitterInstance::GetPosition() const
		{
			return myOrientation.GetPosition();
		}
}}