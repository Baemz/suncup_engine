#include "stdafx.h"
#include "ParticleEngine.h"
#include "..\EngineCore\DebugHUD.h"

namespace sce
{
	namespace gfx
	{
		struct SParticleEngineArrayHelp
		{
			using value_type_Instances = decltype(CParticleEngine::myInstances)::value_type;
			using value_type_UpdatedParticles = std::remove_all_extents_t<decltype(CParticleEngine::myRenderedParticles)>::value_type;
			using value_type_FreeIDs = decltype(CParticleEngine::myFreeIDs)::value_type;
			using value_type_QueuedAdd = std::remove_all_extents_t<decltype(CParticleEngine::myQueuedForAddIDs)>::value_type;
			using value_type_QueuedUpdate = std::remove_all_extents_t<decltype(CParticleEngine::myQueuedForUpdateIDs)>::value_type;

			static constexpr unsigned long long sizeInstances = sizeof(value_type_Instances) * CParticleEngine::ourMaxAmountOfParticles;
			static constexpr unsigned long long sizeUpdatedParticles = (sizeof(value_type_UpdatedParticles) * (sizeof(CParticleEngine::myRenderedParticles) / sizeof(CParticleEngine::myRenderedParticles[0]))) * CParticleEngine::ourMaxAmountOfParticles;
			static constexpr unsigned long long sizeFreeIDs = sizeof(value_type_FreeIDs) * CParticleEngine::ourMaxAmountOfParticles;
			static constexpr unsigned long long sizeQueuedAdd = (sizeof(value_type_QueuedAdd) * (sizeof(CParticleEngine::myQueuedForAddIDs) / sizeof(CParticleEngine::myQueuedForAddIDs[0]))) * CParticleEngine::ourMaxAmountOfParticles;
			static constexpr unsigned long long sizeQueuedUpdate = (sizeof(value_type_QueuedUpdate) * (sizeof(CParticleEngine::myQueuedForUpdateIDs) / sizeof(CParticleEngine::myQueuedForUpdateIDs[0]))) * CParticleEngine::ourMaxAmountOfParticles;

			static __forceinline void AddBytes(unsigned char** aDataArray, const unsigned long long& aByteSize)
			{
				*reinterpret_cast<unsigned long long*>(aDataArray) += aByteSize;
			}
		};

		CParticleEngine::CParticleEngine()
			: myData(sce_newArray(unsigned char
				, (SParticleEngineArrayHelp::sizeInstances
					+ SParticleEngineArrayHelp::sizeUpdatedParticles
					+ SParticleEngineArrayHelp::sizeFreeIDs
					+ SParticleEngineArrayHelp::sizeQueuedAdd
					+ SParticleEngineArrayHelp::sizeQueuedUpdate)
			))
			, myThreadIndexWrite(0)
			, myThreadIndexRead(1)
			, myThreadIndexFree(2)
			, myThreadIndexHasBeenUpdated{ false, true, true }
			, myThreadForceQuit(false)
			, myIndexWrite(0)
			, myIndexRead(1)
			, myIndexFree(2)
			, myIndexHasBeenUpdated{ false, true, true }
		{
			unsigned char* tempData(myData);

 			myInstances.SetRawData(reinterpret_cast<SParticleEngineArrayHelp::value_type_Instances*>(tempData), ourMaxAmountOfParticles);
 			SParticleEngineArrayHelp::AddBytes(&tempData, (sizeof(SParticleEngineArrayHelp::value_type_Instances) * ourMaxAmountOfParticles));

			for (auto& updatedParticles : myRenderedParticles)
			{
				updatedParticles.SetRawData(reinterpret_cast<SParticleEngineArrayHelp::value_type_UpdatedParticles*>(tempData), ourMaxAmountOfParticles);
				SParticleEngineArrayHelp::AddBytes(&tempData, (sizeof(SParticleEngineArrayHelp::value_type_UpdatedParticles) * ourMaxAmountOfParticles));
			}

			myFreeIDs.SetRawData(reinterpret_cast<SParticleEngineArrayHelp::value_type_FreeIDs*>(tempData), ourMaxAmountOfParticles);
			SParticleEngineArrayHelp::AddBytes(&tempData, (sizeof(SParticleEngineArrayHelp::value_type_FreeIDs) * ourMaxAmountOfParticles));

			for (auto& queuedForAddID : myQueuedForAddIDs)
			{
				queuedForAddID.SetRawData(reinterpret_cast<SParticleEngineArrayHelp::value_type_QueuedAdd*>(tempData), ourMaxAmountOfParticles);
				SParticleEngineArrayHelp::AddBytes(&tempData, (sizeof(SParticleEngineArrayHelp::value_type_QueuedAdd) * ourMaxAmountOfParticles));
			}

			for (auto& queuedForUpdateID : myQueuedForUpdateIDs)
			{
				queuedForUpdateID.SetRawData(reinterpret_cast<SParticleEngineArrayHelp::value_type_QueuedUpdate*>(tempData), ourMaxAmountOfParticles);
				SParticleEngineArrayHelp::AddBytes(&tempData, (sizeof(SParticleEngineArrayHelp::value_type_QueuedUpdate) * ourMaxAmountOfParticles));
			}

			for (auto& instance : myInstances)
			{
				new(&instance) SParticleEngineArrayHelp::value_type_Instances;
			}

			for (auto& updatedParticles : myRenderedParticles)
			{
				for (auto& updatedParticle : updatedParticles)
				{
					new(&updatedParticle) SParticleEngineArrayHelp::value_type_UpdatedParticles;
				}
				updatedParticles.RemoveAll();
			}

			for (decltype(myFreeIDs)::size_type index(0); index < myFreeIDs.size(); ++index)
			{
				myFreeIDs[index] = index;
			}

			for (auto& addIDs : myQueuedForAddIDs)
			{
				for (auto& addID : addIDs)
				{
					new(&addID) SParticleEngineArrayHelp::value_type_QueuedAdd;
				}
				addIDs.RemoveAll();
			}

			for (auto& updateIDs : myQueuedForUpdateIDs)
			{
				for (auto& updateID : updateIDs)
				{
					new(&updateID) SParticleEngineArrayHelp::value_type_QueuedUpdate;
				}
				updateIDs.RemoveAll();
			}

			myThread = std::thread(&CParticleEngine::ThreadLoop, this);
			CU::ThreadHelper::SetThreadName(myThread, "SCE | ParticleEngine");
		}

		CParticleEngine::~CParticleEngine()
		{
			myThreadForceQuit = true;

			myThread.join();

			if (myFreeIDs.size() != myFreeIDs.capacity())
			{
				ENGINE_LOG("[Particle] WARNING! Particle engine was shutdown while all particles have not been returned, leak? %u/%u returned"
					, myFreeIDs.size(), myFreeIDs.capacity());
			}

			sce_delete(myData);
		}

		ParticleIDType CParticleEngine::QueueCreateParticle(const char* aParticleJson, const CU::Vector3f aBoxOffset)
		{
			std::unique_lock<std::mutex> uniqueLockFree(myFreeIDsMutex, std::defer_lock);
			std::unique_lock<std::mutex> uniqueLockAdd(myQueuedForAddIDsMutex, std::defer_lock);
			std::shared_lock<std::shared_mutex> sharedLockIndex(myIndicesMutex, std::defer_lock);
			std::lock(uniqueLockFree, uniqueLockAdd, sharedLockIndex);

			if (myFreeIDs.Empty())
			{
				ENGINE_LOG("[Particle] WARNING! Too many particle emitters created, exceeded %u"
					, ourMaxAmountOfParticles);
				return ParticleIDTypeINVALID;
			}

			ParticleIDType returnValue(myFreeIDs.GetLast());
			
			myFreeIDs.RemoveCyclicAtIndex(myFreeIDs.GetLastIndex());
			myQueuedForAddIDs[myIndexWrite].Add(returnValue);
			auto& addedParticle(myQueuedForAddIDs[myIndexWrite].GetLast());
			addedParticle.myParticleJson = aParticleJson;
			addedParticle.myBoxOffset = aBoxOffset;

			return returnValue;
		}

		void CParticleEngine::DestroyParticle(const ParticleIDType& aParticleID)
		{
			if (aParticleID == ParticleIDTypeINVALID)
			{
				return;
			}

			std::unique_lock<std::mutex> uniqueLockFree(myFreeIDsMutex, std::defer_lock);
			std::shared_lock<std::shared_mutex> sharedLockIndex(myIndicesMutex, std::defer_lock);
			std::lock(uniqueLockFree, sharedLockIndex);
			myFreeIDs.Add(aParticleID);
		}

		const bool CParticleEngine::IsFinished(const ParticleIDType & aParticleID)
		{
			std::unique_lock<std::mutex> uniqueLockFinished(myFinishedParticlesMutex, std::defer_lock);
			std::shared_lock<std::shared_mutex> sharedLockIndex(myIndicesMutex, std::defer_lock);
			std::lock(uniqueLockFinished, sharedLockIndex);

			ParticleIDType foundIndex(myFinishedParticles.Find(aParticleID));
			if (foundIndex != myFinishedParticles.FoundNone)
			{
				myFinishedParticles.RemoveCyclicAtIndex(foundIndex);
				return true;
			}

			return false;
		}

		void CParticleEngine::QueueUpdateParticle(const SParticleUpdate& aParticleUpdate)
		{
			std::unique_lock<std::mutex> uniqueLockUpdate(myQueuedForUpdateIDsMutex, std::defer_lock);
			std::shared_lock<std::shared_mutex> sharedLockIndex(myIndicesMutex, std::defer_lock);
			std::lock(uniqueLockUpdate, sharedLockIndex);
			myQueuedForUpdateIDs[myIndexWrite].Add(aParticleUpdate);
		}

		void CParticleEngine::QueueUpdateParticle(const CU::GrowingArray<SParticleUpdate, ParticleIDType>& aParticleUpdates)
		{
			std::unique_lock<std::mutex> uniqueLockUpdate(myQueuedForUpdateIDsMutex, std::defer_lock);
			std::shared_lock<std::shared_mutex> sharedLockIndex(myIndicesMutex, std::defer_lock);
			std::lock(uniqueLockUpdate, sharedLockIndex);
			myQueuedForUpdateIDs[myIndexWrite].Add(aParticleUpdates);
		}

		void CParticleEngine::QueueRenderParticle(const CU::GrowingArray<ParticleIDType, ParticleIDType>& aParticleRender)
		{
			std::unique_lock<std::mutex> uniqueLockRender(myQueuedForRenderIDsMutex, std::defer_lock);
			std::shared_lock<std::shared_mutex> sharedLockIndex(myIndicesMutex, std::defer_lock);
			std::lock(uniqueLockRender, sharedLockIndex);
			myQueuedForRenderIDs[myIndexWrite].Add(aParticleRender);
		}

		void CParticleEngine::SubmitUpdateAndRenderQueue()
		{
			std::unique_lock<std::shared_mutex> uniqueLockIndex(myIndicesMutex);
			myIndexHasBeenUpdated[myIndexFree] = false;
			std::swap(myIndexWrite, myIndexFree);
			uniqueLockIndex.unlock();

			myQueuedForUpdateIDs[myIndexWrite].RemoveAll();
			myQueuedForRenderIDs[myIndexWrite].RemoveAll();
		}

		bool CParticleEngine::GetLatestParticles(CU::GrowingArray<CParticleEmitterInstance, ParticleIDType>& aParticlesArray)
		{
			std::unique_lock<std::shared_mutex> uniqueLockThreadIndex(myThreadIndicesMutex);
			if (myThreadIndexHasBeenUpdated[myThreadIndexFree] == false)
			{
				myThreadIndexHasBeenUpdated[myThreadIndexFree] = true;
				std::swap(myThreadIndexRead, myThreadIndexFree);
			}
			uniqueLockThreadIndex.unlock();

			aParticlesArray.Add(myRenderedParticles[myThreadIndexRead]);

			return (aParticlesArray.Empty() == false);
		}

		void CParticleEngine::ThreadLoop()
		{
			constexpr float maxFPS(600.0f);
			constexpr float minFPSForSlowmotion(20.0f);

			myFramerateLimiter.Init(maxFPS, minFPSForSlowmotion);

			while (myThreadForceQuit == false)
			{
				const float deltaTime(myFramerateLimiter.Update());

				ThreadSwitchUpdateBuffers();

				auto& addIDs(myQueuedForAddIDs[myIndexRead]);
				auto& updateIDs(myQueuedForUpdateIDs[myIndexRead]);
				auto& renderIDs(myQueuedForRenderIDs[myIndexRead]);

				ThreadInitParticles(addIDs);
				ThreadUpdateParticles(updateIDs, deltaTime);
				ThreadRenderParticles(renderIDs);

				ThreadSwitchRenderBuffers();

				CDebugHUD::SetMS(CDebugHUD::eMSTexts::Particle);
			}
		}

		void CParticleEngine::ThreadSwitchUpdateBuffers()
		{
			std::unique_lock<std::shared_mutex> uniqueLockIndex(myIndicesMutex);
			if (myIndexHasBeenUpdated[myIndexFree] == false)
			{
				myIndexHasBeenUpdated[myIndexFree] = true;
				std::swap(myIndexRead, myIndexFree);
				uniqueLockIndex.unlock();

				myFinishedParticlesForThisUpdateQueue.RemoveAll();
			}
		}

		void CParticleEngine::ThreadSwitchRenderBuffers()
		{
			std::unique_lock<std::shared_mutex> uniqueLockThreadIndex(myThreadIndicesMutex);
			myThreadIndexHasBeenUpdated[myThreadIndexFree] = false;
			std::swap(myThreadIndexWrite, myThreadIndexFree);
			uniqueLockThreadIndex.unlock();

			myRenderedParticles[myThreadIndexWrite].RemoveAll();
		}

		void CParticleEngine::ThreadInitParticles(CU::GrowingArray<SParticleInitData, ParticleIDType>& aAddIDs)
		{
			for (const auto& particleInitData : aAddIDs)
			{
				auto& particle(myInstances[particleInitData.myParticleID]);
				particle.Init(particleInitData.myParticleJson.c_str(), particleInitData.myBoxOffset);
			}

			aAddIDs.RemoveAll();
		}

		void CParticleEngine::ThreadUpdateParticles(CU::GrowingArray<SParticleUpdate, ParticleIDType>& aUpdateIDs, const float& aDeltaTime)
		{
			for (const auto& particleUpdate : aUpdateIDs)
			{
				auto& particle(myInstances[particleUpdate.myParticleID]);

				if (particle.IsEmitting() == false && particle.IsEmpty())
				{
					if (myFinishedParticlesForThisUpdateQueue.Find(particleUpdate.myParticleID) == myFinishedParticlesForThisUpdateQueue.FoundNone)
					{
						myFinishedParticlesForThisUpdateQueue.Add(particleUpdate.myParticleID);
						std::unique_lock<std::mutex> uniqueLockFinished(myFinishedParticlesMutex);
						myFinishedParticles.Add(particleUpdate.myParticleID);
					}
				}
				else
				{
					particle.SetPosition(particleUpdate.myPosition);
					particle.Update(aDeltaTime);
				}
			}
		}

		void CParticleEngine::ThreadRenderParticles(CU::GrowingArray<ParticleIDType, ParticleIDType>& aRenderIDs)
		{
			auto& renderedParticles(myRenderedParticles[myThreadIndexWrite]);

			for (const auto& particleID : aRenderIDs)
			{
				auto& particle(myInstances[particleID]);
				renderedParticles.Add(particle);
			}
		}
	}
}
