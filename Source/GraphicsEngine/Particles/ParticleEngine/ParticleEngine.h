#pragma once
#include "ParticleTypes.h"
#include "..\CommonUtilities\GrowingArray.h"
#include <shared_mutex>
#include "..\CommonUtilities\ThreadHelper.h"

namespace sce
{
	namespace gfx
	{
		class CParticleEngine
		{
			friend struct SParticleEngineArrayHelp;

		public:
			CParticleEngine();
			~CParticleEngine();

			ParticleIDType QueueCreateParticle(const char* aParticleJson, const CU::Vector3f aBoxOffset);
			void DestroyParticle(const ParticleIDType& aParticleID);
			const bool IsFinished(const ParticleIDType& aParticleID);

			void QueueUpdateParticle(const SParticleUpdate& aParticleUpdate);
			void QueueUpdateParticle(const CU::GrowingArray<SParticleUpdate, ParticleIDType>& aParticleUpdates);
			void QueueRenderParticle(const CU::GrowingArray<ParticleIDType, ParticleIDType>& aParticleRender);
			void SubmitUpdateAndRenderQueue();

			bool GetLatestParticles(CU::GrowingArray<CParticleEmitterInstance, ParticleIDType>& aParticlesArray);

		private:
			struct SParticleInitData
			{
				SParticleInitData() : myParticleID(ParticleIDTypeINVALID) {};
				SParticleInitData(const ParticleIDType& aParticleID) : myParticleID(aParticleID) {};

				bool operator==(const SParticleInitData& aParticleInitData) const
				{
					return (myParticleID == aParticleInitData.myParticleID);
				}

				ParticleIDType myParticleID;
				std::string myParticleJson;
				CU::Vector3f myBoxOffset;
			};

			void ThreadLoop();
			void ThreadSwitchUpdateBuffers();
			void ThreadSwitchRenderBuffers();
			void ThreadInitParticles(CU::GrowingArray<SParticleInitData, ParticleIDType>& aAddIDs);
			void ThreadUpdateParticles(CU::GrowingArray<SParticleUpdate, ParticleIDType>& aUpdateIDs, const float& aDeltaTime);
			void ThreadRenderParticles(CU::GrowingArray<ParticleIDType, ParticleIDType>& aRenderIDs);

			static constexpr ParticleIDType ourMaxAmountOfParticles = 1024;

			unsigned char* myData;

			CU::GrowingArray<CParticleEmitterInstance, ParticleIDType> myInstances;
			CU::GrowingArray<ParticleIDType, ParticleIDType> myFinishedParticlesForThisUpdateQueue;
			
			CU::GrowingArray<CParticleEmitterInstance, ParticleIDType> myRenderedParticles[3];
			unsigned char myThreadIndexWrite;
			unsigned char myThreadIndexRead;
			unsigned char myThreadIndexFree;
			bool myThreadIndexHasBeenUpdated[3];
			std::shared_mutex myThreadIndicesMutex;

			std::thread myThread;
			volatile bool myThreadForceQuit;
			CU::ThreadHelper::CFramerateLimiter myFramerateLimiter;

			unsigned char myIndexWrite;
			unsigned char myIndexRead;
			unsigned char myIndexFree;
			bool myIndexHasBeenUpdated[3];
			std::shared_mutex myIndicesMutex;
			
			CU::GrowingArray<ParticleIDType, ParticleIDType> myFreeIDs;
			std::mutex myFreeIDsMutex;
			CU::GrowingArray<SParticleInitData, ParticleIDType> myQueuedForAddIDs[3];
			std::mutex myQueuedForAddIDsMutex;
			CU::GrowingArray<SParticleUpdate, ParticleIDType> myQueuedForUpdateIDs[3];
			std::mutex myQueuedForUpdateIDsMutex;
			CU::GrowingArray<ParticleIDType, ParticleIDType> myQueuedForRenderIDs[3];
			std::mutex myQueuedForRenderIDsMutex;
			CU::GrowingArray<ParticleIDType, ParticleIDType> myFinishedParticles;
			std::mutex myFinishedParticlesMutex;

		};
	}
}
