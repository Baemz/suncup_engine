#pragma once


namespace sce
{
	namespace gfx
	{
		using ParticleIDType = unsigned int;
		
		constexpr ParticleIDType ParticleIDTypeINVALID = static_cast<ParticleIDType>(-1);

		struct SParticleUpdate
		{
			SParticleUpdate() : myParticleID(ParticleIDTypeINVALID) {};

			ParticleIDType myParticleID;
			CU::Vector3f myPosition;
		};
	}
}
