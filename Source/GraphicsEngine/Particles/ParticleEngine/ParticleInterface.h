#pragma once
#include "..\CommonUtilities\Vector3.h"
#include "..\ParticleEmitterInstance.h"
#include "ParticleTypes.h"

namespace sce
{
	namespace gfx
	{
		class CParticleEngine;

		class CParticleInterface
		{
		public:
			~CParticleInterface();

			static bool Create();
			static bool Destroy();

			inline static CParticleInterface* Get() { return ourInstance; };

			ParticleIDType QueueCreateParticle(const char* aParticleJson, const CU::Vector3f aBoxOffset = { 0.0f, 0.0f, 0.0f });
			void QueueDestroyParticle(const ParticleIDType& aParticleID);
			const bool IsFinished(const ParticleIDType& aParticleID);
	
			void QueueUpdateParticle(const SParticleUpdate& aParticleUpdate);
			void QueueUpdateParticle(const CU::GrowingArray<SParticleUpdate, ParticleIDType>& aParticleUpdates);
			void QueueRenderParticle(const CU::GrowingArray<ParticleIDType, ParticleIDType>& aParticleRender);
			void SubmitUpdateAndRenderQueue();

			bool GetLatestParticles(CU::GrowingArray<CParticleEmitterInstance, ParticleIDType>& aParticlesArray);

		private:
			CParticleInterface();

			static CParticleInterface* ourInstance;

			CParticleEngine* myParticleEngine;

		};
	}
}
