#include "stdafx.h"
#include "ParticleInterface.h"
#include "ParticleEngine.h"

namespace sce
{
	namespace gfx
	{
		CParticleInterface* CParticleInterface::ourInstance = nullptr;

		CParticleInterface::~CParticleInterface()
		{
			sce_delete(myParticleEngine);
		}

		bool CParticleInterface::Create()
		{
			if (ourInstance == nullptr)
			{
				ourInstance = sce_new(CParticleInterface);
				return true;
			}
			return false;
		}

		bool CParticleInterface::Destroy()
		{
			if (ourInstance == nullptr)
			{
				return false;
			}
			sce_delete(ourInstance);
			return true;
		}

		ParticleIDType CParticleInterface::QueueCreateParticle(const char* aParticleJson, const CU::Vector3f aBoxOffset)
		{
			return myParticleEngine->QueueCreateParticle(aParticleJson, aBoxOffset);
		}

		void CParticleInterface::QueueDestroyParticle(const ParticleIDType& aParticleID)
		{
			myParticleEngine->DestroyParticle(aParticleID);
		}

		const bool CParticleInterface::IsFinished(const ParticleIDType & aParticleID)
		{
			return myParticleEngine->IsFinished(aParticleID);
		}

		void CParticleInterface::QueueUpdateParticle(const SParticleUpdate& aParticleUpdate)
		{
			myParticleEngine->QueueUpdateParticle(aParticleUpdate);
		}

		void CParticleInterface::QueueUpdateParticle(const CU::GrowingArray<SParticleUpdate, ParticleIDType>& aParticleUpdates)
		{
			myParticleEngine->QueueUpdateParticle(aParticleUpdates);
		}

		void CParticleInterface::QueueRenderParticle(const CU::GrowingArray<ParticleIDType, ParticleIDType>& aParticleRender)
		{
			myParticleEngine->QueueRenderParticle(aParticleRender);
		}

		void CParticleInterface::SubmitUpdateAndRenderQueue()
		{
			myParticleEngine->SubmitUpdateAndRenderQueue();
		}

		bool CParticleInterface::GetLatestParticles(CU::GrowingArray<CParticleEmitterInstance, ParticleIDType>& aParticlesArray)
		{
			return myParticleEngine->GetLatestParticles(aParticlesArray);
		}

		CParticleInterface::CParticleInterface()
			: myParticleEngine(sce_new(CParticleEngine))
		{
		}
	}
}
