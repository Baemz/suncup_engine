#pragma once
#include <array>

namespace sce { namespace gfx {

	enum class ERenderProjectionMode
	{
		VIEW,
		HORIZONTAL,
		VERTICAL,
	};

	struct SParticleJsonData
	{
		std::string textureFileName;
		std::string materialName;
		std::array<CU::Vector4f, 100> colorgrad1;
		std::array<CU::Vector4f, 100> colorgrad2;
		CU::Vector4f startColor;
		CU::Vector3f velocityOverLife;
		CU::Vector3f direction;
		CU::Vector3f spawnPositionLimits;
		CU::Vector3f rotationOverLife;
		CU::Vector3f rotationOverLifeMin;
		CU::Vector3f rotationOverLifeMax;
		float duration;
		float startDelay;
		float startLifetime;
		float startSpeed;
		float startSize;
		float startSizeMin;
		float startSizeMax;
		float startSizeCurve;
		float endSize;
		float startRotationMin;
		float startRotationMax;
		float gravityModifier;
		float emissionRateOverTime;
		int maxParticles;
		ERenderProjectionMode myRenderProjectionMode = ERenderProjectionMode::VIEW;
		bool useSizeOverLifetime;
		bool useSizeCurve;
		bool useRotationOverLifetime;
		bool looping;
		bool randomTwoGradients;
		bool useColorOverLife;
		bool useBoxOffset;
		bool useSheetAnim;
	};
}}