#include "stdafx.h"
#include "ParticleEmitterInstance.h"
#include "ParticleFactory.h"
#include "GraphicsEngineInterface.h"

namespace sce { namespace gfx {

	CParticleEmitterInstance::CParticleEmitterInstance()
		: myEmitterID(UINT_MAX)
		, myMaterialID(UINT_MAX)
		, mySpawnTimer(0.0f)
		, myDurationTimer(0.0f)
		, myIsEmitting(true)
	{
	}

	CParticleEmitterInstance::~CParticleEmitterInstance()
	{
	}

	void CParticleEmitterInstance::Init(const char* aParticleJson, const CU::Vector3f aBoxOffset)
	{
		myEmitterID = UINT_MAX;
		mySpawnTimer = 0.0f;
		myDurationTimer = 0.0f;

		if (CParticleFactory::Get() != nullptr)
		{
			myEmitterID = CParticleFactory::Get()->CreateEmittor(aParticleJson, myJsonData, myMaterialID);

			if (myEmitterID != UINT_MAX)
			{
				myFrustumCollider.SetPosition(myOrientation);
				myFrustumCollider.SetRadius(1.f);
				myBoxOffset = aBoxOffset;

				myParticles.Init(static_cast<unsigned int>(myJsonData.startLifetime * myJsonData.emissionRateOverTime));
				myIsEmitting = true;
			}
			else
			{
				myIsEmitting = false;
			}
		}
		else
		{
			ENGINE_LOG("WARNING! No ParticleFactory created.");
		}
	}

	void CParticleEmitterInstance::Update(const float aDeltaTime)
	{
		if (IsEmitting() == false && IsEmpty())
		{
			return;
		}
		if (myJsonData.looping == true || myDurationTimer <= myJsonData.duration)
		{
			if (myParticles.Size() < static_cast<unsigned int>(myJsonData.maxParticles))
			{
				SpawnParticles(aDeltaTime);
			}
		}
		if (myJsonData.looping == false && myIsEmitting != false)
		{
			myDurationTimer += aDeltaTime;
			if (myDurationTimer >= myJsonData.duration)
			{
				myIsEmitting = false;
			}
		}

		for (unsigned int i = myParticles.Size(); i-- > 0;)
		{
			SParticle& currentParticle(myParticles[i]);

			if (currentParticle.myCurrentLifeTime >= myJsonData.startLifetime)
			{
				myParticles.RemoveCyclicAtIndex(i);
			}
			else
			{
				UpdateParticle(currentParticle, aDeltaTime);
			}
		}
	}

	void CParticleEmitterInstance::SetPosition(const CU::Vector3f& aPosition)
	{
		myOrientation.SetPosition(aPosition);
	}

	void CParticleEmitterInstance::SpawnParticles(const float aDeltaTime)
	{
		if (myIsEmitting == false)
		{
			return;
		}

		bool spawnedParticle(false);
		while (mySpawnTimer >= (1 / myJsonData.emissionRateOverTime))
		{
			spawnedParticle = true;

			// handle different types of emit-shapes (currently only box)
			CU::Vector3f direction(0.f, 1.f, 0.f);
			CU::Quaternion q;
			q.RotateLocal(myJsonData.direction);
			direction = q * direction;
			direction.Normalize();
			
			CU::Vector3f boxMin;
			CU::Vector3f boxMax;
			if (myJsonData.useBoxOffset)
			{
				boxMin = (-0.5f + myBoxOffset.x, -0.5f + myBoxOffset.y, -0.5f + myBoxOffset.z);
				boxMin.x *= myJsonData.spawnPositionLimits.x;
				boxMin.y *= myJsonData.spawnPositionLimits.y;
				boxMin.z *= myJsonData.spawnPositionLimits.z;
				boxMax = (0.5f + myBoxOffset.x, 0.5f + myBoxOffset.y, 0.5f + myBoxOffset.z);
				boxMax.x *= myJsonData.spawnPositionLimits.x;
				boxMax.y *= myJsonData.spawnPositionLimits.y;
				boxMax.z *= myJsonData.spawnPositionLimits.z;
			}
			
// 			if (myParentPosition != nullptr)
// 			{
// 				myOrientation.SetPosition(*myParentPosition);
// 			}
// 
// 			if (myParentOrientation != nullptr)
// 			{
// 				direction = *myParentOrientation * direction;
// 				boxMin = *myParentOrientation * boxMin;
// 				boxMax = *myParentOrientation * boxMax;
// 			}
// 			else
			{
				boxMin = q * boxMin;
				boxMax = q * boxMax;
			}

			CU::Vector3f offset(CU::GetRandomInRange(boxMin.x, boxMax.x), CU::GetRandomInRange(boxMin.y, boxMax.y), CU::GetRandomInRange(boxMin.z, boxMax.z));

			CU::Vector4f startColor;
			startColor = myJsonData.startColor;

			bool gradient = false;
			if (myJsonData.useColorOverLife)
			{
				if (myJsonData.randomTwoGradients)
				{
					int randGrad = CU::GetRandomInRange(0, 100);
					if (randGrad < 50)
					{
						gradient = false;
						startColor = myJsonData.colorgrad1[0];
					}
					else
					{
						gradient = true;
						startColor = myJsonData.colorgrad2[0];
					}
				}
				else
				{
					startColor = myJsonData.colorgrad1[0];
				}
			}

			float rotateVel = 0.f;
			if (myJsonData.useRotationOverLifetime)
			{
				rotateVel = CU::GetRandomInRange(myJsonData.rotationOverLifeMin.z, myJsonData.rotationOverLifeMax.z);
			}
			else
			{
			}

			float startRotation = 0.f; 
			startRotation = CU::GetRandomInRange(myJsonData.startRotationMin, myJsonData.startRotationMax);

			float startSize = 0.f;
			if (myJsonData.useSizeOverLifetime == true && myJsonData.useSizeCurve == false)
			{
				startSize = CU::GetRandomInRange(myJsonData.startSizeMin, myJsonData.startSizeMax);
			}
			else
			{
				startSize = myJsonData.startSize;
			}

			CU::Vector4f uv(0.f, 1.f, 0.f, 1.f);
			if (myJsonData.useSheetAnim)
			{
				int rand = CU::GetRandomInRange(0, 3);
				switch (rand)
				{
				default:
				case 0:
					uv = { 0.f, 0.5f, 0.f, 0.5f};
					break;
				case 1:
					uv = { 0.5f, 1.0f, 0.f, 0.5f };
					break;
				case 2:
					uv = { 0.f, 0.5f, 0.5f, 1.0f };
					break;
				case 3:
					uv = { 0.5f, 1.0f, 0.5f, 1.0f };
					break;
				}
			}

			myParticles.Add(SParticle({ myOrientation.GetPosition() + offset , 1.0f }, startColor, uv, direction, startSize, startRotation, gradient, rotateVel));
			mySpawnTimer -= (1 / myJsonData.emissionRateOverTime);
			UpdateParticle(myParticles.GetLast(), mySpawnTimer);
		}
		if (spawnedParticle)
		{
			mySpawnTimer = 0.f;
		}
		mySpawnTimer += aDeltaTime;
	}

	static float Round(float f) // Don't replace this with C++11 version, not the same functionality. (Not actually only round)
	{
		return floorf(f * 100) / 100;
	}

	void CParticleEmitterInstance::UpdateParticle(SParticle& aParticle, const float aDeltaTime) const
	{
		const float lifetimePercent(aParticle.myCurrentLifeTime / myJsonData.startLifetime);
		aParticle.myPosition += CU::Vector4f(aParticle.myDirection, 1.0f) * myJsonData.startSpeed * aDeltaTime;
		aParticle.myCurrentLifeTime += aDeltaTime;

		if (myJsonData.useSizeCurve)
		{
			aParticle.mySize = myJsonData.startSize * (myJsonData.startSizeCurve + (myJsonData.endSize - myJsonData.startSizeCurve) * (lifetimePercent));
		}

		float lifetimeClamped = Round(lifetimePercent);
		unsigned int index = static_cast<unsigned int>(lifetimeClamped * 100);
		if (myJsonData.useColorOverLife)
		{
			if (aParticle.myGradient == false)
			{
				aParticle.myColor = myJsonData.colorgrad1[index];
			}
			else
			{
				aParticle.myColor = myJsonData.colorgrad2[index];
			}
		}
		if (myJsonData.useRotationOverLifetime)
		{
			aParticle.myRotation += aDeltaTime * aParticle.myRotationVelocity;
		}
	}
}}