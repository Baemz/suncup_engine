#pragma once
#include "..\..\CommonUtilities\Matrix.h"
#include "..\..\CommonUtilities\GrowingArray.h"
#include "..\Camera\Frustum\FrustumCollider.h"
#include "..\Utilities\VertexStructs.h"

namespace sce { namespace gfx {

	class CStreakEmitterInstance
	{
		friend class CParticleFactory;
		friend class CParticleRenderer;
		friend class CScene;
	public:
		CStreakEmitterInstance();
		~CStreakEmitterInstance();

		void Init(const char* aTexturePath, const float aMaxLifeTime, const float aSpawnRate);
		void Update(const float aDeltaTime);

		void SetPosition(const CU::Vector3f& aPosition);
		const CU::Vector3f GetPosition() const;
	private:
		CU::GrowingArray<SStreak, unsigned int> myPoints;
		CU::Matrix44f myOrientation;
		CFrustumCollider myFrustumCollider;
		float myMaxLifeTime;
		float mySpawnRate;
		float mySpawnTimer;
		unsigned int myEmitterID;
	};

}}