#pragma once
#define MAX_EMITTORS 100

namespace sce { namespace gfx {

	class CParticleEmitter;
	class CStreakEmitter;
	struct SParticleJsonData;
	class CParticleFactory
	{
	public:
		CParticleFactory();
		~CParticleFactory();

		static void Create();
		static void Destroy();
		static CParticleFactory* Get() { return ourInstance; };

		unsigned int CreateEmittor(const char* aParticleJson, SParticleJsonData& aJsonDataToFill, unsigned int& aMaterialIDToFill);
		unsigned int CreateStreakEmittor(const char* aTexturePath, const float aMaxLifeTime, const float aSpawnRate);

		CParticleEmitter& GetEmittorWithID(unsigned int aID);
		CStreakEmitter& GetSEmittorWithID(unsigned int aID);
	private:
		static CParticleFactory* ourInstance;

		bool LoadParticleData(const char* aDataPath, SParticleJsonData& aStructToFill);

		bool EmitterExists(const std::string& aIdentifier);
		unsigned int GetEmittorID(const std::string& aIdentifier);
		unsigned int myCurrentlyFreeEmittorID;
		std::map<std::string, unsigned int> myAssignedEmittorIDs;
		CParticleEmitter* myEmittors;

		bool SEmitterExists(const std::string& aIdentifier);
		unsigned int GetSEmittorID(const std::string& aIdentifier);
		unsigned int myCurrentlyFreeSEmittorID;
		std::map<std::string, unsigned int> myAssignedSEmittorIDs;
		CStreakEmitter* mySEmitters;
	};

}}