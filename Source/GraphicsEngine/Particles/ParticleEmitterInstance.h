#pragma once
#include "..\Camera\Frustum\FrustumCollider.h"
#include "..\Utilities\VertexStructs.h"
#include "..\CommonUtilities\GrowingArray.h"
#include "..\CommonUtilities\Matrix.h"
#include "ParticleStructs.h"

namespace sce { namespace gfx{

	class CParticleEmitterInstance
	{
		friend class CParticleFactory;
		friend class CParticleRenderer;
		friend class CScene;
	public:
		CParticleEmitterInstance();
		~CParticleEmitterInstance();

		void Init(const char* aParticleJson, const CU::Vector3f aBoxOffset = { 0.0f, 0.0f, 0.0f });
		void Update(const float aDeltaTime);
		const bool IsEmitting() const { return myIsEmitting; };
		const bool IsEmpty() const { return (myParticles.Size() <= 0); };

		void SetPosition(const CU::Vector3f& aPosition);

		const unsigned int GetEmitterID() const { return myEmitterID; };

	private:

		void SpawnParticles(const float aDeltaTime);
		void UpdateParticle(SParticle& aParticle, const float aDeltaTime) const;

	private:
		CU::GrowingArray<SParticle, unsigned int> myParticles;
		CU::Matrix44f myOrientation;
		CFrustumCollider myFrustumCollider;
		SParticleJsonData myJsonData;
		float mySpawnTimer;
		float myDurationTimer;
		unsigned int myEmitterID;
		unsigned int myMaterialID;
		CU::Vector3f myBoxOffset;
		bool myIsEmitting;
	};

}}