#include "stdafx.h"
#include "ParticleEmitter.h"
#include "DirectXFramework\API\DX11VertexBuffer.h"

namespace sce { namespace gfx {

	CParticleEmitter::CParticleEmitter()
		: myGeometryShaderID(UINT_MAX)
		, myMaterialID(UINT_MAX)
	{
		myVBuffer = sce_new(CDX11VertexBuffer());
	}


	CParticleEmitter::~CParticleEmitter()
	{
		sce_delete(myVBuffer);
	}
}}