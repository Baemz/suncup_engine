#include "stdafx.h"
#include "ParticleFactory.h"
#include "ParticleEmitter.h"
#include "ResourceManager\ResourceManager.h"
#include "DirectXFramework\API\DX11VertexBuffer.h"
#include "DirectXFramework\API\DX11Shader.h"
#include "DirectXFramework\API\DX11Texture2D.h"
#include "StreakEmitter.h"
#include "../EngineCore/JSON/json.hpp"
#include "ParticleStructs.h"

using json = nlohmann::json;

static int LoadInt(json& aFileReader, const std::string& aVariableName)
{
	try
	{
		return aFileReader[aVariableName].get<int>();
	}
	catch (...)
	{
		RESOURCE_LOG("ERROR: Wrong value(int) in data variable %s.", aVariableName.c_str());
		return -1;
	}
}
static float LoadFloat(json& aFileReader, const std::string& aVariableName)
{
	try
	{
		return aFileReader[aVariableName].get<float>();
	}
	catch (...)
	{
		RESOURCE_LOG("ERROR: Wrong value(float) in data variable %s.", aVariableName.c_str());
		return -1;
	}
}
static bool LoadBool(json& aFileReader, const std::string& aVariableName)
{
	try
	{
		return aFileReader[aVariableName].get<bool>();
	}
	catch (...)
	{
		RESOURCE_LOG("ERROR: Wrong value(bool) in data variable %s.", aVariableName.c_str());
		return false;
	}
}
static const CU::Vector4f LoadVec4(json& aFileReader, const std::string& aVariableName)
{
	try
	{
		CU::Vector4f vec;
		unsigned short i = 3;
		for (auto& var : aFileReader[aVariableName])
		{
			vec[i] = var.get<float>();
			if(i-- < 0)
			{
				break;
			}
		}
		return vec;
	}
	catch (...)
	{
		RESOURCE_LOG("ERROR: Wrong value(vec4) in data variable %s.", aVariableName.c_str());
		return CU::Vector4f();
	}
}
static const CU::Vector3f LoadVec3(json& aFileReader, const std::string& aVariableName)
{
	try
	{
		CU::Vector3f vec;
		unsigned char i = 0;
		for (auto& var : aFileReader[aVariableName])
		{
			vec[i] = var.get<float>();
			if (++i > 2)
			{
				break;
			}
		}
		return vec;
	}
	catch (...)
	{
		RESOURCE_LOG("ERROR: Wrong value(vec3) in data variable %s.", aVariableName.c_str());
		return CU::Vector3f();
	}
}


namespace sce { namespace gfx {

	CParticleFactory* CParticleFactory::ourInstance = nullptr;

	CParticleFactory::CParticleFactory()
		: myCurrentlyFreeEmittorID(0)
		, myCurrentlyFreeSEmittorID(0)
	{
		myEmittors = sce_newArray(CParticleEmitter, MAX_EMITTORS);
		mySEmitters = sce_newArray(CStreakEmitter, MAX_EMITTORS);
	}


	CParticleFactory::~CParticleFactory()
	{
		sce_delete(myEmittors);
		sce_delete(mySEmitters);
	}

	void CParticleFactory::Create()
	{
		if (ourInstance == nullptr)
		{
			ourInstance = sce_new(CParticleFactory());
		}

		return;
	}

	void CParticleFactory::Destroy()
	{
		sce_delete(ourInstance);
	}

	unsigned int CParticleFactory::CreateEmittor(const char* aParticleJson, SParticleJsonData& aJsonDataToFill, unsigned int& aMaterialIDToFill)
	{
		std::string identifier = std::string(aParticleJson);
		if (EmitterExists(identifier))
		{
			unsigned int emittorID = GetEmittorID(identifier);
			aJsonDataToFill = myEmittors[emittorID].myJsonData;
			aMaterialIDToFill = myEmittors[emittorID].myMaterialID;
			return emittorID;
		}

		unsigned int emittorID = GetEmittorID(identifier);
		if (emittorID == UINT_MAX)
		{
			return emittorID;
		}

		if (LoadParticleData(aParticleJson, aJsonDataToFill) == false)
		{
			RESOURCE_LOG("[%s] Couldnt load particle data from json.", aParticleJson);
			return UINT_MAX;
		}


		CResourceManager* resourceManager = CResourceManager::Get();
		CParticleEmitter& emittor = myEmittors[emittorID];

		// Material
		unsigned int materialID = UINT_MAX;
		CModelFactory::Get()->MaterialExistsCreateIfNot(aJsonDataToFill.materialName, materialID, true);
		emittor.myMaterialID = materialID;
		aMaterialIDToFill = materialID;

		// Vertex-buffer
		CDX11VertexBuffer* vBuffer = emittor.myVBuffer;
		vBuffer->CreateParticleBuffer(aJsonDataToFill.startLifetime, aJsonDataToFill.emissionRateOverTime, EBufferUsage::DYNAMIC);
		vBuffer = nullptr;

		// GeometryShader
		
		unsigned int gShaderID = UINT_MAX;
		std::string gShaderIdentifier("Data/Shaders/ParticleGS.hlsl");
		std::string gShaderProjectionMode;
		if (aJsonDataToFill.myRenderProjectionMode == ERenderProjectionMode::HORIZONTAL)
		{
			gShaderProjectionMode = "HORIZONTAL_PROJECTION";
		}
		else if (aJsonDataToFill.myRenderProjectionMode == ERenderProjectionMode::VERTICAL)
		{
			gShaderProjectionMode = "VERTICAL_PROJECTION";
		}
		else
		{
			gShaderProjectionMode = "VIEW_PROJECTION";
		}
		if (resourceManager->ShaderExists((gShaderIdentifier + gShaderProjectionMode).c_str()))
		{
			gShaderID = resourceManager->GetShaderID((gShaderIdentifier + gShaderProjectionMode).c_str());
		}
		else
		{
			gShaderID = resourceManager->GetShaderID((gShaderIdentifier + gShaderProjectionMode).c_str());
			CDX11Shader& gShader = resourceManager->GetShaderWithID(gShaderID);

			// Check if particle should be Horizontally-, Vertically- or View-projected.
			const D3D_SHADER_MACRO define[] =
			{
				{ gShaderProjectionMode.c_str(), "1" },
				{ nullptr, nullptr }
			};
			gShader.InitGeometry(gShaderIdentifier.c_str(), "GSMain", define);
		}

		emittor.myGeometryShaderID = gShaderID;
		emittor.myJsonData = aJsonDataToFill;
		return emittorID;
	}

	unsigned int CParticleFactory::CreateStreakEmittor(const char * aTexturePath, const float aMaxLifeTime, const float aSpawnRate)
	{
		std::string identifier = (aTexturePath)+std::to_string(aMaxLifeTime) + std::to_string(aSpawnRate);
		if (SEmitterExists(identifier))
		{
			unsigned int emittorID = GetSEmittorID(identifier);
			return emittorID;
		}

		unsigned int emitterID = GetSEmittorID(identifier);
		if (emitterID == UINT_MAX)
		{
			return emitterID;
		}

		CResourceManager* resourceManager = CResourceManager::Get();
		CStreakEmitter& emitter(mySEmitters[emitterID]);

		CDX11VertexBuffer* vBuffer = emitter.myVBuffer;
		vBuffer->CreateParticleBuffer(aMaxLifeTime, aSpawnRate, EBufferUsage::DYNAMIC);
		vBuffer = nullptr;

		unsigned int vShaderID = UINT_MAX;
		if (resourceManager->ShaderExists("Data/Shaders/StreakVS.hlsl"))
		{
			vShaderID = resourceManager->GetShaderID("Data/Shaders/StreakVS.hlsl");
		}
		else
		{
			vShaderID = resourceManager->GetShaderID("Data/Shaders/StreakVS.hlsl");
			CDX11Shader& vShader = resourceManager->GetShaderWithID(vShaderID);
			vShader.InitVertex("Data/Shaders/StreakVS.hlsl", "VSMain");
		}
		emitter.myVertexShaderID = vShaderID;

		unsigned int gShaderID = UINT_MAX;
		if (resourceManager->ShaderExists("Data/Shaders/StreakGS.hlsl"))
		{
			gShaderID = resourceManager->GetShaderID("Data/Shaders/StreakGS.hlsl");
		}
		else
		{
			gShaderID = resourceManager->GetShaderID("Data/Shaders/StreakGS.hlsl");
			CDX11Shader& gShader = resourceManager->GetShaderWithID(gShaderID);
			gShader.InitGeometry("Data/Shaders/StreakGS.hlsl", "GSMain");
		}
		emitter.myGeometryShaderID = gShaderID;

		unsigned int pShaderID = UINT_MAX;
		if (resourceManager->ShaderExists("Data/Shaders/StreakPS.hlsl"))
		{
			pShaderID = resourceManager->GetShaderID("Data/Shaders/StreakPS.hlsl");
		}
		else
		{
			pShaderID = resourceManager->GetShaderID("Data/Shaders/StreakPS.hlsl");
			CDX11Shader& pShader = resourceManager->GetShaderWithID(pShaderID);
			pShader.InitPixel("Data/Shaders/StreakPS.hlsl", "PSMain");
		}
		emitter.myPixelShaderID = pShaderID;

		unsigned int textureID = resourceManager->GetTextureID(aTexturePath);
		CDX11Texture& texture = resourceManager->GetTextureWithID(textureID);
		texture.CreateTexture(aTexturePath, ETextureType::Diffuse);
		emitter.myTextureID = textureID;

		return emitterID;
	}

	CParticleEmitter& CParticleFactory::GetEmittorWithID(unsigned int aID)
	{
		return myEmittors[aID];
	}

	CStreakEmitter& CParticleFactory::GetSEmittorWithID(unsigned int aID)
	{
		return mySEmitters[aID];
	}

	bool CParticleFactory::LoadParticleData(const char* aDataPath, SParticleJsonData& aStructToFill)
	{
		std::ifstream dataFile(aDataPath);

		if (dataFile.bad())
		{
			return false;
		}

		json fileReader;
		try
		{
			dataFile >> fileReader;
			dataFile.close();
		}
		catch (...)
		{
			ENGINE_LOG("ERROR: Wrong values in AI data file %s.", aDataPath);
			return false;
		}

		aStructToFill.textureFileName = std::string("Data/Particles/") + fileReader["texturePath"].get<std::string>() + std::string(".dds");
		aStructToFill.materialName = std::string("Data/Materials/") + fileReader["materialName"].get<std::string>() + std::string(".material");

		const int renderProjMode = fileReader["myRenderProjectionMode"].get<int>();
		switch (renderProjMode)
		{
		case 0:
			aStructToFill.myRenderProjectionMode = ERenderProjectionMode::VIEW;
			break;
		case 1:
			aStructToFill.myRenderProjectionMode = ERenderProjectionMode::HORIZONTAL;
			break;
		case 2:
			aStructToFill.myRenderProjectionMode = ERenderProjectionMode::VERTICAL;
			break;
		}

		aStructToFill.duration = LoadFloat(fileReader, "duration");
		aStructToFill.looping = LoadBool(fileReader, "looping");
		aStructToFill.startLifetime = LoadFloat(fileReader, "startLifetime");
		aStructToFill.startDelay = LoadFloat(fileReader, "startDelay");
		aStructToFill.startSpeed = LoadFloat(fileReader, "startSpeed");
		aStructToFill.startColor = LoadVec4(fileReader, "startColor");
		aStructToFill.startSize = LoadFloat(fileReader, "startSize");
		aStructToFill.startRotationMin = LoadFloat(fileReader, "startRotationMin");
		aStructToFill.startRotationMax = LoadFloat(fileReader, "startRotationMax");
		aStructToFill.startSizeCurve = LoadFloat(fileReader, "startSizeCurve");
		aStructToFill.gravityModifier = LoadFloat(fileReader, "gravityModifier");
		aStructToFill.maxParticles = LoadInt(fileReader, "maxParticles");
		aStructToFill.emissionRateOverTime = LoadFloat(fileReader, "emissionRateOverTime");
		aStructToFill.velocityOverLife = LoadVec3(fileReader, "velocityOverLife");
		aStructToFill.useSheetAnim = LoadBool(fileReader, "useSheetAnim");

		if (LoadBool(fileReader, "useRotationOverLifetime"))
		{
			aStructToFill.useRotationOverLifetime = true;
			aStructToFill.rotationOverLife = LoadVec3(fileReader, "rotationOverLife");
			aStructToFill.rotationOverLifeMin = LoadVec3(fileReader, "rotationOverLifeMin");
			aStructToFill.rotationOverLifeMax = LoadVec3(fileReader, "rotationOverLifeMax");
		}
		else
		{
			aStructToFill.useRotationOverLifetime = false;
		}

		aStructToFill.direction = LoadVec3(fileReader, "direction");
		aStructToFill.direction.x = CU::ToRadians(aStructToFill.direction.x);
		aStructToFill.direction.y = CU::ToRadians(aStructToFill.direction.y);
		aStructToFill.direction.z = CU::ToRadians(aStructToFill.direction.z);

		if (LoadBool(fileReader, "useSizeOverLifetime"))
		{
			aStructToFill.useSizeOverLifetime = true;
			if (LoadBool(fileReader, "useSizeCurve"))
			{
				aStructToFill.useSizeCurve = true;
				aStructToFill.endSize = LoadFloat(fileReader, "endSize");
			}
			else
			{
				aStructToFill.useSizeCurve = false;
				aStructToFill.startSizeMin = LoadFloat(fileReader, "startSizeMin");
				aStructToFill.startSizeMax = LoadFloat(fileReader, "startSizeMax");
			}
		}
		else
		{
			aStructToFill.useSizeCurve = false;
			aStructToFill.useSizeOverLifetime = false;
		}

		aStructToFill.endSize = LoadFloat(fileReader, "endSize");
		aStructToFill.useBoxOffset = LoadBool(fileReader, "useBoxOffset");
		aStructToFill.spawnPositionLimits = LoadVec3(fileReader, "spawnPositionLimits");
		if (LoadBool(fileReader, "useColorOverLife"))
		{
			aStructToFill.useColorOverLife = true;

			unsigned int i = 0;
			for (auto& time : fileReader["colorgrad1"])
			{
				aStructToFill.colorgrad1[i] = CU::Vector4f(time["r"].get<float>(), time["g"].get<float>(), time["b"].get<float>(), time["a"].get<float>());
				++i;
			}

			if (LoadBool(fileReader, "randomTwoGradients"))
			{
				aStructToFill.randomTwoGradients = true;
				unsigned int j = 0;
				for (auto& time : fileReader["colorgrad2"])
				{
					aStructToFill.colorgrad2[j] = CU::Vector4f(time["r"].get<float>(), time["g"].get<float>(), time["b"].get<float>(), time["a"].get<float>());
					++j;
				}
			}
			else
			{
				aStructToFill.randomTwoGradients = false;
			}
		}
		else
		{
			aStructToFill.useColorOverLife = false;
		}



		return true;
	}

	bool CParticleFactory::EmitterExists(const std::string& aIdentifier)
	{
		if (myAssignedEmittorIDs.find(aIdentifier) != myAssignedEmittorIDs.end())
		{
			return true;
		}
		return false;
	}

	unsigned int CParticleFactory::GetEmittorID(const std::string& aIdentifier)
	{
		if (myAssignedEmittorIDs.find(aIdentifier) != myAssignedEmittorIDs.end())
		{
			return myAssignedEmittorIDs[aIdentifier];
		}
		else
		{
			if (myCurrentlyFreeEmittorID > (MAX_EMITTORS - 1))
			{
				assert(false && "Too many emittors created");
				RESOURCE_LOG("ERROR! Too many emittors created");
				return UINT_MAX;
			}
			myAssignedEmittorIDs[aIdentifier] = myCurrentlyFreeEmittorID;
			return myCurrentlyFreeEmittorID++;
		}
	}

	bool CParticleFactory::SEmitterExists(const std::string & aIdentifier)
	{
		if (myAssignedSEmittorIDs.find(aIdentifier) != myAssignedSEmittorIDs.end())
		{
			return true;
		}
		return false;
	}

	unsigned int CParticleFactory::GetSEmittorID(const std::string & aIdentifier)
	{
		if (myAssignedSEmittorIDs.find(aIdentifier) != myAssignedSEmittorIDs.end())
		{
			return myAssignedSEmittorIDs[aIdentifier];
		}
		else
		{
			if (myCurrentlyFreeSEmittorID > (MAX_EMITTORS - 1))
			{
				assert(false && "Too many emittors created");
				RESOURCE_LOG("ERROR! Too many emittors created");
				return UINT_MAX;
			}
			myAssignedSEmittorIDs[aIdentifier] = myCurrentlyFreeSEmittorID;
			return myCurrentlyFreeSEmittorID++;
		}
	}

}}