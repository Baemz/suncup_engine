#include "stdafx.h"
#include "Scene.h"
#include "..\Model\ModelInstance.h"
#include "Camera\Camera.h"
#include "ResourceManager\ResourceManager.h"
#include "Lights\LightFactory.h"
#include "Sprite\Sprite.h"
#include "Sprite\Sprite3D.h"
#include "Lights\PointLight.h"
#include "Lights\SpotLight.h"

#include <iostream>
#include "Particles\ParticleEmitterInstance.h"
#include "Material.h"
#include "Model\Loading\ModelFactory.h"
#include "Particles\ParticleEngine\ParticleInterface.h"
#include "Particles\ParticleFactory.h"
#include "Particles\ParticleEmitter.h"
using namespace sce::gfx;

CScene::CScene()
	: myWriteIndex(0)
	, myReadIndex(1)
	, myFreeIndex(2)
	, myPWriteIndex(0)
	, myPReadIndex(1)
	, myPFreeIndex(2)
	, myTWriteIndex(0)
	, myTReadIndex(1)
	, myTFreeIndex(2)
{
	myBuffers[myReadIndex].myModelInstances.Init(1000);
	myBuffers[myReadIndex].myPointLightInstances.Init(100);
	myBuffers[myReadIndex].mySpotLightInstances.Init(100);
	myBuffers[myWriteIndex].myModelInstances.Init(1000);
	myBuffers[myWriteIndex].myPointLightInstances.Init(100);
	myBuffers[myWriteIndex].mySpotLightInstances.Init(100);
	myBuffers[myFreeIndex].myModelInstances.Init(1000);
	myBuffers[myFreeIndex].myPointLightInstances.Init(100);
	myBuffers[myFreeIndex].mySpotLightInstances.Init(100);
	myBuffers[myFreeIndex].myShaderEffectInstances.Init(20);
}


CScene::~CScene()
{
	for (auto& buffer : myBuffers)
	{
		if (buffer.myModelInstances.Empty() == false)
		{
			buffer.myModelInstances.Resize(buffer.myModelInstances.Capacity());
		}

		for (auto& model : buffer.myModelInstances)
		{
			model.myLoadingState = CModelInstance::ELoadingState::Loaded;
		}

		buffer.mySkybox.myLoadingState = CModelInstance::ELoadingState::Loaded;
	}
}

void CScene::AddModelInstance(const CModelInstance& aModelInstance)
{
	if (aModelInstance.myModelName == "InvertedCube")
	{
		myBuffers[myWriteIndex].mySkybox = aModelInstance;
	}
	else
	{
		myBuffers[myWriteIndex].myModelInstances.Add(aModelInstance);
	}
}

void sce::gfx::CScene::AddPointLightInstance(const CPointLightInstance& aPLInstance)
{
	myBuffers[myWriteIndex].myPointLightInstances.Add(aPLInstance);
}

void sce::gfx::CScene::AddSpotLightInstance(const CSpotLightInstance& aSLInstance)
{
	myBuffers[myWriteIndex].mySpotLightInstances.Add(aSLInstance);
}

void sce::gfx::CScene::AddShaderEffectInstance(const CShaderEffectInstance & aShaderEffectInstance)
{
	myBuffers[myWriteIndex].myShaderEffectInstances.Add(aShaderEffectInstance);
}

void sce::gfx::CScene::AddText(const CText& aText)
{
	std::unique_lock<std::mutex> bufferLock(myTBufferLock);
	myTextBuffers[myTWriteIndex].myTexts.Add(aText);
}

void sce::gfx::CScene::SetCamera(const CCameraInstance& aCameraInstance)
{
	myBuffers[myWriteIndex].myCurrentCamera = aCameraInstance;
}

void sce::gfx::CScene::SetEnvironmentalLight(const CEnvironmentalLight& aEnvLight)
{
	myBuffers[myWriteIndex].myEnvLight = aEnvLight;
}

void sce::gfx::CScene::SetDirectionalLight(const CDirectionalLight& aDirLight)
{
	myBuffers[myWriteIndex].myDirLight = aDirLight;
}

CDirectionalLight & sce::gfx::CScene::GetDirectionalLight()
{
	return myBuffers[myReadIndex].myDirLight;
}

CEnvironmentalLight& sce::gfx::CScene::GetEnvironmentalLight()
{
	return myBuffers[myReadIndex].myEnvLight;
}

void sce::gfx::CScene::AddSprite(const CSprite& aSprite)
{
	myBuffers[myWriteIndex].mySprites.push_back(aSprite);
}

void sce::gfx::CScene::AddSprite3D(const CSprite3D& a3DSprite)
{
	myBuffers[myWriteIndex].my3DSprites.push_back(a3DSprite);
}

void sce::gfx::CScene::GetSpriteList(CU::GrowingArray<CSprite, unsigned int>& aListOfSprites)
{
	std::sort(myBuffers[myReadIndex].mySprites.begin(), myBuffers[myReadIndex].mySprites.begin() + myBuffers[myReadIndex].mySprites.size());
	for (auto& sprite : myBuffers[myReadIndex].mySprites)
	{
		aListOfSprites.Add(sprite);
	}
}

void sce::gfx::CScene::GetSprite3DList(CU::GrowingArray<CSprite3D, unsigned int>& aListOf3DSprites)
{
	std::sort(myBuffers[myReadIndex].my3DSprites.begin(), myBuffers[myReadIndex].my3DSprites.begin() + myBuffers[myReadIndex].my3DSprites.size());
	for (auto& sprite3D : myBuffers[myReadIndex].my3DSprites)
	{
		aListOf3DSprites.Add(sprite3D);
	}
}

void sce::gfx::CScene::GetTextList(CU::GrowingArray<CText, unsigned int>& aListOfTexts)
{
	std::unique_lock<std::mutex> bufferLock(myTBufferLock);
	aListOfTexts = myTextBuffers[myTReadIndex].myTexts;
}

bool CScene::Cull(SRenderData& aRenderData)
{
	bool culled(false);
	if (myBuffers[myReadIndex].myCurrentCamera.myCameraID != UINT_MAX)
	{
		culled = true;

		CResourceManager* resourceManager = CResourceManager::Get();
		CCamera& camera(resourceManager->GetCameraWithID(myBuffers[myReadIndex].myCurrentCamera.myCameraID));

		// Skybox should always render first
		if (myBuffers[myReadIndex].mySkybox.IsLoaded())
		{
			myBuffers[myReadIndex].mySkybox.myOrientation.SetPosition(myBuffers[myReadIndex].myCurrentCamera.GetPosition());
			aRenderData.myListToRender.Add(myBuffers[myReadIndex].mySkybox);
		}
		else
		{
			aRenderData.myListToRender.Add(CModelInstance());
		}
		for (unsigned int index = 0; index < myBuffers[myReadIndex].myModelInstances.Size(); ++index)
		{
			CModelInstance& ml(myBuffers[myReadIndex].myModelInstances[index]);
			CMaterial& material(CModelFactory::Get()->GetMaterialWithID(ml.myMaterialID));
			if (material.IsLoaded())
			{
				ml.UpdateColliderPosition();
				if (ml.IsCullable() == false)
				{
					if (material.GetRenderMode() == ERenderMode::Forward)
					{
						aRenderData.myTransparentListToRender.Add(ml);
					}
					else
					{
						aRenderData.myListToRender.Add(ml);
					}
				}
				else if (camera.Intersects(ml.myFrustumCollider))
				{
					if (material.GetRenderMode() == ERenderMode::Forward)
					{
						aRenderData.myTransparentListToRender.Add(ml);
					}
					else
					{
						aRenderData.myListToRender.Add(ml);
					}
				}
				else
				{
					if (ml.IsCastingShadows() == false)
					{
						continue;
					}

					CU::Vector3f tempCamPos(myBuffers[myReadIndex].myCurrentCamera.GetPosition() + myBuffers[myReadIndex].myCurrentCamera.GetLook() * 30.f);
					CU::Vector3f dirLightTempPos((tempCamPos)+(myBuffers[myReadIndex].myDirLight.myLightData.myToLightDirection * (50.f)));
					float length = (ml.GetPosition() - dirLightTempPos).Length2();
					if ((length > 0.f) && (length <= (myBuffers[myReadIndex].myCurrentCamera.GetPosition() - dirLightTempPos).Length2()))
					{
						if (material.GetRenderMode() == ERenderMode::Forward)
						{
							aRenderData.myTransparentListToRender.Add(ml);
						}
						else
						{
							aRenderData.myListToRender.Add(ml);
						}
					}
				}
			}
		}

		// SORT DEFERRED-LIST
		// +1 to account for the reserved skybox-model at index 0.
		std::sort(aRenderData.myListToRender.begin() + 1, aRenderData.myListToRender.end(),
			[](const CModelInstance& aFirst, const CModelInstance& aSecond) -> bool
		{
			return (aFirst.GetMaterialID() < aSecond.GetMaterialID());
		}
		);
		////////////////////

		// SORT FORWARD-LIST
		CU::Vector3f camPos(aRenderData.myCameraInstance.GetPosition());
		std::sort(aRenderData.myTransparentListToRender.begin(), aRenderData.myTransparentListToRender.end(),
			[&aRenderData, &camPos](const CModelInstance& aFirst, const CModelInstance& aSecond) -> const bool
		{
			const float dist1 = fabs((camPos - aFirst.GetPosition()).Length2());
			const float dist2 = fabs((camPos - aSecond.GetPosition()).Length2());
			const bool result = (dist1 > dist2);
			return result;
		}
		);
		////////////////////


		for (unsigned int index = 0; index < myBuffers[myReadIndex].myPointLightInstances.Size(); ++index)
		{
			CPointLightInstance& pl(myBuffers[myReadIndex].myPointLightInstances[index]);
			if (camera.Intersects(pl.myPosition, pl.myRange))
			{
				aRenderData.myPointLightData.Add(pl);
			}
		}

		for (unsigned int index = 0; index < myBuffers[myReadIndex].mySpotLightInstances.Size(); ++index)
		{
			CSpotLightInstance& sl(myBuffers[myReadIndex].mySpotLightInstances[index]);
			if (camera.Intersects(sl.myPosition, sl.myRange))
			{
				aRenderData.mySpotLightData.Add(sl);
			}
		}

		for (unsigned int index = 0; index < myBuffers[myReadIndex].myShaderEffectInstances.Size(); ++index)
		{
			aRenderData.myShaderEffectsToRender.Add(myBuffers[myReadIndex].myShaderEffectInstances[index]);
		}

		GetParticleRenderBuffer();
		unsigned int size = myParticleBuffer.myParticleInstances.Size();
		if (size > 0)
		{
			for (unsigned int index = 0; index < size; ++index)
			{
				//if (camera.Intersects(myParticleBuffer[myPReadIndex].myEmittorInstances[index].myFrustumCollider))
				//{
				if (myParticleBuffer.myParticleInstances[index].myMaterialID == UINT_MAX)
				{
					continue;
				}
				CMaterial& material(CModelFactory::Get()->GetMaterialWithID(myParticleBuffer.myParticleInstances[index].myMaterialID));
				if (material.GetRenderMode() == ERenderMode::Particle)
				{
					aRenderData.myParticleEmittorsToRender.Add(myParticleBuffer.myParticleInstances[index]);
				}
				//}
			}
		}

// 		size = myParticleBuffer[myPReadIndex].myStreakInstances.Size();
// 		if (size > 0)
// 		{
// 			for (unsigned int index = 0; index < size; ++index)
// 			{
// 				//if (camera.Intersects(myParticleBuffer[myPReadIndex].myStreakEmittorsToRender[index].myFrustumCollider))
// 				//{
// 				aRenderData.myStreakEmittorsToRender.Add(myParticleBuffer[myPReadIndex].myStreakInstances[index]);
// 				//}
// 			}
// 		}
	}
	else
	{
		for (unsigned int index = 0; index < myBuffers[myReadIndex].myShaderEffectInstances.Size(); ++index)
		{
			aRenderData.myShaderEffectsToRender.Add(myBuffers[myReadIndex].myShaderEffectInstances[index]);
		}
	}
	
	//std::cout << "[" << aRenderData.myListToRender.Size() << "] ml rendered." << std::endl;
	ChangeTextRenderBuffer();
	ChangeRenderBuffer();
	return culled;
}

CCameraInstance& sce::gfx::CScene::GetCamera()
{
	return myBuffers[myReadIndex].myCurrentCamera;
}

void sce::gfx::CScene::ChangeUpdateBuffer()
{
	std::unique_lock<std::mutex> bufferLock(myBufferLock);
	Swap(myWriteIndex, myFreeIndex);
	bufferLock.unlock();
	myBuffers[myWriteIndex].myCurrentCamera = CCameraInstance();
	myBuffers[myWriteIndex].myEnvLight = CEnvironmentalLight();
	myBuffers[myWriteIndex].mySkybox = CModelInstance();
	myBuffers[myWriteIndex].myModelInstances.RemoveAll();
	myBuffers[myWriteIndex].myPointLightInstances.RemoveAll();
	myBuffers[myWriteIndex].mySpotLightInstances.RemoveAll();
	myBuffers[myWriteIndex].myShaderEffectInstances.RemoveAll();
	myBuffers[myWriteIndex].mySprites.clear();
	myBuffers[myWriteIndex].my3DSprites.clear();
	myBuffers[myWriteIndex].myIsRead = false;

	ChangeTextBuffer();
}

void sce::gfx::CScene::ChangeTextBuffer()
{
	std::unique_lock<std::mutex> bufferLock(myTBufferLock);
	Swap(myTWriteIndex, myTFreeIndex);
	bufferLock.unlock();
	myTextBuffers[myTWriteIndex].myTexts.RemoveAll();
	myTextBuffers[myTWriteIndex].myIsRead = false;
}

void sce::gfx::CScene::ChangeRenderBuffer()
{
	std::unique_lock<std::mutex> bufferLock(myBufferLock);
	if (myBuffers[myFreeIndex].myIsRead == false)
	{
		Swap(myReadIndex, myFreeIndex);
	}
	bufferLock.unlock();
	myBuffers[myReadIndex].myIsRead = true;
}

void sce::gfx::CScene::GetParticleRenderBuffer()
{
	myParticleBuffer.myParticleInstances.RemoveAll();

	CParticleInterface* particleInterface(CParticleInterface::Get());
	if (particleInterface == nullptr)
	{
		return;
	}

	particleInterface->GetLatestParticles(myParticleBuffer.myParticleInstances);
}

void sce::gfx::CScene::ChangeTextRenderBuffer()
{
	std::unique_lock<std::mutex> bufferLock(myTBufferLock);
	if (myTextBuffers[myTFreeIndex].myIsRead == false)
	{
		Swap(myTReadIndex, myTFreeIndex);
	}
	bufferLock.unlock();
	myTextBuffers[myTReadIndex].myIsRead = true;
}
