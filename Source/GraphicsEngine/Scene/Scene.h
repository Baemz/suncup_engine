#pragma once
#include "../CommonUtilities/GrowingArray.h"
#include "Camera/CameraInstance.h"
#include "Lights/EnvironmentalLight.h"
#include "Utilities/LightStructs.h"
#include "Lights/PointLightInstance.h"
#include "Lights/SpotLightInstance.h"
#include "ShaderEffects/ShaderEffectInstance.h"
#include "Utilities/RenderStructs.h"
#include "Text/Text.h"
#include <mutex>
#include "Lights/DirectionalLight.h"
#include "Particles/ParticleEngine/ParticleTypes.h"

namespace sce { namespace gfx {

	class CModelInstance;
	class CCameraInstance;
	class CParticleEmitterInstance;
	class CStreakEmitterInstance;
	class CSprite;
	class CSprite3D;

	class CScene
	{
	public:
		CScene();
		~CScene();

		void AddModelInstance(const CModelInstance& aModelInstance);
		void AddPointLightInstance(const CPointLightInstance& aPLInstance);
		void AddSpotLightInstance(const CSpotLightInstance& aSLInstance);
		void AddStreakEmittorInstance(const CStreakEmitterInstance& aStreakEmittorInstance);
		void AddShaderEffectInstance(const CShaderEffectInstance& aShaderEffectInstance);
		void AddText(const CText& aText);
		void AddSprite(const CSprite& aSprite);
		void AddSprite3D(const CSprite3D& a3DSprite);
		void GetSpriteList(CU::GrowingArray<CSprite, unsigned int>& aListOfSprites);
		void GetSprite3DList(CU::GrowingArray<CSprite3D, unsigned int>& aListOf3DSprites);

		void GetTextList(CU::GrowingArray<CText, unsigned int>& aListOfTexts);

		bool Cull(SRenderData& aRenderData);
		
		void SetEnvironmentalLight(const CEnvironmentalLight& aEnvLight);
		CEnvironmentalLight& GetEnvironmentalLight();
		void SetDirectionalLight(const CDirectionalLight & aDirLight);
		CDirectionalLight& GetDirectionalLight();

		void SetCamera(const CCameraInstance& aCameraInstance);
		CCameraInstance& GetCamera();
		
		void ChangeUpdateBuffer();

	private:
		struct SRenderQueueBuffer
		{
			CU::GrowingArray<CModelInstance, unsigned int> myModelInstances;
			CU::GrowingArray<CPointLightInstance, unsigned int> myPointLightInstances;
			CU::GrowingArray<CSpotLightInstance, unsigned int> mySpotLightInstances;
			std::vector<CSprite> mySprites;
			std::vector<CSprite3D> my3DSprites;
			CU::GrowingArray<CShaderEffectInstance, unsigned int> myShaderEffectInstances;
			CModelInstance mySkybox;
			CCameraInstance myCurrentCamera;
			CEnvironmentalLight myEnvLight;
			CDirectionalLight myDirLight;
			bool myIsRead;
		};

		struct SParticleRenderQueueBuffer
		{
			CU::GrowingArray<CParticleEmitterInstance, ParticleIDType> myParticleInstances;
			//CU::GrowingArray<CStreakEmitterInstance, unsigned int> myStreakInstances;
		};

		struct STextRenderQueueBuffer
		{
			CU::GrowingArray<CText, unsigned int> myTexts;
			bool myIsRead = false;
		};

	private:
		unsigned char myFreeIndex;
		unsigned char myReadIndex;
		unsigned char myWriteIndex;

		unsigned char myPFreeIndex;
		unsigned char myPReadIndex;
		unsigned char myPWriteIndex;

		unsigned char myTFreeIndex;
		unsigned char myTReadIndex;
		unsigned char myTWriteIndex;

		SRenderQueueBuffer myBuffers[3];
		SParticleRenderQueueBuffer myParticleBuffer;
		STextRenderQueueBuffer myTextBuffers[3];

		std::mutex myBufferLock;
		std::mutex myTBufferLock;

		void ChangeTextBuffer();

		void ChangeRenderBuffer();
		void GetParticleRenderBuffer();
		void ChangeTextRenderBuffer();

	};
}}
