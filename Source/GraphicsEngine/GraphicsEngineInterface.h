#pragma once
#include "../CommonUtilities/Vector2.h"
namespace std
{
	template<class T>
	class function;
}


namespace sce
{
	class CEngine;
}
namespace sce { namespace gfx {

	class CModelInstance;
	class CCameraInstance;
	class CGraphicsEngine;
	class CSprite;
	class CSprite3D;
	class CEnvironmentalLight;
	class CDirectionalLight;
	class CPointLightInstance;
	class CSpotLightInstance;
	class CParticleEmitterInstance;
	class CStreakEmitterInstance;
	class CShaderEffectInstance;
	class CText;

	class CGraphicsEngineInterface
	{
		friend CEngine;
		friend class CWindowHandler;
	public:

		static void AddToScene(const CModelInstance& aInstance);
		static void AddToScene(const CCameraInstance& aCameraInstance);
		static void AddToScene(const CSprite& aSprite);
		static void AddToScene(const CSprite3D& a3DSprite);
		static void AddToScene(const CPointLightInstance& aPLInstance);
		static void AddToScene(const CSpotLightInstance& aPLInstance);
		static void AddToScene(const CText& aText);
		static void AddToScene(const CShaderEffectInstance& aShaderEffectInstance);
		static void UseForRendering(const CEnvironmentalLight& aEnvLight);
		static void UseForRendering(const CDirectionalLight& aDirLight);

		static void EnableCursor();
		static void DisableCursor();
		static bool LoadCustomCursor(const unsigned char& aIDToUse, const std::string& aCursorPath);
		static bool UnloadCustomCursor(const unsigned char& aID);
		static bool SetCustomCursor(const unsigned char& aID);

		static const bool IsFullscreen();
		static void ToggleFullscreen();
		static void TakeScreenshot(const wchar_t* aDestinationPath);
		
		static const CU::Vector2f GetResolution();
		static const CU::Vector2f GetFullResolution();
		static const CU::Vector2f GetWindowedResolution();
		static const CU::Vector2f GetFullWindowedResolution();
		static const CU::Vector2f& GetTargetResolution();
		static const float GetRatio();

		static void SetResolution(const CU::Vector2f& aNewResolution);
		
		static const bool IsVSyncEnabled();
		static const bool IsSSAOEnabled();
		static const bool IsColorGradingEnabled();
		static const bool IsLinearFogEnabled();
		static const bool IsBloomEnabled();
		static const bool IsFXAAEnabled();
		static void ToggleVSync();
		static void ToggleSSAO();
		static void ToggleColorGrading();
		static void ToggleLinearFog();
		static void ToggleFXAA();
		static void ToggleBloom();
		static void SetVSync(const bool aUseVsync);
		static void SetSSAO(const bool aBool);
		static void SetLinearFog(const bool aBool);
		static void SetBloom(const bool aBool);
		static void SetFXAA(const bool aBool);

		static void SetConsoleRenderCallback(std::function<void()> aFunc);

	private:
		static const CU::Vector2f myTargetResolution;

		CGraphicsEngineInterface() = delete;
		~CGraphicsEngineInterface() = delete;
		static CGraphicsEngine* myGraphicsEngine;
	};
}}