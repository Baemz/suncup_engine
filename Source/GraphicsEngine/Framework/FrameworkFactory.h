#pragma once
namespace sce { namespace gfx {

	class CVertexBuffer;
	class CShader;
	class CIndexBuffer;
	class CConstantBuffer;
	class CTexture2D;

	class CFrameworkFactory
	{
	public:
		CFrameworkFactory();
		~CFrameworkFactory();

		unsigned int GetVertexBufferID();
		unsigned int GetShaderID();
		unsigned int GetIndexBufferID();
		unsigned int GetConstantBufferID();
		unsigned int GetTexture2DID();

	private:
		CVertexBuffer* myVertexBuffers;
		CShader* myShaders;
		CIndexBuffer* myIndexBuffers;
		CConstantBuffer* myConstantBuffers;
		CTexture2D* myTexture2Ds;
	};
}}