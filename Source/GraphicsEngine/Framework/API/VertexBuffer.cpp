#include "stdafx.h"
#include "VertexBuffer.h"

using namespace sce::gfx;

CVertexBuffer::CVertexBuffer()
	: myVertexCount(0)
	, myStride(0)
	, myOffset(0)
{
}


CVertexBuffer::~CVertexBuffer()
{
}
