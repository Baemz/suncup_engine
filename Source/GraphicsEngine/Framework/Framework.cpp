#include "stdafx.h"
#include "Framework.h"
#include "../DirectXFramework/Direct3D11.h"

using namespace sce::gfx;

CFramework* CFramework::ourFramework = nullptr;
eRenderAPI CFramework::ourRenderAPI = eRenderAPI::NONE;

CFramework::CFramework()
{
	
}


CFramework::~CFramework()
{
}


CFramework* CFramework::Create(const eRenderAPI& aRenderAPI)
{
	switch (aRenderAPI)
	{
		case eRenderAPI::DirectX11:
			ourRenderAPI = eRenderAPI::DirectX11;
			ourFramework = sce_new(CDirect3D11());
			return ourFramework;

		case eRenderAPI::OpenGL:
			ourRenderAPI = eRenderAPI::OpenGL;

			break;
	}
	return nullptr;
}

void CFramework::Destroy()
{
	SAFE_DELETE(ourFramework);
}
