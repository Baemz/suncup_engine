#include "stdafx.h"
#include "FrameworkFactory.h"
#include "Framework.h"

namespace sce { namespace gfx {

	static eRenderAPI RENDER_API = eRenderAPI::NONE;

	CFrameworkFactory::CFrameworkFactory()
		: myVertexBuffers(nullptr)
		, myIndexBuffers(nullptr)
		, myConstantBuffers(nullptr)
		, myShaders(nullptr)
		, myTexture2Ds(nullptr)
	{
		RENDER_API = CFramework::GetRenderAPI();
	}


	CFrameworkFactory::~CFrameworkFactory()
	{
	}

	unsigned int CFrameworkFactory::GetVertexBufferID()
	{
		return UINT_MAX;
	}

	unsigned int CFrameworkFactory::GetShaderID()
	{
		return UINT_MAX;
	}

	unsigned int CFrameworkFactory::GetIndexBufferID()
	{
		return UINT_MAX;
	}

	unsigned int CFrameworkFactory::GetConstantBufferID()
	{
		return UINT_MAX;
	}

	unsigned int CFrameworkFactory::GetTexture2DID()
	{
		return UINT_MAX;
	}

}}