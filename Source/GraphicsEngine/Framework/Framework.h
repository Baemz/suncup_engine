#pragma once

namespace sce { namespace gfx {
	enum class eRenderAPI
	{
		DirectX11,
		DirectX12,
		OpenGL,
		NONE,
	};

	class CWindowHandler;
	class CFramework
	{
	public:
		virtual ~CFramework();

		virtual bool Init(CWindowHandler& aWindowHandler, const bool aStartInFullscreen, const bool aUseVSync) = 0;

		virtual void BeginFrame(float aClearColor[4]) = 0;
		virtual void EndFrame() = 0;

		virtual void TakeScreenshot(const wchar_t* aDestinationPath) = 0;
		virtual void ToggleFullscreen() = 0;
		virtual void SetResolution(const unsigned int aWidth, const unsigned int aHeight) = 0;

		virtual void ActivateScreenTarget() = 0;

		// HACK (?)
		virtual void SetVsync(const bool aUseVsync) = 0;
		virtual const bool IsVsyncEnabled() const = 0;


		static inline const eRenderAPI& GetRenderAPI() { return ourRenderAPI; };
		static CFramework* Create(const eRenderAPI& aRenderAPI);
		static void Destroy();

	protected:
		CFramework();

		static CFramework* ourFramework;
		static eRenderAPI ourRenderAPI;
	};
}}
