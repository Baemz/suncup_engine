#include "stdafx.h"
#include "Material.h"
#include "DirectXFramework\API\DX11Texture2D.h"
#include "DirectXFramework\API\DX11ConstantBuffer.h"
#include "ResourceManager\ResourceManager.h"
#include "../EngineCore/JSON/json.hpp"

using json = nlohmann::json;
namespace sce { namespace gfx {

	CMaterial::CMaterial()
		: myVertexShaderID(UINT_MAX)
		, myPixelShaderID(UINT_MAX)
		, myTextures(nullptr)
		, myLoadingState(ELoadingState::Unloaded)
	{
	}

	CMaterial::~CMaterial()
	{
		Destroy();
	}

	void CMaterial::Destroy()
	{
		sce_delete(myTextures);
	}

	const bool CMaterial::Init(const std::string& aMaterial)
	{
		if (FileExists(aMaterial.c_str()) == false)
		{
			return false;
		}
		sce_delete(myTextures);
		myTextures = sce_newArray(CDX11Texture, MAX_TEXTURE_SLOT);

		SMatData matData;
		std::ifstream dataFile(aMaterial);
		json fileReader;
		if (dataFile.good() == false)
		{
			dataFile.close();
			return false;
		}
		else
		{
			try
			{
				dataFile >> fileReader;
			}
			catch (...)
			{
				RESOURCE_LOG("[%s] failed to load.", aMaterial.c_str());
				dataFile.close();
				return false;
			}
			dataFile.close();

			matData.myRenderMode = static_cast<ERenderMode>(fileReader["renderMode"].get<unsigned int>());
			std::string path;
			std::string entry;
			unsigned char index = 0;
			for (auto& shader : fileReader["vertexShader"])
			{
				if (index == 0)
				{
					entry = shader.get<std::string>();
				}
				else
				{
					path = shader.get<std::string>();
				}
				++index;
			}
			AddVertexShader(path, entry);
			index = 0;
			for (auto& shader : fileReader["pixelShader"])
			{
				if (index == 0)
				{
					entry = shader.get<std::string>();
				}
				else
				{
					path = shader.get<std::string>();
				}
				++index;
			}
			AddPixelShader(path, entry);

			for (auto& tex : fileReader["textureData"])
			{
				STexData texData;
				unsigned char tIndex = 0;
				for (auto& data : tex)
				{
					if (tIndex == 0)
					{
						texData.path = data.get<std::string>();
					}
					else
					{
						texData.slot = data.get<unsigned int>();
					}
					++tIndex;
				}
				matData.myTextureSlots.push_back(texData);
			}
		}


		myRenderMode = matData.myRenderMode;
		for (auto& tex : matData.myTextureSlots)
		{
			AddTextureAtSlot(tex.path, tex.slot);
		}

		return true;
	}

	bool CMaterial::Bind()
	{
		if (myPixelShaderID == UINT_MAX || myVertexShaderID == UINT_MAX)
		{
			return false;
		}

		// Bind shaders
		CResourceManager* resourceManager = CResourceManager::Get();
		CDX11Shader& vs(resourceManager->GetShaderWithID(myVertexShaderID));
		CDX11Shader& ps(resourceManager->GetShaderWithID(myPixelShaderID));

		if (vs.IsLoading() || ps.IsLoading())
		{
			return false;
		}
		else
		{
			vs.Bind();
			ps.Bind();
		}

		// Bind resources
		for (unsigned short i = 0; i < MAX_TEXTURE_SLOT; ++i)
		{
			if (myTextures[i].myIsLoaded)
			{
				myTextures[i].Bind();
			}
		}

		return true;
	}

	void CMaterial::BindOnlyTextures()
	{
		for (unsigned short i = 0; i < MAX_TEXTURE_SLOT; ++i)
		{
			if (myTextures[i].myIsLoaded)
			{
				myTextures[i].Bind();
			}
		}
	}

	void CMaterial::AddVertexShader(const std::string& aShader, const std::string& aEntryPoint)
	{
		CResourceManager* resourceManager = CResourceManager::Get();
		const std::string identifier(aShader + aEntryPoint);
		if (resourceManager->ShaderExists(identifier.c_str()))
		{
			myVertexShaderID = resourceManager->GetShaderID(identifier.c_str());
		}
		else
		{
			unsigned int shaderID = resourceManager->GetShaderID(identifier.c_str());
			CDX11Shader& shader = resourceManager->GetShaderWithID(shaderID);
			shader.InitVertex(aShader.c_str(), aEntryPoint.c_str());
			myVertexShaderID = shaderID;
		}
	}

	void CMaterial::AddPixelShader(const std::string& aShader, const std::string& aEntryPoint)
	{
		CResourceManager* resourceManager = CResourceManager::Get();
		const std::string identifier(aShader + aEntryPoint);
		if (resourceManager->ShaderExists(identifier.c_str()))
		{
			myPixelShaderID = resourceManager->GetShaderID(identifier.c_str());
		}
		else
		{
			unsigned int shaderID = resourceManager->GetShaderID(identifier.c_str());
			CDX11Shader& shader = resourceManager->GetShaderWithID(shaderID);
			shader.InitPixel(aShader.c_str(), aEntryPoint.c_str());
			myPixelShaderID = shaderID;
		}
	}

	void CMaterial::AddTextureAtSlot(const std::string& aTexture, const unsigned int aSlot)
	{
		if (aSlot <= MAX_TEXTURE_SLOT)
		{
			if (myTextures[aSlot].CreateTexture(aTexture.c_str(), aSlot))
			{
				myTextures[aSlot].myIsLoaded = true;
			}
			else
			{
				RESOURCE_LOG("ERROR! [%s] Cannot create texture to slot [%i]", aTexture, aSlot);
			}
		}
		else
		{
			RESOURCE_LOG("ERROR! [%s] Cannot add texture to slot [%i]", aTexture, aSlot);
		}
	}

	const ERenderMode CMaterial::GetRenderMode() const
	{
		return myRenderMode;
	}
	const bool CMaterial::IsLoading() const
	{
		return (myLoadingState == ELoadingState::Loading);
	}
	const bool CMaterial::IsLoaded() const
	{
		return (myLoadingState == ELoadingState::Loaded);
	}
}}
