#pragma once
#include "../ModelInstance.h"
#include "../../FBXLoader/FBXLoader.h"
#include "../../Sprite/SpriteFactory.h"
#include <queue>

#define MAX_MATERIAL_COUNT 500

namespace sce
{
	class CFileWatcherWrapper;
};

namespace sce { namespace gfx {

	class CScene;
	class CModel;
	class CMaterial;
	class CModelFactory
	{
		friend class CRenderer2D;
		friend class CSpriteRenderer3D;
		friend class CParticleFactory;
	public:
		~CModelFactory();

		static void Create(sce::CFileWatcherWrapper& aFileWatcher);
		static void Destroy();
		static CModelFactory* Get() { return ourInstance; };

		CModelInstance CreateModel(const char* aFilePath);
		void CreateModelExisting(const std::string aFilePath, CModelInstance& aInstance, const std::string aMaterialFilePath);
		void CreateCustomSphere(CModelInstance& aInstance, const CU::Vector3f& aColor, const float aRadius, const unsigned int aRingCount, const unsigned int aPointPerRingCount);
		CModelInstance CreatePrimitiveTriangle();
		CModelInstance CreateCube();
		CModelInstance CreateTexturedCube(const wchar_t* aTexturePath);
		CModelInstance CreateSkyboxCube(const char* aSkyboxPath = "Data/Models/Misc/Material_Preview_Skybox.dds");
		unsigned int CreateSprite(const char* aSpritePath, CU::Vector2f& aTextureSizeRef);
		unsigned int Create3DSprite(const char* aSpritePath, CU::Vector2f& aTextureSizeRef);
		CMaterial& GetMaterialWithID(const unsigned int aID);
		void RemoveMaterialRefCount(const unsigned int aID);
		void AddMaterialRefCount(const unsigned int aID);

	private:
		static bool ModelChangedCallback(void* aThis, const char* aModelChanged);
		static bool MaterialChangedCallback(void* aThis, const char* aMaterialChanged);

		CModelFactory(sce::CFileWatcherWrapper& aFileWatcher);
		bool LoadModel(const char* aFilePath, const char* aMaterialFilePath, CModelInstance* aNewInstance);
		bool ReloadModel(const char* aFilePath);
		void UseDefaultTexture(CModel& aModel);
		void UseDefaultMaterialID(CModelInstance* aNewInstance);
		void UseDefaultMaterialIDImpl(unsigned int& aMaterialID);
		void UseDefaultParticleMaterialIDImpl(unsigned int& aMaterialID);

		bool MaterialExists(const std::string& aMaterialFilePath) const;
		bool MaterialExistsImpl(const std::string& aMaterialFilePath) const;
		bool MaterialExistsCreateIfNot(const std::string& aMaterialFilePath, unsigned int& aMaterialID, const bool aIsParticleMaterial = false);
		bool UnloadMaterial(const unsigned int aID);
		bool UnloadMaterialImpl(const unsigned int aID);
		unsigned int GetMaterialID(const std::string& aMaterialFilePath);
		unsigned int GetMaterialIDImpl(const std::string& aMaterialFilePath);
		CMaterial& GetMaterialWithIDImpl(const unsigned int aID);
		const CMaterial& GetMaterialWithID(const unsigned int aID) const;
		const CMaterial& GetMaterialWithIDImpl(const unsigned int aID) const;
		void RemoveMaterialRefCountImpl(const unsigned int aID);
		void AddMaterialRefCountImpl(const unsigned int aID);

	private:
		static CModelFactory* ourInstance;

		CSpriteFactory mySpriteFactory;
		CFBXLoader myFBXLoader;
		std::unordered_map<std::string, unsigned int> myAssignedMaterialDs;
		CU::GrowingArray<unsigned int, unsigned int> myMaterialRefCounts;
		std::queue<unsigned int> myFreedIDs;
		unsigned int myCurrentlyFreeMaterialID;
		CMaterial* myMaterials;
		sce::CFileWatcherWrapper& myFileWatcher;

	};
}}
