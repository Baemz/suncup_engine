#pragma once

namespace sce { namespace gfx {

	class CDX11VertexBuffer;
	class CDX11Shader;
	class CDX11IndexBuffer;
	class CDX11Texture;

	class CModel
	{
		friend class CModelFactory;
		friend class CResourceManager;
		friend class CForwardRenderer;
		friend class CDeferredRenderer;
		friend class CDeferredRendererGI;
		friend class CSkyboxRenderer;
		friend class CDebugRenderer;
	public:
		~CModel();

	private:
		CModel();
		unsigned int myTextureID[6];
		float myMaxRadius;
		unsigned int myVertexShaderID;
		unsigned int myPixelShaderID;
		unsigned int myVertexBufferID;
		unsigned int myIndexBufferID;
		CU::Vector3f myCenterPos;

		unsigned int myAlternateTexture0;
		unsigned int myAlternateTexture1;
		volatile bool myIsLoaded;
	};

}}