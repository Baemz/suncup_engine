#pragma once
#ifndef AV_SCENEANIMATOR_H
#define AV_SCENEANIMATOR_H
#include "../../FBXLoader/assimp/cimport.h"
#include "../../FBXLoader/assimp/scene.h"
#include "../../FBXLoader/assimp/postprocess.h"
#include "../CommonUtilities/Matrix.h"
#include "../CommonUtilities/Quaternion.h"

#include <map>
#include <vector>
#include <fstream>
#include <tuple>
#include <string>
#include <functional>
#include <shared_mutex>

namespace sce
{
	class CFBXLoader;
}

class cBone
{
public:
	std::string Name;
	CU::Matrix44f Offset, LocalTransform, GlobalTransform, OriginalLocalTransform;
	cBone* Parent;
	CU::GrowingArray<cBone*, std::size_t> Children;

	cBone() :Parent(nullptr) {}
	~cBone()
	{
		for (auto& child : Children)
		{
			sce_delete(child);
		}
	}
};
class cAnimationChannel
{
public:
	std::string Name;
	CU::GrowingArray<aiVectorKey, std::size_t> mPositionKeys;
	CU::GrowingArray<aiQuatKey, std::size_t> mRotationKeys;
	CU::GrowingArray<aiVectorKey, std::size_t> mScalingKeys;
};

class cAnimEvaluator
{
public:

	cAnimEvaluator() : mLastTime(0.0f), TicksPerSecond(0.0f), Duration(0.0f), PlayAnimationForward(true) {}
	cAnimEvaluator(const aiAnimation* pAnim, const char* aAnimName = nullptr);
	void Evaluate(float pTime, std::map<std::string, cBone*>& bones);
	unsigned int GetFrameIndexAt(float time);
	CU::GrowingArray<CU::Matrix44f, std::size_t>& GetTransforms(float dt, std::function<void(const unsigned short, const std::string&)> aCallback = nullptr)
	{ 
		volatile unsigned int frame(GetFrameIndexAt(dt));

		if (aCallback != nullptr)
		{
			if (myLastFrame != frame)
			{
				if (frame == 0)
				{
					// Send start-frame message.
					aCallback(0, Name);
				}
				else if (frame == Transforms.GetLastIndex() - 1)
				{
					// Send end-frame message.
					aCallback(2, Name);
				}
				else if (frame == static_cast<unsigned int>(Transforms.size() / 2.f))
				{
					// send mid-frame message.
					aCallback(1, Name);
				}
			}
		}

		myLastFrame = frame;
		return Transforms[frame];
	}

	std::string Name;
	CU::GrowingArray<cAnimationChannel, std::size_t> Channels;
	bool PlayAnimationForward;// play forward == true, play backward == false
	float mLastTime, TicksPerSecond, Duration;
	CU::GrowingArray<std::tuple<unsigned int, unsigned int, unsigned int>, std::size_t> mLastPositions;
	CU::GrowingArray<CU::GrowingArray<CU::Matrix44f, std::size_t>, std::size_t> Transforms;//, QuatTransforms;/** Array to return transformations results inside. */
	unsigned int myLastFrame;
};


class SceneAnimator
{
public:

	SceneAnimator() : Skeleton(0), CurrentAnimIndex(-1), myIsInited(false) {}
	~SceneAnimator() { Release(); }

	void Init(const aiScene* pScene, const std::string& aFileName, sce::CFBXLoader* aFBXLoader);// this must be called to fill the SceneAnimator with valid data
	const bool IsInitialized() const;
	void Release();// frees all memory and initializes everything to a default state
	bool HasSkeleton() const { return !Bones.empty(); }// lets the caller know if there is a skeleton present
	// the set animation returns whether the animation changed or is still the same. 
	bool SetAnimIndex(int32_t pAnimIndex);// this takes an index to set the current animation to
	bool SetAnimation(const std::string& name);// this takes a string to set the animation to, i.e. SetAnimation("Idle");
	// the next two functions are good if you want to change the direction of the current animation. You could use a forward walking animation and reverse it to get a walking backwards
	void PlayAnimationForward() { Animations[CurrentAnimIndex].PlayAnimationForward = true; }
	void PlayAnimationBackward() { Animations[CurrentAnimIndex].PlayAnimationForward = false; }
	//this function will adjust the current animations speed by a percentage. So, passing 100, would do nothing, passing 50, would decrease the speed by half, and 150 increase it by 50%
	void AdjustAnimationSpeedBy(float percent) { Animations[CurrentAnimIndex].TicksPerSecond *= percent / 100.0f; }
	//This will set the animation speed
	void AdjustAnimationSpeedTo(float tickspersecond) { Animations[CurrentAnimIndex].TicksPerSecond = tickspersecond; }
	// get the animationspeed... in ticks per second
	float GetAnimationSpeed() const { return Animations[CurrentAnimIndex].TicksPerSecond; }
	// get the transforms needed to pass to the vertex shader. This will wrap the dt value passed, so it is safe to pass 50000000 as a valid number
	const bool IsAnimationsEmpty() const { return Animations.empty(); };

	CU::GrowingArray<CU::Matrix44f, std::size_t>& GetTransforms(float dt, std::function<void(const unsigned short, const std::string&)> aCallback)
	{
		return Animations[CurrentAnimIndex].GetTransforms(dt, aCallback); 
	}

	int32_t GetAnimationIndex() const { return CurrentAnimIndex; }
	std::string GetAnimationName() const { return Animations[CurrentAnimIndex].Name; }
	float GetAnimationDuration() const { return Animations[CurrentAnimIndex].Duration; }
	//GetBoneIndex will return the index of the bone given its name. The index can be used to index directly into the vector returned from GetTransform
	int GetBoneIndex(const std::string& bname) { std::map<std::string, unsigned int>::iterator found = BonesToIndex.find(bname); if (found != BonesToIndex.end()) return found->second; else return -1; }
	//GetBoneTransform will return the matrix of the bone given its name and the time. be careful with this to make sure and send the correct dt. If the dt is different from what the model is currently at, the transform will be off
	CU::Matrix44f GetBoneTransform(float dt, const std::string& bname) { int bindex = GetBoneIndex(bname); if (bindex == -1) return CU::Matrix44f(); return Animations[CurrentAnimIndex].GetTransforms(dt)[bindex]; }
	// same as above, except takes the index
	CU::Matrix44f GetBoneTransform(float dt, unsigned int bindex) { return Animations[CurrentAnimIndex].GetTransforms(dt)[bindex]; }

	CU::GrowingArray<cAnimEvaluator, std::size_t> Animations;// a CU::GrowingArray that holds each animation 
	int32_t CurrentAnimIndex;/** Current animation index */

protected:

	std::map<std::string, cBone*> BonesByName;/** Name to node map to quickly find nodes by their name */
	std::map<std::string, unsigned int> BonesToIndex;/** Name to node map to quickly find nodes by their name */
	std::map<std::string, uint32_t> AnimationNameToId;// find animations quickly
	CU::GrowingArray<CU::Matrix44f, std::size_t> Transforms;// temp array of transfrorms
	CU::GrowingArray<cBone*, std::size_t> Bones;// DO NOT DELETE THESE when the destructor runs... THEY ARE JUST COPIES!!
	cBone* Skeleton;/** Root node of the internal scene structure */
	bool myIsInited;

	void UpdateTransforms(cBone* pNode);
	void CalculateBoneToWorldTransform(cBone* pInternalNode);/** Calculates the global transformation matrix for the given internal node */

	void Calculate(float pTime);
	void CalcBoneMatrices();

	void ExtractAnimations(const aiScene* pScene, const std::string& aAnimationName);
	cBone* CreateBoneTree(aiNode* pNode, cBone* pParent);// Recursively creates an internal node structure matching the current scene and animation. 


private:
	static std::shared_mutex myFileMutex;
	static std::unordered_map<std::string, CU::GrowingArray<std::string>> myFileMap;

};

#endif