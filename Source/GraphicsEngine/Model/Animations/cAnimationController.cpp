#include "stdafx.h"
#include "cAnimationController.h"
#include "..\EngineCore\FileFinder\FileFinder.h"
#include "FBXLoader\FBXLoader.h"
#include <shared_mutex>

std::shared_mutex SceneAnimator::myFileMutex;
std::unordered_map<std::string, CU::GrowingArray<std::string>> SceneAnimator::myFileMap;

void TransformMatrix(CU::Matrix44f& out, const aiMatrix4x4& in) {// there is some type of alignment issue with my mat4 and the aimatrix4x4 class, so the copy must be manually
	out.m11 = in.a1;
	out.m12 = in.a2;
	out.m13 = in.a3;
	out.m14 = in.a4;

	out.m21 = in.b1;
	out.m22 = in.b2;
	out.m23 = in.b3;
	out.m24 = in.b4;

	out.m31 = in.c1;
	out.m32 = in.c2;
	out.m33 = in.c3;
	out.m34 = in.c4;

	out.m41 = in.d1;
	out.m42 = in.d2;
	out.m43 = in.d3;
	out.m44 = in.d4;
}
// ------------------------------------------------------------------------------------------------
// Constructor on a given animation. 
cAnimEvaluator::cAnimEvaluator(const aiAnimation* pAnim, const char* aAnimName) {
	mLastTime = 0.0;
	TicksPerSecond = static_cast<float>(pAnim->mTicksPerSecond != 0.0f ? pAnim->mTicksPerSecond : 100.0f);
	Duration = static_cast<float>(pAnim->mDuration);
	if (aAnimName != nullptr)
	{
		Name = aAnimName;
	}
	else
	{
		Name = pAnim->mName.data;
	}
	PlayAnimationForward = true;
	//OUTPUT_DEBUG_MSG("Creating Animation named: "<<Name);
	Channels.Resize(pAnim->mNumChannels);
	for (unsigned int a = 0; a < pAnim->mNumChannels; a++) {
		Channels[a].Name = pAnim->mChannels[a]->mNodeName.data;
		for (unsigned int i(0); i < pAnim->mChannels[a]->mNumPositionKeys; i++) Channels[a].mPositionKeys.Add(pAnim->mChannels[a]->mPositionKeys[i]);
		for (unsigned int i(0); i < pAnim->mChannels[a]->mNumRotationKeys; i++) Channels[a].mRotationKeys.Add(pAnim->mChannels[a]->mRotationKeys[i]);
		for (unsigned int i(0); i < pAnim->mChannels[a]->mNumScalingKeys; i++) Channels[a].mScalingKeys.Add(pAnim->mChannels[a]->mScalingKeys[i]);
	}
	mLastPositions.Resize(pAnim->mNumChannels, std::make_tuple(0u, 0u, 0u));
	//OUTPUT_DEBUG_MSG("Finished Creating Animation named: "<<Name);
}

unsigned int cAnimEvaluator::GetFrameIndexAt(float ptime) {
	// get a [0.f ... 1.f) value by allowing the percent to wrap around 1
	ptime *= TicksPerSecond;

	float time = 0.0f;

	if (Duration > 0.0)
		time = fmodf(ptime, Duration);

	float percent = time / Duration;
	if (!PlayAnimationForward) percent = (percent - 1.0f) * -1.0f;// this will invert the percent so the animation plays backwards
	unsigned int returnVal = static_cast<unsigned int>((static_cast<float>(Transforms.size()) * percent));
	CU::Clamp<unsigned int>(returnVal, static_cast<unsigned int>(0), static_cast<unsigned int>(Transforms.GetLastIndex()));
	return returnVal;
}

// ------------------------------------------------------------------------------------------------
// Evaluates the animation tracks for a given time stamp. 
void cAnimEvaluator::Evaluate(float pTime, std::map<std::string, cBone*>& bones)
{

	pTime *= TicksPerSecond;

	float time = 0.0f;
	if (Duration > 0.0)
		time = fmod(pTime, Duration);

	// calculate the transformations for each animation channel
	for (unsigned int a = 0; a < Channels.size(); a++) {
		const cAnimationChannel* channel = &Channels[a];
		std::map<std::string, cBone*>::iterator bonenode = bones.find(channel->Name);

		if (bonenode == bones.end()) {
			//OUTPUT_DEBUG_MSG("did not find the bone node "<<channel->Name);
			continue;
		}

		// ******** Position *****
		aiVector3D presentPosition(0, 0, 0);
		if (channel->mPositionKeys.size() > 0) {
			// Look for present frame number. Search from last position if time is after the last time, else from beginning
			// Should be much quicker than always looking from start for the average use case.
			unsigned int frame = (time >= mLastTime) ? std::get<0>(mLastPositions[a]) : 0;
			while (frame < channel->mPositionKeys.size() - 1) {
				if (time < channel->mPositionKeys[frame + 1].mTime) {
					break;
				}
				frame++;
			}

			// interpolate between this frame's value and next frame's value
			unsigned int nextFrame = (frame + 1) % channel->mPositionKeys.size();

			const aiVectorKey& key = channel->mPositionKeys[frame];
			const aiVectorKey& nextKey = channel->mPositionKeys[nextFrame];
			double diffTime = nextKey.mTime - key.mTime;
			if (diffTime < 0.0)
				diffTime += Duration;
			if (diffTime > 0) {
				float factor = float((time - key.mTime) / diffTime);
				presentPosition = key.mValue + (nextKey.mValue - key.mValue) * factor;
			}
			else {
				presentPosition = key.mValue;
			}
			std::get<0>(mLastPositions[a]) = frame;
		}
		// ******** Rotation *********
		aiQuaternion presentRotation(1, 0, 0, 0);
		if (channel->mRotationKeys.size() > 0)
		{
			unsigned int frame = (time >= mLastTime) ? std::get<1>(mLastPositions[a]) : 0;
			while (frame < channel->mRotationKeys.size() - 1) {
				if (time < channel->mRotationKeys[frame + 1].mTime)
					break;
				frame++;
			}

			// interpolate between this frame's value and next frame's value
			unsigned int nextFrame = (frame + 1) % channel->mRotationKeys.size();

			const aiQuatKey& key = channel->mRotationKeys[frame];
			const aiQuatKey& nextKey = channel->mRotationKeys[nextFrame];
			double diffTime = nextKey.mTime - key.mTime;
			if (diffTime < 0.0) diffTime += Duration;
			if (diffTime > 0) {
				float factor = float((time - key.mTime) / diffTime);
				aiQuaternion::Interpolate(presentRotation, key.mValue, nextKey.mValue, factor);
			}
			else presentRotation = key.mValue;
			std::get<1>(mLastPositions[a]) = frame;
		}

		// ******** Scaling **********
		aiVector3D presentScaling(1, 1, 1);
		if (channel->mScalingKeys.size() > 0) {
			unsigned int frame = (time >= mLastTime) ? std::get<2>(mLastPositions[a]) : 0;
			while (frame < channel->mScalingKeys.size() - 1) {
				if (time < channel->mScalingKeys[frame + 1].mTime)
					break;
				frame++;
			}

			presentScaling = channel->mScalingKeys[frame].mValue;
			std::get<2>(mLastPositions[a]) = frame;
		}

		aiMatrix4x4 mat = aiMatrix4x4(presentRotation.GetMatrix());

		mat.a1 *= presentScaling.x; mat.b1 *= presentScaling.x; mat.c1 *= presentScaling.x;
		mat.a2 *= presentScaling.y; mat.b2 *= presentScaling.y; mat.c2 *= presentScaling.y;
		mat.a3 *= presentScaling.z; mat.b3 *= presentScaling.z; mat.c3 *= presentScaling.z;
		mat.a4 = presentPosition.x; mat.b4 = presentPosition.y; mat.c4 = presentPosition.z;
		mat.Transpose();

		TransformMatrix(bonenode->second->LocalTransform, mat);
	}
	mLastTime = time;
}

void SceneAnimator::Release() {// this should clean everything up 
	CurrentAnimIndex = -1;
	Animations.clear();// clear all animations
	sce_delete(Skeleton);// This node will delete all children recursively, also zero it out
}

// this will build the skeleton based on the scene passed to it and CLEAR EVERYTHING
void SceneAnimator::Init(const aiScene* pScene, const std::string& aFileName, sce::CFBXLoader* aFBXLoader)
{
	START_TIMER(loadTimer);
	if (aFBXLoader == nullptr || pScene == nullptr)
	{
		return;
	}

	std::string fbxName = aFileName.substr(0, aFileName.find_first_of('.'));
	Release();

	CU::GrowingArray<std::string> animFiles;

	{
		std::shared_lock<std::shared_mutex> fileSharedLock(myFileMutex);
		auto fileIterator(myFileMap.find(fbxName));
		if (fileIterator != myFileMap.end())
		{
			animFiles.Add(fileIterator->second);
			fileSharedLock.unlock();
		}
		else
		{
			fileSharedLock.unlock();

			std::unique_lock<std::shared_mutex> fileUniqueLock(myFileMutex);
			fileIterator = myFileMap.find(fbxName);
			if (fileIterator != myFileMap.end())
			{
				animFiles.Add(fileIterator->second);
			}
			else
			{
				sce::CFileFinder::FindFilesThatContainsInFolder(animFiles, fbxName.c_str(), "fbx");
				myFileMap.emplace(fbxName, animFiles);
			}
			fileUniqueLock.unlock();
		}
	}

	if (animFiles.Empty())
	{
		Skeleton = CreateBoneTree(pScene->mRootNode, NULL);
		const std::string nullString;
		ExtractAnimations(pScene, nullString);
	}
	else
	{
		bool hasAnim = false;
		bool first = true;
		for (auto& file : animFiles)
		{
			const std::string tempName = file.substr(file.find_first_of('@') + 1);
			const std::string animName = tempName.substr(0, tempName.find_first_of('.'));
			const aiScene* scene = aFBXLoader->GetAIScene(file.c_str());
			if (scene == nullptr)
			{
				continue;
			}
			if (!scene->HasAnimations())
			{
				continue;
			}

			hasAnim = true;

			if (first)
			{
				Skeleton = CreateBoneTree(pScene->mRootNode, NULL);
				first = false;
			}
			ExtractAnimations(scene, animName);
		}
		if (hasAnim == false)
		{
			return;
		}
	}



	for (unsigned int i = 0; i < pScene->mNumMeshes; ++i)
	{
		const aiMesh* mesh = pScene->mMeshes[i];

		for (unsigned int n = 0; n < mesh->mNumBones; ++n)
		{
			const aiBone* bone = mesh->mBones[n];
			std::map<std::string, cBone*>::iterator found = BonesByName.find(bone->mName.data);
			if (found != BonesByName.end())
			{// FOUND IT!!! woohoo, make sure its not already in the bone list
				const std::string bname(bone->mName.data);
				auto foundIterator(std::find_if(Bones.begin(), Bones.end()
					, [&bname](const cBone* const& aBone) -> bool
				{
					return (aBone->Name == bname);
				}));

				if (foundIterator == Bones.end())
				{// only insert the bone if it has not already been inserted
					std::string tes = found->second->Name;
					TransformMatrix(found->second->Offset, bone->mOffsetMatrix);
					found->second->Offset = CU::Matrix44f::Transpose(found->second->Offset);// transpose their matrix to get in the correct format
					Bones.Add(found->second);
					BonesToIndex[found->first] = (unsigned int)Bones.size() - 1;
				}
			}
		}
	}
	Transforms.Resize(Bones.size());
	float timestep = 1.0f / 30.0f;// 30 per second
	for (size_t i(0); i < Animations.size(); i++)
	{// pre calculate the animations
		SetAnimIndex((int32_t)i);
		float dt = 0;
		for (float ticks = 0; ticks < Animations[i].Duration; ticks += Animations[i].TicksPerSecond / 30.0f)
		{
			dt += timestep;
			Calculate(dt);
			Animations[i].Transforms.Add(CU::GrowingArray<CU::Matrix44f, std::size_t>());
			CU::GrowingArray<CU::Matrix44f, std::size_t>& trans = Animations[i].Transforms.back();
			for (size_t a = 0; a < Transforms.size(); ++a)
			{
				CU::Matrix44f rotationmat = Bones[a]->Offset * Bones[a]->GlobalTransform;
				trans.Add(rotationmat);
			}
		}
	}

	myIsInited = true;
	int time = 0;
	END_TIMER(loadTimer, time);
	RESOURCE_LOG("[MODEL_ANIMATION] (%s) took %i ms to load.", fbxName.c_str(), time);
	SetAnimation("idle");
}

const bool SceneAnimator::IsInitialized() const
{
	return myIsInited;
}

void SceneAnimator::ExtractAnimations(const aiScene* pScene, const std::string& aAnimationName)
{
	//OUTPUT_DEBUG_MSG("Extracting Animations . . ");
	for (size_t i(0); i < pScene->mNumAnimations; i++) {
		if (aAnimationName.empty())
		{
			Animations.Add(cAnimEvaluator(pScene->mAnimations[i]));// add the animations
		}
		else
		{
			Animations.Add(cAnimEvaluator(pScene->mAnimations[i], aAnimationName.c_str()));// add the animations
			break;
		}
	}
	for (uint32_t i(0); i < Animations.size(); i++) {// get all the animation names so I can reference them by name and get the correct id
		AnimationNameToId.insert(std::map<std::string, uint32_t>::value_type(Animations[i].Name, i));
	}
	CurrentAnimIndex = 0;
	SetAnimation("idle");
}
bool SceneAnimator::SetAnimation(const std::string& name)
{
	std::map<std::string, uint32_t>::iterator itr = AnimationNameToId.find(name);
	//int32_t oldindex = CurrentAnimIndex;
	if (itr == AnimationNameToId.end()) /*CurrentAnimIndex = itr->second;*/
	{
		return false;
	}
	
	CurrentAnimIndex = itr->second;
	return true/*oldindex != CurrentAnimIndex*/;
}
bool SceneAnimator::SetAnimIndex(int32_t  pAnimIndex)
{
	if (pAnimIndex >= static_cast<int32_t>(Animations.size())) return false;// no change, or the animations data is out of bounds
	CurrentAnimIndex = pAnimIndex;// only set this after the checks for good data and the object was actually inserted
	return true;
}

// ------------------------------------------------------------------------------------------------
// Calculates the node transformations for the scene. 
void SceneAnimator::Calculate(float pTime)
{
	if ((CurrentAnimIndex < 0) | (CurrentAnimIndex >= static_cast<int32_t>(Animations.size()))) return;// invalid animation

	Animations[CurrentAnimIndex].Evaluate(pTime, BonesByName);
	UpdateTransforms(Skeleton);
}

void UQTtoUDQ(CU::Vector4f* dual, CU::Quaternion q, CU::Vector4f& tran)
{
	dual[0].x = q.x;
	dual[0].y = q.y;
	dual[0].z = q.z;
	dual[0].w = q.w;
	dual[1].x = 0.5f *  (tran[0] * q.w + tran[1] * q.z - tran[2] * q.y);
	dual[1].y = 0.5f *  (-tran[0] * q.z + tran[1] * q.w + tran[2] * q.x);
	dual[1].z = 0.5f *  (tran[0] * q.y - tran[1] * q.x + tran[2] * q.w);
	dual[1].w = -0.5f * (tran[0] * q.x + tran[1] * q.y + tran[2] * q.z);
}

// ------------------------------------------------------------------------------------------------
// Calculates the bone matrices for the given mesh. 
void SceneAnimator::CalcBoneMatrices()
{
	for (size_t a = 0; a < Transforms.size(); ++a)
	{
		Transforms[a] = Bones[a]->Offset * Bones[a]->GlobalTransform;
	}
}

// ------------------------------------------------------------------------------------------------
// Recursively creates an internal node structure matching the current scene and animation.
cBone* SceneAnimator::CreateBoneTree(aiNode* pNode, cBone* pParent)
{
	cBone* internalNode(sce_new(cBone));// create a node
	internalNode->Name = pNode->mName.data;// get the name of the bone
	internalNode->Parent = pParent; //set the parent, in the case this is theroot node, it will be null

	BonesByName[internalNode->Name] = internalNode;// use the name as a key
	TransformMatrix(internalNode->LocalTransform, pNode->mTransformation);
	internalNode->LocalTransform = CU::Matrix44f::Transpose(internalNode->LocalTransform);
	internalNode->OriginalLocalTransform = internalNode->LocalTransform;// a copy saved
	CalculateBoneToWorldTransform(internalNode);

	// continue for all child nodes and assign the created internal nodes as our children
	for (unsigned int a = 0; a < pNode->mNumChildren; a++)
	{// recursivly call this function on all children
		internalNode->Children.Add(CreateBoneTree(pNode->mChildren[a], internalNode));
	}
	return internalNode;
}

// ------------------------------------------------------------------------------------------------
// Recursively updates the internal node transformations from the given matrix array
void SceneAnimator::UpdateTransforms(cBone* pNode)
{
	CalculateBoneToWorldTransform(pNode);// update global transform as well
	for (auto& it : pNode->Children)// continue for all children
		UpdateTransforms(it);
}

// ------------------------------------------------------------------------------------------------
// Calculates the global transformation matrix for the given internal node
void SceneAnimator::CalculateBoneToWorldTransform(cBone* child)
{
	child->GlobalTransform = child->LocalTransform;
	cBone* parent = child->Parent;
	while (parent)
	{// this will climb the nodes up along through the parents concentating all the matrices to get the Object to World transform, or in this case, the Bone To World transform
		child->GlobalTransform *= parent->LocalTransform;
		parent = parent->Parent;// get the parent of the bone we are working on 
	}
}
