#pragma once
#include "../CommonUtilities/Matrix.h"
#include "../Camera/Frustum/FrustumCollider.h"
#include "../CommonUtilities/GrowingArray.h"
#include <functional>

class SceneAnimator;
namespace sce { namespace gfx {
	
	class CModelInstance
	{
		friend class CModelFactory;
		friend class CForwardRenderer;
		friend class CSkyboxRenderer;
		friend class CScene;
		friend class CDeferredRenderer;
		friend class CDeferredRendererGI;
		friend class CDebugRenderer;
		friend class CGraphicsEngine;
		friend class CDebugTools;

	public:
		CModelInstance();
		CModelInstance(const CModelInstance& aModelInstance);
		~CModelInstance();

		void operator=(const CModelInstance& aModelInstance);

		enum class ELoadingState : char
		{
			Unloaded = 0,
			Loading = 1,
			Loaded = 2,
			FailedLoading = 3,
		};


		void Init();
		void InitCustomSphere(const CU::Vector3f& aColor, const float aRadius, const unsigned int aRingCount, const unsigned int aPointPerRingCount);

		void Move(const CU::Vector3f& aValue);
		void SetScale(const CU::Vector3f& aScale);
		void SetScaleForInit(const CU::Vector3f& aScale);
		void Rotate(float aValue);
		void Rotate(const CU::Vector3f& aRotation);

		void SetPosition(const CU::Vector3f& aPosition);
		void SetOrientation(const CU::Matrix44f& aOrientation);
		void SetModelPath(const std::string& aModelPath);
		void SetMaterialPath(const std::string& aMaterialPath);

		const bool IsAffectedByFog() const { return myAffectedByFog; };
		void SetAffectedByFog(const bool aAffectedByFog);
		const bool IsCullable() const { return myIsCullable; };
		void SetIsCullable(const bool aIsCullable);
		void UpdateColliderPosition();

		const bool IsLoaded() const;
		const bool IsLoading() const;
		const bool LoadingFailed() const;

		const std::string& GetModelPath() const;
		const CU::Vector3f GetPosition() const;
		const CU::Matrix44f GetOrientation() const;
		const float GetZValue() const;
		const unsigned int GetMaterialID() const;
		unsigned int GetModelID() const { return myModelID; };
		inline const float GetRadius() const { return myRadius; }

		const bool IsCastingShadows() const;
		void SetIsCastingShadows(const bool aIsCastingShadows);

		// Animation functions
		void SetAnimationWithIndex(const unsigned int aIndex);
		void SetAnimationWithTag(const std::string& aTag);
		void SetAnimationSpeed(const float aSpeed);
		void PlayAnimation();
		void StopAnimation();
		void PauseAnimation();
		const float GetAnimationDuration() const;
		void SetAnimationEventCallback(std::function<void(const unsigned short, const std::string&)> aCallback);


		void Update(const float aDeltaTime); // Updates animation if model is set to animate.
		void Render();

		const CFrustumCollider& GetFrustumCollider() const;

#ifdef _MATEDIT
		CU::Vector3f centerPos;
#endif
	private:
		CU::GrowingArray<CU::Matrix44f, std::size_t> myBoneTransforms;
		std::string myModelName;
		std::string myModelPath;
		std::string myMaterialPath;
		CU::Matrix44f myOrientation;
		CU::Vector3f myScaleToUseForInit;
		CU::Vector3f myScale;
		CFrustumCollider myFrustumCollider;
		SceneAnimator* myAnimator; 
		float myRadius;
		float myTotalAnimationTime;
		unsigned int myModelID;
		unsigned int myMaterialID;
		std::function<void(const unsigned short, const std::string&)> myAnimEventCallback;

		enum class EPlaybackState
		{
			Play,
			Pause,
		} myPlaybackState;

		volatile ELoadingState myLoadingState;
		bool myAffectedByFog;
		bool myIsCullable;
		bool myIsCastingShadows;


	};
}}