#include "stdafx.h"
#include "ModelInstance.h"
#include "GraphicsEngineInterface.h"
#include "..\EngineCore\WorkerPool\WorkerPool.h"
#include "Loading\ModelFactory.h"
#include "Animations/cAnimationController.h"

using namespace sce::gfx;

CModelInstance::CModelInstance()
	: myModelID(UINT_MAX)
	, myMaterialID(UINT_MAX)
	, myModelName("")
	, myLoadingState(ELoadingState::Unloaded)
	, myScale(1.f, 1.f, 1.f)
	, myRadius(100.0f)
	, myAffectedByFog(true)
	, myMaterialPath("Data/Materials/DEFAULT.material")
	, myIsCullable(false)
	, myTotalAnimationTime(0.f)
	, myAnimator(nullptr)
	, myIsCastingShadows(true)
	, myPlaybackState(EPlaybackState::Pause)
{
}

sce::gfx::CModelInstance::CModelInstance(const CModelInstance& aModelInstance)
{
	myModelName = aModelInstance.myModelName;
	myModelPath = aModelInstance.myModelPath;
	myMaterialPath = aModelInstance.myMaterialPath;
	myOrientation = aModelInstance.myOrientation;
	myScaleToUseForInit = aModelInstance.myScaleToUseForInit;
	myScale = aModelInstance.myScale;
	myFrustumCollider = aModelInstance.myFrustumCollider;
	myModelID = aModelInstance.myModelID;
	myRadius = aModelInstance.myRadius;
	myMaterialID = aModelInstance.myMaterialID;
	myLoadingState = aModelInstance.myLoadingState;
	myAffectedByFog = aModelInstance.myAffectedByFog;
	myIsCullable = aModelInstance.myIsCullable;
	myBoneTransforms = aModelInstance.myBoneTransforms;
	myIsCastingShadows = aModelInstance.myIsCastingShadows;
	myAnimator = nullptr;

	CModelFactory* modelFactory = CModelFactory::Get();
	if (modelFactory && myMaterialID != UINT_MAX)
	{
		modelFactory->AddMaterialRefCount(myMaterialID);
	}
}


CModelInstance::~CModelInstance()
{
	while (IsLoading() == true)
	{
		std::this_thread::yield();
	}

	CModelFactory* modelFactory = CModelFactory::Get();
	if (modelFactory && myMaterialID != UINT_MAX)
	{
		modelFactory->RemoveMaterialRefCount(myMaterialID);
	}
	if (myAnimator != nullptr)
	{
		myAnimator->Release();
		sce_delete(myAnimator);
	}
}

void sce::gfx::CModelInstance::operator=(const CModelInstance & aModelInstance)
{
	CModelFactory* modelFactory(CModelFactory::Get());

	if (modelFactory && aModelInstance.myMaterialID != UINT_MAX)
	{
		modelFactory->AddMaterialRefCount(aModelInstance.myMaterialID);
	}

	if (modelFactory && myMaterialID != UINT_MAX)
	{
		modelFactory->RemoveMaterialRefCount(myMaterialID);
	}

	myModelName = aModelInstance.myModelName;
	myModelPath = aModelInstance.myModelPath;
	myMaterialPath = aModelInstance.myMaterialPath;
	myOrientation = aModelInstance.myOrientation;
	myScaleToUseForInit = aModelInstance.myScaleToUseForInit;
	myScale = aModelInstance.myScale;
	myRadius = aModelInstance.myRadius;
	myFrustumCollider = aModelInstance.myFrustumCollider;
	myModelID = aModelInstance.myModelID;
	myMaterialID = aModelInstance.myMaterialID;
	myLoadingState = aModelInstance.myLoadingState;
	myAffectedByFog = aModelInstance.myAffectedByFog;
	myIsCullable = aModelInstance.myIsCullable;
	myBoneTransforms = aModelInstance.myBoneTransforms;
	myIsCastingShadows = aModelInstance.myIsCastingShadows;
	myAnimator = nullptr;
}

void sce::gfx::CModelInstance::Init()
{
	CModelFactory* modelFactory = CModelFactory::Get();
	if (modelFactory)
	{
		if (myLoadingState == ELoadingState::Unloaded && myModelPath != "")
		{
			myLoadingState = ELoadingState::Loading;
 			WORKER_POOL_QUEUE_WORK(&CModelFactory::CreateModelExisting, CModelFactory::Get(), myModelPath, std::ref<CModelInstance>(*this), myMaterialPath);
		}
	}
	else
	{
		ENGINE_LOG("ERROR! No ModelFactory created.");
	}
	
}

void sce::gfx::CModelInstance::InitCustomSphere(const CU::Vector3f& aColor, const float aRadius, const unsigned int aRingCount, const unsigned int aPointPerRingCount)
{
	if (CModelFactory::Get() != nullptr)
	{
		if (myLoadingState == ELoadingState::Unloaded)
		{
			myLoadingState = ELoadingState::Loading;
			WORKER_POOL_QUEUE_WORK(&CModelFactory::CreateCustomSphere, CModelFactory::Get(), std::ref<CModelInstance>(*this), aColor, aRadius, aRingCount, aPointPerRingCount);
		}
		else
		{
			ENGINE_LOG("[CModelInstance] WARNING! Tried to load a custom sphere on a model that is already loaded or is currently loading its model. The custom sphere will not be loaded.");
		}
	}
	else
	{
		ENGINE_LOG("ERROR! No ModelFactory created.");
	}
}

void sce::gfx::CModelInstance::Move(const CU::Vector3f & aValue)
{
	myOrientation.SetPosition(myOrientation.GetPosition() + aValue);
	myFrustumCollider.SetPosition(myOrientation);
}

void sce::gfx::CModelInstance::SetScale(const CU::Vector3f& aScale)
{
	myScale = aScale;
}

void sce::gfx::CModelInstance::SetScaleForInit(const CU::Vector3f & aScale)
{
	myScaleToUseForInit = aScale;
}

void sce::gfx::CModelInstance::Rotate(float aValue)
{
	CU::Vector3f aPos = myOrientation.GetPosition();
	myOrientation *= myOrientation.CreateRotateAroundY(aValue);
	myOrientation.SetPosition(aPos);
}

void sce::gfx::CModelInstance::Rotate(const CU::Vector3f& aRotation)
{
	CU::Vector3f aPrevPos = myOrientation.GetPosition();
	myOrientation *= myOrientation.CreateRotateAroundX(aRotation.y);
	myOrientation *= myOrientation.CreateRotateAroundY(aRotation.x);
	myOrientation *= myOrientation.CreateRotateAroundZ(aRotation.z);
	myOrientation.SetPosition(aPrevPos);
}

void sce::gfx::CModelInstance::SetPosition(const CU::Vector3f& aPosition)
{
	myOrientation.SetPosition(aPosition);
	myFrustumCollider.SetPosition(myOrientation);
}

const CU::Matrix44f sce::gfx::CModelInstance::GetOrientation() const
{
	return myOrientation;
}

void sce::gfx::CModelInstance::SetOrientation(const CU::Matrix44f & aOrientation)
{
	myOrientation = aOrientation;
	myFrustumCollider.SetPosition(myOrientation);
}

void sce::gfx::CModelInstance::SetModelPath(const std::string& aModelPath)
{
	myModelPath = aModelPath;
}

void sce::gfx::CModelInstance::SetMaterialPath(const std::string & aMaterialPath)
{
	myMaterialPath = aMaterialPath;
}

void sce::gfx::CModelInstance::SetAffectedByFog(const bool aAffectedByFog)
{
	myAffectedByFog = aAffectedByFog;
}
void sce::gfx::CModelInstance::SetIsCullable(const bool aIsCullable)
{
	myIsCullable = aIsCullable;
}

void sce::gfx::CModelInstance::UpdateColliderPosition()
{
	myFrustumCollider.SetPosition(myOrientation);
}

const bool sce::gfx::CModelInstance::IsLoaded() const
{
	if (myLoadingState == ELoadingState::Loaded)
	{
		return true;
	}
	return false;
}

const bool sce::gfx::CModelInstance::IsLoading() const
{
	return (myLoadingState == ELoadingState::Loading);
}

const bool sce::gfx::CModelInstance::LoadingFailed() const
{
	return (myLoadingState == ELoadingState::FailedLoading);
}

const std::string& sce::gfx::CModelInstance::GetModelPath() const
{
	return myModelPath;
}

const CU::Vector3f sce::gfx::CModelInstance::GetPosition() const
{
	return myOrientation.GetPosition();
}

const float sce::gfx::CModelInstance::GetZValue() const
{
	return myOrientation[14];
}

const unsigned int sce::gfx::CModelInstance::GetMaterialID() const
{
	return myMaterialID;
}

const bool sce::gfx::CModelInstance::IsCastingShadows() const
{
	return myIsCastingShadows;
}

void sce::gfx::CModelInstance::SetIsCastingShadows(const bool aIsCastingShadows)
{
	myIsCastingShadows = aIsCastingShadows;
}

void sce::gfx::CModelInstance::SetAnimationWithIndex(const unsigned int aIndex)
{
	if (myAnimator != nullptr)
	{
		if (myAnimator->SetAnimIndex(aIndex) == false)
		{
			GAME_LOG("WARNING! \"%s\" trying to set invalid animation index (%u)", myModelName.c_str(), aIndex);
		}
		myTotalAnimationTime = 0.f;
	}
}

void sce::gfx::CModelInstance::SetAnimationWithTag(const std::string & aTag)
{
	if (myAnimator != nullptr)
	{
		if (myAnimator->SetAnimation(aTag) == false)
		{
			GAME_LOG("WARNING! \"%s\" trying to set invalid animation tag (%s)", myModelName.c_str(), aTag.c_str());
		}
		myTotalAnimationTime = 0.f;
	}
}

void sce::gfx::CModelInstance::SetAnimationSpeed(const float aSpeed)
{
	if (myAnimator != nullptr)
	{
		myAnimator->AdjustAnimationSpeedTo(aSpeed);
	}
}

void sce::gfx::CModelInstance::PlayAnimation()
{
	myPlaybackState = EPlaybackState::Play;
}

void sce::gfx::CModelInstance::StopAnimation()
{
	myPlaybackState = EPlaybackState::Pause;
	myTotalAnimationTime = 0.f;
}

void sce::gfx::CModelInstance::PauseAnimation()
{
	myPlaybackState = EPlaybackState::Pause;
}

const float sce::gfx::CModelInstance::GetAnimationDuration() const
{
	if (myAnimator != nullptr)
	{
		return myAnimator->GetAnimationDuration();
	}
	else
	{
		return 0.f;
	}
}

void sce::gfx::CModelInstance::SetAnimationEventCallback(std::function<void(const unsigned short, const std::string&)> aCallback)
{
	myAnimEventCallback = aCallback;
}

void sce::gfx::CModelInstance::Update(const float aDeltaTime)
{
	if (myLoadingState == ELoadingState::Loaded && myModelID != UINT_MAX)
	{
		if (myAnimator != nullptr)
		{
			if (myAnimator->IsAnimationsEmpty() == false)
			{
				if (myPlaybackState == EPlaybackState::Play)
				{
					myTotalAnimationTime += aDeltaTime;
					myBoneTransforms = myAnimator->GetTransforms(myTotalAnimationTime, myAnimEventCallback);
				}
			}
		}
	}
}

void sce::gfx::CModelInstance::Render()
{
	if (myLoadingState == ELoadingState::Loaded && myModelID != UINT_MAX)
	{
		CGraphicsEngineInterface::AddToScene(*this);
	}
}

const CFrustumCollider & sce::gfx::CModelInstance::GetFrustumCollider() const
{
	return myFrustumCollider;
}
