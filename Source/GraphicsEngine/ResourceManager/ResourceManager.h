#pragma once
#include <map>

#define MAX_MODEL_COUNT 300
#define MAX_CAMERA_COUNT 10
#define MAX_SHADER_COUNT 300
#define MAX_BUFFER_COUNT 300
#define MAX_TEXTURE_COUNT (MAX_MODEL_COUNT * 6)

namespace sce { namespace gfx {

	class CModel;
	class CCamera;
	class CDX11Shader;
	class CDX11VertexBuffer;
	class CDX11IndexBuffer;
	class CDX11ConstantBuffer;
	class CDX11Texture;

	class CResourceManager
	{
		friend class CModelFactory;
	public:
		~CResourceManager();

		static void Create();
		static void Destroy();
		static CResourceManager* Get();

		bool ModelExists(const char* aModelFilePath) const;
		bool UnloadModel(const char* aModelFilePath);
		unsigned int GetModelID(const char* aModelFilePath);
		const CModel& GetModelWithID(const unsigned int aID) const;
		CModel& GetModelWithID(const unsigned int aID);

		bool CameraExists(const char* aCameraName) const;
		unsigned int GetCameraID(const char* aCameraName);
		const CCamera& GetCameraWithID(const unsigned int aID) const;
		CCamera& GetCameraWithID(const unsigned int aID);

		unsigned int GetVertexBufferID(const unsigned int aModelID);
		bool UnloadVertexBuffer(const unsigned int aModelID);
		const CDX11VertexBuffer& GetVertexBufferWithID(const unsigned int aID) const;
		CDX11VertexBuffer& GetVertexBufferWithID(const unsigned int aID);

		unsigned int GetIndexBufferID(const unsigned int aModelID);
		bool UnloadIndexBuffer(const unsigned int aModelID);
		const CDX11IndexBuffer& GetIndexBufferWithID(const unsigned int aID) const;
		CDX11IndexBuffer& GetIndexBufferWithID(const unsigned int aID);

		unsigned int GetConstantBufferID(const char* aObjectRef);
		const CDX11ConstantBuffer& GetConstantBufferWithID(const unsigned int aID) const;
		CDX11ConstantBuffer& GetConstantBufferWithID(const unsigned int aID);

		bool ShaderExists(const char* aFilePath) const;
		unsigned int GetShaderID(const char* aFilePath);
		const CDX11Shader& GeShaderWithID(const unsigned int aID) const;
		CDX11Shader& GetShaderWithID(const unsigned int aID);

		bool TextureExists(const wchar_t* aFilePath);
		bool TextureExists(const char* aFilePath);
		bool TextureExistsCreateIfNot(const char* aFilePath, unsigned int& aTextureID);
		unsigned int GetTextureID(const wchar_t* aFilePath);
		unsigned int GetTextureID(const char* aFilePath);
		const CDX11Texture& GetTextureWithID(const unsigned int aID) const;
		CDX11Texture& GetTextureWithID(const unsigned int aID);

	private:
		CResourceManager();
		static CResourceManager* ourInstance;

		bool ModelExistsImpl(const char* aModelFilePath) const;
		unsigned int GetModelIDImpl(const char* aModelFilePath, bool aUseLocks);

		bool UnloadVertexBufferImpl(const unsigned int aModelID);
		bool UnloadIndexBufferImpl(const unsigned int aModelID);

		bool TextureExistsImpl(const wchar_t* aFilePath);
		bool TextureExistsImpl(const char* aFilePath);
		unsigned int GetTextureIDImpl(const wchar_t* aFilePath, bool aUseLocks);
		unsigned int GetTextureIDImpl(const char* aFilePath, bool aUseLocks);

		unsigned int myCurrentlyFreeModelID;
		CModel* myModels;
		std::map<std::string, unsigned int> myAssignedModelIDs;

		unsigned int myCurrentlyFreeCameraID;
		CCamera* myCameras;
		std::map<std::string, unsigned int> myAssignedCameraIDs;

		unsigned int myCurrentlyFreeVertexBufferID;
		CDX11VertexBuffer* myVertexBuffers;
		std::map<unsigned int, unsigned int> myAssignedVertexBufferIDs; // Key = the id of the model for that specific buffer.

		unsigned int myCurrentlyFreeIndexBufferID;
		CDX11IndexBuffer* myIndexBuffers;
		std::map<unsigned int, unsigned int> myAssignedIndexBufferIDs; // Key = the id of the model for that specific buffer.

		unsigned int myCurrentlyFreeConstantBufferID;
		CDX11ConstantBuffer* myConstantBuffers;
		std::map<std::string, unsigned int> myAssignedConstantBufferIDs; // Key = the name of the object holding the buffer.

		unsigned int myCurrentlyFreeShaderID;
		CDX11Shader* myShaders;
		std::map<std::string, unsigned int> myAssignedShaderIDs;

		unsigned int myCurrentlyFreeTextureID;
		CDX11Texture* myTextures;
		std::map<std::wstring, unsigned int> myAssignedTextureIDs;

	};
}}
