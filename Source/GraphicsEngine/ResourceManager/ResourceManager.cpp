﻿#include "stdafx.h"
#include "ResourceManager.h"
#include "../Model/Model.h"
#include "../Camera/Camera.h"
#include "../DirectXFramework/API/DX11VertexBuffer.h"
#include "../DirectXFramework/API/DX11IndexBuffer.h"
#include "../DirectXFramework/API/DX11ConstantBuffer.h"
#include "../DirectXFramework/API/DX11Shader.h"
#include "../DirectXFramework/API/DX11Texture2D.h"
#include <shared_mutex>

namespace sce
{
	namespace gfx
	{
		namespace ResourceManager🔒
		{
			std::shared_mutex modelSharedMutex;
			std::shared_mutex cameraSharedMutex;
			std::shared_mutex vertexSharedMutex;
			std::shared_mutex indexSharedMutex;
			std::shared_mutex constantSharedMutex;
			std::shared_mutex shaderSharedMutex;
			std::shared_mutex textureSharedMutex;
		}
	}
}

using namespace sce::gfx;

CResourceManager* CResourceManager::ourInstance = nullptr;

void CResourceManager::Create()
{
	assert(ourInstance == nullptr && "Instance already created.");
	CDX11Texture::CreateDefaultTextures();
	ourInstance = sce_new(CResourceManager());
}

void CResourceManager::Destroy()
{
	CDX11Texture::DestroyDefaultTextures();
	sce_delete(ourInstance);
}

CResourceManager* CResourceManager::Get()
{
	return ourInstance;
}

CResourceManager::CResourceManager()
	: myCurrentlyFreeModelID(0)
	, myModels(sce_newArray(CModel, MAX_MODEL_COUNT))
	, myCurrentlyFreeCameraID(0)
	, myCameras(sce_newArray(CCamera, MAX_CAMERA_COUNT))
	, myCurrentlyFreeVertexBufferID(0)
	, myVertexBuffers(sce_newArray(CDX11VertexBuffer, MAX_BUFFER_COUNT))
	, myCurrentlyFreeIndexBufferID(0)
	, myIndexBuffers(sce_newArray(CDX11IndexBuffer, MAX_BUFFER_COUNT))
	, myCurrentlyFreeConstantBufferID(0)
	, myConstantBuffers(sce_newArray(CDX11ConstantBuffer, MAX_BUFFER_COUNT))
	, myCurrentlyFreeShaderID(0)
	, myShaders(sce_newArray(CDX11Shader, MAX_SHADER_COUNT))
	, myCurrentlyFreeTextureID(0)
	, myTextures(sce_newArray(CDX11Texture, MAX_TEXTURE_COUNT))
{
}

bool sce::gfx::CResourceManager::ModelExistsImpl(const char* aModelFilePath) const
{
	if (myAssignedModelIDs.find(aModelFilePath) != myAssignedModelIDs.end())
	{
		return true;
	}
	return false;
}

unsigned int sce::gfx::CResourceManager::GetModelIDImpl(const char* aModelFilePath, bool aUseLocks)
{
	const std::string modelFilePath(aModelFilePath);

	std::shared_lock<std::shared_mutex> lgShared(ResourceManager🔒::modelSharedMutex, std::defer_lock);
	if (aUseLocks == true)
	{
		lgShared.lock();
	}

	auto foundIterator(myAssignedModelIDs.find(modelFilePath));

	if (foundIterator != myAssignedModelIDs.end())
	{
		return foundIterator->second;
	}
	else
	{
		std::unique_lock<std::shared_mutex> lgUnique(ResourceManager🔒::modelSharedMutex, std::defer_lock);
		if (aUseLocks == true)
		{
			lgShared.unlock();
			lgUnique.lock();

			foundIterator = myAssignedModelIDs.find(modelFilePath);

			if (foundIterator != myAssignedModelIDs.end())
			{
				return foundIterator->second;
			}
		}

		if (myCurrentlyFreeModelID > (MAX_MODEL_COUNT - 1))
		{
			assert(false && "Too many models created");
			RESOURCE_LOG("WARNING! Too many models created.");
			return UINT_MAX;
		}
		myAssignedModelIDs.insert({ modelFilePath, myCurrentlyFreeModelID });
		return myCurrentlyFreeModelID++;
	}
}

bool sce::gfx::CResourceManager::UnloadVertexBufferImpl(const unsigned int aModelID)
{
	const unsigned int bufferID(GetVertexBufferID(aModelID));
	myVertexBuffers[bufferID] = CDX11VertexBuffer();
	myAssignedVertexBufferIDs.erase(aModelID);
	return true;
}

bool sce::gfx::CResourceManager::UnloadIndexBufferImpl(const unsigned int aModelID)
{
	const unsigned int bufferID(GetIndexBufferID(aModelID));
	myIndexBuffers[bufferID] = CDX11IndexBuffer();
	myAssignedIndexBufferIDs.erase(aModelID);
	return true;
}

bool sce::gfx::CResourceManager::TextureExistsImpl(const wchar_t* aFilePath)
{
	if (myAssignedTextureIDs.find(aFilePath) != myAssignedTextureIDs.end())
	{
		return true;
	}
	return false;
}

bool sce::gfx::CResourceManager::TextureExistsImpl(const char* aFilePath)
{
	wchar_t wstring[4096];
	return TextureExistsImpl(ConvertCharArrayToLPCWSTR(aFilePath, (wchar_t*)(&wstring), 4096));
}

unsigned int sce::gfx::CResourceManager::GetTextureIDImpl(const wchar_t* aFilePath, bool aUseLocks)
{
	const std::wstring textureFilePath(aFilePath);
	
	std::shared_lock<std::shared_mutex> lgShared(ResourceManager🔒::textureSharedMutex, std::defer_lock);
	if (aUseLocks == true)
	{
		lgShared.lock();
	}

	auto foundIterator(myAssignedTextureIDs.find(textureFilePath));

	if (foundIterator != myAssignedTextureIDs.end())
	{
		return foundIterator->second;
	}
	else
	{
		std::unique_lock<std::shared_mutex> lgUnique(ResourceManager🔒::textureSharedMutex, std::defer_lock);
		if (aUseLocks == true)
		{
			lgShared.unlock();
			lgUnique.lock();

			foundIterator = myAssignedTextureIDs.find(textureFilePath);

			if (foundIterator != myAssignedTextureIDs.end())
			{
				return foundIterator->second;
			}
		}

		if (myCurrentlyFreeTextureID > (MAX_TEXTURE_COUNT - 1))
		{
			assert(false && "Too many textures created");
			RESOURCE_LOG("WARNING! Too many textures created");
			return UINT_MAX;
		}
		myAssignedTextureIDs.insert({ textureFilePath, myCurrentlyFreeTextureID });
		return myCurrentlyFreeTextureID++;
	}
}

unsigned int sce::gfx::CResourceManager::GetTextureIDImpl(const char* aFilePath, bool aUseLocks)
{
	wchar_t wstring[4096];
	return GetTextureIDImpl(ConvertCharArrayToLPCWSTR(aFilePath, (wchar_t*)(&wstring), 4096), aUseLocks);
}

CResourceManager::~CResourceManager()
{
	sce_delete(myModels);
	sce_delete(myCameras);
	sce_delete(myVertexBuffers);
	sce_delete(myIndexBuffers);
	sce_delete(myConstantBuffers);
	sce_delete(myShaders);
	sce_delete(myTextures);
}

bool CResourceManager::ModelExists(const char* aModelFilePath) const
{
	std::shared_lock<std::shared_mutex> lg(ResourceManager🔒::modelSharedMutex);
	return ModelExistsImpl(aModelFilePath);
}

bool sce::gfx::CResourceManager::UnloadModel(const char* aModelFilePath)
{
	std::unique_lock<std::shared_mutex> lgModel(ResourceManager🔒::modelSharedMutex, std::defer_lock);
	std::unique_lock<std::shared_mutex> lgVertex(ResourceManager🔒::vertexSharedMutex, std::defer_lock);
	std::unique_lock<std::shared_mutex> lgIndex(ResourceManager🔒::indexSharedMutex, std::defer_lock);
	std::lock(lgModel, lgVertex, lgIndex);

	if (ModelExistsImpl(aModelFilePath) == false)
	{
		return true;
	}

	const unsigned int modelID = GetModelIDImpl(aModelFilePath, false);

	UnloadVertexBufferImpl(modelID);
	lgVertex.unlock();
	UnloadIndexBufferImpl(modelID);
	lgIndex.unlock();

	myModels[modelID] = CModel();
	myAssignedModelIDs.erase(aModelFilePath);
	return true;
}

unsigned int CResourceManager::GetModelID(const char* aModelFilePath)
{
	return GetModelIDImpl(aModelFilePath, true);
}

const CModel& CResourceManager::GetModelWithID(const unsigned int aID) const
{
	std::shared_lock<std::shared_mutex> lg(ResourceManager🔒::modelSharedMutex);
	return myModels[aID];
}

CModel& CResourceManager::GetModelWithID(const unsigned int aID)
{
	std::shared_lock<std::shared_mutex> lg(ResourceManager🔒::modelSharedMutex);
	return myModels[aID];
}

unsigned int CResourceManager::GetVertexBufferID(const unsigned int aModelID)
{
	std::shared_lock<std::shared_mutex> lgShared(ResourceManager🔒::vertexSharedMutex);

	auto foundIterator(myAssignedVertexBufferIDs.find(aModelID));

	if (foundIterator != myAssignedVertexBufferIDs.end())
	{
		return foundIterator->second;
	}
	else
	{
		lgShared.unlock();

		std::unique_lock<std::shared_mutex> lgUnique(ResourceManager🔒::vertexSharedMutex);

		foundIterator = myAssignedVertexBufferIDs.find(aModelID);

		if (foundIterator != myAssignedVertexBufferIDs.end())
		{
			return foundIterator->second;
		}

		if (myCurrentlyFreeVertexBufferID > (MAX_BUFFER_COUNT - 1))
		{
			assert(false && "Too many vertex-buffers created");
			RESOURCE_LOG("WARNING! Too many vertex-buffers created");
			return UINT_MAX;
		}
		myAssignedVertexBufferIDs.insert({ aModelID, myCurrentlyFreeVertexBufferID });
		return myCurrentlyFreeVertexBufferID++;
	}
}

bool sce::gfx::CResourceManager::UnloadVertexBuffer(const unsigned int aModelID)
{
	std::unique_lock<std::shared_mutex> lg(ResourceManager🔒::vertexSharedMutex);
	return UnloadVertexBufferImpl(aModelID);
}

const CDX11VertexBuffer& CResourceManager::GetVertexBufferWithID(const unsigned int aID) const
{
	std::shared_lock<std::shared_mutex> lg(ResourceManager🔒::vertexSharedMutex);
	return myVertexBuffers[aID];
}

CDX11VertexBuffer& CResourceManager::GetVertexBufferWithID(const unsigned int aID)
{
	std::shared_lock<std::shared_mutex> lg(ResourceManager🔒::vertexSharedMutex);
	return myVertexBuffers[aID];
}

unsigned int sce::gfx::CResourceManager::GetIndexBufferID(const unsigned int aModelID)
{
	std::unique_lock<std::shared_mutex> lgShared(ResourceManager🔒::indexSharedMutex);

	auto foundIterator(myAssignedIndexBufferIDs.find(aModelID));

	if (foundIterator != myAssignedIndexBufferIDs.end())
	{
		return foundIterator->second;
	}
	else
	{
		lgShared.unlock();

		std::unique_lock<std::shared_mutex> lgUnique(ResourceManager🔒::indexSharedMutex);

		foundIterator = myAssignedIndexBufferIDs.find(aModelID);

		if (foundIterator != myAssignedIndexBufferIDs.end())
		{
			return foundIterator->second;
		}

		if (myCurrentlyFreeIndexBufferID > (MAX_BUFFER_COUNT - 1))
		{
			assert(false && "Too many index-buffers created");
			RESOURCE_LOG("WARNING! Too many index-buffers created");
			return UINT_MAX;
		}
		myAssignedIndexBufferIDs.insert({ aModelID, myCurrentlyFreeIndexBufferID });
		return myCurrentlyFreeIndexBufferID++;
	}
}

bool sce::gfx::CResourceManager::UnloadIndexBuffer(const unsigned int aModelID)
{
	std::unique_lock<std::shared_mutex> lg(ResourceManager🔒::indexSharedMutex);
	return UnloadIndexBufferImpl(aModelID);
}

const CDX11IndexBuffer& sce::gfx::CResourceManager::GetIndexBufferWithID(const unsigned int aID) const
{
	std::shared_lock<std::shared_mutex> lg(ResourceManager🔒::indexSharedMutex);
	return myIndexBuffers[aID];
}

CDX11IndexBuffer& sce::gfx::CResourceManager::GetIndexBufferWithID(const unsigned int aID)
{
	std::shared_lock<std::shared_mutex> lg(ResourceManager🔒::indexSharedMutex);
	return myIndexBuffers[aID];
}

unsigned int sce::gfx::CResourceManager::GetConstantBufferID(const char* aObjectRef)
{
	const std::string objectRef(aObjectRef);
	
	std::shared_lock<std::shared_mutex> lgShared(ResourceManager🔒::constantSharedMutex);

	auto foundIterator(myAssignedConstantBufferIDs.find(objectRef));

	if (foundIterator != myAssignedConstantBufferIDs.end())
	{
		return foundIterator->second;
	}
	else
	{
		lgShared.unlock();

		std::unique_lock<std::shared_mutex> lgUnique(ResourceManager🔒::constantSharedMutex);

		foundIterator = myAssignedConstantBufferIDs.find(objectRef);

		if (foundIterator != myAssignedConstantBufferIDs.end())
		{
			return foundIterator->second;
		}

		if (myCurrentlyFreeConstantBufferID > (MAX_BUFFER_COUNT - 1))
		{
			assert(false && "Too many constant-buffers created");
			RESOURCE_LOG("WARNING! Too many constant-buffers created");
			return UINT_MAX;
		}
		myAssignedConstantBufferIDs.insert({ objectRef, myCurrentlyFreeConstantBufferID });
		return myCurrentlyFreeConstantBufferID++;
	}
}

const CDX11ConstantBuffer& sce::gfx::CResourceManager::GetConstantBufferWithID(const unsigned int aID) const
{
	std::shared_lock<std::shared_mutex> lg(ResourceManager🔒::constantSharedMutex);
	return myConstantBuffers[aID];
}

CDX11ConstantBuffer& sce::gfx::CResourceManager::GetConstantBufferWithID(const unsigned int aID)
{
	std::shared_lock<std::shared_mutex> lg(ResourceManager🔒::constantSharedMutex);
	return myConstantBuffers[aID];
}

bool sce::gfx::CResourceManager::CameraExists(const char* aCameraName) const
{
	std::shared_lock<std::shared_mutex> lg(ResourceManager🔒::cameraSharedMutex);
	if (myAssignedCameraIDs.find(aCameraName) != myAssignedCameraIDs.end())
	{
		return true;
	}
	return false;
}

unsigned int sce::gfx::CResourceManager::GetCameraID(const char* aCameraName)
{
	const std::string cameraName(aCameraName);

	std::shared_lock<std::shared_mutex> lgShared(ResourceManager🔒::cameraSharedMutex);

	auto foundIterator(myAssignedCameraIDs.find(cameraName));

	if (foundIterator != myAssignedCameraIDs.end())
	{
		return foundIterator->second;
	}
	else
	{
		lgShared.unlock();

		std::unique_lock<std::shared_mutex> lgUnique(ResourceManager🔒::cameraSharedMutex);

		foundIterator = myAssignedCameraIDs.find(cameraName);

		if (foundIterator != myAssignedCameraIDs.end())
		{
			return foundIterator->second;
		}

		if (myCurrentlyFreeCameraID > (MAX_MODEL_COUNT - 1))
		{
			assert(false && "Too many models created");
			RESOURCE_LOG("WARNING! Too many models created");
			return UINT_MAX;
		}
		myAssignedCameraIDs.insert({ cameraName, myCurrentlyFreeCameraID });
		return myCurrentlyFreeCameraID++;
	}
}

const CCamera& sce::gfx::CResourceManager::GetCameraWithID(const unsigned int aID) const
{
	std::shared_lock<std::shared_mutex> lg(ResourceManager🔒::cameraSharedMutex);
	return myCameras[aID];
}

CCamera& sce::gfx::CResourceManager::GetCameraWithID(const unsigned int aID)
{
	std::shared_lock<std::shared_mutex> lg(ResourceManager🔒::cameraSharedMutex);
	return myCameras[aID];
}

bool sce::gfx::CResourceManager::ShaderExists(const char* aFilePath) const
{
	std::shared_lock<std::shared_mutex> lg(ResourceManager🔒::shaderSharedMutex);
	if (myAssignedShaderIDs.find(aFilePath) != myAssignedShaderIDs.end())
	{
		return true;
	}
	return false;
}

unsigned int CResourceManager::GetShaderID(const char* aFilePath)
{
	const std::string shaderFilePath(aFilePath);

	std::shared_lock<std::shared_mutex> lgShared(ResourceManager🔒::shaderSharedMutex);

	auto foundIterator(myAssignedShaderIDs.find(shaderFilePath));

	if (foundIterator != myAssignedShaderIDs.end())
	{
		return foundIterator->second;
	}
	else
	{
		lgShared.unlock();

		std::unique_lock<std::shared_mutex> lgUnique(ResourceManager🔒::shaderSharedMutex);

		foundIterator = myAssignedShaderIDs.find(shaderFilePath);

		if (foundIterator != myAssignedShaderIDs.end())
		{
			return foundIterator->second;
		}

		if (myCurrentlyFreeShaderID > (MAX_SHADER_COUNT - 1))
		{
			assert(false && "Too many shaders created");
			RESOURCE_LOG("WARNING! Too many shaders created");
			return UINT_MAX;
		}
		myAssignedShaderIDs.insert({ shaderFilePath, myCurrentlyFreeShaderID });
		return myCurrentlyFreeShaderID++;
	}
}

const CDX11Shader& CResourceManager::GeShaderWithID(const unsigned int aID) const
{
	std::shared_lock<std::shared_mutex> lg(ResourceManager🔒::shaderSharedMutex);
	return myShaders[aID];
}

CDX11Shader& CResourceManager::GetShaderWithID(const unsigned int aID)
{
	std::shared_lock<std::shared_mutex> lg(ResourceManager🔒::shaderSharedMutex);
	return myShaders[aID];
}

bool sce::gfx::CResourceManager::TextureExists(const wchar_t* aFilePath)
{
	std::shared_lock<std::shared_mutex> lg(ResourceManager🔒::textureSharedMutex);
	return TextureExistsImpl(aFilePath);
}

bool sce::gfx::CResourceManager::TextureExists(const char* aFilePath)
{
	std::shared_lock<std::shared_mutex> lg(ResourceManager🔒::textureSharedMutex);
	return TextureExistsImpl(aFilePath);
}

bool sce::gfx::CResourceManager::TextureExistsCreateIfNot(const char* aFilePath, unsigned int & aTextureID)
{
	std::unique_lock<std::shared_mutex> lg(ResourceManager🔒::textureSharedMutex);
	if (TextureExistsImpl(aFilePath) == true)
	{
		return true;
	}

	aTextureID = GetTextureIDImpl(aFilePath, false);

	return false;
}

unsigned int sce::gfx::CResourceManager::GetTextureID(const wchar_t* aFilePath)
{
	return GetTextureIDImpl(aFilePath, true);
}

unsigned int sce::gfx::CResourceManager::GetTextureID(const char* aFilePath)
{
	return GetTextureIDImpl(aFilePath, true);
}

const CDX11Texture& sce::gfx::CResourceManager::GetTextureWithID(const unsigned int aID) const
{
	std::shared_lock<std::shared_mutex> lg(ResourceManager🔒::textureSharedMutex);
	return myTextures[aID];
}

CDX11Texture& sce::gfx::CResourceManager::GetTextureWithID(const unsigned int aID)
{
	std::shared_lock<std::shared_mutex> lg(ResourceManager🔒::textureSharedMutex);
	return myTextures[aID];
}
