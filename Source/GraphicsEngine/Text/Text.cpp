#include "stdafx.h"
#include "Text.h"
#include "../Renderer/TextRenderer.h"
#include "GraphicsEngineInterface.h"
#include "../CommonUtilities/Macros.h"

#ifdef max
#undef max
#endif // max

namespace sce
{
	namespace gfx
	{
		unsigned int CText::ColorFloatToUint(const CU::Vector3f& aColor, float aA)
		{
			return ColorFloatToUint(aColor.x, aColor.y, aColor.z, aA);
		}

		unsigned int CText::ColorFloatToUint(const CU::Vector4f& aColor)
		{
			return ColorFloatToUint(aColor.x, aColor.y, aColor.z, aColor.w);
		}

		unsigned int CText::ColorFloatToUint(float aR, float aG, float aB, float aA)
		{
			const float maxUnsignedShort(std::numeric_limits<unsigned short>::max());
			aR = CLAMP(aR, 0, maxUnsignedShort);
			aG = CLAMP(aG, 0, maxUnsignedShort);
			aB = CLAMP(aB, 0, maxUnsignedShort);
			aA = CLAMP(aA, 0, maxUnsignedShort);

			return ColorUshortsToUint(static_cast<unsigned short>(aR * 255), static_cast<unsigned short>(aG * 255), static_cast<unsigned short>(aB * 255), static_cast<unsigned short>(aA * 255));
		}

		unsigned int CText::ColorUintsToUint(unsigned int aR, unsigned int aG, unsigned int aB, unsigned int aA)
		{
			const unsigned int maxUnsignedShort(std::numeric_limits<unsigned short>::max());
			aR = MIN(aR, maxUnsignedShort);
			aG = MIN(aG, maxUnsignedShort);
			aB = MIN(aB, maxUnsignedShort);
			aA = MIN(aA, maxUnsignedShort);
			return ColorUshortsToUint(static_cast<unsigned short>(aR), static_cast<unsigned short>(aG), static_cast<unsigned short>(aB), static_cast<unsigned short>(aA));
		}

		unsigned int CText::ColorUshortsToUint(unsigned short aR, unsigned short aG, unsigned short aB, unsigned short aA)
		{
			aR = CLAMP(aR, 0, 255);
			aG = CLAMP(aG, 0, 255);
			aB = CLAMP(aB, 0, 255);
			aA = CLAMP(aA, 0, 255);

			unsigned int returnValue(0);

			returnValue += aA;
			returnValue <<= 8;
			returnValue += aB;
			returnValue <<= 8;
			returnValue += aG;
			returnValue <<= 8;
			returnValue += aR;

			return returnValue;
		}

		CText::CText()
		{
		}

		CText::~CText()
		{
		}

		bool CText::Init(const std::string& aFontName, const std::string& aText, float aSize, const CU::Vector2f& aWordWrapBoxSize, const CU::Vector2f& aPosition, const unsigned int aColor, const EFontAlignment aAlignSetting)
		{
			myFontName = aFontName;
			myText = aText;
			mySize = aSize;
			myPosition = aPosition;
			myWordWrapBoxSize = aWordWrapBoxSize;
			myColor = aColor;
			myAlignSetting = aAlignSetting;

			return CTextRenderer::GetInstance()->InitFont(aFontName);
		}

		void CText::Render()
		{
			if (myFontName != "")
			{
				CGraphicsEngineInterface::AddToScene(*this);
			}
		}

		const std::string& CText::GetFontName() const
		{
			return myFontName;
		}

		const std::string& CText::GetText() const
		{
			return myText;
		}

		float CText::GetSize() const
		{
			return mySize;
		}

		const CU::Vector2f& CText::GetPosition() const
		{
			return myPosition;
		}

		unsigned int CText::GetColor() const
		{
			return myColor;
		}

		float CText::GetWidth() const
		{
			return CTextRenderer::GetInstance()->GetTextWidth(*this);
		}

		void CText::SetText(const char* aCharPointer)
		{
			myText = aCharPointer;
		}

		void CText::SetText(const std::string& aString)
		{
			myText = aString;
		}

		void CText::SetSize(float aSize)
		{
			mySize = aSize;
		}

		void CText::SetPosition(const CU::Vector2f& aPosition)
		{
			myPosition = aPosition;
		}

		void CText::SetColor(unsigned int aColor)
		{
			myColor = aColor;
		}
		void CText::SetAlignCenter(const EFontAlignment aAlignSetting)
		{
			myAlignSetting = aAlignSetting;
		}
	}
}
