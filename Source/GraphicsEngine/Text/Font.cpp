#include "stdafx.h"
#include "Font.h"
#include <d3d11.h>
#include "External\FW1FontWrapper.h"
#include "GraphicsEngineInterface.h"
#pragma comment(lib, "FW1FontWrapper.lib")

bool CharToWString(const char* aText, std::wstring& aWString);

namespace sce
{
	namespace gfx
	{
		static IFW1Factory* fontFactory = nullptr;

#pragma region Custom font creation helpers
		// Font enumerator wrapper for a single font
		class CFontEnum : public IDWriteFontFileEnumerator
		{
			friend CMemoryPool;

		public:
			CFontEnum(IDWriteFontFile* pFontFile)
				: m_cRefCount(1)
				, m_pFontFile(pFontFile)
				, m_count(0)
			{
				if (m_pFontFile != nullptr)
				{
					m_pFontFile->AddRef();
				}
			}

			virtual HRESULT STDMETHODCALLTYPE QueryInterface(REFIID riid, void** ppvObject) override
			{
				if (ppvObject == nullptr)
				{
					return E_INVALIDARG;
				}

				if (IsEqualIID(riid, __uuidof(IUnknown)))
				{
					*ppvObject = static_cast<IUnknown*>(this);
					AddRef();
					return S_OK;
				}
				else if (IsEqualIID(riid, __uuidof(IDWriteFontFileEnumerator)))
				{
					*ppvObject = static_cast<IDWriteFontFileEnumerator*>(this);
					AddRef();
					return S_OK;
				}

				*ppvObject = nullptr;
				return E_NOINTERFACE;
			}

			virtual ULONG STDMETHODCALLTYPE AddRef() override
			{
				return static_cast<ULONG>(InterlockedIncrement(reinterpret_cast<LONG*>(&m_cRefCount)));
			}

			virtual ULONG STDMETHODCALLTYPE Release() override
			{
				ULONG newCount = static_cast<ULONG>(InterlockedDecrement(reinterpret_cast<LONG*>(&m_cRefCount)));

				if (newCount == 0)
				{
					CFontEnum* thisPointer(this);
					sce_delete(thisPointer);
				}

				return newCount;
			}

			virtual HRESULT STDMETHODCALLTYPE GetCurrentFontFile(IDWriteFontFile** ppFontFile) override
			{
				if (ppFontFile == nullptr)
				{
					return E_INVALIDARG;
				}

				if (m_pFontFile == nullptr)
				{
					return E_FAIL;
				}

				m_pFontFile->AddRef();
				*ppFontFile = m_pFontFile;

				return S_OK;
			}

			virtual HRESULT STDMETHODCALLTYPE MoveNext(BOOL* pHasCurrentFile) override
			{
				if (pHasCurrentFile == nullptr)
				{
					return E_INVALIDARG;
				}

				if (m_count > 0)
				{
					*pHasCurrentFile = FALSE;
				}
				else
				{
					*pHasCurrentFile = TRUE;
				}

				++m_count;

				return S_OK;
			}

		private:
			~CFontEnum()
			{
				if (m_pFontFile != nullptr)
				{
					m_pFontFile->Release();
				}
			}

			CFontEnum(const CFontEnum&) = default;
			CFontEnum& operator=(const CFontEnum&) = default;

			ULONG m_cRefCount;
			IDWriteFontFile* m_pFontFile;
			UINT m_count;

		};

		// Font collection loader for a single font
		class CCollectionLoader : public IDWriteFontCollectionLoader
		{
			friend CMemoryPool;

		public:
			CCollectionLoader(const WCHAR* szFontfilePath)
				: m_cRefCount(1)
				, m_filepath(szFontfilePath)
			{
			}

			virtual HRESULT STDMETHODCALLTYPE QueryInterface(REFIID riid, void** ppvObject) override
			{
				if (ppvObject == nullptr)
				{
					return E_INVALIDARG;
				}

				if (IsEqualIID(riid, __uuidof(IUnknown)))
				{
					*ppvObject = static_cast<IUnknown*>(this);
					AddRef();
					return S_OK;
				}
				else if (IsEqualIID(riid, __uuidof(IDWriteFontCollectionLoader)))
				{
					*ppvObject = static_cast<IDWriteFontCollectionLoader*>(this);
					AddRef();
					return S_OK;
				}

				*ppvObject = nullptr;
				return E_NOINTERFACE;
			}

			virtual ULONG STDMETHODCALLTYPE AddRef() override
			{
				return static_cast<ULONG>(InterlockedIncrement(reinterpret_cast<LONG*>(&m_cRefCount)));
			}

			virtual ULONG STDMETHODCALLTYPE Release() override
			{
				ULONG newCount = static_cast<ULONG>(InterlockedDecrement(reinterpret_cast<LONG*>(&m_cRefCount)));

				if (newCount == 0)
				{
					CCollectionLoader* thisPointer(this);
					sce_delete(thisPointer);
				}

				return newCount;
			}

			virtual HRESULT STDMETHODCALLTYPE CreateEnumeratorFromKey(IDWriteFactory* pFactory, const void* collectionKey, UINT32 collectionKeySize, IDWriteFontFileEnumerator** ppFontFileEnumerator) override
			{
				collectionKey;
				collectionKeySize;

				if ((pFactory == nullptr) || (ppFontFileEnumerator == nullptr))
				{
					return E_INVALIDARG;
				}

				IDWriteFontFile* pFontFile;
				HRESULT hResult = pFactory->CreateFontFileReference(m_filepath.c_str(), nullptr, &pFontFile);
				if (FAILED(hResult))
				{
					return hResult;
				}

				CFontEnum* pEnum = sce_new(CFontEnum(pFontFile));
				*ppFontFileEnumerator = pEnum;

				pFontFile->Release();

				return S_OK;
			}

		private:
			~CCollectionLoader() = default;

			CCollectionLoader(const CCollectionLoader&) = default;
			CCollectionLoader& operator=(const CCollectionLoader&) = default;

			ULONG m_cRefCount;
			std::wstring m_filepath;

		};
#pragma endregion

		CFont::CFont()
			: myIsUsingCustomFont(false)
			, mySizeLastRender(0.0f)
			, myAlignSettingLastRender(EFontAlignment::eLeft)
			, myDevice(nullptr)
			, myDeviceContext(nullptr)
			, myFont(nullptr)
		{
		}

		CFont::~CFont()
		{
			SAFE_RELEASE(myFont);
		}

		bool CFont::Init(ID3D11Device* aDevice, ID3D11DeviceContext* aDeviceContext, const char* aFontName)
		{
			if (fontFactory == nullptr)
			{
				if (FAILED(FW1CreateFactory(FW1_VERSION, &fontFactory)) == true)
				{
					ENGINE_LOG("ERROR! Failed to create font factory");
					return false;
				}
			}

			std::wstring fontName;
			if (CharToWString(aFontName, fontName) == false)
			{
				ENGINE_LOG("ERROR! Failed to convert font name \"%s\" from char to WString", aFontName);
				return false;
			}

			myDevice = aDevice;
			myDeviceContext = aDeviceContext;

			const std::wstring::size_type lastDotPosition(fontName.find_last_of(L'.'));
			if (lastDotPosition != fontName.npos)
			{
				myIsUsingCustomFont = true;

				std::wstring::size_type lastSlashPosition(fontName.find_last_of(L'/'));
				std::wstring::size_type diff(1);
				if (lastSlashPosition == fontName.npos)
				{
					lastSlashPosition = fontName.find_last_of(L'\\');

					if (lastSlashPosition == fontName.npos)
					{
						lastSlashPosition = 0;
						diff = 0;
					}
				}

				myFontName = fontName.substr((lastSlashPosition + diff), ((lastDotPosition - lastSlashPosition) - diff));
				myFontPath = fontName;

				if (FAILED(fontFactory->CreateFontWrapper(myDevice, L"DUMMY", &myFont)) == true)
				{
					ENGINE_LOG("ERROR! Failed to create custom font wrapper object");
					SAFE_RELEASE(myFont);
					return false;
				}
			}
			else
			{
				if (FAILED(fontFactory->CreateFontWrapper(aDevice, fontName.c_str(), &myFont)) == true)
				{
					ENGINE_LOG("ERROR! Failed to create font wrapper object");
					SAFE_RELEASE(myFont);
					return false;
				}

				myFontName = fontName;
			}

			return true;
		}

		bool CFont::Render(const char* aText, float aSize, const CU::Vector2f& aPosition, unsigned int aColor, const CU::Matrix44f& aTransform, const CU::Vector2f& aWordWrapBoxSize, const EFontAlignment aAlignSetting)
		{
			std::wstring wstring;
			if (CharToWString(aText, wstring) == false)
			{
				ENGINE_LOG("ERROR! Failed to convert text \"%s\" from char to WString", aText);
				return false;
			}
			return Render(wstring.c_str(), aSize, aPosition, aColor, aTransform, aWordWrapBoxSize, aAlignSetting);
		}

		bool CFont::Render(const wchar_t* aText, float aSize, const CU::Vector2f& aPosition, unsigned int aColor, const CU::Matrix44f& aTransform, const CU::Vector2f& aWordWrapBoxSize, const EFontAlignment aAlignSetting)
		{
			if ((myFont != nullptr) || (myIsUsingCustomFont == true))
			{
				FW1_RECTF rect;
				rect.Left = aPosition.x;
				rect.Top = aPosition.y;
				rect.Right = rect.Left;
				rect.Bottom = rect.Top;

				const auto renderResolution(CGraphicsEngineInterface::GetFullWindowedResolution());
				const auto windowResolution(CGraphicsEngineInterface::GetResolution());
				const auto targetResolution(CGraphicsEngineInterface::GetTargetResolution());

				const float sizeToUse(aSize * (renderResolution.y / targetResolution.y));
				const CU::Vector2f scale(windowResolution / renderResolution);

				CU::Matrix44f transform(aTransform);
				transform[0] *= scale.x;
				transform[5] *= scale.y;

				if (myIsUsingCustomFont == true)
				{
					if (UpdateCustomFont(aText, sizeToUse, aWordWrapBoxSize, aAlignSetting) == false)
					{
						ENGINE_LOG("ERROR! Failed to render custom font \"%ls\" with text \"%ls\"", myFontPath.c_str(), aText);
						return false;
					}
					if (myFontCreation.myTextLayout != nullptr)
					{
						myFont->DrawTextLayout(myDeviceContext, myFontCreation.myTextLayout, rect.Left, rect.Top, aColor, nullptr, (float*)&transform, FW1_RESTORESTATE | FW1_NOWORDWRAP);
					}
				}
				else
				{
					myFont->DrawString(myDeviceContext, aText, myFontName.c_str(), sizeToUse, &rect, aColor, nullptr, (float*)&transform, FW1_RESTORESTATE | FW1_NOWORDWRAP);
				}

				return true;
			}

			ENGINE_LOG("ERROR! Tried to render text with no font attached");
			return false;
		}

		const std::wstring& CFont::GetName() const
		{
			return myFontName;
		}

		float CFont::SetDataAndGetWidth(const std::string& aText, const float aSize, const CU::Vector2f& aWordWrapBoxSize, const EFontAlignment aAlignSetting)
		{
			const CU::Vector2f resolution(CGraphicsEngineInterface::GetResolution());
			const CU::Vector2f targetResolution(CGraphicsEngineInterface::GetTargetResolution());

			std::wstring wstring;
			if (CharToWString(aText.c_str(), wstring) == false)
			{
				ENGINE_LOG("ERROR! Failed to convert text \"%s\" from char to WString", aText);
				return false;
			}

			if (myIsUsingCustomFont == true)
			{
				const float sizeToUse(aSize * (resolution.y / targetResolution.y));

				if (UpdateCustomFont(wstring.c_str(), sizeToUse, aWordWrapBoxSize, aAlignSetting) == false)
				{
					ENGINE_LOG("ERROR! Failed to update font \"%ls\" with text \"%ls\"", myFontPath.c_str(), wstring);
					return false;
				}
			}
			else
			{
				assert("Get width is only implemented for custom fonts" && false);
				return -1.0f;
			}

			float minWidth(0.0f);
			
			if (FAILED(myFontCreation.myTextLayout->DetermineMinWidth(&minWidth)) == true)
			{
				ENGINE_LOG("WARNING! Failed to get width of font \"%ls\" with text \"ls\" and size %d"
					, myFontPath.c_str(), myTextLastRender.c_str(), aSize);
			}

			return (minWidth / resolution.x);
		}

		bool CFont::UpdateCustomFont(const wchar_t* aText, float aSize, const CU::Vector2f& aWordWrapBoxSize, const EFontAlignment aAlignSetting)
		{
			const bool textDifferent(aText != myTextLastRender);
			const bool sizeDifferent(aSize != mySizeLastRender);
			const bool wrapDifferent(aWordWrapBoxSize != myWordWrapBoxLastRender);

			if ((textDifferent == false)
				&& (sizeDifferent == false)
				&& (wrapDifferent == false)
				&& (myAlignSettingLastRender == myAlignSettingLastRender))
			{
				return true;
			}

			myTextLastRender = aText;
			mySizeLastRender = aSize;
			myWordWrapBoxLastRender = aWordWrapBoxSize;
			myAlignSettingLastRender = aAlignSetting;

			if (myFontCreation.myFactory == nullptr)
			{
				if (FAILED(myFont->GetDWriteFactory(&myFontCreation.myFactory)) == true)
				{
					ENGINE_LOG("ERROR! Failed to get DWriteFactory");
					SAFE_RELEASE(myFontCreation.myFactory);
					return false;
				}
			}

			if (myFontCreation.myFontCollectionLoader == nullptr)
			{
				myFontCreation.myFontCollectionLoader = sce_new(CCollectionLoader(myFontPath.c_str()));
				if (FAILED(myFontCreation.myFactory->RegisterFontCollectionLoader(myFontCreation.myFontCollectionLoader)) == true)
				{
					ENGINE_LOG("ERROR! Failed to register font collection loader");
					SAFE_RELEASE(myFontCreation.myFontCollectionLoader);
					return false;
				}
			}

			if (myFontCreation.myFontCollection == nullptr)
			{
				if (FAILED(myFontCreation.myFactory->CreateCustomFontCollection(myFontCreation.myFontCollectionLoader, nullptr, 0, &myFontCreation.myFontCollection)) == true)
				{
					ENGINE_LOG("ERROR! Failed to create font collection");
					SAFE_RELEASE(myFontCreation.myFontCollection);
					return false;
				}
			}

			if (sizeDifferent == true)
			{
				SAFE_RELEASE(myFontCreation.myTextFormat);
				if (FAILED(myFontCreation.myFactory->CreateTextFormat(myFontName.c_str(), myFontCreation.myFontCollection, DWRITE_FONT_WEIGHT_NORMAL, DWRITE_FONT_STYLE_NORMAL, DWRITE_FONT_STRETCH_NORMAL, mySizeLastRender, L"", &myFontCreation.myTextFormat)) == true)
				{
					ENGINE_LOG("ERROR! Failed to create text format");
					SAFE_RELEASE(myFontCreation.myTextFormat);
					return false;
				}
			}

			if ((sizeDifferent == true) || (textDifferent == true) || (wrapDifferent == true))
			{
				SAFE_RELEASE(myFontCreation.myTextLayout);
				if (FAILED(myFontCreation.myFactory->CreateTextLayout(myTextLastRender.c_str(), static_cast<unsigned int>(myTextLastRender.size()), myFontCreation.myTextFormat, myWordWrapBoxLastRender.x, myWordWrapBoxLastRender.y, &myFontCreation.myTextLayout)) == true)
				{
					ENGINE_LOG("ERROR! Failed to create text layout");
					SAFE_RELEASE(myFontCreation.myTextLayout);
					return false;
				}

				if (aAlignSetting == EFontAlignment::eCenter)
				{
					DWRITE_TEXT_ALIGNMENT alignment = DWRITE_TEXT_ALIGNMENT_CENTER;
					myFontCreation.myTextLayout->SetTextAlignment(alignment);
				}
				else if (aAlignSetting == EFontAlignment::eRight)
				{
					DWRITE_TEXT_ALIGNMENT alignment = DWRITE_TEXT_ALIGNMENT_TRAILING;
					myFontCreation.myTextLayout->SetTextAlignment(alignment);
				}

				if (myWordWrapBoxLastRender == CU::Vector2f::Zero)
				{
					myFontCreation.myTextLayout->SetWordWrapping(DWRITE_WORD_WRAPPING_NO_WRAP);
				}
				else
				{
					myFontCreation.myTextLayout->SetWordWrapping(DWRITE_WORD_WRAPPING_WHOLE_WORD);
				}
			}

			return true;
		}

		CFont::SFontCreation::~SFontCreation()
		{
			if (myFactory != nullptr)
			{
				if (FAILED(myFactory->UnregisterFontCollectionLoader(myFontCollectionLoader)) == true)
				{
					ENGINE_LOG("ERROR! Failed to release font collection loader");
				}
			}
			SAFE_RELEASE(myTextLayout);
			SAFE_RELEASE(myTextFormat);
			SAFE_RELEASE(myFontCollection);
			SAFE_RELEASE(myFontCollectionLoader);
			SAFE_RELEASE(myFactory);
		}
	}
}

bool CharToWString(const char* aText, std::wstring& aWString)
{
	const size_t size(std::strlen(aText) + 1);
	if (size > 0)
	{
		aWString.resize(size);
		size_t charsConverted;
		mbstowcs_s(&charsConverted, &aWString[0], size, aText, size - 1);
		return true;
	}
	return false;
}
