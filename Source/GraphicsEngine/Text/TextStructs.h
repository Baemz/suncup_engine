#pragma once

namespace sce
{
	namespace gfx
	{
		enum class EFontAlignment
		{
			eLeft
			, eCenter
			, eRight
		};
	}
}