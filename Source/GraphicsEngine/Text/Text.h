#pragma once
#include "..\CommonUtilities\Vector.h"
#include "TextStructs.h"
#include <string>

namespace sce
{
	namespace gfx
	{
		class CText
		{
		public:
			static unsigned int ColorFloatToUint(const CU::Vector3f& aColor, float aA = 1.0f);
			static unsigned int ColorFloatToUint(const CU::Vector4f& aColor);
			static unsigned int ColorFloatToUint(float aR, float aG, float aB, float aA = 1.0f);
			static unsigned int ColorUintsToUint(unsigned int aR, unsigned int aG, unsigned int aB, unsigned int aA = 1.0f);
			static unsigned int ColorUshortsToUint(unsigned short aR, unsigned short aG, unsigned short aB, unsigned short aA = 1.0f);

			CText();
			~CText();

			// Color is in 0xAABBGGRR, use the conversion functions
			bool Init(const std::string& aFontName, const std::string& aText = "", float aSize = 24.0f, const CU::Vector2f& aWordWrapBoxSize = CU::Vector2f(0.0f), const CU::Vector2f& aPosition = CU::Vector2f(0.0f, 0.0f), const unsigned int aColor = 0xffffffff, const EFontAlignment aAlignSetting = EFontAlignment::eLeft);
			//
			void Render();

			const std::string& GetFontName() const;
			const std::string& GetText() const;
			float GetSize() const;
			const CU::Vector2f& GetPosition() const;
			inline const CU::Vector2f& GetWordWrapBoxSize() const { return myWordWrapBoxSize; }
			inline const EFontAlignment& GetAlignSetting() const { return myAlignSetting; }
			unsigned int GetColor() const;
			float GetWidth() const; // NOTE: Can be expensive (always mutex, maybe re-calculating vector graphics)

			void SetText(const char* aCharPointer);
			void SetText(const std::string& aString);

			CText& operator=(const CText& aText) = default;
			CText& operator=(const char* aCharPointer)
			{
				myText = aCharPointer;
				return *this;
			}
			CText& operator=(const std::string& aString)
			{
				myText = aString;
				return *this;
			}

			void SetSize(float aSize);
			void SetPosition(const CU::Vector2f& aPosition);
			void SetColor(unsigned int aColor);
			void SetAlignCenter(const EFontAlignment aAlignSetting);

		private:
			std::string myFontName;
			std::string myText;
			float mySize;
			CU::Vector2f myPosition;
			CU::Vector2f myWordWrapBoxSize;
			unsigned int myColor;
			EFontAlignment myAlignSetting;
		};
	}
}
