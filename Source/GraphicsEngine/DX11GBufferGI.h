#pragma once
#include "GI_Declarations.h"
struct ID3D11Device;
struct ID3D11DeviceContext;
struct ID3D11Texture3D;
struct ID3D11Buffer;
struct ID3D11RenderTargetView;
struct ID3D11DepthStencilView;
struct ID3D11ShaderResourceView;
struct ID3D11UnorderedAccessView;
struct D3D11_VIEWPORT;

namespace sce { namespace gfx {

	enum class EGBufferComponent
	{
		Normals,
		AlbedoAndRoughness,
		EmissiveAndMetallic,
		AOAndFogFactor
	};

	class CDX11RenderTargetGI
	{
	public:
		CDX11RenderTargetGI();
		~CDX11RenderTargetGI();

		void Init(const EVoxelSize& aQualityLevel);
		void SetAsRenderTarget();
		void UnbindRenderTarget();
		void BindRadianceAsResourceOnSlot(const unsigned int aSlot);
		void CSBindRadianceAsResourceOnSlot(const unsigned int aSlot);
		void BindBouncedRadianceAsResourceOnSlot(const unsigned int aSlot);

		void VSBindRadianceAsResourceOnSlot(const unsigned int aSlot);
		void VSBindBouncedRadianceAsResourceOnSlot(const unsigned int aSlot);
		void CSBindDecodedRadianceAsUAVOnSlot(const unsigned int aSlot);
		void CSBindBouncedRadianceAsUAVOnSlot(const unsigned int aSlot);
		void CSBindEncodedRadianceAsUAVOnSlot(const unsigned int aSlot);
		void CSBindEncodedRadianceAsResourceOnSlot(const unsigned int aSlot);
		void CSUnbindUAVOnSlot(const unsigned int aSlot);
		void CSUnbindResourceOnSlot(const unsigned int aSlot);

		const EVoxelSize& GetVoxelSize() const;
		const unsigned int GetMips() const;

	private:
		EVoxelSize myVoxelSize;
		unsigned int myMips;
		ID3D11Device* myDevice;
		ID3D11DeviceContext* myContext;
		ID3D11ShaderResourceView** myShaderResource;
		ID3D11UnorderedAccessView** myUAVTarget;
		ID3D11DepthStencilView* myDepthStencil;
		ID3D11Texture3D* myTexture2;
		ID3D11Texture3D* myTexture;
		ID3D11Buffer* myStructuredBuffer;
		D3D11_VIEWPORT* myViewPort;
	};

}}