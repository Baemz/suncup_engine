#pragma once
#include <assert.h>
#include <memory.h>

namespace sce
{
	using uint = unsigned int;
	using ushort = unsigned short;

	enum EModelBluePrint
	{
		EModelBluePrint_Position = 1 << 0,
		EModelBluePrint_UV = 1 << 1,
		EModelBluePrint_Normal = 1 << 2,
		EModelBluePrint_BinormTan = 1 << 3,
		EModelBluePrint_Bones = 1 << 4,
		EModelBluePrint_ScreenSpace = 1 << 5,
	};

#define NUM_BONES_PER_VEREX 4
#define ZERO_MEM(a) memset(a, 0, sizeof(a))
#define ARRAY_SIZE_IN_ELEMENTS(a) (sizeof(a)/sizeof(a[0]))

	struct VertexBoneData
	{
		unsigned int IDs[NUM_BONES_PER_VEREX];
		float Weights[NUM_BONES_PER_VEREX];

		VertexBoneData()
		{
			Reset();
		};

		void Reset()
		{
			ZERO_MEM(IDs);
			ZERO_MEM(Weights);
		}

		void AddBoneData(unsigned int BoneID, float Weight)
		{
			for (unsigned int i = 0; i < ARRAY_SIZE_IN_ELEMENTS(IDs); i++) {
				if (Weights[i] == 0.0) {
					IDs[i] = BoneID;
					Weights[i] = Weight;
					return;
				}
			}

			// should never get here - more bones than we have space for
			assert(0);
		}
	};
}

#define PUSH_V4(aX, aY, aZ, aW)													\
*(float*)(&aLoaderMesh->myVerticies[sizeof(float) * (pushIndex + 0)]) = (aX);	\
*(float*)(&aLoaderMesh->myVerticies[sizeof(float) * (pushIndex + 1)]) = (aY);	\
*(float*)(&aLoaderMesh->myVerticies[sizeof(float) * (pushIndex + 2)]) = (aZ);	\
*(float*)(&aLoaderMesh->myVerticies[sizeof(float) * (pushIndex + 3)]) = (aW);	\
pushIndex += 4;																	\

#define PUSH_V2(aX, aY)															\
*(float*)(&aLoaderMesh->myVerticies[sizeof(float) * (pushIndex + 0)]) = (aX);	\
*(float*)(&aLoaderMesh->myVerticies[sizeof(float) * (pushIndex + 1)]) = (aY);	\
pushIndex += 2;																	\

