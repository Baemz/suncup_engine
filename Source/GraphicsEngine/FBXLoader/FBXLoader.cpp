#include "stdafx.h"
#include "FBXLoader.h"

#include "assimp/cimport.h"
#include "assimp/scene.h"
#include "assimp/postprocess.h"

#include <map>
#include <string>
#include <windows.h>
#include <fstream>

#include <memory.h>

#define POW2(x) ((x)*(x))

namespace sce
{
	CFBXLoader::CFBXLoader()
		: myCFXLoaderCustom(nullptr)
	{
	}

	CFBXLoader::~CFBXLoader()
	{
		sce_delete(myCFXLoaderCustom);
	}

	bool CFBXLoader::Init()
	{
		if (myCFXLoaderCustom == nullptr)
		{
			myCFXLoaderCustom = sce_new(CFBXLoaderCustom());
			return true;
		}

		return false;
	}

	const aiScene * CFBXLoader::GetAIScene(const char * aPathToModel)
	{
		if (myCFXLoaderCustom != nullptr)
		{
			return myCFXLoaderCustom->GetAIScene(aPathToModel);
		}
		return nullptr;
	}

	bool CFBXLoader::LoadModel(const char* aPathToModel, CLoaderModel** aModel) const
	{
		*aModel = nullptr;

		if (myCFXLoaderCustom != nullptr)
		{
			*aModel = myCFXLoaderCustom->LoadModel(aPathToModel);
		}

		if (*aModel != nullptr)
		{
			for (unsigned int i = 0; i < (*aModel)->myMeshes.Size(); ++i)
			{
				if ((*aModel)->myMaxRadius < (*aModel)->myMeshes[i]->myMaxRadius)
				{
					(*aModel)->myMaxRadius = (*aModel)->myMeshes[i]->myMaxRadius;
				}
				(*aModel)->centerPos = (*aModel)->myMeshes[0]->centerPos;
			}
			return true;
		}

		return false;
	}

#pragma comment (lib, "..\\..\\Libs\\assimp\\assimp.lib")

#define TEXTURE_SET_0 0

	CFBXLoaderCustom::CFBXLoaderCustom()
	{
	}


	CFBXLoaderCustom::~CFBXLoaderCustom()
	{
	}

	CU::Matrix44f ConvertToEngineMatrix33(const aiMatrix3x3& AssimpMatrix)
	{

		CU::Matrix44f mat;
		mat.myMatrix[0] = AssimpMatrix.a1; mat.myMatrix[1] = AssimpMatrix.a2; mat.myMatrix[2] = AssimpMatrix.a3; mat.myMatrix[3] = 0.0f;
		mat.myMatrix[4] = AssimpMatrix.b1; mat.myMatrix[5] = AssimpMatrix.b2; mat.myMatrix[6] = AssimpMatrix.b3; mat.myMatrix[7] = 0.0f;
		mat.myMatrix[8] = AssimpMatrix.c1; mat.myMatrix[9] = AssimpMatrix.c2; mat.myMatrix[10] = AssimpMatrix.c3; mat.myMatrix[11] = 0.0f;
		mat.myMatrix[12] = 0.0f; mat.myMatrix[13] = 0.0f; mat.myMatrix[14] = 0.0f; mat.myMatrix[15] = 1.0f;
		return mat;
	}

	// constructor from Assimp matrix
	CU::Matrix44f ConvertToEngineMatrix44(const aiMatrix4x4& AssimpMatrix)
	{
		CU::Matrix44f mat;
		mat.myMatrix[0] = AssimpMatrix.a1; mat.myMatrix[1] = AssimpMatrix.a2; mat.myMatrix[2] = AssimpMatrix.a3; mat.myMatrix[3] = AssimpMatrix.a4;
		mat.myMatrix[4] = AssimpMatrix.b1; mat.myMatrix[5] = AssimpMatrix.b2; mat.myMatrix[6] = AssimpMatrix.b3; mat.myMatrix[7] = AssimpMatrix.b4;
		mat.myMatrix[8] = AssimpMatrix.c1; mat.myMatrix[9] = AssimpMatrix.c2; mat.myMatrix[10] = AssimpMatrix.c3; mat.myMatrix[11] = AssimpMatrix.c4;
		mat.myMatrix[12] = AssimpMatrix.d1; mat.myMatrix[13] = AssimpMatrix.d2; mat.myMatrix[14] = AssimpMatrix.d3; mat.myMatrix[15] = AssimpMatrix.d4;
		return mat;
	}

	int CFBXLoaderCustom::DetermineAndLoadVerticies(aiMesh* fbxMesh, CLoaderMesh* aLoaderMesh)
	{
		unsigned int modelBluePrintType = 0;

		modelBluePrintType |= (fbxMesh->HasPositions() ? EModelBluePrint_Position : 0);
		modelBluePrintType |= (fbxMesh->HasTextureCoords(0) ? EModelBluePrint_UV : 0);
		modelBluePrintType |= (fbxMesh->HasNormals() ? EModelBluePrint_Normal : 0);
		modelBluePrintType |= (fbxMesh->HasTangentsAndBitangents() ? EModelBluePrint_BinormTan : 0);
		modelBluePrintType |= (fbxMesh->HasBones() ? EModelBluePrint_Bones : 0);

		int vertexBufferSize = 0;
		vertexBufferSize += ((fbxMesh->HasPositions() || true) ? sizeof(float) * 4 : 0);
		vertexBufferSize += ((fbxMesh->HasTextureCoords(0) || true) ? sizeof(float) * 2 : 0);
		vertexBufferSize += ((fbxMesh->HasNormals() || true) ? sizeof(float) * 4 : 0);
		vertexBufferSize += ((fbxMesh->HasTangentsAndBitangents() || true) ? sizeof(float) * 8 : 0);
		vertexBufferSize += ((fbxMesh->HasBones() || true) ? sizeof(float) * 8 : 0); // Better with an UINT, but this works

		aLoaderMesh->myShaderType = modelBluePrintType;
		aLoaderMesh->myVertexBufferSize = vertexBufferSize;

		aLoaderMesh->myVerticies = sce_newArray(char, (vertexBufferSize * fbxMesh->mNumVertices));
		aLoaderMesh->myVertexCount = fbxMesh->mNumVertices;

		CU::GrowingArray<VertexBoneData, unsigned int> collectedBoneData;
		if (fbxMesh->HasBones())
		{
			collectedBoneData.Resize(fbxMesh->mNumVertices);

			unsigned int BoneIndex = 0;
			for (unsigned int i = 0; i < fbxMesh->mNumBones; i++)
			{
				std::string BoneName(fbxMesh->mBones[i]->mName.data);
				if (aLoaderMesh->myModel->myBoneNameToIndex.find(BoneName) == aLoaderMesh->myModel->myBoneNameToIndex.end())
				{
					BoneIndex = aLoaderMesh->myModel->myNumBones;
					aLoaderMesh->myModel->myNumBones++;
					BoneInfo bi;
					aLoaderMesh->myModel->myBoneInfo.Add(bi);


					CU::Matrix44f NodeTransformation = ConvertToEngineMatrix44(fbxMesh->mBones[i]->mOffsetMatrix);

					aLoaderMesh->myModel->myBoneInfo[BoneIndex].BoneOffset = NodeTransformation;
					aLoaderMesh->myModel->myBoneNameToIndex[BoneName] = BoneIndex;
				}
				else
				{
					BoneIndex = aLoaderMesh->myModel->myBoneNameToIndex[BoneName];
				}

				for (unsigned int j = 0; j < fbxMesh->mBones[i]->mNumWeights; j++)
				{
					unsigned int VertexID = fbxMesh->mBones[i]->mWeights[j].mVertexId;
					float Weight = fbxMesh->mBones[i]->mWeights[j].mWeight;
					collectedBoneData[VertexID].AddBoneData(BoneIndex, Weight);
				}
			}
		}

		unsigned int pushIndex(0);
		float maxDist = 0.f;
		aiVector3D avgVert;
		for (unsigned int i = 0; i < fbxMesh->mNumVertices; i++)
		{
			if (fbxMesh->HasPositions())
			{
				aiVector3D& mVertice = fbxMesh->mVertices[i];
				PUSH_V4(mVertice.x, mVertice.y, mVertice.z, 1);
				const float newVal = POW2(mVertice.x) + POW2(mVertice.y) + POW2(mVertice.z);
				if (newVal > maxDist)
				{
					maxDist = newVal;
				}

				avgVert.x += mVertice.x;
				avgVert.y += mVertice.y;
				avgVert.z += mVertice.z;
			}
			else
			{
				maxDist = 15.f;
				PUSH_V4(0.f, 0.f, 0.f, 0.f);
			}
			if (fbxMesh->HasNormals())
			{
				aiVector3D& mNorm = fbxMesh->mNormals[i];
				PUSH_V4(mNorm.x, mNorm.y, mNorm.z, 1);
			}
			else
			{
				PUSH_V4(0.f, 0.f, 0.f, 0.f);
			}
			if (fbxMesh->HasTangentsAndBitangents())
			{
				aiVector3D& mTangent = fbxMesh->mTangents[i];
				aiVector3D& biTangent = fbxMesh->mBitangents[i];

				PUSH_V4(mTangent.x, mTangent.y, mTangent.z, 1);
				PUSH_V4(biTangent.x, biTangent.y, biTangent.z, 1);
			}
			else
			{
				PUSH_V4(0.f, 0.f, 0.f, 0.f);
				PUSH_V4(0.f, 0.f, 0.f, 0.f);
			}
			if (fbxMesh->HasTextureCoords(TEXTURE_SET_0))
			{
				PUSH_V2(fbxMesh->mTextureCoords[TEXTURE_SET_0][i].x, fbxMesh->mTextureCoords[TEXTURE_SET_0][i].y);
			}
			else
			{
				PUSH_V2(0.f, 0.f);
			}
			if (fbxMesh->HasBones())
			{
				VertexBoneData& boneData = collectedBoneData[i];

				aiVector3D bones;
				PUSH_V4((float)boneData.IDs[0], (float)boneData.IDs[1], (float)boneData.IDs[2], (float)boneData.IDs[3]);

				aiVector3D weights;
				PUSH_V4(boneData.Weights[0], boneData.Weights[1], boneData.Weights[2], boneData.Weights[3]);
			}
			else
			{
				// Sets data to 0 if no animation-data is available in model.
				PUSH_V4(0.f, 0.f, 0.f, 0.f);
				PUSH_V4(0.f, 0.f, 0.f, 0.f);
			}
		}

		aLoaderMesh->centerPos = { avgVert.x / fbxMesh->mNumVertices, avgVert.y / fbxMesh->mNumVertices, avgVert.z / fbxMesh->mNumVertices };
		aLoaderMesh->myMaxRadius = sqrt(fabs(maxDist));

		return vertexBufferSize;
	}

	CLoaderModel* CFBXLoaderCustom::LoadModel(const char* aModel)
	{
		CLoaderModel* newModel = sce_new(CLoaderModel());
		newModel->SetData(aModel);

		if (!LoadModelInternal(newModel))
		{
			sce_delete(newModel);
			newModel = nullptr;
		}

		return newModel;
	}

	bool does_file_exist(const char *fileName)
	{
		std::ifstream infile(fileName);
		return infile.good();
	}

	const aiScene* CFBXLoaderCustom::GetAIScene(const char* aPathToModel)
	{
		const aiScene* scene = nullptr;

		if (!does_file_exist(aPathToModel))
		{
			//OutputDebugStringA("File not found\n");
			return nullptr;
		}
		scene = aiImportFile(aPathToModel, aiProcessPreset_TargetRealtime_MaxQuality | aiProcess_ConvertToLeftHanded);
		return scene;
	}

	void* CFBXLoaderCustom::LoadModelInternal(CLoaderModel* someInput)
	{
		CLoaderModel* model = someInput;
		const struct aiScene* scene = nullptr;

		if (!does_file_exist(model->myModelPath.c_str()))
		{
			//OutputDebugStringA("File not found\n");
			return nullptr;
		}
		scene = aiImportFile(model->myModelPath.c_str(), aiProcessPreset_TargetRealtime_MaxQuality | aiProcess_ConvertToLeftHanded);

		//OutputDebugStringA((model->myModelPath + "\n").c_str());
		// HACK (commented out "OutputDebugStringA")

		if (!scene)
		{
			//OutputDebugStringA(aiGetErrorString());
			return nullptr;
		}

		model->myScene = scene;


		for (unsigned int n = 0; n < scene->mNumMeshes; ++n)
		{
			CLoaderMesh* mesh = model->CreateMesh();

			aiMesh* fbxMesh = scene->mMeshes[n];

			DetermineAndLoadVerticies(fbxMesh, mesh);

			for (unsigned int i = 0; i < fbxMesh->mNumFaces; i++)
			{
				for (uint j = 0; j < fbxMesh->mFaces[i].mNumIndices; j++)
				{
					mesh->myIndexes.Add(fbxMesh->mFaces[i].mIndices[j]);
				}
			}
		}

		// Change to support multiple animations
		if (scene->mNumAnimations > 0)
		{
			model->myAnimationDuration = (float)scene->mAnimations[0]->mDuration;
		}

		LoadMaterials(scene, model);

		model->myGlobalInverseTransform = ConvertToEngineMatrix44(scene->mRootNode->mTransformation);

		return model;

	}


	void CFBXLoaderCustom::LoadMaterials(const struct aiScene *sc, CLoaderModel* aModel)
	{

		for (unsigned int m = 0; m < sc->mNumMaterials; m++)
		{
			LoadTexture(aiTextureType_DIFFUSE, aModel->myTextures, sc->mMaterials[m]); // TEXTURE_DEFINITION_ALBEDO
			LoadTexture(aiTextureType_SPECULAR, aModel->myTextures, sc->mMaterials[m]); // TEXTURE_DEFINITION_ROUGHNESS
			LoadTexture(aiTextureType_AMBIENT, aModel->myTextures, sc->mMaterials[m]); // TEXTURE_DEFINITION_AMBIENTOCCLUSION
			LoadTexture(aiTextureType_EMISSIVE, aModel->myTextures, sc->mMaterials[m]); // TEXTURE_DEFINITION_EMISSIVE
			LoadTexture(aiTextureType_HEIGHT, aModel->myTextures, sc->mMaterials[m]);
			LoadTexture(aiTextureType_NORMALS, aModel->myTextures, sc->mMaterials[m]); // TEXTURE_DEFINITION_NORMAL
			LoadTexture(aiTextureType_SHININESS, aModel->myTextures, sc->mMaterials[m]);
			LoadTexture(aiTextureType_OPACITY, aModel->myTextures, sc->mMaterials[m]);
			LoadTexture(aiTextureType_DISPLACEMENT, aModel->myTextures, sc->mMaterials[m]);
			LoadTexture(aiTextureType_LIGHTMAP, aModel->myTextures, sc->mMaterials[m]);
			LoadTexture(aiTextureType_REFLECTION, aModel->myTextures, sc->mMaterials[m]); // TEXTURE_DEFINITION_METALNESS
		}
	}

	void CFBXLoaderCustom::LoadTexture(int aType, CU::GrowingArray<std::string, unsigned int>& someTextures, aiMaterial* aMaterial)
	{
		int texIndex = 0;
		aiReturn texFound = AI_SUCCESS;

		aiString path;	// filename

		texFound = aMaterial->GetTexture((aiTextureType)aType, texIndex, &path);
		if (texFound == AI_FAILURE)
		{
			someTextures.Add("");
			return;
		}

		std::string filePath = std::string(path.data);

		const size_t last_slash_idx = filePath.find_last_of("\\/");
		if (std::string::npos != last_slash_idx)
		{
			filePath.erase(0, last_slash_idx + 1);
		}

		someTextures.Add(filePath);
	}

	void CLoaderModel::Destroy()
	{
		for (unsigned int meshIndex = 0; meshIndex < myMeshes.Size(); ++meshIndex)
		{
			sce_delete(myMeshes[meshIndex]->myVerticies);
			sce_delete(myMeshes[meshIndex]);
		}
	}
}
