#pragma once

enum class EVoxelSize
{
	x64,
	x128,
	x256,
};