#pragma once
#include "GI_Declarations.h"
#include "..\EngineCore\EventSystem\EventReceiver.h"
#include "..\CommonUtilities\GrowingArray.h"
/************************************************************************/
/*        This renderer handles the global-illumination by				*/
/*        conetracing in a voxelized scene.                             */
/************************************************************************/

#define __VOXEL_DATABUFFER_SLOT 13

struct ID3D11DeviceContext;
namespace sce { namespace gfx {

	class CCameraInstance;
	class CModelInstance;
	class CDirectionalLight;
	class CSpotLightInstance;
	class CPointLightInstance;
	class CEnvironmentalLight;
	class CDX11Shader;
	class CDX11VertexBuffer;
	class CDX11IndexBuffer;
	class CDX11RenderTargetGI;
	class CModel;
	class CDX11FullscreenTexture;
	class CSpotLight;

	

	class CDeferredRendererGI : public CEventReceiver
	{
	public:
		CDeferredRendererGI();
		~CDeferredRendererGI();

		void Init(const EVoxelSize& aQualityLevel = EVoxelSize::x256);
		void Destroy();

		void BindVoxelRadiance();
		void RenderVoxelScene(SRenderData& aRenderData);
		void DEBUG_RenderVoxelizedScene();
		const bool ShouldRenderVoxels() const { return myShouldRenderVoxels; };
		const bool UseGI() const { return (myVoxelData.useGI == 1); };

		void RenderGUI(SRenderData& aRenderData);

		void ReceiveEvent(const Event::SEvent& aEvent) override;

	private:
		void BindClosestLights(const CU::Vector3f & aPos, SRenderData & aRenderData);
		void DoSecondaryBounce();
		void DoRadianceDecode();
		void ClearNormals();
		void ExecuteEvents();

		struct SInstanceBufferData
		{
			CU::Matrix44f myToWorld;
			float useFog = 1;
			float pad[3];
		};
		struct SInvCameraBufferData
		{
			CU::Matrix44f myInvView;
			CU::Matrix44f myInvProjection;
		};
		struct SBoneBufferData
		{
			unsigned int useAnimation;
			unsigned int crap[3];
			CU::Matrix44f bones[64];
		};
		struct SBoneBufferDataOnlyBool
		{
			unsigned int useAnimation;
			unsigned int crap[3];
		};
		struct SVoxelData
		{
			CU::Vector3f voxelCenter;
			unsigned int centerChangedThisFrame = 0;
			unsigned int voxelGridSize = 256;
			float voxelSize = 0.125f;
			unsigned int useGI = 1;
			unsigned int voxelRadianceMips = 1;
			float coneAngleDiffuse;
			float coneAngleSpecular;
			float voxelSizeInversed;
			float voxelGridSizeInversed;
		};

	private:
		CU::GrowingArray<Event::SEvent*> myEvents;
		SVoxelData myVoxelData;
		CU::Vector3f myCamPosLastFrame;
		float myDirLightMultiplier;
		float myDirectionalLightRotateX;
		float myDirectionalLightRotateY;
		float myDirectionalLightRotateZ;
		unsigned int myVoxelDataBufferID;
		unsigned int myDirectionalLightBufferID;
		unsigned int myCBufferID;
		unsigned int myTimerCBufferID;
		unsigned int myInstanceBonesCBufferID;
		unsigned int myCamInvCBufferID;
		unsigned int mySpotlightCBufferID;
		unsigned int myPointlightCBufferID;
		bool myShouldRenderVoxels;
		bool myShouldDoSecondBounce;
		CDX11RenderTargetGI* myGBuffer;
		ID3D11DeviceContext* myContext;
		CDX11Shader* myVoxelizerVS;
		CDX11Shader* myVoxelizerGS;
		CDX11Shader* myVoxelizerPS;
		CDX11Shader* myVoxelizerCS;
		CDX11Shader* mySecondBounceCS;
		CDX11Shader* myNormalClearCS;

		CDX11Shader* myDebugVoxelVS;
		CDX11Shader* myDebugVoxelGS;
		CDX11Shader* myDebugVoxelPS;
	};

}}