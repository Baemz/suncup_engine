#include "stdafx.h"
#include "CDirectXMathHelper.h"
#include <DirectXMath.h>
#include <DirectXCollision.h>

CU::Matrix44f sce::gfx::CreateProjectionMatrixLH(const float aFov, const float aAspect, const float aNear, const float aFar)
{
	DirectX::XMMATRIX mat = DirectX::XMMatrixPerspectiveFovLH(aFov, aAspect, aNear, aFar);
	DirectX::XMFLOAT4X4 mat2;
	DirectX::XMStoreFloat4x4(&mat2, mat);
	return CU::Matrix44f(
		mat2._11, mat2._12, mat2._13, mat2._14,
		mat2._21, mat2._22, mat2._23, mat2._24,
		mat2._31, mat2._32, mat2._33, mat2._34,
		mat2._41, mat2._42, mat2._43, mat2._44);
}

CU::Matrix44f sce::gfx::CreateOrthogonalMatrixLH(const float aWidth, const float aHeight, const float aNear, const float aFar)
{
	DirectX::XMMATRIX mat = DirectX::XMMatrixOrthographicLH(aWidth, aHeight, aNear, aFar);
	DirectX::XMFLOAT4X4 mat2;
	DirectX::XMStoreFloat4x4(&mat2, mat);
	return CU::Matrix44f(
		mat2._11, mat2._12, mat2._13, mat2._14,
		mat2._21, mat2._22, mat2._23, mat2._24,
		mat2._31, mat2._32, mat2._33, mat2._34,
		mat2._41, mat2._42, mat2._43, mat2._44);
}

CU::Matrix44f sce::gfx::LookAtDX(const CU::Vector3f& aFrom, const CU::Vector3f& aTo, const CU::Vector3f& aWorldUp)
{
	DirectX::XMVECTOR from = DirectX::XMVectorSet(aFrom.x, aFrom.y, aFrom.z, 1.f);
	DirectX::XMVECTOR to = DirectX::XMVectorSet(aTo.x, aTo.y, aTo.z, 1.f);
	DirectX::XMVECTOR up = DirectX::XMVectorSet(aWorldUp.x, aWorldUp.y, aWorldUp.z, 1.f);
	DirectX::XMMATRIX mat = DirectX::XMMatrixLookAtLH(from, to, up);
	DirectX::XMFLOAT4X4 mat2;
	DirectX::XMStoreFloat4x4(&mat2, mat);
	return CU::Matrix44f(
		mat2._11, mat2._12, mat2._13, mat2._14,
		mat2._21, mat2._22, mat2._23, mat2._24,
		mat2._31, mat2._32, mat2._33, mat2._34,
		mat2._41, mat2._42, mat2._43, mat2._44);
}

CU::Matrix44f sce::gfx::MatrixTranslationFromVector(const CU::Vector3f & aPosition)
{
	DirectX::XMFLOAT3 position = DirectX::XMFLOAT3(aPosition.x, aPosition.y, aPosition.z);
	DirectX::XMMATRIX mat = DirectX::XMMatrixTranslationFromVector(DirectX::XMLoadFloat3(&position));

	DirectX::XMFLOAT4X4 mat2;
	DirectX::XMStoreFloat4x4(&mat2, mat);
	return CU::Matrix44f(
		mat2._11, mat2._12, mat2._13, mat2._14,
		mat2._21, mat2._22, mat2._23, mat2._24,
		mat2._31, mat2._32, mat2._33, mat2._34,
		mat2._41, mat2._42, mat2._43, mat2._44);
}


DirectX::XMMATRIX sce::gfx::TransformToDXMatrix(const CU::Matrix44f& aMatrix)
{
	return DirectX::XMMatrixSet(aMatrix[0], aMatrix[1], aMatrix[2], aMatrix[3], aMatrix[4], aMatrix[5], aMatrix[6]
		, aMatrix[7], aMatrix[8], aMatrix[9], aMatrix[10], aMatrix[11], aMatrix[12], aMatrix[13], aMatrix[14], aMatrix[15]);
}

const CU::Matrix44f sce::gfx::InverseDX(const CU::Matrix44f & aMat)
{
	DirectX::XMMATRIX mat = DirectX::XMMatrixSet(aMat[0], aMat[1], aMat[2], aMat[3], aMat[4], aMat[5], aMat[6]
		, aMat[7], aMat[8], aMat[9], aMat[10], aMat[11], aMat[12], aMat[13], aMat[14], aMat[15]);

	DirectX::XMVECTOR det;
	mat = DirectX::XMMatrixInverse(&det, mat);
	DirectX::XMFLOAT4X4 mat2;
	DirectX::XMStoreFloat4x4(&mat2, mat);

	return CU::Matrix44f(
		mat2._11, mat2._12, mat2._13, mat2._14,
		mat2._21, mat2._22, mat2._23, mat2._24,
		mat2._31, mat2._32, mat2._33, mat2._34,
		mat2._41, mat2._42, mat2._43, mat2._44);
}
