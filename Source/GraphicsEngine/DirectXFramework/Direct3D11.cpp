#include "stdafx.h"
#include "Direct3D11.h"

#include <dxgi.h>
#pragma comment(lib, "dxgi.lib")
#pragma comment(lib, "d3d11.lib")

#include "../WindowHandler/WindowHandler.h"
#include "DirectXTK/ScreenGrab.h"

// For debugging of DX
//#define DEBUG_DEVICE

using namespace sce::gfx;

CDirect3D11::CDirect3D11()
	: myShouldRenderWireFrame(false)
	, myUseVSync(false)
	, mySwapChain(nullptr)
	, myDevice(nullptr)
	, myContext(nullptr)
	, myDepthState2D(nullptr)
	, myDepthState(nullptr)
	, myDepthStateParticle(nullptr)
	, myAlphablendState(nullptr)
	, myDisabledBlendState(nullptr)
	, myDefaultSamplerState(nullptr)
	, myWindowHandler(nullptr)
	, myRasterStateNormal(nullptr)
	, myRasterStateWireframe(nullptr)
	, myAdditiveblendState(nullptr)
	, myRasterStateFrontFaceCull(nullptr)
	, myRasterStateNoFaceCull(nullptr)
	, myMin_Mag_Mip_Point_SamplerState(nullptr)
	, myScreenTarget(nullptr)
	, myAnisoMirroredSamplerState(nullptr)
	
{
	ourRenderAPI = eRenderAPI::DirectX11;
}

CDirect3D11::~CDirect3D11()
{
	mySwapChain->Release();
	mySwapChain = nullptr;
	myDevice->Release();
	myDevice = nullptr;
	myContext->Release();
	myContext = nullptr;

	myDepthState2D->Release();
	myDepthState2D = nullptr;
	myDepthState->Release();
	myDepthState = nullptr;
	myDepthStateParticle->Release();
	myDepthStateParticle = nullptr;

	myAlphablendState->Release();
	myAlphablendState = nullptr;
	myDisabledBlendState->Release();
	myDisabledBlendState = nullptr;
	myAdditiveblendState->Release();
	myAdditiveblendState = nullptr;

	myRasterStateNormal->Release();
	myRasterStateNormal = nullptr;
	myRasterStateWireframe->Release();
	myRasterStateWireframe = nullptr;
	myRasterStateFrontFaceCull->Release();
	myRasterStateFrontFaceCull = nullptr;
	myRasterStateNoFaceCull->Release();
	myRasterStateNoFaceCull = nullptr;

	myDefaultSamplerState->Release();
	myDefaultSamplerState = nullptr;
	myMin_Mag_Mip_Point_SamplerState->Release();
	myMin_Mag_Mip_Point_SamplerState = nullptr;
	myAnisoMirroredSamplerState->Release();
	myAnisoMirroredSamplerState = nullptr;

	sce_delete(myScreenTarget);
}

bool CDirect3D11::Init(CWindowHandler& aWindowHandler, const bool aStartInFullscreen, const bool aUseVSync)
{
	myWindowHandler = &aWindowHandler;
	myUseVSync = aUseVSync;
	HRESULT result;

	DXGI_SWAP_CHAIN_DESC swapchainDesc = {};
	swapchainDesc.BufferCount = 1;
	swapchainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	swapchainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	swapchainDesc.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;
	swapchainDesc.OutputWindow = aWindowHandler.GetWindowHandle();
	swapchainDesc.SampleDesc.Count = 1;
	swapchainDesc.SampleDesc.Quality = 0;
	swapchainDesc.Windowed = !aStartInFullscreen;

#ifdef DEBUG_DEVICE
	UINT createFlags = D3D11_CREATE_DEVICE_DEBUG;
#else
	UINT createFlags = 0;
#endif

	result = D3D11CreateDeviceAndSwapChain(
		nullptr, D3D_DRIVER_TYPE_HARDWARE,
		nullptr, 
		createFlags,
		nullptr, 0, D3D11_SDK_VERSION,
		&swapchainDesc, &mySwapChain, &myDevice, nullptr, &myContext);
	if (FAILED(result))
	{
		ENGINE_LOG("DX11-ERROR! Could not create swapchain.");
		return false;
	}

	ID3D11Texture2D* backBufferTexture;
	result = mySwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (void**)&backBufferTexture);
	if (FAILED(result))
	{
		ENGINE_LOG("DX11-ERROR! Could not get swapchain-buffer.");
		return false;
	}

	myScreenTarget = sce_new(CDX11FullscreenTexture());
	if (myScreenTarget->Init(backBufferTexture, false) == false)
	{
		ENGINE_LOG("DX11-ERROR! Could not create CDX11FullscreenTexture.");
		return false;
	}

	if (CreateDepthBuffers() == false)
	{
		ENGINE_LOG("DX11-ERROR! Could not create depthStencilStates.");
		return false;
	}

	SetDefaultDepth();
	CreateAlphablend();
	CreateSamplerState();
	CreateRasterState();
	ourFramework = this;

	ENGINE_LOG("SUCCESS! Sucessfully initialized DirecX11.");
	return true;
}

void CDirect3D11::BeginFrame(float aClearColor[4])
{
	mySwapChain->GetFullscreenState(&mySwapChainIsInFullscreen, nullptr);
	if (mySwapChainIsInFullscreen != static_cast<BOOL>(myWindowHandler->IsFullscreen()))
	{
		myWindowHandler->SetFullscreenState(!!(mySwapChainIsInFullscreen));
	}

	CU::Vector4f clearColor(aClearColor[0], aClearColor[1], aClearColor[2], aClearColor[3]);
	myScreenTarget->ClearTexture(clearColor);
}

void CDirect3D11::EndFrame()
{
	if (myUseVSync)
	{
		mySwapChain->Present(1, 0);
	}
	else
	{
		mySwapChain->Present(0, 0);
	}
}

void sce::gfx::CDirect3D11::TakeScreenshot(const wchar_t* aDestinationPath)
{
	ID3D11Texture2D* backBufferTexture;
	HRESULT result = mySwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (void**)&backBufferTexture);
	if (FAILED(result))
	{
		ENGINE_LOG("DX11-ERROR! Could not get swapchain-buffer.");
		return;
	}

	if (backBufferTexture == nullptr)
	{
		return;
	}

	if (backBufferTexture == nullptr)
	{
		return;
	}
	DirectX::SaveDDSTextureToFile(myContext, backBufferTexture, aDestinationPath);
}

void sce::gfx::CDirect3D11::ActivateScreenTarget()
{
	myScreenTarget->SetAsActiveTarget();
}

void sce::gfx::CDirect3D11::ToggleFullscreen()
{
	mySwapChain->SetFullscreenState(static_cast<BOOL>(!myWindowHandler->IsFullscreen()), nullptr);
	myWindowHandler->SetFullscreenState(!myWindowHandler->IsFullscreen());
}

void sce::gfx::CDirect3D11::SetResolution(const unsigned int aWidth, const unsigned int aHeight)
{
	sce_delete(myScreenTarget);
	ID3D11RenderTargetView* nullView[] = { nullptr };
	myContext->OMSetRenderTargets(ARRAYSIZE(nullView), nullView, nullptr);
	myContext->RSSetViewports(0, nullptr);
	myContext->ClearState();
	myContext->Flush();

	DXGI_SWAP_CHAIN_DESC swapchainDesc = {};
	swapchainDesc.BufferCount = 1;
	swapchainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	swapchainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	swapchainDesc.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;
	swapchainDesc.OutputWindow = myWindowHandler->GetWindowHandle();
	swapchainDesc.SampleDesc.Count = 1;
	swapchainDesc.SampleDesc.Quality = 0;
	swapchainDesc.Windowed = !myWindowHandler->IsFullscreen();

	IDXGIDevice* dxgiDevicePtr(nullptr);
	HRESULT result = myDevice->QueryInterface(__uuidof(IDXGIDevice), (void**)&dxgiDevicePtr);
	if (FAILED(result))
	{
		ENGINE_LOG("DX11-ERROR! Couldn't query DXGI device while updating the resolution.");
		return;
	}

	IDXGIAdapter* dxgiAdapterPtr(nullptr);
	result = dxgiDevicePtr->GetParent(__uuidof(IDXGIAdapter), (void**)&dxgiAdapterPtr);
	if (FAILED(result))
	{
		ENGINE_LOG("DX11-ERROR! Couldn't get DXGI adapter while updating the resolution.");
		return;
	}

	IDXGIFactory* dxgiFactoryPtr(nullptr);
	result = dxgiAdapterPtr->GetParent(__uuidof(IDXGIFactory), (void**)&dxgiFactoryPtr);
	if (FAILED(result))
	{
		ENGINE_LOG("DX11-ERROR! Couldn't get DXGI factory while updating the resolution.");
		return;
	}

	SAFE_RELEASE(mySwapChain);
	result = dxgiFactoryPtr->CreateSwapChain(myDevice, &swapchainDesc, &mySwapChain);
	if (FAILED(result))
	{
		ENGINE_LOG("DX11-ERROR! Couldn't create swapchain while updating the resolution.");
		return;
	}
	dxgiFactoryPtr->Release();
	dxgiAdapterPtr->Release();
	dxgiDevicePtr->Release();

	result = mySwapChain->ResizeBuffers(1, aWidth, aHeight, DXGI_FORMAT_R8G8B8A8_UNORM, 0);
	if (FAILED(result))
	{
		ENGINE_LOG("DX11-ERROR! Couldn't resize backbuffer while updating the resolution.");
	}

	ID3D11Texture2D* backBufferTexture;
	result = mySwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (void**)&backBufferTexture);
	if (FAILED(result))
	{
		ENGINE_LOG("DX11-ERROR! Could not get swapchain-buffer while updating the resolution.");
		return;
	}

	myScreenTarget = sce_new(CDX11FullscreenTexture());
	if (myScreenTarget->Init(backBufferTexture, false) == false)
	{
		ENGINE_LOG("DX11-ERROR! Could not create CDX11FullscreenTexture while updating the resolution.");
		return;
	}
	backBufferTexture->Release();
	myScreenTarget->SetAsActiveTarget();
}

void sce::gfx::CDirect3D11::SetVsync(const bool aUseVsync)
{
	myUseVSync = aUseVsync;
}

const bool sce::gfx::CDirect3D11::IsVsyncEnabled() const
{
	return myUseVSync;
}

void sce::gfx::CDirect3D11::SetDefaultDepth()
{
	myContext->OMSetDepthStencilState(myDepthState, 0);
}

void sce::gfx::CDirect3D11::DisableDepth()
{
	myContext->OMSetDepthStencilState(myDepthState2D, 0);
}

void sce::gfx::CDirect3D11::EnableReadOnlyDepth()
{
	myContext->OMSetDepthStencilState(myDepthStateParticle, 0);
}

void sce::gfx::CDirect3D11::CreateAlphablend()
{
	D3D11_BLEND_DESC blendDesc = {};
	blendDesc.RenderTarget[0].BlendEnable = true;
	blendDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
	blendDesc.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
	blendDesc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
	blendDesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ZERO;
	blendDesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ONE;
	blendDesc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_MAX;
	blendDesc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;

	HRESULT result = myDevice->CreateBlendState(&blendDesc, &myAlphablendState);
	if (FAILED(result))
	{
		ENGINE_LOG("ERROR! Could not create blendstate.");
		return;
	}

	blendDesc.RenderTarget[0].BlendEnable = false;
	result = myDevice->CreateBlendState(&blendDesc, &myDisabledBlendState);
	if (FAILED(result))
	{
		ENGINE_LOG("ERROR! Could not create blendstate.");
		return;
	}

	blendDesc.RenderTarget[0].BlendEnable = true;
	blendDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
	blendDesc.RenderTarget[0].DestBlend = D3D11_BLEND_ONE;
	blendDesc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
	blendDesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
	blendDesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ONE;
	blendDesc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_MAX;
	blendDesc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
	result = myDevice->CreateBlendState(&blendDesc, &myAdditiveblendState);
	if (FAILED(result))
	{
		ENGINE_LOG("ERROR! Could not create blendstate.");
		return;
	}
}

bool sce::gfx::CDirect3D11::CreateSamplerState()
{
	D3D11_SAMPLER_DESC samplerDesc; 
	samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;
	samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;
	samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;
	samplerDesc.MipLODBias = 0.0f;
	samplerDesc.MaxAnisotropy = 1;
	samplerDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
	samplerDesc.BorderColor[0] = 0;
	samplerDesc.BorderColor[1] = 0;
	samplerDesc.BorderColor[2] = 0;
	samplerDesc.BorderColor[3] = 0;
	samplerDesc.MinLOD = -FLT_MAX;
	samplerDesc.MaxLOD = FLT_MAX;

	HRESULT result = myDevice->CreateSamplerState(&samplerDesc, &myDefaultSamplerState);
	if (FAILED(result))
	{
		ENGINE_LOG("[ENGINE] ERROR! Could not create SamplerState.");
		return false;
	}

	samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_POINT;
	result = myDevice->CreateSamplerState(&samplerDesc, &myMin_Mag_Mip_Point_SamplerState);
	if (FAILED(result))
	{
		ENGINE_LOG("[ENGINE] ERROR! Could not create SamplerState.");
		return false;
	}

	samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.Filter = D3D11_FILTER_ANISOTROPIC;
	samplerDesc.MaxAnisotropy = 16;
	result = myDevice->CreateSamplerState(&samplerDesc, &myAnisoSamplerState);
	if (FAILED(result))
	{
		ENGINE_LOG("[ENGINE] ERROR! Could not create SamplerState.");
		return false;
	}

	samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_MIRROR;
	samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_MIRROR;
	samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_MIRROR;
	result = myDevice->CreateSamplerState(&samplerDesc, &myAnisoMirroredSamplerState);
	if (FAILED(result))
	{
		ENGINE_LOG("[ENGINE] ERROR! Could not create SamplerState.");
		return false;
	}

	SetSamplerState(ESamplerState::Default);
	return true;
}

bool sce::gfx::CDirect3D11::CreateRasterState()
{
	D3D11_RASTERIZER_DESC normalRasterDesc;

	normalRasterDesc.FillMode = D3D11_FILL_SOLID;
	normalRasterDesc.CullMode = D3D11_CULL_BACK;
	normalRasterDesc.FrontCounterClockwise = FALSE;
	normalRasterDesc.DepthBias = 0;
	normalRasterDesc.SlopeScaledDepthBias = 0.0f;
	normalRasterDesc.DepthBiasClamp = 0.0f;
	normalRasterDesc.DepthClipEnable = TRUE;
	normalRasterDesc.ScissorEnable = FALSE;
	normalRasterDesc.MultisampleEnable = FALSE;
	normalRasterDesc.AntialiasedLineEnable = true;

	HRESULT result = myDevice->CreateRasterizerState(&normalRasterDesc, &myRasterStateNormal);

	if (FAILED(result))
	{
		ENGINE_LOG("[ENGINE] ERROR! Could not create RasterStateNormal.");
		return false;
	}

	D3D11_RASTERIZER_DESC wireframeRasterDesc(normalRasterDesc);

	wireframeRasterDesc.FillMode = D3D11_FILL_WIREFRAME;
	wireframeRasterDesc.CullMode = D3D11_CULL_NONE;

	result = myDevice->CreateRasterizerState(&wireframeRasterDesc, &myRasterStateWireframe);

	if (FAILED(result))
	{
		ENGINE_LOG("[ENGINE] ERROR! Could not create RasterStateWireframe.");
		return false;
	}

	// FrontFace-culling
	wireframeRasterDesc.FillMode = D3D11_FILL_SOLID;
	wireframeRasterDesc.CullMode = D3D11_CULL_FRONT;

	result = myDevice->CreateRasterizerState(&wireframeRasterDesc, &myRasterStateFrontFaceCull);

	if (FAILED(result))
	{
		ENGINE_LOG("[ENGINE] ERROR! Could not create RasterStateFrontFaceCull.");
		return false;
	}

	// No Face-culling
	wireframeRasterDesc.FillMode = D3D11_FILL_SOLID;
	wireframeRasterDesc.CullMode = D3D11_CULL_NONE;
	result = myDevice->CreateRasterizerState(&wireframeRasterDesc, &myRasterStateNoFaceCull);

	if (FAILED(result))
	{
		ENGINE_LOG("[ENGINE] ERROR! Could not create RasterStateNoFaceCull.");
		return false;
	}

	normalRasterDesc.DepthBias = 1000;
	normalRasterDesc.DepthBiasClamp = 0.0f;
	normalRasterDesc.SlopeScaledDepthBias = 1.f;


	result = myDevice->CreateRasterizerState(&normalRasterDesc, &myRasterStateSlopedBias);

	if (FAILED(result))
	{
		ENGINE_LOG("[ENGINE] ERROR! Could not create RasterStateSlopedBias.");
		return false;
	}


	myContext->RSSetState(myRasterStateWireframe);

	return true;
}

void sce::gfx::CDirect3D11::EnableAlphablend()
{
	float blendFactor[4];
	blendFactor[0] = 0.0f;
	blendFactor[1] = 0.0f;
	blendFactor[2] = 0.0f;
	blendFactor[3] = 0.0f;

	myContext->OMSetBlendState(myAlphablendState, blendFactor, 0xffffffff);
}

void sce::gfx::CDirect3D11::EnableAdditiveblend()
{
	float blendFactor[4];
	blendFactor[0] = 0.0f;
	blendFactor[1] = 0.0f;
	blendFactor[2] = 0.0f;
	blendFactor[3] = 0.0f;

	myContext->OMSetBlendState(myAdditiveblendState, blendFactor, 0xffffffff);
}

void sce::gfx::CDirect3D11::DisableBlending()
{
	myContext->OMSetBlendState(myDisabledBlendState, 0, 0xffffffff);
}

void sce::gfx::CDirect3D11::SetSamplerState(const ESamplerState& aState, const unsigned int aSlot)
{
	switch (aState)
	{
	case ESamplerState::Min_Mag_Mip_Point:

		myContext->PSSetSamplers(aSlot, 1, &myMin_Mag_Mip_Point_SamplerState);
		break;

	case ESamplerState::Anisotropic_x16:

		myContext->PSSetSamplers(aSlot, 1, &myAnisoSamplerState);
		break;

	case ESamplerState::Anisotropic_x16_Mirrored:

		myContext->PSSetSamplers(aSlot, 1, &myAnisoMirroredSamplerState);
		break;

	case ESamplerState::Default:
	default:
		myContext->PSSetSamplers(aSlot, 1, &myDefaultSamplerState);
		break;
	}
}

void sce::gfx::CDirect3D11::SetShouldRenderWireFrame()
{
	myShouldRenderWireFrame = !myShouldRenderWireFrame;
}

const bool sce::gfx::CDirect3D11::GetShouldRenderWireFrame() const
{
	return myShouldRenderWireFrame;
}

void sce::gfx::CDirect3D11::SetRasterStateWireframe(const bool aState) const
{
	if (aState)
	{
		myContext->RSSetState(myRasterStateWireframe);
	}
	else
	{
		myContext->RSSetState(myRasterStateNormal);
	}
}

void sce::gfx::CDirect3D11::EnableFrontFaceCulling()
{
	myContext->RSSetState(myRasterStateFrontFaceCull);
}

void sce::gfx::CDirect3D11::DisableFaceCulling()
{
	myContext->RSSetState(myRasterStateNoFaceCull);
}

void sce::gfx::CDirect3D11::SetDefaultFaceCulling()
{
	myContext->RSSetState(myRasterStateNormal);
}

void sce::gfx::CDirect3D11::EnableSlopedDepthBias()
{
	myContext->RSSetState(myRasterStateSlopedBias);
}

void sce::gfx::CDirect3D11::SetDefaultRasterState()
{
	myContext->RSSetState(myRasterStateNormal);
}

bool sce::gfx::CDirect3D11::CreateDepthBuffers()
{
	D3D11_DEPTH_STENCIL_DESC depthStencilDesc2D;
	ZeroMemory(&depthStencilDesc2D, sizeof(depthStencilDesc2D));

	depthStencilDesc2D.DepthEnable = false;
	depthStencilDesc2D.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
	depthStencilDesc2D.DepthFunc = D3D11_COMPARISON_LESS_EQUAL;
	depthStencilDesc2D.StencilEnable = true;
	depthStencilDesc2D.StencilReadMask = 0xFF;
	depthStencilDesc2D.StencilWriteMask = 0xFF;
	depthStencilDesc2D.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	depthStencilDesc2D.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
	depthStencilDesc2D.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	depthStencilDesc2D.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	depthStencilDesc2D.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	depthStencilDesc2D.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
	depthStencilDesc2D.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	depthStencilDesc2D.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

	HRESULT result = myDevice->CreateDepthStencilState(&depthStencilDesc2D, &myDepthState2D);
	if (FAILED(result))
	{
		ENGINE_LOG("[ENGINE] ERROR! Could not create 2D-DepthState.");
		return false;
	}

	D3D11_DEPTH_STENCIL_DESC depthStencilDesc;
	ZeroMemory(&depthStencilDesc, sizeof(depthStencilDesc));

	depthStencilDesc.DepthEnable = true;
	depthStencilDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
	depthStencilDesc.DepthFunc = D3D11_COMPARISON_LESS_EQUAL;
	depthStencilDesc.StencilEnable = true;
	depthStencilDesc.StencilReadMask = 0xFF;
	depthStencilDesc.StencilWriteMask = 0xFF;
	depthStencilDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	depthStencilDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
	depthStencilDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	depthStencilDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	depthStencilDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	depthStencilDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
	depthStencilDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	depthStencilDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

	result = myDevice->CreateDepthStencilState(&depthStencilDesc, &myDepthState);
	if (FAILED(result))
	{
		ENGINE_LOG("[ENGINE] ERROR! Could not create 2D-DepthState.");
		return false;
	}

	D3D11_DEPTH_STENCIL_DESC pdepthStencilDesc;
	ZeroMemory(&pdepthStencilDesc, sizeof(pdepthStencilDesc));

	pdepthStencilDesc.DepthEnable = true;
	pdepthStencilDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ZERO;
	pdepthStencilDesc.DepthFunc = D3D11_COMPARISON_LESS_EQUAL;
	pdepthStencilDesc.StencilEnable = true;
	pdepthStencilDesc.StencilReadMask = 0xFF;
	pdepthStencilDesc.StencilWriteMask = 0xFF;
	pdepthStencilDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	pdepthStencilDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
	pdepthStencilDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	pdepthStencilDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	pdepthStencilDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	pdepthStencilDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
	pdepthStencilDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	pdepthStencilDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

	result = myDevice->CreateDepthStencilState(&pdepthStencilDesc, &myDepthStateParticle);
	if (FAILED(result))
	{
		ENGINE_LOG("[ENGINE] ERROR! Could not create 2D-DepthState.");
		return false;
	}

	return true;
}
