#pragma once
#include "..\CommonUtilities\Matrix44.h"

namespace DirectX
{
	struct XMMATRIX;
}

namespace sce { namespace gfx {

	CU::Matrix44f CreateProjectionMatrixLH(const float aFov, const float aAspect, const float aNear, const float aFar);
	CU::Matrix44f CreateOrthogonalMatrixLH(const float aWidth, const float aHeight, const float aNear, const float aFar);

	CU::Matrix44f LookAtDX(const CU::Vector3f& aFrom, const CU::Vector3f& aTo, const CU::Vector3f& aWorldUp);
	CU::Matrix44f MatrixTranslationFromVector(const CU::Vector3f& aPosition);

	DirectX::XMMATRIX TransformToDXMatrix(const CU::Matrix44f& aMatrix);

	const CU::Matrix44f InverseDX(const CU::Matrix44f& aMat);
}}