#pragma once

struct ID3D11Buffer;
struct ID3D11Device;
struct ID3D11DeviceContext;

namespace sce { namespace gfx {

	class CDX11ConstantBuffer
	{
		friend class CFullscreenRenderer;
		friend class CMaterial;

	public:
		CDX11ConstantBuffer();
		~CDX11ConstantBuffer();

		
		template<class T> void SetData(T& aData);

		void BindVS(unsigned char aRegisterIndex = 0);
		void BindGS(unsigned char aRegisterIndex = 0);
		void BindPS(unsigned char aRegisterIndex = 0);
		void BindCS(unsigned char aRegisterIndex = 0);

		static void PrintCount();

		const bool IsLoaded() const;
	private:
		ID3D11Buffer* myCBuffer;
		ID3D11Device* myDevice;
		ID3D11DeviceContext* myContext;
		unsigned int mySize;
		volatile bool myIsLoaded;

		void CreateOrUpdateBuffer(unsigned int aSize, void* someData);
	};

	template<class T>
	inline void CDX11ConstantBuffer::SetData(T& aData)
	{
		mySize = static_cast<unsigned int>(sizeof(T));
		CreateOrUpdateBuffer(mySize, &aData);
	}
}}