#pragma once
#include <string>

struct ID3D11ShaderResourceView;
struct ID3D11Device;
struct ID3D11DeviceContext;
namespace sce { namespace gfx {

	enum class ETextureType : unsigned int
	{
		Diffuse = 0,
		NormalMap,
		Roughness,
		Metallic,
		Emissive,
		AmbientOcclusion,
		Skymap,
		EnvironmentLight,
		LUT,
		ShaderEffect,
		SSAORand,
	};

	class CDX11Texture
	{
		friend class CModelFactory;
		friend class CMaterial;
		friend class CFullscreenRenderer;
		friend class CSpriteFactory;
		friend class CParticleFactory;
		friend class CLightFactory;
		friend class CShaderEffectFactory;
	public:
		CDX11Texture();
		~CDX11Texture();
		
		static void CreateDefaultTextures();
		static void DestroyDefaultTextures();

		void Bind();
		void Bind(const unsigned int aSlot);
		const CU::Vector2f GetTextureSize();
		const unsigned int GetMipCount() const { return myTextureMips; };

	private:
		static ID3D11ShaderResourceView* ourDefaultAlbedo;
		static ID3D11ShaderResourceView* ourDefaultBlack;
		static ID3D11ShaderResourceView* ourDefaultWhite;
		static ID3D11ShaderResourceView* ourDefaultNormal;
		static CU::Vector2f ourDefaultAlbedoTextureSize;
		static CU::Vector2f ourDefaultBlackTextureSize;
		static CU::Vector2f ourDefaultWhiteTextureSize;
		static CU::Vector2f ourDefaultNormalTextureSize;

		CU::Vector2f myTextureSize;
		unsigned int myTextureSlot;
		unsigned int myTextureMips;
		std::wstring myName;
		ID3D11ShaderResourceView* myTexture;
		ID3D11Device* myDevice;
		ID3D11DeviceContext* myContext;

		volatile bool myIsLoaded;

		const void SetTextureSize();
		bool CreateTexture(const wchar_t* aFilePath, const ETextureType aType);
		bool CreateTexture(const wchar_t* aFilePath, const unsigned int aSlot);
		bool CreateTexture(const char * aFilePath, const ETextureType aType);
		bool CreateTexture(const char * aFilePath, const unsigned int aSlot);
	};
}}