#include "stdafx.h"
#include "DX11VertexBuffer.h"
#include "../Direct3D11.h"

using namespace sce::gfx;

CDX11VertexBuffer::CDX11VertexBuffer(const EBufferUsage& aUsage)
	: CVertexBuffer()
	, myBuffer(nullptr)
	, mySize(0)
	, myMaxSize(0)
	, myVertexCount(0)
	, myUsage(aUsage)
{
	myDevice = CDirect3D11::GetAPI()->GetDevice();
	myContext = CDirect3D11::GetAPI()->GetContext();
}


CDX11VertexBuffer::~CDX11VertexBuffer()
{
	SAFE_RELEASE(myBuffer);
	myDevice = nullptr;
	myContext = nullptr;
}

void CDX11VertexBuffer::Bind()
{
	myContext->IASetVertexBuffers(0, 1, &myBuffer, &myStride, &myOffset);
}

void sce::gfx::CDX11VertexBuffer::Bind(CDX11VertexBuffer* aInstanceBuffer)
{
	unsigned int strides[2]; 
	strides[0] = myStride;
	strides[1] = aInstanceBuffer->myStride;

	unsigned int offsets[2];
	offsets[0] = 0;
	offsets[1] = 0;

	ID3D11Buffer* bufferPointers[2];
	bufferPointers[0] = myBuffer;
	bufferPointers[1] = aInstanceBuffer->myBuffer;

	myContext->IASetVertexBuffers(0, 2, bufferPointers, strides, offsets);
}

void sce::gfx::CDX11VertexBuffer::SetData(void* aData)
{
	if (aData == nullptr)
	{
		return;
	}
	if (myUsage == EBufferUsage::STATIC)
	{
		ENGINE_LOG("WARNING! Trying to change data on static vertex-buffer.");
		return;
	}

	D3D11_MAPPED_SUBRESOURCE subData = {};
	HRESULT result = myContext->Map(myBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &subData);
	if (FAILED(result))
	{
		ENGINE_LOG("ERROR! Cannot change data on vertex-buffer.");
	}
	memcpy(subData.pData, aData, static_cast<size_t>(mySize));
	myContext->Unmap(myBuffer, 0);
}

void sce::gfx::CDX11VertexBuffer::SetData(void* aData, unsigned int aSize)
{
	if (aData == nullptr)
	{
		return;
	}
	if (aSize > myMaxSize)
	{
		SAFE_RELEASE(myBuffer);
		CreateBuffer(myStride, aSize / myStride);
	}
	else
	{
		mySize = aSize;
	}
	SetData(aData);
}

void sce::gfx::CDX11VertexBuffer::CreateBuffer(const unsigned int aStrideSize, const unsigned int aCount, const EBufferUsage& aUsage)
{
	myUsage = aUsage;
	myVertexCount = aCount;
	mySize = aStrideSize * myVertexCount;
	myMaxSize = mySize;
	myStride = aStrideSize;
	myOffset = 0;

	D3D11_BUFFER_DESC bufferDesc = {};
	bufferDesc.ByteWidth = static_cast<unsigned int>(mySize);
	bufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	if (myUsage == EBufferUsage::DYNAMIC)
	{
		bufferDesc.Usage = D3D11_USAGE_DYNAMIC;
		bufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	}
	else
	{
		bufferDesc.Usage = D3D11_USAGE_IMMUTABLE;
	}


	HRESULT result = myDevice->CreateBuffer(&bufferDesc, nullptr, &myBuffer);
	if (FAILED(result))
	{
		RESOURCE_LOG("ERROR! Could not create VBuffer");
		return;
	}
}

void sce::gfx::CDX11VertexBuffer::CreateBuffer(char* aVertexList, unsigned int aListSize, int aVertexCount, const EBufferUsage& aUsage)
{
	myUsage = aUsage;
	myVertexCount = aVertexCount;
	mySize = aListSize * aVertexCount;
	myMaxSize = mySize;
	myStride = sizeof(SModelVertex);
	myOffset = 0;

	D3D11_BUFFER_DESC bufferDesc = {};
	bufferDesc.ByteWidth = static_cast<unsigned int>(mySize);
	bufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	if (myUsage == EBufferUsage::DYNAMIC)
	{
		bufferDesc.Usage = D3D11_USAGE_DYNAMIC;
		bufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	}
	else
	{
		bufferDesc.Usage = D3D11_USAGE_IMMUTABLE;
	}

	D3D11_SUBRESOURCE_DATA bufferData = {};
	bufferData.pSysMem = aVertexList;

	HRESULT result = myDevice->CreateBuffer(&bufferDesc, &bufferData, &myBuffer);
	if (FAILED(result))
	{
		RESOURCE_LOG("ERROR! Could not create VBuffer");
		return;
	}
}

void sce::gfx::CDX11VertexBuffer::CreateParticleBuffer(float aParticleLifeTime, float aParticleSpawnRate, const EBufferUsage& aUsage)
{
	float size = aParticleLifeTime * aParticleSpawnRate * sizeof(SParticle);
	if (size < 5.f)
	{
		mySize = 5;
	}
	else
	{
		mySize = static_cast<unsigned int>(std::ceil(size));
	}
	myMaxSize = mySize;

	myUsage = aUsage;
	myStride = sizeof(SParticle);
	myOffset = 0;

	D3D11_BUFFER_DESC bufferDesc = {};
	bufferDesc.ByteWidth = static_cast<unsigned int>(mySize);
	bufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	if (myUsage == EBufferUsage::DYNAMIC)
	{
		bufferDesc.Usage = D3D11_USAGE_DYNAMIC;
		bufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	}
	else
	{
		bufferDesc.Usage = D3D11_USAGE_IMMUTABLE;
	}

	HRESULT result = myDevice->CreateBuffer(&bufferDesc, nullptr, &myBuffer);
	if (FAILED(result))
	{
		RESOURCE_LOG("ERROR! Could not create VBuffer");
		return;
	}
}

bool sce::gfx::CDX11VertexBuffer::CreateDebugBuffer(unsigned int aSize, const EBufferUsage & aUsage)
{
	myStride = sizeof(SSimpleDebugVertex);
	myVertexCount = aSize;
	mySize = sizeof(SSimpleDebugVertex) * aSize;
	myMaxSize = mySize;
	myOffset = 0;
	myUsage = aUsage;

	D3D11_BUFFER_DESC bufferDesc = {};
	bufferDesc.ByteWidth = sizeof(SSimpleDebugVertex) * aSize;
	bufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	if (myUsage == EBufferUsage::DYNAMIC)
	{
		bufferDesc.Usage = D3D11_USAGE_DYNAMIC;
		bufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	}
	else
	{
		bufferDesc.Usage = D3D11_USAGE_IMMUTABLE;
	}

	HRESULT result = myDevice->CreateBuffer(&bufferDesc, nullptr, &myBuffer);
	if (FAILED(result))
	{
		RESOURCE_LOG("ERROR! Debug could not create VBuffer");
		return false;
	}
	return true;
}

bool CDX11VertexBuffer::CreatePrimitiveTriangleBuffer()
{
	SSimpleVertex vertices[3] = 
	{
		{ -0.8f, -0.8f, 0, 1, 0, 1, 1, 1 },
		{ 0.0f, 0.8f, 0, 1, 1, 0, 1, 1 },
		{ 0.8f, -0.8f, 0, 1, 1, 1, 0, 1 }
	};

	myStride = sizeof(SSimpleVertex);
	myVertexCount = sizeof(vertices) / sizeof(SSimpleVertex);
	myOffset = 0;

	D3D11_BUFFER_DESC bufferDesc = {};
	bufferDesc.ByteWidth = sizeof(vertices); 
	bufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	if (myUsage == EBufferUsage::DYNAMIC)
	{
		bufferDesc.Usage = D3D11_USAGE_DYNAMIC;
		bufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	}
	else
	{
		bufferDesc.Usage = D3D11_USAGE_IMMUTABLE;
	}

	D3D11_SUBRESOURCE_DATA bufferData = {};
	bufferData.pSysMem = vertices;

	HRESULT result = myDevice->CreateBuffer(&bufferDesc, &bufferData, &myBuffer);
	if (FAILED(result))
	{
		RESOURCE_LOG("ERROR! Triangle could not create VBuffer");
		return false;
	}
	return true;
}

bool sce::gfx::CDX11VertexBuffer::CreateCube(const EBufferUsage& aUsage)
{
	SSimpleVertex vertices[8] =
	{
		{ 0.5f, 0.5f, 0.5f, 0.5f, 1.0f, 1.0f, 1.0f, 1.0f },
		{ 0.5f, 0.5f, -0.5f, 0.5f, 1.0f, 1.0f, 0.f, 1.0f },
		{ 0.5f, -0.5f, 0.5f, 0.5f, 1.0f, 0.f, 1.0f, 1.0f },
		{ -0.5f, 0.5f, 0.5f, 0.5f, 0.f, 1.0f, 1.0f, 1.0f },
		{ 0.5f, -0.5f, -0.5f, 0.5f, 1.0f, 0.f, 0.f, 1.0f },
		{ -0.5f, 0.5f, -0.5f, 0.5f, 0.f, 1.0f, 0.f, 1.0f },
		{ -0.5f, -0.5f, 0.5f, 0.5f, 0.f, 0.f, 1.0f, 1.0f },
		{ -0.5f, -0.5f, -0.5f, 0.5f, 0.f, 0.f, 0.f, 1.0f }
	};

	myUsage = aUsage;
	myStride = sizeof(SSimpleVertex);
	myVertexCount = sizeof(vertices) / sizeof(SSimpleVertex);
	myOffset = 0;

	D3D11_BUFFER_DESC bufferDesc = {};
	bufferDesc.ByteWidth = sizeof(vertices);
	bufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER; 
	if (myUsage == EBufferUsage::DYNAMIC)
	{
		bufferDesc.Usage = D3D11_USAGE_DYNAMIC;
		bufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	}
	else
	{
		bufferDesc.Usage = D3D11_USAGE_IMMUTABLE;
	}

	D3D11_SUBRESOURCE_DATA bufferData = {};
	bufferData.pSysMem = vertices;

	HRESULT result = myDevice->CreateBuffer(&bufferDesc, &bufferData, &myBuffer);
	if (FAILED(result))
	{
		RESOURCE_LOG("ERROR! Cube could not create VBuffer.");
		return false;
	}
	return true;
}

bool sce::gfx::CDX11VertexBuffer::CreateCube(float aScale, const EBufferUsage& aUsage)
{
	SSimpleVertex vertices[8] =
	{
		{  0.5f * aScale,  0.5f * aScale,  0.5f * aScale, 1.f, 1.f, 1.f, 1.f, 1.f },
		{  0.5f * aScale,  0.5f * aScale, -0.5f * aScale, 1.f, 1.f, 1.f, 0.f, 1.f },
		{  0.5f * aScale, -0.5f * aScale,  0.5f * aScale, 1.f, 1.f, 0.f, 1.f, 1.f },
		{ -0.5f * aScale,  0.5f * aScale,  0.5f * aScale, 1.f, 0.f, 1.f, 1.f, 1.f },
		{  0.5f * aScale, -0.5f * aScale, -0.5f * aScale, 1.f, 1.f, 0.f, 0.f, 1.f },
		{ -0.5f * aScale,  0.5f * aScale, -0.5f * aScale, 1.f, 0.f, 1.f, 0.f, 1.f },
		{ -0.5f * aScale, -0.5f * aScale,  0.5f * aScale, 1.f, 0.f, 0.f, 1.f, 1.f },
		{ -0.5f * aScale, -0.5f * aScale, -0.5f * aScale, 1.f, 0.f, 0.f, 0.f, 1.f }
	};

	myUsage = aUsage;
	myStride = sizeof(SSimpleVertex);
	myVertexCount = sizeof(vertices) / sizeof(SSimpleVertex);
	myOffset = 0;

	D3D11_BUFFER_DESC bufferDesc = {};
	bufferDesc.ByteWidth = sizeof(vertices);
	bufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	if (myUsage == EBufferUsage::DYNAMIC)
	{
		bufferDesc.Usage = D3D11_USAGE_DYNAMIC;
		bufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	}
	else
	{
		bufferDesc.Usage = D3D11_USAGE_IMMUTABLE;
	}

	D3D11_SUBRESOURCE_DATA bufferData = {};
	bufferData.pSysMem = vertices;

	HRESULT result = myDevice->CreateBuffer(&bufferDesc, &bufferData, &myBuffer);
	if (FAILED(result))
	{
		RESOURCE_LOG("ERROR! Cube could not create VBuffer.");
		return false;
	}
	return true;
}

bool sce::gfx::CDX11VertexBuffer::CreateSphere(CU::GrowingArray<unsigned int, unsigned int>& aIndexList, const CU::Vector3f& aColor, const float aRadius, const unsigned int aRingCount, const unsigned int aSliceCount,
	const EBufferUsage& aUsage)
{
	const unsigned int vertexCount((aRingCount * aSliceCount) + 2);

	SSimpleVertex* vertices = sce_newArray(SSimpleVertex, vertexCount);
	if (vertices == nullptr)
	{
		RESOURCE_LOG("[CDX11VertexBuffer] ERROR! Failed to allocate memory for the vertex list.");
		return false;
	}

	GetSphereVertices(vertices, aColor, vertexCount, aRingCount, aSliceCount, aRadius);
	GetSphereIndices(aIndexList, vertexCount, aRingCount, aSliceCount);
	
	myUsage = aUsage;
	myStride = sizeof(SSimpleVertex);
	myVertexCount = vertexCount;
	mySize = sizeof(SSimpleVertex) * myVertexCount;
	myMaxSize = mySize;
	myOffset = 0;

	D3D11_BUFFER_DESC bufferDesc = {};
	bufferDesc.ByteWidth = sizeof(SSimpleVertex) * myVertexCount;
	bufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;

	if (myUsage == EBufferUsage::DYNAMIC)
	{
		bufferDesc.Usage = D3D11_USAGE_DYNAMIC;
		bufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	}
	else
	{
		bufferDesc.Usage = D3D11_USAGE_IMMUTABLE;
	}

	D3D11_SUBRESOURCE_DATA bufferData = {};
	bufferData.pSysMem = (void*)vertices;

	HRESULT result = myDevice->CreateBuffer(&bufferDesc, &bufferData, &myBuffer);
	sce_delete(vertices);

	if (FAILED(result))
	{
		RESOURCE_LOG("ERROR! Sphere could not create VBuffer.");
		return false;
	}

	return true;
}

bool sce::gfx::CDX11VertexBuffer::CreateTexturedCube(const EBufferUsage& aUsage)
{
	SSimpleTexturedVertex vertices[8] =
	{
		{ 0.5f, 0.5f, 0.5f, 0.5f,		1.f, 0.f },
		{ 0.5f, 0.5f, -0.5f, 0.5f,		1.f, 0.f },
		{ 0.5f, -0.5f, 0.5f, 0.5f,		1.f, 1.f },
		{ -0.5f, 0.5f, 0.5f, 0.5f,		0.f, 0.f },
		{ 0.5f, -0.5f, -0.5f, 0.5f,		1.f, 1.f },
		{ -0.5f, 0.5f, -0.5f, 0.5f,		0.f, 0.f },
		{ -0.5f, -0.5f, 0.5f, 0.5f,		0.f, 1.f },
		{ -0.5f, -0.5f, -0.5f, 0.5f,	0.f, 1.f }
	};

	myUsage = aUsage;
	myStride = sizeof(SSimpleTexturedVertex);
	myVertexCount = sizeof(vertices) / sizeof(SSimpleTexturedVertex);
	myOffset = 0;

	D3D11_BUFFER_DESC bufferDesc = {};
	bufferDesc.ByteWidth = sizeof(vertices);
	bufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	if (myUsage == EBufferUsage::DYNAMIC)
	{
		bufferDesc.Usage = D3D11_USAGE_DYNAMIC;
		bufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	}
	else
	{
		bufferDesc.Usage = D3D11_USAGE_IMMUTABLE;
	}

	D3D11_SUBRESOURCE_DATA bufferData = {};
	bufferData.pSysMem = vertices;

	HRESULT result = myDevice->CreateBuffer(&bufferDesc, &bufferData, &myBuffer);
	if (FAILED(result))
	{
		RESOURCE_LOG("ERROR! TexturedCube could not create VBuffer.");
		return false;
	}
	return true;
}

bool sce::gfx::CDX11VertexBuffer::CreateQuad(const EBufferUsage& aUsage)
{
	const float leftX = -1.0f;
	const float topY = 1.0f;
	const float rightX = 1.0f;
	const float bottomY = -1.0f;
	SSimpleTexturedVertex vertices[4] =
	{
		// X	Y			Z	 W		U	 V
		{ leftX, bottomY,	0.f, 1.f,	0.f, 1.f }, // Bottom left
		{ leftX, topY,		0.f, 1.f,	0.f, 0.f }, // Top left
		{ rightX, topY,		0.f, 1.f,	1.f, 0.f }, // Top right
		{ rightX, bottomY,	0.f, 1.f,	1.f, 1.f }  // Bottom right
	};

	myUsage = aUsage;
	myStride = sizeof(SSimpleTexturedVertex);
	myVertexCount = sizeof(vertices) / sizeof(SSimpleTexturedVertex);
	myOffset = 0;

	D3D11_BUFFER_DESC bufferDesc = {};
	bufferDesc.ByteWidth = sizeof(vertices);
	bufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	if (myUsage == EBufferUsage::DYNAMIC)
	{
		bufferDesc.Usage = D3D11_USAGE_DYNAMIC;
		bufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	}
	else
	{
		bufferDesc.Usage = D3D11_USAGE_IMMUTABLE;
	}

	D3D11_SUBRESOURCE_DATA bufferData = {};
	bufferData.pSysMem = vertices;

	HRESULT result = myDevice->CreateBuffer(&bufferDesc, &bufferData, &myBuffer);
	if (FAILED(result))
	{
		RESOURCE_LOG("ERROR! Could not create Quad-buffer");
		return false;
	}
	return true;
}

void sce::gfx::CDX11VertexBuffer::Resize(const unsigned int aSize)
{
	SAFE_RELEASE(myBuffer);
	mySize = aSize;
	myMaxSize = mySize;
	myOffset = 0;

	D3D11_BUFFER_DESC bufferDesc = {};
	bufferDesc.ByteWidth = static_cast<unsigned int>(mySize);
	bufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	bufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

	HRESULT result = myDevice->CreateBuffer(&bufferDesc, nullptr, &myBuffer);
	if (FAILED(result))
	{
		RESOURCE_LOG("ERROR! Could not create VBuffer");
		return;
	}
}

/* PRIVATE FUNCTIONS */

void sce::gfx::CDX11VertexBuffer::GetSphereVertices(SSimpleVertex*& aVertexList, const CU::Vector3f& aColor, const unsigned int aVertexCount, const unsigned int aRingCount, const unsigned int aSliceCount, const float aRadius) const
{
	SSimpleVertex* currVertexPtr(nullptr);
	
	currVertexPtr = &aVertexList[0];
	currVertexPtr->x = 0.0f;
	currVertexPtr->y = 0.0f;
	currVertexPtr->z = aRadius;
	currVertexPtr->w = 1.0f;
	SetColorOnVertex(currVertexPtr, aColor, { 0.0f });

	currVertexPtr = &aVertexList[aVertexCount - 1];
	currVertexPtr->x = 0.0f;
	currVertexPtr->y = 0.0f;
	currVertexPtr->z = -aRadius;
	currVertexPtr->w = 1.0f;
	SetColorOnVertex(currVertexPtr, aColor, { 1.0f, 0.0f, 0.0f });

	const float deltaTheta(MATH_PI / (float)(aRingCount + 1));
	const float deltaPhi((MATH_PI * 2.0f) / (float)aSliceCount);
	float currTheta(0.0f);
	float currPhi(0.0f);
	float x(0.0f);
	float y(0.0f);
	float z(0.0f);

	for (unsigned int r(0); r < aRingCount; ++r)
	{
		currTheta += deltaTheta;
		currPhi = 0.0f;

		for (unsigned int p(0); p < aSliceCount; ++p)
		{
			currPhi += deltaPhi;

			x = aRadius * std::sin(currTheta) * std::cos(currPhi);
			y = aRadius * std::sin(currTheta) * std::sin(currPhi);
			z = aRadius * std::cos(currTheta);

			currVertexPtr = &aVertexList[r * aSliceCount + p + 1];

			currVertexPtr->x = x;
			currVertexPtr->y = y;
			currVertexPtr->z = z;
			currVertexPtr->w = 1.0f;
			SetColorOnVertex(currVertexPtr, aColor, { (float)r / aRingCount, 0.0f, (float)p / aSliceCount });
		}
	}
}

void sce::gfx::CDX11VertexBuffer::GetSphereIndices(CU::GrowingArray<unsigned int, unsigned int>& aIndexList, const unsigned int aVertexCount, const unsigned int aRingCount, const unsigned int aSliceCount) const
{
	aIndexList.Init(aRingCount * aSliceCount * 6 + aRingCount * 2 + 2);

	// First, add indices attached to the vertex on the top.
	for (unsigned int i(1); i <= aSliceCount; ++i)
	{
		aIndexList.Add(i);
		if (i == aSliceCount)
		{
			aIndexList.Add(1);
		}
		else
		{
			aIndexList.Add(i + 1);
		}
		aIndexList.Add(0);
	}

	unsigned int lowerLevel(UINT_MAX);
	unsigned int higherLevel(UINT_MAX);

	unsigned int lowerLeft(UINT_MAX);
	unsigned int lowerRight(UINT_MAX);
	unsigned int higherLeft(UINT_MAX);
	unsigned int higherRight(UINT_MAX);

	for (unsigned int r(0); r < aRingCount - 1; ++r)
	{
		lowerLevel =	(r + 0) * aSliceCount;
		higherLevel =	(r + 1) * aSliceCount;
	
		for (unsigned int p(0); p < aSliceCount; ++p)
		{
			lowerLeft =		1 + lowerLevel	+ p + 0;
			higherLeft =	1 + higherLevel + p + 0;
			if (p == aSliceCount - 1)
			{
				lowerRight =	1 + lowerLevel	+ 0 + 0;
				higherRight =	1 + higherLevel + 0 + 0;
			}
			else
			{
				lowerRight =	1 + lowerLevel	+ p + 1;
				higherRight =	1 + higherLevel + p + 1;
			}
	
			aIndexList.Add(higherLeft);
			aIndexList.Add(lowerRight);
			aIndexList.Add(lowerLeft);
	
			aIndexList.Add(higherRight);
			aIndexList.Add(lowerRight);
			aIndexList.Add(higherLeft);
		}
	}
	
	// Later add indices attached to the vertex on the bottom.
	const unsigned int lastVertexIndex(aVertexCount - 1);
	for (unsigned int i(1); i <= aSliceCount; ++i)
	{
		aIndexList.Add(lastVertexIndex);
		aIndexList.Add(lastVertexIndex - i);
		if (i == aSliceCount)
		{
			aIndexList.Add(lastVertexIndex - 1);
		}
		else
		{
			aIndexList.Add(lastVertexIndex - (i + 1));
		}
	}
}

void sce::gfx::CDX11VertexBuffer::SetColorOnVertex(SSimpleVertex* aVertexPtr, const CU::Vector3f& aColor, const CU::Vector3f& aAltColor) const
{
	if (aColor.r < 0.0f || aColor.g < 0.0f || aColor.b < 0.0f)
	{
		aVertexPtr->r = aAltColor.r;
		aVertexPtr->g = aAltColor.g;
		aVertexPtr->b = aAltColor.b;
	}
	else
	{
		aVertexPtr->r = aColor.r;
		aVertexPtr->g = aColor.g;
		aVertexPtr->b = aColor.b;
	}

	aVertexPtr->a = 1.0f;
}
