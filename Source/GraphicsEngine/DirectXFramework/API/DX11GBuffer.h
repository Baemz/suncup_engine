#pragma once

struct ID3D11Device;
struct ID3D11DeviceContext;
struct ID3D11Texture2D;
struct ID3D11RenderTargetView;
struct ID3D11DepthStencilView;
struct ID3D11ShaderResourceView;
struct D3D11_VIEWPORT;

namespace sce { namespace gfx {

	class CDX11GBuffer
	{
	public:
		CDX11GBuffer();
		~CDX11GBuffer();

		void Init(const CU::Vector2f& aSize);
		void ClearBuffer();
		void SetAsRenderTarget();
		void UnbindAsRenderTarget();
		void BindAsResource();
		void BindSSAOResources();

		ID3D11DepthStencilView * GetDepth() const;

		void BindCullingMask();

	private:
		ID3D11Device* myDevice;
		ID3D11DeviceContext* myContext;
		ID3D11RenderTargetView** myRenderTarget;
		ID3D11Texture2D** myTexture;
		ID3D11DepthStencilView* myDepthStencil;
		ID3D11ShaderResourceView** myShaderResource;
		D3D11_VIEWPORT* myViewPort;
	};
}}
