#pragma once

struct ID3D11Buffer;
struct ID3D11Device;
struct ID3D11DeviceContext;
namespace sce { namespace gfx {

	class CDX11IndexBuffer
	{
	public:
		CDX11IndexBuffer();
		~CDX11IndexBuffer();

		void Bind();
		bool CreateBuffer(const CommonUtilities::GrowingArray<unsigned int, unsigned int>& aIndices);
		bool CreateCubeIndex();
		bool CreateQuadIndex();
		bool CreateInvertedCubeIndex();

		inline unsigned int GetIndexCount() const { return myIdxCount; }

	private:
		ID3D11Buffer* myBuffer;
		ID3D11Device* myDevice;
		ID3D11DeviceContext* myContext;
		unsigned int myMaxSize;
		unsigned int mySize;
		unsigned int myIdxCount;
	};
}}
