#include "stdafx.h"
#include "DX11GBuffer.h"

#define NUM_TARGETS 4
#define NUM_SRV 5

namespace sce { namespace gfx {

	CDX11GBuffer::CDX11GBuffer()
		: myRenderTarget(nullptr)
		, myDepthStencil(nullptr)
		, myShaderResource(nullptr)
		, myViewPort(nullptr)
		, myTexture(nullptr)
	{
		myDevice = CDirect3D11::GetAPI()->GetDevice();
		myContext = CDirect3D11::GetAPI()->GetContext();
	}


	CDX11GBuffer::~CDX11GBuffer()
	{
		for (unsigned int rtv = 0; rtv < NUM_TARGETS; ++rtv)
		{
			SAFE_RELEASE(myRenderTarget[rtv]);
			SAFE_RELEASE(myTexture[rtv]);
		}
		for (unsigned int srv = 0; srv < NUM_SRV; ++srv)
		{
			SAFE_RELEASE(myShaderResource[srv])
		}
		sce_delete(myShaderResource);
		sce_delete(myTexture);
		sce_delete(myRenderTarget);
		SAFE_RELEASE(myDepthStencil);
		sce_delete(myViewPort);
	}

	void sce::gfx::CDX11GBuffer::Init(const CU::Vector2f& aSize)
	{
		D3D11_TEXTURE2D_DESC textureDesc[NUM_TARGETS];

		// Normals
		textureDesc[0].Width = static_cast<UINT>(aSize.x);
		textureDesc[0].Height = static_cast<UINT>(aSize.y);
		textureDesc[0].MipLevels = 1;
		textureDesc[0].ArraySize = 1;
		textureDesc[0].Format = DXGI_FORMAT_R10G10B10A2_UNORM;
		textureDesc[0].SampleDesc.Count = 1;
		textureDesc[0].SampleDesc.Quality = 0;
		textureDesc[0].Usage = D3D11_USAGE_DEFAULT;
		textureDesc[0].BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
		textureDesc[0].CPUAccessFlags = 0;
		textureDesc[0].MiscFlags = 0;

		// Textures (Albedo, Metallic, Roughness, AO, etc.)
		for (unsigned int i = 1; i < NUM_TARGETS; ++i)
		{
			textureDesc[i].Width = static_cast<UINT>(aSize.x);
			textureDesc[i].Height = static_cast<UINT>(aSize.y);
			textureDesc[i].MipLevels = 1;
			textureDesc[i].ArraySize = 1;
			textureDesc[i].Format = DXGI_FORMAT_R8G8B8A8_UNORM;
			textureDesc[i].SampleDesc.Count = 1;
			textureDesc[i].SampleDesc.Quality = 0;
			textureDesc[i].Usage = D3D11_USAGE_DEFAULT;
			textureDesc[i].BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
			textureDesc[i].CPUAccessFlags = 0;
			textureDesc[i].MiscFlags = 0;
		}

		myRenderTarget = sce_newArray(ID3D11RenderTargetView*, NUM_TARGETS);

		ID3D11Texture2D* depthTexture = nullptr;
		D3D11_TEXTURE2D_DESC depthDesc = {};
		depthDesc.Width = static_cast<UINT>(aSize.x);
		depthDesc.Height = static_cast<UINT>(aSize.y);
		depthDesc.ArraySize = 1; 
		depthDesc.Format = DXGI_FORMAT_R32_TYPELESS;
		depthDesc.Usage = D3D11_USAGE_DEFAULT;
		depthDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL | D3D11_BIND_SHADER_RESOURCE;
		depthDesc.SampleDesc.Count = 1;
		depthDesc.SampleDesc.Quality = 0;
		depthDesc.MipLevels = 1;
		depthDesc.CPUAccessFlags = 0;
		depthDesc.MiscFlags = 0;

		HRESULT result = myDevice->CreateTexture2D(&depthDesc, nullptr, &depthTexture);
		if (FAILED(result))
		{
			ENGINE_LOG("DX11-ERROR! Could not create depth-texture.");
			return;
		}

		D3D11_DEPTH_STENCIL_VIEW_DESC dsvdesc;
		dsvdesc.Flags = 0;
		dsvdesc.Format = DXGI_FORMAT_D32_FLOAT;
		dsvdesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
		dsvdesc.Texture2D.MipSlice = 0;
		result = myDevice->CreateDepthStencilView(depthTexture, &dsvdesc, &myDepthStencil);
		if (FAILED(result))
		{
			ENGINE_LOG("DX11-ERROR! Could not create depth-stencil.");
			return;
		}

		myTexture = sce_newArray(ID3D11Texture2D*, NUM_TARGETS);
		myShaderResource = sce_newArray(ID3D11ShaderResourceView*, NUM_SRV);

		D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
		srvDesc.Format = DXGI_FORMAT_R32_FLOAT;
		srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
		srvDesc.Texture2D.MostDetailedMip = 0;
		srvDesc.Texture2D.MipLevels = 1;
		result = myDevice->CreateShaderResourceView(depthTexture, &srvDesc, &myShaderResource[0]);
		if (FAILED(result))
		{
			ENGINE_LOG("DX11-ERROR! Could not create depth shaderresouceview.");
			return;
		}

		for (unsigned int rtv = 0; rtv < NUM_TARGETS; ++rtv)
		{
			result = myDevice->CreateTexture2D(&textureDesc[rtv], nullptr, &myTexture[rtv]);
			if (FAILED(result))
			{
				ENGINE_LOG("DX11-ERROR! Could not create empty texture in CDX11FullscreenTexture.");
				return;
			}
			
			result = myDevice->CreateRenderTargetView(myTexture[rtv], nullptr, &myRenderTarget[rtv]);
			if (FAILED(result))
			{
				ENGINE_LOG("DX11-ERROR! Could not create rendertargetview.");
				return;
			}

			result = myDevice->CreateShaderResourceView(myTexture[rtv], nullptr, &myShaderResource[rtv + 1]);
			if (FAILED(result))
			{
				ENGINE_LOG("DX11-ERROR! Could not create shaderresouceview.");
				return;
			}
		}

		myViewPort = sce_new(D3D11_VIEWPORT());
		myViewPort->TopLeftX = 0;
		myViewPort->TopLeftY = 0;
		myViewPort->Width = aSize.x;
		myViewPort->Height = aSize.y;
		myViewPort->MinDepth = 0.0f;
		myViewPort->MaxDepth = 1.0f;

	}

	void CDX11GBuffer::ClearBuffer()
	{
		if (myDepthStencil != nullptr)
		{
			float clearColor[4] = { 0.f, 0.f, 0.f, 0.f };
			for (unsigned int i = 0; i < NUM_TARGETS; ++i)
			{
				myContext->ClearRenderTargetView(myRenderTarget[i], clearColor);
			}
			myContext->ClearDepthStencilView(myDepthStencil, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);
		}
	}

	void CDX11GBuffer::SetAsRenderTarget()
	{
		myContext->OMSetRenderTargets(NUM_TARGETS, myRenderTarget, myDepthStencil);
		myContext->RSSetViewports(1, myViewPort);
	}

	void CDX11GBuffer::UnbindAsRenderTarget()
	{
		ID3D11RenderTargetView* nullRTV[NUM_TARGETS] = { nullptr };
		myContext->OMSetRenderTargets(NUM_TARGETS, nullRTV, myDepthStencil);
	}

	void CDX11GBuffer::BindAsResource()
	{
		if (myShaderResource)
		{
			myContext->PSSetShaderResources(0, NUM_SRV, myShaderResource);
		}
	}

	void CDX11GBuffer::BindSSAOResources()
	{
		if (myShaderResource)
		{
			myContext->PSSetShaderResources(0, 2, myShaderResource);
		}
	}

	ID3D11DepthStencilView* CDX11GBuffer::GetDepth() const
	{
		return myDepthStencil;
	}

	void CDX11GBuffer::BindCullingMask()
	{
		if (myShaderResource)
		{
			myContext->PSSetShaderResources(1, 1, &myShaderResource[0]);
		}
	}

}}