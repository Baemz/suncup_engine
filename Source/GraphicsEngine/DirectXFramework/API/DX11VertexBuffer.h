#pragma once
#include "../../Framework/API/VertexBuffer.h"

struct ID3D11Buffer;
struct ID3D11Device;
struct ID3D11DeviceContext;
struct SSimpleVertex;

namespace sce { namespace gfx {

	enum class EBufferUsage
	{
		STATIC,
		DYNAMIC,
	};

	class CDX11VertexBuffer : public CVertexBuffer
	{
	public:
		CDX11VertexBuffer(const EBufferUsage& aUsage = EBufferUsage::STATIC);
		~CDX11VertexBuffer();

		void Bind() override;
		void Bind(CDX11VertexBuffer* aInstanceBuffer);
		void SetData(void* aData);
		void SetData(void* aData, unsigned int aSize);
		void CreateBuffer(const unsigned int aStrideSize, const unsigned int aCount, const EBufferUsage& aUsage = EBufferUsage::DYNAMIC);
		void CreateBuffer(char* aVertexList, unsigned int aListSize, int aVertexCount, const EBufferUsage& aUsage = EBufferUsage::STATIC);
		void CreateParticleBuffer(float aParticleLifeTime, float aParticleSpawnRate, const EBufferUsage& aUsage = EBufferUsage::STATIC);
		bool CreateDebugBuffer(unsigned int aSize, const EBufferUsage& aUsage = EBufferUsage::STATIC);
		bool CreatePrimitiveTriangleBuffer() override;
		bool CreateCube(const EBufferUsage& aUsage = EBufferUsage::STATIC);
		bool CreateCube(float aScale, const EBufferUsage& aUsage = EBufferUsage::STATIC);
		bool CreateSphere(CU::GrowingArray<unsigned int, unsigned int>& aIndexList, const CU::Vector3f& aColor, const float aRadius, const unsigned int aRingCount, const unsigned int aSliceCount,
			const EBufferUsage& aUsage = EBufferUsage::STATIC);
		bool CreateTexturedCube(const EBufferUsage& aUsage = EBufferUsage::STATIC);
		bool CreateQuad(const EBufferUsage& aUsage = EBufferUsage::STATIC);
		unsigned int GetSizeInBytes() const { return mySize; };
		void Resize(const unsigned int aSize);

	private:
		void GetSphereVertices(SSimpleVertex*& aVertexList, const CU::Vector3f& aColor, const unsigned int aVertexCount, const unsigned int aRingCount, const unsigned int aPointPerRingCount, const float aRadius) const;
		void GetSphereIndices(CU::GrowingArray<unsigned int, unsigned int>& aIndexList, const unsigned int aVertexCount, const unsigned int aRingCount, const unsigned int aPointPerRingCount) const;
		void SetColorOnVertex(SSimpleVertex* aVertexPtr, const CU::Vector3f& aColor, const CU::Vector3f& aAltColor = CU::Vector3f(0.5f)) const;

	private:
		ID3D11Buffer* myBuffer;
		ID3D11Device* myDevice;
		ID3D11DeviceContext* myContext;
		EBufferUsage myUsage;
		unsigned int mySize;
		unsigned int myMaxSize;
		int myVertexCount;
	};

}}