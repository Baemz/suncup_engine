#include "stdafx.h"
#include "DX11Texture2D.h"
#include "../Direct3D11.h"
#include "../DirectXTK/DDSTextureLoader.h"

namespace sce { namespace gfx {

	ID3D11ShaderResourceView* CDX11Texture::ourDefaultAlbedo = nullptr;
	ID3D11ShaderResourceView* CDX11Texture::ourDefaultBlack = nullptr;
	ID3D11ShaderResourceView* CDX11Texture::ourDefaultWhite = nullptr;
	ID3D11ShaderResourceView* CDX11Texture::ourDefaultNormal = nullptr;
	CU::Vector2f CDX11Texture::ourDefaultAlbedoTextureSize = CU::Vector2f();
	CU::Vector2f CDX11Texture::ourDefaultBlackTextureSize = CU::Vector2f();
	CU::Vector2f CDX11Texture::ourDefaultWhiteTextureSize = CU::Vector2f();
	CU::Vector2f CDX11Texture::ourDefaultNormalTextureSize = CU::Vector2f();

	CDX11Texture::CDX11Texture()
		: myTextureSlot(0)
		, myTextureMips(0)
		, myTexture(nullptr)
		, myDevice(CDirect3D11::GetAPI()->GetDevice())
		, myContext(CDirect3D11::GetAPI()->GetContext())
		, myIsLoaded(false)
	{
	}


	CDX11Texture::~CDX11Texture()
	{
		SAFE_RELEASE(myTexture);
	}

	void CDX11Texture::CreateDefaultTextures()
	{
		ID3D11Device* device = CDirect3D11::GetAPI()->GetDevice();
		DirectX::CreateDDSTextureFromFile(device, L"Data/Models/Misc/txt_PBRfiller_maybe.dds", nullptr, &ourDefaultAlbedo);
		ourDefaultAlbedoTextureSize = { 4.f, 4.f };
		DirectX::CreateDDSTextureFromFile(device, L"Data/Models/Misc/txt_PBRfiller_no.dds", nullptr, &ourDefaultBlack);
		ourDefaultBlackTextureSize = { 4.f, 4.f };
		DirectX::CreateDDSTextureFromFile(device, L"Data/Models/Misc/txt_PBRfiller_yes.dds", nullptr, &ourDefaultWhite);
		ourDefaultWhiteTextureSize = { 4.f, 4.f };
		DirectX::CreateDDSTextureFromFile(device, L"Data/Models/Misc/txt_PBRfiller_normalMap.dds", nullptr, &ourDefaultNormal);
		ourDefaultNormalTextureSize = { 128.f, 128.f };
	}

	void CDX11Texture::DestroyDefaultTextures()
	{
		SAFE_RELEASE(ourDefaultAlbedo);
		SAFE_RELEASE(ourDefaultBlack);
		SAFE_RELEASE(ourDefaultWhite);
		SAFE_RELEASE(ourDefaultNormal);
	}

	void CDX11Texture::Bind()
	{
		if (myTexture != nullptr)
		{
			myContext->PSSetShaderResources(myTextureSlot, 1, &myTexture);
		}
	}

	void CDX11Texture::Bind(const unsigned int aSlot)
	{
		if (myTexture != nullptr)
		{
			myContext->PSSetShaderResources(aSlot, 1, &myTexture);
		}
	}

	const CU::Vector2f CDX11Texture::GetTextureSize()
	{
		return myTextureSize;
	}

	const void CDX11Texture::SetTextureSize()
	{
		if (myTexture == nullptr)
		{
			return;
		}

		ID3D11Resource* resource = nullptr;
		myTexture->GetResource(&resource);

		if (resource == nullptr)
		{
			return;
		}

		ID3D11Texture2D* texture = reinterpret_cast<ID3D11Texture2D*>(resource);
		D3D11_TEXTURE2D_DESC textDesc;
		texture->GetDesc(&textDesc);
		myTextureMips = textDesc.MipLevels;
		myTextureSize.x = static_cast<float>(textDesc.Width);
		myTextureSize.y = static_cast<float>(textDesc.Height);

		CU::Vector2f size(static_cast<float>(textDesc.Width), static_cast<float>(textDesc.Height));
		resource->Release();
	}

	bool CDX11Texture::CreateTexture(const wchar_t* aFilePath, const ETextureType aType)
	{
		if (aType == ETextureType::Skymap)
		{
			HRESULT result = DirectX::CreateDDSTextureFromFileEx(myDevice, aFilePath, 0, D3D11_USAGE_IMMUTABLE, D3D11_BIND_SHADER_RESOURCE, 0, D3D11_RESOURCE_MISC_TEXTURECUBE, false, nullptr, &myTexture);
			
			if (FAILED(result))
			{
				RESOURCE_LOG("WARNING! [%ls] could not be loaded.", aFilePath);
				return false;
			}
			myTextureSlot = 0;
		}
		else if (aType == ETextureType::LUT)
		{
			HRESULT result = DirectX::CreateDDSTextureFromFile(myDevice, aFilePath, nullptr, &myTexture);
			if (FAILED(result))
			{
				RESOURCE_LOG("WARNING! [%ls] could not be loaded.", aFilePath);
				return false;
			}
			myTextureSlot = 1;
		}
		else
		{
			HRESULT result = DirectX::CreateDDSTextureFromFile(myDevice, aFilePath, nullptr, &myTexture);
			if (FAILED(result))
			{
				if (aType == ETextureType::NormalMap)
				{
					RESOURCE_LOG("WARNING! [%ls] could not be loaded, loading default-texture.", aFilePath);
					myTexture = ourDefaultNormal;
					myTextureSize = ourDefaultNormalTextureSize;
				}
				else if (aType == ETextureType::Diffuse)
				{
					RESOURCE_LOG("WARNING! [%ls] could not be loaded, loading default-texture.", aFilePath);
					myTexture = ourDefaultAlbedo;
					myTextureSize = ourDefaultAlbedoTextureSize;
				}
				else if (aType == ETextureType::Metallic)
				{
					RESOURCE_LOG("WARNING! [%ls] could not be loaded, loading default-texture.", aFilePath);
					myTexture = ourDefaultBlack;
					myTextureSize = ourDefaultBlackTextureSize;
				}
				else if (aType == ETextureType::Roughness)
				{
					RESOURCE_LOG("WARNING! [%ls] could not be loaded, loading default-texture.", aFilePath);
					myTexture = ourDefaultBlack;
					myTextureSize = ourDefaultBlackTextureSize;
				}
				else if (aType == ETextureType::AmbientOcclusion)
				{
					RESOURCE_LOG("WARNING! [%ls] could not be loaded, loading default-texture.", aFilePath);
					myTexture = ourDefaultWhite;
					myTextureSize = ourDefaultWhiteTextureSize;
				}
				else if (aType == ETextureType::Emissive)
				{
					RESOURCE_LOG("WARNING! [%ls] could not be loaded, loading default-texture.", aFilePath);
					myTexture = ourDefaultBlack;
					myTextureSize = ourDefaultBlackTextureSize;
				}
				myTextureSlot = static_cast<unsigned int>(aType);
				return true;
			}
			myTextureSlot = static_cast<unsigned int>(aType);
		}
		myName = aFilePath;
		SetTextureSize();
		return true;
	}

	bool CDX11Texture::CreateTexture(const wchar_t * aFilePath, const unsigned int aSlot)
	{
		if (CreateTexture(aFilePath, ETextureType::Diffuse))
		{
			myTextureSlot = aSlot;
			return true;
		}
		return false;
	}

	bool CDX11Texture::CreateTexture(const char* aFilePath, const ETextureType aType)
	{
		wchar_t wstring[4096];
		return CreateTexture(ConvertCharArrayToLPCWSTR(aFilePath, (wchar_t*)(&wstring), 4096), aType);
	}
	bool CDX11Texture::CreateTexture(const char* aFilePath, const unsigned int aSlot)
	{
		if (CreateTexture(aFilePath, ETextureType::Diffuse))
		{
			myTextureSlot = aSlot;
			return true;
		}
		return false;
	}
}}