#pragma once
#include <string>

struct ID3D11Device;
struct ID3D11DeviceContext;
struct ID3D11VertexShader;
struct ID3D11PixelShader;
struct ID3D11GeometryShader;
struct ID3D11ComputeShader;
struct ID3D11InputLayout;
struct ID3D10Blob;

namespace sce { namespace gfx {
	enum class eShaderType
	{
		Vertex,
		Pixel,
		Geometry,
		Compute,
		NONE
	};

	class CDX11Shader
	{
	public:
		CDX11Shader();
		~CDX11Shader();

		void InitVertex(const char* aPath, const char* aEntryPoint);
		void InitPixel(const char* aPath, const char* aEntryPoint);
		void InitVertex(const char* aPath, const char* aEntryPoint, const D3D_SHADER_MACRO* aDefines);
		void InitPixel(const char* aPath, const char* aEntryPoint, const D3D_SHADER_MACRO* aDefines);
		void InitGeometry(const char* aPath, const char* aEntryPoint, const D3D_SHADER_MACRO* aDefines);
		void InitGeometry(const char* aPath, const char* aEntryPoint);
		void InitCompute(const char* aPath, const char* aEntryPoint, const D3D_SHADER_MACRO* aDefines);
		void InitCompute(const char* aPath, const char* aEntryPoint);
		void Bind();
		void Unbind();
		const bool IsLoading() const;

	private:
		std::string myPath;
		std::string myEntryPoint;
		std::string myDefine;
		std::string myDefineName;
		eShaderType myType;
		bool myHasInitialized;
		bool myHasInitializedOnce;
		volatile bool myIsLoading;
		ID3D11VertexShader* myVertexShader;
		ID3D11InputLayout* myInputLayout;
		ID3D11PixelShader* myPixelShader;
		ID3D11GeometryShader* myGeometryShader;
		ID3D11ComputeShader* myComputeShader;
		ID3D11Device* myDevice;
		ID3D11DeviceContext* myContext;

		void CompileShader(const char* aPath, const char* aEntryPoint, ID3D10Blob*& aDataBlob);
		void CreateLayout(ID3D10Blob*& aDataBlob);
		static bool ShaderChangedCallback(void* aThis, const char* aShaderChanged);
	};

}}