#include "stdafx.h"
#include "DX11IndexBuffer.h"
#include "../Direct3D11.h"
#include "../CommonUtilities/GrowingArray.h"

using namespace sce::gfx;

CDX11IndexBuffer::CDX11IndexBuffer()
	: myBuffer(nullptr)
	, mySize(0)
	, myMaxSize(0)
	, myIdxCount(0)
{
	myDevice = CDirect3D11::GetAPI()->GetDevice();
	myContext = CDirect3D11::GetAPI()->GetContext();
}


CDX11IndexBuffer::~CDX11IndexBuffer()
{
	SAFE_RELEASE(myBuffer);
}

void sce::gfx::CDX11IndexBuffer::Bind()
{
	myContext->IASetIndexBuffer(myBuffer, DXGI_FORMAT_R32_UINT, 0);
}

bool sce::gfx::CDX11IndexBuffer::CreateBuffer(const CommonUtilities::GrowingArray<unsigned int, unsigned int>& aIndices)
{
	D3D11_BUFFER_DESC bufferDesc = {};
	bufferDesc.ByteWidth = sizeof(unsigned int) * aIndices.Size();
	bufferDesc.Usage = D3D11_USAGE_IMMUTABLE;
	bufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;

	D3D11_SUBRESOURCE_DATA bufferData = {};
	bufferData.pSysMem = &aIndices[0];

	HRESULT result = myDevice->CreateBuffer(&bufferDesc, &bufferData, &myBuffer);
	if (FAILED(result))
	{
		RESOURCE_LOG("WARNING! Could not create index-buffer.");
		return false;
	}
	myIdxCount = static_cast<unsigned int>(aIndices.Size());
	return true;
}

bool sce::gfx::CDX11IndexBuffer::CreateCubeIndex()
{
	unsigned int indices[36] =
	{
		0, 2, 1,
		0, 1, 3,
		0, 3, 2,
		1, 2, 4,
		2, 3, 6,
		3, 1, 5,
		4, 5, 1,
		5, 6, 3,
		6, 4, 2,
		7, 6, 5,
		7, 5, 4,
		7, 4, 6
	};

	D3D11_BUFFER_DESC bufferDesc = {};
	bufferDesc.ByteWidth = sizeof(indices);
	bufferDesc.Usage = D3D11_USAGE_IMMUTABLE;
	bufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;

	D3D11_SUBRESOURCE_DATA bufferData = {};
	bufferData.pSysMem = indices;

	HRESULT result = myDevice->CreateBuffer(&bufferDesc, &bufferData, &myBuffer);
	if (FAILED(result))
	{
		RESOURCE_LOG("WARNING! Could not create index-buffer.");
		return false;
	}
	myIdxCount = 36;
	return true;
}

bool sce::gfx::CDX11IndexBuffer::CreateQuadIndex()
{
	unsigned int indices[6] =
	{
		1, 2, 0,
		0, 2, 3
	};

	D3D11_BUFFER_DESC bufferDesc = {};
	bufferDesc.ByteWidth = sizeof(indices);
	bufferDesc.Usage = D3D11_USAGE_IMMUTABLE;
	bufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;

	D3D11_SUBRESOURCE_DATA bufferData = {};
	bufferData.pSysMem = indices;

	HRESULT result = myDevice->CreateBuffer(&bufferDesc, &bufferData, &myBuffer);
	if (FAILED(result))
	{
		RESOURCE_LOG("WARNING! Could not create index-buffer.");
		return false;
	}
	myIdxCount = 6;
	return true;
}

bool sce::gfx::CDX11IndexBuffer::CreateInvertedCubeIndex()
{
	unsigned int indices[36] =
	{
		0, 1, 2,
		0, 3, 1,
		0, 2, 3,
		1, 4, 2,
		2, 6, 3,
		3, 5, 1,
		4, 1, 5,
		5, 3, 6,
		6, 2, 4,
		7, 5, 6,
		7, 4, 5,
		7, 6, 4
	};

	D3D11_BUFFER_DESC bufferDesc = {};
	bufferDesc.ByteWidth = sizeof(indices);
	bufferDesc.Usage = D3D11_USAGE_IMMUTABLE;
	bufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;

	D3D11_SUBRESOURCE_DATA bufferData = {};
	bufferData.pSysMem = indices;

	HRESULT result = myDevice->CreateBuffer(&bufferDesc, &bufferData, &myBuffer);
	if (FAILED(result))
	{
		RESOURCE_LOG("WARNING! Could not create index-buffer.");
		return false;
	}
	myIdxCount = 36;
	return true;
}
