#pragma once
#include "../Framework/Framework.h"
#include "API/DX11FullscreenTexture.h"
//#include <atomic>

struct IDXGISwapChain;
struct ID3D11Device;
struct ID3D11DeviceContext;
struct ID3D11RenderTargetView;
struct ID3D11DepthStencilState;
struct ID3D11BlendState;
struct ID3D11SamplerState;
struct ID3D11RasterizerState;

namespace sce { namespace gfx {

	enum class ESamplerState
	{
		Default,
		Min_Mag_Mip_Point,
		Anisotropic_x16,
		Anisotropic_x16_Mirrored,
	};

	class CWindowHandler;

	class CDirect3D11 : public CFramework
	{
	public:
		CDirect3D11();
		~CDirect3D11();

		bool Init(CWindowHandler& aWindowHandler, const bool aStartInFullscreen, const bool aUseVSync) override;

		void BeginFrame(float aClearColor[4]) override;
		void EndFrame() override;
		void TakeScreenshot(const wchar_t* aDestinationPath) override;
		void ActivateScreenTarget() override;
		void ToggleFullscreen() override;

		void SetResolution(const unsigned int aWidth, const unsigned int aHeight) override;
		void SetVsync(const bool aUseVsync) override;

		const bool IsVsyncEnabled() const override;

		ID3D11Device* GetDevice() { return myDevice; };
		ID3D11DeviceContext* GetContext() { return myContext; };

		void SetDefaultDepth();
		void DisableDepth();
		void EnableReadOnlyDepth();

		void EnableAlphablend();
		void EnableAdditiveblend();
		void DisableBlending();

		void SetSamplerState(const ESamplerState& aState, const unsigned int aSlot = 0);

		void SetShouldRenderWireFrame();
		const bool GetShouldRenderWireFrame() const;
		void SetRasterStateWireframe(const bool aState) const;
		void EnableFrontFaceCulling();
		void DisableFaceCulling();
		void SetDefaultFaceCulling();
		void EnableSlopedDepthBias();
		void SetDefaultRasterState();

		inline static CDirect3D11* GetAPI() { return (CDirect3D11*)ourFramework; }

	private:
		bool CreateDepthBuffers();
		void CreateAlphablend();
		bool CreateSamplerState();
		bool CreateRasterState();

	private:
		bool myShouldRenderWireFrame;
		bool myUseVSync;
		CWindowHandler* myWindowHandler;
		IDXGISwapChain* mySwapChain;
		ID3D11Device* myDevice;
		ID3D11DeviceContext* myContext;
		CDX11FullscreenTexture* myScreenTarget;

		// Depth
		ID3D11DepthStencilState* myDepthState2D;
		ID3D11DepthStencilState* myDepthStateParticle;
		ID3D11DepthStencilState* myDepthState;

		// Blending
		ID3D11BlendState* myAlphablendState;
		ID3D11BlendState* myAdditiveblendState;
		ID3D11BlendState* myDisabledBlendState;

		// Sampler
		ID3D11SamplerState* myDefaultSamplerState;
		ID3D11SamplerState* myMin_Mag_Mip_Point_SamplerState;
		ID3D11SamplerState* myAnisoSamplerState;
		ID3D11SamplerState* myAnisoMirroredSamplerState;

		// Rasterizer
		ID3D11RasterizerState* myRasterStateNormal;
		ID3D11RasterizerState* myRasterStateFrontFaceCull;
		ID3D11RasterizerState* myRasterStateNoFaceCull;
		ID3D11RasterizerState* myRasterStateWireframe;
		ID3D11RasterizerState* myRasterStateSlopedBias;
		int mySwapChainIsInFullscreen;
	};
}}