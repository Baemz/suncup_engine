﻿#include "stdafx.h"
#include "TextRenderer.h"
#include "../Text/Font.h"
#include "DirectXFramework\Direct3D11.h"
#include "GraphicsEngineInterface.h"
#include <mutex>

namespace sce
{
	namespace gfx
	{
		namespace TextRenderer🔒
		{
			std::mutex ourTextRendererMutex;
		}

		CTextRenderer* CTextRenderer::ourInstance(nullptr);

		CTextRenderer::CTextRenderer()
			: myDevice(nullptr)
			, myDeviceContext(nullptr)
		{
		}

		CTextRenderer::~CTextRenderer()
		{
			Destroy();
		}

		CTextRenderer* CTextRenderer::GetInstance()
		{
			return ourInstance;
		}

		void CTextRenderer::Init()
		{
			std::unique_lock<std::mutex> lg(TextRenderer🔒::ourTextRendererMutex);

			ourInstance = this;

			myDevice = CDirect3D11::GetAPI()->GetDevice();
			myDeviceContext = CDirect3D11::GetAPI()->GetContext();
		}

		void CTextRenderer::Destroy()
		{
			myDevice = nullptr;
			myDeviceContext = nullptr;

			for (auto currentNamePairFont : myFonts)
			{
				sce_delete(currentNamePairFont.second);
			}

			myFonts.clear();
		}

		bool CTextRenderer::InitFont(const std::string& aFontName)
		{
			std::unique_lock<std::mutex> lg(TextRenderer🔒::ourTextRendererMutex);

			if (myFonts.find(aFontName) == myFonts.end())
			{
				CFont* newFont(sce_new(CFont()));
				if (newFont->Init(myDevice, myDeviceContext, aFontName.c_str()) == false)
				{
					sce_delete(newFont);
					return false;
				}

				myFonts.insert(std::pair<std::string, CFont*>(aFontName, newFont));
			}

			return true;
		}

		float CTextRenderer::GetTextWidth(const CText& aText)
		{
			std::unique_lock<std::mutex> lg(TextRenderer🔒::ourTextRendererMutex);

			auto fontsIterator(myFonts.find(aText.GetFontName()));

			if (fontsIterator == myFonts.end())
			{
				return false;
			}

			return fontsIterator->second->SetDataAndGetWidth(aText.GetText(), aText.GetSize(), aText.GetWordWrapBoxSize(), aText.GetAlignSetting());
		}

		bool CTextRenderer::Render(const CU::GrowingArray<CText, unsigned int>& aTexts, const CU::Vector2f& aResolution)
		{
			unsigned short failedTexts(0);

			CU::Vector2f resolution;
			resolution = CGraphicsEngineInterface::GetResolution();

			CU::Matrix44f transformMatrix;
			transformMatrix[0] = 2.0f / resolution.x;
			transformMatrix[12] = -1.0f;
			transformMatrix[5] = -2.0f / resolution.y;
			transformMatrix[13] = 1.0f;
			transformMatrix[10] = 1.0f;
			transformMatrix[15] = 1.0f;

			for (unsigned short textIndex = 0; textIndex < aTexts.Size(); ++textIndex)
			{
				const CText& currentText(aTexts[textIndex]);
				if (RenderText(currentText, aResolution, transformMatrix) == false)
				{
					++failedTexts;
				}
			}

			if (failedTexts != 0)
			{
				ENGINE_LOG("ERROR! Failed to render %hu texts", failedTexts);
				return false;
			}

			return true;
		}

		bool CTextRenderer::RenderText(const CText& aText, const CU::Vector2f& aResolution, const CU::Matrix44f& aTransform) const
		{
			std::unique_lock<std::mutex> lg(TextRenderer🔒::ourTextRendererMutex);

			auto fontsIterator(myFonts.find(aText.GetFontName()));

			if (fontsIterator == myFonts.end())
			{
				return false;
			}

			CFont* fontToUse(fontsIterator->second);

			return fontToUse->Render(aText.GetText().c_str(), aText.GetSize(), (aText.GetPosition() * aResolution), aText.GetColor(), aTransform, aText.GetWordWrapBoxSize(), aText.GetAlignSetting());
		}
	}
}
