#include "stdafx.h"
#include "ForwardRenderer.h"
#include "../Model/ModelInstance.h"
#include "../Model/Model.h"
#include "../ResourceManager/ResourceManager.h"
#include "../Camera/CameraInstance.h"
#include "../Camera/Camera.h"
#include "Lights/DirectionalLight.h"
#include "Lights/EnvironmentalLight.h"
#include "Model/Loading/ModelFactory.h"
#include "Material.h"
#include "Lights/SpotLight.h"
#include "Utilities/LightStructs.h"
#include "Lights/LightFactory.h"
#include "Lights/PointLight.h"

using namespace sce::gfx;
#define SHADOW_BUFFER_SIZE_X 4096.f
#define SHADOW_BUFFER_SIZE_Y 4096.f

CForwardRenderer::CForwardRenderer()
	: myContext(nullptr)
	, myPointlightCBufferID(UINT_MAX)
	, mySpotlightCBufferID(UINT_MAX)
	, myDirLightCBufferID(UINT_MAX)
	, myShadowCBufferID(UINT_MAX)
{
}


CForwardRenderer::~CForwardRenderer()
{
}

void CForwardRenderer::Destroy()
{
}

void CForwardRenderer::Init()
{
	CResourceManager* resourceManager = CResourceManager::Get();
	myContext = CDirect3D11::GetAPI()->GetContext();
	myCBufferID = resourceManager->GetConstantBufferID("ForwardRenderer");
	myPointlightCBufferID = resourceManager->GetConstantBufferID("PLightForwardRenderer");
	mySpotlightCBufferID = resourceManager->GetConstantBufferID("SLightForwardRenderer");
	myDirLightCBufferID = resourceManager->GetConstantBufferID("DLightForwardRenderer");
	myShadowCBufferID = resourceManager->GetConstantBufferID("DLightShadowForwardRenderer");
	myInstanceBonesCBufferID = resourceManager->GetConstantBufferID("ForwardBonesBuffer");
	CDX11ConstantBuffer& instanceBoneCBuffer(resourceManager->GetConstantBufferWithID(myInstanceBonesCBufferID));
	SBoneBufferData initData;
	instanceBoneCBuffer.SetData(initData);

}

void CForwardRenderer::Render(SRenderData& aRenderData)
{
	if(myContext == nullptr)
	{
		return;
	}
	if (aRenderData.myCameraInstance.myCameraID == UINT_MAX)
	{
		return;
	}

	CDirect3D11::GetAPI()->EnableReadOnlyDepth();
	CDirect3D11::GetAPI()->EnableAlphablend();
	myContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	CResourceManager* resourceManager = CResourceManager::Get();
	CModelFactory* modelFactory = CModelFactory::Get();

	SInstanceBufferData instanceData;
	CDX11ConstantBuffer& instanceCBuffer(resourceManager->GetConstantBufferWithID(myCBufferID));

	CDX11ConstantBuffer& dirLightBuffer(resourceManager->GetConstantBufferWithID(myDirLightCBufferID));
	SDirLightData dirLightData(aRenderData.myDirLight.myLightData);

	if (aRenderData.myEnvLight.myTextureID != UINT_MAX)
	{
		CDX11Texture& envtexture = resourceManager->GetTextureWithID(aRenderData.myEnvLight.myTextureID);
		dirLightData.myToLightDirection.w = static_cast<float>(envtexture.GetMipCount());
		envtexture.Bind(7);
	}

	dirLightBuffer.SetData(dirLightData);
	dirLightBuffer.BindPS(5);

	CU::Vector3f lightDir(aRenderData.myDirLight.myLightData.myToLightDirection);
	SetShadowMatrices(resourceManager->GetCameraWithID(aRenderData.myCameraInstance.myCameraID), aRenderData.myCameraInstance, lightDir);
	SShadowBufferData depthMVP;
	depthMVP.depthProj = myShadowProjection;
	depthMVP.depthView = myLightView;
	depthMVP.resolution = CU::Vector2f(SHADOW_BUFFER_SIZE_X, SHADOW_BUFFER_SIZE_Y);
	CDX11ConstantBuffer& depthCBuffer(resourceManager->GetConstantBufferWithID(myShadowCBufferID));
	depthCBuffer.SetData(depthMVP);
	depthCBuffer.BindPS(6);

	SBoneBufferData data;
	CDX11ConstantBuffer& instanceBoneCBuffer(resourceManager->GetConstantBufferWithID(myInstanceBonesCBufferID));

	for (unsigned int i = 0; i < aRenderData.myTransparentListToRender.Size(); ++i)
	{
		CModelInstance& instance = aRenderData.myTransparentListToRender[i];
		if (instance.myModelID == UINT_MAX)
		{
			ENGINE_LOG("[%s] could not render, model not found.", instance.myModelName.c_str());
			continue;
		}
		if (instance.myMaterialID == UINT_MAX)
		{
			continue;
		}
		CModel& model(resourceManager->GetModelWithID(instance.myModelID));


		if (model.myVertexBufferID == UINT_MAX)
		{
			ENGINE_LOG("[%s] could not render, VBuffer not found.", instance.myModelName.c_str());
			continue;
		}

		CDX11VertexBuffer& vb(resourceManager->GetVertexBufferWithID(model.myVertexBufferID));
		CDX11IndexBuffer& ib(resourceManager->GetIndexBufferWithID(model.myIndexBufferID));


		CMaterial& mat(modelFactory->GetMaterialWithID(instance.myMaterialID));
		if (mat.IsLoading())
		{
			continue;
		}
		mat.Bind();

		CU::Matrix44f orientation(instance.myOrientation);
		orientation.ScaleBy(instance.myScale);
		instanceData.myToWorld = orientation;
		instanceCBuffer.SetData(instanceData);
		instanceCBuffer.BindVS(3);

		if (instance.myBoneTransforms.empty() == false)
		{
			unsigned int index = 0;
			for (auto& transfrom : instance.myBoneTransforms)
			{
				data.bones[index] = transfrom;
				++index;
			}
			data.useAnimation = 1;
			instanceBoneCBuffer.SetData(data);
		}
		else
		{
			SBoneBufferDataOnlyBool dataOnlyBool;
			dataOnlyBool.useAnimation = 0;
			instanceBoneCBuffer.SetData(dataOnlyBool);
		}
		instanceBoneCBuffer.BindVS(4);

		vb.Bind();
		ib.Bind();

		BindClosestLights(instance.GetPosition(), aRenderData);
		myContext->DrawIndexed(ib.GetIndexCount(), 0, 0);
	}
	CDirect3D11::GetAPI()->SetDefaultDepth();
	CDirect3D11::GetAPI()->DisableBlending();
	CDirect3D11::GetAPI()->SetDefaultFaceCulling();
}

void sce::gfx::CForwardRenderer::SetShadowMatrices(const CCamera & aCamera, const CCameraInstance & aCameraInstance, const CU::Vector3f & aToLightDirection)
{
	CU::Vector3f tempCamPos(aCameraInstance.GetPosition());
	CU::Vector3f dirLightTempPos((tempCamPos)+(aToLightDirection * (50.f)));
	myLightView = LookAtDX(dirLightTempPos, tempCamPos, { 0.f, 1.0f, 0.f });
	myShadowProjection = CreateOrthogonalMatrixLH(50.f, 50.f, 1.f, aCamera.GetFar() * 0.3f);
}

void sce::gfx::CForwardRenderer::BindClosestLights(const CU::Vector3f & aPos, SRenderData & aRenderData)
{
	CLightFactory* lightFactory = CLightFactory::Get();
	// Pointlights
	CU::GrowingArray<CPointLightInstance*> pointlights;
	pointlights.Reserve(MAX_LIGHTS);

	for (unsigned short i = 0; i < aRenderData.myPointLightData.size(); ++i)
	{
		auto& pl = aRenderData.myPointLightData[i];
		const float dist1 = (pl.GetPosition() - aPos).Length2();

		if (dist1 > pl.myRange * pl.myRange)
		{
			continue;
		}
		else if (pointlights.size() < MAX_LIGHTS)
		{
			pointlights.Add(&pl);
		}
		else
		{
			for (unsigned short plIndex = 0; plIndex < pointlights.size(); ++plIndex)
			{
				auto& pl2 = pointlights[plIndex];
				const float dist2 = (pl2->GetPosition() - aPos).Length2();
				if (dist1 < dist2)
				{
					pointlights[plIndex] = &pl;
					break;
				}
			}
		}
	}

	SPointLightData plData;
	plData.numLights = pointlights.size();
	{
		unsigned int i = 0;
		for (auto& instance : pointlights)
		{
			CPointLight& pl(lightFactory->GetPointLightWithID(instance->myPointLightID));
			SPointLightData::PL data;
			data.myColor = pl.myColor;
			data.myIntensity = instance->myIntensity;
			data.myPosition = CU::Vector4f(instance->GetPosition(), 1.f);
			data.myRange = pl.myRange;
			plData.pointLights[i] = data;
			++i;
		}
	}
	CDX11ConstantBuffer& pointlightBuffer(CResourceManager::Get()->GetConstantBufferWithID(myPointlightCBufferID));
	pointlightBuffer.SetData(plData);
	pointlightBuffer.BindPS(7);


	// Spotlights
	CU::GrowingArray<CSpotLightInstance*> spotlights;
	spotlights.Reserve(MAX_LIGHTS);
	for (unsigned short i = 0; i < aRenderData.mySpotLightData.size(); ++i)
	{
		auto& sl = aRenderData.mySpotLightData[i];
		const float dist1 = (sl.GetPosition() - aPos).Length2();

		if (dist1 > sl.myRange * sl.myRange)
		{
			continue;
		}
		else if (spotlights.size() < MAX_LIGHTS)
		{
			spotlights.Add(&sl);
		}
		else
		{
			for (unsigned short plIndex = 0; plIndex < spotlights.size(); ++plIndex)
			{
				auto& sl2 = spotlights[plIndex];

				const float dist2 = (sl2->GetPosition() - aPos).Length2();

				if ((sl.GetPosition() - aPos).Length2() < (sl2->GetPosition() - aPos).Length2())
				{
					spotlights[plIndex] = &sl;
					break;
				}
			}
		}
	}

	SSpotLightData slData;
	slData.numLights = spotlights.size();
	{
		unsigned int i = 0;
		for (auto& instance : spotlights)
		{
			CSpotLight& sl(lightFactory->GetSpotLightWithID(instance->mySpotLightID));
			SSpotLightData::SL data;
			data.myColor = sl.myColor;
			data.myIntensity = instance->myIntensity;
			data.myPosition = CU::Vector4f(instance->GetPosition(), 1.f);
			data.myRange = sl.myRange;
			data.myAngle = sl.myAngle;
			data.myDirection = instance->myDirection;
			slData.spotLights[i] = data;
			++i;
		}
	}
	CDX11ConstantBuffer& spotlightBuffer(CResourceManager::Get()->GetConstantBufferWithID(mySpotlightCBufferID));
	spotlightBuffer.SetData(slData);
	spotlightBuffer.BindPS(8);
}
