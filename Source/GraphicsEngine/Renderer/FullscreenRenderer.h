#pragma once
#include "../DirectXFramework/API/DX11FullscreenTexture.h"
#include <array>
#include "../../CommonUtilities/Timer.h"

struct ID3D11DeviceContext;
namespace sce { namespace gfx {

	class CDX11VertexBuffer;
	class CDX11IndexBuffer;
	class CDX11Shader;
	class CDX11ConstantBuffer; 
	class CDX11Texture;

	enum class EFXType
	{
		Copy,
		Add,
		BloomAdd,
		Luminance,
		GaussianBlurVertical4,
		GaussianBlurHorizontal4,
		GaussianBlurVertical8,
		GaussianBlurHorizontal8,
		GaussianBlurVertical16,
		GaussianBlurHorizontal16,
		FXAA,
		Tonemap,
		TonemapLum,
		ColorGrading,
		SSAO,
		SSAOBlur,
		COUNT,
	};

	class CFullscreenRenderer
	{
	public:
		CFullscreenRenderer();
		~CFullscreenRenderer();

		void Destroy();

		void Init();
		void Render(const EFXType aFXType, std::array<CDX11FullscreenTexture*, 2> aFullscreenTexture);
		void RenderEffect(CShaderEffectInstance& aEffect, CDX11FullscreenTexture* aFullscreenTexture);

	private:
		void AddPass(std::array<CDX11FullscreenTexture*, 2>& aFullscreenTexture);
		void CopyPass(CDX11FullscreenTexture* aTexture);
		void LuminancePass(CDX11FullscreenTexture* aTexture);
		void BloomAddPass(std::array<CDX11FullscreenTexture*, 2>& aFullscreenTexture);
		void GaussianBlurHPass(CDX11FullscreenTexture* aTexture, unsigned int aBlurLevel);
		void GaussianBlurVPass(CDX11FullscreenTexture* aTexture, unsigned int aBlurLevel);
		void FXAAPass(CDX11FullscreenTexture* aTexture);
		void TonemapPass(std::array<CDX11FullscreenTexture*, 2>& aFullscreenTexture);
		void TonemapLumPass(CDX11FullscreenTexture* aTexture);
		void ColorGradingPass(CDX11FullscreenTexture* aTexture);
		void SSAOPass();
		void SSAOBlurPass(CDX11FullscreenTexture* aTexture);

	private:
		struct STexelSize
		{
			float x, y;
			float pad[2];
		};

		struct SHalfPixels
		{
			float numHalfPixels;
			float pad[3];
		};

		struct SPostFXSettings
		{
			float bloomCutoff;
			float bloomIntensity;
			unsigned int blurLevel;
			float pad[1];
		};

	private:
		SPostFXSettings myPostFXSettings;
		ID3D11DeviceContext* myContext;
		CDX11Texture* myLUT;
		CDX11Texture* mySSAORandom;
		CDX11VertexBuffer* myVBuffer;
		CDX11IndexBuffer* myIBuffer;
		CDX11ConstantBuffer* myCBuffer;
		CDX11ConstantBuffer* myColorGradeCBuffer;
		CDX11ConstantBuffer* mySettingsCBuffer;
		CDX11Shader* myVertexShader;
		CDX11Shader* myCopyShader;
		CDX11Shader* myLuminanceShader;
		CDX11Shader* myBloomAddShader;
		CDX11Shader* myAddShader;
		CDX11Shader* myGBlurHShader;
		CDX11Shader* myGBlurVShader;
		CDX11Shader* myFXAAShader;
		CDX11Shader* myTonemapShader;
		CDX11Shader* myTonemapLumShader;
		CDX11Shader* myColorGradingShader;
		CDX11Shader* mySSAOShader;
		CDX11Shader* mySSAOBlurShader;
	};

}}