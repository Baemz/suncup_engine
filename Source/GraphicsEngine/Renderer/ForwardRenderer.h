#pragma once
#include "../CommonUtilities/Matrix.h"

struct ID3D11DeviceContext;

namespace sce { namespace gfx {
	
	class CCameraInstance;
	class CModelInstance;
	class CDirectionalLight;
	class CEnvironmentalLight;
	struct SRenderData;

	

	class CForwardRenderer
	{
	public:
		CForwardRenderer();
		~CForwardRenderer();

		void Destroy();

		void Init();
		void Render(SRenderData& aRenderData);

	private:
		void SetShadowMatrices(const CCamera & aCamera, const CCameraInstance & aCameraInstance, const CU::Vector3f & aToLightDirection);
		void BindClosestLights(const CU::Vector3f & aPos, SRenderData & aRenderData);
		struct SInstanceBufferData
		{
			CU::Matrix44f myToWorld;
		};
		struct SShadowBufferData
		{
			CU::Matrix44f depthProj;
			CU::Matrix44f depthView;
			CU::Matrix44f toWorld;
			CU::Vector2f resolution;
		private:
			float pad[2];
		};
		struct SBoneBufferData
		{
			unsigned int useAnimation;
			unsigned int crap[3];
			CU::Matrix44f bones[128];
		};
		struct SBoneBufferDataOnlyBool
		{
			unsigned int useAnimation;
			unsigned int crap[3];
		};

		CU::Matrix44f myShadowProjection;
		CU::Matrix44f myLightView;
		unsigned int myCBufferID;
		unsigned int myShadowCBufferID;
		unsigned int mySpotlightCBufferID;
		unsigned int myPointlightCBufferID;
		unsigned int myDirLightCBufferID;
		unsigned int myInstanceBonesCBufferID;
		ID3D11DeviceContext* myContext;
	};
}}
