#include "stdafx.h"
#include "FullscreenRenderer.h"
#include "../ResourceManager/ResourceManager.h"
#include "../ShaderEffects/ShaderEffectFactory.h"
#include "../ShaderEffects/ShaderEffectInstance.h"
#include "../ShaderEffects/ShaderEffect.h"
#include "GraphicsEngineInterface.h"

namespace sce { namespace gfx {

	CFullscreenRenderer::CFullscreenRenderer()
		: myBloomAddShader(nullptr)
		, myAddShader(nullptr)
		, myVertexShader(nullptr)
		, myCopyShader(nullptr)
		, myLuminanceShader(nullptr)
		, myVBuffer(nullptr)
		, myContext(nullptr)
		, myLUT(nullptr)
		, myIBuffer(nullptr)
		, myCBuffer(nullptr)
		, mySettingsCBuffer(nullptr)
		, myGBlurHShader(nullptr)
		, myGBlurVShader(nullptr)
		, myFXAAShader(nullptr)
		, myTonemapShader(nullptr)
		, myTonemapLumShader(nullptr)
		, myColorGradingShader(nullptr)
		, myColorGradeCBuffer(nullptr)
	{
	}


	CFullscreenRenderer::~CFullscreenRenderer()
	{
		sce_delete(myBloomAddShader);
		sce_delete(myAddShader);
		sce_delete(myVertexShader);
		sce_delete(myCopyShader);
		sce_delete(myLuminanceShader);
		sce_delete(myVBuffer);
		sce_delete(myIBuffer);
		sce_delete(myCBuffer);
		sce_delete(mySettingsCBuffer);
		sce_delete(myGBlurHShader);
		sce_delete(myGBlurVShader);
		sce_delete(myFXAAShader);
		sce_delete(myTonemapShader);
		sce_delete(myTonemapLumShader);
		sce_delete(myColorGradingShader);
		sce_delete(myColorGradeCBuffer);
		sce_delete(myLUT);
		sce_delete(mySSAOShader);
		sce_delete(mySSAOBlurShader);
		sce_delete(mySSAORandom); 
	}

	void CFullscreenRenderer::Destroy()
	{
		sce_delete(myBloomAddShader);
		sce_delete(myAddShader);
		sce_delete(myVertexShader);
		sce_delete(myCopyShader);
		sce_delete(myLuminanceShader);
		sce_delete(myVBuffer);
		sce_delete(myIBuffer);
		sce_delete(myCBuffer);
		sce_delete(mySettingsCBuffer);
		sce_delete(myGBlurHShader);
		sce_delete(myGBlurVShader);
		sce_delete(myFXAAShader);
		sce_delete(myTonemapShader);
		sce_delete(myTonemapLumShader);
		sce_delete(myColorGradingShader);
		sce_delete(myColorGradeCBuffer);
		sce_delete(myLUT);
		sce_delete(mySSAOShader);
		sce_delete(mySSAOBlurShader);
		sce_delete(mySSAORandom);
	}

	void CFullscreenRenderer::Init()
	{
		myContext = CDirect3D11::GetAPI()->GetContext();
		myVBuffer = sce_new(CDX11VertexBuffer());
		myIBuffer = sce_new(CDX11IndexBuffer());
		myCBuffer = sce_new(CDX11ConstantBuffer());
		mySettingsCBuffer = sce_new(CDX11ConstantBuffer());
		myColorGradeCBuffer = sce_new(CDX11ConstantBuffer());
		myVertexShader = sce_new(CDX11Shader());
		myCopyShader = sce_new(CDX11Shader());
		myLuminanceShader = sce_new(CDX11Shader());
		myBloomAddShader = sce_new(CDX11Shader());
		myAddShader = sce_new(CDX11Shader());
		myGBlurHShader = sce_new(CDX11Shader());
		myGBlurVShader = sce_new(CDX11Shader());
		myFXAAShader = sce_new(CDX11Shader());
		myTonemapShader = sce_new(CDX11Shader());
		myTonemapLumShader = sce_new(CDX11Shader());
		myColorGradingShader = sce_new(CDX11Shader());
		myLUT = sce_new(CDX11Texture());
		mySSAOShader = sce_new(CDX11Shader());
		mySSAOBlurShader = sce_new(CDX11Shader());
		mySSAORandom = sce_new(CDX11Texture());

		myVBuffer->CreateQuad();
		myIBuffer->CreateQuadIndex();
		{
			const D3D_SHADER_MACRO define[] =
			{
				{"FX_VERTEX", "1" },
				{ nullptr, nullptr }
			};
			myVertexShader->InitVertex("Data/Shaders/PostFX.hlsl", "VSMain", define);
		}
		{
			const D3D_SHADER_MACRO define[] =
			{
				{"FX_BLOOMADD", "1"},
				{ nullptr, nullptr }
			};
			myBloomAddShader->InitPixel("Data/Shaders/PostFX.hlsl", "PS_FXBloomAdd", define);
		}
		{
			const D3D_SHADER_MACRO define[] =
			{
				{ "FX_ADD", "1" },
				{ nullptr, nullptr }
			};
			myAddShader->InitPixel("Data/Shaders/PostFX.hlsl", "PS_FXAdd", define);
		}
		{
			const D3D_SHADER_MACRO define[] =
			{
				{"FX_COPY", "1"},
				{ nullptr, nullptr }
			};
			myCopyShader->InitPixel("Data/Shaders/PostFX.hlsl", "PS_FXCopy", define);
		}
		{
			const D3D_SHADER_MACRO define[] =
			{
				{"FX_LUMINANCE", "1"},
				{ nullptr, nullptr }
			};
			myLuminanceShader->InitPixel("Data/Shaders/PostFX.hlsl", "PS_FXLuminance", define);
		}
		{
			const D3D_SHADER_MACRO define[] =
			{
				{"FX_GBLURH", "1"},
				{ nullptr, nullptr }
			};
			myGBlurHShader->InitPixel("Data/Shaders/PostFX.hlsl", "PS_FXGaussianBlurH", define);
		}
		{
			const D3D_SHADER_MACRO define[] =
			{
				{"FX_GBLURV", "1"},
				{ nullptr, nullptr }
			};
			myGBlurVShader->InitPixel("Data/Shaders/PostFX.hlsl", "PS_FXGaussianBlurV", define);
		}
		{
			const D3D_SHADER_MACRO define[] =
			{
				{ "FX_TONEMAP", "1" },
				{ nullptr, nullptr }
			};
			myTonemapShader->InitPixel("Data/Shaders/PostFX.hlsl", "PS_FXTonemap", define);
		}
		{
			const D3D_SHADER_MACRO define[] =
			{
				{ "FX_TONEMAPLUM", "1" },
				{ nullptr, nullptr }
			};
			myTonemapLumShader->InitPixel("Data/Shaders/PostFX.hlsl", "PS_FXTonemapLuminance", define);
		}
		{
			const D3D_SHADER_MACRO define[] =
			{
				{ "FX_SSAO", "1" },
				{ nullptr, nullptr }
			};
			mySSAOShader->InitPixel("Data/Shaders/FXSSAO.hlsl", "PS_FXSSAO", define);
		}
		{
			const D3D_SHADER_MACRO define[] =
			{
				{ "FX_SSAOBLUR", "1" },
				{ nullptr, nullptr }
			};
			mySSAOBlurShader->InitPixel("Data/Shaders/FXSSAO.hlsl", "PS_FXSSAOBlur", define);
		}

		myFXAAShader->InitPixel("Data/Shaders/FXAA.hlsl", "PS_FXAA");
		myColorGradingShader->InitPixel("Data/Shaders/FXColorGradingPS.hlsl", "PS_FXColorGrading");
		if (mySSAORandom->CreateTexture("Data/Misc/ssao_rand.dds", ETextureType::SSAORand) == false)
		{
			RESOURCE_LOG("ERROR! No SSAORand set.");
		}

		if (myLUT->CreateTexture("Data/Misc/colorGrading.dds", ETextureType::LUT) == false)
		{
			RESOURCE_LOG("ERROR! No LUT set.");
		}

		myPostFXSettings.bloomCutoff = 1.2f;
		myPostFXSettings.bloomIntensity = 1.0f;

	}

	void CFullscreenRenderer::Render(const EFXType aFXType, std::array<CDX11FullscreenTexture*, 2> aFullscreenTexture)
	{
		if (myVertexShader->IsLoading())
		{
			return;
		}
		myContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
		myVBuffer->Bind();
		myIBuffer->Bind();
		myVertexShader->Bind();

		switch (aFXType)
		{
		case EFXType::Copy:
			CopyPass(aFullscreenTexture[0]);
			break;
		case EFXType::Luminance:
			LuminancePass(aFullscreenTexture[0]);
			break;
		case EFXType::BloomAdd:
			BloomAddPass(aFullscreenTexture);
			break;
		case EFXType::GaussianBlurHorizontal4:
			GaussianBlurHPass(aFullscreenTexture[0], 0);
			break;
		case EFXType::GaussianBlurVertical4:
			GaussianBlurVPass(aFullscreenTexture[0], 0);
			break;
		case EFXType::GaussianBlurHorizontal8:
			GaussianBlurHPass(aFullscreenTexture[0], 1);
			break;
		case EFXType::GaussianBlurVertical8:
			GaussianBlurVPass(aFullscreenTexture[0], 1);
			break; 
		case EFXType::GaussianBlurHorizontal16:
			GaussianBlurHPass(aFullscreenTexture[0], 2);
			break;
		case EFXType::GaussianBlurVertical16:
			GaussianBlurVPass(aFullscreenTexture[0], 2);
			break;
		case EFXType::FXAA:
			FXAAPass(aFullscreenTexture[0]);
			break;
		case EFXType::TonemapLum:
			TonemapLumPass(aFullscreenTexture[0]);
			break;
		case EFXType::Tonemap:
			TonemapPass(aFullscreenTexture);
			break;
		case EFXType::ColorGrading:
			ColorGradingPass(aFullscreenTexture[0]);
			break; 
		case EFXType::SSAO:
			SSAOPass();
			break;
		case EFXType::SSAOBlur:
			SSAOBlurPass(aFullscreenTexture[0]);
			break;
		case EFXType::Add:
			AddPass(aFullscreenTexture);
			break;
		}

		mySettingsCBuffer->SetData(myPostFXSettings);
		mySettingsCBuffer->BindPS(4);
		myContext->DrawIndexed(myIBuffer->GetIndexCount(), 0, 0);
	}

	void CFullscreenRenderer::RenderEffect(CShaderEffectInstance & aEffect, CDX11FullscreenTexture* aFullscreenTexture)
	{
		if (myVertexShader->IsLoading())
		{
			return;
		}
		myContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
		myVBuffer->Bind();
		myIBuffer->Bind();
		myVertexShader->Bind();

		CShaderEffect& shader = CShaderEffectFactory::Get()->GetEffectWithID(aEffect.myShaderEffectID);
		CDX11Shader& pixelShader(CResourceManager::Get()->GetShaderWithID(shader.myPixelShaderID));
		if (pixelShader.IsLoading())
		{
			return;
		}
		pixelShader.Bind();
		
		CDX11ConstantBuffer& cBuffer(CResourceManager::Get()->GetConstantBufferWithID(shader.myConstantBufferID));
		
		switch (aEffect.myEffect)
		{
		case CShaderEffectInstance::eGlitchScreen:
			cBuffer.SetData(aEffect.myGlitchData);
			break;
		case CShaderEffectInstance::eStatic:
			cBuffer.SetData(aEffect.myStaticData);
			break;
		}

		cBuffer.BindPS(shader.myConstantBufferSlot);

		CDX11Texture& noiseTexture(CResourceManager::Get()->GetTextureWithID(shader.myTextureID));
		aFullscreenTexture->SetAsResourceOnSlot(0);
		noiseTexture.Bind(1);

		myContext->DrawIndexed(myIBuffer->GetIndexCount(), 0, 0);
	}

	void CFullscreenRenderer::AddPass(std::array<CDX11FullscreenTexture*, 2>& aFullscreenTexture)
	{
		if (myAddShader->IsLoading())
		{
			return;
		}
		myAddShader->Bind();
		aFullscreenTexture[0]->SetAsResourceOnSlot(0);
		aFullscreenTexture[1]->SetAsResourceOnSlot(1);
		CDirect3D11::GetAPI()->SetSamplerState(ESamplerState::Default);
	}

	void CFullscreenRenderer::CopyPass(CDX11FullscreenTexture* aTexture)
	{
		if (myCopyShader->IsLoading())
		{
			return;
		}
		myCopyShader->Bind();
		aTexture->SetAsResourceOnSlot(0);
		CDirect3D11::GetAPI()->SetSamplerState(ESamplerState::Default);
	}

	void CFullscreenRenderer::LuminancePass(CDX11FullscreenTexture* aTexture)
	{
		if (myLuminanceShader->IsLoading())
		{
			return;
		}
		myLuminanceShader->Bind();
		aTexture->SetAsResourceOnSlot(0);
		CDirect3D11::GetAPI()->SetSamplerState(ESamplerState::Default);
	}

	void CFullscreenRenderer::BloomAddPass(std::array<CDX11FullscreenTexture*, 2>& aFullscreenTexture)
	{
		if (myBloomAddShader->IsLoading())
		{
			return;
		}
		myBloomAddShader->Bind();
		aFullscreenTexture[0]->SetAsResourceOnSlot(0);
		aFullscreenTexture[1]->SetAsResourceOnSlot(1);
		CDirect3D11::GetAPI()->SetSamplerState(ESamplerState::Default);
	}

	void CFullscreenRenderer::GaussianBlurHPass(CDX11FullscreenTexture* aTexture, unsigned int aBlurLevel)
	{
		if (myGBlurHShader->IsLoading())
		{
			return;
		}

		STexelSize tSize;
		tSize.x = aTexture->GetResolution().x;
		tSize.y = aTexture->GetResolution().y;
		myCBuffer->SetData(tSize);
		myCBuffer->BindPS(3);

		myPostFXSettings.blurLevel = aBlurLevel;

		myGBlurHShader->Bind();
		aTexture->SetAsResourceOnSlot(0);
		CDirect3D11::GetAPI()->SetSamplerState(ESamplerState::Default);
	}

	void CFullscreenRenderer::GaussianBlurVPass(CDX11FullscreenTexture* aTexture, unsigned int aBlurLevel)
	{
		if (myGBlurVShader->IsLoading())
		{
			return;
		}
		STexelSize tSize;
		tSize.x = aTexture->GetResolution().x;
		tSize.y = aTexture->GetResolution().y;
		myCBuffer->SetData(tSize);
		myCBuffer->BindPS(3);

		myPostFXSettings.blurLevel = aBlurLevel;

		myGBlurVShader->Bind();
		aTexture->SetAsResourceOnSlot(0);
		CDirect3D11::GetAPI()->SetSamplerState(ESamplerState::Default);
	}

	void CFullscreenRenderer::FXAAPass(CDX11FullscreenTexture* aTexture)
	{
		if (myFXAAShader->IsLoading())
		{
			return;
		}
		STexelSize tSize;
		tSize.x = aTexture->GetResolution().x;
		tSize.y = aTexture->GetResolution().y;
		myCBuffer->SetData(tSize);
		myCBuffer->BindPS(3);

		myFXAAShader->Bind();
		aTexture->SetAsResourceOnSlot(0);
		CDirect3D11::GetAPI()->SetSamplerState(ESamplerState::Default);
	}

	void CFullscreenRenderer::TonemapLumPass(CDX11FullscreenTexture* aTexture)
	{
		if (myTonemapLumShader->IsLoading())
		{
			return;
		}

		myTonemapLumShader->Bind();
		aTexture->SetAsResourceOnSlot(0);
		CDirect3D11::GetAPI()->SetSamplerState(ESamplerState::Default);
	}

	void CFullscreenRenderer::ColorGradingPass(CDX11FullscreenTexture* aTexture)
	{
		if (myColorGradingShader->IsLoading())
		{
			return;
		}
		SHalfPixels hPix;
		hPix.numHalfPixels = (16.f * 2.0f/*aTexture->GetResolution().x / 2.f) * (aTexture->GetResolution().y / 2.f*/);
		myColorGradeCBuffer->SetData(hPix);
		myColorGradeCBuffer->BindPS(3);

		myColorGradingShader->Bind();
		aTexture->SetAsResourceOnSlot(0);
		myLUT->Bind(1);
		CDirect3D11::GetAPI()->SetSamplerState(ESamplerState::Default);
	}

	void CFullscreenRenderer::SSAOPass()
	{
		if (mySSAOShader->IsLoading())
		{
			return;
		}
		CDirect3D11::GetAPI()->SetSamplerState(ESamplerState::Anisotropic_x16_Mirrored);
		STexelSize tSize;
		tSize.x = CGraphicsEngineInterface::GetResolution().x;
		tSize.y = CGraphicsEngineInterface::GetResolution().y;
		myCBuffer->SetData(tSize);
		myCBuffer->BindPS(3);
		mySSAOShader->Bind();
		mySSAORandom->Bind(6);
		CDirect3D11::GetAPI()->SetSamplerState(ESamplerState::Default);
	}

	void CFullscreenRenderer::SSAOBlurPass(CDX11FullscreenTexture* aTexture)
	{
		if (mySSAOBlurShader->IsLoading())
		{
			return;
		}
		STexelSize tSize;
		tSize.x = CGraphicsEngineInterface::GetResolution().x;
		tSize.y = CGraphicsEngineInterface::GetResolution().y;
		myCBuffer->SetData(tSize);
		myCBuffer->BindPS(3);
		aTexture->SetAsResourceOnSlot(0);
		mySSAOBlurShader->Bind();
		CDirect3D11::GetAPI()->SetSamplerState(ESamplerState::Default);
	}

	void CFullscreenRenderer::TonemapPass(std::array<CDX11FullscreenTexture*, 2>& aFullscreenTexture)
	{
		if (myTonemapShader->IsLoading())
		{
			return;
		}

		myTonemapShader->Bind();
		aFullscreenTexture[0]->SetAsResourceOnSlot(0);
		aFullscreenTexture[1]->SetAsResourceOnSlot(1);

		CDirect3D11::GetAPI()->SetSamplerState(ESamplerState::Anisotropic_x16);
	}
}}