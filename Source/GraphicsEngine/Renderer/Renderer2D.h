#pragma once

struct ID3D11DeviceContext;
namespace sce { namespace gfx {
	class CSprite;
	class CRenderer2D
	{
	public:
		CRenderer2D();
		~CRenderer2D();

		void Init();
		void Render(CU::GrowingArray<sce::gfx::CSprite, unsigned int>& aListToRender);

	private:
		struct SSpriteData
		{
			CU::Vector4f myPositionAndRotation;
			CU::Vector4f myTextureSizeAndTargetSize;
			CU::Vector4f myColor;
			CU::Vector4f mySizeAndPivot;
			CU::Vector4f myCrop;
		};
	private:
		unsigned int myCBufferID;
		ID3D11DeviceContext* myContext;
	};

}}