#include "stdafx.h"
#include "DebugRenderer.h"
#ifndef USE_DEBUG_RENDERER

#else
#include "DirectXFramework\Direct3D11.h"
#include "DirectXFramework\API\DX11Shader.h"
#include "ResourceManager\ResourceManager.h"
#include "Model\Loading\ModelFactory.h"
#include "Model\Model.h"
#include "Camera\Camera.h"
#include "Material.h"

#ifdef max
#undef max
#endif // max

namespace sce {
	namespace gfx {
		CDebugRenderer::CDebugRenderer()
			: myContext(nullptr)
			, myLineVBuffer(nullptr)
			, myRect3DVBuffer(nullptr)
			, my2DLineVBuffer(nullptr)
			, myCubeVBuffer(nullptr)
			, myLinePixelShader(nullptr)
			, myLineVertexShader(nullptr)
			, myLineGShader(nullptr)
			, my2DLineVertexShader(nullptr)
			, my2DLineGShader(nullptr)
			, my3DRectGShader(nullptr)
			, myCubeGShader(nullptr)
			, myCBufferID(std::numeric_limits<decltype(myCBufferID)>::max())
		{
		}

		CDebugRenderer::~CDebugRenderer()
		{
			Destroy();
		}

		void CDebugRenderer::Destroy()
		{
			sce_delete(myLineVBuffer);
			sce_delete(myCubeVBuffer);
			sce_delete(myLineVertexShader);
			sce_delete(myLinePixelShader);
			sce_delete(myLineGShader);
			sce_delete(myCubeGShader);
			sce_delete(my2DLineVertexShader);
			sce_delete(my2DLineVBuffer);
			sce_delete(myRect3DVBuffer);
			sce_delete(my2DLineGShader);
			sce_delete(my3DRectGShader);
		}

		void CDebugRenderer::Init()
		{
			myContext = CDirect3D11::GetAPI()->GetContext();
			myLineVBuffer = sce_new(CDX11VertexBuffer());
			myCubeVBuffer = sce_new(CDX11VertexBuffer());
			my2DLineVBuffer = sce_new(CDX11VertexBuffer());
			myRect3DVBuffer = sce_new(CDX11VertexBuffer());
			myLineVertexShader = sce_new(CDX11Shader());
			my2DLineVertexShader = sce_new(CDX11Shader());
			myLinePixelShader = sce_new(CDX11Shader());
			myLineGShader = sce_new(CDX11Shader());
			myCubeGShader = sce_new(CDX11Shader());
			my2DLineGShader = sce_new(CDX11Shader());
			my3DRectGShader = sce_new(CDX11Shader());

			myLinePixelShader->InitPixel("Data/Shaders/DebugShapesPS.hlsl", "PSMain");
			myLineVertexShader->InitVertex("Data/Shaders/DebugShapesVS.hlsl", "VSMain");
			my2DLineVertexShader->InitVertex("Data/Shaders/DebugShapes2DVS.hlsl", "VSMain");

			myLineGShader->InitGeometry("Data/Shaders/DebugShapesLinesGS.hlsl", "GSMain");
			my3DRectGShader->InitGeometry("Data/Shaders/DebugShapesRectangle3DGS.hlsl", "GSMain");
			my2DLineGShader->InitGeometry("Data/Shaders/DebugShapes2DLinesGS.hlsl", "GSMain");
			myCubeGShader->InitGeometry("Data/Shaders/DebugShapesCubesGS.hlsl", "GSMain");

			my2DLineVBuffer->CreateDebugBuffer(MAX_DEBUG2DLINES, EBufferUsage::DYNAMIC);
			myLineVBuffer->CreateDebugBuffer(MAX_DEBUGLINES, EBufferUsage::DYNAMIC);
			myCubeVBuffer->CreateDebugBuffer(MAX_DEBUGCUBES, EBufferUsage::DYNAMIC);
			myRect3DVBuffer->CreateDebugBuffer(MAX_DEBUGRECTANGLE3D, EBufferUsage::DYNAMIC);

			myCBufferID = CResourceManager::Get()->GetConstantBufferID("DebugRenderer");
		}


		void CDebugRenderer::Render(SRenderData& aRenderData)
		{
			if (myContext == nullptr)
			{
				return;
			}
			if (aRenderData.myCameraInstance.myCameraID == UINT_MAX)
			{
				return;
			}
			if (myLinePixelShader->IsLoading())
			{
				return;
			}
			if (myLineVertexShader->IsLoading())
			{
				return;
			}
			if (myLineGShader->IsLoading())
			{
				return;
			}
			if (myCubeGShader->IsLoading())
			{
				return;
			}

			CDirect3D11::GetAPI()->DisableDepth();
			CDirect3D11::GetAPI()->DisableBlending();

			//2DLines
			myContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_LINELIST);
			myLinePixelShader->Bind();
			if (aRenderData.myDebugData.myDebug2DLineData.Size() > MAX_DEBUG2DLINES)
			{
				ENGINE_LOG("WARNING! You are trying to render more 2D-Lines than the maximum allowed, everything passed the limit is cut: Trying to print ( %u / %u )", aRenderData.myDebugData.myDebug2DLineData.Size(), MAX_DEBUG2DLINES);
			}
			if (aRenderData.myDebugData.myDebug2DLineData.Empty() == false)
			{
				my2DLineVBuffer->SetData(aRenderData.myDebugData.myDebug2DLineData.GetRawData()
					, (sizeof(aRenderData.myDebugData.myDebug2DLineData[0]) * aRenderData.myDebugData.myDebug2DLineData.size()));
				my2DLineVBuffer->Bind();

				my2DLineVertexShader->Bind();

				my2DLineGShader->Bind();

				myContext->Draw(aRenderData.myDebugData.myDebug2DLineData.Size(), 0);
			}

			//Not 2D
			myLineVertexShader->Bind();

			CDirect3D11::GetAPI()->EnableReadOnlyDepth();
			//LINES
			if (aRenderData.myDebugData.myDebugLineData.Size() > MAX_DEBUGLINES)
			{
				ENGINE_LOG("WARNING! You are trying to render more Lines than the maximum allowed, everything passed the limit is cut: Trying to print ( %u / %u )", aRenderData.myDebugData.myDebugLineData.Size(), MAX_DEBUGLINES);
			}
			if (aRenderData.myDebugData.myDebugLineData.Empty() == false)
			{
				myLineVBuffer->SetData(aRenderData.myDebugData.myDebugLineData.GetRawData()
					, (sizeof(aRenderData.myDebugData.myDebugLineData[0]) * aRenderData.myDebugData.myDebugLineData.size()));
				myLineVBuffer->Bind();

				myLineGShader->Bind();

				myContext->Draw(aRenderData.myDebugData.myDebugLineData.Size(), 0);
			}

			//CUBES
			myContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_POINTLIST);
			if (aRenderData.myDebugData.myDebugCubeData.Size() > MAX_DEBUGCUBES)
			{
				ENGINE_LOG("WARNING! You are trying to render more cubes than the maximum allowed, everything passed the limit is cut: Trying to print ( %u / %u )", aRenderData.myDebugData.myDebugCubeData.Size(), MAX_DEBUGCUBES);
			}
			if (aRenderData.myDebugData.myDebugCubeData.Empty() == false)
			{
				myCubeVBuffer->SetData(aRenderData.myDebugData.myDebugCubeData.GetRawData()
					, (sizeof(aRenderData.myDebugData.myDebugCubeData[0]) * aRenderData.myDebugData.myDebugCubeData.size()));
				myCubeVBuffer->Bind();

				myCubeGShader->Bind();

				myContext->Draw(aRenderData.myDebugData.myDebugCubeData.Size(), 0);
			}

			//RECTANGLE 3D
			if (aRenderData.myDebugData.myDebugRectangle3DData.Size() > MAX_DEBUGRECTANGLE3D)
			{
				ENGINE_LOG("WARNING! You are trying to render more 3DRectangles than the maximum allowed, everything passed the limit is cut: Trying to print ( %u / %u )", aRenderData.myDebugData.myDebugRectangle3DData.Size(), MAX_DEBUGRECTANGLE3D);
			}
			if (aRenderData.myDebugData.myDebugRectangle3DData.Empty() == false)
			{
				myRect3DVBuffer->SetData(aRenderData.myDebugData.myDebugRectangle3DData.GetRawData()
					, (sizeof(aRenderData.myDebugData.myDebugRectangle3DData[0]) * aRenderData.myDebugData.myDebugRectangle3DData.size()));
				myRect3DVBuffer->Bind();

				my3DRectGShader->Bind();

				myContext->Draw(aRenderData.myDebugData.myDebugRectangle3DData.Size(), 0);
			}
			RenderSpheres(aRenderData);

			myContext->GSSetShader(nullptr, nullptr, 0);
			CDirect3D11::GetAPI()->SetDefaultDepth();
		}

		/* PRIVATE FUNCTIONS */

		void CDebugRenderer::RenderSpheres(SRenderData& aRenderData)
		{
			myContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
			myContext->GSSetShader(nullptr, nullptr, 0);

			CModelFactory* modelFactoryPtr = CModelFactory::Get();
			CResourceManager* resourceMngPtr = CResourceManager::Get();
			if (resourceMngPtr == nullptr)
			{
				ENGINE_LOG("[CDebugRenderer] ERROR! resource manager is not created.");
				return;
			}
			if (modelFactoryPtr == nullptr)
			{
				ENGINE_LOG("[CDebugRenderer] ERROR! model factory is not created.");
				return;
			}
			CModelInstance* currInstancePtr(nullptr);
			CModel* currModelPtr(nullptr);
			CDX11ConstantBuffer& instanceCBuffer(resourceMngPtr->GetConstantBufferWithID(myCBufferID));
			SInstanceBufferData instanceData;

			for (unsigned int i(0); i < aRenderData.myDebugData.myDebugSphereData.Size(); ++i)
			{
				currInstancePtr = &aRenderData.myDebugData.myDebugSphereData[i];
				if (currInstancePtr->IsLoading())
				{
					continue;
				}
				if (currInstancePtr->myModelID == UINT_MAX)
				{
					ENGINE_LOG("[CDebugRenderer] WARNING! Failed to render a debug model because model ID is invalid. Model name: \"%s\".", currInstancePtr->myModelName.c_str());
					continue;
				}
				if (currInstancePtr->myMaterialID == UINT_MAX)
				{
					ENGINE_LOG("[CDebugRenderer] WARNING! Failed to render a debug model because material ID is invalid. Model name: \"%s\".", currInstancePtr->myModelName.c_str());
					continue;
				}

				currModelPtr = &resourceMngPtr->GetModelWithID(currInstancePtr->myModelID);
				if (currModelPtr->myVertexBufferID == UINT_MAX)
				{
					ENGINE_LOG("[CDebugRenderer] WARNING! Failed to render a debug model because it vertex buffer ID is invalid. Model name: \"%s\".", currInstancePtr->myModelName.c_str());
					continue;
				}

				CDX11VertexBuffer& vb(resourceMngPtr->GetVertexBufferWithID(currModelPtr->myVertexBufferID));
				CDX11IndexBuffer& ib(resourceMngPtr->GetIndexBufferWithID(currModelPtr->myIndexBufferID));
				CMaterial& mat(modelFactoryPtr->GetMaterialWithID(currInstancePtr->myMaterialID));
				CU::Matrix44f orientation(currInstancePtr->myOrientation);
				orientation.ScaleBy(currInstancePtr->myScale);
				instanceData.myToWorld = orientation;

				if (!mat.Bind())
				{
					ENGINE_LOG("[CDebugRenderer] WARNING! Failed to render a debug model because the material could not be bound. Model name: \"%s\".", currInstancePtr->myModelName.c_str());
					continue;
				}
				instanceCBuffer.SetData(instanceData);
				instanceCBuffer.BindVS(3);

				vb.Bind();
				ib.Bind();

				myContext->DrawIndexed(ib.GetIndexCount(), 0, 0);
			}
		}
	}
}
#endif // !USE_DEBUG_RENDERER
