#include "stdafx.h"
#include "ParticleRenderer.h"
#include "Particles\ParticleEmitterInstance.h"
#include "ResourceManager\ResourceManager.h"
#include "Camera\CameraInstance.h"
#include "Camera\Camera.h"
#include "Particles\ParticleEmitter.h"
#include "Particles\ParticleFactory.h"
#include "Particles\StreakEmitterInstance.h"
#include "Particles\StreakEmitter.h"
#include "Material.h"

namespace sce { namespace gfx {

	CParticleRenderer::CParticleRenderer()
		: myContext(nullptr)
	{
	}


	CParticleRenderer::~CParticleRenderer()
	{
	}

	void sce::gfx::CParticleRenderer::Init()
	{
		myContext = CDirect3D11::GetAPI()->GetContext();
		myCBufferID = CResourceManager::Get()->GetConstantBufferID("ParticleRenderer");
	}

	void sce::gfx::CParticleRenderer::Render(CCameraInstance& aCamera, CU::GrowingArray<CParticleEmitterInstance, unsigned int>& aParticleListToRender,
		CU::GrowingArray<CStreakEmitterInstance, unsigned int>& aStreakListToRender)
	{
		CDirect3D11::GetAPI()->EnableAdditiveblend();
		CDirect3D11::GetAPI()->EnableReadOnlyDepth();
		if (myContext == nullptr)
		{
			return;
		}
		if (aCamera.myCameraID == UINT_MAX)
		{
/*
#ifdef _DEBUG
			ENGINE_LOG("ERROR! CameraInstance does not have a valid ID.");
#endif*/
			return;
		}

		CResourceManager* resourceManager = CResourceManager::Get();

		RenderParticles(aCamera, aParticleListToRender);
		RenderStreaks(aCamera, aStreakListToRender);
		

		myContext->GSSetShader(nullptr, nullptr, 0);
		CDirect3D11::GetAPI()->DisableBlending();
		CDirect3D11::GetAPI()->SetDefaultDepth();
	}

	void CParticleRenderer::RenderParticles(CCameraInstance & aCamera, CU::GrowingArray<CParticleEmitterInstance, unsigned int>& aListToRender)
	{
		aCamera;
		myContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_POINTLIST);

		CResourceManager* resourceManager = CResourceManager::Get();
		SInstanceBufferData instanceData;
		CDX11ConstantBuffer& instanceCBuffer(resourceManager->GetConstantBufferWithID(myCBufferID));

		for (unsigned int i = 0; i < aListToRender.Size(); ++i)
		{
			CParticleEmitterInstance& instance = aListToRender[i];
			if (instance.myEmitterID == UINT_MAX)
			{
				continue;
			}
			if (instance.myParticles.Size() == 0)
			{
				continue;
			}


			CParticleEmitter& emittor(CParticleFactory::Get()->GetEmittorWithID(instance.myEmitterID));
			CDX11VertexBuffer& vb(*(emittor.myVBuffer));
			vb.SetData(instance.myParticles.GetRawData());

			CDX11Shader& gs(resourceManager->GetShaderWithID(emittor.myGeometryShaderID));

			CMaterial& mat(CModelFactory::Get()->GetMaterialWithID(emittor.myMaterialID));
			if (mat.IsLoading())
			{
				continue;
			}
			mat.Bind();

			if (gs.IsLoading())
			{
				continue;
			}

			instanceData.myToWorld = instance.myOrientation;
			instanceCBuffer.SetData(instanceData);
			instanceCBuffer.BindVS(3);

			vb.Bind();
			gs.Bind();

			myContext->Draw(instance.myParticles.Size(), 0);
		}
	}

	void CParticleRenderer::RenderStreaks(CCameraInstance& aCamera, CU::GrowingArray<CStreakEmitterInstance, unsigned int>& aListToRender)
	{
		aCamera;
		myContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_LINESTRIP_ADJ);

		CResourceManager* resourceManager = CResourceManager::Get();
		SInstanceBufferData instanceData;
		CDX11ConstantBuffer& instanceCBuffer(resourceManager->GetConstantBufferWithID(myCBufferID));

		for (unsigned int i = 0; i < aListToRender.Size(); ++i)
		{
			CStreakEmitterInstance& instance = aListToRender[i];
			if (instance.myEmitterID == UINT_MAX)
			{
				continue;
			}
			if (instance.myPoints.Size() == 0)
			{
				continue;
			}


			CStreakEmitter& emitter(CParticleFactory::Get()->GetSEmittorWithID(instance.myEmitterID));
			CDX11VertexBuffer& vb(*(emitter.myVBuffer));
			vb.SetData(instance.myPoints.GetRawData());

			if (emitter.myVertexShaderID == UINT_MAX)
			{
				continue;
			}
			if (emitter.myPixelShaderID == UINT_MAX)
			{
				continue;
			}
			if (emitter.myGeometryShaderID == UINT_MAX)
			{
				continue;
			}
			CDX11Shader& vs(resourceManager->GetShaderWithID(emitter.myVertexShaderID));
			CDX11Shader& ps(resourceManager->GetShaderWithID(emitter.myPixelShaderID));
			CDX11Shader& gs(resourceManager->GetShaderWithID(emitter.myGeometryShaderID));

			if (vs.IsLoading())
			{
				continue;
			}
			if (ps.IsLoading())
			{
				continue;
			}
			if (gs.IsLoading())
			{
				continue;
			}

			instanceData.myToWorld = instance.myOrientation;
			instanceCBuffer.SetData(instanceData);
			instanceCBuffer.BindVS(3);

			if (emitter.myTextureID != UINT_MAX)
			{
				CDX11Texture& texture(resourceManager->GetTextureWithID(emitter.myTextureID));
				texture.Bind();
			}

			vb.Bind();
			vs.Bind();
			gs.Bind();
			ps.Bind();

			myContext->Draw(instance.myPoints.Size(), 0);
		}
	}

}}