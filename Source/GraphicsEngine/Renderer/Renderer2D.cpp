#include "stdafx.h"
#include "Renderer2D.h"
#include "Sprite/Sprite.h"
#include "Sprite/SpriteFactory.h"
#include "Model/Loading/ModelFactory.h"
#include "ResourceManager/ResourceManager.h"
#include "Sprite/TexturedQuad/TexturedQuad.h"

namespace sce { namespace gfx {

	CRenderer2D::CRenderer2D()
		: myContext(nullptr)
	{
	}


	CRenderer2D::~CRenderer2D()
	{
	}


	void CRenderer2D::Init()
	{
		myContext = CDirect3D11::GetAPI()->GetContext();
		myCBufferID = CResourceManager::Get()->GetConstantBufferID("Renderer2D");
	}


	void CRenderer2D::Render(CU::GrowingArray<sce::gfx::CSprite, unsigned int>& aListToRender)
	{
		if (aListToRender.Size() <= 0)
		{
			return;
		}
		CDirect3D11::GetAPI()->DisableDepth();
		CDirect3D11::GetAPI()->EnableAlphablend();
		CDirect3D11::GetAPI()->SetSamplerState(ESamplerState::Default);


		myContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
		CResourceManager* resourceManager(CResourceManager::Get());

		SSpriteData instanceData;
		CDX11ConstantBuffer& instanceCBuffer(resourceManager->GetConstantBufferWithID(myCBufferID));

		for (unsigned int index = 0; index < aListToRender.Size(); ++index)
		{
			if (aListToRender[index].myTexturedQuadID == UINT_MAX)
			{
				continue;
			}

			CSpriteFactory& spriteFactory(CModelFactory::Get()->mySpriteFactory);

			CTexturedQuad& texQuad = spriteFactory.GetTexturedQuadWithID(aListToRender[index].myTexturedQuadID);

			if (texQuad.myVertexBufferID == UINT_MAX)
			{
				continue;
			}
			if (texQuad.myIndexBufferID == UINT_MAX)
			{
				continue;
			}
			if (texQuad.myPixelShaderID == UINT_MAX)
			{
				continue;
			}
			if (texQuad.myVertexShaderID == UINT_MAX)
			{
				continue;
			}

			CDX11VertexBuffer& vBuffer = resourceManager->GetVertexBufferWithID(texQuad.myVertexBufferID);
			CDX11IndexBuffer& iBuffer = resourceManager->GetIndexBufferWithID(texQuad.myIndexBufferID);

			vBuffer.Bind();
			iBuffer.Bind();

			CDX11Shader& pShader = resourceManager->GetShaderWithID(texQuad.myPixelShaderID);
			CDX11Shader& vShader = resourceManager->GetShaderWithID(texQuad.myVertexShaderID);

			vShader.Bind();
			pShader.Bind();

			if (texQuad.myTextureID != UINT_MAX)
			{
				CDX11Texture& texture = resourceManager->GetTextureWithID(texQuad.myTextureID);
				texture.Bind();
			}
			else
			{
				ENGINE_LOG("WARNING! Texture for Sprite could not bind. Textures may appear inaccurate.");
			}

			instanceData.myTextureSizeAndTargetSize.x = texQuad.myTextureSize.x;
			instanceData.myTextureSizeAndTargetSize.y = texQuad.myTextureSize.y;
			instanceData.myTextureSizeAndTargetSize.z = 1920.f;
			instanceData.myTextureSizeAndTargetSize.w = 1080.f;
			instanceData.myPositionAndRotation.x = aListToRender[index].myPosition.x;
			instanceData.myPositionAndRotation.y = aListToRender[index].myPosition.y;
			instanceData.myPositionAndRotation.z = aListToRender[index].myRotation;
			instanceData.myPositionAndRotation.w = 1.f;
			instanceData.myColor = aListToRender[index].myColor;

			instanceData.mySizeAndPivot.x = aListToRender[index].myScale.x;
			instanceData.mySizeAndPivot.y = aListToRender[index].myScale.y;
			instanceData.mySizeAndPivot.z = aListToRender[index].myPivot.x;
			instanceData.mySizeAndPivot.w = aListToRender[index].myPivot.y;
			instanceData.myCrop.x = aListToRender[index].myCrop.x;
			instanceData.myCrop.y = aListToRender[index].myCrop.y;
			instanceData.myCrop.z = aListToRender[index].myCrop.z;
			instanceData.myCrop.w = aListToRender[index].myCrop.w;

			instanceCBuffer.SetData(instanceData);
			instanceCBuffer.BindVS(0);
			
			myContext->DrawIndexed(iBuffer.GetIndexCount(), 0, 0);
		}

		CDirect3D11::GetAPI()->DisableBlending();
		CDirect3D11::GetAPI()->SetDefaultDepth();
	}
}}