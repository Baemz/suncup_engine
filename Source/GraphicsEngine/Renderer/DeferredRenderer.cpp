
#include "stdafx.h"
#include "DeferredRenderer.h"
#include "Camera\Camera.h"
#include "Model\ModelInstance.h"
#include "Model\Model.h"
#include "Lights\DirectionalLight.h"

#include "DirectXFramework\API\DX11Shader.h"
#include "Lights\LightFactory.h"
#include "Utilities\LightStructs.h"
#include "Lights\PointLight.h"
#include "Lights\SpotLight.h"
#include "DirectXFramework\CDirectXMathHelper.h"
#include "Material.h"
#include "DebugTools.h"

#define TIME_DATA_CBUFFER_INDEX 0
#define CAMERA_DATA_CBUFFER_INDEX 1
#define INVERSECAMERA_DATA_CBUFFER_INDEX 2

#define SHADOW_BUFFER_SIZE_X 4096.f
#define SHADOW_BUFFER_SIZE_Y 4096.f

namespace sce { namespace gfx {

	CDeferredRenderer::CDeferredRenderer()
		: myShadowMap(nullptr)
		, myLastMaterialBound(UINT_MAX)
		, myLastVBBound(UINT_MAX)
		, myLastIBBound(UINT_MAX)
	{
		myElapsedTime[0] = 0.0f;
	}


	CDeferredRenderer::~CDeferredRenderer()
	{
		Destroy();
	}

	void CDeferredRenderer::Destroy()
	{
		sce_delete(myVertexShader);
		sce_delete(myEnvLightShader);
		sce_delete(myDirLightShader);
		sce_delete(myVBuffer);
		sce_delete(myIBuffer);
		sce_delete(myEmissiveLightShader);
		sce_delete(myLinearFogShader);
		sce_delete(myShadowPSShader);
		sce_delete(myShadowVSShader);
		sce_delete(myLightModelVShader);
		sce_delete(myLightModelVBuffer);
		sce_delete(myLightModelIBuffer);
		sce_delete(myLightModelInstanceBuffer);
		sce_delete(myShadowMap);
	}

	void CDeferredRenderer::Init()
	{
		auto resourceManager = CResourceManager::Get();
		myContext = CDirect3D11::GetAPI()->GetContext();
		myCBufferID = resourceManager->GetConstantBufferID("DeferredRenderer");
		myDirLightCBufferID = resourceManager->GetConstantBufferID("DeferredRendererDirLight");
		myShadowCBufferID = resourceManager->GetConstantBufferID("DeferredRendererShadow");
		myShadowCBuffer2ID = resourceManager->GetConstantBufferID("DeferredRendererShadow2");
		myTimerCBufferID = resourceManager->GetConstantBufferID("DeferredTimeBuffer");
		myInstanceBonesCBufferID = resourceManager->GetConstantBufferID("DeferredBonesBuffer");
		CDX11ConstantBuffer& instanceBoneCBuffer(resourceManager->GetConstantBufferWithID(myInstanceBonesCBufferID));
		SBoneBufferData initData;
		instanceBoneCBuffer.SetData(initData);
		myEmissiveSettingsCBufferID = resourceManager->GetConstantBufferID("DeferredEmissiveBuffer");
		myCamInvCBufferID = resourceManager->GetConstantBufferID("DeferredInvCamBuffer");

		myShadowMap = sce_new(CDX11FullscreenTexture());
		myShadowMap->Init(CU::Vector2f(SHADOW_BUFFER_SIZE_X, SHADOW_BUFFER_SIZE_Y), DXGI_FORMAT_R32_FLOAT, true, true);
		myVertexShader = sce_new(CDX11Shader());
		myVBuffer = sce_new(CDX11VertexBuffer());
		myIBuffer = sce_new(CDX11IndexBuffer());
		myVBuffer->CreateQuad();
		myIBuffer->CreateQuadIndex();

		myLightModelVShader = sce_new(CDX11Shader());
		myLightModelVShader->InitVertex("Data/Shaders/LightModelVS.hlsl", "VSMain");
		myLightModelVBuffer = sce_new(CDX11VertexBuffer());
		myLightModelIBuffer = sce_new(CDX11IndexBuffer());
		CU::GrowingArray<unsigned int, unsigned int> indexList;
		myLightModelVBuffer->CreateSphere(indexList, { 0.f, 0.f, 0.f }, 1.0f, 12, 12, EBufferUsage::STATIC);
		myLightModelIBuffer->CreateBuffer(indexList);
		myLightModelInstanceBuffer = sce_new(CDX11VertexBuffer());
		myLightModelInstanceBuffer->CreateBuffer(sizeof(SLightInstanceData), 1);

		{
			const D3D_SHADER_MACRO define[] =
			{
				{ "FX_VERTEX", "1" },
				{ nullptr, nullptr }
			};
			myVertexShader->InitVertex("Data/Shaders/PostFX.hlsl", "VSMain", define);
		}

		myDirLightShader = sce_new(CDX11Shader());
		myEnvLightShader = sce_new(CDX11Shader());
		myEmissiveLightShader = sce_new(CDX11Shader());
		myLinearFogShader = sce_new(CDX11Shader());
		myShadowPSShader = sce_new(CDX11Shader());
		myShadowVSShader = sce_new(CDX11Shader());
		myDirLightShader->InitPixel("Data/Shaders/DirectionalLightPS.hlsl", "PSMain");
		myEnvLightShader->InitPixel("Data/Shaders/EnvironmentLightPS.hlsl", "PSMain");
		myEmissiveLightShader->InitPixel("Data/Shaders/EmissiveLightPS.hlsl", "PSMain");
		myLinearFogShader->InitPixel("Data/Shaders/FogPS.hlsl", "PSMain");
		myShadowPSShader->InitPixel("Data/Shaders/ShadowPass.hlsl", "PSMain");
		myShadowVSShader->InitVertex("Data/Shaders/ShadowPass.hlsl", "VSMain");
	}

	void CDeferredRenderer::LightPass(const SRenderData& aRenderData, CDX11FullscreenTexture* aRenderTarget)
	{
		if (myVertexShader->IsLoading())
		{
			return;
		}
		CDirect3D11::GetAPI()->EnableReadOnlyDepth();
		CDirect3D11::GetAPI()->SetSamplerState(ESamplerState::Default, 1);
		CDirect3D11::GetAPI()->EnableAdditiveblend();
		CDirect3D11::GetAPI()->SetSamplerState(ESamplerState::Anisotropic_x16);
		CDirect3D11::GetAPI()->SetSamplerState(ESamplerState::Default, 10);
		myContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
		myVBuffer->Bind();
		myIBuffer->Bind();
		myVertexShader->Bind();

		CResourceManager* resourceManager = CResourceManager::Get();
		CLightFactory* lightFactory = CLightFactory::Get();

		CDX11ConstantBuffer& dirLightBuffer(resourceManager->GetConstantBufferWithID(myDirLightCBufferID));
		SDirLightData dirLightData(aRenderData.myDirLight.myLightData);

		// ENVIRONMENTAL-LIGHT
		if (aRenderData.myEnvLight.myTextureID != UINT_MAX)
		{
			CDX11Texture& envtexture = resourceManager->GetTextureWithID(aRenderData.myEnvLight.myTextureID);
			dirLightData.myToLightDirection.w = static_cast<float>(envtexture.GetMipCount());
			dirLightBuffer.SetData(dirLightData);
			dirLightBuffer.BindPS(3);
			envtexture.Bind();
			myEnvLightShader->Bind();
			myContext->DrawIndexed(myIBuffer->GetIndexCount(), 0, 0);
		}
		else
		{
			dirLightData.myToLightDirection.w = static_cast<float>(1);
			dirLightBuffer.SetData(dirLightData);
			dirLightBuffer.BindPS(3);
			myEnvLightShader->Bind();
			myContext->DrawIndexed(myIBuffer->GetIndexCount(), 0, 0);
		}

		// DIRECTIONAL LIGHT
		myDirLightShader->Bind();
		BindShadowBuffers();

		myContext->DrawIndexed(myIBuffer->GetIndexCount(), 0, 0);

		myLightModelIBuffer->Bind();
		myLightModelVShader->Bind();

		CDirect3D11::GetAPI()->EnableFrontFaceCulling();

		CU::GrowingArray<SLightInstanceData> lightInstanceData;

		// POINTLIGHTS
		for (unsigned int i = 0; i < aRenderData.myPointLightData.Size(); ++i)
		{
			CPointLightInstance instance(aRenderData.myPointLightData[i]);
			if (instance.myPointLightID == UINT_MAX)
			{
				continue;
			}
			CPointLight& pl(lightFactory->GetPointLightWithID(instance.myPointLightID));
			CDX11Shader& pShader(resourceManager->GetShaderWithID(pl.myPixelShader));
			pShader.Bind();

			CU::Matrix44f mat = CU::Matrix44f::Identity;
			mat.ScaleBy(pl.myRange);
			mat.SetPosition(instance.myPosition);

			SLightInstanceData lData;
			lData.toWorld = mat;
			lData.myLightColor = CU::Vector4f(pl.myColor, 1.0f);
			lData.myRange = pl.myRange;
			lData.myIntensity = instance.myIntensity;

			// SHADOWS
			if (instance.IsCastingShadows())
			{
				// DoPointLightShadowPass();
				// RenderLight
				myLightModelInstanceBuffer->SetData(&lData, (unsigned int)(sizeof(SLightInstanceData)));
				myLightModelVBuffer->Bind(myLightModelInstanceBuffer);
				aRenderTarget->SetAsActiveTarget();
				myContext->DrawIndexedInstanced(myLightModelIBuffer->GetIndexCount(), 1, 0, 0, 0);
			}
			else
			{
				lightInstanceData.Add(lData);
			}
		}

		if (lightInstanceData.Size() > 0)
		{
			myLightModelInstanceBuffer->SetData(&lightInstanceData[0], (unsigned int)(sizeof(SLightInstanceData) * lightInstanceData.size()));
			myLightModelVBuffer->Bind(myLightModelInstanceBuffer);
			myContext->DrawIndexedInstanced(myLightModelIBuffer->GetIndexCount(), (UINT)lightInstanceData.size(), 0, 0, 0);
			lightInstanceData.RemoveAll();
		}

		// SPOTLIGHTS
		for (unsigned int i = 0; i < aRenderData.mySpotLightData.Size(); ++i)
		{
			const CSpotLightInstance& instance(aRenderData.mySpotLightData[i]);
			if (instance.mySpotLightID == UINT_MAX)
			{
				continue;
			}

			CSpotLight& sl(lightFactory->GetSpotLightWithID(instance.mySpotLightID));
			CDX11Shader& pShader(resourceManager->GetShaderWithID(sl.myPixelShader));

			CU::Matrix44f mat = CU::Matrix44f::Identity;
			mat.ScaleBy(sl.myRange);
			mat.SetPosition(instance.myPosition);

			SLightInstanceData lData;
			lData.toWorld = mat;
			lData.myLightColor = CU::Vector4f(sl.myColor, 1.0f);
			lData.myRange = sl.myRange;
			lData.myIntensity = instance.myIntensity;
			lData.myDirection = CU::Vector4f(instance.myDirection, 0.f);
			lData.myAngle = sl.myAngle;


			// SHADOWS
			if (instance.IsCastingShadows())
			{
				myShadowMap->ClearTexture({ 1.f, 1.f, 1.f, 1.f });
				myShadowMap->SetAsActiveTarget();
				DoSpotlightShadows(aRenderData, instance, sl);
				// RenderLight
				myShadowMap->UnbindAsActiveTarget();
				myShadowMap->SetAsResourceOnSlot(8);
				pShader.Bind();
				myLightModelInstanceBuffer->SetData(&lData, (unsigned int)(sizeof(SLightInstanceData)));
				myLightModelVBuffer->Bind(myLightModelInstanceBuffer);
				aRenderTarget->SetAsActiveTarget();
				myContext->DrawIndexedInstanced(myLightModelIBuffer->GetIndexCount(), 1, 0, 0, 0);
			}
			else
			{
				lightInstanceData.Add(lData);
			}
			pShader.Bind();
		}

		if (lightInstanceData.Size() > 0)
		{
			myLightModelInstanceBuffer->SetData(&lightInstanceData[0], (unsigned int)(sizeof(SLightInstanceData) * lightInstanceData.size()));
			myLightModelVBuffer->Bind(myLightModelInstanceBuffer);
			myContext->DrawIndexedInstanced(myLightModelIBuffer->GetIndexCount(), (UINT)lightInstanceData.size(), 0, 0, 0);
			lightInstanceData.RemoveAll();
		}

		CDirect3D11::GetAPI()->SetDefaultFaceCulling();
		CDirect3D11::GetAPI()->DisableBlending();
		CDirect3D11::GetAPI()->SetDefaultDepth();
	}

	void CDeferredRenderer::DoEmissive(const SRenderData& aRenderData, const float aEmissiveMultiplier)
	{
		aRenderData;

		if (myVertexShader->IsLoading())
		{
			return;
		}
		CDirect3D11::GetAPI()->EnableReadOnlyDepth();
		CDirect3D11::GetAPI()->EnableAdditiveblend();
		myContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

		//CDX11ConstantBuffer& cBuffer(CResourceManager::Get()->GetConstantBufferWithID(myEmissiveSettingsCBufferID));
		//SEmissiveSetings settings;
		//settings.intensityMultiplier = aEmissiveMultiplier;
		//cBuffer.SetData(settings);
		//cBuffer.BindPS(11);

		myVBuffer->Bind();
		myIBuffer->Bind();
		myVertexShader->Bind();

		myEmissiveLightShader->Bind();
		myContext->DrawIndexed(myIBuffer->GetIndexCount(), 0, 0);

		CDirect3D11::GetAPI()->DisableBlending();
		CDirect3D11::GetAPI()->SetDefaultDepth();
	}

	void CDeferredRenderer::DoLinearFog()
	{
		CDirect3D11::GetAPI()->DisableBlending();
		CDirect3D11::GetAPI()->EnableReadOnlyDepth();
		myVBuffer->Bind();
		myIBuffer->Bind();
		myVertexShader->Bind();
		myLinearFogShader->Bind();
		myContext->DrawIndexed(myIBuffer->GetIndexCount(), 0, 0);
		CDirect3D11::GetAPI()->SetDefaultDepth();
	}

	void CDeferredRenderer::DoDirectionalLightShadows(const SRenderData& aRenderData)
	{
		myLastIBBound = UINT_MAX;
		myLastVBBound = UINT_MAX;
		if (myContext == nullptr)
		{
			return;
		}
		if (aRenderData.myCameraInstance.myCameraID == UINT_MAX)
		{
			return;
		}
		if (myShadowVSShader->IsLoading() || myShadowPSShader->IsLoading())
		{
			return;
		}

		CDirect3D11::GetAPI()->SetSamplerState(ESamplerState::Anisotropic_x16);
		CDirect3D11::GetAPI()->EnableSlopedDepthBias();
		CResourceManager* resourceManager = CResourceManager::Get();
		CDX11ConstantBuffer& cBuffer(resourceManager->GetConstantBufferWithID(myShadowCBufferID));

		myContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
		myShadowVSShader->Bind();
		myShadowPSShader->Bind();

		SShadowBufferData mvp;
		CCamera& camera(resourceManager->GetCameraWithID(aRenderData.myCameraInstance.myCameraID));
		CU::Vector3f lightDir(aRenderData.myDirLight.myLightData.myToLightDirection);
		SetShadowMatrices(camera, aRenderData.myCameraInstance, lightDir);
		mvp.depthProj = myShadowProjection;
		mvp.depthView = myLightView;

		SBoneBufferData data;
		CDX11ConstantBuffer& instanceBoneCBuffer(resourceManager->GetConstantBufferWithID(myInstanceBonesCBufferID));
		for (unsigned int i = 1; i < aRenderData.myListToRender.Size(); ++i)
		{
			const CModelInstance& instance = aRenderData.myListToRender[i];
			if (instance.myIsCastingShadows == false)
			{
				continue;
			}
			if (instance.myModelID == UINT_MAX)
			{
				ENGINE_LOG("[%s] could not render, model not found.", instance.myModelName.c_str());
				continue;
			}
			if (instance.myMaterialID == UINT_MAX)
			{
				continue;
			}
			CModel& model(resourceManager->GetModelWithID(instance.myModelID));

			if (model.myVertexBufferID == UINT_MAX || model.myIndexBufferID == UINT_MAX)
			{
				ENGINE_LOG("[%s] could not render, VBuffer not found.", instance.myModelName.c_str());
				continue;
			}

			CU::Matrix44f orientation(instance.myOrientation);
			orientation.ScaleBy(instance.myScale);
			mvp.toWorld = orientation;
			cBuffer.SetData(mvp);
			cBuffer.BindVS(5);

			if (instance.myBoneTransforms.empty() == false)
			{
				unsigned int index = 0;
				for (auto& transfrom : instance.myBoneTransforms)
				{
					data.bones[index] = transfrom;
					++index;
				}
				data.useAnimation = 1;
				instanceBoneCBuffer.SetData(data);
			}
			else
			{
				SBoneBufferDataOnlyBool dataOnlyBool;
				dataOnlyBool.useAnimation = 0;
				instanceBoneCBuffer.SetData(dataOnlyBool);
			}
			instanceBoneCBuffer.BindVS(4);

			if (myLastVBBound != model.myVertexBufferID)
			{
				CDX11VertexBuffer& vb(resourceManager->GetVertexBufferWithID(model.myVertexBufferID));
				vb.Bind();
				myLastVBBound = model.myVertexBufferID;
			}

			CDX11IndexBuffer& ib(resourceManager->GetIndexBufferWithID(model.myIndexBufferID));
			if (myLastIBBound != model.myIndexBufferID)
			{
				ib.Bind();
				myLastIBBound = model.myIndexBufferID;
			}
			myContext->DrawIndexed(ib.GetIndexCount(), 0, 0);
		}
		CDirect3D11::GetAPI()->SetSamplerState(ESamplerState::Default);
	}

	void CDeferredRenderer::BindShadowBuffers()
	{
		SShadowBufferData depthMVP;
		depthMVP.depthProj = myShadowProjection;
		depthMVP.depthView = myLightView;
		depthMVP.resolution = CU::Vector2f(SHADOW_BUFFER_SIZE_X, SHADOW_BUFFER_SIZE_Y);
		CDX11ConstantBuffer& depthCBuffer(CResourceManager::Get()->GetConstantBufferWithID(myShadowCBufferID));
		depthCBuffer.SetData(depthMVP);
		depthCBuffer.BindPS(4);
	}

	void CDeferredRenderer::BindCBuffers(const SRenderData& aRenderData)
	{
		CResourceManager* resourceManager = CResourceManager::Get();
		CCamera& camera(resourceManager->GetCameraWithID(aRenderData.myCameraInstance.myCameraID));
		CDX11ConstantBuffer& cameraCBuffer(resourceManager->GetConstantBufferWithID(camera.myCBufferID));
		cameraCBuffer.BindVS(CAMERA_DATA_CBUFFER_INDEX);
		cameraCBuffer.BindGS(CAMERA_DATA_CBUFFER_INDEX);
		cameraCBuffer.BindPS(CAMERA_DATA_CBUFFER_INDEX);

		CDX11ConstantBuffer& invCameraCBuffer(resourceManager->GetConstantBufferWithID(myCamInvCBufferID));
		invCameraCBuffer.BindVS(INVERSECAMERA_DATA_CBUFFER_INDEX);
		invCameraCBuffer.BindGS(INVERSECAMERA_DATA_CBUFFER_INDEX);
		invCameraCBuffer.BindPS(INVERSECAMERA_DATA_CBUFFER_INDEX);

		// Time ConstantBuffer
		CDX11ConstantBuffer& timeBuffer(resourceManager->GetConstantBufferWithID(myTimerCBufferID));
		timeBuffer.BindVS(TIME_DATA_CBUFFER_INDEX);
		timeBuffer.BindGS(TIME_DATA_CBUFFER_INDEX);
		timeBuffer.BindPS(TIME_DATA_CBUFFER_INDEX);
	}

	void CDeferredRenderer::DoSpotlightShadows(const SRenderData& aRenderData, const CSpotLightInstance& aSpotLight, const CSpotLight& aSL)
	{
		if (myContext == nullptr)
		{
			return;
		}
		if (aRenderData.myCameraInstance.myCameraID == UINT_MAX)
		{
			return;
		}
		if (myShadowVSShader->IsLoading() || myShadowPSShader->IsLoading())
		{
			return;
		}

		CDirect3D11::GetAPI()->SetSamplerState(ESamplerState::Anisotropic_x16);
		CDirect3D11::GetAPI()->EnableSlopedDepthBias();
		CDirect3D11::GetAPI()->DisableBlending();
		CResourceManager* resourceManager = CResourceManager::Get();
		CDX11ConstantBuffer& cBuffer(resourceManager->GetConstantBufferWithID(myShadowCBufferID));

		myContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
		myShadowVSShader->Bind();
		myShadowPSShader->Bind();

		SShadowBufferData mvp;
		CCamera& camera(resourceManager->GetCameraWithID(aRenderData.myCameraInstance.myCameraID));
		SetSpotlightShadowMatrices(camera, aRenderData.myCameraInstance, aSpotLight, aSL);
		mvp.depthProj = myShadowProjection;
		mvp.depthView = myLightView;

		SBoneBufferData data;
		CDX11ConstantBuffer& instanceBoneCBuffer(resourceManager->GetConstantBufferWithID(myInstanceBonesCBufferID));
		for (unsigned int i = 1; i < aRenderData.myListToRender.Size(); ++i)
		{
			const CModelInstance& instance = aRenderData.myListToRender[i];
			if (instance.myIsCastingShadows == false)
			{
				continue;
			}
			if (instance.myModelID == UINT_MAX)
			{
				ENGINE_LOG("[%s] could not render, model not found.", instance.myModelName.c_str());
				continue;
			}
			if (instance.myMaterialID == UINT_MAX)
			{
				continue;
			}
			CModel& model(resourceManager->GetModelWithID(instance.myModelID));

			if (model.myVertexBufferID == UINT_MAX || model.myIndexBufferID == UINT_MAX)
			{
				ENGINE_LOG("[%s] could not render, VBuffer not found.", instance.myModelName.c_str());
				continue;
			}

			CDX11VertexBuffer& vb(resourceManager->GetVertexBufferWithID(model.myVertexBufferID));
			CDX11IndexBuffer& ib(resourceManager->GetIndexBufferWithID(model.myIndexBufferID));

			CU::Matrix44f orientation(instance.myOrientation);
			orientation.ScaleBy(instance.myScale);

			mvp.toWorld = orientation;
			cBuffer.SetData(mvp);
			cBuffer.BindVS(5);

			vb.Bind();
			ib.Bind();

			if (instance.myBoneTransforms.empty() == false)
			{
				unsigned int index = 0;
				for (auto& transfrom : instance.myBoneTransforms)
				{
					data.bones[index] = transfrom;
					++index;
				}
				data.useAnimation = 1;
				instanceBoneCBuffer.SetData(data);
			}
			else
			{
				SBoneBufferDataOnlyBool dataOnlyBool;
				dataOnlyBool.useAnimation = 0;
				instanceBoneCBuffer.SetData(dataOnlyBool);
			}
			instanceBoneCBuffer.BindVS(4);

			myContext->DrawIndexed(ib.GetIndexCount(), 0, 0);
		}
		CDirect3D11::GetAPI()->SetSamplerState(ESamplerState::Default);
	}

	void CDeferredRenderer::SetShadowMatrices(const CCamera& aCamera, const CCameraInstance& aCameraInstance, const CU::Vector3f& aToLightDirection)
	{
		CU::Vector3f tempCamPos(aCameraInstance.GetPosition());
		CU::Vector3f dirLightTempPos((tempCamPos) + (aToLightDirection * (50.f)));
		myLightView = LookAtDX(dirLightTempPos, tempCamPos, { 0.f, 1.0f, 0.f });
		myShadowProjection = CreateOrthogonalMatrixLH(50.f, 50.f, 1.f, aCamera.GetFar() * 0.3f);
	}

	void CDeferredRenderer::SetSpotlightShadowMatrices(const CCamera& aCamera, const CCameraInstance& aCameraInstance, const CSpotLightInstance& aLight, const CSpotLight& aSL)
	{
		aCameraInstance;
		myLightView = LookAtDX(aLight.GetPosition(), aLight.GetPosition() + aLight.myDirection, { 0.f, 1.0f, 0.f });
		myShadowProjection = CreateProjectionMatrixLH(aSL.myAngle, 1.f, aCamera.GetNear(), aLight.myRange);
	}

	void CDeferredRenderer::CollectData(const SRenderData& aRenderData)
	{
		if (myContext == nullptr)
		{
			return;
		}
		if (aRenderData.myCameraInstance.myCameraID == UINT_MAX)
		{
			return;
		}
		myLastMaterialBound = UINT_MAX;
		myLastIBBound = UINT_MAX;
		myLastVBBound = UINT_MAX;

		myContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
		CDirect3D11::GetAPI()->SetSamplerState(ESamplerState::Anisotropic_x16);
		CResourceManager* resourceManager = CResourceManager::Get();
		CModelFactory* modelFactory = CModelFactory::Get();

		// Camera ConstantBuffer
		CCamera& camera(resourceManager->GetCameraWithID(aRenderData.myCameraInstance.myCameraID));
		camera.UpdateBuffers(aRenderData.myCameraInstance.myMatrixOrientation);
		CDX11ConstantBuffer& cameraCBuffer(resourceManager->GetConstantBufferWithID(camera.myCBufferID));
		cameraCBuffer.SetData(camera.myCBufferData);
		cameraCBuffer.BindVS(CAMERA_DATA_CBUFFER_INDEX);
		cameraCBuffer.BindGS(CAMERA_DATA_CBUFFER_INDEX);
		cameraCBuffer.BindPS(CAMERA_DATA_CBUFFER_INDEX);

		CDX11ConstantBuffer& invCameraCBuffer(resourceManager->GetConstantBufferWithID(myCamInvCBufferID));
		SInvCameraBufferData invCamData;
		invCamData.myInvProjection = camera.GetInverseProjection();
		invCamData.myInvView = camera.myCBufferData.myCameraOrientation;
		invCameraCBuffer.SetData(invCamData);
		invCameraCBuffer.BindVS(INVERSECAMERA_DATA_CBUFFER_INDEX);
		invCameraCBuffer.BindGS(INVERSECAMERA_DATA_CBUFFER_INDEX);
		invCameraCBuffer.BindPS(INVERSECAMERA_DATA_CBUFFER_INDEX);

		// Time ConstantBuffer
		CDX11ConstantBuffer& timeBuffer(resourceManager->GetConstantBufferWithID(myTimerCBufferID));
		STimeData tData = aRenderData.myTimeData;
		tData.myRandomNumber = CU::GetRandomInRange(0, 1000);
		timeBuffer.SetData(tData);
		timeBuffer.BindVS(TIME_DATA_CBUFFER_INDEX);
		timeBuffer.BindGS(TIME_DATA_CBUFFER_INDEX);
		timeBuffer.BindPS(TIME_DATA_CBUFFER_INDEX);

		SInstanceBufferData instanceData;
		CDX11ConstantBuffer& instanceCBuffer(resourceManager->GetConstantBufferWithID(myCBufferID));
		CDX11ConstantBuffer& instanceBoneCBuffer(resourceManager->GetConstantBufferWithID(myInstanceBonesCBufferID));

		CDirect3D11::GetAPI()->SetDefaultDepth();

		if (aRenderData.myListToRender.Empty())
		{
			return;
		}
		SBoneBufferData data;
		// Start at 1 because index [0] is reserved for skybox.
		for (unsigned int i = 1; i < aRenderData.myListToRender.Size(); ++i)
		{
			const CModelInstance& instance = aRenderData.myListToRender[i];
			if (instance.myModelID == UINT_MAX)
			{
				ENGINE_LOG("[%s] could not render, model not found.", instance.myModelName.c_str());
				continue;
			}
			if (instance.myMaterialID == UINT_MAX)
			{
				ENGINE_LOG("[%s] could not render, material not found.", instance.myModelName.c_str());
				continue;
			}
			CModel& model(resourceManager->GetModelWithID(instance.myModelID));


			if (model.myVertexBufferID == UINT_MAX || model.myIndexBufferID == UINT_MAX)
			{
				ENGINE_LOG("[%s] could not render, VBuffer not found.", instance.myModelName.c_str());
				continue;
			}


			if (myLastMaterialBound != instance.myMaterialID)
			{
				CMaterial& mat(modelFactory->GetMaterialWithID(instance.myMaterialID));
				if (mat.IsLoading())
				{
					continue;
				}
				mat.Bind();
				myLastMaterialBound = instance.myMaterialID;
			}

			CU::Matrix44f orientation(instance.myOrientation);
			orientation.ScaleBy(instance.myScale);
			instanceData.myToWorld = orientation;

			if (instance.IsAffectedByFog())
			{
				instanceData.useFog = 1;
			}
			else
			{
				instanceData.useFog = 0;
			}

			instanceCBuffer.SetData(instanceData);
			instanceCBuffer.BindVS(3);

			if (instance.myBoneTransforms.empty() == false)
			{
				unsigned int index = 0;
				for (auto& transfrom : instance.myBoneTransforms)
				{
					data.bones[index] = transfrom;
					++index;
				}
				data.useAnimation = 1;
				instanceBoneCBuffer.SetData(data);
			}
			else
			{
				SBoneBufferDataOnlyBool dataOnlyBool;
				dataOnlyBool.useAnimation = 0;
				instanceBoneCBuffer.SetData(dataOnlyBool);
			}
			instanceBoneCBuffer.BindVS(4);

			if (myLastVBBound != model.myVertexBufferID)
			{
				CDX11VertexBuffer& vb(resourceManager->GetVertexBufferWithID(model.myVertexBufferID));
				vb.Bind();
				myLastVBBound = model.myVertexBufferID;
			}

			CDX11IndexBuffer& ib(resourceManager->GetIndexBufferWithID(model.myIndexBufferID));
			if (myLastIBBound != model.myIndexBufferID)
			{
				ib.Bind();
				myLastIBBound = model.myIndexBufferID;
			}

			myContext->DrawIndexed(ib.GetIndexCount(), 0, 0);
		}
	}

}}