#pragma once

struct ID3D11DeviceContext;
namespace sce { namespace gfx {
	class CSprite3D;
	class CSpriteRenderer3D
	{
	public:
		CSpriteRenderer3D();
		~CSpriteRenderer3D();

		void Init();
		void Render(CCameraInstance& aCameraInstance, CU::GrowingArray<sce::gfx::CSprite3D, unsigned int>& aListToRender);

	private:
		struct SSprite3DData
		{
			CU::Matrix44f myOrientation;
			CU::Vector4f myPositionAndRotation;
			CU::Vector4f myTextureSizeAndTargetSize;
			CU::Vector4f myColor;
			CU::Vector4f mySizeAndPivot;
			CU::Vector4f myCrop;
		};

	private:

		float myGlowAndPadding[4];

		unsigned int myCBufferID;
		unsigned int mySpriteEffectCBufferID;
		ID3D11DeviceContext* myContext;
	};

}}