#pragma once
#include "Utilities/RenderStructs.h"
#include "Renderer/ForwardRenderer.h"
#include "Renderer/Renderer2D.h"
#include "Renderer/SpriteRenderer3D.h"
#include "Renderer/FullscreenRenderer.h"
#include "ParticleRenderer.h"
#include "Renderer/TextRenderer.h"
#include "DeferredRenderer.h"
#include "SkyboxRenderer.h"
#include "DebugRenderer.h"
#include "DeferredRendererGI.h"

namespace sce { namespace gfx {

	class CFramework;
	class CDX11GBuffer;
	class CDX11FullscreenTexture;

	enum class EAntiAliasingType
	{
		NONE,
		FXAA,
		SSAA,
	};

	struct SRenderOptions
	{
		bool myUseBloom = false;
		bool myUseFXAA = false;
		bool myUseColorGrading = false;
		bool myUseNoiseDistortion = false;
		bool myUseSSAO = true;
		bool myUseHDR = true;
		bool myUseLinearFog = false;
		EAntiAliasingType myAAType = EAntiAliasingType::FXAA;
	};

	class CRenderManager
	{
	public:
		CRenderManager();
		~CRenderManager();

		void SetRenderOptions(const SRenderOptions& aRenderOptions);
		void ReInit(CFramework * aFramework, const CU::Vector2f& aResolution);
		void DestroyContent();

		void Init(CFramework* aFramework, const CU::Vector2f& myResolution);
		void RenderFrame(SRenderData& aRenderData, const CU::Vector2f& aResolution);

		inline const bool IsSSAOEnabled() const { return myRenderOptions.myUseSSAO; }
		inline const bool IsColorGradingEnabled() const { return myRenderOptions.myUseColorGrading; }
		inline const bool IsLinearFogEnabled() const { return myRenderOptions.myUseLinearFog; }
		inline const bool IsBloomEnabled() const { return myRenderOptions.myUseBloom; }
		inline const bool IsFXAAEnabled() const { return myRenderOptions.myAAType == EAntiAliasingType::FXAA; }
		inline void ToggleSSAO() { myRenderOptions.myUseSSAO = !myRenderOptions.myUseSSAO; }; // Disabled for this project.
		inline void ToggleColorGrading() { myRenderOptions.myUseColorGrading = !myRenderOptions.myUseColorGrading; };
		inline void ToggleLinearFog() { myRenderOptions.myUseLinearFog = !myRenderOptions.myUseLinearFog; };
		inline void ToggleBloom() { myRenderOptions.myUseBloom = !myRenderOptions.myUseBloom; };
		inline void SetBloom(const bool aBool) { myRenderOptions.myUseBloom = aBool; };
		inline void SetFXAA(const bool aBool) { myRenderOptions.myAAType = (aBool == true) ? EAntiAliasingType::FXAA : EAntiAliasingType::NONE; };
		inline void SetSSAO(const bool aBool) { myRenderOptions.myUseSSAO = aBool; };
		inline void SetLinearFog(const bool aBool) { myRenderOptions.myUseLinearFog = aBool; };

		void ToggleFXAA();


	private:
		void DoBloom();
		void DoFXAA();
		void DoTonemapping();
		void DoColorGrading();
		const CU::Vector2f GetClosestPow2(const CU::Vector2f& aVector);

	private:
		static SRenderOptions myRenderOptions;

		CU::Vector2f myResolution;
		volatile bool myShouldReinit;
		float myEmissiveMultiplier;

		CDX11GBuffer* myGBuffer;

		CDX11FullscreenTexture* myLastTarget;
		CDX11FullscreenTexture* myNewTarget;
		CDX11FullscreenTexture* mySSAADownsampleTexture;
		//CDX11FullscreenTexture* mySSAADownsampleTexture2;
		CDX11FullscreenTexture* myIntermediateTexture;
		CDX11FullscreenTexture* myIntermediateTexture2;
		CDX11FullscreenTexture* myAfterToneMapTexture;
		CDX11FullscreenTexture* myAfterToneMapTexture2;

		// FOR BLOOM
		CDX11FullscreenTexture* myFullSizeTexture;
		CDX11FullscreenTexture* myHalfSizeTexture;
		CDX11FullscreenTexture* myHalfSizeTexture2;
		CDX11FullscreenTexture* myQuaterSizeTexture;
		CDX11FullscreenTexture* myQuaterSizeTexture2;
		CDX11FullscreenTexture* myEighthSizeTexture;
		CDX11FullscreenTexture* myEighthSizeTexture2;
		CDX11FullscreenTexture* mySixteenthSizeTexture;
		CDX11FullscreenTexture* mySixteenthSizeTexture2;
		CDX11FullscreenTexture* myShadowBuffer;


		// FOR TONEMAPPING
		unsigned int myNumTonemapTextures;
		CDX11FullscreenTexture* myTonemapTextures;
		CDX11FullscreenTexture* myTonemapLumTexture;
		//////////////////

		// FOR SSAO
		CDX11FullscreenTexture* mySSAOTexture;
		CDX11FullscreenTexture* mySSAOBlurredTexture;
		///////////////

		CForwardRenderer myForwardRenderer;
		CDeferredRenderer myDeferredRenderer;
		CDeferredRendererGI myDeferredRendererGI;
		CParticleRenderer myParticleRenderer;
		CSkyboxRenderer mySkyboxRenderer;
		CDebugRenderer myDebugRenderer;
		CRenderer2D myRenderer2D;
		CSpriteRenderer3D mySpriteRenderer3D;
		CFullscreenRenderer myPostFXRenderer;
		CTextRenderer myTextRenderer;
		CFramework* myFramework;
	};

}}