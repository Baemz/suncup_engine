#pragma once
#include "..\CommonUtilities\Matrix.h"
#include "../CommonUtilities/Timer.h"

struct ID3D11DeviceContext;
namespace sce { namespace gfx {

	class CCameraInstance;
	class CModelInstance;
	class CDirectionalLight;
	class CSpotLightInstance;
	class CPointLightInstance;
	class CEnvironmentalLight;
	class CDX11Shader;
	class CDX11VertexBuffer;
	class CDX11IndexBuffer;
	class CModel;
	class CDX11FullscreenTexture;
	class CSpotLight;

	class CDeferredRenderer
	{
	public:
		CDeferredRenderer();
		~CDeferredRenderer();
		void Destroy();


		void Init();
		void CollectData(const SRenderData& aRenderData);
		void LightPass(const SRenderData& aRenderData, CDX11FullscreenTexture* aRenderTarget);
		void DoEmissive(const SRenderData& aRenderData, const float aEmissiveMultiplier = 1.0f);
		void DoLinearFog();
		void DoDirectionalLightShadows(const SRenderData& aRenderData);
		void BindShadowBuffers();
		void BindCBuffers(const SRenderData& aRenderData);

	private:
		void DoSpotlightShadows(const SRenderData& aRenderData, const CSpotLightInstance& aSpotLight, const CSpotLight& aSL);
		void SetShadowMatrices(const CCamera& aCamera, const CCameraInstance& aCameraInstance, const CU::Vector3f& aToLightDirection);
		void SetSpotlightShadowMatrices(const CCamera& aCamera, const CCameraInstance& aCameraInstance, const CSpotLightInstance& aLight, const CSpotLight& aSL);
	private:
		struct SInstanceBufferData
		{
			CU::Matrix44f myToWorld;
			float useFog = 1;
			float pad[3];
		};
		struct SInvCameraBufferData
		{
			CU::Matrix44f myInvView;
			CU::Matrix44f myInvProjection;
		};
		struct SShadowBufferData
		{
			CU::Matrix44f depthProj;
			CU::Matrix44f depthView;
			CU::Matrix44f toWorld;
			CU::Vector2f resolution;
		private:
			float pad[2];
		}; 
		struct SBoneBufferData
		{
			unsigned int useAnimation;
			unsigned int crap[3];
			CU::Matrix44f bones[64];
		};
		struct SBoneBufferDataOnlyBool
		{
			unsigned int useAnimation;
			unsigned int crap[3];
		};
		struct SShadowBufferMatrices
		{
			CU::Matrix44f depthView;
			CU::Matrix44f depthProj;
		}; 
		struct SLightMatrixData
		{
			CU::Matrix44f myToWorld[100];
		};
		struct SLightInstanceData
		{
			CU::Matrix44f toWorld;
			CU::Vector4f myLightColor;
			CU::Vector4f myDirection;
			float myRange;
			float myIntensity;
			float myAngle;

		}; 
		
		struct SEmissiveSetings
		{
			float intensityMultiplier;
		private:
			float pad[3];
		};

	private:
		CU::Matrix44f myShadowProjection;
		CU::Matrix44f myLightView;
		unsigned int myCBufferID;
		unsigned int myDirLightCBufferID;
		unsigned int myInstanceBonesCBufferID;
		unsigned int myEmissiveSettingsCBufferID;
		unsigned int myShadowCBufferID;
		unsigned int myShadowCBuffer2ID;
		unsigned int myCamInvCBufferID;
		unsigned int myLastMaterialBound;
		unsigned int myLastVBBound;
		unsigned int myLastIBBound;
		CDX11FullscreenTexture* myShadowMap;
		ID3D11DeviceContext* myContext;
		CDX11Shader* myDirLightShader;
		CDX11Shader* myEnvLightShader;
		CDX11Shader* myEmissiveLightShader;
		CDX11Shader* myVertexShader; 
		CDX11Shader* myLinearFogShader;
		CDX11Shader* myShadowPSShader;
		CDX11Shader* myShadowVSShader;
		CDX11Shader* myLightModelVShader;
		CDX11VertexBuffer* myVBuffer;
		CDX11IndexBuffer* myIBuffer;
		CDX11VertexBuffer* myLightModelVBuffer;
		CDX11VertexBuffer* myLightModelInstanceBuffer;
		CDX11IndexBuffer* myLightModelIBuffer;

		unsigned int myTimerCBufferID;
		CU::Timer myTimer;
		float myElapsedTime[4];
	};
}}
