#include "stdafx.h"
#include "RenderManager.h"
#include "Framework\Framework.h"
#include "../DirectXFramework/API/DX11FullscreenTexture.h"
#include "../DirectXFramework/API/DX11GBuffer.h"
#include "ImGUI/imgui.h"

#define SHADOW_BUFFER_SIZE_X 4096.f
#define SHADOW_BUFFER_SIZE_Y 4096.f

namespace sce { namespace gfx {

	SRenderOptions CRenderManager::myRenderOptions = SRenderOptions();

	CRenderManager::CRenderManager()
		: myIntermediateTexture(nullptr)
		, myIntermediateTexture2(nullptr)
		, myFullSizeTexture(nullptr)
		, myHalfSizeTexture(nullptr)
		, myHalfSizeTexture2(nullptr)
		, myQuaterSizeTexture(nullptr)
		, myQuaterSizeTexture2(nullptr)
		, myEighthSizeTexture(nullptr)
		, myEighthSizeTexture2(nullptr)
		, myGBuffer(nullptr)
		, myAfterToneMapTexture(nullptr)
		, myAfterToneMapTexture2(nullptr)
		, myTonemapTextures(nullptr)
		, mySixteenthSizeTexture(nullptr)
		, mySixteenthSizeTexture2(nullptr)
		, mySSAOTexture(nullptr)
		, mySSAOBlurredTexture(nullptr)
		, mySSAADownsampleTexture(nullptr)
		, myShouldReinit(false)
	{
	}

	CRenderManager::~CRenderManager()
	{
		DestroyContent();
	}

	void CRenderManager::SetRenderOptions(const SRenderOptions& aRenderOptions)
	{
		myRenderOptions = aRenderOptions;
	}

	void CRenderManager::ReInit(CFramework* aFramework, const CU::Vector2f& aResolution)
	{
		DestroyContent();
		Init(aFramework, aResolution);
	}

	void CRenderManager::DestroyContent()
	{
		sce_delete(myIntermediateTexture);
		sce_delete(myIntermediateTexture2);
		sce_delete(myFullSizeTexture);
		sce_delete(myHalfSizeTexture);
		sce_delete(myHalfSizeTexture2);
		sce_delete(myQuaterSizeTexture);
		sce_delete(myQuaterSizeTexture2);
		sce_delete(myEighthSizeTexture);
		sce_delete(myEighthSizeTexture2);
		sce_delete(myGBuffer);
		sce_delete(myAfterToneMapTexture);
		sce_delete(myAfterToneMapTexture2);
		sce_delete(myTonemapTextures);
		sce_delete(mySixteenthSizeTexture);
		sce_delete(mySixteenthSizeTexture2);
		sce_delete(mySSAOTexture);
		sce_delete(mySSAOBlurredTexture);
		sce_delete(myShadowBuffer);
		sce_delete(mySSAADownsampleTexture);
		//sce_delete(mySSAADownsampleTexture2);
		myFramework = nullptr;

		myForwardRenderer.Destroy();
		myDeferredRenderer.Destroy();
		myPostFXRenderer.Destroy();
		myDebugRenderer.Destroy();
		myDeferredRendererGI.Destroy();
	}

	void CRenderManager::Init(CFramework* aFramework, const CU::Vector2f& aResolution)
	{
		myResolution = aResolution;
		myLastTarget = nullptr;
		myNewTarget = nullptr;

		myFramework = aFramework;
		myForwardRenderer.Init();
		myDeferredRenderer.Init();
		myDebugRenderer.Init();
		myRenderer2D.Init();
		mySpriteRenderer3D.Init();
		myParticleRenderer.Init();
		myPostFXRenderer.Init();
		myTextRenderer.Init();
		mySkyboxRenderer.Init();
		myDeferredRendererGI.Init();

		myIntermediateTexture = sce_new(CDX11FullscreenTexture());
		myIntermediateTexture2 = sce_new(CDX11FullscreenTexture());

		myFullSizeTexture = sce_new(CDX11FullscreenTexture());
		myHalfSizeTexture = sce_new(CDX11FullscreenTexture());
		myHalfSizeTexture2 = sce_new(CDX11FullscreenTexture());
		myQuaterSizeTexture = sce_new(CDX11FullscreenTexture());
		myQuaterSizeTexture2 = sce_new(CDX11FullscreenTexture());
		myEighthSizeTexture = sce_new(CDX11FullscreenTexture());
		myEighthSizeTexture2 = sce_new(CDX11FullscreenTexture());
		myAfterToneMapTexture = sce_new(CDX11FullscreenTexture());
		myAfterToneMapTexture2 = sce_new(CDX11FullscreenTexture());
		mySixteenthSizeTexture = sce_new(CDX11FullscreenTexture());
		mySixteenthSizeTexture2 = sce_new(CDX11FullscreenTexture());
		mySSAOTexture = sce_new(CDX11FullscreenTexture());
		mySSAOBlurredTexture = sce_new(CDX11FullscreenTexture());
		mySSAADownsampleTexture = sce_new(CDX11FullscreenTexture());

		myShadowBuffer = sce_new(CDX11FullscreenTexture());


		DXGI_FORMAT format = DXGI_FORMAT_R16G16B16A16_FLOAT;
		mySSAADownsampleTexture->Init(myResolution, format, false);

		CU::Vector2f resolution;
		if (myRenderOptions.myAAType == EAntiAliasingType::SSAA)
		{
			resolution = (myResolution * 2.f);
			myEmissiveMultiplier = 3.f;
		}
		else
		{
			resolution = myResolution;
			myEmissiveMultiplier = 4.f;
		}

		myGBuffer = sce_new(CDX11GBuffer());
		myGBuffer->Init(resolution);

		myShadowBuffer->Init(CU::Vector2f(SHADOW_BUFFER_SIZE_X, SHADOW_BUFFER_SIZE_Y), DXGI_FORMAT_R32_FLOAT, true, true);

		myIntermediateTexture->Init(resolution, format, true);
		myIntermediateTexture2->Init(resolution, format, true);
		mySSAOTexture->Init(resolution, DXGI_FORMAT_R32_FLOAT, false);
		mySSAOBlurredTexture->Init(resolution, DXGI_FORMAT_R32_FLOAT, false);
		myAfterToneMapTexture->Init(resolution, DXGI_FORMAT_R8G8B8A8_UNORM, false);
		myAfterToneMapTexture2->Init(resolution, DXGI_FORMAT_R8G8B8A8_UNORM, false);

		myFullSizeTexture->Init(resolution, format, false);
		myHalfSizeTexture->Init((resolution * 0.5f), format, false);
		myHalfSizeTexture2->Init((resolution * 0.5f), format, false);
		myQuaterSizeTexture->Init((resolution * 0.25f), format, false);
		myQuaterSizeTexture2->Init((resolution * 0.25f), format, false);
		myEighthSizeTexture->Init((resolution * (1 / 8.f)), format, false);
		myEighthSizeTexture2->Init((resolution * (1 / 8.f)), format, false);
		mySixteenthSizeTexture->Init((resolution * (1 / 16.f)), format, false);
		mySixteenthSizeTexture2->Init((resolution * (1 / 16.f)), format, false);

		CU::Vector2f half(GetClosestPow2(resolution));
		std::vector<CU::Vector2f> sizes;
		sizes.push_back(half);
		do
		{
			half.x *= 0.5f;
			CU::Clamp(half.x, 1.f, resolution.x);
			half.y *= 0.5f;
			CU::Clamp(half.y, 1.f, resolution.y);
			sizes.push_back(half);
		} while (half.x > 1.0f && half.y > 1.0f);
		myNumTonemapTextures = static_cast<unsigned int>(sizes.size());
		myTonemapTextures = sce_newArray(CDX11FullscreenTexture, myNumTonemapTextures);
		for (unsigned int i = 0; i < myNumTonemapTextures; ++i)
		{
			myTonemapTextures[i].Init(sizes[i], format, false);
		}
	}

	void CRenderManager::RenderFrame(SRenderData& aRenderData, const CU::Vector2f& aResolution)
	{
		if (myShouldReinit)
		{
			ReInit(myFramework, myResolution);
			myShouldReinit = false;
		}

#ifdef _RETAIL
		CU::Vector4f clearColor = { 0.f, 0.f, 0.f, 1.f };
#else
		CU::Vector4f clearColor = { 0.5f, 1.f, 0.5f, 1.f };
#endif
		const bool hasDeferredData(aRenderData.myListToRender.Size() > 1);
		const bool hasSkybox(aRenderData.myListToRender.Size() > 0);
		const bool hasForwardData(aRenderData.myTransparentListToRender.Size() > 0);
		const bool hasParticles(aRenderData.myParticleEmittorsToRender.Size() > 0);
		const bool has3DSprites(aRenderData.my3DSpritesToRender.Size() > 0);
		const bool hasRenderData(hasDeferredData || hasForwardData || has3DSprites || hasSkybox);

		myDeferredRendererGI.RenderGUI(aRenderData);
		// Render scene
		myGBuffer->ClearBuffer();
		myGBuffer->SetAsRenderTarget();
#ifndef _RETAIL
		if (CDirect3D11::GetAPI()->GetShouldRenderWireFrame())
		{
			CDirect3D11::GetAPI()->SetRasterStateWireframe(true);
			myDeferredRenderer.CollectData(aRenderData);
			CDirect3D11::GetAPI()->SetRasterStateWireframe(false);
		}
		else
#endif
		{
			myDeferredRenderer.CollectData(aRenderData);
			//myLastTarget = myIntermediateTexture;
			//myLastTarget->ClearTexture(clearColor);
			//myLastTarget->SetAsActiveTarget();
			////myDeferredRendererGI.DEBUG_RenderVoxelizedScene();
			//ImGui::Render();
			//if (myLastTarget != nullptr)
			//{
			//	myFramework->ActivateScreenTarget();
			//	myPostFXRenderer.Render(EFXType::Copy, { myLastTarget, nullptr });
			//}
			//return;
		}
		myGBuffer->UnbindAsRenderTarget();
		myShadowBuffer->ClearTexture({ 0.f, 0.f, 0.f, 0.f });
		myShadowBuffer->SetAsActiveTarget();
		myDeferredRenderer.DoDirectionalLightShadows(aRenderData);


		// SSAO
		CU::Vector4f clearColorSSAO = { 1.f, 1.f, 1.f, 1.f };
		if (myRenderOptions.myUseSSAO)
		{
			mySSAOTexture->ClearTexture(clearColorSSAO);
			mySSAOTexture->SetAsActiveTarget();
			myGBuffer->BindSSAOResources();
			myPostFXRenderer.Render(EFXType::SSAO, { nullptr, nullptr });
			mySSAOTexture->UnbindAsActiveTarget();

			mySSAOBlurredTexture->ClearTexture(clearColorSSAO);
			mySSAOBlurredTexture->SetAsActiveTarget();
			myPostFXRenderer.Render(EFXType::SSAOBlur, { mySSAOTexture, nullptr });
			mySSAOBlurredTexture->UnbindAsActiveTarget();

			//ImGui::Render();
			//myFramework->ActivateScreenTarget();
			//myPostFXRenderer.Render(EFXType::Copy, { mySSAOBlurredTexture, nullptr });
			//return;
		}
		else
		{
			mySSAOBlurredTexture->ClearTexture(clearColorSSAO);
		}

			//ImGui::Render();
			//myFramework->ActivateScreenTarget();
			//myPostFXRenderer.Render(EFXType::Copy, { myShadowBuffer, nullptr });
			//return;

		
		
		myShadowBuffer->SetDepthAsResourceOnSlot(8);
		myDeferredRenderer.BindShadowBuffers();
		myDeferredRendererGI.RenderVoxelScene(aRenderData);

		if (myDeferredRendererGI.ShouldRenderVoxels())
		{
			//////////////////////
			myLastTarget = myIntermediateTexture;
			myLastTarget->ClearTexture(clearColor);
			myLastTarget->SetAsActiveTarget();
			//if (hasSkybox)
			//{
			//	mySkyboxRenderer.RenderFrame(aRenderData);
			//}
			myDeferredRendererGI.DEBUG_RenderVoxelizedScene();
			ImGui::Render();
			myLastTarget->UnbindAsActiveTarget();
			if (myLastTarget != nullptr)
			{
				myFramework->ActivateScreenTarget();
				myPostFXRenderer.Render(EFXType::Copy, { myLastTarget, nullptr });
			}
			return;
			//////////////////////
		}
		// Light-pass
		CU::Vector4f clearColorLightPass = { 0.f, 0.f, 0.f, 1.f };
		myIntermediateTexture->ClearTexture(clearColorLightPass);
		myIntermediateTexture->SetAsActiveTarget();
		myGBuffer->BindAsResource();
		if (hasDeferredData)
		{
			mySSAOBlurredTexture->SetAsResourceOnSlot(6);
			myDeferredRendererGI.BindVoxelRadiance();
			myDeferredRenderer.LightPass(aRenderData, myIntermediateTexture);
		}

		// Linear fog
		if (myRenderOptions.myUseLinearFog)
		{
			myIntermediateTexture2->ClearTexture(clearColor);
			myIntermediateTexture2->SetAsActiveTarget();
			myIntermediateTexture->SetAsResourceOnSlot(0);
			if (hasDeferredData)
			{
				myDeferredRenderer.DoLinearFog();
				myDeferredRenderer.DoEmissive(aRenderData, myEmissiveMultiplier);
			}
			myIntermediateTexture->UnbindAsActiveTarget();
			myIntermediateTexture2->SetAsActiveTarget(myGBuffer->GetDepth());
			myLastTarget = myIntermediateTexture2;
			myNewTarget = myIntermediateTexture;
		}
		else
		{
			if (hasDeferredData)
			{
				myDeferredRenderer.DoEmissive(aRenderData, myEmissiveMultiplier);
			}
			myIntermediateTexture2->UnbindAsActiveTarget();
			myIntermediateTexture->SetAsActiveTarget(myGBuffer->GetDepth());
			myLastTarget = myIntermediateTexture;
			myNewTarget = myIntermediateTexture2;
		}

		// Skybox
		if (hasSkybox)
		{
			mySkyboxRenderer.RenderFrame(aRenderData);
		}

		//Particles
		if (hasParticles)
		{
			myParticleRenderer.Render(aRenderData.myCameraInstance, aRenderData.myParticleEmittorsToRender, aRenderData.myStreakEmittorsToRender);
		}

		// Forward Renderer
		if (hasForwardData)
		{
			myShadowBuffer->SetDepthAsResourceOnSlot(8);
			myForwardRenderer.Render(aRenderData);
		}


		// 3D Sprites
		if (has3DSprites)
		{
			mySpriteRenderer3D.Render(aRenderData.myCameraInstance, aRenderData.my3DSpritesToRender);
		}

		// Post processing
		if (hasRenderData)
		{
			if (hasDeferredData || hasForwardData)
			{
				DoBloom();
			}
			DoColorGrading();
			if (myRenderOptions.myAAType == EAntiAliasingType::FXAA)
			{
				DoFXAA();
			}
			else if (myRenderOptions.myAAType == EAntiAliasingType::SSAA)
			{
				mySSAADownsampleTexture->SetAsActiveTarget();
				myPostFXRenderer.Render(EFXType::Copy, { myLastTarget, nullptr });
				myLastTarget = mySSAADownsampleTexture;
			}
			myAfterToneMapTexture->ClearTexture(clearColor);
			DoTonemapping();
		}

		myLastTarget->SetAsActiveTarget(myGBuffer->GetDepth());

#ifndef _RETAIL
		if (CDirect3D11::GetAPI()->GetShouldRenderWireFrame())
		{
			CDirect3D11::GetAPI()->SetRasterStateWireframe(true);
			myDebugRenderer.Render(aRenderData);
			CDirect3D11::GetAPI()->SetRasterStateWireframe(false);
		}
		else
#endif
		{
			myDebugRenderer.Render(aRenderData);
		}

		// 2D Sprites
		myRenderer2D.Render(aRenderData.mySpritesToRender);
		// Text
		//myTextRenderer.Render(aRenderData.myTextsToRender, aResolution);
		
		ImGui::Render();

		// Copy to backbuffer
		myLastTarget->UnbindAsActiveTarget();
		if (myLastTarget != nullptr)
		{
			myFramework->ActivateScreenTarget();
			myPostFXRenderer.Render(EFXType::Copy, { myLastTarget, nullptr });
		}
	}

	void CRenderManager::ToggleFXAA()
	{
		switch (myRenderOptions.myAAType)
		{
		case EAntiAliasingType::NONE:
			myRenderOptions.myAAType = EAntiAliasingType::FXAA;
			break;
		case EAntiAliasingType::FXAA:
			myRenderOptions.myAAType = EAntiAliasingType::NONE;
			break;
		}
		myShouldReinit = true;
	}

	void CRenderManager::DoBloom()
	{
		if (myRenderOptions.myUseBloom == false)
		{
			myNewTarget->SetAsActiveTarget();
			myPostFXRenderer.Render(EFXType::Copy, { myLastTarget, nullptr });

			auto tempTarget = myNewTarget;
			myNewTarget = myLastTarget;
			myLastTarget = tempTarget;
			return;
		}

		myFullSizeTexture->SetAsActiveTarget();
		myPostFXRenderer.Render(EFXType::Luminance, { myLastTarget, nullptr });

		myHalfSizeTexture->SetAsActiveTarget();
		myPostFXRenderer.Render(EFXType::Copy, { myFullSizeTexture, nullptr });

		// (1/4)
		myQuaterSizeTexture->SetAsActiveTarget();
		myPostFXRenderer.Render(EFXType::Copy, { myHalfSizeTexture, nullptr });

		//myQuaterSizeTexture2->SetAsActiveTarget();
		//myPostFXRenderer.Render(EFXType::GaussianBlurVertical4, { myQuaterSizeTexture, nullptr });
		//
		//myQuaterSizeTexture->SetAsActiveTarget();
		//myPostFXRenderer.Render(EFXType::GaussianBlurHorizontal4, { myQuaterSizeTexture2, nullptr });
		//
		//myQuaterSizeTexture2->SetAsActiveTarget();
		//myPostFXRenderer.Render(EFXType::GaussianBlurVertical4, { myQuaterSizeTexture, nullptr });
		//
		//myQuaterSizeTexture->SetAsActiveTarget();
		//myPostFXRenderer.Render(EFXType::GaussianBlurHorizontal4, { myQuaterSizeTexture2, nullptr });

		// (1/8)

		myEighthSizeTexture->SetAsActiveTarget();
		myPostFXRenderer.Render(EFXType::Copy, { myQuaterSizeTexture, nullptr });
		
		myEighthSizeTexture2->SetAsActiveTarget();
		myPostFXRenderer.Render(EFXType::GaussianBlurVertical4, { myEighthSizeTexture, nullptr });
		
		myEighthSizeTexture->SetAsActiveTarget();
		myPostFXRenderer.Render(EFXType::GaussianBlurHorizontal4, { myEighthSizeTexture2, nullptr });
		
		myEighthSizeTexture2->SetAsActiveTarget();
		myPostFXRenderer.Render(EFXType::GaussianBlurVertical4, { myEighthSizeTexture, nullptr });
		
		myEighthSizeTexture->SetAsActiveTarget();
		myPostFXRenderer.Render(EFXType::GaussianBlurHorizontal4, { myEighthSizeTexture2, nullptr });

		// (1/16)

			mySixteenthSizeTexture->SetAsActiveTarget();
			myPostFXRenderer.Render(EFXType::Copy, { myEighthSizeTexture, nullptr });

			mySixteenthSizeTexture2->SetAsActiveTarget();
			myPostFXRenderer.Render(EFXType::GaussianBlurVertical4, { mySixteenthSizeTexture, nullptr });

			mySixteenthSizeTexture->SetAsActiveTarget();
			myPostFXRenderer.Render(EFXType::GaussianBlurHorizontal4, { mySixteenthSizeTexture2, nullptr });

			mySixteenthSizeTexture2->SetAsActiveTarget();
			myPostFXRenderer.Render(EFXType::GaussianBlurVertical4, { mySixteenthSizeTexture, nullptr });

			mySixteenthSizeTexture->SetAsActiveTarget();
			myPostFXRenderer.Render(EFXType::GaussianBlurHorizontal4, { mySixteenthSizeTexture2, nullptr });

			myEighthSizeTexture2->SetAsActiveTarget();
			myPostFXRenderer.Render(EFXType::Add, { mySixteenthSizeTexture, myEighthSizeTexture });

			myEighthSizeTexture->SetAsActiveTarget();
			myPostFXRenderer.Render(EFXType::GaussianBlurVertical4, { myEighthSizeTexture2, nullptr });

			myEighthSizeTexture2->SetAsActiveTarget();
			myPostFXRenderer.Render(EFXType::GaussianBlurHorizontal4, { myEighthSizeTexture, nullptr });

			myEighthSizeTexture->SetAsActiveTarget();
			myPostFXRenderer.Render(EFXType::GaussianBlurVertical4, { myEighthSizeTexture2, nullptr });

			myEighthSizeTexture2->SetAsActiveTarget();
			myPostFXRenderer.Render(EFXType::GaussianBlurHorizontal4, { myEighthSizeTexture, nullptr });


			myQuaterSizeTexture2->SetAsActiveTarget();
			myPostFXRenderer.Render(EFXType::Add, { myEighthSizeTexture2, myQuaterSizeTexture });

		// Up-scale

		myQuaterSizeTexture->SetAsActiveTarget();
		myPostFXRenderer.Render(EFXType::GaussianBlurVertical4, { myQuaterSizeTexture2, nullptr });

		myQuaterSizeTexture2->SetAsActiveTarget();
		myPostFXRenderer.Render(EFXType::GaussianBlurHorizontal4, { myQuaterSizeTexture, nullptr }); 
		
		myQuaterSizeTexture->SetAsActiveTarget();
		myPostFXRenderer.Render(EFXType::GaussianBlurVertical4, { myQuaterSizeTexture2, nullptr });

		myQuaterSizeTexture2->SetAsActiveTarget();
		myPostFXRenderer.Render(EFXType::GaussianBlurHorizontal4, { myQuaterSizeTexture, nullptr });

		myHalfSizeTexture2->SetAsActiveTarget();
		myPostFXRenderer.Render(EFXType::Add, { myQuaterSizeTexture2, myHalfSizeTexture });

		//myHalfSizeTexture->SetAsActiveTarget();
		//myPostFXRenderer.Render(EFXType::GaussianBlurHorizontal4, { myHalfSizeTexture2, nullptr });
		//
		//myHalfSizeTexture2->SetAsActiveTarget();
		//myPostFXRenderer.Render(EFXType::GaussianBlurVertical4, { myHalfSizeTexture, nullptr });
		//
		//myHalfSizeTexture->SetAsActiveTarget();
		//myPostFXRenderer.Render(EFXType::GaussianBlurHorizontal4, { myHalfSizeTexture2, nullptr });
		//
		//myHalfSizeTexture2->SetAsActiveTarget();
		//myPostFXRenderer.Render(EFXType::GaussianBlurVertical4, { myHalfSizeTexture, nullptr });

		myNewTarget->SetAsActiveTarget();
		myPostFXRenderer.Render(EFXType::BloomAdd, { myLastTarget, myHalfSizeTexture2 });

		auto tempTarget = myNewTarget;
		myNewTarget = myLastTarget;
		myLastTarget = tempTarget;
	}
	void CRenderManager::DoFXAA()
	{
		myNewTarget->SetAsActiveTarget();
		myPostFXRenderer.Render(EFXType::FXAA, { myLastTarget, nullptr });

		auto tempTarget = myNewTarget;
		myNewTarget = myLastTarget;
		myLastTarget = tempTarget;
	}
	void CRenderManager::DoTonemapping()
	{
		if (myRenderOptions.myUseHDR == false)
		{
			myAfterToneMapTexture->SetAsActiveTarget();
			myPostFXRenderer.Render(EFXType::Copy, { myLastTarget, nullptr });
			myLastTarget = myAfterToneMapTexture;
			myNewTarget = myAfterToneMapTexture2;
			return;
		}

		for (unsigned int i = 0; i < myNumTonemapTextures; ++i)
		{
			myTonemapTextures[i].SetAsActiveTarget();
			if (i == 0)
			{
				myPostFXRenderer.Render(EFXType::TonemapLum, { myLastTarget, nullptr });
			}
			else
			{
				myPostFXRenderer.Render(EFXType::Copy, { &myTonemapTextures[i - 1], nullptr });
			}
		}

		myAfterToneMapTexture->SetAsActiveTarget();
		myPostFXRenderer.Render(EFXType::Tonemap, { myLastTarget, &myTonemapTextures[myNumTonemapTextures - 1] });
		myLastTarget = myAfterToneMapTexture;
		myNewTarget = myAfterToneMapTexture2;
	}

	void CRenderManager::DoColorGrading()
	{
		myNewTarget->SetAsActiveTarget();
		if (myRenderOptions.myUseColorGrading == false)
		{
			myPostFXRenderer.Render(EFXType::Copy, { myLastTarget, nullptr });
		}
		else
		{
			myPostFXRenderer.Render(EFXType::ColorGrading, { myLastTarget, nullptr });
		}

		auto tempTarget = myNewTarget;
		myNewTarget = myLastTarget;
		myLastTarget = tempTarget;
	}

	const CU::Vector2f CRenderManager::GetClosestPow2(const CU::Vector2f& aVector)
	{
		unsigned int x = static_cast<unsigned int>(aVector.x);
		unsigned int y = static_cast<unsigned int>(aVector.y);

		x--;
		x |= x >> 1;
		x |= x >> 2;
		x |= x >> 4;
		x |= x >> 8;
		x |= x >> 16;
		x++;

		y--;
		y |= y >> 1;
		y |= y >> 2;
		y |= y >> 4;
		y |= y >> 8;
		y |= y >> 16;
		y++;

		/*y |= (y >> 1);
		y |= (y >> 2);
		y |= (y >> 4);
		y |= (y >> 8);
		y |= (y >> 16);*/

		return CU::Vector2f(static_cast<float>(x), static_cast<float>(y));
	}
}}