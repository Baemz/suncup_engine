#pragma once

struct ID3D11DeviceContext;
namespace sce { namespace gfx {
	struct SRenderData;
	class CSkyboxRenderer
	{
	public:
		CSkyboxRenderer();
		~CSkyboxRenderer();

		void Init();
		void RenderFrame(SRenderData& aRenderData);

	private:
		struct SInstanceBufferData
		{
			CU::Matrix44f myToWorld;
		};

	private:
		unsigned int myCBufferID;
		ID3D11DeviceContext* myContext;
	};

}}