#include "stdafx.h"
#include "DX11GBufferGI.h"

#define NUM_TARGETS 3
#define NUM_SRV 4
#define ENCODED_RADIANCE_UAV 0
#define ENCODED_RADIANCE_SRV 1
#define DECODED_RADIANCE_UAV 1
#define DECODED_RADIANCE_SRV 2
#define BOUNCED_RADIANCE_UAV 2
#define BOUNCED_RADIANCE_SRV 3
namespace sce { namespace gfx {

	CDX11RenderTargetGI::CDX11RenderTargetGI()
		: myUAVTarget(nullptr)
		, myShaderResource(nullptr)
		, myDepthStencil(nullptr)
		, myViewPort(nullptr)
		, myTexture(nullptr)
		, myTexture2(nullptr)
		, myStructuredBuffer(nullptr)
	{
		myDevice = CDirect3D11::GetAPI()->GetDevice();
		myContext = CDirect3D11::GetAPI()->GetContext();
	}


	CDX11RenderTargetGI::~CDX11RenderTargetGI()
	{
		if (myUAVTarget)
		{
			SAFE_RELEASE(myUAVTarget[ENCODED_RADIANCE_UAV]);
			SAFE_RELEASE(myUAVTarget[DECODED_RADIANCE_UAV]);
		}
		SAFE_RELEASE(myTexture);
		SAFE_RELEASE(myTexture2);
		SAFE_RELEASE(myStructuredBuffer);
		for (unsigned int srv = 0; srv < NUM_SRV; ++srv)
		{
			SAFE_RELEASE(myShaderResource[srv])
		}
		sce_delete(myShaderResource);
		sce_delete(myUAVTarget);
		SAFE_RELEASE(myDepthStencil);
		sce_delete(myViewPort);
	}

	void CDX11RenderTargetGI::Init(const EVoxelSize& aQualityLevel)
	{
		CU::Vector3ui size;
		myVoxelSize = aQualityLevel;
		switch (aQualityLevel)
		{
		default:
		case EVoxelSize::x128:
			size = { 128, 128, 128 };
			break;
		case EVoxelSize::x256:
			size = { 256, 256, 256 };
			break;
		case EVoxelSize::x64:
			size = { 64, 64, 64 };
			break;
		}

		D3D11_BUFFER_DESC structuredBufferDesc;
		structuredBufferDesc.StructureByteStride = sizeof(UINT) * 2;
		structuredBufferDesc.ByteWidth = structuredBufferDesc.StructureByteStride * static_cast<UINT>(size.x) * static_cast<UINT>(size.y) * static_cast<UINT>(size.z);
		structuredBufferDesc.Usage = D3D11_USAGE_DEFAULT;
		structuredBufferDesc.BindFlags = D3D11_BIND_UNORDERED_ACCESS | D3D11_BIND_SHADER_RESOURCE;
		structuredBufferDesc.CPUAccessFlags = 0;
		structuredBufferDesc.MiscFlags = D3D11_RESOURCE_MISC_BUFFER_STRUCTURED;

		D3D11_TEXTURE3D_DESC textureDesc;
		textureDesc.Width = static_cast<UINT>(size.x);
		textureDesc.Height = static_cast<UINT>(size.y);
		textureDesc.Depth = static_cast<UINT>(size.z);
		textureDesc.MipLevels = 0;
		textureDesc.Format = DXGI_FORMAT_R16G16B16A16_FLOAT;
		textureDesc.Usage = D3D11_USAGE_DEFAULT;
		textureDesc.BindFlags = D3D11_BIND_UNORDERED_ACCESS | D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_RENDER_TARGET;
		textureDesc.CPUAccessFlags = 0;
		textureDesc.MiscFlags = D3D11_RESOURCE_MISC_GENERATE_MIPS;


		ID3D11Texture2D* depthTexture = nullptr;
		D3D11_TEXTURE2D_DESC depthDesc = {};
		depthDesc.Width = static_cast<UINT>(CGraphicsEngineInterface::GetFullResolution().x);
		depthDesc.Height = static_cast<UINT>(CGraphicsEngineInterface::GetFullResolution().y);
		depthDesc.ArraySize = 1;
		depthDesc.Format = DXGI_FORMAT_R32_TYPELESS;
		depthDesc.Usage = D3D11_USAGE_DEFAULT;
		depthDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL | D3D11_BIND_SHADER_RESOURCE;
		depthDesc.SampleDesc.Count = 1;
		depthDesc.SampleDesc.Quality = 0;
		depthDesc.MipLevels = 1;
		depthDesc.CPUAccessFlags = 0;
		depthDesc.MiscFlags = 0;

		HRESULT result = myDevice->CreateTexture2D(&depthDesc, nullptr, &depthTexture);
		if (FAILED(result))
		{
			ENGINE_LOG("DX11-ERROR! Could not create depth-texture.");
			return;
		}

		D3D11_DEPTH_STENCIL_VIEW_DESC dsvdesc;
		dsvdesc.Flags = 0;
		dsvdesc.Format = DXGI_FORMAT_D32_FLOAT;
		dsvdesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
		dsvdesc.Texture2D.MipSlice = 0;
		result = myDevice->CreateDepthStencilView(depthTexture, &dsvdesc, &myDepthStencil);
		if (FAILED(result))
		{
			ENGINE_LOG("DX11-ERROR! Could not create depth-stencil.");
			return;
		}

		myUAVTarget = sce_newArray(ID3D11UnorderedAccessView*, NUM_TARGETS);
		myShaderResource = sce_newArray(ID3D11ShaderResourceView*, NUM_SRV);

		D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
		srvDesc.Format = DXGI_FORMAT_R32_FLOAT;
		srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
		srvDesc.Texture2D.MostDetailedMip = 0;
		srvDesc.Texture2D.MipLevels = 1;
		result = myDevice->CreateShaderResourceView(depthTexture, &srvDesc, &myShaderResource[0]);
		if (FAILED(result))
		{
			ENGINE_LOG("DX11-ERROR! Could not create depth shaderresouceview.");
			return;
		}

		D3D11_UNORDERED_ACCESS_VIEW_DESC uav_desc;
		ZeroMemory(&uav_desc, sizeof(uav_desc));
		uav_desc.ViewDimension = D3D11_UAV_DIMENSION_BUFFER;
		uav_desc.Format = DXGI_FORMAT_UNKNOWN;
		uav_desc.Buffer.FirstElement = 0;
		uav_desc.Buffer.NumElements = structuredBufferDesc.ByteWidth / structuredBufferDesc.StructureByteStride;
		
		// StructuredBuffer
		result = myDevice->CreateBuffer(&structuredBufferDesc, nullptr, &myStructuredBuffer);
		if (FAILED(result))
		{
			ENGINE_LOG("DX11-ERROR! Could not create empty Texture1D.");
			return;
		}
		result = myDevice->CreateUnorderedAccessView(myStructuredBuffer, &uav_desc, &myUAVTarget[ENCODED_RADIANCE_UAV]);
		if (FAILED(result))
		{
			ENGINE_LOG("DX11-ERROR! Could not create UnorderedAcessView.");
			return;
		}

		ZeroMemory(&srvDesc, sizeof(srvDesc));
		srvDesc.Format = DXGI_FORMAT_UNKNOWN;
		srvDesc.ViewDimension = D3D11_SRV_DIMENSION_BUFFEREX;
		srvDesc.Buffer.FirstElement = 0;
		srvDesc.Buffer.NumElements = static_cast<UINT>(size.x) * static_cast<UINT>(size.y) * static_cast<UINT>(size.z);
		srvDesc.Buffer.ElementOffset = 0;
		srvDesc.Buffer.ElementWidth = structuredBufferDesc.StructureByteStride;

		result = myDevice->CreateShaderResourceView(myStructuredBuffer, &srvDesc, &myShaderResource[ENCODED_RADIANCE_SRV]);
		if (FAILED(result))
		{
			ENGINE_LOG("DX11-ERROR! Could not create shaderresouceview.");
			return;
		}

		// 3D-Texture
		result = myDevice->CreateTexture3D(&textureDesc, nullptr, &myTexture);
		result = myDevice->CreateTexture3D(&textureDesc, nullptr, &myTexture2);
		if (FAILED(result))
		{
			ENGINE_LOG("DX11-ERROR! Could not create empty Texture3D.");
			return;
		}
		ZeroMemory(&uav_desc, sizeof(uav_desc));
		uav_desc.ViewDimension = D3D11_UAV_DIMENSION_TEXTURE3D;
		uav_desc.Texture3D.FirstWSlice = 0;
		uav_desc.Texture3D.MipSlice = 0;
		uav_desc.Texture3D.WSize = textureDesc.Depth;
		result = myDevice->CreateUnorderedAccessView(myTexture, &uav_desc, &myUAVTarget[DECODED_RADIANCE_UAV]);
		result = myDevice->CreateUnorderedAccessView(myTexture2, &uav_desc, &myUAVTarget[BOUNCED_RADIANCE_UAV]);
		if (FAILED(result))
		{
			ENGINE_LOG("DX11-ERROR! Could not create UnorderedAcessView.");
			return;
		}
		result = myDevice->CreateShaderResourceView(myTexture, nullptr, &myShaderResource[DECODED_RADIANCE_SRV]);
		result = myDevice->CreateShaderResourceView(myTexture2, nullptr, &myShaderResource[BOUNCED_RADIANCE_SRV]);
		if (FAILED(result))
		{
			ENGINE_LOG("DX11-ERROR! Could not create shaderresouceview.");
			return;
		}

		myViewPort = sce_new(D3D11_VIEWPORT());
		myViewPort->TopLeftX = 0;
		myViewPort->TopLeftY = 0;
		myViewPort->Width = (FLOAT)size.x;
		myViewPort->Height = (FLOAT)size.y;
		myViewPort->MinDepth = 0.0f;
		myViewPort->MaxDepth = 1;


		
	}

	void CDX11RenderTargetGI::SetAsRenderTarget()
	{
		myContext->OMSetRenderTargetsAndUnorderedAccessViews(0, nullptr, nullptr, 0, 1, &myUAVTarget[ENCODED_RADIANCE_UAV], nullptr);
		myContext->RSSetViewports(1, myViewPort);
	}
	void CDX11RenderTargetGI::UnbindRenderTarget()
	{
		ID3D11UnorderedAccessView* nullUAV[1] = {nullptr};
		myContext->OMSetRenderTargetsAndUnorderedAccessViews(0, nullptr, nullptr, 0, 1, nullUAV, nullptr);
	}
	void CDX11RenderTargetGI::BindRadianceAsResourceOnSlot(const unsigned int aSlot)
	{
		if (myShaderResource[DECODED_RADIANCE_SRV])
		{
			myContext->GenerateMips(myShaderResource[DECODED_RADIANCE_SRV]);

			ID3D11Resource* resource = nullptr;
			myShaderResource[DECODED_RADIANCE_SRV]->GetResource(&resource);

			if (resource == nullptr)
			{
				return;
			}

			ID3D11Texture3D* texture = reinterpret_cast<ID3D11Texture3D*>(resource);
			D3D11_TEXTURE3D_DESC textDesc;
			texture->GetDesc(&textDesc);
			myMips = textDesc.MipLevels;
			resource->Release();

			myContext->PSSetShaderResources(aSlot, 1, &myShaderResource[DECODED_RADIANCE_SRV]);
		}
	}
	void CDX11RenderTargetGI::CSBindRadianceAsResourceOnSlot(const unsigned int aSlot)
	{
		if (myShaderResource[DECODED_RADIANCE_SRV])
		{
			myContext->GenerateMips(myShaderResource[DECODED_RADIANCE_SRV]);
			myContext->CSSetShaderResources(aSlot, 1, &myShaderResource[DECODED_RADIANCE_SRV]);
		}
	}
	void CDX11RenderTargetGI::BindBouncedRadianceAsResourceOnSlot(const unsigned int aSlot)
	{
		if (myShaderResource[BOUNCED_RADIANCE_SRV])
		{
			myContext->GenerateMips(myShaderResource[BOUNCED_RADIANCE_SRV]);

			ID3D11Resource* resource = nullptr;
			myShaderResource[BOUNCED_RADIANCE_SRV]->GetResource(&resource);

			if (resource == nullptr)
			{
				return;
			}

			ID3D11Texture3D* texture = reinterpret_cast<ID3D11Texture3D*>(resource);
			D3D11_TEXTURE3D_DESC textDesc;
			texture->GetDesc(&textDesc);
			myMips = textDesc.MipLevels;
			resource->Release();

			myContext->PSSetShaderResources(aSlot, 1, &myShaderResource[BOUNCED_RADIANCE_SRV]);
		}
	}
	void CDX11RenderTargetGI::VSBindRadianceAsResourceOnSlot(const unsigned int aSlot)
	{
		if (myShaderResource[DECODED_RADIANCE_SRV])
		{
			myContext->GenerateMips(myShaderResource[DECODED_RADIANCE_SRV]);
			myContext->VSSetShaderResources(aSlot, 1, &myShaderResource[DECODED_RADIANCE_SRV]);
		}
	}
	void CDX11RenderTargetGI::VSBindBouncedRadianceAsResourceOnSlot(const unsigned int aSlot)
	{
		if (myShaderResource[BOUNCED_RADIANCE_SRV])
		{
			myContext->GenerateMips(myShaderResource[BOUNCED_RADIANCE_SRV]);
			myContext->VSSetShaderResources(aSlot, 1, &myShaderResource[BOUNCED_RADIANCE_SRV]);
		}
	}
	void CDX11RenderTargetGI::CSBindDecodedRadianceAsUAVOnSlot(const unsigned int aSlot)
	{
		if (myUAVTarget[DECODED_RADIANCE_UAV])
		{
			myContext->CSSetUnorderedAccessViews(aSlot, 1, &myUAVTarget[DECODED_RADIANCE_UAV], nullptr);
		}
	}
	void CDX11RenderTargetGI::CSBindBouncedRadianceAsUAVOnSlot(const unsigned int aSlot)
	{
		if (myUAVTarget[BOUNCED_RADIANCE_UAV])
		{
			myContext->CSSetUnorderedAccessViews(aSlot, 1, &myUAVTarget[BOUNCED_RADIANCE_UAV], nullptr);
		}
	}
	void CDX11RenderTargetGI::CSBindEncodedRadianceAsUAVOnSlot(const unsigned int aSlot)
	{
		if (myUAVTarget[ENCODED_RADIANCE_UAV])
		{
			myContext->CSSetUnorderedAccessViews(aSlot, 1, &myUAVTarget[ENCODED_RADIANCE_UAV], nullptr);
		}
	}
	void CDX11RenderTargetGI::CSBindEncodedRadianceAsResourceOnSlot(const unsigned int aSlot)
	{
		if (myShaderResource[ENCODED_RADIANCE_SRV])
		{
			myContext->CSSetShaderResources(aSlot, 1, &myShaderResource[ENCODED_RADIANCE_SRV]);
		}
	}
	void CDX11RenderTargetGI::CSUnbindUAVOnSlot(const unsigned int aSlot)
	{
		ID3D11UnorderedAccessView* nullUAV = nullptr;
		myContext->CSSetUnorderedAccessViews(aSlot, 1, &nullUAV, nullptr);
	}
	void CDX11RenderTargetGI::CSUnbindResourceOnSlot(const unsigned int aSlot)
	{
		ID3D11ShaderResourceView* nullResource = nullptr;
		myContext->CSSetShaderResources(aSlot, 1, &nullResource);
	}
	const EVoxelSize & CDX11RenderTargetGI::GetVoxelSize() const
	{
		return myVoxelSize;
	}
	const unsigned int CDX11RenderTargetGI::GetMips() const
	{
		return myMips;
	}
}}