#pragma once
#include "../../CommonUtilities/Vector.h"
struct SSimpleDebugVertex
{
	CU::Vector4f myPosition;
	CU::Vector4f myColor;
	float t;
};

struct SSimpleVertex
{
	float x, y, z, w;
	float r, g, b, a;
};

struct SSimpleTexturedVertex
{
	float x, y, z, w;
	float u, v;
};

struct SModelVertex
{
	CU::Vector4f myPosition;
	CU::Vector4f myNormals;
	CU::Vector4f myTangents;
	CU::Vector4f myBitangents;
	CU::Vector2f myUV;
	CU::Vector4f myBones;
	CU::Vector4f myWeights;
};

struct SParticle
{
	SParticle()
		: mySize(1.f)
		, myCurrentLifeTime(0.f)
	{};

	SParticle(const CU::Vector4f& aPosition, const CU::Vector4f& aColor, const CU::Vector4f& aUV, const CU::Vector3f& aDirection, const float aSize, const float aRotation, const bool aGradient, const float aRotationVel)
		: myPosition(aPosition)
		, myColor(aColor)
		, myDirection(aDirection)
		, mySize(aSize)
		, myRotation(aRotation)
		, myCurrentLifeTime(0.f)
		, myGradient(aGradient)
		, myRotationVelocity(aRotationVel)
		, myUV(aUV)
	{};

	CU::Vector4f myPosition;
	CU::Vector4f myColor;
	CU::Vector4f myUV;
	float mySize;
	float myRotation;
	float myCurrentLifeTime;
	CU::Vector3f myDirection;
	bool myGradient; // false == 0 && true == 1
	float myRotationVelocity;
};

struct SStreak
{
	SStreak()
		: mySize(1.f)
		, myCurrentLifeTime(0.f)
	{};

	SStreak(const CU::Vector4f& aPosition, const CU::Vector4f& aColor, const CU::Vector3f& aDirection, const float aSize)
		: myPosition(aPosition)
		, myColor(aColor)
		, myDirection(aDirection)
		, mySize(aSize)
		, myCurrentLifeTime(0.f)
	{};

	SStreak(const SStreak& aStreak)
	{
		myPosition = aStreak.myPosition;
		myColor = aStreak.myColor;
		myDirection = aStreak.myDirection;
		mySize = aStreak.mySize;
		myCurrentLifeTime = aStreak.myCurrentLifeTime;
	}

	CU::Vector4f myPosition;
	CU::Vector4f myColor;
	CU::Vector3f myDirection;
	float mySize;
	float myCurrentLifeTime;
};