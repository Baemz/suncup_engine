#pragma once
#include <string>
#include "..\EngineCore\MemoryPool\MemoryPool.h"
#include <stringapiset.h>

inline std::wstring ConvertStringToWString(const std::string& aString)
{
	std::wstring ws(aString.begin(), aString.end());
	return ws;
}

inline wchar_t* ConvertCharArrayToLPCWSTR(const char* charArray, wchar_t* aBuffer, const int& aBufferSize)
{
	MultiByteToWideChar(CP_ACP, 0, charArray, -1, aBuffer, aBufferSize);
	return aBuffer;
}

inline bool FileExists(const char* aFileName) {
	struct stat buffer;
	return (stat(aFileName, &buffer) == 0);
}

inline std::string GetFilename(const std::string& aPath)
{
	return aPath.substr(aPath.find_last_of("/\\") + 1);
}

template<class T>
inline void Swap(T& aFirst, T& aSecond)
{
	// F�r Jacobs skull
	/*aFirst ^= aSecond;
	aSecond ^= aFirst;
	aFirst ^= aSecond;*/

	T temp = aFirst;
	aFirst = aSecond;
	aSecond = temp;
}