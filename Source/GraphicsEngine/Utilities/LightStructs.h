#pragma once
#include "../CommonUtilities/Vector.h"
#define MAX_LIGHTS 8

struct SPointLightData
{
	unsigned int numLights;
	unsigned int __pad[3];
	struct PL
	{
		CU::Vector4f myPosition;
		CU::Vector3f myColor;
		float myRange;
		float myIntensity;
		float __pad2[3];
	}pointLights[MAX_LIGHTS];
};

struct SSpotLightData
{
	unsigned int numLights;
	unsigned int __pad[3];
	struct SL
	{
		CU::Vector4f myPosition;
		CU::Vector3f myColor;
		float myAngle;
		CU::Vector3f myDirection;
		float myRange;
		float myIntensity;
		float __pad2[3];
	}spotLights[MAX_LIGHTS];
};