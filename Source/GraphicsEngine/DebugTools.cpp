#include "stdafx.h"
#include "DebugTools.h"
namespace sce {
	namespace gfx {
		CDebugTools* CDebugTools::ourInstance = nullptr;

		void CDebugTools::Create()
		{
			ourInstance = sce_new(CDebugTools());
		}

		void CDebugTools::Destroy()
		{
			sce_delete(ourInstance);
		}

		CDebugTools* CDebugTools::Get()
		{
			return ourInstance;
		}

#ifndef USE_DEBUG_TOOLS
	}
}
#else
		CDebugTools::CDebugTools()
			: myFreeIndex(0)
			, myReadIndex(1)
			, myWriteIndex(2)
		{
			myDebugDataBuffers[myFreeIndex].myDebug2DLineData.myData.Reserve(200000);
			myDebugDataBuffers[myReadIndex].myDebug2DLineData.myData.Reserve(200000);
			myDebugDataBuffers[myWriteIndex].myDebug2DLineData.myData.Reserve(200000);

			myDebugDataBuffers[myFreeIndex].myDebugLineData.myData.Reserve(200000);
			myDebugDataBuffers[myReadIndex].myDebugLineData.myData.Reserve(200000);
			myDebugDataBuffers[myWriteIndex].myDebugLineData.myData.Reserve(200000);

			myDebugDataBuffers[myFreeIndex].myDebugCubeData.myData.Reserve(200000);
			myDebugDataBuffers[myReadIndex].myDebugCubeData.myData.Reserve(200000);
			myDebugDataBuffers[myWriteIndex].myDebugCubeData.myData.Reserve(200000);

			myDebugDataBuffers[myFreeIndex].myDebugRectangle3DData.myData.Reserve(200000);
			myDebugDataBuffers[myReadIndex].myDebugRectangle3DData.myData.Reserve(200000);
			myDebugDataBuffers[myWriteIndex].myDebugRectangle3DData.myData.Reserve(200000);

			myDebugDataBuffers[myFreeIndex].myDebugSphereData.myData.Reserve(20000);
			myDebugDataBuffers[myReadIndex].myDebugSphereData.myData.Reserve(20000);
			myDebugDataBuffers[myWriteIndex].myDebugSphereData.myData.Reserve(20000);
		}


		CDebugTools::~CDebugTools()
		{
			for (auto& buffer : myDebugDataBuffers)
			{
				if (buffer.myDebugSphereData.myData.Empty() == false)
				{
					buffer.myDebugSphereData.myData.Resize(buffer.myDebugSphereData.myData.Capacity());
				}

				for (auto& model : buffer.myDebugSphereData.myData)
				{
					model.myLoadingState = CModelInstance::ELoadingState::Loaded;
				}
			}
		}

		void CDebugTools::DrawLine(CU::Vector3f aStartPos, CU::Vector3f aEndPos, CU::Vector3f aStartColorRGB, CU::Vector3f aEndColorRGB, float aStartThickness, float aEndThickness)
		{
			DrawLine(aStartPos.x, aStartPos.y, aStartPos.z, aEndPos.x, aEndPos.y, aEndPos.z, aStartColorRGB.r, aStartColorRGB.g, aStartColorRGB.b, aEndColorRGB.r, aEndColorRGB.g, aEndColorRGB.b, aStartThickness, aEndThickness);
		}

		void CDebugTools::DrawLine(const float aStartPosX, const float aStartPosY, const float aStartPosZ, const float aEndPosX, const float aEndPosY, const float aEndPosZ, const unsigned char aStartColorR, const unsigned char aStartColorG, const unsigned char aStartColorB, const unsigned char aEndColorR, const unsigned char aEndColorG, const unsigned char aEndColorB, float aStartThickness, float aEndThickness)
		{
			DrawLine(aStartPosX, aStartPosY, aStartPosZ, aEndPosX, aEndPosY, aEndPosZ, (aStartColorR / 255.0f), (aStartColorG / 255.0f), (aStartColorB / 255.0f), (aEndColorR / 255.0f), (aEndColorG / 255.0f), (aEndColorB / 255.0f), aStartThickness, aEndThickness);
		}

		void CDebugTools::DrawLine(const float aStartPosX, const float aStartPosY, const float aStartPosZ, const float aEndPosX, const float aEndPosY, const float aEndPosZ, const float aStartColorR, const float aStartColorG, const float aStartColorB, const float aEndColorR, const float aEndColorG, const float aEndColorB, float aStartThickness, float aEndThickness)
		{
			SSimpleDebugVertex lineStart;
			lineStart.myPosition.x = aStartPosX;
			lineStart.myPosition.y = aStartPosY;
			lineStart.myPosition.z = aStartPosZ;
			lineStart.myPosition.w = 1.f;
			lineStart.myColor.r = aStartColorR;
			lineStart.myColor.g = aStartColorG;
			lineStart.myColor.b = aStartColorB;
			lineStart.myColor.a = 1.f;
			lineStart.t = aStartThickness;

			SSimpleDebugVertex lineEnd;
			lineEnd.myPosition.x = aEndPosX;
			lineEnd.myPosition.y = aEndPosY;
			lineEnd.myPosition.z = aEndPosZ;
			lineEnd.myPosition.w = 1.f;
			lineEnd.myColor.r = aEndColorR;
			lineEnd.myColor.g = aEndColorG;
			lineEnd.myColor.b = aEndColorB;
			lineEnd.myColor.a = 1.f;
			lineEnd.t = aEndThickness;

			std::shared_lock<std::shared_mutex> sharedLock(myBufferLock);
			std::unique_lock<std::mutex> uniqueLock(myDebugDataBuffers[myWriteIndex].myDebugLineData.myMutex);
			myDebugDataBuffers[myWriteIndex].myDebugLineData.myData.Add(lineStart);
			myDebugDataBuffers[myWriteIndex].myDebugLineData.myData.Add(lineEnd);
		}

		void CDebugTools::DrawLine2D(CU::Vector2f aStartScreenPos, CU::Vector2f aEndScreenPos, CU::Vector3f aStartColorRGB, CU::Vector3f aEndColorRGB)
		{
			DrawLine2D(aStartScreenPos.x, aStartScreenPos.y, aEndScreenPos.x, aEndScreenPos.y, aStartColorRGB.r, aStartColorRGB.g, aStartColorRGB.b, aEndColorRGB.r, aEndColorRGB.g, aEndColorRGB.b);
		}

		void CDebugTools::DrawLine2D(const float aStartScreenPosX, const float aStartScreenPosY, const float aEndScreenPosX, const float aEndScreenPosY, const unsigned char aStartColorR, const unsigned char aStartColorG, const unsigned char aStartColorB, const unsigned char aEndColorR, const unsigned char aEndColorG, const unsigned char aEndColorB)
		{
			DrawLine2D(aStartScreenPosX, aStartScreenPosY, aEndScreenPosX, aEndScreenPosY, (aStartColorR / 255.0f), (aStartColorG / 255.0f), (aStartColorB / 255.0f), (aEndColorR / 255.0f), (aEndColorG / 255.0f), (aEndColorB / 255.0f));
		}

		void CDebugTools::DrawLine2D(const float aStartScreenPosX, const float aStartScreenPosY, const float aEndScreenPosX, const float aEndScreenPosY, const float aStartColorR, const float aStartColorG, const float aStartColorB, const float aEndColorR, const float aEndColorG, const float aEndColorB)
		{
			SSimpleDebugVertex lineStart;
			lineStart.myPosition.x = aStartScreenPosX;
			lineStart.myPosition.y = aStartScreenPosY;
			lineStart.myPosition.z = 1.f;
			lineStart.myPosition.w = 1.f;
			lineStart.myColor.r = aStartColorR;
			lineStart.myColor.g = aStartColorG;
			lineStart.myColor.b = aStartColorB;
			lineStart.myColor.a = 1.f;
			lineStart.t = 1.f;

			SSimpleDebugVertex lineEnd;
			lineEnd.myPosition.x = aEndScreenPosX;
			lineEnd.myPosition.y = aEndScreenPosY;
			lineEnd.myPosition.z = 1.f;
			lineEnd.myPosition.w = 1.f;
			lineEnd.myColor.r = aEndColorR;
			lineEnd.myColor.g = aEndColorG;
			lineEnd.myColor.b = aEndColorB;
			lineEnd.myColor.a = 1.f;
			lineEnd.t = 1.f;

			std::shared_lock<std::shared_mutex> sharedLock(myBufferLock);
			std::unique_lock<std::mutex> uniqueLock(myDebugDataBuffers[myWriteIndex].myDebug2DLineData.myMutex);
			myDebugDataBuffers[myWriteIndex].myDebug2DLineData.myData.Add(lineStart);
			myDebugDataBuffers[myWriteIndex].myDebug2DLineData.myData.Add(lineEnd);
		}

		void CDebugTools::DrawLinePlane(const CU::Vector4f& aAABBWithYZero, const CU::Vector3f aColor)
		{
			const CU::Vector3f ii(aAABBWithYZero.x1, 0.0f, aAABBWithYZero.y1);
			const CU::Vector3f ia(aAABBWithYZero.x1, 0.0f, aAABBWithYZero.y2);
			const CU::Vector3f ai(aAABBWithYZero.x2, 0.0f, aAABBWithYZero.y1);
			const CU::Vector3f aa(aAABBWithYZero.x2, 0.0f, aAABBWithYZero.y2);

			DrawLine(ii, ia, aColor, aColor);
			DrawLine(ii, ai, aColor, aColor);
			DrawLine(aa, ia, aColor, aColor);
			DrawLine(aa, ai, aColor, aColor);
		}

		void CDebugTools::DrawCube(const CU::Vector3f aPos, const float aSize, const CU::Vector3f aCubeRGB)
		{
			DrawCube(aPos.x, aPos.y, aPos.z, aSize, aCubeRGB.r, aCubeRGB.g, aCubeRGB.b);
		}

		void CDebugTools::DrawCube(const float aPosX, const float aPosY, const float aPosZ, const float aSize, const unsigned char aR, const unsigned char aG, const unsigned char aB)
		{
			DrawCube(aPosX, aPosY, aPosZ, aSize, (aR / 255.0f), (aG / 255.0f), (aB / 255.0f));
		}

		void CDebugTools::DrawCube(const float aPosX, const float aPosY, const float aPosZ, const float aSize, const float aR, const float aG, const float aB)
		{
			SSimpleDebugVertex cubePos;
			cubePos.myPosition = { aPosX, aPosY, aPosZ, 1.f };
			cubePos.myColor = { aR, aG, aB, 1.f };
			cubePos.t = aSize;
			std::shared_lock<std::shared_mutex> sharedLock(myBufferLock);
			std::unique_lock<std::mutex> uniqueLock(myDebugDataBuffers[myWriteIndex].myDebugCubeData.myMutex);
			myDebugDataBuffers[myWriteIndex].myDebugCubeData.myData.Add(cubePos);
		}

		void CDebugTools::DrawRectangle3D(const CU::Vector3f aPos, const CU::Vector3f aSizes, const CU::Vector3f aRectangleRGB)
		{
			DrawRectangle3D(aPos.x, aPos.y, aPos.z, aSizes.x, aSizes.y, aSizes.z, aRectangleRGB.r, aRectangleRGB.g, aRectangleRGB.b);
		}

		void CDebugTools::DrawRectangle3D(const float aPosX, const float aPosY, const float aPosZ, const float aSizeX, const float aSizeY, const float aSizeZ, const unsigned char aR, const unsigned char aG, const unsigned char aB)
		{
			DrawRectangle3D(aPosX, aPosY, aPosZ, aSizeX, aSizeY, aSizeZ, (aR / 255.0f), (aG / 255.0f), (aB / 255.0f));
		}

		void CDebugTools::DrawRectangle3D(const float aPosX, const float aPosY, const float aPosZ, const float aSizeX, const float aSizeY, const float aSizeZ, const float aR, const float aG, const float aB)
		{
			SSimpleDebugVertex rectanglePos;
			rectanglePos.myPosition = { aPosX, aPosY, aPosZ, aSizeX };
			rectanglePos.myColor = { aR, aG, aB, aSizeY };
			rectanglePos.t = aSizeZ;
			std::shared_lock<std::shared_mutex> sharedLock(myBufferLock);
			std::unique_lock<std::mutex> uniqueLock(myDebugDataBuffers[myWriteIndex].myDebugRectangle3DData.myMutex);
			myDebugDataBuffers[myWriteIndex].myDebugRectangle3DData.myData.Add(rectanglePos);
		}

		void CDebugTools::DrawText2D(const std::string& aText, const CU::Vector2f& aPosition, float aSize, const CU::Vector3f& aColor)
		{
			DrawText2D(aText, aPosition.x, aPosition.y, aSize, aColor.r, aColor.g, aColor.b);
		}

		void CDebugTools::DrawText2D(const std::string aText, const float aPositionX, const float aPositionY, float aSize, const unsigned char aR, const unsigned char aG, const unsigned char aB)
		{
			DrawText2D(aText, aPositionX, aPositionY, aSize, (aR / 255.0f), (aG / 255.0f), (aB / 255.0f));
		}

		void CDebugTools::DrawText2D(const std::string& aText, const float aPositionX, const float aPositionY, float aSize, const float aR, const float aG, const float aB)
		{
			CText newText;
			newText.Init("Comic Sans MS", aText, aSize, { 0.0f }, CU::Vector2f(aPositionX, aPositionY), CText::ColorFloatToUint(CU::Vector3f(aR, aG, aB)));
			newText.Render();
		}
		
		void CDebugTools::DrawSphere(const CU::Vector3f& aPos, const float aRadius, const CU::Vector3f& aColor, const CU::Vector3f& aRotation,
			const unsigned int aRingCount, const unsigned int aSliceCount)
		{
			std::shared_lock<std::shared_mutex> sharedLock(myBufferLock);
			std::unique_lock<std::mutex> uniqueLock(myDebugDataBuffers[myWriteIndex].myDebugSphereData.myMutex);
			myDebugDataBuffers[myWriteIndex].myDebugSphereData.myData.Add(CModelInstance());

			CModelInstance& instance_ref(myDebugDataBuffers[myWriteIndex].myDebugSphereData.myData.GetLast());
			instance_ref.InitCustomSphere(aColor, aRadius, aRingCount, aSliceCount);
			instance_ref.SetPosition(aPos);
			instance_ref.Rotate(aRotation);
		}

		const CU::GrowingArray<SSimpleDebugVertex, unsigned int>& CDebugTools::GetDebugLineData()
		{
			std::shared_lock<std::shared_mutex> sharedLock(myBufferLock);
			return myDebugDataBuffers[myReadIndex].myDebugLineData.myData;
		}

		const CU::GrowingArray<SSimpleDebugVertex, unsigned int>& CDebugTools::GetDebug2DLineData()
		{
			std::shared_lock<std::shared_mutex> sharedLock(myBufferLock);
			return myDebugDataBuffers[myReadIndex].myDebug2DLineData.myData;
		}

		const CU::GrowingArray<SSimpleDebugVertex, unsigned int>& CDebugTools::GetDebugCubeData()
		{
			std::shared_lock<std::shared_mutex> sharedLock(myBufferLock);
			return myDebugDataBuffers[myReadIndex].myDebugCubeData.myData;
		}

		const CU::GrowingArray<SSimpleDebugVertex, unsigned int>& CDebugTools::GetRectangle3DData()
		{
			std::shared_lock<std::shared_mutex> sharedLock(myBufferLock);
			return myDebugDataBuffers[myReadIndex].myDebugRectangle3DData.myData;
		}

		const CU::GrowingArray<sce::gfx::CModelInstance, unsigned int>& CDebugTools::GetDebugSphereData()
		{
			std::shared_lock<std::shared_mutex> sharedLock(myBufferLock);
			return myDebugDataBuffers[myReadIndex].myDebugSphereData.myData;
		}

		void CDebugTools::ChangeUpdateBuffer()
		{
			std::unique_lock<std::shared_mutex> bufferLock(myBufferLock);
			Swap(myWriteIndex, myFreeIndex);
			bufferLock.unlock();
			myDebugDataBuffers[myWriteIndex].RemoveAll();
			myDebugDataBuffers[myWriteIndex].myIsRead = false;
		}

		void CDebugTools::ChangeRenderBuffer()
		{
			std::unique_lock<std::shared_mutex> bufferLock(myBufferLock);
			if (myDebugDataBuffers[myFreeIndex].myIsRead == false)
			{
				Swap(myReadIndex, myFreeIndex);
			}
			bufferLock.unlock();
			myDebugDataBuffers[myReadIndex].myIsRead = true;
		}
	}
}
#endif // !USE_DEBUG_TOOLS
