#include "stdafx.h"
#include "DeferredRendererGI.h"
#include "DX11GBufferGI.h"
#include "DirectXFramework\API\DX11Shader.h"
#include "ResourceManager\ResourceManager.h"
#include "Model\Loading\ModelFactory.h"
#include "DirectXFramework\Direct3D11.h"
#include "Camera\Camera.h"
#include "Material.h"
#include "Model\Model.h"
#include "..\EngineCore\EventSystem\EventStructs.h"
#include <mutex>
#include "Utilities\LightStructs.h"
#include "Lights\LightFactory.h"
#include "Lights\PointLight.h"
#include "Lights\SpotLight.h"
#include "Lights\SpotLightInstance.h"
#include "ImGUI\imgui.h"

static std::mutex ourLock;

#define TIME_DATA_CBUFFER_INDEX 0
#define CAMERA_DATA_CBUFFER_INDEX 1
#define INVERSECAMERA_DATA_CBUFFER_INDEX 2
static const float VOXEL_STRUCTURE_WIDTH = 256.f;

namespace sce { namespace gfx {

	CDeferredRendererGI::CDeferredRendererGI()
		: myGBuffer(nullptr)
		, myVoxelizerGS(nullptr)
		, myVoxelizerVS(nullptr)
		, myVoxelizerPS(nullptr)
		, myDebugVoxelGS(nullptr)
		, myDebugVoxelVS(nullptr)
		, myDebugVoxelPS(nullptr)
		, myContext(nullptr)
		, myDirLightMultiplier(1.0f)
		, myShouldRenderVoxels(false)
		, myShouldDoSecondBounce(false)
		, myDirectionalLightRotateX(0.f)
		, myDirectionalLightRotateY(0.f)
		, myDirectionalLightRotateZ(0.f)
	{
	}


	CDeferredRendererGI::~CDeferredRendererGI()
	{
	}

	void CDeferredRendererGI::Init(const EVoxelSize& aQualityLevel)
	{
		Destroy();
		//AttachToEventReceiving(Event::ESubscribedEvents(Event::eKeyInput));
		myContext = CDirect3D11::GetAPI()->GetContext();
		auto resourceManager = CResourceManager::Get();
		myGBuffer = sce_new(CDX11RenderTargetGI());
		myGBuffer->Init(aQualityLevel);

		myVoxelizerGS = sce_new(CDX11Shader());
		myVoxelizerGS->InitGeometry("Data/Shaders/GI_VoxelizationGS.hlsl", "GSMain");
		myVoxelizerVS = sce_new(CDX11Shader());
		myVoxelizerVS->InitVertex("Data/Shaders/GI_VoxelizationVS.hlsl", "VSMain");
		myVoxelizerPS = sce_new(CDX11Shader());
		myVoxelizerPS->InitPixel("Data/Shaders/GI_VoxelizationPS.hlsl", "PSMain");
		myVoxelizerCS = sce_new(CDX11Shader());
		myVoxelizerCS->InitCompute("Data/Shaders/VoxelRadianceDecodeCS.hlsl", "CSMain");
		mySecondBounceCS = sce_new(CDX11Shader());
		mySecondBounceCS->InitCompute("Data/Shaders/VoxelConeTraceSecondBounceCS.hlsl", "CSMain");
		myNormalClearCS = sce_new(CDX11Shader());
		myNormalClearCS->InitCompute("Data/Shaders/VoxelRadianceNormalClearCS.hlsl", "CSMain");

		// FOR DEBUGGING
		myDebugVoxelGS = sce_new(CDX11Shader());
		myDebugVoxelGS->InitGeometry("Data/Shaders/Debug_VoxelGS.hlsl", "GSMain");
		myDebugVoxelVS = sce_new(CDX11Shader());
		myDebugVoxelVS->InitVertex("Data/Shaders/Debug_VoxelVS.hlsl", "VSMain");
		myDebugVoxelPS = sce_new(CDX11Shader());
		myDebugVoxelPS->InitPixel("Data/Shaders/Debug_VoxelPS.hlsl", "PSMain");
		//******************************************************************************//

		myCBufferID = resourceManager->GetConstantBufferID("DeferredRendererGI");
		myTimerCBufferID = resourceManager->GetConstantBufferID("DeferredTimeBufferGI");
		myInstanceBonesCBufferID = resourceManager->GetConstantBufferID("DeferredBonesBufferGI");
		myCamInvCBufferID = resourceManager->GetConstantBufferID("DeferredInvCamBufferGI");
		myDirectionalLightBufferID = resourceManager->GetConstantBufferID("DeferredDirLightBufferGI");
		myVoxelDataBufferID = resourceManager->GetConstantBufferID("DeferredVoxelBufferGI");
		mySpotlightCBufferID = resourceManager->GetConstantBufferID("DeferredSpotlightBufferGI");
		myPointlightCBufferID = resourceManager->GetConstantBufferID("DeferredPointlightBufferGI");
		CDX11ConstantBuffer& voxelBuffer(resourceManager->GetConstantBufferWithID(myVoxelDataBufferID));
		myVoxelData.voxelGridSize = (aQualityLevel == EVoxelSize::x128) ? 128 : ((aQualityLevel == EVoxelSize::x256) ? 256 : 64);
		myVoxelData.voxelSize = 0.25f;
		myVoxelData.centerChangedThisFrame = 0;
		myVoxelData.useGI = 1;
		myVoxelData.voxelGridSizeInversed = 1.0f / myVoxelData.voxelGridSize;
		myVoxelData.voxelSizeInversed = 1.0f / myVoxelData.voxelSize;
		myVoxelData.coneAngleDiffuse = 0.125f;
		myVoxelData.coneAngleSpecular = 0.125f;
		voxelBuffer.SetData(myVoxelData);
	}

	void CDeferredRendererGI::Destroy()
	{
		DetachFromEventReceiving();
		sce_delete(myGBuffer);
		myContext = nullptr;
		sce_delete(myVoxelizerVS);
		sce_delete(myVoxelizerGS);
		sce_delete(myVoxelizerPS);
		sce_delete(myDebugVoxelGS);
		sce_delete(myDebugVoxelVS);
		sce_delete(myDebugVoxelPS);
	}

	void CDeferredRendererGI::BindVoxelRadiance()
	{
		if (myShouldDoSecondBounce)
		{
			myGBuffer->BindBouncedRadianceAsResourceOnSlot(10);
		}
		else
		{
			myGBuffer->BindRadianceAsResourceOnSlot(10);
		}
	}

	void CDeferredRendererGI::BindClosestLights(const CU::Vector3f& aPos, SRenderData& aRenderData)
	{
		CLightFactory* lightFactory = CLightFactory::Get();
		// Pointlights
		CU::GrowingArray<CPointLightInstance*> pointlights;
		pointlights.Reserve(MAX_LIGHTS); 
		
		for (unsigned short i = 0; i < aRenderData.myPointLightData.size(); ++i)
		{
			auto& pl = aRenderData.myPointLightData[i];
			const float dist1 = (pl.GetPosition() - aPos).Length2();

			if (dist1 > pl.myRange * pl.myRange)
			{
				continue;
			}
			else if (pointlights.size() < MAX_LIGHTS)
			{
				pointlights.Add(&pl);
			}
			else
			{
				for (unsigned short plIndex = 0; plIndex < pointlights.size(); ++plIndex)
				{
					auto& pl2 = pointlights[plIndex];
					const float dist2 = (pl2->GetPosition() - aPos).Length2();
					if (dist1 < dist2)
					{
						pointlights[plIndex] = &pl;
						break;
					}
				}
			}
		}

		SPointLightData plData;
		plData.numLights = pointlights.size();
		{
			unsigned int i = 0;
			for (auto& instance : pointlights)
			{
				CPointLight& pl(lightFactory->GetPointLightWithID(instance->myPointLightID));
				SPointLightData::PL data;
				data.myColor = pl.myColor;
				data.myIntensity = instance->myIntensity;
				data.myPosition = CU::Vector4f(instance->GetPosition(), 1.f);
				data.myRange = pl.myRange;
				plData.pointLights[i] = data;
				++i;
			}
		}
		CDX11ConstantBuffer& pointlightBuffer(CResourceManager::Get()->GetConstantBufferWithID(myPointlightCBufferID));
		pointlightBuffer.SetData(plData);
		pointlightBuffer.BindPS(5);


		// Spotlights
		CU::GrowingArray<CSpotLightInstance*> spotlights;
		spotlights.Reserve(MAX_LIGHTS);
		for (unsigned short i = 0; i < aRenderData.mySpotLightData.size(); ++i)
		{
			auto& sl = aRenderData.mySpotLightData[i];
			const float dist1 = (sl.GetPosition() - aPos).Length2();

			if (dist1 > sl.myRange * sl.myRange)
			{
				continue;
			}
			else if (spotlights.size() < MAX_LIGHTS)
			{
				spotlights.Add(&sl);
			}
			else
			{
				for (unsigned short plIndex = 0; plIndex < spotlights.size(); ++plIndex)
				{
					auto& sl2 = spotlights[plIndex];

					const float dist2 = (sl2->GetPosition() - aPos).Length2();

					if ((sl.GetPosition() - aPos).Length2() < (sl2->GetPosition() - aPos).Length2())
					{
						spotlights[plIndex] = &sl;
						break;
					}
				}
			}
		}

		SSpotLightData slData;
		slData.numLights = spotlights.size();
		{
			unsigned int i = 0;
			for (auto& instance : spotlights)
			{
				CSpotLight& sl(lightFactory->GetSpotLightWithID(instance->mySpotLightID));
				SSpotLightData::SL data;
				data.myColor = sl.myColor;
				data.myIntensity = instance->myIntensity;
				data.myPosition = CU::Vector4f(instance->GetPosition(), 1.f);
				data.myRange = sl.myRange;
				data.myAngle = sl.myAngle;
				data.myDirection = instance->myDirection;
				slData.spotLights[i] = data;
				++i;
			}
		}
		CDX11ConstantBuffer& spotlightBuffer(CResourceManager::Get()->GetConstantBufferWithID(mySpotlightCBufferID));
		spotlightBuffer.SetData(slData);
		spotlightBuffer.BindPS(6);
	}

	void CDeferredRendererGI::DoSecondaryBounce()
	{
		myGBuffer->CSBindRadianceAsResourceOnSlot(0);
		myGBuffer->CSBindEncodedRadianceAsResourceOnSlot(1);
		myGBuffer->CSBindBouncedRadianceAsUAVOnSlot(0);
		mySecondBounceCS->Bind();

		const unsigned int threadGroups = (UINT)((myVoxelData.voxelGridSize * myVoxelData.voxelGridSize * myVoxelData.voxelGridSize) / 64);
		myContext->Dispatch(threadGroups, 1, 1);
		mySecondBounceCS->Unbind();
		myGBuffer->CSUnbindResourceOnSlot(0);
		myGBuffer->CSUnbindResourceOnSlot(1);
		myGBuffer->CSUnbindUAVOnSlot(0);
	}

	void CDeferredRendererGI::DoRadianceDecode()
	{
		myGBuffer->CSBindEncodedRadianceAsUAVOnSlot(0);
		myGBuffer->CSBindDecodedRadianceAsUAVOnSlot(1);
		myVoxelizerCS->Bind();

		const unsigned int threadGroups = (UINT)((myVoxelData.voxelGridSize * myVoxelData.voxelGridSize * myVoxelData.voxelGridSize) / 256);
		myContext->Dispatch(threadGroups, 1, 1);
		myVoxelizerCS->Unbind();
		myGBuffer->CSUnbindUAVOnSlot(0);
		myGBuffer->CSUnbindUAVOnSlot(1);
	}

	void CDeferredRendererGI::ClearNormals()
	{
		myGBuffer->CSBindEncodedRadianceAsUAVOnSlot(0);
		myNormalClearCS->Bind();

		const unsigned int threadGroups = (UINT)((myVoxelData.voxelGridSize * myVoxelData.voxelGridSize * myVoxelData.voxelGridSize) / 256);
		myContext->Dispatch(threadGroups, 1, 1);
		myNormalClearCS->Unbind();
		myGBuffer->CSUnbindUAVOnSlot(0);
	}

	void CDeferredRendererGI::RenderVoxelScene(SRenderData& aRenderData)
	{
		if (myContext == nullptr)
		{
			return;
		}
		else if (aRenderData.myCameraInstance.myCameraID == UINT_MAX)
		{
			return;
		}
		else if (aRenderData.myListToRender.Empty())
		{
			return;
		}
		//ExecuteEvents();

		CResourceManager* resourceManager = CResourceManager::Get();
		CDX11ConstantBuffer& voxelBuffer(resourceManager->GetConstantBufferWithID(myVoxelDataBufferID));
		CU::Vector3f camPos(aRenderData.myCameraInstance.GetPosition());


		const float f = 0.05f / myVoxelData.voxelSize;
		CU::Vector3f center;// = CU::Vector3f(floorf(camPos.x * f) / f, floorf(camPos.y * f) / f, floorf(camPos.z * f) / f);

		//if (fabs((center - myVoxelData.voxelCenter).Length2()) > 0.f)
		//{
		//	myVoxelData.centerChangedThisFrame = 1;
		//}
		//else
		//{
			myVoxelData.centerChangedThisFrame = 0;
		//}

		myVoxelData.voxelCenter = center;
		myVoxelData.voxelRadianceMips = myGBuffer->GetMips();
		voxelBuffer.SetData(myVoxelData);
		voxelBuffer.BindPS(__VOXEL_DATABUFFER_SLOT);
		voxelBuffer.BindGS(__VOXEL_DATABUFFER_SLOT);
		voxelBuffer.BindVS(__VOXEL_DATABUFFER_SLOT);
		voxelBuffer.BindCS(__VOXEL_DATABUFFER_SLOT);


		CDX11ConstantBuffer& dirLightBuffer(resourceManager->GetConstantBufferWithID(myDirectionalLightBufferID));
		aRenderData.myDirLight.myLightData.myIntensity *= myDirLightMultiplier;
		SDirLightData dirLightData(aRenderData.myDirLight.myLightData);
		dirLightBuffer.SetData(dirLightData);
		dirLightBuffer.BindPS(3);

		myGBuffer->SetAsRenderTarget();
		if (myVoxelData.useGI < 1)
		{
			return;
		}


		myContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
		CModelFactory* modelFactory = CModelFactory::Get();

		SInstanceBufferData instanceData;
		CDX11ConstantBuffer& instanceCBuffer(resourceManager->GetConstantBufferWithID(myCBufferID));
		CDX11ConstantBuffer& instanceBoneCBuffer(resourceManager->GetConstantBufferWithID(myInstanceBonesCBufferID));

		CDirect3D11::GetAPI()->SetDefaultDepth();
		CDirect3D11::GetAPI()->DisableFaceCulling();
		CDirect3D11::GetAPI()->SetSamplerState(ESamplerState::Default, 0);
		CDirect3D11::GetAPI()->SetSamplerState(ESamplerState::Anisotropic_x16, 10);

		SBoneBufferData data;
		// Start at 1 because index [0] is reserved for skybox.
		for (unsigned int i = 1; i < aRenderData.myListToRender.Size(); ++i)
		{
			const CModelInstance& instance = aRenderData.myListToRender[i];
			if (instance.myModelID == UINT_MAX)
			{
				ENGINE_LOG("[%s] could not render, model not found.", instance.myModelName.c_str());
				continue;
			}
			if (instance.myMaterialID == UINT_MAX)
			{
				ENGINE_LOG("[%s] could not render, material not found.", instance.myModelName.c_str());
				continue;
			}
			CModel& model(resourceManager->GetModelWithID(instance.myModelID));


			if (model.myVertexBufferID == UINT_MAX || model.myIndexBufferID == UINT_MAX)
			{
				ENGINE_LOG("[%s] could not render, VBuffer not found.", instance.myModelName.c_str());
				continue;
			}
			CMaterial& mat(modelFactory->GetMaterialWithID(instance.myMaterialID));
			if (mat.IsLoading())
			{
				continue;
			}
			mat.BindOnlyTextures();

			CU::Matrix44f orientation(instance.myOrientation);
			orientation.ScaleBy(instance.myScale);
			instanceData.myToWorld = orientation;

			if (instance.IsAffectedByFog())
			{
				instanceData.useFog = 1;
			}
			else
			{
				instanceData.useFog = 0;
			}

			instanceCBuffer.SetData(instanceData);
			instanceCBuffer.BindVS(3);

			if (instance.myBoneTransforms.empty() == false)
			{
				unsigned int index = 0;
				for (auto& transfrom : instance.myBoneTransforms)
				{
					data.bones[index] = transfrom;
					++index;
				}
				data.useAnimation = 1;
				instanceBoneCBuffer.SetData(data);
			}
			else
			{
				SBoneBufferDataOnlyBool dataOnlyBool;
				dataOnlyBool.useAnimation = 0;
				instanceBoneCBuffer.SetData(dataOnlyBool);
			}
			instanceBoneCBuffer.BindVS(4);

			CDX11VertexBuffer& vb(resourceManager->GetVertexBufferWithID(model.myVertexBufferID));
			vb.Bind();

			CDX11IndexBuffer& ib(resourceManager->GetIndexBufferWithID(model.myIndexBufferID));
			ib.Bind();

			myVoxelizerGS->Bind();
			myVoxelizerPS->Bind();
			myVoxelizerVS->Bind();

			BindClosestLights(instance.GetPosition(), aRenderData);
			myContext->DrawIndexed(ib.GetIndexCount(), 0, 0);
		}

		for (unsigned int i = 0; i < aRenderData.myTransparentListToRender.Size(); ++i)
		{
			const CModelInstance& instance = aRenderData.myTransparentListToRender[i];
			if (instance.myModelID == UINT_MAX)
			{
				ENGINE_LOG("[%s] could not render, model not found.", instance.myModelName.c_str());
				continue;
			}
			if (instance.myMaterialID == UINT_MAX)
			{
				ENGINE_LOG("[%s] could not render, material not found.", instance.myModelName.c_str());
				continue;
			}
			CModel& model(resourceManager->GetModelWithID(instance.myModelID));


			if (model.myVertexBufferID == UINT_MAX || model.myIndexBufferID == UINT_MAX)
			{
				ENGINE_LOG("[%s] could not render, VBuffer not found.", instance.myModelName.c_str());
				continue;
			}
			CMaterial& mat(modelFactory->GetMaterialWithID(instance.myMaterialID));
			if (mat.IsLoading())
			{
				continue;
			}
			mat.BindOnlyTextures();

			CU::Matrix44f orientation(instance.myOrientation);
			orientation.ScaleBy(instance.myScale);
			instanceData.myToWorld = orientation;

			if (instance.IsAffectedByFog())
			{
				instanceData.useFog = 1;
			}
			else
			{
				instanceData.useFog = 0;
			}

			instanceCBuffer.SetData(instanceData);
			instanceCBuffer.BindVS(3);

			if (instance.myBoneTransforms.empty() == false)
			{
				unsigned int index = 0;
				for (auto& transfrom : instance.myBoneTransforms)
				{
					data.bones[index] = transfrom;
					++index;
				}
				data.useAnimation = 1;
				instanceBoneCBuffer.SetData(data);
			}
			else
			{
				SBoneBufferDataOnlyBool dataOnlyBool;
				dataOnlyBool.useAnimation = 0;
				instanceBoneCBuffer.SetData(dataOnlyBool);
			}
			instanceBoneCBuffer.BindVS(4);

			CDX11VertexBuffer& vb(resourceManager->GetVertexBufferWithID(model.myVertexBufferID));
			vb.Bind();

			CDX11IndexBuffer& ib(resourceManager->GetIndexBufferWithID(model.myIndexBufferID));
			ib.Bind();

			myVoxelizerGS->Bind();
			myVoxelizerPS->Bind();
			myVoxelizerVS->Bind();

			BindClosestLights(instance.GetPosition(), aRenderData);
			myContext->DrawIndexed(ib.GetIndexCount(), 0, 0);
		}
		myContext->GSSetShader(nullptr, nullptr, 0);
		myGBuffer->UnbindRenderTarget();
		CDirect3D11::GetAPI()->SetDefaultFaceCulling();

		DoRadianceDecode();
		if (myShouldDoSecondBounce)
		{
			DoSecondaryBounce();
		}
		ClearNormals();
	}

	void CDeferredRendererGI::DEBUG_RenderVoxelizedScene()
	{
		myContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_POINTLIST);
		CDirect3D11::GetAPI()->SetSamplerState(ESamplerState::Anisotropic_x16);
		CDirect3D11::GetAPI()->SetDefaultFaceCulling();
		CDirect3D11::GetAPI()->SetDefaultDepth();
		CResourceManager* resourceManager = CResourceManager::Get();
		myDebugVoxelVS->Bind();
		myDebugVoxelPS->Bind();
		myDebugVoxelGS->Bind();

		if (myShouldDoSecondBounce)
		{
			myGBuffer->VSBindBouncedRadianceAsResourceOnSlot(0);
		}
		else
		{
			myGBuffer->VSBindRadianceAsResourceOnSlot(0);
		}

		myContext->Draw(myVoxelData.voxelGridSize * myVoxelData.voxelGridSize * myVoxelData.voxelGridSize, 0);
		myContext->GSSetShader(nullptr, nullptr, 0);
	}

	void CDeferredRendererGI::RenderGUI(SRenderData& aRenderData)
	{
		//////////////////////////////////////////////////////////////////
		// CONTROLS
		ImGui::Begin("Controls");

		ImGui::SetWindowSize({ 162,150 });
		ImGui::SetWindowPos({ 898,20 });

		ImGui::Text("Toggle Free Look: [O]");
		ImGui::Text("Move Forward: [W]");
		ImGui::Text("Move Left: [A]");
		ImGui::Text("Move Backward: [S]");
		ImGui::Text("Move Right: [D]");
		ImGui::Text("Move Up: [Space]");
		ImGui::Text("Move Down: [Shift]");

		ImGui::End();
		//////////////////////////////////////////////////////////////////

		//////////////////////////////////////////////////////////////////
		// Directional Light Settings
		ImGui::Begin("Directional Light");

		ImGui::SetWindowSize({ 300,120 });
		ImGui::SetWindowPos({ 588,20 });
		if (ImGui::SliderFloat("Intensity", &myDirLightMultiplier, 0.0f, 5.0f))
		{
		}
		if (ImGui::SliderFloat("Rotation X", &myDirectionalLightRotateX, -360.0f, 360.0f))
		{
		}


		CU::Matrix44f mat = CU::Matrix44f::CreateRotateAroundX(CU::ToRadians(myDirectionalLightRotateX));
		aRenderData.myDirLight.myLightData.myToLightDirection = mat * aRenderData.myDirLight.myLightData.myToLightDirection;

		if (ImGui::SliderFloat("Rotation Y", &myDirectionalLightRotateY, -360.0f, 360.0f))
		{
		}
		CU::Matrix44f mat2 = CU::Matrix44f::CreateRotateAroundY(CU::ToRadians(myDirectionalLightRotateY));
		aRenderData.myDirLight.myLightData.myToLightDirection = mat2 * aRenderData.myDirLight.myLightData.myToLightDirection;
		if (ImGui::SliderFloat("Rotation Z", &myDirectionalLightRotateZ, -360.0f, 360.0f))
		{
		}
		CU::Matrix44f mat3 = CU::Matrix44f::CreateRotateAroundZ(CU::ToRadians(myDirectionalLightRotateZ));
		aRenderData.myDirLight.myLightData.myToLightDirection = mat3 * aRenderData.myDirLight.myLightData.myToLightDirection;

		ImGui::End();
		//////////////////////////////////////////////////////////////////

		bool useGI = (myVoxelData.useGI > 0) ? true : false;
		bool onlyGI = (myVoxelData.useGI == 2) ? true : false;

		ImGui::Begin("Global Illumination Settings"); // begin window

		ImGui::SetWindowSize({ 558,220 });
		ImGui::SetWindowPos({ 20,20 });
		if (ImGui::SliderFloat("Voxel Size", &myVoxelData.voxelSize, 0.01f, 2.0f))
		{
		}
		myVoxelData.voxelSizeInversed = 1.0f / myVoxelData.voxelSize;

		if (ImGui::Checkbox("Use Global Illumination", &useGI))
		{
		}
		if (ImGui::Checkbox("DEBUG Render Voxels", &myShouldRenderVoxels))
		{
		}
		if (useGI == false)
		{
			myVoxelData.useGI = 0;
		}
		else
		{
			if (ImGui::Checkbox("GI Second Bounce", &myShouldDoSecondBounce))
			{
			}
			if (ImGui::Checkbox("Only GI", &onlyGI))
			{
			}
			if (onlyGI == false)
			{
				myVoxelData.useGI = 1;
			}
			else
			{
				myVoxelData.useGI = 2;
			}
			if (ImGui::SliderFloat("Cone-trace angle diffuse", &myVoxelData.coneAngleDiffuse, 0.01f, 0.250f))
			{
			}
			if (ImGui::SliderFloat("Cone-trace angle specular", &myVoxelData.coneAngleSpecular, 0.01f, 0.250f))
			{
			}
		}

		ImGui::End();
	}

	void CDeferredRendererGI::ReceiveEvent(const Event::SEvent & aEvent)
	{
		Event::SEvent* newEvent(nullptr);

		if (aEvent.myType == Event::EEventType::eKeyInput)
		{
			newEvent = sce_new(Event::SKeyEvent(*reinterpret_cast<const Event::SKeyEvent*>(&aEvent)));
		}

		if (newEvent != nullptr)
		{
			std::unique_lock<std::mutex> lock(ourLock);
			if (myEvents.size() < 10000)
			{
				myEvents.Add(newEvent);
			}
			else
			{
				sce_delete(newEvent);
			}
		}
		
	}
	void CDeferredRendererGI::ExecuteEvents()
	{
		std::unique_lock<std::mutex> lock(ourLock);
		for (auto& currentEvent : myEvents)
		{
			const auto& event(*currentEvent);
			if (event.myType == Event::EEventType::eKeyInput)
			{
				const Event::SKeyEvent* keyMsgPtr = reinterpret_cast<const Event::SKeyEvent*>(&event);

				if (keyMsgPtr->key == 'K'  && keyMsgPtr->state == EButtonState::Down)
				{
					myVoxelData.voxelSize -= 0.001f;
					myVoxelData.voxelSizeInversed = 1.0f / myVoxelData.voxelSize;
					CU::Clamp(myVoxelData.voxelSize, 0.01f, 10.f);
					CDX11ConstantBuffer& voxelBuffer(CResourceManager::Get()->GetConstantBufferWithID(myVoxelDataBufferID));
					voxelBuffer.SetData(myVoxelData);
					voxelBuffer.BindPS(__VOXEL_DATABUFFER_SLOT);
					voxelBuffer.BindGS(__VOXEL_DATABUFFER_SLOT);
					voxelBuffer.BindVS(__VOXEL_DATABUFFER_SLOT);
					printf("vsize: %f\n", myVoxelData.voxelSize);

				}
				else if (keyMsgPtr->key == 'L'  && keyMsgPtr->state == EButtonState::Down)
				{
					myVoxelData.voxelSize += 0.001f;
					myVoxelData.voxelSizeInversed = 1.0f / myVoxelData.voxelSize;
					CU::Clamp(myVoxelData.voxelSize, 0.01f, 10.f);
					CDX11ConstantBuffer& voxelBuffer(CResourceManager::Get()->GetConstantBufferWithID(myVoxelDataBufferID));
					voxelBuffer.SetData(myVoxelData);
					voxelBuffer.BindPS(__VOXEL_DATABUFFER_SLOT);
					voxelBuffer.BindGS(__VOXEL_DATABUFFER_SLOT);
					voxelBuffer.BindVS(__VOXEL_DATABUFFER_SLOT);
				}
				else if (keyMsgPtr->key == '9'  && keyMsgPtr->state == EButtonState::Pressed)
				{
					SVoxelData dat(myVoxelData);
					if (dat.voxelGridSize == 128)
					{
						Init(EVoxelSize::x256);
					}
					else if (dat.voxelGridSize == 256)
					{
						Init(EVoxelSize::x64);
					}
					else
					{
						Init(EVoxelSize::x128);
					}
					//myVoxelData.traceFalloff = dat.traceFalloff;
					myVoxelData.voxelCenter = dat.voxelCenter;
					myVoxelData.voxelSize = dat.voxelSize;
					myVoxelData.voxelGridSizeInversed = 1.0f / myVoxelData.voxelGridSize;
					myVoxelData.voxelSizeInversed = 1.0f / myVoxelData.voxelSize;

					CDX11ConstantBuffer& voxelBuffer(CResourceManager::Get()->GetConstantBufferWithID(myVoxelDataBufferID));
					voxelBuffer.SetData(myVoxelData);
					voxelBuffer.BindPS(__VOXEL_DATABUFFER_SLOT);
					voxelBuffer.BindGS(__VOXEL_DATABUFFER_SLOT);
					voxelBuffer.BindVS(__VOXEL_DATABUFFER_SLOT);
				}
				else if (keyMsgPtr->key == '0'  && keyMsgPtr->state == EButtonState::Pressed)
				{
					SVoxelData dat(myVoxelData);
					if (dat.voxelGridSize == 128)
					{
						Init(EVoxelSize::x64);
					}
					else if (dat.voxelGridSize == 256)
					{
						Init(EVoxelSize::x128);
					}
					else
					{
						Init(EVoxelSize::x256);
					}
					//myVoxelData.traceFalloff = dat.traceFalloff;
					myVoxelData.voxelCenter = dat.voxelCenter;
					myVoxelData.voxelSize = dat.voxelSize;
					myVoxelData.voxelGridSizeInversed = 1.0f / myVoxelData.voxelGridSize;
					myVoxelData.voxelSizeInversed = 1.0f / myVoxelData.voxelSize;

					CDX11ConstantBuffer& voxelBuffer(CResourceManager::Get()->GetConstantBufferWithID(myVoxelDataBufferID));
					voxelBuffer.SetData(myVoxelData);
					voxelBuffer.BindPS(__VOXEL_DATABUFFER_SLOT);
					voxelBuffer.BindGS(__VOXEL_DATABUFFER_SLOT);
					voxelBuffer.BindVS(__VOXEL_DATABUFFER_SLOT);
				}
				else if (keyMsgPtr->key == '7'  && keyMsgPtr->state == EButtonState::Down)
				{
					myDirLightMultiplier -= 0.001f;
					CU::Clamp(myDirLightMultiplier, 0.0f, 10.f);
				}
				else if (keyMsgPtr->key == '8'  && keyMsgPtr->state == EButtonState::Down)
				{
					myDirLightMultiplier += 0.001f;
					CU::Clamp(myDirLightMultiplier, 0.0f, 10.f);
				}
				else if (keyMsgPtr->key == 'V'  && keyMsgPtr->state == EButtonState::Pressed)
				{
					myShouldRenderVoxels = !myShouldRenderVoxels;
				}
				else if (keyMsgPtr->key == 'G'  && keyMsgPtr->state == EButtonState::Pressed)
				{
					myVoxelData.useGI = (myVoxelData.useGI == 2) ? 0 : ((myVoxelData.useGI == 1) ? 2 : 1); 
					CDX11ConstantBuffer& voxelBuffer(CResourceManager::Get()->GetConstantBufferWithID(myVoxelDataBufferID));
					voxelBuffer.SetData(myVoxelData);
					voxelBuffer.BindPS(__VOXEL_DATABUFFER_SLOT);
					voxelBuffer.BindGS(__VOXEL_DATABUFFER_SLOT);
					voxelBuffer.BindVS(__VOXEL_DATABUFFER_SLOT);
				}
				else if (keyMsgPtr->key == 'B'  && keyMsgPtr->state == EButtonState::Pressed)
				{
					myShouldDoSecondBounce = !myShouldDoSecondBounce;
					if (myShouldDoSecondBounce)
					{
						printf("SecondBounce on\n");
					}
					else
					{
						printf("SecondBounce off\n");
					}
				}
				else if (keyMsgPtr->key == 'Y'  && keyMsgPtr->state == EButtonState::Down)
				{
					myVoxelData.coneAngleDiffuse -= 0.001f;
					CU::Clamp(myVoxelData.coneAngleDiffuse, 0.01f, 10.f);
					CDX11ConstantBuffer& voxelBuffer(CResourceManager::Get()->GetConstantBufferWithID(myVoxelDataBufferID));
					voxelBuffer.SetData(myVoxelData);
					voxelBuffer.BindPS(__VOXEL_DATABUFFER_SLOT);
					voxelBuffer.BindGS(__VOXEL_DATABUFFER_SLOT);
					voxelBuffer.BindVS(__VOXEL_DATABUFFER_SLOT);

				}
				else if (keyMsgPtr->key == 'U'  && keyMsgPtr->state == EButtonState::Down)
				{
					myVoxelData.coneAngleDiffuse += 0.001f;
					CU::Clamp(myVoxelData.coneAngleDiffuse, 0.01f, 10.f);
					CDX11ConstantBuffer& voxelBuffer(CResourceManager::Get()->GetConstantBufferWithID(myVoxelDataBufferID));
					voxelBuffer.SetData(myVoxelData);
					voxelBuffer.BindPS(__VOXEL_DATABUFFER_SLOT);
					voxelBuffer.BindGS(__VOXEL_DATABUFFER_SLOT);
					voxelBuffer.BindVS(__VOXEL_DATABUFFER_SLOT);
				}
			}

			sce_delete(currentEvent);
		}
		myEvents.RemoveAll();
	}
}}