#include "stdafx.h"
#include "GraphicsEngine.h"
#include "WindowHandler/WindowHandler.h"
#include "DirectXFramework/Direct3D11.h"
#include "Framework/Framework.h"
#include "ResourceManager/ResourceManager.h"

// Kanske inte b�sta st�llet att starta factory, men f�rnuvarande funkar det
#include "Model/Loading/ModelFactory.h"
#include "Lights/LightFactory.h"
#include "Camera/CameraFactory.h"
#include "Sprite/Sprite.h"
#include "Particles\ParticleFactory.h"
#include "ShaderEffects/ShaderEffectFactory.h"
#include "../EngineCore/DebugHUD.h"
#include "DebugTools.h"
#include "ImGUI/imgui.h"
#include "ImGUI/imgui_impl_dx11.h"
#include "Particles/ParticleEngine/ParticleInterface.h"

namespace sce { namespace gfx {

	static std::mutex myConsoleLock;

	CGraphicsEngine::CGraphicsEngine()
		: myDebugTools(nullptr)
		, myFramework(nullptr)
		, myWindowHandler(nullptr)
		, myShouldRender(true)
		, myDefaultCameraInstance(nullptr)
	{
	}


	CGraphicsEngine::~CGraphicsEngine()
	{
		sce_delete(myDefaultCameraInstance);

		sce_delete(myFramework);

		sce_delete(myWindowHandler);

		sce_delete(myWindowHandler);
		ImGui_ImplDX11_Shutdown();
		// Kanske inte b�sta st�llet att starta factory, men f�rnuvarande funkar det
		CModelFactory::Destroy();
		CLightFactory::Destroy();
		CParticleFactory::Destroy();
		CShaderEffectFactory::Destroy();
		CResourceManager::Destroy();
		CCameraFactory::Destroy();
		CDebugTools::Destroy();
		CParticleInterface::Destroy();

		if (myRenderData.myListToRender.Empty() == false)
		{
			myRenderData.myListToRender.Resize(myRenderData.myListToRender.Capacity());
		}
		if (myRenderData.myTransparentListToRender.Empty() == false)
		{
			myRenderData.myTransparentListToRender.Resize(myRenderData.myTransparentListToRender.Capacity());
		}
		if (myRenderData.myDebugData.myDebugSphereData.Empty() == false)
		{
			myRenderData.myDebugData.myDebugSphereData.Resize(myRenderData.myDebugData.myDebugSphereData.Capacity());
		}

		for (auto& model : myRenderData.myListToRender)
		{
			model.myLoadingState = CModelInstance::ELoadingState::Loaded;
		}
		for (auto& model : myRenderData.myTransparentListToRender)
		{
			model.myLoadingState = CModelInstance::ELoadingState::Loaded;
		}
		for (auto& model : myRenderData.myDebugData.myDebugSphereData)
		{
			model.myLoadingState = CModelInstance::ELoadingState::Loaded;
		}
	}

	bool CGraphicsEngine::Init(SWindowData& aSomeWindowData, CFileWatcherWrapper& aFileWatcher)
	{
		myWindowHandler = sce_new(CWindowHandler());
		if (!myWindowHandler->Init(aSomeWindowData))
		{
			ENGINE_LOG("ERROR! Could not init window.");
			return false;
		}

		myFramework = CFramework::Create(eRenderAPI::DirectX11);
		if (!myFramework->Init(*myWindowHandler, aSomeWindowData.myStartInFullScreen, aSomeWindowData.myUseVSync))
		{
			ENGINE_LOG("ERROR! Could not init framework.");
			return false;
		}
		//HACK
		myWindowHandler->EnableCursor();

		ENGINE_LOG("Initiating ImGUI...");
		ImGuiIO& io = ImGui::GetIO();
		io.DisplaySize.x = CGraphicsEngineInterface::GetResolution().x;
		io.DisplaySize.y = CGraphicsEngineInterface::GetResolution().y;
		io.ImeWindowHandle = myWindowHandler->GetWindowHandle();
		io.RenderDrawListsFn = NULL;

		ImGui_ImplDX11_Init(myWindowHandler->GetWindowHandle(), CDirect3D11::GetAPI()->GetDevice(), CDirect3D11::GetAPI()->GetContext());

		// Kanske inte b�sta st�llet att starta factory, men f�rnuvarande funkar det
		CModelFactory::Create(aFileWatcher);
		CLightFactory::Create();
		CParticleFactory::Create();
		CShaderEffectFactory::Create();
		CDebugTools::Create();
		CCameraFactory::Create(&myScene);
		CResourceManager::Create();
		CParticleInterface::Create();

		myDebugTools = CDebugTools::Get();
		SRenderOptions renderOptions;
		renderOptions.myUseBloom = aSomeWindowData.myUseBloom;
		renderOptions.myUseSSAO = aSomeWindowData.myUseSSAO;
		renderOptions.myUseFXAA = aSomeWindowData.myUseFXAA;
		renderOptions.myUseColorGrading = false;
#ifdef _MATEDIT
		renderOptions.myUseLinearFog = false;
		renderOptions.myAAType = EAntiAliasingType::SSAA;
#else 
		renderOptions.myAAType = (renderOptions.myUseFXAA) ? EAntiAliasingType::FXAA : EAntiAliasingType::NONE;
		renderOptions.myUseLinearFog = true;
#endif // _MATEDIT


		myRenderData.myDebugData.myDebugLineData.Reserve(200000);
		myRenderData.myDebugData.myDebugCubeData.Reserve(200000);
		myRenderData.myDebugData.myDebug2DLineData.Reserve(200000);
		myRenderData.myDebugData.myDebugRectangle3DData.Reserve(200000);
		myRenderData.myDebugData.myDebugSphereData.Reserve(20000);

		myRenderManager.SetRenderOptions(renderOptions);
		myRenderManager.Init(myFramework, myWindowHandler->GetValidResolution());
		myFramework->ActivateScreenTarget();
		SetResolution(CU::Vector2f(static_cast<float>(aSomeWindowData.myWidth), static_cast<float>(aSomeWindowData.myHeight)));

		myDefaultCameraInstance = sce_new(CCameraInstance);
		*myDefaultCameraInstance = sce::gfx::CCameraFactory::Get()->CreateCameraAtPosition({ 0.0f, 0.0f, 0.0f });

		ENGINE_LOG("SUCCESS! GraphicsEngine successfully initialized.");
		return true;
	}

	void CGraphicsEngine::BeginFrame()
	{
		if (myShouldRender == false)
		{
			return;
		}
#ifdef _RETAIL
		float clearColor[] = { 0.f, 0.f, 0.f, 1.f };
#else
		float clearColor[] = { 0.f, 0.f, 0.f, 1.f };
#endif
		myFramework->BeginFrame(clearColor);
		ImGui_ImplDX11_NewFrame();
	}

	void CGraphicsEngine::RenderFrame(const float aDeltaTime, const float aTotalTime)
	{
		if (myShouldRender == false)
		{
			return;
		}

		myRenderData.myCameraInstance = myScene.GetCamera();
		if (myRenderData.myCameraInstance.myCameraID == UINT_MAX)
		{
			myRenderData.myCameraInstance = *myDefaultCameraInstance;
		}
		myRenderData.myEnvLight = myScene.GetEnvironmentalLight();
		myRenderData.myDirLight = myScene.GetDirectionalLight();
		myScene.GetSpriteList(myRenderData.mySpritesToRender);
		myScene.GetSprite3DList(myRenderData.my3DSpritesToRender);
		myScene.GetTextList(myRenderData.myTextsToRender);
		myRenderData.myDebugData.myDebugLineData = myDebugTools->GetDebugLineData();
		myRenderData.myDebugData.myDebug2DLineData = myDebugTools->GetDebug2DLineData();
		myRenderData.myDebugData.myDebugCubeData = myDebugTools->GetDebugCubeData();
		myRenderData.myDebugData.myDebugRectangle3DData = myDebugTools->GetRectangle3DData();
		myRenderData.myDebugData.myDebugSphereData = myDebugTools->GetDebugSphereData();
		myDebugTools->ChangeRenderBuffer();

		bool shouldRender = false;
		shouldRender = myScene.Cull(myRenderData);

		myRenderData.myTimeData.myThreadDeltaTime = aDeltaTime;
		myRenderData.myTimeData.myThreadTotalTime = aTotalTime;
		
		myConsoleLock.lock();
		if (myConsoleRenderCallback != nullptr)
		{
			myConsoleRenderCallback();
		}
		myConsoleLock.unlock();
		
		myRenderManager.RenderFrame(myRenderData, myWindowHandler->GetFullWindowedResolution());

		myRenderData.myListToRender.RemoveAll();
		myRenderData.myTransparentListToRender.RemoveAll();
		myRenderData.myParticleEmittorsToRender.RemoveAll();
		myRenderData.myPointLightData.RemoveAll();
		myRenderData.mySpotLightData.RemoveAll();
		myRenderData.mySpritesToRender.RemoveAll();
		myRenderData.my3DSpritesToRender.RemoveAll();
		myRenderData.myTextsToRender.RemoveAll();
		myRenderData.myStreakEmittorsToRender.RemoveAll();
		myRenderData.myShaderEffectsToRender.RemoveAll();
		myRenderData.myDebugData.Clean();
	}

	void CGraphicsEngine::EndFrame()
	{
		if (myShouldRender == false)
		{
			CDebugHUD::SetMS(CDebugHUD::eMSTexts::Render);
			return;
		}
		myFramework->EndFrame();

		CDebugHUD::SetMS(CDebugHUD::eMSTexts::Render);
	}

	void CGraphicsEngine::EndGameUpdateFrame()
	{
		myScene.ChangeUpdateBuffer();
		myDebugTools->ChangeUpdateBuffer();
	}

	void CGraphicsEngine::TakeScreenshot(const wchar_t* aDestinationPath)
	{
		myShouldRender = false;
		Sleep(100);
		myFramework->TakeScreenshot(aDestinationPath);
		myShouldRender = true;
	}

	void CGraphicsEngine::ToggleFullscreen()
	{
		myShouldRender = false;
		//Sleep(100);
		myFramework->ToggleFullscreen();
		myShouldRender = true;
	}

	void CGraphicsEngine::SetResolution(const CU::Vector2f& aNewResolution)
	{
		myShouldRender = false;
		//Sleep(100);
		myRenderManager.DestroyContent();

		bool myChangedFullscreenState(false);
		if (myWindowHandler->IsFullscreen())
		{
			myFramework->ToggleFullscreen();
			myChangedFullscreenState = true;
		}

		myWindowHandler->SetResolution({ (unsigned int)aNewResolution.x, (unsigned int)aNewResolution.y });
		myFramework->SetResolution((unsigned int)aNewResolution.x, (unsigned int)aNewResolution.y);

		if (myChangedFullscreenState)
		{
			myFramework->ToggleFullscreen();
		}

		myRenderManager.Init(myFramework, aNewResolution);
		myShouldRender = true;
	}

	const CU::Vector2f CGraphicsEngine::GetResolution() const
	{
		return myWindowHandler->GetValidResolution();
	}

	const float CGraphicsEngine::GetRatio() const
	{
		return myWindowHandler->GetValidResolution().x / myWindowHandler->GetValidResolution().y;
	}

	void CGraphicsEngine::SetConsoleRenderCallback(std::function<void()> aFunc)
	{
		myConsoleLock.lock();
		myConsoleRenderCallback = aFunc;
		myConsoleLock.unlock();
	}

	const bool CGraphicsEngine::IsVsyncEnabled() const
	{
		return myFramework->IsVsyncEnabled();
	}

	const bool CGraphicsEngine::IsSSAOEnabled() const
	{
		return myRenderManager.IsSSAOEnabled();
	}

	const bool CGraphicsEngine::IsColorGradingEnabled() const
	{
		return myRenderManager.IsColorGradingEnabled();
	}

	const bool CGraphicsEngine::IsLinearFogEnabled() const
	{
		return myRenderManager.IsLinearFogEnabled();
	}

	const bool CGraphicsEngine::IsBloomEnabled() const
	{
		return myRenderManager.IsBloomEnabled();
	}

	const bool CGraphicsEngine::IsFXAAEnabled() const
	{
		return myRenderManager.IsFXAAEnabled();
	}

	void CGraphicsEngine::ToggleVsync()
	{
		myFramework->SetVsync(!myFramework->IsVsyncEnabled());
	}

	void CGraphicsEngine::ToggleSSAO()
	{
		myRenderManager.ToggleSSAO();
	}

	void CGraphicsEngine::ToggleColorGrading()
	{
		myRenderManager.ToggleColorGrading();
	}

	void CGraphicsEngine::ToggleLinearFog()
	{
		myRenderManager.ToggleLinearFog();
	}
	
	void CGraphicsEngine::ToggleFXAA()
	{
		myRenderManager.ToggleFXAA();
	}

	void CGraphicsEngine::SetVsync(const bool aUseVsync)
	{
		myFramework->SetVsync(aUseVsync);
	}

	void CGraphicsEngine::SetSSAO(const bool aBool)
	{
		myRenderManager.SetSSAO(aBool);
	}

	void CGraphicsEngine::SetLinearFog(const bool aBool)
	{
		myRenderManager.SetLinearFog(aBool);
	}

	void CGraphicsEngine::SetBloom(const bool aBool)
	{
		myRenderManager.SetBloom(aBool);
	}
	
	void CGraphicsEngine::SetFXAA(const bool aBool)
	{
		myRenderManager.SetFXAA(aBool);
	}

	void CGraphicsEngine::ToggleBloom()
	{
		myRenderManager.ToggleBloom();
	}

}}