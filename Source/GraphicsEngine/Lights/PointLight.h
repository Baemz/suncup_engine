#pragma once

namespace sce { namespace gfx {

	class CPointLight
	{
		friend class CForwardRenderer;
		friend class CDeferredRenderer;
		friend class CDeferredRendererGI;
		friend class CLightFactory;
		friend class CScene;
	public:
		CPointLight();
		~CPointLight();

	private:
		CU::Vector3f myColor;
		float myRange;
		unsigned int myPixelShader;
	};

}}