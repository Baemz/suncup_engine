#pragma once
#include "../CommonUtilities/Vector4.h"

#define MAX_POINTLIGHTS 750
#define MAX_SPOTLIGHTS 750

namespace sce { namespace gfx {

	class CPointLight;
	class CSpotLight;
	class CLightFactory
	{
	public:
		CLightFactory();
		~CLightFactory();

		static void Create();
		static void Destroy();
		static CLightFactory* Get() { return ourInstance; };

		unsigned int CreateEnvironmentLight(const char* aCubemap);
		unsigned int CreatePointLight(const CU::Vector3f& aColor, const float aRange);
		unsigned int CreateSpotLight(const CU::Vector3f& aColor, const float aAngle, const float aRange);


		CPointLight& GetPointLightWithID(const unsigned int aID);
		CSpotLight& GetSpotLightWithID(const unsigned int aID);
	private:
		static CLightFactory* ourInstance;

		bool PLExists(const std::string& aIdentifier);
		unsigned int GetPointLightID(const std::string& aIdentifier);
		unsigned int myCurrentlyFreePointLightID;
		std::map<std::string, unsigned int> myAssignedPointLightIDs;
		CPointLight* myPointLights;

		bool SLExists(const std::string& aIdentifier);
		unsigned int GetSpotLightID(const std::string& aIdentifier);
		unsigned int myCurrentlyFreeSpotLightID;
		std::map<std::string, unsigned int> myAssignedSpotLightIDs;
		CSpotLight* mySpotLights;

	};

}}
