#pragma once
namespace sce { namespace gfx {

	class CSpotLight
	{
		friend class CForwardRenderer;
		friend class CDeferredRenderer;
		friend class CDeferredRendererGI;
		friend class CLightFactory;
		friend class CScene;
	public:
		CSpotLight();
		~CSpotLight();

	private:
		CU::Vector3f myColor;
		unsigned int myPixelShader;
		float myAngle;
		float myRange;
	};

}}