#pragma once

namespace sce { namespace gfx {

	class CEnvironmentalLight
	{
		friend class CLightFactory;
		friend class CDeferredRenderer;
		friend class CForwardRenderer;
	public:
		CEnvironmentalLight();
		~CEnvironmentalLight();

		void Init(const char* aCubemap);
		void UseForRendering();

	private:
		unsigned int myTextureID;
	};

}}