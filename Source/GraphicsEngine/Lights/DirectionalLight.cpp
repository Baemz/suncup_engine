#include "stdafx.h"
#include "DirectionalLight.h"
#include "GraphicsEngineInterface.h"

namespace sce { namespace gfx {

	CDirectionalLight::CDirectionalLight()
	{
		myLightData.myColor = { 0.0f, 0.0f, 0.0f, 0.0f };
		myLightData.myToLightDirection = { 0.0005f, 1.f, 0.0f, 0.f };
		myLightData.myToLightDirection = myLightData.myToLightDirection.GetNormalized();
		myLightData.myIntensity = 0.5f;
	}

	CDirectionalLight::~CDirectionalLight()
	{
	}
	void CDirectionalLight::UseForRendering()
	{
		CGraphicsEngineInterface::UseForRendering(*this);
	}
}}