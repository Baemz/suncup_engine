#include "stdafx.h"
#include "PointLightInstance.h"
#include "LightFactory.h"
#include "GraphicsEngineInterface.h"

namespace sce { namespace gfx {

	CPointLightInstance::CPointLightInstance()
		: myPointLightID(UINT_MAX)
		, myIsCastingShadows(false)
		, myRange(1.0f)
		, myIntensity(1.0f)
	{
	}


	CPointLightInstance::~CPointLightInstance()
	{
	}


	void CPointLightInstance::Init(const CU::Vector3f& aPosition, const CU::Vector3f& aColor, const float aRange, const float aIntensity)
	{
		CLightFactory* lf = CLightFactory::Get();
		if (lf)
		{
			myPointLightID = lf->CreatePointLight(aColor, aRange);
			myPosition = aPosition;
			myIntensity = aIntensity;
			myRange = aRange;
		}
		else
		{
			ENGINE_LOG("WARNING! No LightFactory created.");
		}
	}

	void CPointLightInstance::SetIsCastingShadows(const bool aBool)
	{
		myIsCastingShadows = aBool;
	}

	const bool CPointLightInstance::IsCastingShadows() const
	{
		return myIsCastingShadows;
	}

	void CPointLightInstance::SetPosition(const CU::Vector3f & aPos)
	{
		myPosition = aPos;
	}

	void CPointLightInstance::Render()
	{
		CGraphicsEngineInterface::AddToScene(*this);
	}

}}