#pragma once

namespace sce { namespace gfx {

	struct SDirLightData
	{
		CU::Vector4f myToLightDirection;
		CU::Vector4f myColor;
		float myIntensity;
	private:
		float pad[3];
	};

	class CDirectionalLight
	{
	public:
		CDirectionalLight();
		~CDirectionalLight();
		void UseForRendering();

		SDirLightData myLightData;
	};
}}
