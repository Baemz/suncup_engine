#include "stdafx.h"
#include "SpriteFactory.h"
#include "../ResourceManager/ResourceManager.h"
#include "DirectXFramework/API/DX11VertexBuffer.h"
#include "DirectXFramework/API/DX11IndexBuffer.h"
#include "DirectXFramework/API/DX11Shader.h"
#include "TexturedQuad\TexturedQuad.h"
#include "DirectXFramework/API/DX11Texture2D.h"

namespace sce { namespace gfx {

	CSpriteFactory::CSpriteFactory()
		: myCurrentlyFreeTexturedQuadID(0)
	{
		myTexturedQuads = sce_newArray(CTexturedQuad, MAX_TEXTUREDQUADS);
	}


	CSpriteFactory::~CSpriteFactory()
	{
		sce_delete(myTexturedQuads);
	}


	unsigned int CSpriteFactory::CreateSprite(const char* aSpritePath, CU::Vector2f& aTextureSizeRef)
	{
		unsigned int texID = GetTexturedQuadID(aSpritePath);
		CTexturedQuad& texQuad = GetTexturedQuadWithID(texID);

		if (FileExists(aSpritePath) == false)
		{
			RESOURCE_LOG("ERROR! Texture [%s] doesn't exist.", aSpritePath);
			return UINT_MAX;
		}

		CResourceManager* resourceManager = CResourceManager::Get();
		unsigned int modID = resourceManager->GetModelID(aSpritePath);
		unsigned int vBufferID = resourceManager->GetVertexBufferID(modID);
		CDX11VertexBuffer& vBuffer = resourceManager->GetVertexBufferWithID(vBufferID);
		if (vBuffer.CreateQuad() == false)
		{
			RESOURCE_LOG("ERROR! Could not create Quad-buffer.");
			return UINT_MAX;
		}
		texQuad.myVertexBufferID = vBufferID;

		unsigned int iBufferID = resourceManager->GetIndexBufferID(modID);
		CDX11IndexBuffer& iBuffer = resourceManager->GetIndexBufferWithID(iBufferID);
		if (iBuffer.CreateQuadIndex() == false)
		{
			RESOURCE_LOG("ERROR! Could not create Quad-buffer.");
			return UINT_MAX;
		}
		texQuad.myIndexBufferID = iBufferID;

		if (resourceManager->ShaderExists("Data/Shaders/Sprite_VS.hlsl"))
		{
			texQuad.myVertexShaderID = resourceManager->GetShaderID("Data/Shaders/Sprite_VS.hlsl");
		}
		else
		{
			unsigned int vertexShaderID = resourceManager->GetShaderID("Data/Shaders/Sprite_VS.hlsl");
			CDX11Shader& vertexShader(resourceManager->GetShaderWithID(vertexShaderID));
			vertexShader.InitVertex("Data/Shaders/Sprite_VS.hlsl", "VSMain");
			texQuad.myVertexShaderID = vertexShaderID;
		}

		if (resourceManager->ShaderExists("Data/Shaders/Sprite_PS.hlsl"))
		{
			texQuad.myPixelShaderID = resourceManager->GetShaderID("Data/Shaders/Sprite_PS.hlsl");
		}
		else
		{
			unsigned int pixelShaderID = resourceManager->GetShaderID("Data/Shaders/Sprite_PS.hlsl");
			CDX11Shader& pixelShader(resourceManager->GetShaderWithID(pixelShaderID));
			pixelShader.InitPixel("Data/Shaders/Sprite_PS.hlsl", "PSMain");
			texQuad.myPixelShaderID = pixelShaderID;
		}

		unsigned int textureID = resourceManager->GetTextureID(aSpritePath);
		CDX11Texture& texture(resourceManager->GetTextureWithID(textureID));
		if (texture.CreateTexture(aSpritePath, ETextureType::Diffuse) == false)
		{
			RESOURCE_LOG("ERROR! Could not create Quad-texture.");
			return UINT_MAX;
		}
		texQuad.myTextureSize = texture.GetTextureSize();
		texQuad.myTextureID = textureID;
		aTextureSizeRef = texQuad.myTextureSize;

		return texID;
	}

	unsigned int CSpriteFactory::Create3DSprite(const char * aSpritePath, CU::Vector2f & aTextureSizeRef)
	{
		unsigned int texID = GetTexturedQuadID(aSpritePath);
		CTexturedQuad& texQuad = GetTexturedQuadWithID(texID);

		if (FileExists(aSpritePath) == false)
		{
			RESOURCE_LOG("ERROR! Texture [%s] doesn't exist.", aSpritePath);
			return UINT_MAX;
		}

		CResourceManager* resourceManager = CResourceManager::Get();
		unsigned int modID = resourceManager->GetModelID(aSpritePath);
		unsigned int vBufferID = resourceManager->GetVertexBufferID(modID);
		CDX11VertexBuffer& vBuffer = resourceManager->GetVertexBufferWithID(vBufferID);
		if (vBuffer.CreateQuad() == false)
		{
			RESOURCE_LOG("ERROR! Could not create Quad-buffer.");
			return UINT_MAX;
		}
		texQuad.myVertexBufferID = vBufferID;

		unsigned int iBufferID = resourceManager->GetIndexBufferID(modID);
		CDX11IndexBuffer& iBuffer = resourceManager->GetIndexBufferWithID(iBufferID);
		if (iBuffer.CreateQuadIndex() == false)
		{
			RESOURCE_LOG("ERROR! Could not create Quad-buffer.");
			return UINT_MAX;
		}
		texQuad.myIndexBufferID = iBufferID;

		if (resourceManager->ShaderExists("Data/Shaders/Sprite3D_VS.hlsl"))
		{
			texQuad.myVertexShaderID = resourceManager->GetShaderID("Data/Shaders/Sprite3D_VS.hlsl");
		}
		else
		{
			unsigned int vertexShaderID = resourceManager->GetShaderID("Data/Shaders/Sprite3D_VS.hlsl");
			CDX11Shader& vertexShader(resourceManager->GetShaderWithID(vertexShaderID));
			vertexShader.InitVertex("Data/Shaders/Sprite3D_VS.hlsl", "VSMain");
			texQuad.myVertexShaderID = vertexShaderID;
		}

		if (resourceManager->ShaderExists("Data/Shaders/Sprite3D_PS.hlsl"))
		{
			texQuad.myPixelShaderID = resourceManager->GetShaderID("Data/Shaders/Sprite3D_PS.hlsl");
		}
		else
		{
			unsigned int pixelShaderID = resourceManager->GetShaderID("Data/Shaders/Sprite3D_PS.hlsl");
			CDX11Shader& pixelShader(resourceManager->GetShaderWithID(pixelShaderID));
			pixelShader.InitPixel("Data/Shaders/Sprite3D_PS.hlsl", "PSMain");
			texQuad.myPixelShaderID = pixelShaderID;
		}

		unsigned int textureID = resourceManager->GetTextureID(aSpritePath);
		CDX11Texture& texture(resourceManager->GetTextureWithID(textureID));
		if (texture.CreateTexture(aSpritePath, ETextureType::Diffuse) == false)
		{
			RESOURCE_LOG("ERROR! Could not create Quad-texture.");
			return UINT_MAX;
		}
		texQuad.myTextureSize = texture.GetTextureSize();
		texQuad.myTextureID = textureID;
		aTextureSizeRef = texQuad.myTextureSize;

		return texID;
	}

	bool CSpriteFactory::UnloadTexturedQuad(const char* aSpritePath)
	{
		aSpritePath;
		return false;
	}

	const CTexturedQuad& CSpriteFactory::GetTexturedQuadWithID(const unsigned int aID) const
	{
		return myTexturedQuads[aID];
	}

	CTexturedQuad& CSpriteFactory::GetTexturedQuadWithID(const unsigned int aID)
	{
		return myTexturedQuads[aID];
	}

	unsigned int CSpriteFactory::GetTexturedQuadID(const char* aSpritePath)
	{
		if (myAssignedTexturedQuadIDs.find(std::string(aSpritePath)) != myAssignedTexturedQuadIDs.end())
		{
			return myAssignedTexturedQuadIDs[std::string(aSpritePath)];
		}
		else
		{
			if (myCurrentlyFreeTexturedQuadID > (MAX_TEXTUREDQUADS - 1))
			{
				assert(false && "Too many sprites created");
				RESOURCE_LOG("WARNING! Too many sprites created");
				return UINT_MAX;
			}
			myAssignedTexturedQuadIDs[std::string(aSpritePath)] = myCurrentlyFreeTexturedQuadID;
			return myCurrentlyFreeTexturedQuadID++;
		}
	}

}}