#pragma once

#include "../CommonUtilities/Vector.h"
#include "../CommonUtilities/Matrix.h"
#include "Sprite.h"

namespace sce {
	namespace gfx {

		class CSprite3D : public CSprite
		{
			friend class CSpriteRenderer3D;

		public:
			CSprite3D() = default;
			CSprite3D(const char* aSpritePath);
			~CSprite3D() = default;

			void Init(const char* aSpritePath) override;
			void Render() const override;

			//inline void SetGlowColor(const CU::Vector3f& aGlowColor) { myGlowColor = aGlowColor; }
			inline void SetGlow(const float aGlow) { myGlow = aGlow; }
			void SetScreenSpacePosition(const CU::Vector3f& aPosition);
			void SetOrientation(const CU::Matrix44f& aOrientation);

			const CU::Matrix44f& GetOrientation() const { return myOrientation; }
			const float GetGlow() const { return myGlow; }

		private:
			CU::Matrix44f myOrientation;
			CU::Vector3f myGlowColor;
			float myGlow;
		};

	}
}