#include "stdafx.h"
#include "Sprite3D.h"
#include "../Model/Loading/ModelFactory.h"
#include "../GraphicsEngineInterface.h"

sce::gfx::CSprite3D::CSprite3D(const char * aSpritePath)
	: myGlow(0.0f)
	, myGlowColor({ 1.0f })
{
	CSprite::Init(aSpritePath);
}

void sce::gfx::CSprite3D::Init(const char * aSpritePath)
{
	myTexturedQuadID = CModelFactory::Get()->Create3DSprite(aSpritePath, myTextureSize);
}

void sce::gfx::CSprite3D::Render() const
{
	CGraphicsEngineInterface::AddToScene(*this);
}

void sce::gfx::CSprite3D::SetScreenSpacePosition(const CU::Vector3f & aPosition)
{
	aPosition;
	assert(false && "Fix!");
	//myOrientation.SetPosition(aPosition);
}

void sce::gfx::CSprite3D::SetOrientation(const CU::Matrix44f & aOrientation)
{
	myOrientation = aOrientation;
}
