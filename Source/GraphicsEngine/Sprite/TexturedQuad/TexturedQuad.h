#pragma once
namespace sce { namespace gfx {

	class CTexturedQuad
	{
		friend class CSpriteFactory;
		friend class CRenderer2D;
		friend class CSpriteRenderer3D;
	public:
		CTexturedQuad();
		~CTexturedQuad();

	private:
		CU::Vector2f myTextureSize;
		unsigned int myVertexShaderID;
		unsigned int myPixelShaderID;
		unsigned int myVertexBufferID;
		unsigned int myIndexBufferID;
		unsigned int myTextureID;
	};

}}