#pragma once

#include "../CommonUtilities/Vector.h"

namespace sce { namespace gfx { 

	class CSprite
	{
		friend class CRenderer2D;
	public:
		CSprite();
		CSprite(const char* aSpritePath);
		~CSprite();

		virtual void Init(const char* aSpritePath);
		virtual void Render() const;

		const bool IsInitialized() const;

		// Corresponds to the z-position in a 3D-space.
		void SetDepth(const float aDepthValue);
		const float GetDepth() const;

		// Sets the alpha of the sprite
		void SetAlpha(const float aAlpha);
		const float GetAlpha() const;

		// Sets the color of the sprite
		void SetColor(const CU::Vector3f& aColor);
		const CU::Vector3f GetColor() const;

		// The position expressed in normalized screen-space
		void SetPosition(const CU::Vector2f& aPosition);
		const CU::Vector2f GetPosition() const;

		const CU::Vector2f& GetOriginalSize() const;
		const CU::Vector2f GetPixelSize() const;

		// The normalized scale of the sprite relative to sprite original size (0 -> 1)
		void SetScale(const CU::Vector2f& aScale);
		const CU::Vector2f& GetScale() const;

		void SetCrop(const CU::Vector4f& aCrop);

		void SetRotation(float aRotation);
		float GetRotation();

		bool operator<(const CSprite& aOtherSprite);

		bool operator<=(const CSprite & aOtherSprite);

		bool operator>(const CSprite & aOtherSprite);

		bool operator>=(const CSprite & aOtherSprite);

		bool operator==(const CSprite & aOtherSprite);

		// The pivot of the sprite. 
		// (0, 0) = Top Left
		// (1, 1) = Bottom Right
		void SetPivot(const CU::Vector2f& aPivot);
		const CU::Vector2f& GetPivot() const;

		const CU::Vector2f GetScreenSpaceSize() const;

	protected:
		CU::Vector4f myColor;
		CU::Vector4f myCrop;
		CU::Vector3f myPosition;
		CU::Vector2f myScale;
		CU::Vector2f myPivot;
		CU::Vector2f myTextureSize;
		float myRotation;
		unsigned int myTexturedQuadID;
	};

}}