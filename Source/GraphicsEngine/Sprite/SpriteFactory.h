#pragma once

#define MAX_TEXTUREDQUADS 500

namespace sce { namespace gfx {

	class CSprite;
	class CTexturedQuad;
	class CSpriteFactory
	{
	public:
		CSpriteFactory();
		~CSpriteFactory();

		unsigned int CreateSprite(const char* aSpritePath, CU::Vector2f& aTextureSizeRef);
		//HACK
		unsigned int Create3DSprite(const char* aSpritePath, CU::Vector2f& aTextureSizeRef);

		bool UnloadTexturedQuad(const char* aSpritePath);
		const CTexturedQuad& GetTexturedQuadWithID(const unsigned int aID) const;
		CTexturedQuad& GetTexturedQuadWithID(const unsigned int aID);

	private:
		unsigned int GetTexturedQuadID(const char* aSpritePath);

		std::map<std::string, unsigned int> myAssignedTexturedQuadIDs;
		unsigned int myCurrentlyFreeTexturedQuadID;
		CTexturedQuad* myTexturedQuads;
	};

}}