#include "stdafx.h"
#include "Sprite.h"
#include "SpriteFactory.h"
#include "../Model/Loading/ModelFactory.h"
#include "../GraphicsEngineInterface.h"

namespace sce { namespace gfx {

	sce::gfx::CSprite::CSprite()
		: myTexturedQuadID(UINT_MAX)
		, myRotation(0.f)
		, myPivot(0.f, 0.f)
		, myScale(1.f, 1.f)
		, myCrop(0.0f, 0.0f, 1.f, 1.f)
		, myPosition(0.f, 1.f, 0.f)
		, myColor(1.0f, 1.0f, 1.0f, 1.f)
	{
	}

	sce::gfx::CSprite::CSprite(const char* aSpritePath)
	{
		Init(aSpritePath);
	}


	sce::gfx::CSprite::~CSprite()
	{
	}

	void sce::gfx::CSprite::Init(const char* aSpritePath)
	{
		myTexturedQuadID = CModelFactory::Get()->CreateSprite(aSpritePath, myTextureSize);
	}

	void CSprite::Render() const
	{
		CGraphicsEngineInterface::AddToScene(*this);
	}

	const bool CSprite::IsInitialized() const
	{
		return myTexturedQuadID != UINT_MAX;
	}

	void CSprite::SetDepth(const float aDepthValue)
	{
		if (aDepthValue < 0.f)
		{
			myPosition.z = 0.f;
		}
		else if (aDepthValue > 1.f)
		{
			myPosition.z = 1.f;
		}
		else
		{
			myPosition.z = aDepthValue;
		}
	}

	const float CSprite::GetDepth() const
	{
		return myPosition.z;
	}

	void CSprite::SetAlpha(const float aAlpha)
	{
		myColor.w = aAlpha;
	}
	const float CSprite::GetAlpha() const
	{
		return myColor.w;
	}

	void CSprite::SetColor(const CU::Vector3f& aColor)
	{
		myColor.x = aColor.x;
		myColor.y = aColor.y;
		myColor.z = aColor.z;
	}
	const CU::Vector3f CSprite::GetColor() const
	{
		return CU::Vector3f(myColor.x, myColor.y, myColor.z);
	}
	void CSprite::SetPosition(const CU::Vector2f& aPosition)
	{
		myPosition.x = aPosition.x;
		myPosition.y = 1.f - aPosition.y;
	}

	const CU::Vector2f CSprite::GetPosition() const
	{
		return CU::Vector2f(myPosition.x, 1.f - myPosition.y);
	}

	const CU::Vector2f& CSprite::GetOriginalSize() const
	{
		return myTextureSize;
	}

	const CU::Vector2f CSprite::GetPixelSize() const
	{
		const CU::Vector2f& resolution(CGraphicsEngineInterface::GetTargetResolution());
		return ((myTextureSize * myScale) / resolution) * CGraphicsEngineInterface::GetWindowedResolution();
	}

	void CSprite::SetScale(const CU::Vector2f& aScale)
	{
		myScale = aScale;
	}

	const CU::Vector2f& CSprite::GetScale() const
	{
		return myScale;
	}

	void CSprite::SetCrop(const CU::Vector4f& aCrop)
	{
		myCrop = aCrop;
	}

	void CSprite::SetRotation(float aRotation)
	{
		myRotation = aRotation;
	}

	float CSprite::GetRotation()
	{
		return myRotation;
	}

	bool CSprite::operator<(const CSprite& aOtherSprite)
	{
		return (aOtherSprite.myPosition.z < myPosition.z);
	}

	bool CSprite::operator<=(const CSprite& aOtherSprite)
	{
		return (myPosition.z <= aOtherSprite.myPosition.z);
	}

	bool CSprite::operator>(const CSprite& aOtherSprite)
	{
		return (myPosition.z > aOtherSprite.myPosition.z);
	}

	bool CSprite::operator>=(const CSprite& aOtherSprite)
	{
		return (myPosition.z >= aOtherSprite.myPosition.z);
	}

	bool CSprite::operator==(const CSprite& aOtherSprite)
	{
		return (myPosition.z == aOtherSprite.myPosition.z);
	}

	void CSprite::SetPivot(const CU::Vector2f& aPivot)
	{
		myPivot = aPivot;
	}

	const CU::Vector2f& CSprite::GetPivot() const
	{
		return myPivot;
	}

	const CU::Vector2f CSprite::GetScreenSpaceSize() const
	{
		const CU::Vector2f& resolution(CGraphicsEngineInterface::GetTargetResolution());
		return{ (myTextureSize.x * myScale.x) / resolution.x, (myTextureSize.y * myScale.y) / resolution.y };
	}

}}
