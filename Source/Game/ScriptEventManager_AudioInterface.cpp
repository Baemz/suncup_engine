#include "stdafx.h"
#include "ScriptEventManager_AudioInterface.h"
#include "ScriptEventManager.h"
#include "AudioWrapper.h"
#include "GameSystems\ExposedFunctions\GameLogicFunctions.h"

bool CScriptEventManager_AudioInterface::InitFunctions(CScriptEventManager& aScriptEventManager)
{
	CScriptManager& myScriptManager(aScriptEventManager.myScriptManager);

	std::function<void(const CScriptManager::StateIndexType, const char*)> postAudioEventBind(
		[&aScriptEventManager](const CScriptManager::StateIndexType aStateIndex, const char* aEventName)
	{
		CAudioWrapper::PostEvent(aEventName, aScriptEventManager.GetEntityUniqueIndex(aStateIndex));
	});
	myScriptManager.RegisterFunctionCallWithStateIndex("PostAudioEvent", postAudioEventBind,
		"String",
		"Triggers an audio event with a specified [name]. Example: post an event that plays an audio object.");

	std::function<void(const char*, float)> setAudioRTCPBind(
		[&aScriptEventManager](const char* aRTPC_Name, float aValue)
	{
		CAudioWrapper::SetRTPC(aRTPC_Name, aValue);
	});
	myScriptManager.RegisterFunction("SetAudioRTCP", setAudioRTCPBind,
		"String, Number",
		"For audio RTPC with a specified [name] set specialized [percentage value (0-100)] that modifies all audio or audio controllers connected to it. Examples for what it's value may modify: pitch or volume.\n");

	std::function<void(const char*, const char*)> setAudioStateBind(
		[&aScriptEventManager](const char* aStateGroupName, const char* aStateName)
	{
		CAudioWrapper::SetState(aStateGroupName, aStateName);
	});
	myScriptManager.RegisterFunction("SetAudioState", setAudioStateBind,
		"String, String",
		"For audio state group with a specified [name] set new [state name]. Example: if there is a state group with name 'MusicState' then your possible state choices for this group would be 'Pause' and 'Play'.");

	std::function<void(const CScriptManager::StateIndexType, const char*, float, float, float)> setAudioObjPositionBind(
		[&aScriptEventManager](const CScriptManager::StateIndexType aStateIndex, const char* aAudioObjectName, float aX, float aY, float aZ)
	{
		CAudioWrapper::SetObjectPosition(CU::Vector3f(aX, aY, aZ), aAudioObjectName, aScriptEventManager.GetEntityUniqueIndex(aStateIndex));
	});
	myScriptManager.RegisterFunctionCallWithStateIndex("SetAudioObjectPosition", setAudioObjPositionBind,
		"String, Number, Number, Number",
		"For an audio object with a specified [name] set position on new world coordinates ([x], [y] and [z]). Note: audio object's name will be exact the same as posted event's name that played that audio.");

	std::function<void(const CScriptManager::StateIndexType, const char*)> updateAudioObjPositionBind(
		[&aScriptEventManager](const CScriptManager::StateIndexType aStateIndex, const char* aAudioObjectName)
	{
		const auto entityIndex(aScriptEventManager.GetEntityUniqueIndex(aStateIndex));
		const CU::Vector3f& entityPosition(CGameLogicFunctions::GetEntityPosition(entityIndex));

		CAudioWrapper::SetObjectPosition(entityPosition, aAudioObjectName, entityIndex);
	});
	myScriptManager.RegisterFunctionCallWithStateIndex("UpdateAudioObjectPosition", updateAudioObjPositionBind,
		"String",
		"For an audio object with a specified [name] set position on entity's world coordinates. Note: audio object's name will be exact the same as posted event's name that played that audio.");

	return true;
}
