#pragma once
#include "State.h"
#include "../GraphicsEngine/Sprite/Sprite.h"

class CSplashScreenState: private CState
{
public:
	CSplashScreenState(EManager& aEntityManager);
	~CSplashScreenState();

	void Init() override;
	void Update(const float aDeltaTime) override;
	void Render() override;

	void OnActivation() override;
	void OnDeactivation() override;

private:
	const float GetAlpha(const float aEndTime, const float aStartTime) const;

private:
	sce::gfx::CSprite myTGALogoSprite;
	sce::gfx::CSprite myGroupLogoSprite;
	sce::gfx::CSprite myBackgroundSprite;
	float myTimeLeftToPop;
};