#include "stdafx.h"
#include "State.h"

#define DELETE_STATE(ptr) if (ptr != nullptr) { sce_delete(ptr); ptr = nullptr; }

CState::CState(EManager& aEntityManager)
	: myEntityManager(aEntityManager)
{
	myUnderStates.Init(2);
	myID = UINT_MAX;
	myState = EStateCommand::eMustInit;
	myCanRenderMainState = false;
}

CState::~CState()
{
	for (unsigned short i(0); i < myUnderStates.Size(); ++i)
	{
		DELETE_STATE(myUnderStates[i]);
	}
	myUnderStates.RemoveAll();
}

/* PRIVATE FUNCTIONS */

void CState::InitState(const unsigned int aID, const bool aCanRenderMainState)
{
	myID = aID;
	myCanRenderMainState = aCanRenderMainState;
}
