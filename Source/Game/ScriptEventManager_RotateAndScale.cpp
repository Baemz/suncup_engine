#include "stdafx.h"
#include "ScriptEventManager_RotateAndScale.h"
#include <shared_mutex>
#include "ScriptEventManager.h"
#include "GameSystems\ExposedFunctions\GameLogicFunctions.h"

#define CHECK_IF_STATE_IS_REMOVED(aStateIndex, aReturnValue)																\
std::shared_lock<std::shared_mutex> sharedLock(aScriptEventManager.myStatesToRemoveMutex);									\
if (aScriptEventManager.myStatesToRemove.Find(aStateIndex) != CU::GrowingArray<CScriptManager::StateIndexType>::FoundNone)	\
return aReturnValue;

bool CScriptEventManager_RotateAndScale::InitFunctions(CScriptEventManager& aScriptEventManager)
{
	CScriptManager& myScriptManager(aScriptEventManager.myScriptManager);

	std::function<void(const CScriptManager::StateIndexType, float, float, float)> scaleByBind(
		[&aScriptEventManager](const CScriptManager::StateIndexType aStateIndex, float aX, float aY, float aZ)
	{
		CHECK_IF_STATE_IS_REMOVED(aStateIndex, ;);
		auto index(aScriptEventManager.GetEntityUniqueIndex(aStateIndex));
		CGameLogicFunctions::ScaleBy(index, CU::Vector3f(aX, aY, aZ));
	});
	myScriptManager.RegisterFunctionCallWithStateIndex("ScaleBy", scaleByBind
		, "Number, Number, Number"
		, "Scales the entities model by ( [aX], [aY], [aZ] ).");

	std::function<void(const CScriptManager::StateIndexType, float, float, float)> setScaleBind(
		[&aScriptEventManager](const CScriptManager::StateIndexType aStateIndex, float aX, float aY, float aZ)
	{
		CHECK_IF_STATE_IS_REMOVED(aStateIndex, ;);
		auto index(aScriptEventManager.GetEntityUniqueIndex(aStateIndex));
		CGameLogicFunctions::SetScale(index, CU::Vector3f(aX, aY, aZ));
	});
	myScriptManager.RegisterFunctionCallWithStateIndex("SetScale", setScaleBind
		, "Number, Number, Number"
		, "Sets the scale of caller entity to ( [aX], [aY], [aZ] ).");

	std::function<void(const CScriptManager::StateIndexType, float, float, float)> lookAtPositionBind(
		[&aScriptEventManager](const CScriptManager::StateIndexType aStateIndex, float aX, float aY, float aZ)
	{
		CHECK_IF_STATE_IS_REMOVED(aStateIndex, ;);
		auto index(aScriptEventManager.GetEntityUniqueIndex(aStateIndex));
		CGameLogicFunctions::LookAtPosition(index, CU::Vector3f(aX, aY, aZ));
	});
	myScriptManager.RegisterFunctionCallWithStateIndex("LookAtPosition", lookAtPositionBind
		, "Number, Number, Number"
		, "Rotates the entity so it looks towards ( [aX], [aY], [aZ] ).");

	std::function<void(const CScriptManager::StateIndexType, UniqueIndexType)> lookAtEntityBind(
		[&aScriptEventManager](const CScriptManager::StateIndexType aStateIndex, UniqueIndexType aEntityUniqueIndex)
	{
		CHECK_IF_STATE_IS_REMOVED(aStateIndex, ;);
		auto index(aScriptEventManager.GetEntityUniqueIndex(aStateIndex));
		CGameLogicFunctions::LookAtEntity(index, aEntityUniqueIndex);
	});
	myScriptManager.RegisterFunctionCallWithStateIndex("LookAtEntity", lookAtEntityBind
		, "Number"
		, "Rotates the entity so it looks towards the [target].");

	std::function<void(const CScriptManager::StateIndexType, float, float, float)> lookAtPositionInverseBind(
		[&aScriptEventManager](const CScriptManager::StateIndexType aStateIndex, float aX, float aY, float aZ)
	{
		CHECK_IF_STATE_IS_REMOVED(aStateIndex, ;);
		auto index(aScriptEventManager.GetEntityUniqueIndex(aStateIndex));
		CGameLogicFunctions::LookAtPositionInverse(index, CU::Vector3f(aX, aY, aZ));
	});
	myScriptManager.RegisterFunctionCallWithStateIndex("LookAtPositionInverse", lookAtPositionInverseBind
		, "Number, Number, Number"
		, "Rotates the entity so it looks towards ( [aX], [aY], [aZ] ).");

	std::function<void(const CScriptManager::StateIndexType, UniqueIndexType)> lookAtEntityInverseBind(
		[&aScriptEventManager](const CScriptManager::StateIndexType aStateIndex, UniqueIndexType aEntityUniqueIndex)
	{
		CHECK_IF_STATE_IS_REMOVED(aStateIndex, ;);
		auto index(aScriptEventManager.GetEntityUniqueIndex(aStateIndex));
		CGameLogicFunctions::LookAtEntityInverse(index, aEntityUniqueIndex);
	});
	myScriptManager.RegisterFunctionCallWithStateIndex("LookAtEntityInverse", lookAtEntityInverseBind
		, "Number"
		, "Rotates the entity so it looks towards the [target].");

	return true;
}
