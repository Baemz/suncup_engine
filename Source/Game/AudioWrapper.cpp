#include "stdafx.h"
#include "AudioWrapper.h"
#include "../AudioEngine/AudioManager.h"
#include "GameSystems/ExposedFunctions/GameLogicFunctions.h"

CAudioWrapper* CAudioWrapper::myInstance_ptr = nullptr;

#define GET_NEW_REGISTER_ID (aGameObjectID + 2)
#define GET_POSTED_EVENT_ID (aEventName + std::string("_ID:") + std::to_string(aGameObjectID))

CAudioWrapper::CAudioWrapper()
	: myListenerIndex(EntityHandleINVALID.myUniqueIndex)
{
}

CAudioWrapper::~CAudioWrapper()
{
}

void CAudioWrapper::Create()
{
#ifndef AUDIO_ENGINE_OFF
	if (myInstance_ptr != nullptr)
	{
		AUDIO_LOG("[CAudioWrapper] WARNING! Tried to create a new instance of Audio Wrapper after it has been already created.");
		return;
	}

	myInstance_ptr = sce_new(CAudioWrapper());

	if (myInstance_ptr == nullptr)
	{
		AUDIO_LOG("[CAudioWrapper] ERROR! Failed to allocate memory for new Audio Wrapper instance.");
	}
#endif
}

void CAudioWrapper::Destroy()
{
#ifndef AUDIO_ENGINE_OFF
	if (myInstance_ptr == nullptr)
	{
		AUDIO_LOG("[CAudioWrapper] WARNING! Tried to destroy Audio Wrapper instance before it has been created.");
		return;
	}

	sce_delete(myInstance_ptr);
#endif
}

void CAudioWrapper::UpdateAudioPositions()
{
#ifndef AUDIO_ENGINE_OFF
	if (myInstance_ptr == nullptr)
	{
		AUDIO_LOG("[CAudioWrapper] ERROR! Tried to use Audio Wrapper functions before creating the instance of it.");
		return;
	}

	if (CGameLogicFunctions::IsEntityValid( static_cast<UniqueIndexType>(myInstance_ptr->myListenerIndex) ))
	{
		// CGameLogicFunctions IS NOT THREAD FRIENDLY!!! MAY CAUSE STRANGE SOUND BEHAVIOURS (like strange positioning of the audio).
		const CU::Vector3f objPosition(CGameLogicFunctions::GetEntityPosition( static_cast<UniqueIndexType>(myInstance_ptr->myListenerIndex) ));
		const CU::Vector3f objFront(CGameLogicFunctions::GetEntityFront( static_cast<UniqueIndexType>(myInstance_ptr->myListenerIndex) ));
		const CU::Vector3f objTop(CGameLogicFunctions::GetEntityTop( static_cast<UniqueIndexType>(myInstance_ptr->myListenerIndex) ));

		if ((objFront.x != 0.0f || objFront.y != 0.0f || objFront.z != 0.0f) && (objTop.x != 0.0f || objTop.y != 0.0f || objTop.z != 0.0f))
		{
			CAudioManager::SetListenerPositionAndOrientation(objPosition, objFront, objTop);
		}
		else
		{
			AUDIO_LOG("[CAudioWrapper] WARNING! Cound not update listener's position and orientation because the game object (entity) has invalid orientation.");
		}
	}

	std::shared_lock<std::shared_mutex> sharedLock(myInstance_ptr->mySharedMutex);
	for (const auto& postedEventPair : myInstance_ptr->myPostedEvents)
	{
		const SPostedEventData& postedEventData(postedEventPair.second);
		if (CGameLogicFunctions::IsEntityValid(postedEventData.myGameObjectID) == false || postedEventData.myCanAutoUpdatePosition == false)
		{
			continue;
		}

		// CGameLogicFunctions IS NOT THREAD FRIENDLY!!! MAY CAUSE STRANGE SOUND BEHAVIOURS (like strange positioning of the audio).
		const CU::Vector3f objPosition(CGameLogicFunctions::GetEntityPosition(postedEventData.myGameObjectID));

		CAudioManager::SetObjectPositionAndOrientation(objPosition, CU::Vector3f(0.0f, 0.0f, 1.0f), CU::Vector3f(0.0f, 1.0f, 0.0f), postedEventData.myAudioID);
	}
#endif
}

void CAudioWrapper::EmptyPostedEventContainer()
{
#ifndef AUDIO_ENGINE_OFF
	if (myInstance_ptr == nullptr)
	{
		AUDIO_LOG("[CAudioWrapper] ERROR! Tried to use Audio Wrapper functions before creating the instance of it.");
		return;
	}

	std::unique_lock<std::shared_mutex> uniqueLock(myInstance_ptr->mySharedMutex);
	myInstance_ptr->myPostedEvents.clear();
#endif
}

void CAudioWrapper::PostEvent(const char* aEventName, const UniqueIndexType& aGameObjectID, const AudioEngine::RegObjectID aRegisteredAudioObjectID)
{
#ifndef AUDIO_ENGINE_OFF
	if (myInstance_ptr == nullptr)
	{
		AUDIO_LOG("[CAudioWrapper] ERROR! Tried to use Audio Wrapper functions before creating the instance of it.");
		return;
	}

	const std::string postedEventID(GET_POSTED_EVENT_ID);
	
	bool mustAddNewAudioObject(false);
	{
		std::shared_lock<std::shared_mutex> sharedLock(myInstance_ptr->mySharedMutex);
		const auto iterator(myInstance_ptr->myPostedEvents.find(postedEventID));
		const bool iteratorIsValid(iterator != myInstance_ptr->myPostedEvents.end());
		const bool gameObjectIsValid(CGameLogicFunctions::IsEntityValid(aGameObjectID));

		if (iteratorIsValid)
		{
			if (aRegisteredAudioObjectID == 0)
			{
				CAudioManager::PostEventAndGetID(aEventName, iterator->second.myAudioID);
			}
			else
			{
				CAudioManager::PostEventAndGetID(aEventName, aRegisteredAudioObjectID);
			}
		}
		else if (gameObjectIsValid)
		{
			mustAddNewAudioObject = true;
		}
		else
		{
			AUDIO_LOG("[CAudioWrapper] ERROR! Failed to post audio event because both iterator and game object (entity) was invalid.");
		}
	}

	if (mustAddNewAudioObject)
	{
		const SPostedEventData postedEventData(GET_NEW_REGISTER_ID, aGameObjectID);

		if (aRegisteredAudioObjectID == 0)
		{
			CAudioManager::RegisterObject(postedEventData.myAudioID);
			CAudioManager::PostEventAndGetID(aEventName, postedEventData.myAudioID);
		}
		else
		{
			CAudioManager::PostEventAndGetID(aEventName, aRegisteredAudioObjectID);
		}

		std::unique_lock<std::shared_mutex> uniqueLock(myInstance_ptr->mySharedMutex);
		myInstance_ptr->myPostedEvents.insert(std::pair<std::string, SPostedEventData>(postedEventID, postedEventData));
	}

	//printf("Posted Event: \"%s\"     with audio ID = %i\n", gameObjectID.c_str(), audioID);
#else
	aEventName;
	aGameObjectID;
	aRegisteredAudioObjectID;
#endif
}

void CAudioWrapper::SetRTPC(const char* aRTPC_Name, const float aValue, const unsigned int aRegisteredAudioObjectID)
{
#ifndef AUDIO_ENGINE_OFF
	CAudioManager::SetRTPC(aRTPC_Name, aValue, aRegisteredAudioObjectID);
#else
	aRTPC_Name;
	aValue;
	aRegisteredAudioObjectID;
#endif
}

void CAudioWrapper::SetState(const char* aStateGroupName, const char* aStateName)
{
#ifndef AUDIO_ENGINE_OFF
	CAudioManager::SetState(aStateGroupName, aStateName);
#else
	aStateGroupName;
	aStateName;
#endif
}

void CAudioWrapper::SetListenerPositionAndOrientation(const CU::Vector3f& aPosition, const CU::Vector3f& aFront, const CU::Vector3f& aTop)
{
#ifndef AUDIO_ENGINE_OFF
	CAudioManager::SetListenerPositionAndOrientation(aPosition, aFront, aTop);
#else
	aPosition;
	aFront;
	aTop;
#endif
}

void CAudioWrapper::SetObjectPositionAndOrientation(const CU::Vector3f& aPosition, const CU::Vector3f& aFront, const CU::Vector3f& aTop,
	const char* aEventName, const UniqueIndexType& aGameObjectID)
{
#ifndef AUDIO_ENGINE_OFF
	if (myInstance_ptr == nullptr)
	{
		AUDIO_LOG("[CAudioWrapper] ERROR! Tried to use Audio Wrapper functions before creating the instance of it.");
		return;
	}

	const std::string postedEventID(GET_POSTED_EVENT_ID);

	std::shared_lock<std::shared_mutex> sharedLock(myInstance_ptr->mySharedMutex);
	const auto iterator(myInstance_ptr->myPostedEvents.find(postedEventID));

	if (iterator == myInstance_ptr->myPostedEvents.end())
	{
		AUDIO_LOG("[CAudioWrapper] ERROR! Failed to set position or orientation for audio object with ID \"%s\". Cause: Audio object couldn't be found.", postedEventID.c_str());
	}
	else
	{
		CAudioManager::SetObjectPositionAndOrientation(aPosition, aFront, aTop, iterator->second.myAudioID);
	}
#else
	aPosition;
	aFront;
	aTop;
	aGameObjectID;
#endif
}

void CAudioWrapper::SetObjectPosition(const CU::Vector3f& aPosition, const char* aAudioObjectName, const UniqueIndexType& aGameObjectID)
{
#ifndef AUDIO_ENGINE_OFF
	SetObjectPositionAndOrientation(aPosition, CU::Vector3f(0.0f, 0.0f, 1.0f), CU::Vector3f(0.0f, 1.0f, 0.0f), aAudioObjectName, aGameObjectID);
#else
	aPosition;
	aGameObjectID;
#endif
}

void CAudioWrapper::SetListenerObjectIndex(const UniqueIndexType& aGameObjectID)
{
#ifndef AUDIO_ENGINE_OFF
	if (myInstance_ptr == nullptr)
	{
		AUDIO_LOG("[CAudioWrapper] ERROR! Tried to use Audio Wrapper functions before creating the instance of it.");
		return;
	}

	myInstance_ptr->myListenerIndex = aGameObjectID;
#else
	aGameObjectID;
#endif
}
