#pragma once
#include "EntitySystemLink\EntitySystemLink.h"
#include "../Script/ScriptManager.h"
#include <unordered_set>
#include <shared_mutex>
#include "..\CommonUtilities\LevenshteinDistance.h"

#ifdef max
#pragma push_macro("max")
#undef max
#define MAX_UNDEFINED
#endif // max

class CScriptEventManager
{
	friend class CScriptEventManager_Sprite;
	friend class CScriptEventManager_EntityState;
	friend class CScriptEventManager_RotateAndScale;
	friend class CScriptEventManager_WorldInterface;
	friend class CScriptEventManager_ControllerFunctions;
	friend class CScriptEventManager_AnimationAndRender;
	friend class CScriptEventManager_AudioInterface;
	friend class CScriptEventManager_MoveObject;

public:
	CScriptEventManager(CScriptManager& aScriptManager, EManager& aEntityManager);
	~CScriptEventManager() = default;

	bool Init();

	bool Update(const float aDeltaTime);

	void SetIsActive(const bool aIsActive);

	void ClearRemovedStates();

	void RegisterScriptComponent(const CScriptManager::StateIndexType aStateIndex, const SEntityHandle& aEntityHandle);

	void KillEntity(const UniqueIndexType& aUniqueIndex);
	void KillEntity_ThreadSafe(const UniqueIndexType& aUniqueIndex);

	template<typename... Args>
	bool CallEventOnTarget(const CScriptManager::StateIndexType aStateIndex, const char* aEventName, const UniqueIndexType& aTargetEntityUniqueIndex, Args&&... aArgs);

	template<typename... Args>
	bool CallEventOnEveryone(const CScriptManager::StateIndexType aStateIndex, const char* aEventName, Args&&... aArgs);

private:
	struct SCallbackKey
	{
		using EventConstIterator = std::unordered_map<std::string, std::string>::const_iterator;

		static inline std::size_t Hash(const CScriptEventManager::SCallbackKey& aCallbackKey)
		{
			return std::hash<decltype(CScriptEventManager::SCallbackKey::myHandle.myUniqueIndex)>()(aCallbackKey.myHandle.myUniqueIndex);
		}

		inline bool operator==(const SCallbackKey& aOther) const
		{
			return (myHandle.myUniqueIndex == aOther.myHandle.myUniqueIndex);
		}

		inline bool CheckAndGetFunctionBindedToEvent(const std::string& aEventName, EventConstIterator& aIteratorToSet) const
		{
			aIteratorToSet = myEvents.find(aEventName);

			return (aIteratorToSet != myEvents.end());
		}

		inline bool CheckIfStateIndex(const CScriptManager::StateIndexType& aStateIndex) const
		{
			return (myStateIndex == aStateIndex);
		}

		inline bool CheckIfUniqueIndex(const UniqueIndexType& aUniqueIndex) const
		{
			return (myHandle.myUniqueIndex == aUniqueIndex);
		}

		mutable CScriptManager::StateIndexType myStateIndex;
		SEntityHandle myHandle;
		mutable std::unordered_map<std::string, std::string> myEvents;
	};

	using RegisteredEntityIterator = std::_List_const_iterator<std::_List_val<std::_List_simple_types<CScriptEventManager::SCallbackKey>>>;

	void KillEntityImpl(const UniqueIndexType& aUniqueIndex);

	bool InitValidCallbacks();
	bool InitFunctions();

	bool RegisterCallback(const CScriptManager::StateIndexType aStateIndex, const char* aEventName, const char* aCallbackName);

	bool UnregisterCallback(const CScriptManager::StateIndexType aStateIndex, const char* aEventName);

	template<typename... Args>
	bool CallEventOnTargetImpl(const CScriptManager::StateIndexType aStateIndex, const std::string& aEventName, const UniqueIndexType& aTargetEntityUniqueIndex, Args&&... aArgs);

	template<typename... Args>
	bool CallEventOnEveryoneImpl(const CScriptManager::StateIndexType aStateIndex, const std::string& aEventName, Args&&... aArgs);

	static bool EventCallbacksChanged(void* aThis, const char* aFileChangedName);
	bool EventCallbacksChangedImpl(const char* aFileChangedName);

	const UniqueIndexType GetEntityUniqueIndex(const CScriptManager::StateIndexType aStateIndex);

	const SEntityHandle GetEntityHandle(const CScriptManager::StateIndexType aStateIndex);

	static constexpr const char* ourEventCallbacksFilePath = "Data/Scripts/eventCallbacks.json";

	CU::GrowingArray<CScriptManager::StateIndexType> myStatesToRemove;
	std::shared_mutex myStatesToRemoveMutex;

	CScriptManager& myScriptManager;
	EManager& myEntityManager;

	std::unordered_set<std::string> myValidCallbackEntries;
	std::shared_mutex myValidCallbacksMutex;
	std::unordered_set<SCallbackKey, std::size_t(*)(const SCallbackKey&)> myEntityCallbacks;
	std::shared_mutex myCallbacksMutex;
	bool myIsActive;

};

template<typename ...Args>
inline bool CScriptEventManager::CallEventOnTarget(const CScriptManager::StateIndexType aStateIndex, const char* aEventName, const UniqueIndexType& aTargetEntityUniqueIndex, Args&& ...aArgs)
{
	std::string eventName(aEventName);

	std::shared_lock<std::shared_mutex> sharedLock(myValidCallbacksMutex);
	if (myValidCallbackEntries.find(eventName) == myValidCallbackEntries.end())
	{
		std::string closestName("NOT FOUND");
		std::string::size_type leastDistance(std::numeric_limits<std::string::size_type>::max());
		for (const auto& callbackEntry : myValidCallbackEntries)
		{
			const std::string::size_type distance(CU::LevenshteinDistance(eventName, callbackEntry));
			if (distance < leastDistance)
			{
				leastDistance = distance;
				closestName = callbackEntry;
			}
		}

		RESOURCE_LOG("[ScriptEventManager] ERROR! Failed to trigger event \"%s\", did you mean \"%s\" instead? Or forgot to add it to the events JSON file."
			, eventName.c_str(), closestName.c_str());

		return false;
	}
	sharedLock.unlock();

	return CallEventOnTargetImpl(aStateIndex, eventName, aTargetEntityUniqueIndex, std::forward<Args>(aArgs)...);
}

template<typename ...Args>
inline bool CScriptEventManager::CallEventOnEveryone(const CScriptManager::StateIndexType aStateIndex, const char* aEventName, Args&& ...aArgs)
{
	std::string eventName(aEventName);

	std::shared_lock<std::shared_mutex> sharedLock(myValidCallbacksMutex);
	if (myValidCallbackEntries.find(eventName) == myValidCallbackEntries.end())
	{
		std::string closestName("NOT FOUND");
		std::string::size_type leastDistance(std::numeric_limits<std::string::size_type>::max());
		for (const auto& callbackEntry : myValidCallbackEntries)
		{
			const std::string::size_type distance(CU::LevenshteinDistance(eventName, callbackEntry));
			if (distance < leastDistance)
			{
				leastDistance = distance;
				closestName = callbackEntry;
			}
		}

		RESOURCE_LOG("[ScriptEventManager] ERROR! Failed to trigger event \"%s\", did you mean \"%s\" instead? Or forgot to add it to the events JSON file."
			, eventName.c_str(), closestName.c_str());

		return false;
	}
	sharedLock.unlock();

	return CallEventOnEveryoneImpl(aStateIndex, eventName, std::forward<Args>(aArgs)...);
}

template<typename ...Args>
inline bool CScriptEventManager::CallEventOnTargetImpl(const CScriptManager::StateIndexType aStateIndex, const std::string& aEventName, const UniqueIndexType& aTargetEntityUniqueIndex, Args&& ...aArgs)
{
	std::shared_lock<std::shared_mutex> sharedLock(myCallbacksMutex);

	//Target Handle and if target has event
	SEntityHandle callerHandle;
	bool hasEvent(false);
	SCallbackKey::EventConstIterator eventIterator;
	const SCallbackKey* entityIterator(nullptr);

	std::find_if(myEntityCallbacks.begin(), myEntityCallbacks.end()
		, [&hasEvent, &eventIterator, &aEventName, &aTargetEntityUniqueIndex, &aStateIndex, &callerHandle, &entityIterator](const SCallbackKey& aCallbackKey) -> bool
	{
		if (aCallbackKey.CheckIfUniqueIndex(aTargetEntityUniqueIndex) == true)
		{
			hasEvent = aCallbackKey.CheckAndGetFunctionBindedToEvent(aEventName, eventIterator);
			entityIterator = &aCallbackKey;
		}
		if (aCallbackKey.CheckIfStateIndex(aStateIndex) == true)
		{
			callerHandle = aCallbackKey.myHandle;
		}

		if ((entityIterator != nullptr) && (callerHandle != EntityHandleINVALID))
		{
			return true;
		}

		return false;
	});

	const bool noStateIndex(aStateIndex == CScriptManager::InvalidStateIndex);

	if ((noStateIndex == false) && (callerHandle == EntityHandleINVALID))
	{
		RESOURCE_LOG("[ScriptEventManager] ERROR! Failed to trigger event \"%s\", caller handle not found"
			, aEventName.c_str());
		return false;
	}

	if (hasEvent == false)
	{
		return false;
	}

	if (((noStateIndex == true) || (myEntityManager.GetComponent<CompController>(callerHandle).Get().myLoaded == CController::ELoadState::Loaded))
		&& (myEntityManager.GetComponent<CompController>(entityIterator->myHandle).Get().myLoaded == CController::ELoadState::Loaded))
	{
		myScriptManager.CallFunction(entityIterator->myStateIndex
			, eventIterator->second
			, callerHandle.myUniqueIndex
			, std::forward<Args>(aArgs)...);
	}

	sharedLock.unlock();

	return true;
}

template<typename... Args>
inline bool CScriptEventManager::CallEventOnEveryoneImpl(const CScriptManager::StateIndexType aStateIndex, const std::string& aEventName, Args&&... aArgs)
{
	std::shared_lock<std::shared_mutex> sharedLock(myCallbacksMutex);
	HandleDataIndex index(HandleDataIndexINVALID);

	//If called from an entity
	if (aStateIndex != CScriptManager::InvalidStateIndex)
	{
		auto entityIterator(std::find_if(myEntityCallbacks.begin(), myEntityCallbacks.end()
			, [&aStateIndex](const SCallbackKey& aCallbackKey) -> bool
		{
			if (aCallbackKey.CheckIfStateIndex(aStateIndex) == true)
			{
				return true;
			}
			return false;
		}));

		if (entityIterator == myEntityCallbacks.end())
		{
			return false;
		}
		index = entityIterator->myHandle.myHandleDataIndex;
	}

	SCallbackKey::EventConstIterator eventIterator;
	for (const auto& callbackKey : myEntityCallbacks)
	{
		if (callbackKey.CheckAndGetFunctionBindedToEvent(aEventName, eventIterator) == true)
		{
			if (myEntityManager.GetComponent<CompController>(callbackKey.myHandle).Get().myLoaded == CController::ELoadState::Loaded)
			{
				myScriptManager.CallFunction(callbackKey.myStateIndex, eventIterator->second, index, std::forward<Args>(aArgs)...);
			}
		}
	}

	sharedLock.unlock();

	return true;
}

#ifdef MAX_UNDEFINED
#undef MAX_UNDEFINED
#pragma pop_macro("max")
#endif // MAX_UNDEFINED
