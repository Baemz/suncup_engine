#pragma once
#include "State.h"
#include "InGameConsoleCommand.h"
#include "../EngineCore/EventSystem/EventReceiver.h"

struct SInGameConsoleCommand;

class CConsoleState : private CState, public sce::CEventReceiver
{
public:
	CConsoleState(EManager& aEntityManager);
	~CConsoleState() override;

	void Init() override;
	void Update(const float aDeltaTime) override;
	void Render() override;

	void OnActivation() override;
	void OnDeactivation() override;
	void ReceiveEvent(const Event::SEvent& aEvent) override;

private:
	void ConsoleRenderCallback();

	bool CallCommand(std::string& aCommandField);

	static __forceinline const bool CompareCommands(const SInGameConsoleCommand& aCmd1, const SInGameConsoleCommand& aCmd2);

private:
	enum EState
	{
		eNothing = 0 << 0,
		eMoveSuggestionUp = 1 << 1,
		eMoveSuggestionDown = 1 << 2,
		eCmdChanged = 1 << 3
	};

	char myCommand[4096];
	std::string myLog;
	CU::GrowingArray<SInGameConsoleCommand, unsigned short> myCommandList;
	static CU::GrowingArray<SInGameConsoleCommand, unsigned short> myExecutedCommandList;
	static CU::GrowingArray<SInGameConsoleCommand, unsigned short> mySimilarCommandList;
	volatile EState myConsoleState;

};
