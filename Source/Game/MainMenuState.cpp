#include "stdafx.h"
#include "MainMenuState.h"
#include "StateStack.h"
#include "InGameState.h"
#include "OptionMenuState.h"
#include "ControlMenuState.h"

#define MAX_FADE_TIME (1.0f)

CMainMenuState::CMainMenuState(EManager& aEntityManager)
	: CState(aEntityManager)
	, myCurrFadeTime(0.0f)
	, myIsFadingIn(false)
	, myShouldFade(false)
	, myPressedStart(false)
	, myPressedCredits(false)
{
	myMenu.Disable();
}

CMainMenuState::~CMainMenuState()
{
}

void CMainMenuState::Init()
{

	myFadeSprite.Init("Data/Misc/background.dds");
	myFadeSprite.SetPosition({ 0.5f });
	myFadeSprite.SetScale({ 1000.0f });
	myFadeSprite.SetPivot({ 0.5f });
	myFadeSprite.SetAlpha(1.0f);

	myCurrFadeTime = MAX_FADE_TIME;
	myIsFadingIn = true;
	myShouldFade = true;
	myPressedStart = false;
	myPressedCredits = false;

	
}

void CMainMenuState::Update(const float aDeltaTime)
{
	aDeltaTime;
	CStateStack::PushMainStateOnTop(sce_new(CInGameState(myEntityManager)));
}

void CMainMenuState::Render()
{
}

void CMainMenuState::OnActivation()
{
	
}

void CMainMenuState::OnDeactivation()
{
}

/* PRIVATE FUNCTIONS */

void CMainMenuState::UpdateFadeAndStartGameOnCommand(const float aDeltaTime)
{
	myCurrFadeTime -= aDeltaTime;
	if (myCurrFadeTime <= 0.0f)
	{
		myCurrFadeTime = 0.0f;

		if (myPressedStart)
		{
			myMenu.Disable();
			// Place next state here
		}
		else if (myPressedCredits)
		{
			myMenu.Disable();
			// Place next state here
		}
	}

	if (myIsFadingIn) myFadeSprite.SetAlpha(myCurrFadeTime / MAX_FADE_TIME);
	else myFadeSprite.SetAlpha(1.0f - (myCurrFadeTime / MAX_FADE_TIME));
}
