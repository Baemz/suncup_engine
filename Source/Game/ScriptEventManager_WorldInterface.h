#pragma once

class CScriptEventManager;

class CScriptEventManager_WorldInterface
{
public:
	static bool InitFunctions(CScriptEventManager& aScriptEventManager);

private:
	CScriptEventManager_WorldInterface() = delete;
	~CScriptEventManager_WorldInterface() = delete;

};
