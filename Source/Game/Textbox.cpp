#include "stdafx.h"
#include "Textbox.h"
#include "../GraphicsEngine/Sprite/Sprite.h"

//#include "EntityManager.h"
//#include "GameObject.h"

#define TIME_TO_OPEN 0.5f

#include "../EngineCore/JSON/json.hpp"

void LoadTextBox(nlohmann::json& aJson, const char* aTextEvent, STextContent& aContent)
{
	try
	{
		for (auto& boxIterator : aJson[aTextEvent])
		{
			STextContent::SBox box;
			for (auto& textIterator : boxIterator)
			{
				std::string line = textIterator.get<std::string>();
				box.myText += line;
				box.myText += "\n";
				if (line[0] == '*')
				{
					//box.myColor = { 1.0f, 0.42f, 0.0f };
					//Make line specific!
					box.myColor = { 0.0f, 1.0f, 0.918f };
				}
				else
				{
					box.myColor = { 0.0f, 1.0f, 0.918f };
				}
				//box.myLines.push(textIterator.get<std::string>());
				//box.myTimer += 0.5f;
				//std::cout << std::setw(4) << textIterator << std::endl;
			}

			aContent.myBoxes.push(box);
		}
	}
	catch (...)
	{
		//assert(false && "ERROR: Wrong values in player data variable %s.", aVariableName);
		GAME_LOG("ERROR: Wrong values in Textbox text event [%s].", aTextEvent);
	}
}

CTextBox::CTextBox()
	: myStateTimer(0.0f)
	//, myClosedPosition({ 0.66f, 1.0f })
	//, myOpenedPosition({ 0.66f, 0.625f })
	, myClosedPosition({ 0.33125f, -0.5f })
	, myOpenedPosition({ 0.33125f, -0.025f })
	, myTextOffset({ 0.06f, 0.1325f })
	, myTimeBetweenCharacters(0.025f)
	, myCurrentString("")
	, myCharacterIndex(0)
	, myCharacterTimer(0.0f)
	, myPopDownBoxTimer(0.0f)
{
}

CTextBox::~CTextBox()
{
	//UnloadAudio();
}

void CTextBox::Init(const char * aSpritePath, const char* aTextFilePath)
{
	mySprite.Init(aSpritePath);
	mySprite.SetScale({ 0.75f });
	mySprite.SetPosition(myClosedPosition);
	//mySprite.SetPosition({ 0.5f });

	myText.Init("Data/Fonts/Adventure Subtitles.ttf", "", 16.0f, mySprite.GetPixelSize() * 0.7f, { 0.0f }, sce::gfx::CText::ColorFloatToUint({ 0.0f, 1.0f, 0.918f }));
// 
// 	LoadAudio();

	bool loadedText(LoadText(aTextFilePath));
	loadedText;
	assert(loadedText && "Could not load text!");
}

void CTextBox::Update(const float aDeltaTime)
{
	//unsigned int playerID = EntityManager::Get()->GetPlayerID();
	//if (EntityManager::Get()->ObjectExists(playerID))
	//{
	//	CU::Vector3f playerPosition = EntityManager::Get()->GetGameObjectWithID(playerID).GetPosition();
	//	AI::SetAudioPosition(mySounds.myOpenBox, playerPosition);
	//	AI::SetAudioPosition(mySounds.myCloseBox, playerPosition);
	//	AI::SetAudioPosition(mySounds.myScrollText, playerPosition);
	//}

	if (!myQueuedTextContents.empty() && myPopDownBoxTimer <= 0.0f)
	{
		if (myStateTimer < TIME_TO_OPEN)
		{
			if (myStateTimer < 0.1f)
			{
				//AI::PlayAudio(mySounds.myOpenBox);
			}

			myStateTimer = (myStateTimer + aDeltaTime < TIME_TO_OPEN) ? myStateTimer + aDeltaTime : TIME_TO_OPEN;

			mySprite.SetPosition(
			{
				myOpenedPosition.x,
				CU::SmootherStep(myClosedPosition.y, myOpenedPosition.y, myStateTimer / TIME_TO_OPEN)
			});

			myText.SetPosition(
			{
				myOpenedPosition.x + myTextOffset.x,
				CU::SmoothStep(myClosedPosition.y + myTextOffset.y, myOpenedPosition.y + myTextOffset.y, myStateTimer / TIME_TO_OPEN)
			});
		}
		else
		{
			STextContent::SBox& currentBox(myQueuedTextContents.front().myBoxes.front());
			myText.SetColor(sce::gfx::CText::ColorFloatToUint(currentBox.myColor));

			if (currentBox.myTimer > 0.0f)
			{
				//if (currentBox.myLines.size() == 0)
				if (myCharacterIndex == currentBox.myText.size() - 1)
				{
					currentBox.myTimer -= aDeltaTime;
				}
				else
				{
					if (myCharacterTimer <= 0)
					{
						//myCurrentString += currentBox.myLines.front()[myCharacterIndex];
						myCurrentString += currentBox.myText[myCharacterIndex];
						myCharacterTimer = myTimeBetweenCharacters;
						myCharacterIndex++;
						//AI::PlayAudio(mySounds.myScrollText);

						//if (myCharacterIndex >= currentBox.myLines.front().size())
						//{
						//	myCurrentString += "\n";
						//	currentBox.myLines.pop();
						//	myCharacterIndex = 0;
						//}
					}
					else
					{
						myCharacterTimer -= aDeltaTime;
					}

					//GAME_LOG("Width: %f", myText.GetWidth());
					short lastLineIndex(  (short)myCurrentString.find_last_of('\n') );
					if (lastLineIndex != std::string::npos)
					{
						std::string lastLine = myCurrentString.substr(myCurrentString.find_last_of('\n'));
						myText.SetText(lastLine);
					}
					
					//if (myText.GetWidth() > mySprite.GetScreenSpaceSize().x * 0.9f)
					//{
					//	myCurrentString += "\n";
					//	currentBox.myTimer += 1.0f;
					//}

					myText.SetText(myCurrentString);
				}
			}
			else
			{
				myQueuedTextContents.front().myBoxes.pop();
				myCurrentString = "";
				myText.SetText(myCurrentString);
				myCharacterIndex = 0;
				myCharacterTimer = 0.0f;
				if (myQueuedTextContents.front().myBoxes.size() == 0)
				{
					myQueuedTextContents.pop();
					if (!myQueuedTextContents.empty())
					{
						myPopDownBoxTimer = TIME_TO_OPEN * 2.0f;
// 						AI::PlayAudio(mySounds.myCloseBox);
					}
				}
			}
		}
	}
	else
	{
		if (myPopDownBoxTimer > 0.0f)
		{
			myPopDownBoxTimer -= aDeltaTime;
			const float percent = 1.0f - (myPopDownBoxTimer / (TIME_TO_OPEN * 2.0f));
			const float interpolation = -(4 * percent * percent) + 4 * percent;
			if (std::abs(interpolation - 0.5f) < 0.1f)
			{
// 				AI::PlayAudio(mySounds.myOpenBox);
			}
			mySprite.SetPosition(
			{
				myOpenedPosition.x,
				CU::SmootherStep(myOpenedPosition.y, myClosedPosition.y, interpolation)
			});
		}
		else if (myStateTimer > 0)
		{
			myStateTimer = (myStateTimer - aDeltaTime > 0) ? myStateTimer - aDeltaTime : 0;

			mySprite.SetPosition(
			{
				myOpenedPosition.x,
				CU::SmootherStep(myOpenedPosition.y, myClosedPosition.y, 1.0f - (myStateTimer / TIME_TO_OPEN))
			});

			myText.SetPosition(
			{
				myOpenedPosition.x + myTextOffset.x,
				CU::SmoothStep(myOpenedPosition.y + myTextOffset.y, myClosedPosition.y + myTextOffset.y, 1.0f - myStateTimer / TIME_TO_OPEN)
			});
		}
	}
}

void CTextBox::Close()
{
	//AI::PlayAudio(mySounds.myCloseBox);
	while (!myQueuedTextContents.empty())
	{
		myQueuedTextContents.pop();
	}
}

void CTextBox::Render()
{
	mySprite.Render();
	myText.Render();
}

bool CTextBox::LoadText(const char* aTextFilePath)
{
	std::ifstream textDataFile(aTextFilePath);

	if (textDataFile.bad())
	{
		return false;
	}

	nlohmann::json fileReader;
	try
	{
		textDataFile >> fileReader;
		textDataFile.close();
	}
	catch (...)
	{
		GAME_LOG("ERROR: Wrong values in text data file %s.", aTextFilePath);
		return false;
	}

	return true;
}

// void CTextBox::LoadAudio()
// {
// 	AI::LoadAudioAndGetID("Audio/Misc/textboxOpen.ogg", mySounds.myOpenBox);
// 	AI::LoadAudioAndGetID("Audio/Misc/textboxClose.ogg", mySounds.myCloseBox);
// 	AI::LoadAudioAndGetID("Audio/Misc/textboxScroll.ogg", mySounds.myScrollText);
// 	AI::SetAudioVolume(mySounds.myScrollText, 0.75f);
// }
// 
// void CTextBox::UnloadAudio()
// {
// 	AI::UnloadAudio(mySounds.myOpenBox);
// 	AI::UnloadAudio(mySounds.myCloseBox);
// 	AI::UnloadAudio(mySounds.myScrollText);
// }
