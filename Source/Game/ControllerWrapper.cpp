#include "stdafx.h"
#include "ControllerWrapper.h"
#include "ControllerIncludes.h"

CControllerWrapper::CControllerWrapper()
	: myController(nullptr)
	, myControllerType(EController::NONE)
{
}

CControllerWrapper::CControllerWrapper(const SControllerWrapperInit& aInitData)
	: myControllerType(aInitData.myControllerType)
{
	sce_delete(myController);

	switch (myControllerType)
	{
	case EController::Input:
		myController = sce_new(CInputController(aInitData.myControllerBaseInitData, aInitData.myInputInitData));
		break;
	case EController::AI:
		myController = sce_new(CAIController(aInitData.myControllerBaseInitData, aInitData.myAIInitData));
		break;
	case EController::Script:
		myController = sce_new(CScriptController(aInitData.myControllerBaseInitData, aInitData.myScriptInitData));
		break;
	default:
		myController = nullptr;
	}
}

CControllerWrapper::~CControllerWrapper()
{
	sce_delete(myController);
}

CController& CControllerWrapper::Get()
{
	assert("Controller is not initiated." && myController != nullptr);
	assert("Tried to get controller that has no type." && myControllerType != EController::NONE);
	return *myController;
}

const CController& CControllerWrapper::Get() const
{
	assert("Controller is not initiated." && myController != nullptr);
	assert("Tried to get controller that has no type." && myControllerType != EController::NONE);
	return *myController;
}

CInputController& CControllerWrapper::GetInput()
{
	assert("Controller is not initiated." && myController != nullptr);
	assert("Tried to get controller of the wrong type." && myControllerType == EController::Input);
	return *reinterpret_cast<CInputController*>(myController);
}

const CInputController& CControllerWrapper::GetInput() const
{
	assert("Controller is not initiated." && myController != nullptr);
	assert("Tried to get controller of the wrong type." && myControllerType == EController::Input);
	return *reinterpret_cast<CInputController*>(myController);
}

CAIController& CControllerWrapper::GetAI()
{
	assert("Controller is not initiated." && myController != nullptr);
	assert("Tried to get controller of the wrong type." && myControllerType == EController::AI);
	return *reinterpret_cast<CAIController*>(myController);
}

const CAIController& CControllerWrapper::GetAI() const
{
	assert("Controller is not initiated." && myController != nullptr);
	assert("Tried to get controller of the wrong type." && myControllerType == EController::AI);
	return *reinterpret_cast<CAIController*>(myController);
}

CScriptController& CControllerWrapper::GetScript()
{
	assert("Controller is not initiated." && myController != nullptr);
	assert("Tried to get controller of the wrong type." && myControllerType == EController::Script);
	return *reinterpret_cast<CScriptController*>(myController);
}

const CScriptController& CControllerWrapper::GetScript() const
{
	assert("Controller is not initiated." && myController != nullptr);
	assert("Tried to get controller of the wrong type." && myControllerType == EController::Script);
	return *reinterpret_cast<CScriptController*>(myController);
}
