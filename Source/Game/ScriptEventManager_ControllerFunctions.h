#pragma once

class CScriptEventManager;

class CScriptEventManager_ControllerFunctions
{
public:
	static bool InitFunctions(CScriptEventManager& aScriptEventManager);

private:
	CScriptEventManager_ControllerFunctions() = delete;
	~CScriptEventManager_ControllerFunctions() = delete;

};

