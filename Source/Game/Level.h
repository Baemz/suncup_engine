#pragma once
#include "../GraphicsEngine/Lights/EnvironmentalLight.h"
#include "NavigationMesh/NavigationMesh.h"
#include "QuadTreeNode.h"
#include "Grid.h"
#include "../GraphicsEngine/Model/ModelInstance.h"

class CMapData;
class CMapDataObject;

class CLevel
{
	template<typename Variable, typename DataPair>
	friend void LoadScriptVariable(CLevel& aLevel, const class CMapDataObject& aDataObject, const char* aScriptPath, Variable*& aVariablesToSet, const DataPair* aVariablesData, const std::size_t& aAmountOfVariables, const Variable& aDefaultValue);

public:
	enum class EObjectType : unsigned char
	{
		None,
		Trigger,
		NoTrigger,
		All,
	};

	CLevel();
	~CLevel();

	bool Init(const char* aLevelPath);
	void Destroy();

	void QueueLevelSwitch(const std::string& aLevelPath);
	bool WantToSwitchLevel(std::string& aLevelPathIfSwitching);

	bool Update(const float aDeltaTime);
	bool PostUpdate();

	void QueueAddToActiveObjects(const SGridData& aGridData);
	void QueueRemoveFromActiveObjects(const UniqueIndexType& aEntityUniqueIndex);
	void QueueUpdateActiveObject(const UniqueIndexType& aEntityUniqueIndex, const CU::Vector3f& aNewPosition);
	void QueueSetOnLeaveGridCallback(const UniqueIndexType& aEntityUniqueIndex, const std::function<void()>& aOnLeaveGridCallback);

	inline void ToggleShouldRenderQuadTreeNodes() { myStaticQuadTree.ToggleShouldRenderNodes(); }
	inline void ToggleShouldRenderGrid() { myDynamicGrid.ToggleShouldRenderGrid(); }

	const char* GetCurrentLevelName() const;
	double GetTimeSpentOnLevel() const;

	inline const bool FailedToLoadLevel() const { return !myMapLoaded; }
	inline const bool IsLoading() const { return myIsLoading; }

	inline CNavigationMesh& GetNavMesh() { return myNavMesh; }
	inline const CU::Vector4f& GetGridAABB() const { return myDynamicGrid.GetAABB(); }

	bool GetStaticObjectsInside(const CU::Vector4f& aAABB, const EObjectType& aStaticObjectTypeToGet, CU::GrowingArray<UniqueIndexType>& aArrayToFill);
	bool GetStaticObjectsNear(const CU::Vector2f& aPoint, const EObjectType& aStaticObjectTypeToGet, CU::GrowingArray<UniqueIndexType>& aArrayToFill);

	bool GetDynamicObjectsInside(const CU::Vector4f& aAABB, const EObjectType& aDynamicObjectTypeToGet, CU::GrowingArray<UniqueIndexType>& aArrayToFill);
	bool GetDynamicObjectsNear(const CU::Vector2f& aPoint, const EObjectType& aDynamicObjectTypeToGet, CU::GrowingArray<UniqueIndexType>& aArrayToFill);

private:
	struct SLevelInitFunction
	{
		SLevelInitFunction()
			: myIndicesVariables(nullptr)
			, myNumberVariables(nullptr)
			, myStringVariables(nullptr)
			, myAmountOfIndicesVariablesAdded(0)
			, myAmountOfNumberVariablesAdded(0)
			, myAmountOfStringVariablesAdded(0)
		{}

		std::function<bool(const UniqueIndexType*, const std::size_t&, const int*, const std::size_t&, const std::string*, const std::size_t&)> myOnInitFunction;
		UniqueIndexType* myIndicesVariables;
		int* myNumberVariables;
		std::string* myStringVariables;
		std::size_t myAmountOfIndicesVariablesAdded;
		std::size_t myAmountOfNumberVariablesAdded;
		std::size_t myAmountOfStringVariablesAdded;
	};

	void LoadLevel(CMapData* aMapData);
	void LoadLevelPreCleanup();

	void LoadGameObject(const void* aCurrentDataObject, void* aUnityToEntityBindMap, void* aModelFilesInFolder, void* aParticleFilesInFolder);

	bool GetObjectsImpl(const CU::GrowingArray<QuadTreeData>& aQuadTreeDatas, const EObjectType& aStaticObjectTypeToGet, CU::GrowingArray<UniqueIndexType>& aArrayToFill);
	bool GetObjectsImpl(const CU::GrowingArray<SGridData>& aQuadTreeDatas, const EObjectType& aStaticObjectTypeToGet, CU::GrowingArray<UniqueIndexType>& aArrayToFill);

	std::string myCurrentLevelName;
	double myTimeSpentOnLevel;
	volatile bool myIsLoading;
	volatile bool myMapLoaded;

	std::string myLevelToSwitchTo;
	bool myIWantToSwitchLevel;

	CNavigationMesh myNavMesh;
	QuadTreeNode myStaticQuadTree;
	CU::GrowingArray<UniqueIndexType> myStaticUniqueIndicesNotCullable;
	CGrid myDynamicGrid;

	CU::GrowingArray<SLevelInitFunction, int> myScriptInitsToBeInitiated;

	std::unordered_set<SGridData, std::size_t(*)(const SGridData&)> myQueuedGridAdd;
	std::mutex myQueuedGridAddMutex;
	std::unordered_set<UniqueIndexType> myQueuedGridRemove;
	std::mutex myQueuedGridRemoveMutex;
	std::unordered_map<UniqueIndexType, CU::Vector2f> myQueuedGridUpdate;
	std::mutex myQueuedGridUpdateMutex;
	std::unordered_map<UniqueIndexType, std::function<void()>> myQueuedOnGridLeaveCallback;
	std::mutex myQueuedOnGridLeaveCallbackMutex;

	// NOTE: Load from level?
	sce::gfx::CEnvironmentalLight myEnvLight;
	sce::gfx::CModelInstance mySkybox;

};
