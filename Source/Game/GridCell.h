#pragma once
#include "..\CommonUtilities\Vector2.h"
#include "..\EntitySystem\Handle.h"
#include <unordered_set>

struct SGridData
{
	SGridData() : myEntityUniqueIndex(EntityHandleINVALID.myUniqueIndex), myIsTrigger(false), myOnOutsideOfGridCallback(nullptr) {};

	static inline std::size_t Hash(const SGridData& aGridData)
	{
		return std::hash<decltype(SGridData::myEntityUniqueIndex)>()(aGridData.myEntityUniqueIndex);
	}

	inline bool operator==(const SGridData& aOther) const
	{
		return (myEntityUniqueIndex == aOther.myEntityUniqueIndex);
	}

	// For Grid
	CU::Vector2f myCenterPosition;
	CU::Vector2f mySize;

	// User data
	UniqueIndexType myEntityUniqueIndex;
	mutable std::function<void()> myOnOutsideOfGridCallback;
	bool myIsTrigger;
};

struct SGridCell
{
	SGridCell() : myEntities(0, SGridData::Hash) {};
	SGridCell(const CU::Vector2f& aPosition) : myPosition(aPosition), myEntities(0, SGridData::Hash) {};
	~SGridCell() = default;

	CU::Vector2f myPosition;

	std::unordered_set<SGridData, std::size_t(*)(const SGridData&)> myEntities;

};
