#include "stdafx.h"
#include "ScriptEventManager.h"
#include "..\EngineCore\JSON\json.hpp"
#include "..\EngineCore\FileWatcher\FileWatcherWrapper.h"
#include "GameSystems\ExposedFunctions\GameLogicFunctions.h"
#include "NavigationMesh\NavigationMesh.h"
#include "..\CommonUtilities\Random.h"
#include "AudioWrapper.h"
#include "ScriptEventManager_Sprite.h"
#include "States\ScriptEventManager_EntityState.h"
#include "ScriptEventManager_RotateAndScale.h"
#include "ScriptEventManager_WorldInterface.h"
#include "ScriptEventManager_ControllerFunctions.h"
#include "ScriptEventManager_AnimationAndRender.h"
#include "ScriptEventManager_AudioInterface.h"
#include "ScriptEventManager_MoveObject.h"

using json = nlohmann::json;

CScriptEventManager::CScriptEventManager(CScriptManager& aScriptManager, EManager& aEntityManager)
	: myEntityCallbacks(0, SCallbackKey::Hash)
	, myScriptManager(aScriptManager)
	, myEntityManager(aEntityManager)
	, myIsActive(false)
{
	myStatesToRemove.Reserve(8);
}

bool CScriptEventManager::Init()
{
	if (InitFunctions() == false)
	{
		GAME_LOG("ERROR! Failed to register script functions");
		return false;
	}

	if (sce::CFileWatcherWrapper::AddFileToWatch(this, ourEventCallbacksFilePath, &CScriptEventManager::EventCallbacksChanged, true) == false)
	{
		RESOURCE_LOG("[ScriptEventManager] ERROR! Event callbacks file \"%s\" doesn't exist, please supply it to have any callbacks in scripts working"
			, ourEventCallbacksFilePath);
	}

	if (InitValidCallbacks() == false)
	{
		GAME_LOG("ERROR! Failed to get valid callbacks");
		return false;
	}

	return true;
}

bool CScriptEventManager::Update(const float aDeltaTime)
{
	if (myIsActive)
	{
		if (CallEventOnEveryoneImpl(CScriptManager::InvalidStateIndex, "Update", aDeltaTime) == false)
		{
			return false;
		}
	}

	return true;
}

void CScriptEventManager::SetIsActive(const bool aIsActive)
{
	myIsActive = aIsActive;
}

void CScriptEventManager::ClearRemovedStates()
{
	myStatesToRemove.RemoveAll();
}

void CScriptEventManager::RegisterScriptComponent(const CScriptManager::StateIndexType aStateIndex, const SEntityHandle& aEntityHandle)
{
	SCallbackKey callbackKey;
	callbackKey.myStateIndex = aStateIndex;
	callbackKey.myHandle = aEntityHandle;

	std::unique_lock<std::shared_mutex> uniqueLock(myCallbacksMutex);
	auto entityInsertPair(myEntityCallbacks.emplace(callbackKey));
	uniqueLock.unlock();
}

void CScriptEventManager::KillEntity(const UniqueIndexType& aUniqueIndex)
{
	KillEntityImpl(aUniqueIndex);

	CGameLogicFunctions::KillEntity(aUniqueIndex);
}

void CScriptEventManager::KillEntity_ThreadSafe(const UniqueIndexType& aUniqueIndex)
{
	KillEntityImpl(aUniqueIndex);

	CGameLogicFunctions::KillEntity_ThreadSafe(aUniqueIndex);
}

void CScriptEventManager::KillEntityImpl(const UniqueIndexType & aUniqueIndex)
{
	std::unique_lock<std::shared_mutex> uniqueCallbacksLock(myCallbacksMutex, std::defer_lock);
	std::unique_lock<std::shared_mutex> uniqueRemovedLock(myStatesToRemoveMutex, std::defer_lock);
	std::lock(uniqueCallbacksLock, uniqueRemovedLock);

	auto entityIterator(std::find_if(myEntityCallbacks.begin(), myEntityCallbacks.end()
		, [&aUniqueIndex](const SCallbackKey& aCallbackKey)
	{
		return aCallbackKey.CheckIfUniqueIndex(aUniqueIndex);
	}));

	if (entityIterator != myEntityCallbacks.end())
	{
		CScriptManager::Get()->QueueRemoveState(entityIterator->myStateIndex);
		myStatesToRemove.Add(entityIterator->myStateIndex);
		myEntityCallbacks.erase(entityIterator);
	}
}

bool CScriptEventManager::InitValidCallbacks()
{
	std::ifstream eventsFile(ourEventCallbacksFilePath);
	if (eventsFile.good() == false)
	{
		GAME_LOG("ERROR! Failed to read script callbacks json file");
		return false;
	}

	json eventsJSON;
	try
	{
		eventsJSON << eventsFile;
	}
	catch (const std::exception&)
	{
		GAME_LOG("ERROR! Invalid json file script callbacks");
	}
	eventsFile.close();

	myValidCallbackEntries.emplace("Update");

	for (const std::string& currentEvent : eventsJSON["EVENTS_AVAILABLE"])
	{
		myValidCallbackEntries.emplace(currentEvent);
	}

	return true;
}

#define CHECK_IF_STATE_IS_REMOVED(aStateIndex, aReturnValue)											\
std::shared_lock<std::shared_mutex> sharedLock(myStatesToRemoveMutex);									\
if (myStatesToRemove.Find(aStateIndex) != CU::GrowingArray<CScriptManager::StateIndexType>::FoundNone)	\
return aReturnValue;

bool CScriptEventManager::InitFunctions()
{
#pragma region REGISTER_FUNCTIONS
	//--------------------REGISTER FUNCTIONS------------------------
	std::function<bool(const CScriptManager::StateIndexType, const char*, const char*)> registerCallbackBind(
		[this](const CScriptManager::StateIndexType aStateIndex, const char* aEventName, const char* aCallbackName) -> bool
	{
		CHECK_IF_STATE_IS_REMOVED(aStateIndex, false);
		return RegisterCallback(aStateIndex, aEventName, aCallbackName);
	});
	myScriptManager.RegisterFunctionCallWithStateIndex("RegisterCallback", registerCallbackBind
		, "String, String"
		, "Register this entity for an [Event] to call [Function] when the event occurs.");

	std::function<bool(const CScriptManager::StateIndexType, const char*)> unregisterCallbackBind(
		[this](const CScriptManager::StateIndexType aStateIndex, const char* aEventName) -> bool
	{
		CHECK_IF_STATE_IS_REMOVED(aStateIndex, false);
		return UnregisterCallback(aStateIndex, aEventName);
	});
	myScriptManager.RegisterFunctionCallWithStateIndex("UnregisterCallback", unregisterCallbackBind
		, "String, Number"
		, "Unregister this entity for [Event].");

#pragma endregion

#pragma region CALL_EVENT_FUNCTIONS
	//--------------------CALL EVENT FUNCTIONS------------------------
	std::function<bool(const CScriptManager::StateIndexType, const char*)> callEventEveryoneBind(
		[this](const CScriptManager::StateIndexType aStateIndex, const char* aEventName) -> bool
	{
		CHECK_IF_STATE_IS_REMOVED(aStateIndex, false);
		return CallEventOnEveryone(aStateIndex, aEventName);
	});
	myScriptManager.RegisterFunctionCallWithStateIndex("CallEventOnEveryone", callEventEveryoneBind
		, "String, Number"
		, "Call [Event] on all entities registered to this event.");

	std::function<bool(const CScriptManager::StateIndexType, const char*, const UniqueIndexType)> callEventTargetedBind(
		[this](const CScriptManager::StateIndexType aStateIndex, const char* aEventName, const UniqueIndexType aTargetEntityIndex) -> bool
	{
		CHECK_IF_STATE_IS_REMOVED(aStateIndex, false);
		return CallEventOnTarget(aStateIndex, aEventName, aTargetEntityIndex);
	});
	myScriptManager.RegisterFunctionCallWithStateIndex("CallEventOnTarget", callEventTargetedBind
		, "String, Number"
		, "Call [Event] on all entities registered to this event. [TargetEntityIndex] as the entity receiving the event.");

	std::function<bool(const CScriptManager::StateIndexType, const char*, const UniqueIndexType, float)> callEventTargetedNumberBind(
		[this](const CScriptManager::StateIndexType aStateIndex, const char* aEventName, const UniqueIndexType aTargetEntityIndex, float aNumberArg) -> bool
	{
		CHECK_IF_STATE_IS_REMOVED(aStateIndex, false);
		return CallEventOnTarget(aStateIndex, aEventName, aTargetEntityIndex, aNumberArg);
	});
	myScriptManager.RegisterFunctionCallWithStateIndex("CallEventOnTargetWithNumber", callEventTargetedNumberBind
		, "String, Number, Number"
		, "Call [Event] on all entities registered to this event. [TargetEntityIndex] as the entity receiving the event. Pass [NumberArg] as extra argument");

	std::function<bool(const CScriptManager::StateIndexType, const char*, const UniqueIndexType, const char*)> callEventTargetedStringBind(
		[this](const CScriptManager::StateIndexType aStateIndex, const char* aEventName, const UniqueIndexType aTargetEntityIndex, const char* aStringArg) -> bool
	{
		CHECK_IF_STATE_IS_REMOVED(aStateIndex, false);
		return CallEventOnTarget(aStateIndex, aEventName, aTargetEntityIndex, aStringArg);
	});
	myScriptManager.RegisterFunctionCallWithStateIndex("CallEventOnTargetWithString", callEventTargetedStringBind
		, "String, Number, String"
		, "Call [Event] on all entities registered to this event. [TargetEntityIndex] as the entity receiving the event. Pass [StringArg] as extra argument");

		std::function<bool(const CScriptManager::StateIndexType, const char*, const UniqueIndexType, bool)> callEventTargetedBoolBind(
		[this](const CScriptManager::StateIndexType aStateIndex, const char* aEventName, const UniqueIndexType aTargetEntityIndex, bool aBoolArg) -> bool
	{
		CHECK_IF_STATE_IS_REMOVED(aStateIndex, false);
		return CallEventOnTarget(aStateIndex, aEventName, aTargetEntityIndex, aBoolArg);
	});
	myScriptManager.RegisterFunctionCallWithStateIndex("CallEventOnTargetWithBool", callEventTargetedBoolBind
		, "String, Number, Bool"
		, "Call [Event] on all entities registered to this event. [TargetEntityIndex] as the entity receiving the event. Pass [BoolArg] as extra argument");

#pragma endregion

	CScriptEventManager_MoveObject::InitFunctions(*this);

	CScriptEventManager_RotateAndScale::InitFunctions(*this);

#pragma region SPAWN_OBJECT_FUNCTIONS
	//--------------------SPAWN OBJECTS--------------------------
	std::function<void(const char*, UniqueIndexType)> spawnParticleOnEntityBind(
		[this](const char* aParticleName, UniqueIndexType aEntityUniqueIndex)
	{
		CGameLogicFunctions::SpawnParticleOnEntity(aParticleName, aEntityUniqueIndex);
	});

	myScriptManager.RegisterFunction("SpawnParticleOnEntity", spawnParticleOnEntityBind,
		"String, Number",
		"Spawns a particle with the [particle name] on the [entity].");

	std::function<void(const char*, float, float, float)> spawnParticleOnPositionBind(
		[this](const char* aParticleName, float aX, float aY, float aZ)
	{
		CGameLogicFunctions::SpawnParticleOnPosition(aParticleName, CU::Vector3f(aX, aY, aZ));
	});

	myScriptManager.RegisterFunction("SpawnParticleOnPosition", spawnParticleOnPositionBind,
		"String, Number, Number, Number",
		"Spawns a particle with the [particle name] on the [entity].");

	std::function<void(UniqueIndexType, const char*)> spawnScriptObjectOnEntityBind(
		[this](UniqueIndexType aEntityUniqueIndex, const char* aScriptName)
	{
		CGameLogicFunctions::SpawnObjectOnEntity(aEntityUniqueIndex, aScriptName);
	});

	myScriptManager.RegisterFunction("SpawnScriptObjectOnEntity", spawnScriptObjectOnEntityBind,
		"Number, String",
		"Spawns a script-controlled entity on the [entity] with the [script name]");

	std::function<const UniqueIndexType(UniqueIndexType, const char*, const char*, float aX, float aY, float aZ)> spawnModelOnPositionBind(
		[this](UniqueIndexType aEntityUniqueIndex, const char* aModelPath, const char* aMaterialName, float aX, float aY, float aZ) -> const UniqueIndexType
	{
		return CGameLogicFunctions::SpawnModelAtPosition(aEntityUniqueIndex, aModelPath, aMaterialName, CU::Vector3f(aX, aY, aZ));
	});

	myScriptManager.RegisterFunction("SpawnModelOnPosition", spawnModelOnPositionBind,
		"Number, String, String, Float, Float, Float",
		"Spawns an entity on the [position] with the [modelName] using [materialName]");

#pragma endregion

	CScriptEventManager_EntityState::InitFunctions(*this);

	CScriptEventManager_ControllerFunctions::InitFunctions(*this);

	CScriptEventManager_Sprite::InitFunctions(*this);

	CScriptEventManager_AnimationAndRender::InitFunctions(*this);

#pragma region MATH_UTILITIES_FUNCTIONS
	//--------------------MATH UTILITIES FUNCTIONS------------------------
	std::function<float(float, float)> getRandomFloatBind(
		[this](float aMin, float aMax) -> float
	{
		return CU::GetRandomInRange(aMin, aMax);
	});
	myScriptManager.RegisterFunction("GetRandomFloat", getRandomFloatBind
		, "Number, Number"
		, "Retrieves a floating-point number between [min] and [max]");

	std::function<int(int, int)> getRandomIntegerBind(
		[this](int aMin, int aMax) -> int
	{
		return CU::GetRandomInRange(aMin, aMax);
	});
	myScriptManager.RegisterFunction("GetRandomInteger", getRandomIntegerBind
		, "Number, Number"
		, "Retrieves a integer number between [min] and [max]");

	std::function<float(UniqueIndexType, UniqueIndexType)> getDistance2Bind(
		[this](UniqueIndexType aFirstEntity, UniqueIndexType aSecondEntity) -> float
	{
		return CGameLogicFunctions::GetDistanceBetweenEntitiesSquared(aFirstEntity, aSecondEntity);
	});

	myScriptManager.RegisterFunction("GetDistanceBetweenEntitiesSquared", getDistance2Bind,
		"Number, Number",
		"Returns the distance between [entity] and [entity], raised to the power of 2.");

	std::function<float(UniqueIndexType, float, float, float)> getDistance2positionBind(
		[this](UniqueIndexType aFirstEntity, float aX, float aY, float aZ) -> float
	{
		return CGameLogicFunctions::GetDistanceFromEntityToPositionSquared(aFirstEntity, CU::Vector3f(aX, aY, aZ));

	});
	myScriptManager.RegisterFunction("GetDistanceFromEntityToPositionSquared", getDistance2positionBind,
		"Number, Number, Number, Number",
		"Returns the distance between [entity] and the position [X], [Y], [Z], raised to the power of 2.");

	std::function<CU::Vector3f(float, float, float, float, float, float)> getDirectionFromPosToPosBind(
		[this](float a1X, float a1Y, float a1Z, float a2X, float a2Y, float a2Z) -> CU::Vector3f
	{
		return (CU::Vector3f(a2X, a2Y, a2Z) - CU::Vector3f(a1X, a1Y, a1Z)).GetNormalized();

	});
	myScriptManager.RegisterFunction("GetDirection", getDirectionFromPosToPosBind,
		"Number, Number, Number, Number, Number, Number",
		"Returns the direction from [x], [y], [z] to another [x], [y], [z]");

	std::function<float(float, float)> getAtan2Bind(
		[this](float aFirstVal, float aSecondVal) -> float
	{
		return std::atan2f(aFirstVal, aSecondVal);

	});
	myScriptManager.RegisterFunction("MathAtan2", getAtan2Bind,
		"Number, Number",
		"Returns the arc tangent of [y] divided by [x] (in radians). Uses the signs of both parameters to find the quadrant of the result.");

#pragma endregion

	CScriptEventManager_WorldInterface::InitFunctions(*this);

	CScriptEventManager_AudioInterface::InitFunctions(*this);

	return true;
}

bool CScriptEventManager::RegisterCallback(const CScriptManager::StateIndexType aStateIndex, const char* aEventName, const char* aCallbackName)
{
	std::string eventName(aEventName);

	std::shared_lock<std::shared_mutex> sharedValidLock(myValidCallbacksMutex);
	if (myValidCallbackEntries.find(eventName) == myValidCallbackEntries.end())
	{
		std::string closestName("NOT FOUND");
		std::string::size_type leastDistance(std::numeric_limits<std::string::size_type>::max());
		for (const auto& callbackEntry : myValidCallbackEntries)
		{
			const std::string::size_type distance(CU::LevenshteinDistance(eventName, callbackEntry));
			if (distance < leastDistance)
			{
				leastDistance = distance;
				closestName = callbackEntry;
			}
		}

		RESOURCE_LOG("[ScriptEventManager] ERROR! Failed to register for event \"%s\", did you mean \"%s\" instead?"
			, eventName.c_str(), closestName.c_str());

		return false;
	}
	sharedValidLock.unlock();

	auto entityIterator(std::find_if(myEntityCallbacks.begin(), myEntityCallbacks.end(), 
		[&aStateIndex](const SCallbackKey& aCallbackKey)
	{
		return (aCallbackKey.CheckIfStateIndex(aStateIndex));
	}));

	if (entityIterator == myEntityCallbacks.end())
	{
		RESOURCE_LOG("[ScriptEventManager] ERROR! Tried to register event from a state index that does not exist. (?)");
		return false;
	}

	std::unique_lock<std::shared_mutex> uniqueLock(myCallbacksMutex);
	auto eventInsertPair(entityIterator->myEvents.emplace(eventName, aCallbackName));
	if (eventInsertPair.second == false)
	{
		eventInsertPair.first->second = aCallbackName;
	}
	uniqueLock.unlock();

	return true;
}

bool CScriptEventManager::UnregisterCallback(const CScriptManager::StateIndexType aStateIndex, const char* aEventName)
{
	std::string eventName(aEventName);

	std::shared_lock<std::shared_mutex> sharedValidLock(myValidCallbacksMutex);
	if (myValidCallbackEntries.find(eventName) == myValidCallbackEntries.end())
	{
		std::string closestName("NOT FOUND");
		std::string::size_type leastDistance(std::numeric_limits<std::string::size_type>::max());
		for (const auto& callbackEntry : myValidCallbackEntries)
		{
			const std::string::size_type distance(CU::LevenshteinDistance(eventName, callbackEntry));
			if (distance < leastDistance)
			{
				leastDistance = distance;
				closestName = callbackEntry;
			}
		}

		RESOURCE_LOG("[ScriptEventManager] ERROR! Failed to unregister event \"%s\", did you mean \"%s\" instead?"
			, eventName.c_str(), closestName.c_str());

		return false;
	}
	sharedValidLock.unlock();

	std::shared_lock<std::shared_mutex> sharedCallbacksLock(myCallbacksMutex);

	auto entityIterator(std::find_if(myEntityCallbacks.begin(), myEntityCallbacks.end()
		, [&aStateIndex](const SCallbackKey& aCallbackKey) -> bool
	{
		return aCallbackKey.CheckIfStateIndex(aStateIndex);
	}));

	if (entityIterator == myEntityCallbacks.end())
	{
		return false;
	}

	if (entityIterator->myStateIndex != aStateIndex)
	{
		RESOURCE_LOG("[ScriptEventManager] ERROR! Failed to unregister event \"%s\", mismatched indices."
			, eventName.c_str());
		return false;
	}

	sharedCallbacksLock.unlock();

	std::unique_lock<std::shared_mutex> uniqueLock(myCallbacksMutex);
	entityIterator->myEvents.erase(eventName);
	uniqueLock.unlock();

	return true;
}

bool CScriptEventManager::EventCallbacksChanged(void* aThis, const char* aFileChangedName)
{
	CScriptEventManager* scriptEventManager(reinterpret_cast<CScriptEventManager*>(aThis));

	if (aThis == nullptr)
	{
		return false;
	}

	return scriptEventManager->EventCallbacksChangedImpl(aFileChangedName);
}

bool CScriptEventManager::EventCallbacksChangedImpl(const char*)
{
	RESOURCE_LOG("[ScriptEventManager] Event callbacks changed, reloading");

	std::unique_lock<std::shared_mutex> uniqueLock(myValidCallbacksMutex);
	myValidCallbackEntries.clear();

	if (InitValidCallbacks() == false)
	{
		GAME_LOG("ERROR! Failed to get valid callbacks");
		return false;
	}
	uniqueLock.unlock();

	RESOURCE_LOG("[ScriptEventManager] SUCCESS! Event callbacks reloaded");

	return true;
}

const UniqueIndexType CScriptEventManager::GetEntityUniqueIndex(const CScriptManager::StateIndexType aStateIndex)
{
	return GetEntityHandle(aStateIndex).myUniqueIndex;
}

const SEntityHandle CScriptEventManager::GetEntityHandle(const CScriptManager::StateIndexType aStateIndex)
{
	std::shared_lock<std::shared_mutex> sharedCallbacksLock(myCallbacksMutex);

	auto entityIterator(std::find_if(myEntityCallbacks.begin(), myEntityCallbacks.end()
		, [&aStateIndex](const SCallbackKey& aCallbackKey)
	{
		return aCallbackKey.CheckIfStateIndex(aStateIndex);
	}));

	if (entityIterator == myEntityCallbacks.end())
	{
		SEntityHandle invalidHandle;
		return invalidHandle;
	}

	return entityIterator->myHandle;
}

