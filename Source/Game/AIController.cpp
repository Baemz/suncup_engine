#include "stdafx.h"
#include "AIController.h"

CAIController::CAIController(const SControllerInit & aInitData, const SAIInit& aAIInitData)
	: CController(aInitData)
	, myWorldInterfaceTag(aAIInitData.myWorldInterfaceTag)
{
	myControllerType = EController::AI;
}
