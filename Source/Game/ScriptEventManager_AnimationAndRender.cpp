#include "stdafx.h"
#include "ScriptEventManager_AnimationAndRender.h"
#include "ScriptEventManager.h"
#include <shared_mutex>
#include "GameSystems\ExposedFunctions\GameLogicFunctions.h"

#define CHECK_IF_STATE_IS_REMOVED(aStateIndex, aReturnValue)																\
std::shared_lock<std::shared_mutex> sharedLock(aScriptEventManager.myStatesToRemoveMutex);									\
if (aScriptEventManager.myStatesToRemove.Find(aStateIndex) != CU::GrowingArray<CScriptManager::StateIndexType>::FoundNone)	\
return aReturnValue;

bool CScriptEventManager_AnimationAndRender::InitFunctions(CScriptEventManager& aScriptEventManager)
{
	CScriptManager& myScriptManager(aScriptEventManager.myScriptManager);

	std::function<void(const CScriptManager::StateIndexType, const char*)> setModelNameBind(
		[&aScriptEventManager](const CScriptManager::StateIndexType aStateIndex, const char* aModelName)
	{
		CHECK_IF_STATE_IS_REMOVED(aStateIndex, ;);

		CGameLogicFunctions::SetModelName(aScriptEventManager.GetEntityUniqueIndex(aStateIndex), aModelName);
	});
	myScriptManager.RegisterFunctionCallWithStateIndex("SetModel", setModelNameBind
		, "String"
		, "Sets the entity's model to the [model] specified");

	std::function<void(const CScriptManager::StateIndexType, const char*)> setMaterialNameBind(
		[&aScriptEventManager](const CScriptManager::StateIndexType aStateIndex, const char* aMaterialName)
	{
		CHECK_IF_STATE_IS_REMOVED(aStateIndex, ;);

		CGameLogicFunctions::SetMaterial(aScriptEventManager.GetEntityUniqueIndex(aStateIndex), aMaterialName);
	});
	myScriptManager.RegisterFunctionCallWithStateIndex("SetMaterial", setMaterialNameBind
		, "String"
		, "Sets the entity's material to the [material] specified");

	std::function<void(const CScriptManager::StateIndexType, const char*)> setAnimationBind(
		[&aScriptEventManager](const CScriptManager::StateIndexType aStateIndex, const char* aAnimationTag)
	{
		CHECK_IF_STATE_IS_REMOVED(aStateIndex, ;);

		CGameLogicFunctions::SetAnimation(aScriptEventManager.GetEntityUniqueIndex(aStateIndex), aAnimationTag);
	});
	myScriptManager.RegisterFunctionCallWithStateIndex("SetAnimation", setAnimationBind
		, "String"
		, "Sets the entity's animation state to the [animation] specified");

	std::function<void(const CScriptManager::StateIndexType, const unsigned int)> setAnimationIndexBind(
		[&aScriptEventManager](const CScriptManager::StateIndexType aStateIndex, const unsigned int aAnimationIndex)
	{
		CHECK_IF_STATE_IS_REMOVED(aStateIndex, ;);

		CGameLogicFunctions::SetAnimationWithIndex(aScriptEventManager.GetEntityUniqueIndex(aStateIndex), aAnimationIndex);
	});
	myScriptManager.RegisterFunctionCallWithStateIndex("SetAnimationWithIndex", setAnimationIndexBind
		, "uint"
		, "Sets the entity's animation state to the index specified");

	std::function<void(const CScriptManager::StateIndexType)> playAnimationBind(
		[&aScriptEventManager](const CScriptManager::StateIndexType aStateIndex)
	{
		CHECK_IF_STATE_IS_REMOVED(aStateIndex, ;);

		CGameLogicFunctions::PlayAnimation(aScriptEventManager.GetEntityUniqueIndex(aStateIndex));
	});
	myScriptManager.RegisterFunctionCallWithStateIndex("PlayAnimation", playAnimationBind
		, ""
		, "Starts the playback of the animation.");

	std::function<void(const CScriptManager::StateIndexType)> pauseAnimationBind(
		[&aScriptEventManager](const CScriptManager::StateIndexType aStateIndex)
	{
		CHECK_IF_STATE_IS_REMOVED(aStateIndex, ;);

		CGameLogicFunctions::PauseAnimation(aScriptEventManager.GetEntityUniqueIndex(aStateIndex));
	});
	myScriptManager.RegisterFunctionCallWithStateIndex("PauseAnimation", pauseAnimationBind
		, ""
		, "Pauses the playback of the animation.");

	std::function<void(const CScriptManager::StateIndexType)> stopAnimationBind(
		[&aScriptEventManager](const CScriptManager::StateIndexType aStateIndex)
	{
		CHECK_IF_STATE_IS_REMOVED(aStateIndex, ;);

		CGameLogicFunctions::StopAnimation(aScriptEventManager.GetEntityUniqueIndex(aStateIndex));
	});
	myScriptManager.RegisterFunctionCallWithStateIndex("StopAnimation", stopAnimationBind
		, ""
		, "Stops the playback of the animation, and resets the animation frame to the initial frame.");

	std::function<void(const CScriptManager::StateIndexType, const float)> setAnimationSpeedBind(
		[&aScriptEventManager](const CScriptManager::StateIndexType aStateIndex, const float aSpeed)
	{
		CHECK_IF_STATE_IS_REMOVED(aStateIndex, ;);

		CGameLogicFunctions::SetAnimationSpeed(aScriptEventManager.GetEntityUniqueIndex(aStateIndex), aSpeed);
	});
	myScriptManager.RegisterFunctionCallWithStateIndex("SetAnimationSpeed", setAnimationSpeedBind
		, "Float"
		, "Sets the animation speed. (DEFAULT VALUE: 30.0)");

	std::function<const float(const CScriptManager::StateIndexType)> getAnimationDurationBind(
		[&aScriptEventManager](const CScriptManager::StateIndexType aStateIndex) -> const float
	{
		CHECK_IF_STATE_IS_REMOVED(aStateIndex, 0.f;);

		return CGameLogicFunctions::GetAnimationDuration(aScriptEventManager.GetEntityUniqueIndex(aStateIndex));
	});
	myScriptManager.RegisterFunctionCallWithStateIndex("GetAnimationDuration", getAnimationDurationBind
		, ""
		, "Gets the full duration of the current animation.");

	std::function<void(const CScriptManager::StateIndexType, const float aX, const float aY, const float aZ)> rotateModelBind(
		[&aScriptEventManager](const CScriptManager::StateIndexType aStateIndex, const float aX, const float aY, const float aZ) -> void
	{
		CHECK_IF_STATE_IS_REMOVED(aStateIndex, ;);

		return CGameLogicFunctions::RotateModel(aScriptEventManager.GetEntityUniqueIndex(aStateIndex), CU::Vector3f(aX, aY, aZ));
	});
	myScriptManager.RegisterFunctionCallWithStateIndex("RotateModel", rotateModelBind
		, "Float, Float, Float"
		, "Rotates the model.");

	return true;
}
