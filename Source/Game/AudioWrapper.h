#pragma once
#include <map>
#include <mutex>
#include "../CommonUtilities/Vector3.h"
#include "../EntitySystem/Entity.h"
#include "../AudioEngine/AudioEngineTypedefs.h"

//Also define in AudioManager.h!
//#define AUDIO_ENGINE_OFF

#ifdef _MATEDIT
#define AUDIO_ENGINE_OFF
#endif

class CAudioWrapper
{
private:
	struct SPostedEventData
	{
		SPostedEventData()
			: myAudioID(ULONG_MAX)
			, myGameObjectID(INT64_MAX)
			, myCanAutoUpdatePosition(true)
		{}

		SPostedEventData(const AudioEngine::RegObjectID aAudioID, const UniqueIndexType& aGameObjectID)
			: myAudioID(aAudioID)
			, myGameObjectID(aGameObjectID)
			, myCanAutoUpdatePosition(true)
		{}

		AudioEngine::RegObjectID myAudioID;
		UniqueIndexType myGameObjectID;
		bool myCanAutoUpdatePosition;
	};

public:
	~CAudioWrapper();

	static void Create();
	static void Destroy();

	static void UpdateAudioPositions();
	static void EmptyPostedEventContainer();

	static void PostEvent(const char* aEventName, const UniqueIndexType& aGameObjectID, const AudioEngine::RegObjectID aRegisteredAudioObjectID = 0);
	static void SetRTPC(const char* aRTPC_Name, const float aValue, const unsigned int aRegisteredAudioObjectID = 1);
	static void SetState(const char* aStateGroupName, const char* aStateName);

	static void SetListenerPositionAndOrientation(const CU::Vector3f& aPosition, const CU::Vector3f& aFront, const CU::Vector3f& aTop);
	static void SetObjectPositionAndOrientation(const CU::Vector3f& aPosition, const CU::Vector3f& aFront, const CU::Vector3f& aTop,
		const char* aEventName, const UniqueIndexType& aGameObjectID);
	static void SetObjectPosition(const CU::Vector3f& aPosition, const char* aAudioObjectName, const UniqueIndexType& aGameObjectID);

	static void SetListenerObjectIndex(const UniqueIndexType& aGameObjectID);

private:
	CAudioWrapper();
	CAudioWrapper(CAudioWrapper&& aAudioWrapper) = delete;
	CAudioWrapper(const CAudioWrapper& aAudioWrapper) = delete;
	void operator=(const CAudioWrapper& aAudioWrapper) = delete;

private:
	static CAudioWrapper* myInstance_ptr;

	std::unordered_map<std::string, SPostedEventData> myPostedEvents;
	std::shared_mutex mySharedMutex;
	volatile UniqueIndexType myListenerIndex;
};

