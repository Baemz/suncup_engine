#pragma once
#include "../CommonUtilities/Vector3.h"
#include <string>

enum class EController
{
	NONE,
	Input,
	AI,
	Script,
	/* Network */
};

enum eWorldInterfaceTag
{
	Polling,
	Events
};

struct SControllerInit
{
	SControllerInit()
		: myMaxSpeed(1.0f)
		, myMaxForce(1.0f)
		, myRotationSpeed(1.0f)
	{
	}

	SControllerInit(const float aMaxSpeed, const float aMaxForce, const float aRotationSpeed)
		: myMaxSpeed(aMaxSpeed)
		, myMaxForce(aMaxForce)
		, myRotationSpeed(aRotationSpeed)
	{
	}

	float myMaxSpeed;
	float myMaxForce;
	float myRotationSpeed;
};

struct SInputInit
{
	//SInputInit() = default;
	SInputInit(const CU::Vector3f& aCameraOffset)
		: myCameraOffset(aCameraOffset)
	{}

	const CU::Vector3f& myCameraOffset;
};

struct SAIInit
{
	SAIInit() = default;
	SAIInit(eWorldInterfaceTag aWorldInterfaceTag)
		: myWorldInterfaceTag(aWorldInterfaceTag)
	{
	}

	eWorldInterfaceTag myWorldInterfaceTag;
};

struct SScriptInit
{
	SScriptInit() = default;
	SScriptInit(const std::string& aScriptFilePath)
		: myScriptFilePath(aScriptFilePath)
	{
	}

	std::string myScriptFilePath;
};