#pragma once
#include "..\CommonUtilities\Vector3.h"
#include "EntitySystemLink\EntitySystemLink.h"
#include "GameSystems\SystemAbstract.h"
class CHealingCircleSystem : public CSystemAbstract<CHealingCircleSystem>
{
public:
	CHealingCircleSystem(EManager& aEntityManager);
	~CHealingCircleSystem() override = default;

	void Activate();
	void Deactivate();

	void Update(const float aDeltaTime, const CU::GrowingArray<const CU::GrowingArray<UniqueIndexType>*>& aEntities) override;
	void TossHealing(const CU::Vector3f& aFromPos, const CU::Vector3f& aToPos, 
		const float aHealLifeTime, const float aHealCircleRadius, const float aHealingPerTick, const float aHealingTickInterval, 
		const float aDamageLifeTime, const float aDamageCircleRadius, const float aDamagePerSecond, const float aDamageZoneSpawnInterval, const float aFlightSpeed);
	void CreateHealingCircle(const CU::Vector3f& aPosition, const float aLifeTime, const float aRadius, const float aHealingPerSecond, const float aHealingPerTickInterval);


private:
	struct STossedHealing
	{
		STossedHealing()
			: myHealLifeTime(0.f)
			, myHealCircleRadius(0.f)
			, myHealingPerTick(0.f)
			, myHealingTickInterval(0.f)
			, myHealingTickIntervalOriginal(0.f)
			, myDamageLifeTime(0.f)
			, myDamageCircleRadius(0.f)
			, myDamagePerSecond(0.f)
			, myDamageZoneSpawnInterval(0.f)
			, myDamageZoneSpawnIntervalOriginal(0.f)
			, myFlightTime(0.f)
			, myFlightSpeed(0.f)

		{}

		STossedHealing(const CU::Vector3f& aFromPos, const CU::Vector3f& aToPos, const CU::Vector3f& aDirection, 
			const float aHealLifeTime, const float aHealCircleRadius, const float aHealingPerTick, const float aHealingTickInterval,
			const float aDamageLifeTime, const float aDamageCircleRadius, const float aDamagePerSecond, const float aDamageZoneSpawnInterval, const float aFlightTime, const float aFlightSpeed)
			: myPosition(aFromPos)
			, myFromPos(aFromPos)
			, myToPos(aToPos)
			, myDirection(aDirection)
			, myHealLifeTime(aHealLifeTime)
			, myHealCircleRadius(aHealCircleRadius)
			, myHealingPerTick(aHealingPerTick)
			, myHealingTickInterval(aHealingTickInterval)
			, myHealingTickIntervalOriginal(aHealingTickInterval)
			, myDamageLifeTime(aDamageLifeTime)
			, myDamageCircleRadius(aDamageCircleRadius)
			, myDamagePerSecond(aDamagePerSecond)
			, myDamageZoneSpawnInterval(0.f)
			, myDamageZoneSpawnIntervalOriginal(aDamageZoneSpawnInterval)
			, myFlightTime(aFlightTime)
			, myFlightSpeed(aFlightSpeed)
		{
		}
		~STossedHealing() = default;

		CU::Vector3f myPosition;
		CU::Vector3f myFromPos;
		CU::Vector3f myToPos;
		CU::Vector3f myDirection;
		float myHealLifeTime;
		float myHealCircleRadius;
		float myHealingPerTick;
		float myHealingTickInterval;
		float myHealingTickIntervalOriginal;
		float myDamageLifeTime;
		float myDamageCircleRadius;
		float myDamagePerSecond;
		float myDamageZoneSpawnInterval;
		float myDamageZoneSpawnIntervalOriginal;
		float myFlightTime;
		float myFlightSpeed;
	};

	struct SHealingCircle
	{
		SHealingCircle()
			: myLifeTime(0.f)
			, myRadius(0.f)
			, myHealingPerTick(0.f)
			, myHealingTickInterval(0.f)
			, myHealingTickIntervalOriginal(0.f)
		{}

		SHealingCircle(const CU::Vector3f& aPosition, const float aLifeTime, const float aRadius, const float aHealingPerTick, const float aHealingTickInterval)
			: myPosition(aPosition)
			, myLifeTime(aLifeTime)
			, myRadius(aRadius)
			, myHealingPerTick(aHealingPerTick)
			, myHealingTickInterval(0.f)
			, myHealingTickIntervalOriginal(aHealingTickInterval)
		{
		}
		~SHealingCircle() = default;

		CU::Vector3f myPosition;
		float myLifeTime;
		float myRadius;
		float myHealingPerTick;
		float myHealingTickInterval;
		float myHealingTickIntervalOriginal;
	};

	CU::GrowingArray<STossedHealing> myTossedHealings;
	CU::GrowingArray<SHealingCircle> myHealingCircles;
	std::mutex myTossedHealingMutex;
	std::mutex myHealingCircleMutex;
};

