#pragma once
#include "State.h"
#include "..\Menu\Menu.h"

class COptionMenuState: private CState
{
public:
	COptionMenuState(EManager& aEntityManager);
	~COptionMenuState() {}

	void Init() override;
	void Update(const float aDeltaTime) override;
	void Render() override;

	void OnActivation() override;
	void OnDeactivation() override;

private:
	void AddAllSFXCheckBoxes();
	void UpdateSpecialEffectsState() const;

private:
	CMenu myMenu;
	bool myKeepMenu;
};

