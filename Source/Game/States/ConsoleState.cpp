#include "stdafx.h"
#include "ConsoleState.h"

#include "../Script/ScriptManager.h"
#include "..\CommonUtilities\LevenshteinDistance.h"
#include "..\GraphicsEngine\ImGUI\imgui.h"
#include "..\GraphicsEngine\GraphicsEngineInterface.h"
#include <shared_mutex>
#include <cctype>
#include <string>
#include <sstream>
#include "../GraphicsEngine/ImGUI/imgui_internal.h"

namespace CConsoleStateStatics
{
	static std::mutex myCommandLock;
	static std::mutex myCommandListLock;
	static std::string myCommandField = "";
	static volatile unsigned short myCurrSelectedCmdIndex = USHRT_MAX;
	static volatile bool myExecuteCommand = false;
	static volatile bool myCommandWasExecuted = false;
	static volatile bool myConsoleWindowWasJustOpened = false;
	static std::stringstream myConsoleOutput;
	static std::shared_mutex myConsoleOutputSharedLock;
}

using namespace CConsoleStateStatics;

CU::GrowingArray<SInGameConsoleCommand, unsigned short> CConsoleState::mySimilarCommandList;

#define ADD_STATE(state) (myConsoleState = EState(myConsoleState | (state)));
#define HAS_STATE(state) ((myConsoleState & (state)) != 0)

void CConsoleState::SetAsConsoleOutput()
{
	sce::CLogger::SStreamBuffer streamBuffer;
	streamBuffer.myStreamBuffer = myConsoleOutput.rdbuf();
	streamBuffer.mySharedLock = &myConsoleOutputSharedLock;
	sce::CLogger::Get()->SetActiveStreamBuffer(streamBuffer);
}

CConsoleState::CConsoleState(EManager& aEntityManager)
	: CState(aEntityManager)
	, myCommand("")
	, myConsoleState(EState::eNothing)
{
}

CConsoleState::~CConsoleState()
{
}

void CConsoleState::Init()
{
	// Collect commands
	if (CScriptManager::Get() == nullptr)
	{
		RESOURCE_LOG("[CConsoleState] ERROR! Failed to get the list of exposed function because Script Manager was invalid (nullptr).");
		return;
	}

	const auto funcList(CScriptManager::Get()->GetRegisteredFunctionNamesOnlyConsole());
	myCommandList.Init((unsigned short)funcList.Size());
	mySimilarCommandList.Init((unsigned short)funcList.Size());
	SInGameConsoleCommand cmdToAdd;

	for (size_t i(0); i < funcList.Size(); ++i)
	{
		cmdToAdd.myCommand = funcList[i];
		cmdToAdd.myFuncName = funcList[i];

		myCommandList.Add(cmdToAdd);
	}

	myCommandField = "";
	myCurrSelectedCmdIndex = USHRT_MAX;
	myConsoleState = EState::eNothing;
	myExecuteCommand = false;
}

void CConsoleState::Update(const float aDeltaTime)
{
	aDeltaTime;

	if (HAS_STATE(EState::eCmdChanged))
	{
		myCurrSelectedCmdIndex = USHRT_MAX;

		if (myCommandField.size() > 0 && myCommandList.Size() > 0)
		{
			std::string commandField("");
			{
				std::lock_guard<std::mutex> lock(myCommandLock);
				commandField = myCommandField;
			}

			std::lock_guard<std::mutex> lock(myCommandListLock);
			mySimilarCommandList.RemoveAll();

			for (unsigned short i(0); i < myCommandList.Size(); ++i)
			{
				if (std::search(myCommandList[i].myCommand.begin(), myCommandList[i].myCommand.end(), commandField.begin(), commandField.end(),
					[](char ch1, char ch2) { return std::toupper(ch1) == std::toupper(ch2); }) != myCommandList[i].myCommand.end())
				{
					if (myCommandList[i].myCommand.size() != commandField.size())
					{
						mySimilarCommandList.Add(myCommandList[i]);
					}
				}
			}

			for (unsigned short i(0); i < mySimilarCommandList.Size(); ++i)
			{
				mySimilarCommandList[i].mySimilarityVal = CU::LevenshteinDistance(commandField, mySimilarCommandList[i].myCommand);
			}

			std::sort(mySimilarCommandList.GetRawData(), mySimilarCommandList.GetRawData() + mySimilarCommandList.Size(), CompareCommands);
		}
	}

	if (HAS_STATE(EState::eMoveSuggestionUp))
	{
		if (myCurrSelectedCmdIndex == USHRT_MAX)
		{
			myCurrSelectedCmdIndex = 0;
		}
		else if (myCurrSelectedCmdIndex <= 0)
		{
			myCurrSelectedCmdIndex = mySimilarCommandList.Size() - 1;
		}
		else
		{
			--myCurrSelectedCmdIndex;
		}
	}

	if (HAS_STATE(EState::eMoveSuggestionDown))
	{
		if (myCurrSelectedCmdIndex == USHRT_MAX)
		{
			myCurrSelectedCmdIndex = 0;
		}
		else if (myCurrSelectedCmdIndex >= mySimilarCommandList.Size() - 1)
		{
			myCurrSelectedCmdIndex = 0;
		}
		else
		{
			++myCurrSelectedCmdIndex;
		}
	}

	if (myConsoleState != EState::eNothing)
	{
		myConsoleState = EState::eNothing;
	}
}

void CConsoleState::Render()
{
}

void CConsoleState::OnActivation()
{
	myLog = "";
	myConsoleWindowWasJustOpened = true;
	sce::gfx::CGraphicsEngineInterface::SetConsoleRenderCallback(std::bind(&CConsoleState::ConsoleRenderCallback, this));
	AttachToEventReceiving(Event::ESubscribedEvents(Event::eKeyInput));
}

void CConsoleState::OnDeactivation()
{
	sce::gfx::CGraphicsEngineInterface::SetConsoleRenderCallback(nullptr);
	DetachFromEventReceiving();
}

void CConsoleState::ReceiveEvent(const Event::SEvent& aEvent)
{
	if (aEvent.myType == Event::EEventType::eKeyInput)
	{
		const Event::SKeyEvent* keyMsgPtr = reinterpret_cast<const Event::SKeyEvent*>(&aEvent);

		if (keyMsgPtr->state == EButtonState::Pressed)
		{
			if (keyMsgPtr->key == CU::CKeyBinds::GetHotKeys().myEscape || keyMsgPtr->key == CU::CKeyBinds::GetHotKeys().myTilde)
			{
				myState = CState::EStateCommand::eRemoveState;
			}
			else if (keyMsgPtr->key == CU::CKeyBinds::GetHotKeys().myEnterInput)
			{
				myExecuteCommand = true;
			}

			if (mySimilarCommandList.Size() > 0 && myCommand[0] > 0)
			{
				if (keyMsgPtr->key == CU::CKeyBinds::GetHotKeys().myArrowUp)
				{
					ADD_STATE(EState::eMoveSuggestionUp);
				}
				else if (keyMsgPtr->key == CU::CKeyBinds::GetHotKeys().myArrowDown)
				{
					ADD_STATE(EState::eMoveSuggestionDown);
				}
				else if (keyMsgPtr->key == CU::CKeyBinds::GetHotKeys().myTab)
				{
					if (myCurrSelectedCmdIndex == USHRT_MAX)
					{
						myCurrSelectedCmdIndex = 0;
					}

					myExecuteCommand = true;
				}
			}
		}
		else if (keyMsgPtr->state == EButtonState::Released)
		{
			if (keyMsgPtr->key == CU::CKeyBinds::GetHotKeys().myTilde)
			{
				myConsoleWindowWasJustOpened = false;
			}
		}
	}
	if (aEvent.myType == Event::EEventType::eMouseClickInput)
	{
		//const Event::SMouseClickEvent* clickPtr = reinterpret_cast<const Event::SMouseClickEvent*>(&aEvent);


	}
}

/* PRIVATE FUNCTIONS */

void CConsoleState::ConsoleRenderCallback()
{
	ImGui::SetNextWindowPos(ImVec2(10, 0));
	ImGui::SetNextWindowSize(ImVec2(sce::gfx::CGraphicsEngineInterface::GetFullWindowedResolution().x, 700), ImGuiSetCond_Appearing);
	ImGui::Begin("Console", 0);
	ImGui::BeginChild("Scrolling", { sce::gfx::CGraphicsEngineInterface::GetFullWindowedResolution().x - 40, 630 }, true, ImGuiWindowFlags_ChildWindow);
	ImGui::TextUnformatted(myLog.c_str());

	ImGui::SetFontSize(ImGui::GetFontSize() * 1.5f);

	bool shouldScrollToBottom(myCommandWasExecuted);

	std::shared_lock<std::shared_mutex> sharedLock(myConsoleOutputSharedLock);

	constexpr std::size_t arraySize(IM_ARRAYSIZE(ImGui::GetCurrentContext()->TempBuffer));
	std::string consoleOutput(myConsoleOutput.str());
	bool changedLastChar(false);
	char lastChar('\0');

	static std::size_t lastSize(0);
	if (lastSize != consoleOutput.size())
	{
		shouldScrollToBottom = true;
	}
	lastSize = consoleOutput.size();

	std::size_t incrementer(arraySize);
	for (std::size_t index(0); index < consoleOutput.size(); index += incrementer)
	{
		if (incrementer != arraySize)
		{
			incrementer = arraySize;
		}

		if (changedLastChar == true)
		{
			changedLastChar = false;
			consoleOutput[index] = lastChar;
		}

		std::size_t endIndex(index + arraySize);

		const bool endIndexAboveSize(endIndex > consoleOutput.size());
		if (endIndexAboveSize == true)
		{
			endIndex = consoleOutput.size();
		}
		else
		{
			changedLastChar = true;

			const std::size_t lastNewlinePosition(consoleOutput.find_last_of('\n'));

			if (consoleOutput.npos == lastNewlinePosition)
			{
				assert("Too big message going to the console" && (false));
				continue;
			}

			endIndex = lastNewlinePosition;
			incrementer = endIndex - index;

			lastChar = consoleOutput[endIndex];
			consoleOutput[endIndex] = '\0';
		}

		const char* const beginText(&(consoleOutput.c_str()[index]));
		const char* const endText(&(consoleOutput.c_str()[endIndex]));

		ImGui::TextUnformatted(beginText, endText);
	}

	sharedLock.unlock();

	if (shouldScrollToBottom == true)
	{
		ImGui::SetScrollY(ImGui::GetScrollMaxY());
	}

	if (mySimilarCommandList.Size() > 0 && myCommand[0] > 0)
	{
		std::lock_guard<std::mutex> lock(myCommandListLock);

		ImGui::SetNextWindowPos(ImVec2(30, 450));
		ImGui::PushStyleColor(ImGuiCol_ChildBg, ImVec4(0.0f, 0.0f, 0.0f, 0.9f));
		ImGui::BeginChild("Suggestions", { sce::gfx::CGraphicsEngineInterface::GetFullWindowedResolution().x - 600, 200 }, true, ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove);
		ImGui::SetFontSize(ImGui::GetFontSize() * 1.5f);
		for (unsigned short i(0); i < mySimilarCommandList.Size(); ++i)
		{
			if (i == myCurrSelectedCmdIndex)
			{
				ImGui::TextColored(ImVec4(0.0f, 1.0f, 0.0f, 1.0f), mySimilarCommandList[i].myCommand.c_str());
			}
			else
			{
				if (i == 0)
				{
					ImGui::TextColored(ImVec4(1.0f, 1.0f, 1.0f, 1.0f), mySimilarCommandList[i].myCommand.c_str());
				}
				else
				{
					ImGui::TextColored(ImVec4(0.5f, 0.5f, 0.5f, 1.0f), mySimilarCommandList[i].myCommand.c_str());
				}
			}
		}

		ImGui::EndChild();
		ImGui::PopStyleColor(1);
	}

	myCommandWasExecuted = false;
	ImGui::EndChild();
	ImGui::Separator();

	{
		auto callbackFunc = [](ImGuiTextEditCallbackData* data)
		{
			if (myExecuteCommand)
			{
				data->DeleteChars(0, data->BufTextLen);

				if (myCurrSelectedCmdIndex != USHRT_MAX)
				{
					std::unique_lock<std::mutex> lock1(myCommandLock, std::defer_lock);
					std::unique_lock<std::mutex> lock2(myCommandListLock, std::defer_lock);
					std::lock(lock1, lock2);

					myCommandField = mySimilarCommandList[(unsigned short)myCurrSelectedCmdIndex].myCommand;
					data->InsertChars(0, myCommandField.c_str(), myCommandField.c_str() + myCommandField.size());
				}
			}

			return 0;
		};

		if (!myConsoleWindowWasJustOpened)
		{
			ImGui::SetFontSize(ImGui::GetFontSize() * 1.5f);
			ImGui::SetKeyboardFocusHere();
			if (ImGui::InputText("command", myCommand, (sizeof(myCommand) / sizeof(myCommand[0])), ImGuiInputTextFlags_CallbackAlways, callbackFunc))
			{
				if (myExecuteCommand)
				{
					myExecuteCommand = false;

					if (myCurrSelectedCmdIndex == USHRT_MAX)
					{
						GAME_LOG("\nCommand: Pushing command: \"%s\""
							, myCommandField.c_str());

						if ((myCommandField == "help") || (myCommandField == "?"))
						{
							GAME_LOG("Commands available:");
							for (const auto& command : myCommandList)
							{
								GAME_LOG(("-> " + command.myCommand).c_str());
							}
						}
						else
						{
							if (CallCommand(myCommandField) == false)
							{
								GAME_LOG("Command: Failed to call \"%s\""
									, myCommandField.c_str());
							}

							myCommandWasExecuted = true;
						}
					}
					else
					{
						myCurrSelectedCmdIndex = USHRT_MAX;
					}
				}
				else
				{
					std::lock_guard<std::mutex> lock(myCommandLock);
					myCommandField = std::string(myCommand);
				}

				ADD_STATE(EState::eCmdChanged);
			}
		}
	}

	ImGui::End();
}

bool CConsoleState::CallCommand(const std::string& aCommandField)
{
	CScriptManager* scriptManager(CScriptManager::Get());
	if (scriptManager == nullptr)
	{
		return false;
	}

	CScriptManager::SFunctionAsIfFromLuaResult result;

	const std::string commandToUse("return " + aCommandField);

	const bool success(scriptManager->CallCPPFunctionAsIfFromLuaSTRING(commandToUse, result));

	if (success == true)
	{
		std::string resultAsString;

		switch (result.myType)
		{
		case CScriptManager::SFunctionAsIfFromLuaResult::EType::None:
		{
			break;
		}
		case CScriptManager::SFunctionAsIfFromLuaResult::EType::Bool:
		{
			resultAsString = ((result.myBool == true) ? ("true") : ("false"));
			break;
		}
		case CScriptManager::SFunctionAsIfFromLuaResult::EType::Number:
		{
			resultAsString = std::to_string(result.myNumber);
			break;
		}
		case CScriptManager::SFunctionAsIfFromLuaResult::EType::String:
		{
			resultAsString = result.myString;
			break;
		}
		case CScriptManager::SFunctionAsIfFromLuaResult::EType::Pointer:
		{
			std::stringstream stringStream;
			stringStream << static_cast<const void* const>(result.myPointer);
			resultAsString = stringStream.str();
			break;
		}
		default:
		{
			assert("Result not supported in CConsoleState::CallCommand" && (false));
			break;
		}
		}

		if (resultAsString.empty() == false)
		{
			GAME_LOG("Command result: %s"
				, resultAsString.c_str());
		}
	}

	return success;
}

const bool CConsoleState::CompareCommands(const SInGameConsoleCommand& aCmd1, const SInGameConsoleCommand& aCmd2)
{
	return aCmd1.mySimilarityVal < aCmd2.mySimilarityVal;
}
