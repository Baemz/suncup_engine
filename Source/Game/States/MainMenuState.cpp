#include "stdafx.h"
#include "MainMenuState.h"
#include "StateStack.h"
#include "InGameState.h"
#include "CreditsState.h"
#include "ShowRoomState.h"
#include "LevelSelectState.h"
#include "OptionMenuState.h"
#include "../AudioEngine/AudioManager.h"
#include "../GraphicsEngine/Sprite/Sprite.h"
#include "../EngineCore/CommandLineManager/CommandLineManager.h"

#define MAX_FADE_TIME (1.0f)

CMainMenuState::CMainMenuState(EManager& aEntityManager)
	: CState(aEntityManager)
	, myQuitGame(false)
	, myRenderBackgroundWhenInactive(false)
	//, myCurrFadeTime(MAX_FADE_TIME)
	//, myIsFadingIn(false)
	//, myShouldFade(false)
	//, myPressedStart(false)
	//, myPressedCredits(false)
{
	AttachToEventReceiving(Event::ESubscribedEvents(Event::EEventType::eGameState));
}

CMainMenuState::~CMainMenuState()
{
	DetachFromEventReceiving();
}

void CMainMenuState::Init()
{
	//myFadeSprite.Init("Data/Misc/background.dds");
	//myFadeSprite.SetPosition({ 0.5f });
	//myFadeSprite.SetScale({ 1000.0f });
	//myFadeSprite.SetPivot({ 0.5f });
	//myFadeSprite.SetAlpha(1.0f);

	//{
	//	std::function<void()> hoverSoundPlay(
	//		[this]()
	//	{
	//		CAudioManager::PostEventAndGetID("Button_Hover", 1);
	//	});
	//
	//	myMenu.SetButtonOnEnterSoundFunc(hoverSoundPlay);
	//}
	//
	//myMenu.AddGraphic("Data/Menu/Black_4x4.dds", { 0.0f }, { 1000.0f });
	//myMenu.AddGraphic("Data/Menu/Main/mainMenu_backGround.dds", { 0.0f });
	//myMenu.AddGraphic("Data/Menu/Main/mainMenu_signBackground.dds", { 0.01f, 0.0f }, { 1.0f }, { 1.0f }, "background");
	//
	//myMenu.AddButton("Data/Menu/Main/mainMenu_startGame.dds",	"Data/Menu/Main/mainMenu_startGame_toggleOn.dds",	"start_game",	{ 0.045f, 0.33f },	{ 0.75f, 0.5f });
	//myMenu.AddButton("Data/Menu/Main/mainMenu_showroom.dds",	"Data/Menu/Main/mainMenu_showroom_toggleOn.dds",	"showroom",		{ 0.045f, 0.42f },	{ 0.75f, 0.5f });
	//myMenu.AddButton("Data/Menu/Main/mainMenu_options.dds",		"Data/Menu/Main/mainMenu_options_toggleOn.dds",		"options",		{ 0.045f, 0.51f },	{ 0.75f, 0.5f });
	//myMenu.AddButton("Data/Menu/Main/mainMenu_credits.dds",		"Data/Menu/Main/mainMenu_credits_toggleOn.dds",		"credits",		{ 0.045f, 0.60f },	{ 0.75f, 0.5f });
	//myMenu.AddButton("Data/Menu/Main/mainMenu_quit.dds",		"Data/Menu/Main/mainMenu_quit_toggleOn.dds",		"exit",			{ 0.045f, 0.69f },	{ 0.75f, 0.5f });

	//if (sce::CCommandLineManager::HasParameter(L"-level"))
	//{
		CStateStack::PushMainStateOnTop(sce_new(CInGameState(myEntityManager, "Data/Levels/VXGI.dataMap")));
		//}
		//else
		//{
		//	CAudioManager::PostEventAndGetID("Play_Menu_Music", 1);
		//}
}

void CMainMenuState::Update(const float aDeltaTime)
{
	if (myQuitGame)
	{
		myState = CState::EStateCommand::eRemoveState;
		return;
	}

	myMenu.Update(aDeltaTime);

	if (myMenu.CheckPressedButton("start_game"))
	{
		myRenderBackgroundWhenInactive = true;
		CAudioManager::PostEventAndGetID("Button_Pressed_Short", 1);
		CStateStack::PushUnderStateOnTop(this, sce_new(CLevelSelectState(myEntityManager)), true);
	}
	else if (myMenu.CheckPressedButton("showroom"))
	{
		CAudioManager::PostEventAndGetID("Stop_Menu_Music", 1);
		CAudioManager::PostEventAndGetID("Button_Pressed_Long", 1);
		CStateStack::PushMainStateOnTop(sce_new(CShowRoomState(myEntityManager)));
	}
	else if (myMenu.CheckPressedButton("options"))
	{
		CAudioManager::PostEventAndGetID("Button_Pressed_Short", 1);
		CStateStack::PushUnderStateOnTop(this, sce_new(COptionMenuState(myEntityManager)), true);
	}
	else if (myMenu.CheckPressedButton("credits"))
	{
		CAudioManager::PostEventAndGetID("Button_Pressed_Short", 1);
		CStateStack::PushUnderStateOnTop(this, sce_new(CCreditsState(myEntityManager, false)), true);
	}
	else if (myMenu.CheckPressedButton("exit"))
	{
		myState = CState::EStateCommand::eRemoveState;
	}
}

void CMainMenuState::Render()
{
	myMenu.Render(false);
}

void CMainMenuState::OnActivation()
{
	myRenderBackgroundWhenInactive = false;
	myMenu.SetGraphicRenderState("background", true);
	myMenu.Enable();
}

void CMainMenuState::OnDeactivation()
{
	if (myRenderBackgroundWhenInactive == false)
	{
		myMenu.SetGraphicRenderState("background", false);
	}
	myMenu.Disable();
}

void CMainMenuState::ReceiveEvent(const Event::SEvent& aEvent)
{
	const Event::SGameState& gameStateEvent(static_cast<const Event::SGameState&>(aEvent));

	if (gameStateEvent.myState == Event::EGameState::eQuit)
	{
		myQuitGame = true;
	}
}

/* PRIVATE FUNCTIONS */

//void CMainMenuState::UpdateFadeAndStartGameOnCommand(const float aDeltaTime)
//{
//	myCurrFadeTime -= aDeltaTime;
//	if (myCurrFadeTime <= 0.0f)
//	{
//		myCurrFadeTime = 0.0f;
//
//		if (myPressedStart)
//		{
//			myMenu.Disable();
//			// Place next state here
//		}
//		else if (myPressedCredits)
//		{
//			myMenu.Disable();
//			// Place next state here
//		}
//	}
//
//	if (myIsFadingIn) myFadeSprite.SetAlpha(myCurrFadeTime / MAX_FADE_TIME);
//	else myFadeSprite.SetAlpha(1.0f - (myCurrFadeTime / MAX_FADE_TIME));
//}
