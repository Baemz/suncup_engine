#include "stdafx.h"
#include "StateStack.h"
#include "PauseMenuState.h"
#include "OptionMenuState.h"
#include "ControlMenuState.h"
#include "../AudioEngine/AudioManager.h"
#include "../EngineCore/EventSystem/EventHandler.h"

#define MENU_POSITION CU::Vector2f(0.3667f, 0.0f)
#define GET_POS_WITH_MENU_POS(aX, aY) (MENU_POSITION + CU::Vector2f(aX, aY))

CPauseMenuState::CPauseMenuState(EManager& aEntityManager)
	: CState(aEntityManager)
	, myKeepMenu(false)
{
}

CPauseMenuState::~CPauseMenuState()
{
	CAudioManager::PostEventAndGetID("Stop_Menu_Music", 1);
}

void CPauseMenuState::Init()
{
	myKeepMenu = false;

	{
		std::function<void()> hoverSoundPlay(
			[this]()
		{
			CAudioManager::PostEventAndGetID("Button_Hover", 1);
		});

		myMenu.SetButtonOnEnterSoundFunc(hoverSoundPlay);
	}

	myMenu.AddGraphic("Data/Menu/Black_4x4.dds", { 0.0 }, { 1000.0f }, { 1.0f, 1.0f, 1.0f, 0.6f });
	myMenu.AddGraphic("Data/Menu/Pause/pauseMenu_sign.dds", MENU_POSITION);

	myMenu.AddButton("Data/Menu/Pause/pauseMenu_resume_noToggle.dds", "Data/Menu/Pause/pauseMenu_resume_withToggle.dds", "resume", GET_POS_WITH_MENU_POS(0.035f, 0.33f), { 0.75f, 0.5f });
	myMenu.AddButton("Data/Menu/Main/mainMenu_options.dds", "Data/Menu/Main/mainMenu_options_toggleOn.dds", "options", GET_POS_WITH_MENU_POS(0.035f, 0.44f), { 0.75f, 0.5f });
	myMenu.AddButton("Data/Menu/Pause/pauseMenu_quittomainMenu_noToggle.dds", "Data/Menu/Pause/pauseMenu_quittomainMenu_withToggle.dds", "goto_main_menu", GET_POS_WITH_MENU_POS(0.035f, 0.55f), { 0.75f, 0.5f });
	myMenu.AddButton("Data/Menu/Main/mainMenu_quit.dds", "Data/Menu/Main/mainMenu_quit_toggleOn.dds", "quit_game", GET_POS_WITH_MENU_POS(0.035f, 0.66f), { 0.75f, 0.5f });

	CAudioManager::PostEventAndGetID("Play_Menu_Music", 1);
}

void CPauseMenuState::Update(const float aDeltaTime)
{
	myKeepMenu = myMenu.Update(aDeltaTime);

	if (myKeepMenu == false || myMenu.CheckPressedButton("resume"))
	{
		myState = CState::EStateCommand::eRemoveState;

		if (myMenu.CheckPressedButton("resume"))
		{
			CAudioManager::PostEventAndGetID("Button_Pressed_Short", 1);
		}
	}
	else
	{
		if (myMenu.CheckPressedButton("options"))
		{
			CAudioManager::PostEventAndGetID("Button_Pressed_Short", 1);
			CStateStack::PushUnderStateOnTop(this, sce_new(COptionMenuState(myEntityManager)), true);
		}
		else if (myMenu.CheckPressedButton("goto_main_menu"))
		{
			CAudioManager::PostEventAndGetID("Button_Pressed_Short", 1);
			myState = CState::EStateCommand::eRemoveMainState;
		}
		else if (myMenu.CheckPressedButton("quit_game"))
		{
			Event::SGameState* event_ptr(sce_new(Event::SGameState));
			event_ptr->myState = Event::EGameState::eQuit;

			sce::CEventHandler::Get().SendEvent(event_ptr);
			myState = CState::EStateCommand::eRemoveMainState;
		}
	}
}

void CPauseMenuState::Render()
{
	myMenu.Render(false);
}

void CPauseMenuState::OnActivation()
{
	myMenu.Enable();
}

void CPauseMenuState::OnDeactivation()
{
	myMenu.Disable();
}
