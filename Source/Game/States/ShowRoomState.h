#pragma once
#include "State.h"
#include "..\EngineCore\EventSystem\EventReceiver.h"
#include "..\CommonUtilities\Vector3.h"

namespace sce
{
	namespace gfx
	{
		class CModelInstance;
		class CCameraInstance;
		class CEnvironmentalLight;
		class CDirectionalLight;
		class CText;
	};
};

class CShowRoomState: private CState, private sce::CEventReceiver
{
private:
	enum ECommands
	{
		eNone = 0 << 0
		, eRotateXClockWise = 1 << 0
		, eRotateXCClockWise = 1 << 1
		, eRotateYClockWise = 1 << 2
		, eRotateYCClockWise = 1 << 3
		, eRotateZClockWise = 1 << 4
		, eRotateZCClockWise = 1 << 5
		, eResetRotation = 1 << 6
		, eResetCamPosition = 1 << 7
		, eIncreaseRotationSpeed = 1 << 8
		, eDecreaseRotationSpeed = 1 << 9
		, eMoveCamera = 1 << 10
		, eExitState = 1 << 11
	};
	enum class EMoveMode: unsigned char
	{
		eStatic
		, eFree
	};

	struct SModels
	{
		SModels(): myLODList(nullptr), myPathList(nullptr), myLODsCount(0), myPrevLODIndex(0), myCurrLODIndex(0) {}

		sce::gfx::CModelInstance* myLODList;
		std::string* myPathList;
		unsigned short myLODsCount;
		unsigned short myPrevLODIndex;
		unsigned short myCurrLODIndex;
	};

public:
	CShowRoomState(EManager& aEntityManager, const bool aRunInDebugMode = false);
	~CShowRoomState();

	void Init() override;
	void Update(const float aDeltaTime) override;
	void Render() override;

	void OnActivation() override;
	void OnDeactivation() override;
	void ReceiveEvent(const Event::SEvent& aEvent) override;

private:
	void ReorderModelsWhenReady();
	void MoveCameraToTargetPosition(const float aDeltaTime);
	void UpdateStates();
	void UpdateControls(const float aDeltaTime);
	void LoadAllModels();
	void GetEveryModelPathAndLatestModelIndex(CU::GrowingArray<CU::GrowingArray<std::string>>& aAllFilePathList,
		unsigned short& aLatestModModelIndexRef, unsigned short& aLatestModLODIndexRef);
	void DeleteAllModels();

private:
	SModels* myModelList;
	sce::gfx::CModelInstance* myCurrModelPtr;
	sce::gfx::CModelInstance* mySkyboxPtr;
	sce::gfx::CCameraInstance* myCamInstancePtr;
	sce::gfx::CEnvironmentalLight* myEnvLightPtr;
	sce::gfx::CDirectionalLight* myDirLightPtr;
	sce::gfx::CText* myModelDescTextPtr;
	CU::Vector3f myCamTargetPosition;
	CU::Vector3f myCurrObjRotation;
	CU::Vector3f myDeltaFromMouse;
	float myObjRotationSpeed;
	float myCamMoveSpeed;
	float myCamZoomSpeed;
	float myZoomDirectionFromMouse;
	unsigned int myCommandState;
	unsigned short myModelsCount;
	unsigned short myMaxModelsCount;
	unsigned short myPrevModelIndex;
	unsigned short myCurrModelIndex;
	EMoveMode myMode; // Requires a "player" in order to move around freely.
	bool myDoneReordering;
	const bool myRunInDebugMode;
};