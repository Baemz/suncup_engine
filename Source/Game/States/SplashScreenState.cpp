#include "stdafx.h"
#include "SplashScreenState.h"

#define MAX_SPLASH_SCREEN_DURATION (6.0f)
#define MAX_WAIT_FOR_LOGOS (0.5f)
#define FADE_IN_TIME_LIMIT(var) ((var) * 0.8f)
#define FADE_OUT_TIME_LIMIT(var) ((var) * 0.2f)

CSplashScreenState::CSplashScreenState(EManager& aEntityManager)
	: CState(aEntityManager)
	, myTimeLeftToPop(MAX_SPLASH_SCREEN_DURATION)
{
}

CSplashScreenState::~CSplashScreenState()
{
}

void CSplashScreenState::Init()
{
	myTGALogoSprite.Init("Data/Menu/Splash Screen/TGA_Logo.dds");
	myGroupLogoSprite.Init("Data/Menu/Splash Screen/startup_teamLogo.dds");
	myBackgroundSprite.Init("Data/Menu/Black_4x4.dds");

	myTGALogoSprite.SetAlpha(0.0f);
	myGroupLogoSprite.SetAlpha(0.0f);
	myBackgroundSprite.SetAlpha(1.0f);

	myTGALogoSprite.SetPivot({ 0.5f });
	myGroupLogoSprite.SetPivot({ 0.5f });
	myBackgroundSprite.SetPivot({ 0.5f });

	myTGALogoSprite.SetPosition({ 0.5f });
	myGroupLogoSprite.SetPosition({ 0.5f });
	myBackgroundSprite.SetPosition({ 0.5f });

	myTGALogoSprite.SetScale({ 1.0f });
	myGroupLogoSprite.SetScale({ 1.0f });
	myBackgroundSprite.SetScale({ 1000.0f });
}

void CSplashScreenState::Update(const float aDeltaTime)
{
	myTimeLeftToPop -= aDeltaTime;

	if (myTimeLeftToPop <= MAX_SPLASH_SCREEN_DURATION - MAX_WAIT_FOR_LOGOS)
	{
		if (myTimeLeftToPop >= (MAX_SPLASH_SCREEN_DURATION - MAX_WAIT_FOR_LOGOS) * 0.5f)
		{
			myTGALogoSprite.SetAlpha(GetAlpha((MAX_SPLASH_SCREEN_DURATION - MAX_WAIT_FOR_LOGOS) * 0.5f, MAX_SPLASH_SCREEN_DURATION - MAX_WAIT_FOR_LOGOS));
		}
		else if (myTimeLeftToPop > 0.0f)
		{
			myGroupLogoSprite.SetAlpha(GetAlpha(0.0f, (MAX_SPLASH_SCREEN_DURATION - MAX_WAIT_FOR_LOGOS) * 0.5f));
		}
	}

	if (myTimeLeftToPop <= 0.0f) myState = EStateCommand::eRemoveState;
}

void CSplashScreenState::Render()
{
	myBackgroundSprite.Render();
	if (myTGALogoSprite.GetAlpha() > 0.0f) myTGALogoSprite.Render();
	if (myGroupLogoSprite.GetAlpha() > 0.0f) myGroupLogoSprite.Render();
}

void CSplashScreenState::OnActivation()
{
}

void CSplashScreenState::OnDeactivation()
{
}

/* PRIVATE FUNCTIONS */

const float CSplashScreenState::GetAlpha(const float aEndTime, const float aStartTime) const
{
	const float timeDifference(aStartTime - aEndTime);
	float returnValue(1.0f);

	if (myTimeLeftToPop >= FADE_IN_TIME_LIMIT(aStartTime))
	{
		const float calculatedAlpha((myTimeLeftToPop - FADE_IN_TIME_LIMIT(aStartTime)) / (aStartTime - FADE_IN_TIME_LIMIT(aStartTime)));
		returnValue = 1.0f - calculatedAlpha;
	}
	else if (myTimeLeftToPop <= FADE_OUT_TIME_LIMIT(timeDifference) + aEndTime)
	{
		const float calculatedAlpha((myTimeLeftToPop - aEndTime) / (FADE_OUT_TIME_LIMIT(timeDifference) + aEndTime - aEndTime));
		returnValue = calculatedAlpha;
	}

	return returnValue;
}
