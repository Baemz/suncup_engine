#pragma once

class CScriptEventManager;

class CScriptEventManager_EntityState
{
public:
	static bool InitFunctions(CScriptEventManager& aScriptEventManager);

private:
	CScriptEventManager_EntityState() = delete;
	~CScriptEventManager_EntityState() = delete;

};
