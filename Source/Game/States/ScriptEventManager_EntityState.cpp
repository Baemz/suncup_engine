#include "stdafx.h"
#include "ScriptEventManager_EntityState.h"
#include "ScriptEventManager.h"
#include "GameSystems\ExposedFunctions\GameLogicFunctions.h"

#define CHECK_IF_STATE_IS_REMOVED(aStateIndex, aReturnValue)																\
std::shared_lock<std::shared_mutex> sharedLock(aScriptEventManager.myStatesToRemoveMutex);									\
if (aScriptEventManager.myStatesToRemove.Find(aStateIndex) != CU::GrowingArray<CScriptManager::StateIndexType>::FoundNone)	\
return aReturnValue;


bool CScriptEventManager_EntityState::InitFunctions(CScriptEventManager & aScriptEventManager)
{
	CScriptManager& myScriptManager(aScriptEventManager.myScriptManager);

	std::function<void(UniqueIndexType)> killBind(
		[&aScriptEventManager](UniqueIndexType aEntityUniqueIndex)
	{
		aScriptEventManager.KillEntity(aEntityUniqueIndex);
	});
	myScriptManager.RegisterFunction("KillEntity", killBind
		, "Number"
		, "Removes the entity with the specified [ID].", true);

	std::function<void(const CScriptManager::StateIndexType)> setToDestroyOnLeaveGridBind(
		[&aScriptEventManager](const CScriptManager::StateIndexType aStateIndex)
	{
		CHECK_IF_STATE_IS_REMOVED(aStateIndex, ;);

		auto additionalRemoveCodeCallback(
			[&aScriptEventManager, aStateIndex]()
		{
			std::unique_lock<std::shared_mutex> uniqueCallbacksLock(aScriptEventManager.myCallbacksMutex, std::defer_lock);
			std::unique_lock<std::shared_mutex> uniqueRemovedLock(aScriptEventManager.myStatesToRemoveMutex, std::defer_lock);
			std::lock(uniqueCallbacksLock, uniqueRemovedLock);

			auto entityIterator(std::find_if(aScriptEventManager.myEntityCallbacks.begin(), aScriptEventManager.myEntityCallbacks.end()
				, [aStateIndex](const CScriptEventManager::SCallbackKey& aCallbackKey)
			{
				return aCallbackKey.CheckIfStateIndex(aStateIndex);
			}));

			if (entityIterator != aScriptEventManager.myEntityCallbacks.end())
			{
				CScriptManager::Get()->QueueRemoveState(entityIterator->myStateIndex);
				aScriptEventManager.myStatesToRemove.Add(entityIterator->myStateIndex);
				aScriptEventManager.myEntityCallbacks.erase(entityIterator);
			}
		});

		CGameLogicFunctions::SetToDestroyOnLeaveGrid(aScriptEventManager.GetEntityUniqueIndex(aStateIndex), additionalRemoveCodeCallback);
	});
	myScriptManager.RegisterFunctionCallWithStateIndex("SetToDestroyOnLeaveGrid", setToDestroyOnLeaveGridBind
		, ""
		, "Prevents crashes; if &aScriptEventManager object leaves the level's grid: destroy it.");

	std::function<void()> respawnBind(
		[&aScriptEventManager]()
	{
		CGameLogicFunctions::Respawn();
	});
	myScriptManager.RegisterFunction("Respawn", respawnBind
		, ""
		, "Resets the player at the last visited checkpoint with full health.", false);

	std::function<void(UniqueIndexType)> setCheckpointBind(
		[&aScriptEventManager](UniqueIndexType aCheckpointIndex)
	{
		CGameLogicFunctions::SetCurrentCheckpoint(aCheckpointIndex);
	});
	myScriptManager.RegisterFunction("SetCurrentCheckpoint", setCheckpointBind
		, "Number"
		, "Sets the player's spawn point to the checkpoint object with [ID]", true);

	std::function<void(const CScriptManager::StateIndexType, float)> takeDamageBind(
		[&aScriptEventManager](const CScriptManager::StateIndexType aStateIndex, float aDamage)
	{
		CHECK_IF_STATE_IS_REMOVED(aStateIndex, ;);
		CGameLogicFunctions::TakeDamage(aScriptEventManager.GetEntityUniqueIndex(aStateIndex), aDamage);
	});
	myScriptManager.RegisterFunctionCallWithStateIndex("TakeDamage", takeDamageBind
		, "Number"
		, "The entity takes [amount] of damage", false);

	std::function<float(UniqueIndexType)> getHealthOfEntityBind(
		[&aScriptEventManager](UniqueIndexType aEntityUniqueIndex)
	{
		return CGameLogicFunctions::GetHealth(aEntityUniqueIndex);
	});
	myScriptManager.RegisterFunction("GetHealthOfEntity", getHealthOfEntityBind
		, "Number"
		, "Returns the health amount of entity with [ID]", true);

	std::function<float(UniqueIndexType)> getMaxHealthOfEntityBind(
		[&aScriptEventManager](UniqueIndexType aEntityUniqueIndex) -> float
	{
		return CGameLogicFunctions::GetMaxHealth(aEntityUniqueIndex);
	});
	myScriptManager.RegisterFunction("GetMaxHealthOfEntity", getMaxHealthOfEntityBind
		, "Number"
		, "Returns the max health amount of entity with [ID]", true);

	std::function<float(const CScriptManager::StateIndexType)> getHealthBind(
		[&aScriptEventManager](const CScriptManager::StateIndexType aStateIndex)
	{
		CHECK_IF_STATE_IS_REMOVED(aStateIndex, -1.0f);
		return CGameLogicFunctions::GetHealth(aScriptEventManager.GetEntityUniqueIndex(aStateIndex));
	});
	myScriptManager.RegisterFunctionCallWithStateIndex("GetHealth", getHealthBind
		, ""
		, "Returns the health amount of the entity", false);

	std::function<void(UniqueIndexType, float)> setHealthOfEntityBind(
		[&aScriptEventManager](UniqueIndexType aEntityUniqueIndex, float aHealth)
	{
		CGameLogicFunctions::SetHealth(aEntityUniqueIndex, aHealth);
	});
	myScriptManager.RegisterFunction("SetHealthOfEntity", setHealthOfEntityBind
		, "Number, Number"
		, "Sets the health [amount] of entity with [ID]", true);

	std::function<void(const CScriptManager::StateIndexType, float)> setHealthBind(
		[&aScriptEventManager](const CScriptManager::StateIndexType aStateIndex, float aHealth)
	{
		CHECK_IF_STATE_IS_REMOVED(aStateIndex, ;);
		CGameLogicFunctions::SetHealth(aScriptEventManager.GetEntityUniqueIndex(aStateIndex), aHealth);
	});
	myScriptManager.RegisterFunctionCallWithStateIndex("SetHealth", setHealthBind
		, "Number"
		, "Sets the health [amount] of the entity", false);

	std::function<void(const CScriptManager::StateIndexType, float)> setMaxHealthBind(
		[&aScriptEventManager](const CScriptManager::StateIndexType aStateIndex, float aMaxHealth)
	{
		CHECK_IF_STATE_IS_REMOVED(aStateIndex, ;);
		CGameLogicFunctions::SetMaxHealth(aScriptEventManager.GetEntityUniqueIndex(aStateIndex), aMaxHealth);
	});
	myScriptManager.RegisterFunctionCallWithStateIndex("SetMaxHealth", setMaxHealthBind
		, "Number"
		, "Sets the maximum health [amount] of the entity", false);

	std::function<UniqueIndexType(UniqueIndexType, float)> getClosestEnemyBind(
		[&aScriptEventManager](UniqueIndexType aEntity, float aRadius) -> UniqueIndexType
	{
		return CGameLogicFunctions::GetClosestEnemy(aEntity, aRadius);
	});
	myScriptManager.RegisterFunction("GetClosestEnemy", getClosestEnemyBind
		, ""
		, "Returns the closest enemy [ID] within the Radius provided, returns -1 if no enemy is within radius");

	std::function<UniqueIndexType(UniqueIndexType, float)> getClosestPaintingBind(
		[&aScriptEventManager](UniqueIndexType aEntity, float aRadius) -> UniqueIndexType
	{
		return CGameLogicFunctions::GetClosestPainting(aEntity, aRadius);
	});
	myScriptManager.RegisterFunction("GetClosestPainting", getClosestPaintingBind
		, ""
		, "Returns the closest painting [ID] within the Radius provided, returns -1 if no painting is within radius");

	std::function<UniqueIndexType()> getInvalidEntityIndexBind(
		[&aScriptEventManager]() -> UniqueIndexType
	{
		return CGameLogicFunctions::GetInvalidEntityIndex();
	});
	myScriptManager.RegisterFunction("GetInvalidEntityIndex", getInvalidEntityIndexBind
		, ""
		, "Returns an Invalid Entity Index");

	std::function<bool(UniqueIndexType)> isAliveBind(
		[&aScriptEventManager](UniqueIndexType aEntity) -> bool
	{
		return CGameLogicFunctions::IsAlive(aEntity);
	});
	myScriptManager.RegisterFunction("IsAlive", isAliveBind
		, ""
		, "Returns True if [ID] is alive, false if it's not");

	std::function<void(const UniqueIndexType, const float, const float, const float)> spawnEnemyBind(
		[&aScriptEventManager](const UniqueIndexType aPaintingIndex, const float aX, const float aY, const float aZ)
	{
		return CGameLogicFunctions::SpawnEnemy(aPaintingIndex, { aX, aY, aZ });
	});
	myScriptManager.RegisterFunction("SpawnEnemy", spawnEnemyBind
		, "PaintingIndex, PositionX, PositionY, PositionZ"
		, "Spawns Enemy at position [PositionX][PositionY][PositionZ]");

	std::function<void(const float, const float, const float, const float, const float, const float)> spawnPaintingBind(
		[&aScriptEventManager](const float aX, const float aY, const float aZ, const float aRotX, const float aRotY, const float aRotZ)
	{
		return CGameLogicFunctions::SpawnPainting({ aX, aY, aZ }, { aRotX, aRotY, aRotZ });
	});
	myScriptManager.RegisterFunction("SpawnPainting", spawnPaintingBind
		, "PositionX, PositionY, PositionZ, RotationX, RotationY, RotationZ"
		, "Spawns Painting at position [PositionX][PositionY][PositionZ] with rotation [RotationX][RotationY][RotationZ]");

	std::function<void(const UniqueIndexType)> rotateBridgeBind(
		[&aScriptEventManager](const UniqueIndexType aBridgeIndex)
	{
		return CGameLogicFunctions::RotateBridge(aBridgeIndex);
	});
	myScriptManager.RegisterFunction("RotateBridge", rotateBridgeBind
		, "EntityIndex"
		, "Rotates the Entity -90 in X-axis");

	std::function<CU::Vector4f(const UniqueIndexType)> bridgeGetStartBind(
		[&aScriptEventManager](const UniqueIndexType aBridgeIndex) -> CU::Vector4f
	{
		return CGameLogicFunctions::BridgeGetStart(aBridgeIndex);
	});
	myScriptManager.RegisterFunction("BridgeGetStart", bridgeGetStartBind
		, "EntityIndex"
		, "Get start rotation for the bridge");

	std::function<CU::Vector4f(const UniqueIndexType)> bridgeGetEndBind(
		[&aScriptEventManager](const UniqueIndexType aBridgeIndex) -> CU::Vector4f
	{
		return CGameLogicFunctions::BridgeGetEnd(aBridgeIndex);
	});
	myScriptManager.RegisterFunction("BridgeGetEnd", bridgeGetEndBind
		, "EntityIndex"
		, "Get end rotation for the bridge");

	std::function<void(const UniqueIndexType, const float, const float, const float, const float, const float, const float, const float, const float, const float)> bridgeSlerpBind(
		[&aScriptEventManager](const UniqueIndexType aBridgeIndex, const float aQuatStartX, const float aQuatStartY, const float aQuatStartZ, const float aQuatStartW,
			const float aQuatEndX, const float aQuatEndY, const float aQuatEndZ, const float aQuatEndW, const float aSlerpProgress)
	{
		CU::Quaternion startQuat(aQuatStartX, aQuatStartY, aQuatStartZ, aQuatStartW);
		CU::Quaternion endQuat(aQuatEndX, aQuatEndY, aQuatEndZ, aQuatEndW);
		return CGameLogicFunctions::BridgeSlerp(aBridgeIndex, startQuat, endQuat, aSlerpProgress);
	});
	myScriptManager.RegisterFunction("BridgeSlerp", bridgeSlerpBind
		, "EntityIndex, QuatStartX, QuatStartY, QuatStartZ, QuatStartW, QuatEndX, QuatEndY, QuatEndZ, QuatEndW, Progress"
		, "Slerps entity from Start Quaternion to End Quaternion based on Progress");

	std::function<void(const CScriptManager::StateIndexType)> spawnProjectileDebrisBind(
		[&aScriptEventManager](const CScriptManager::StateIndexType aStateIndex)
	{
		const UniqueIndexType uniqueIndex(aScriptEventManager.GetEntityUniqueIndex(aStateIndex));
		// HACK
		if (uniqueIndex != EntityHandleINVALID.myUniqueIndex)
		{
			CGameLogicFunctions::SpawnProjectileDebris(uniqueIndex);
		}
	});
	myScriptManager.RegisterFunctionCallWithStateIndex("SpawnProjectileDebris", spawnProjectileDebrisBind
		, ""
		, "Spawns projectile debris at the callers position.");

	std::function<void()> goToCreditsBind(
		[&aScriptEventManager]()
	{
		CGameLogicFunctions::SetShouldGoToCredits(true);
	});

	myScriptManager.RegisterFunction("GoToCredits", goToCreditsBind,
		"",
		"Triggers a credits state that returns the player to the main menu");

	return true;
}
