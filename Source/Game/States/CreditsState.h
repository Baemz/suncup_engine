#pragma once
#include "State.h"
#include "../Menu/Menu.h"

class CCreditsState : private CState
{
public:
	CCreditsState(EManager& aEntityManager, const bool aRemoveMainState);
	~CCreditsState();

	void Init() override;
	void Update(const float aDeltaTime) override;
	void Render() override;

	void OnActivation() override;
	void OnDeactivation() override;

private:
	CMenu myMenu;
	bool myKeepMenu;
	bool myRemoveMainState;
};