#include "stdafx.h"
#include "CreditsState.h"

CCreditsState::CCreditsState(EManager& aEntityManager, const bool aRemoveMainState)
	: CState(aEntityManager)
	, myRemoveMainState(aRemoveMainState)
{
}

CCreditsState::~CCreditsState()
{
}

void CCreditsState::Init()
{
	myMenu.AddGraphic("Data/Menu/Credits/creditsMenu_names.dds", { 0.2333f, 0.0f });
}

void CCreditsState::Update(const float aDeltaTime)
{
	myKeepMenu = myMenu.Update(aDeltaTime);

	if (myKeepMenu == false)
	{
		if (myRemoveMainState)
		{
			myState = CState::EStateCommand::eRemoveMainState;
		}
		else
		{
			myState = CState::EStateCommand::eRemoveState;
		}
	}
}

void CCreditsState::Render()
{
	myMenu.Render(false);
}

void CCreditsState::OnActivation()
{
	myMenu.Enable();
}

void CCreditsState::OnDeactivation()
{
	myMenu.Disable();
}
