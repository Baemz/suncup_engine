#include "stdafx.h"
#include "OptionMenuState.h"
#include "../AudioEngine/AudioManager.h"
#include "../GraphicsEngine/GraphicsEngineInterface.h"

#define MENU_POSITION CU::Vector2f(0.3667f, 0.0f)
#define GET_POS_WITH_MENU_POS(aX, aY) (MENU_POSITION + CU::Vector2f(aX, aY))

static float globalCurrMasterVolume = 0.5f;
static float globalPrevMasterVolume = 0.5f;

static float globalCurrSFXVolume = 0.5f;
static float globalPrevSFXVolume = 0.5f;

typedef sce::gfx::CGraphicsEngineInterface GEI;

COptionMenuState::COptionMenuState(EManager& aEntityManager)
	: CState(aEntityManager)
	, myKeepMenu(false)
{
}

void COptionMenuState::Init()
{
	{
		std::function<void()> hoverSoundPlay(
			[this]()
		{
			CAudioManager::PostEventAndGetID("Button_Hover", 1);
		});

		myMenu.SetButtonOnEnterSoundFunc(hoverSoundPlay);
	}

	//myMenu.AddGraphic("Data/Menu/Black_4x4.dds", { 0.0f }, { 1000.0f });
	myMenu.AddGraphic("Data/Menu/Options/optionsMenu_signBackground.dds", MENU_POSITION);

	{
		MenuComponents::SButton sliderButton;
		sliderButton.myName = "volume";
		sliderButton.myCollisionSize = { 0.625f, 0.90625f };
		sliderButton.myIdleSprite.Init("Data/Menu/Options/optionsMenu_sliderButton.dds");
		sliderButton.myIdleSprite.SetPivot({ -0.25f, 0.25f });
		sliderButton.myHoverSprite.Init("Data/Menu/Options/optionsMenu_sliderButton_toggleOn.dds");
		sliderButton.myHoverSprite.SetPivot({ -0.25f, 0.25f });

		myMenu.AddSlider(
			"Data/Menu/Options/optionsMenu_sliderLine.dds",
			"Data/Menu/Options/optionsMenu_sliderLine.dds",
			"master_volume",
			sliderButton,
			GET_POS_WITH_MENU_POS(0.055f, 0.360f),
			{ 0.515625f, 1.0f, 0.0f }
		);

		myMenu.AddSlider(
			"Data/Menu/Options/optionsMenu_sliderLine.dds",
			"Data/Menu/Options/optionsMenu_sliderLine.dds",
			"sfx_volume",
			sliderButton,
			GET_POS_WITH_MENU_POS(0.055f, 0.440f),
			{ 0.515625f, 1.0f, 0.0f }
		);

		std::function<void()> sliderSoundPlay(
			[this]()
		{
			CAudioManager::PostEventAndGetID("Slide_Move", 1);
		});

		myMenu.SetSliderMoveSoundFunc(sliderSoundPlay);
		myMenu.SetSliderMoveSoundPlayTime(0.1f);

		CAudioManager::SetRTPC("Master_Volume", globalCurrMasterVolume * 100.0f);
		CAudioManager::SetRTPC("SFX_Volume", globalCurrSFXVolume * 100.0f);

		myMenu.SetSliderValue("master_volume", globalCurrMasterVolume);
		myMenu.SetSliderValue("sfx_volume", globalCurrSFXVolume);
	}

	AddAllSFXCheckBoxes();
}

void COptionMenuState::Update(const float aDeltaTime)
{
	myKeepMenu = myMenu.Update(aDeltaTime);

	if (myKeepMenu == false)
	{
		myState = CState::EStateCommand::eRemoveState;
	}
	else
	{
		globalPrevMasterVolume = globalCurrMasterVolume;
		globalCurrMasterVolume = myMenu.GetSliderValue("master_volume");

		globalPrevSFXVolume = globalCurrSFXVolume;
		globalCurrSFXVolume = myMenu.GetSliderValue("sfx_volume");

		if (globalCurrMasterVolume != globalPrevMasterVolume)
		{
			CAudioManager::SetRTPC("Master_Volume", globalCurrMasterVolume * 100.0f);
		}

		if (globalCurrSFXVolume != globalPrevSFXVolume)
		{
			CAudioManager::SetRTPC("SFX_Volume", globalCurrSFXVolume * 100.0f);
		}

		UpdateSpecialEffectsState();
	}
}

void COptionMenuState::Render()
{
	myMenu.Render(false);
}

void COptionMenuState::OnActivation()
{
	myMenu.Enable();
}

void COptionMenuState::OnDeactivation()
{
	myMenu.Disable();
}

/* PRIVATE FUNCTIONS */

void COptionMenuState::AddAllSFXCheckBoxes()
{
	myMenu.AddCheckBox(
		"Data/Menu/Options/optionsMenu_noHoverOff.dds",
		"Data/Menu/Options/optionsMenu_hoverOff.dds",
		"Data/Menu/Options/optionsMenu_noHoverOn.dds",
		"Data/Menu/Options/optionsMenu_hoverOn.dds",
		"bloom",
		GET_POS_WITH_MENU_POS(0.129f, 0.475f),
		{ 0.90625f }
	);

	myMenu.AddCheckBox(
		"Data/Menu/Options/optionsMenu_noHoverOff.dds",
		"Data/Menu/Options/optionsMenu_hoverOff.dds",
		"Data/Menu/Options/optionsMenu_noHoverOn.dds",
		"Data/Menu/Options/optionsMenu_hoverOn.dds",
		"ssao",
		GET_POS_WITH_MENU_POS(0.113f, 0.547f),
		{ 0.90625f }
	);

	myMenu.AddCheckBox(
		"Data/Menu/Options/optionsMenu_noHoverOff.dds",
		"Data/Menu/Options/optionsMenu_hoverOff.dds",
		"Data/Menu/Options/optionsMenu_noHoverOn.dds",
		"Data/Menu/Options/optionsMenu_hoverOn.dds",
		"fxaa",
		GET_POS_WITH_MENU_POS(0.119f, 0.620f),
		{ 0.90625f }
	);

	myMenu.AddCheckBox(
		"Data/Menu/Options/optionsMenu_noHoverOff.dds",
		"Data/Menu/Options/optionsMenu_hoverOff.dds",
		"Data/Menu/Options/optionsMenu_noHoverOn.dds",
		"Data/Menu/Options/optionsMenu_hoverOn.dds",
		"vsync",
		GET_POS_WITH_MENU_POS(0.135f, 0.695f),
		{ 0.90625f }
	);

	if (GEI::IsBloomEnabled())
	{
		myMenu.SelectCheckBox("bloom");
	}
	
	if (GEI::IsSSAOEnabled())
	{
		myMenu.SelectCheckBox("ssao");
	}

	if (GEI::IsFXAAEnabled())
	{
		myMenu.SelectCheckBox("fxaa");
	}

	if (GEI::IsVSyncEnabled())
	{
		myMenu.SelectCheckBox("vsync");
	}
}

void COptionMenuState::UpdateSpecialEffectsState() const
{
	const bool bloomPressed(myMenu.CheckPressedCheckBox("bloom"));
	const bool ssaoPressed(myMenu.CheckPressedCheckBox("ssao"));
	const bool fxaaPressed(myMenu.CheckPressedCheckBox("fxaa"));
	const bool vsyncPressed(myMenu.CheckPressedCheckBox("vsync"));

	if (bloomPressed || ssaoPressed || fxaaPressed || vsyncPressed)
	{
		CAudioManager::PostEventAndGetID("Button_Pressed_Short", 1);

		if (bloomPressed)
		{
			GEI::ToggleBloom();
		}

		if (ssaoPressed)
		{
			GEI::ToggleSSAO();
		}

		if (fxaaPressed)
		{
			GEI::ToggleFXAA();
		}

		if (vsyncPressed)
		{
			GEI::ToggleVSync();
		}
	}
}
