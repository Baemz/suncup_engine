#include "stdafx.h"
#include "InGameState.h"
#include "../EngineCore/CommandLineManager/CommandLineManager.h"
#include "../EngineCore/WorkerPool/WorkerPool.h"
#include "../GraphicsEngine/Camera/CameraFactory.h"
#include "../GraphicsEngine/Model/Loading/ModelFactory.h"
#include "../GraphicsEngine/DirectXFramework/Direct3D11.h"
#include "../GraphicsEngine/DebugTools.h"

#include "EntitySystemLink/EntitySystemLink.h"
#include "GameSystems/RenderSystem.h"
#include "GameSystems/ControllerSystem.h"
#include "GameSystems/GameSystems.h"
#include "GameSystems/MovementSystem.h"
#include "GameSystems/AISystem.h"

#include "EntityFactory.h"

#include "../Script/ScriptManager.h"
#include "../AudioEngine/AudioManager.h"

#include "../CommonUtilities/Random.h"
#include "../GraphicsEngine/GraphicsEngineInterface.h"
#include "ConsoleState.h"
#include "StateStack.h"
#include "../EngineCore/CommandLineManager/CommandLineManager.h"
#include <codecvt>
#include "GameSystems/ExposedFunctions/GameLogicFunctions.h"
#include "GameSystems/ScriptSystem.h"

#include "GameSystems/ExposedFunctions/GameLogicFunctions.h" // Binds navigation mesh.
#include "AudioWrapper.h"
#include "MouseInterface.h"
#include "HealingCircleSystem.h"
#include "DamageZoneSystem.h"
#include "CreditsState.h"
#include "PauseMenuState.h"

CInGameState::CInGameState(EManager& aEntityManager, const char* aLevelPath)
	: CState(aEntityManager)
	, myLevelPath((aLevelPath == nullptr) ? "" : aLevelPath)
	, myShouldRenderFrustumColliders(false)
	, myUseMouseLook(false)
{
	myEntities.Reserve(EntityCount);
	CGameLogicFunctions::SetShouldGoToCredits(false);
}

CInGameState::~CInGameState()
{
	for (auto& currentEvent : myEvents)
	{
		sce_delete(currentEvent);
	}

	myEntityManager.Kill(myCamera);

	myLevel.Destroy();

	CMouseInterface::Destroy();
	while (sce::CWorkerPool::GetHasWorkToDo())
	{
		std::this_thread::sleep_for(std::chrono::milliseconds(1));
	}
}

void CInGameState::Init()
{
	if (sce::CCommandLineManager::HasParameter(L"-showroom"))
	{
	}
	else
	{
		sce::gfx::CGraphicsEngineInterface::EnableCursor();

		//CEntityFactory::CreateEntity(EEntityType::EnemyCube);

		std::string levelPath("NO_MAP_OMG, use command args");
		if (myLevelPath.empty())
		{
			if (sce::CCommandLineManager::HasParameter(L"-level"))
			{
				auto levels(sce::CCommandLineManager::GetArguments(L"-level"));
				if (levels.Empty() == false)
				{
					levelPath = std::wstring_convert<std::codecvt_utf8<wchar_t>, wchar_t>().to_bytes(levels[0]);
				}
			}
			else
			{
				levelPath = "data/levels/blockout03.dataMap";
			}
		}
		else
		{
			levelPath = myLevelPath;
		}

		myCameraEulerRotation.x;// = CU::Pif * 0.30f;
		myCameraEulerRotation.y;// = CU::Pif * 0.30f;

		myCamera = CEntityFactory::CreateEntityCamera({ 5.0f, 5.0f, 0.0f }, myCameraEulerRotation);
		
		myCameraProjection = &myEntityManager.GetComponent<CompCameraInstance>(myEntityManager.GetEntityIndex(myCamera)).myInstance.GetProjection();

		myLevel.Init(levelPath.c_str());
		CGameLogicFunctions::SetLevel(&myLevel);


		//myCameraEulerRotation.x = CU::Pif * 0.5f;
		//myCamera = CEntityFactory::CreateEntityCamera({ 0.0f, 25.0f, 0.0f }, myCameraEulerRotation);

		//CEntityFactory::SEntityCreationData data;
		//data.myParticleData.myIsValid = true;
		//data.myParticleData.myParticlePath = 

		CGameSystems::Get()->GetSystem<CRenderSystem>()->ActivateCanvasFade(4.0f);

		CMouseInterface::Create(&myEntityManager, myCamera, myCameraProjection);
		myEntityManager.Refresh();
	}
}

void CInGameState::Update(const float aDeltaTime)
{
	myDeltaTime = aDeltaTime;

	std::string newLevelPath;
	if (myLevel.WantToSwitchLevel(newLevelPath) == true)
	{
		myLevel.Init(newLevelPath.c_str());
	}

	if ((myOnLevelLoadedCallback != nullptr) && !(CGameSystems::Get()->GetSystem<CRenderSystem>()->IsFading()))
	{
		myOnLevelLoadedCallback();
		myOnLevelLoadedCallback = nullptr;
	}

	myRefreshEntitiesTimer += myDeltaTime;
	myHasGottenEntitiesThisFrame = true;
	// HACK
	myRefreshEntitiesTimer = myRefreshEntitesFrequency;
	if (myRefreshEntitiesTimer >= myRefreshEntitesFrequency)
	{
		GetEntitiesToUse();
		//Does not sync up! Don't activate unless this function becomes a problem.
		//myRefreshEntitiesTimer = 0.0f;
	}
	CGameLogicFunctions::SetEntities(myEntities);

	myLevel.Update(aDeltaTime);

	ExecuteEvents();

	if (CGameLogicFunctions::GetShouldGoToCredits())
	{
		CStateStack::PushUnderStateOnTop(this, sce_new(CCreditsState(myEntityManager, true)), false);
	}

	CGameSystems::Get()->QueueUpdateEntityUniqueIndices(&myEntities);
}

void CInGameState::Render()
{
	if (myLevel.IsLoading() == false)
	{
		myLevel.GetNavMesh().Render();
	}

	if (myHasGottenEntitiesThisFrame == false)
	{
		myHasGottenEntitiesThisFrame = true;
		GetEntitiesToUse();
		CGameLogicFunctions::SetEntities(myEntities);
	}

	CGameSystems::Get()->QueueRenderEntityUniqueIndices(&myEntities);
	CGameSystems::Get()->QueuePostUpdateFunction(
		[&myEntities = myEntities, &myLevel = myLevel]() -> bool
	{
		return myLevel.PostUpdate();
	});

	if (myShouldRenderFrustumColliders)
	{
		sce::gfx::CDebugTools::Get()->DrawLinePlane(myFrustumViewAABB, { 1.0f, 0.0f, 0.0f });

		for (char i = 0; i < 4; ++i)
		{
			sce::gfx::CDebugTools::Get()->DrawCube(myFrustumIntersections[i], 5.0f, { 0.25f * i, 0.0f, 0.0f });
		}
	}
	myHasGottenEntitiesThisFrame = false;
}

void CInGameState::OnActivation()
{
	AttachToEventReceiving(Event::ESubscribedEvents(Event::eKeyInput | Event::eRawMouseInput));

	myRefreshEntitiesTimer = 0.0f;
	myShouldRenderFrustumColliders = false;

	myOnLevelLoadedCallback = 
		[this]()
	{
		CGameSystems::Get()->ActivateSystem<CControllerSystem>(myCamera, myLevel.GetNavMesh());
		CGameSystems::Get()->ActivateSystem<CMovementSystem>();
		CGameSystems::Get()->ActivateSystem<CAISystem>();
		CGameSystems::Get()->ActivateSystem<CHealingCircleSystem>();
		CGameSystems::Get()->ActivateSystem<CDamageZoneSystem>();
		CScriptSystem::SetIsActive(true);
	};

	CAudioManager::PostEventAndGetID("Combat_Is_Off", 1);
	CAudioManager::PostEventAndGetID("Play_InGame_Music", 1);
}

void CInGameState::OnDeactivation()
{
	DetachFromEventReceiving();

	myOnLevelLoadedCallback = nullptr;

	CGameSystems::Get()->RemoveQueuedUpdateEntities(&myEntities);
	CGameSystems::Get()->DeactivateSystem<CControllerSystem>();
	CGameSystems::Get()->DeactivateSystem<CMovementSystem>();
	CGameSystems::Get()->DeactivateSystem<CAISystem>();
	CGameSystems::Get()->DeactivateSystem<CHealingCircleSystem>();
	CGameSystems::Get()->DeactivateSystem<CDamageZoneSystem>();
	CScriptSystem::SetIsActive(false);

	CAudioManager::PostEventAndGetID("Stop_InGame_Music", 1);
}

void CInGameState::ReceiveEvent(const Event::SEvent& aEvent)
{
	Event::SEvent* newEvent(nullptr);

	if (myState == CState::EStateCommand::eActive)
	{
		if (aEvent.myType == Event::EEventType::eKeyInput)
		{
			newEvent = sce_new(Event::SKeyEvent(*reinterpret_cast<const Event::SKeyEvent*>(&aEvent)));
		}
		else if (aEvent.myType == Event::EEventType::eRawMouseInput)
		{
			newEvent = sce_new(Event::SRawMouseMoveEvent(*reinterpret_cast<const Event::SRawMouseMoveEvent*>(&aEvent)));
		}
		else
		{
			assert("Add your thing" && (false));
		}
	}
	
	if (newEvent != nullptr)
	{
		myEvents.Add(newEvent);
	}
}

void CInGameState::ExecuteEvents()
{
	for (auto& currentEvent : myEvents)
	{
		const auto& event(*currentEvent);

		if (event.myType == Event::EEventType::eKeyInput)
		{
			const Event::SKeyEvent* keyMsgPtr = reinterpret_cast<const Event::SKeyEvent*>(&event);

			if (keyMsgPtr->key == CU::CKeyBinds::GetHotKeys().myPause && keyMsgPtr->state == EButtonState::Pressed)
			{
				CStateStack::PushUnderStateOnTop(this, sce_new(CPauseMenuState(myEntityManager)), true);
			}

#ifndef _RETAIL
			if (keyMsgPtr->key == CU::CKeyBinds::GetHotKeys().myF1 && keyMsgPtr->state == EButtonState::Pressed)
			{
				sce::gfx::CDirect3D11::GetAPI()->SetShouldRenderWireFrame();
			}
			else if (keyMsgPtr->key == CU::CKeyBinds::GetHotKeys().myF2 && keyMsgPtr->state == EButtonState::Pressed)
			{
				myLevel.GetNavMesh().ToggleShouldRender();
			}
			else if (keyMsgPtr->key == CU::CKeyBinds::GetHotKeys().myF4 && keyMsgPtr->state == EButtonState::Pressed)
			{
				myLevel.ToggleShouldRenderQuadTreeNodes();
			}
			else if (keyMsgPtr->key == CU::CKeyBinds::GetHotKeys().myF5 && keyMsgPtr->state == EButtonState::Pressed)
			{
				myLevel.ToggleShouldRenderGrid();
			}
			else if (keyMsgPtr->key == CU::CKeyBinds::GetHotKeys().myF6 && keyMsgPtr->state == EButtonState::Pressed)
			{
				myShouldRenderFrustumColliders = !myShouldRenderFrustumColliders;
			}
			else if (keyMsgPtr->key == CU::CKeyBinds::GetHotKeys().myF7 && keyMsgPtr->state == EButtonState::Pressed)
			{
				CGameSystems::Get()->GetSystem<CRenderSystem>()->ToggleShouldRenderObjectSpheres();
			}
			else if (keyMsgPtr->key == CU::CKeyBinds::GetHotKeys().myF8 && keyMsgPtr->state == EButtonState::Pressed)
			{
				CGameSystems::Get()->GetSystem<CRenderSystem>()->ToggleRenderTriggers();
			}
			else if (keyMsgPtr->key == CU::CKeyBinds::GetHotKeys().myTilde && keyMsgPtr->state == EButtonState::Pressed)
			{
				CStateStack::PushUnderStateOnTop(this, sce_new(CConsoleState(myEntityManager)), true);
			}
			else if (keyMsgPtr->key == 'O' && keyMsgPtr->state == EButtonState::Pressed)
			{
				myUseMouseLook = !myUseMouseLook;
			}
#endif // !_RETAIL

			if (myUseMouseLook)
			{
				CU::Vector3f movement;
				CU::Vector3f rotation;

				bool switchCameraState(false);
				if (myEntityManager.GetComponent<CompCameraInstance>(myCamera).myFreeCamera == true)
				{
					if (keyMsgPtr->key == CU::CKeyBinds::GetHotKeys().myMove_UP && keyMsgPtr->state == EButtonState::Down)
					{
						movement.z = 1.0f;
					}

					if (keyMsgPtr->key == CU::CKeyBinds::GetHotKeys().myMove_DOWN && keyMsgPtr->state == EButtonState::Down)
					{
						movement.z = -1.0f;
					}

					if (keyMsgPtr->key == CU::CKeyBinds::GetHotKeys().myMove_RIGHT && keyMsgPtr->state == EButtonState::Down)
					{
						movement.x = 1.0f;
					}

					if (keyMsgPtr->key == CU::CKeyBinds::GetHotKeys().myMove_LEFT && keyMsgPtr->state == EButtonState::Down)
					{
						movement.x = -1.0f;
					}

					if (keyMsgPtr->key == CU::CKeyBinds::GetHotKeys().myShoot && keyMsgPtr->state == EButtonState::Down)
					{
						movement.y = 1.0f;
					}

					if (keyMsgPtr->key == CU::CKeyBinds::GetHotKeys().myBoost && keyMsgPtr->state == EButtonState::Down)
					{
						movement.y = -1.0f;
					}

					if (keyMsgPtr->key == CU::CKeyBinds::GetHotKeys().myRoll_RIGHT && keyMsgPtr->state == EButtonState::Down)
					{
						rotation.z = -0.5f;
					}

					if (keyMsgPtr->key == CU::CKeyBinds::GetHotKeys().myRoll_LEFT && keyMsgPtr->state == EButtonState::Down)
					{
						rotation.z = 0.5f;
					}

					if (keyMsgPtr->key == CU::CKeyBinds::GetHotKeys().myArrowUp && keyMsgPtr->state == EButtonState::Down)
					{
						rotation.x = -0.5f;
					}

					if (keyMsgPtr->key == CU::CKeyBinds::GetHotKeys().myArrowDown && keyMsgPtr->state == EButtonState::Down)
					{
						rotation.x = 0.5f;
					}

					if (keyMsgPtr->key == CU::CKeyBinds::GetHotKeys().myArrowRight && keyMsgPtr->state == EButtonState::Down)
					{
						rotation.y = 0.5f;
					}

					if (keyMsgPtr->key == CU::CKeyBinds::GetHotKeys().myArrowLeft && keyMsgPtr->state == EButtonState::Down)
					{
						rotation.y = -0.5f;
					}
				}
				auto moveCamera = [&movement, &rotation, &myDeltaTime = myDeltaTime, &switchCameraState](auto&, CompCameraInstance& aCameraComponent)
				{
					sce::gfx::CCameraInstance& instance(aCameraComponent.myInstance);
					float speed = 4;
					instance.Move(instance.GetRight(), speed * movement.x * myDeltaTime);
					instance.Move(instance.GetUp(), speed * movement.y * myDeltaTime);
					instance.Move(instance.GetLook(), speed * movement.z * myDeltaTime);

					if (switchCameraState)
					{
						aCameraComponent.myFreeCamera = !aCameraComponent.myFreeCamera;
					}
				};
				myEntityManager.RunIfMatching<SigCamera>(myCamera, moveCamera);
			}
		}
		else if (event.myType == Event::EEventType::eRawMouseInput)
		{
			if (myUseMouseLook)
			{
				constexpr float radiansPerMouseDelta(CU::Pif * 0.001f);
				const Event::SRawMouseMoveEvent* mouseMsg = reinterpret_cast<const Event::SRawMouseMoveEvent*>(&event);
				mouseMsg;

				myMouseMessage.deltaX = mouseMsg->x;
				myMouseMessage.deltaY = mouseMsg->y;

				const float deltaX = myMouseMessage.deltaX;
				const float deltaY = myMouseMessage.deltaY;

				if (deltaX != 0 || deltaY != 0)
				{
					const float maxVerticalFOVRadians = (CU::Pif / 180.0f) * (180 / 2.0f);
					myRotationScreenX += deltaX * radiansPerMouseDelta;
					myRotationScreenY += deltaY * radiansPerMouseDelta;

					myRotationScreenX = (myRotationScreenX > CU::Pif * 2.0f) ? 0.0f : (myRotationScreenX < 0.0f) ? CU::Pif * 2.0f : myRotationScreenX;
					myRotationScreenY = (myRotationScreenY > maxVerticalFOVRadians) ? maxVerticalFOVRadians : (myRotationScreenY < -maxVerticalFOVRadians) ? -maxVerticalFOVRadians : myRotationScreenY;

					//myAccumulatedScreenDeviation += deltaX * radiansPerMouseDelta;

					CU::Quaternion lookRotation(CU::Quaternion::CreateFromAxisAngle({ 1.0f, 0.0f, 0.0f }, myRotationScreenY));
					lookRotation = CU::Quaternion::CreateFromAxisAngle({ 0.0f, 1.0f, 0.0f }, myRotationScreenX) * lookRotation;

					auto rotateCamera = [&lookRotation](auto&, CompCameraInstance& aCameraComponent)
					{
						sce::gfx::CCameraInstance& instance(aCameraComponent.myInstance);
						instance.SetOrientation(lookRotation);
					};
					myEntityManager.RunIfMatching<SigCamera>(myCamera, rotateCamera);
				}
			}
		}

		sce_delete(currentEvent);
	}
	myEvents.RemoveAll();
}

bool CInGameState::RegisterFunctionsFunction()
{
	//CScriptManager& scriptManager(*CScriptManager::Get());

	//scriptManager.RegisterFunction("Print", PrintMsg,
	//	"Prints a message to the console window. \nTakes in only one parameter (can be a string, number or bool). Returns no values.");

	return true;
}

void CInGameState::GetEntitiesToUse()
{
	myEntities.RemoveAll();
	
	if (myLevel.FailedToLoadLevel())
	{
		return;
	}

#ifndef USE_GRID_CULLING
	myFrustumViewAABB = CU::Vector4f
	(
		-10000.0f,
		-10000.0f,
		10000.0f,
		10000.0f
	);

#else
	if (!myShouldRenderFrustumColliders)
	{
		const CU::Vector2f resolution = sce::gfx::CGraphicsEngineInterface::GetResolution();
		const CU::Matrix44f& camOrientation(myEntityManager.GetComponent<CompCameraInstance>(myEntityManager.GetEntityIndex(myCamera)).myInstance.GetOrientation());

		myFrustumIntersections.Resize(4);
		char counter = 0;
		for (signed char y = -1; y <= 1; y += 2)
		{
			for (signed char x = -1; x <= 1; x += 2)
			{
				CU::Vector2f projPos = { (x * 0.95f) / (*myCameraProjection)[0], (y * 0.95f) / (*myCameraProjection)[5] };

				CU::Collision::Ray ray;
				ray.myOrigin = { camOrientation[12], camOrientation[13], camOrientation[14] };
				ray.myDirection =
				{
					(projPos.x * camOrientation[0]) + (-projPos.y * camOrientation[4]) + camOrientation[8],
					(projPos.x * camOrientation[1]) + (-projPos.y * camOrientation[5]) + camOrientation[9],
					(projPos.x * camOrientation[2]) + (-projPos.y * camOrientation[6]) + camOrientation[10]
				};

				constexpr float levelMargin = 250.0f;
				CU::Vector4f levelGridAABB(myLevel.GetGridAABB());
				CU::Collision::AABB3D levelBox
				(
				{ myLevel.GetGridAABB().x1 - levelMargin, -1.0f, myLevel.GetGridAABB().y1 - levelMargin },
				{ myLevel.GetGridAABB().x2 + levelMargin, 1.0f, myLevel.GetGridAABB().y2 + levelMargin }
				);

				if (!CU::Collision::IntersectionRayPlane(ray, CU::Collision::Plane({ 0.0f }, { 0.0f, -1.0f, 0.0f }), myFrustumIntersections[counter]))
				{
					printf("Frustum collision %i does not collide with world plane.\n", counter);
				}
				counter++;
			}
		}

		myFrustumViewAABB = CU::Vector4f(
			std::numeric_limits<float>::max()
			, std::numeric_limits<float>::max()
			, std::numeric_limits<float>::lowest()
			, std::numeric_limits<float>::lowest()
		);

		for (char index(0); index < 4; ++index)
		{
			myFrustumViewAABB.x1 = ((myFrustumIntersections[index].x < myFrustumViewAABB.x1) ? myFrustumIntersections[index].x : myFrustumViewAABB.x1);
			myFrustumViewAABB.x2 = ((myFrustumIntersections[index].x > myFrustumViewAABB.x2) ? myFrustumIntersections[index].x : myFrustumViewAABB.x2);
			myFrustumViewAABB.y1 = ((myFrustumIntersections[index].z < myFrustumViewAABB.y1) ? myFrustumIntersections[index].z : myFrustumViewAABB.y1);
			myFrustumViewAABB.y2 = ((myFrustumIntersections[index].z > myFrustumViewAABB.y2) ? myFrustumIntersections[index].z : myFrustumViewAABB.y2);
		}

		constexpr float viewAABBMargin = 8.0f;
		myFrustumViewAABB +=
		{
			-viewAABBMargin,
			-viewAABBMargin,
			viewAABBMargin,
			viewAABBMargin
		};	
	};
#endif

	myEntities.Add(myCamera.myUniqueIndex);


	myLevel.GetStaticObjectsInside(myFrustumViewAABB, CLevel::EObjectType::All, myEntities);
	myLevel.GetDynamicObjectsInside(myFrustumViewAABB, CLevel::EObjectType::All, myEntities);
}
