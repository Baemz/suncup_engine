#pragma once
#include "State.h"
#include "../EngineCore/EventSystem/EventReceiver.h"
#include "../GraphicsEngine/Camera/CameraInstance.h"
#include "../GraphicsEngine/Model/ModelInstance.h"
#include "Level.h"

#include "../EntitySystem/Handle.h"
#include "../EngineCore/EventSystem/EventStructs.h"

//#define USE_GRID_CULLING

namespace sce { namespace gfx {
	class CText;
}}  


class CInGameState: private CState, public sce::CEventReceiver
{
public:
	CInGameState(EManager& aEntityManager, const char* aLevelPath = nullptr);
	~CInGameState();

	void Init() override;
	void Update(const float aDeltaTime) override;
	void Render() override;

	void OnActivation() override;
	void OnDeactivation() override;
	void ReceiveEvent(const Event::SEvent& aEvent) override;

private:
	void ExecuteEvents();

	bool RegisterFunctionsFunction();

	void GetEntitiesToUse();

	SEntityHandle myCamera;
	const CU::Matrix44f* myCameraProjection;
	const std::string myLevelPath;

	Event::SMouseClickEvent myMouseMessage;

	CU::Vector3f myCameraEulerRotation;
	float myDeltaTime; 
	float myRotationScreenX;
	float myRotationScreenY;
	bool myUseMouseLook;
	CLevel myLevel;

	float myRefreshEntitiesTimer;
	static constexpr float myRefreshEntitesFrequency = 0.5f;

	CU::Vector4f myFrustumViewAABB;
	CU::GrowingArray<CU::Vector3f> myFrustumIntersections;
	bool myShouldRenderFrustumColliders;

	CU::GrowingArray<UniqueIndexType> myEntities;
	bool myHasGottenEntitiesThisFrame;

	CU::GrowingArray<Event::SEvent*> myEvents;

	std::function<void()> myOnLevelLoadedCallback;
};