#pragma once
#include "State.h"
#include "../EngineCore/EventSystem/EventReceiver.h"
#include "../GraphicsEngine/ShaderEffects/ShaderEffectInstance.h"

namespace sce
{
	namespace gfx
	{
		class CSprite;
		class CText;
	}
}

class CCutsceneState : private CState, public sce::CEventReceiver
{
public:
	CCutsceneState(EManager& aEntityManager, const std::string& aCutsceneFolderPath, bool aFadeOutPrevious, float& aCutsceneMusicVolume);
	~CCutsceneState();

	void Init() override;
	void Update(const float aDeltaTime) override;
	void Render() override;

	void OnActivation() override;
	void OnDeactivation() override;

	void ReceiveEvent(const Event::SEvent& aEvent) override;

private:
	std::string myFolderPath;
	sce::gfx::CSprite* myCutsceneImages;
	sce::gfx::CSprite* myBlackCanvasSprite;
	sce::gfx::CText* myPressToContinueText;

	enum class EFadeState : char
	{
		None,
		FadeOutPrevious,
		FadeIn,
		FadeOutCutscene,
	};
	const float myTimeToFade;
	float myFadeTimer;
	EFadeState myFadeState;
	char myCurrentCutsceneIndex;
	char myMaxCutsceneIndex;
	char myRenderStaticIndex;

	float& myCutsceneMusicVolume;
};