#pragma once
#include "State.h"
#include "..\Menu\Menu.h"
#include "../GraphicsEngine/Sprite/Sprite.h"

class CLevelSelectState : private CState
{
public:
	CLevelSelectState(EManager& aEntityManager);
	~CLevelSelectState();

	void Init() override;
	void Update(const float aDeltaTime) override;
	void Render() override;

	void OnActivation() override;
	void OnDeactivation() override;

private:
	CMenu myMenu;
	bool myKeepMenu;
	bool mySelectedLevel;
};