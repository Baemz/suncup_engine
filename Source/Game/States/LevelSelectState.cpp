#include "stdafx.h"
#include "LevelSelectState.h"
#include "StateStack.h"
#include "InGameState.h"
#include "OptionMenuState.h"
#include "ControlMenuState.h"
#include "../AudioEngine/AudioManager.h"
#include "../EngineCore/CommandLineManager/CommandLineManager.h"

#define MAX_FADE_TIME (1.0f)

CLevelSelectState::CLevelSelectState(EManager& aEntityManager)
	: CState(aEntityManager)
	, myKeepMenu(false)
	, mySelectedLevel(false)
{
}

CLevelSelectState::~CLevelSelectState()
{
}

void CLevelSelectState::Init()
{
	if (sce::CCommandLineManager::HasParameter(L"-level"))
	{
		CStateStack::PushMainStateOnTop(sce_new(CInGameState(myEntityManager, nullptr)));
		myState = CState::EStateCommand::eRemoveState;
	}

	{
		std::function<void()> hoverSoundPlay(
			[this]()
		{
			CAudioManager::PostEventAndGetID("Button_Hover", 1);
		});

		myMenu.SetButtonOnEnterSoundFunc(hoverSoundPlay);
	}

	//myMenu.AddGraphic("Data/Menu/Black_4x4.dds", { 0.0f }, { 1000.0f });
	//myMenu.AddGraphic("Data/Menu/Level/levelSelect_levelselectMenu.dds", { 0.01f, 0.0f });

	myMenu.AddButton("Data/Menu/Level/Start_Level_1_No_Hover.dds",	"Data/Menu/Level/Start_Level_1_Hover.dds",			"level1",	{ 0.045f, 0.33f },	{ 0.75f, 0.5f });
	myMenu.AddButton("Data/Menu/Level/Start_Level_2_No_Hover.dds",	"Data/Menu/Level/Start_Level_2_Hover.dds",			"level2",	{ 0.045f, 0.42f },	{ 0.75f, 0.5f });
	myMenu.AddButton("Data/Menu/Level/Start_Level_3_No_Hover.dds",	"Data/Menu/Level/Start_Level_3_Hover.dds",			"level3",	{ 0.045f, 0.51f },	{ 0.75f, 0.5f });
	myMenu.AddButton("Data/Menu/Main/backButton_menu_noToggle.dds",	"Data/Menu/Main/backButton_menu_withToggle.dds",	"back",		{ 0.055f, 0.67f },	{ 0.859375f, 0.625f });

	myKeepMenu = false;
	mySelectedLevel = false;
}

void CLevelSelectState::Update(const float aDeltaTime)
{
	myKeepMenu = myMenu.Update(aDeltaTime);

	if (myKeepMenu == false)
	{
		myState = CState::EStateCommand::eRemoveState;
	}
	else
	{
		if (myMenu.CheckPressedButton("level1"))
		{
			mySelectedLevel = true;
			CStateStack::PushMainStateOnTop(sce_new(CInGameState(myEntityManager, "Data/Levels/ChurchLevel.dataMap")));
		}
		else if (myMenu.CheckPressedButton("level2"))
		{
			mySelectedLevel = true;
			CStateStack::PushMainStateOnTop(sce_new(CInGameState(myEntityManager, "Data/Levels/CityLevel.dataMap")));
		}
		else if (myMenu.CheckPressedButton("level3"))
		{
			mySelectedLevel = true;
			CStateStack::PushMainStateOnTop(sce_new(CInGameState(myEntityManager, "Data/Levels/GalleryLevel.dataMap")));
		}
		else if (myMenu.CheckPressedButton("back"))
		{
			CAudioManager::PostEventAndGetID("Button_Pressed_Short", 1);
			myState = CState::EStateCommand::eRemoveState;
		}

		if (mySelectedLevel)
		{
			CAudioManager::PostEventAndGetID("Stop_Menu_Music", 1);
			CAudioManager::PostEventAndGetID("Button_Pressed_Long", 1);
			myState = CState::EStateCommand::eRemoveState;
		}
	}
}

void CLevelSelectState::Render()
{
	myMenu.Render(false);
}

void CLevelSelectState::OnActivation()
{
	myMenu.Enable();
}

void CLevelSelectState::OnDeactivation()
{
	myMenu.Disable();
}