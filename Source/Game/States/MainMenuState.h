#pragma once
#include "State.h"
#include "../Menu/Menu.h"
#include "../EngineCore/EventSystem/EventReceiver.h"

class CMainMenuState : private CState, public sce::CEventReceiver
{
public:
	CMainMenuState(EManager& aEntityManager);
	~CMainMenuState();

	void Init() override;
	void Update(const float aDeltaTime) override;
	void Render() override;

	void OnActivation() override;
	void OnDeactivation() override;

	void ReceiveEvent(const Event::SEvent& aEvent) override;

private:
	//void UpdateFadeAndStartGameOnCommand(const float aDeltaTime);

private:
	CMenu myMenu;
	bool myRenderBackgroundWhenInactive;
	bool myQuitGame;
	//sce::gfx::CSprite myFadeSprite; // HACK (all this fade)
	//float myCurrFadeTime;
	//bool myIsFadingIn;
	//bool myShouldFade;
	//bool myPressedStart;
	//bool myPressedCredits;
};