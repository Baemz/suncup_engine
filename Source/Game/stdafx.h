#pragma once

// STD-includes
#include <map>
#include <vector>
#include <array>
#include <algorithm>
#include <string>
#include <functional>
#include <cassert>

#include "..\EngineCore\MemoryPool\MemoryPool.h"

// CommonUtilities-includes
#include "../CommonUtilities/GrowingArray.h"
#include "../CommonUtilities/KeyBinds.h"
#include "..\CommonUtilities\CommonMath.h"

//For logging purposes
#include "../EngineCore/Logger/Logger.h"

// For manipulating float precision (added just in case).
namespace Help
{
	static inline const float CutValuePrecision(const float aValueToCut, const unsigned char aDecimalCountToCut = 4)
	{
		char cutValue[64];
		const std::string setting("%." + std::to_string(aDecimalCountToCut) + "lf\n");

		sprintf_s(cutValue, 64, (setting).c_str(), aValueToCut);
		const float value((float)std::atof(cutValue));

		return value;
	}
};