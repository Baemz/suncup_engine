#include "stdafx.h"
#include "ScriptEventManager_EntityState.h"
#include "ScriptEventManager.h"
#include "GameSystems\ExposedFunctions\GameLogicFunctions.h"

#define CHECK_IF_STATE_IS_REMOVED(aStateIndex, aReturnValue)																\
std::shared_lock<std::shared_mutex> sharedLock(aScriptEventManager.myStatesToRemoveMutex);									\
if (aScriptEventManager.myStatesToRemove.Find(aStateIndex) != CU::GrowingArray<CScriptManager::StateIndexType>::FoundNone)	\
return aReturnValue;


bool CScriptEventManager_EntityState::InitFunctions(CScriptEventManager & aScriptEventManager)
{
	CScriptManager& myScriptManager(aScriptEventManager.myScriptManager);

	std::function<void(UniqueIndexType)> killBind(
		[&aScriptEventManager](UniqueIndexType aEntityUniqueIndex)
	{
		aScriptEventManager.KillEntity(aEntityUniqueIndex);
	});
	myScriptManager.RegisterFunction("KillEntity", killBind
		, "Number"
		, "Removes the entity with the specified [ID].", true);

	std::function<void(const CScriptManager::StateIndexType)> setToDestroyOnLeaveGridBind(
		[&aScriptEventManager](const CScriptManager::StateIndexType aStateIndex)
	{
		CHECK_IF_STATE_IS_REMOVED(aStateIndex, ;);

		auto additionalRemoveCodeCallback(
			[&aScriptEventManager, aStateIndex]()
		{
			std::unique_lock<std::shared_mutex> uniqueCallbacksLock(aScriptEventManager.myCallbacksMutex, std::defer_lock);
			std::unique_lock<std::shared_mutex> uniqueRemovedLock(aScriptEventManager.myStatesToRemoveMutex, std::defer_lock);
			std::lock(uniqueCallbacksLock, uniqueRemovedLock);

			auto entityIterator(std::find_if(aScriptEventManager.myEntityCallbacks.begin(), aScriptEventManager.myEntityCallbacks.end()
				, [aStateIndex](const CScriptEventManager::SCallbackKey& aCallbackKey)
			{
				return aCallbackKey.CheckIfStateIndex(aStateIndex);
			}));

			if (entityIterator != aScriptEventManager.myEntityCallbacks.end())
			{
				CScriptManager::Get()->QueueRemoveState(entityIterator->myStateIndex);
				aScriptEventManager.myStatesToRemove.Add(entityIterator->myStateIndex);
				aScriptEventManager.myEntityCallbacks.erase(entityIterator);
			}
		});

		CGameLogicFunctions::SetToDestroyOnLeaveGrid(aScriptEventManager.GetEntityUniqueIndex(aStateIndex), additionalRemoveCodeCallback);
	});
	myScriptManager.RegisterFunctionCallWithStateIndex("SetToDestroyOnLeaveGrid", setToDestroyOnLeaveGridBind
		, ""
		, "Prevents crashes; if &aScriptEventManager object leaves the level's grid: destroy it.");

	std::function<void()> respawnBind(
		[&aScriptEventManager]()
	{
		CGameLogicFunctions::Respawn();
	});
	myScriptManager.RegisterFunction("Respawn", respawnBind
		, ""
		, "Resets the player at the last visited checkpoint with full health.", false);

	std::function<void(UniqueIndexType)> setCheckpointBind(
		[&aScriptEventManager](UniqueIndexType aCheckpointIndex)
	{
		CGameLogicFunctions::SetCurrentCheckpoint(aCheckpointIndex);
	});
	myScriptManager.RegisterFunction("SetCurrentCheckpoint", setCheckpointBind
		, "Number"
		, "Sets the player's spawn point to the checkpoint object with [ID]", true);

	std::function<void(const CScriptManager::StateIndexType, float)> takeDamageBind(
		[&aScriptEventManager](const CScriptManager::StateIndexType aStateIndex, float aDamage)
	{
		CHECK_IF_STATE_IS_REMOVED(aStateIndex, ;);
		CGameLogicFunctions::TakeDamage(aScriptEventManager.GetEntityUniqueIndex(aStateIndex), aDamage);
	});
	myScriptManager.RegisterFunctionCallWithStateIndex("TakeDamage", takeDamageBind
		, "Number"
		, "The entity takes [amount] of damage", false);

	std::function<float(UniqueIndexType)> getHealthOfEntityBind(
		[&aScriptEventManager](UniqueIndexType aEntityUniqueIndex)
	{
		return CGameLogicFunctions::GetHealth(aEntityUniqueIndex);
	});
	myScriptManager.RegisterFunction("GetHealthOfEntity", getHealthOfEntityBind
		, "Number"
		, "Returns the health amount of entity with [ID]", true);

	std::function<float(const CScriptManager::StateIndexType)> getHealthBind(
		[&aScriptEventManager](const CScriptManager::StateIndexType aStateIndex)
	{
		CHECK_IF_STATE_IS_REMOVED(aStateIndex, -1.0f);
		return CGameLogicFunctions::GetHealth(aScriptEventManager.GetEntityUniqueIndex(aStateIndex));
	});
	myScriptManager.RegisterFunctionCallWithStateIndex("GetHealth", getHealthBind
		, ""
		, "Returns the health amount of the entity", false);

	std::function<void(UniqueIndexType, float)> setHealthOfEntityBind(
		[&aScriptEventManager](UniqueIndexType aEntityUniqueIndex, float aHealth)
	{
		CGameLogicFunctions::SetHealth(aEntityUniqueIndex, aHealth);
	});
	myScriptManager.RegisterFunction("SetHealthOfEntity", setHealthOfEntityBind
		, "Number, Number"
		, "Sets the health [amount] of entity with [ID]", true);

	std::function<void(const CScriptManager::StateIndexType, float)> setHealthBind(
		[&aScriptEventManager](const CScriptManager::StateIndexType aStateIndex, float aHealth)
	{
		CHECK_IF_STATE_IS_REMOVED(aStateIndex, ;);
		CGameLogicFunctions::SetHealth(aScriptEventManager.GetEntityUniqueIndex(aStateIndex), aHealth);
	});
	myScriptManager.RegisterFunctionCallWithStateIndex("SetHealth", setHealthBind
		, "Number"
		, "Sets the health [amount] of the entity", false);

	std::function<void(const CScriptManager::StateIndexType, float)> setMaxHealthBind(
		[&aScriptEventManager](const CScriptManager::StateIndexType aStateIndex, float aMaxHealth)
	{
		CHECK_IF_STATE_IS_REMOVED(aStateIndex, ;);
		CGameLogicFunctions::SetMaxHealth(aScriptEventManager.GetEntityUniqueIndex(aStateIndex), aMaxHealth);
	});
	myScriptManager.RegisterFunctionCallWithStateIndex("SetMaxHealth", setMaxHealthBind
		, "Number"
		, "Sets the maximum health [amount] of the entity", false);

	std::function<UniqueIndexType(UniqueIndexType, float)> getClosestEnemyBind(
		[&aScriptEventManager](UniqueIndexType aEntity, float aRadius) -> UniqueIndexType
	{
		return CGameLogicFunctions::GetClosestEnemy(aEntity, aRadius);
	});
	myScriptManager.RegisterFunction("GetClosestEnemy", getClosestEnemyBind
		, ""
		, "Returns the closest enemy [ID] within the Radius provided, returns -1 if no enemy is within radius");

	std::function<UniqueIndexType(UniqueIndexType, float)> getClosestPaintingBind(
		[&aScriptEventManager](UniqueIndexType aEntity, float aRadius) -> UniqueIndexType
	{
		return CGameLogicFunctions::GetClosestPainting(aEntity, aRadius);
	});
	myScriptManager.RegisterFunction("GetClosestPainting", getClosestPaintingBind
		, ""
		, "Returns the closest painting [ID] within the Radius provided, returns -1 if no painting is within radius");

	std::function<UniqueIndexType()> getInvalidEntityIndexBind(
		[&aScriptEventManager]() -> UniqueIndexType
	{
		return CGameLogicFunctions::GetInvalidEntityIndex();
	});
	myScriptManager.RegisterFunction("GetInvalidEntityIndex", getInvalidEntityIndexBind
		, ""
		, "Returns an Invalid Entity Index");

	std::function<bool(UniqueIndexType)> isAliveBind(
		[&aScriptEventManager](UniqueIndexType aEntity) -> bool
	{
		return CGameLogicFunctions::IsAlive(aEntity);
	});
	myScriptManager.RegisterFunction("IsAlive", isAliveBind
		, ""
		, "Returns True if [ID] is alive, false if it's not");

	std::function<void(float, float, float)> spawnEnemyBind(
		[&aScriptEventManager](float aX, float aY, float aZ)
	{
		return CGameLogicFunctions::SpawnEnemy({ aX, aY, aZ });
	});
	myScriptManager.RegisterFunction("SpawnEnemy", spawnEnemyBind
		, ""
		, "Spawns Enemy at position [X][Y][Z]");

	return true;
}
