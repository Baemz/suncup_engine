#include "stdafx.h"
#include "Game.h"
#include "StateStack.h"
#include "States/MainMenuState.h"
#include "States/InGameState.h"
#include "States/ShowRoomState.h"
#include "States/SplashScreenState.h"
#include "../GraphicsEngine/GraphicsEngineInterface.h"
#include "..\EngineCore\WorkerPool\WorkerPool.h"
#include "../CommonUtilities/KeyBinds.h"
#include <Windows.h>

#include "EntitySystemLink/EntitySystemLink.h"
#include "GameSystems/GameSystems.h"
#include "TestScene.h"
#include "../GraphicsEngine/DebugTools.h"
#include "EntityFactory.h"
#include "GameSystems/ScriptSystem.h"
#include "GameSystems/RenderSystem.h"
#include "../AudioEngine/AudioManager.h"
#include "AudioWrapper.h"

#include "../EngineCore/CommandLineManager/CommandLineManager.h"
#include "States/ConsoleState.h"

#define CURSORS_TYPE CU::GrowingArray<bool, unsigned char>

#define myEntityManager (*(CEntityManager<EntitySystemSettings>**)(&myEntityManagerVoid))
#define myCursors (*(CURSORS_TYPE**)(&myCursors))

CGame::CGame()
	: myEntityManagerVoid(nullptr)
{
}

CGame::~CGame()
{
}

void CGame::Init()
{
	CConsoleState::SetAsConsoleOutput();

	AttachToEventReceiving(Event::ESubscribedEvents::eMouseClickInput);

	myCursors = sce_new(CURSORS_TYPE);
	myCursors->Resize(static_cast<unsigned char>(ECursors::SIZE));

	(*myCursors)[static_cast<unsigned char>(ECursors::Default)] = sce::gfx::CGraphicsEngineInterface::LoadCustomCursor(static_cast<unsigned char>(ECursors::Default), "Data/Cursors/mouse_noToggle.cur");
	(*myCursors)[static_cast<unsigned char>(ECursors::MouseDown)] = sce::gfx::CGraphicsEngineInterface::LoadCustomCursor(static_cast<unsigned char>(ECursors::MouseDown), "Data/Cursors/mouse_withToggle.cur");

	sce::gfx::CGraphicsEngineInterface::SetCustomCursor(static_cast<unsigned char>(ECursors::Default));

	CU::CKeyBinds::Create("Data/HotKeys.json");
	//Swap for general savestate manager
	CStateStack::Create();

	myEntityManager = sce_new(CEntityManager<EntitySystemSettings>);

	CGameSystems::Create(*myEntityManager);
	CEntityFactory::ourEntityManagerVoid = myEntityManager;

	if (sce::CCommandLineManager::HasArgument(L"-state", L"debugShowroom"))
	{
		CStateStack::PushMainStateOnTop(sce_new(CShowRoomState(*myEntityManager, true)));
	}
	else if (sce::CCommandLineManager::HasArgument(L"-state", L"showroom"))
	{
		CStateStack::PushMainStateOnTop(sce_new(CShowRoomState(*myEntityManager)));
	}
	else
	{
#ifndef _RETAIL
		CStateStack::PushMainStateOnTop(sce_new(CMainMenuState(*myEntityManager)));
#else
		CStateStack::PushMainStateOnTop(sce_new(CMainMenuState(*myEntityManager)));
		CStateStack::PushMainStateOnTop(sce_new(CSplashScreenState(*myEntityManager)));
#endif
	}
	//CStateStack::PushMainStateOnTop(sce_new(CTestScene(*myEntityManager)));
	CGameSystems::Get()->ActivateSystem<CScriptSystem>();
	CGameSystems::Get()->ActivateSystem<CRenderSystem>();

	CAudioManager::LoadBank("Data/Audio/Character_Sound_Effects.bnk");
	CAudioManager::LoadBank("Data/Audio/General_Sound_Effects.bnk");
	CAudioManager::LoadBank("Data/Audio/Menu_Sound_Effects.bnk");
	CAudioManager::LoadBank("Data/Audio/Music.bnk");
	CAudioManager::RegisterObject(1);
}

void CGame::Update(const float aDeltaTime)
{
	if (!CStateStack::IsEmpty())
	{
		auto gameSystems(CGameSystems::Get());

		gameSystems->QueuePostUpdateFunction([]() -> bool { CAudioWrapper::UpdateAudioPositions(); return true; });

		CStateStack::InitStatesCreatedFromPreviousFrame();

		CStateStack::UpdateStateOnTop(aDeltaTime);

		gameSystems->UpdateAllSystems(aDeltaTime);

		CStateStack::RenderApprovedStates();

		gameSystems->RenderAllSystems(aDeltaTime);

		gameSystems->RemoveAllQueuedEntities();
	}
	else
	{
		PostQuitMessage(0);
	}
}

void CGame::Shutdown()
{
	CStateStack::Destroy();

	CGameSystems::Get()->DeactivateSystem<CScriptSystem>();
	CGameSystems::Get()->DeactivateSystem<CRenderSystem>();
	CGameSystems::Destroy();

	sce_delete(myEntityManager);

	CU::CKeyBinds::Destroy();

	sce_delete(myCursors);

	DetachFromEventReceiving();
}

void CGame::ReceiveEvent(const Event::SEvent& aEvent)
{
	if (aEvent.myType == Event::EEventType::eMouseClickInput)
	{
		const Event::SMouseClickEvent& mouseClickEvent(*reinterpret_cast<const Event::SMouseClickEvent*>(&aEvent));

		if ((mouseClickEvent.state == EButtonState::Pressed) || (mouseClickEvent.state == EButtonState::Released))
		{
			static unsigned int amountDown(0);
			const unsigned int beforeChecked(amountDown);

			if (mouseClickEvent.state == EButtonState::Pressed)
			{
				++amountDown;
				
			}
			else if (mouseClickEvent.state == EButtonState::Released)
			{
				--amountDown;
			}

			if ((amountDown == 1) && (beforeChecked == 0))
			{
				sce::gfx::CGraphicsEngineInterface::SetCustomCursor(static_cast<unsigned char>(ECursors::MouseDown));
			}
			else if ((amountDown == 0) && (beforeChecked == 1))
			{
				sce::gfx::CGraphicsEngineInterface::SetCustomCursor(static_cast<unsigned char>(ECursors::Default));
			}
		}
	}
}
