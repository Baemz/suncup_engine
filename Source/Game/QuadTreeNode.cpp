#include "stdafx.h"
#include "QuadTreeNode.h"
#include "..\GraphicsEngine\DebugTools.h"

#ifdef max
#undef max
#endif // max

const unsigned short QuadTreeNode::ourAmountOfChildren = 4;
const CU::Vector3f QuadTreeNode::ourDebugColors[] =
{
	{ 1.0f, 0.0f, 0.0f },
	{ 1.0f, 0.5f, 0.0f },
	{ 0.5f, 1.0f, 0.0f },
	{ 0.0f, 1.0f, 0.0f },
	{ 0.0f, 1.0f, 0.5f },
	{ 0.0f, 1.0f, 1.0f },
	{ 0.0f, 0.5f, 1.0f },
	{ 0.0f, 0.0f, 1.0f },
	{ 0.5f, 0.0f, 1.0f },
	{ 1.0f, 0.0f, 1.0f },
	{ 1.0f, 0.0f, 0.5f },
};

QuadTreeNode::QuadTreeNode(const unsigned short aLoosenessFactor)
	: myChildren(ourAmountOfChildren)
	, myMaxAmountOfObjects(0)
	, myLoosenessFactor(aLoosenessFactor)
	, myShouldRenderNodes(false)
{
}

QuadTreeNode::~QuadTreeNode()
{
	Clear();
}

void QuadTreeNode::Init(const unsigned short aMaxAmountOfObjectsInEachNode, const CU::Vector4f& aAABB)
{
	Clear();

	myMaxAmountOfObjects = aMaxAmountOfObjectsInEachNode;
	SetAABB(aAABB);

	myObjects.Init(myMaxAmountOfObjects);
}

bool QuadTreeNode::GetObjects(CU::GrowingArray<QuadTreeData>& aArrayToFill, const bool aIgnoreChildren) const
{
	aArrayToFill.Add(myObjects);

	if (aIgnoreChildren == false)
	{
		for (unsigned short childIndex(0); childIndex < myChildren.Size(); ++childIndex)
		{
			myChildren[childIndex]->GetObjects(aArrayToFill, false);
		}
	}

	return !aArrayToFill.Empty();
}

void QuadTreeNode::Clear()
{
	for (unsigned short childIndex(0); childIndex < myChildren.Size(); ++childIndex)
	{
		sce_delete(myChildren[childIndex]);
	}
	myChildren.RemoveAll();

	myMaxAmountOfObjects = 0;
	myShouldRenderNodes = false;
}

const CU::GrowingArray<QuadTreeNode*>& QuadTreeNode::GetChildren() const
{
	return myChildren;
}

bool QuadTreeNode::GetObjectsCloseToPoint(const CU::Vector2f& aPoint, CU::GrowingArray<QuadTreeData>& aArrayToFill) const
{
	if (PointFitsInside(aPoint) == true)
	{
		aArrayToFill.Add(myObjects);
	}

	CU::GrowingArray<unsigned short> childsInsideIndices;
	childsInsideIndices.Init(ourAmountOfChildren);

	if (PointFitsInsideAnyChildren(aPoint, childsInsideIndices) == true)
	{
		for (unsigned short childIndex(0); childIndex < childsInsideIndices.Size(); ++childIndex)
		{
			myChildren[childsInsideIndices[childIndex]]->GetObjectsCloseToPoint(aPoint, aArrayToFill);
		}
	}

	return !aArrayToFill.Empty();
}

bool QuadTreeNode::GetObjectsCloseToAABB(const CU::Vector4f& aAABB, CU::GrowingArray<QuadTreeData>& aArrayToFill) const
{
	if (AABBFitsInside(aAABB) == true)
	{
		aArrayToFill.Add(myObjects);
	}

	CU::GrowingArray<unsigned short> childsInsideIndices;
	childsInsideIndices.Init(ourAmountOfChildren);

	if (AABBFitsInsideAnyChildren(aAABB, childsInsideIndices) == true)
	{
		for (unsigned short childIndex(0); childIndex < childsInsideIndices.Size(); ++childIndex)
		{
			myChildren[childsInsideIndices[childIndex]]->GetObjectsCloseToAABB(aAABB, aArrayToFill);
		}
	}

	return !aArrayToFill.Empty();
}

QuadTreeNode* QuadTreeNode::GetNodeInside(const CU::Vector2f& aPoint)
{
	CU::GrowingArray<unsigned short> childsInsideIndex;

	if ((PointFitsInsideAnyChildren(aPoint, childsInsideIndex) == true) && (childsInsideIndex.Size() > 0))
	{
		return myChildren[childsInsideIndex[0]]->GetNodeInside(aPoint);
	}
	else if (PointFitsInside(aPoint) == true)
	{
		return this;
	}

	return nullptr;
}

const QuadTreeNode* QuadTreeNode::GetNodeInside(const CU::Vector2f& aPoint) const
{
	CU::GrowingArray<unsigned short> childsInsideIndex;

	if ((PointFitsInsideAnyChildren(aPoint, childsInsideIndex) == true) && (childsInsideIndex.Size() > 0))
	{
		return myChildren[childsInsideIndex[0]]->GetNodeInside(aPoint);
	}
	else if (PointFitsInside(aPoint) == true)
	{
		return this;
	}

	return nullptr;
}

const bool QuadTreeNode::GetNodeListInside(const CU::Vector2f& aPoint, CU::GrowingArray<const QuadTreeNode*>& aArrayToFill) const
{
	if (PointFitsInside(aPoint) == true)
	{
		aArrayToFill.Add(this);
	}

	CU::GrowingArray<unsigned short> childsInsideIndex(4);
	if ((PointFitsInsideAnyChildren(aPoint, childsInsideIndex) == true) && (childsInsideIndex.Size() > 0))
	{
		for (unsigned short i(0); i < childsInsideIndex.size(); ++i)
		{
			myChildren[childsInsideIndex[i]]->GetNodeListInside(aPoint, aArrayToFill);
		}
	}

	return (aArrayToFill.size() > 0);
}

const CU::Vector4f& QuadTreeNode::GetAABB() const
{
	return myAABB;
}

void QuadTreeNode::SetAABB(const CU::Vector4f& aAABB)
{
	myAABB = aAABB;
}

void QuadTreeNode::AddObject(const QuadTreeData& aObject)
{
	unsigned short childToAddObjectTo(std::numeric_limits<unsigned short>::max());

	assert("Object doesn't fit in this quad tree" && (ObjectFitsInside(aObject) == true));

	if ((myChildren.Size() > 0) && (ObjectFitsInsideAChild(aObject, childToAddObjectTo) == true))
	{
		myChildren[childToAddObjectTo]->AddObject(aObject);
	}
	else if ((myChildren.Size() == 0) && (myObjects.Size() >= myMaxAmountOfObjects))
	{
		QuadTreeNode* newChild(nullptr);

		CU::Vector4f tempAABB(myAABB);

		CU::Vector2f size((tempAABB.x2 - tempAABB.x1), (tempAABB.y2 - tempAABB.y1));
		size = GetSize() * -1.0f;

		tempAABB.x1 -= size.x;
		tempAABB.x2 += size.x;
		tempAABB.y1 -= size.y;
		tempAABB.y2 += size.y;

		const float zMinusXDiv2((tempAABB.x2 - tempAABB.x1) / 2.0f);
		const float wMinusYDiv2((tempAABB.y2 - tempAABB.y1) / 2.0f);

		newChild = sce_new(QuadTreeNode());
		newChild->Init(myMaxAmountOfObjects
			, ConvertAABBWithLooseness(*newChild, CU::Vector4f((tempAABB.x1), (tempAABB.y1)
				, (tempAABB.x1 + zMinusXDiv2), (tempAABB.y1 + wMinusYDiv2))));
		myChildren.Add(newChild);

		newChild = sce_new(QuadTreeNode());
		newChild->Init(myMaxAmountOfObjects
			, ConvertAABBWithLooseness(*newChild, CU::Vector4f((tempAABB.x1 + zMinusXDiv2), (tempAABB.y1)
				, (tempAABB.x2), (tempAABB.y1 + wMinusYDiv2))));
		myChildren.Add(newChild);

		newChild = sce_new(QuadTreeNode());
		newChild->Init(myMaxAmountOfObjects
			, ConvertAABBWithLooseness(*newChild, CU::Vector4f((tempAABB.x1), (tempAABB.y1 + wMinusYDiv2)
				, (tempAABB.x1 + zMinusXDiv2), (tempAABB.y2))));
		myChildren.Add(newChild);

		newChild = sce_new(QuadTreeNode());
		newChild->Init(myMaxAmountOfObjects
			, ConvertAABBWithLooseness(*newChild, CU::Vector4f((tempAABB.x1 + zMinusXDiv2), (tempAABB.y1 + wMinusYDiv2)
				, (tempAABB.x2), (tempAABB.y2))));
		myChildren.Add(newChild);

		const CU::GrowingArray<QuadTreeData> oldObjects(myObjects);

		myObjects.RemoveAll();

		for (unsigned short objectIndex(0); objectIndex < oldObjects.Size(); ++objectIndex)
		{
			AddObject(oldObjects[objectIndex]);
		}

		AddObject(aObject);
	}
	else
	{
		assert("Adding object to full quad tree!" && (myObjects.Full() == false));
		myObjects.Add(aObject);
	}
}

unsigned short QuadTreeNode::GetLoosenessFactor() const
{
	return myLoosenessFactor;
}

CU::Vector2f QuadTreeNode::GetSize() const
{
	return mySize;
}

void QuadTreeNode::SetSize(const CU::Vector2f& aSize)
{
	mySize = aSize;
}

#ifndef _RETAIL
void QuadTreeNode::DebugRenderNodes()
{
	DebugRenderNodes(myAABB);
}

void QuadTreeNode::DebugRenderNodes(const CU::Vector4f& aAffectedAABB)
{
	if (!myShouldRenderNodes)
	{
		return;
	}

	const CU::Vector3f x1y1z(aAffectedAABB.x1, 0.0f, aAffectedAABB.y1);
	const CU::Vector3f x1y2z(aAffectedAABB.x1, 0.0f, aAffectedAABB.y2);
	const CU::Vector3f x2y1z(aAffectedAABB.x2, 0.0f, aAffectedAABB.y1);
	const CU::Vector3f x2y2z(aAffectedAABB.x2, 0.0f, aAffectedAABB.y2);

	constexpr float sizeHardcoded(10.0f / 200.0f);
	const float size3DRectangle(x1y1z.x * sizeHardcoded);
	CU::Vector3f sizeOf3DRectangles(size3DRectangle, 100.0f, size3DRectangle);

	sce::gfx::CDebugTools::Get()->DrawRectangle3D(x1y1z, sizeOf3DRectangles);
	sce::gfx::CDebugTools::Get()->DrawRectangle3D(x1y2z, sizeOf3DRectangles);
	sce::gfx::CDebugTools::Get()->DrawRectangle3D(x2y1z, sizeOf3DRectangles);
	sce::gfx::CDebugTools::Get()->DrawRectangle3D(x2y2z, sizeOf3DRectangles);

	constexpr eDebugRenderColor startColor(eDebugRenderColor::START);
	DebugRenderNodesImpl(startColor, aAffectedAABB, 0.0f);

}

void QuadTreeNode::DebugRenderObjects()
{
	DebugRenderObjects(myAABB);
}

void QuadTreeNode::DebugRenderObjects(const CU::Vector4f& aAffectedAABB)
{
	constexpr eDebugRenderColor startColor(eDebugRenderColor::START);
	DebugRenderObjectsImpl(startColor, aAffectedAABB, 0.0f);
}

void QuadTreeNode::DebugRenderNodesImpl(const eDebugRenderColor& aDebugColor, const CU::Vector4f& aAffectedAABB, const float aRenderPositionY)
{
	if (AABBFitsInside(aAffectedAABB) == false)
	{
		return;
	}

	eDebugRenderColor colorToUse(aDebugColor);

#define DRAW_LINE_INPUT_x1_y1_0 { myAABB.x1, aRenderPositionY, myAABB.y1 }
#define DRAW_LINE_INPUT_x1_y2_0 { myAABB.x1, aRenderPositionY, myAABB.y2 }
#define DRAW_LINE_INPUT_x2_y1_0 { myAABB.x2, aRenderPositionY, myAABB.y1 }
#define DRAW_LINE_INPUT_x2_y2_0 { myAABB.x2, aRenderPositionY, myAABB.y2 }
#define DRAW_LINE_INPUT_COLOR ourDebugColors[static_cast<unsigned char>(colorToUse)]

	DT_DRAW_LINE_4_ARGS(DRAW_LINE_INPUT_x1_y1_0, DRAW_LINE_INPUT_x1_y2_0, DRAW_LINE_INPUT_COLOR, DRAW_LINE_INPUT_COLOR);
	DT_DRAW_LINE_4_ARGS(DRAW_LINE_INPUT_x1_y1_0, DRAW_LINE_INPUT_x2_y1_0, DRAW_LINE_INPUT_COLOR, DRAW_LINE_INPUT_COLOR);
	DT_DRAW_LINE_4_ARGS(DRAW_LINE_INPUT_x1_y2_0, DRAW_LINE_INPUT_x2_y2_0, DRAW_LINE_INPUT_COLOR, DRAW_LINE_INPUT_COLOR);
	DT_DRAW_LINE_4_ARGS(DRAW_LINE_INPUT_x2_y1_0, DRAW_LINE_INPUT_x2_y2_0, DRAW_LINE_INPUT_COLOR, DRAW_LINE_INPUT_COLOR);

#undef DRAW_LINE_INPUT_x1_y1_0
#undef DRAW_LINE_INPUT_x2_y2_0
#undef DRAW_LINE_INPUT_x2_y1_0
#undef DRAW_LINE_INPUT_x2_y2_0
#undef DRAW_LINE_INPUT_COLOR

	for (unsigned short childIndex(0); childIndex < myChildren.Size(); ++childIndex)
	{
		colorToUse = static_cast<eDebugRenderColor>(static_cast<unsigned char>(colorToUse) + 1);
		if (colorToUse == eDebugRenderColor::END)
		{
			colorToUse = eDebugRenderColor::START;
		}
		myChildren[childIndex]->DebugRenderNodesImpl(colorToUse, aAffectedAABB, (aRenderPositionY + 1.0f));
	}
}

void QuadTreeNode::DebugRenderObjectsImpl(const eDebugRenderColor& aDebugColor, const CU::Vector4f& aAffectedAABB, const float aScaleY)
{
	if (AABBFitsInside(aAffectedAABB) == false)
	{
		return;
	}

	eDebugRenderColor colorToUse(aDebugColor);

#define DRAW_CUBE_INPUT_COLOR ourDebugColors[static_cast<unsigned char>(colorToUse)]

	for (unsigned short objectIndex(0); objectIndex < myObjects.Size(); ++objectIndex)
	{
		const QuadTreeData& currentObject(myObjects[objectIndex]);

		const CU::Vector3f positionToDrawAt(currentObject.myCenterPosition.x, 0.0f, currentObject.myCenterPosition.y);

		sce::gfx::CDebugTools::Get()->DrawRectangle3D(positionToDrawAt, { currentObject.mySize.x, aScaleY, currentObject.mySize.y }, DRAW_CUBE_INPUT_COLOR);
	}

#undef DRAW_CUBE_INPUT_COLOR

	for (unsigned short childIndex(0); childIndex < myChildren.Size(); ++childIndex)
	{
		colorToUse = static_cast<eDebugRenderColor>(static_cast<unsigned char>(colorToUse) + 1);
		if (colorToUse == eDebugRenderColor::END)
		{
			colorToUse = eDebugRenderColor::START;
		}
		myChildren[childIndex]->DebugRenderObjectsImpl(colorToUse, aAffectedAABB, (aScaleY + 1.0f));
	}
}
#endif // _RETAIL

CU::Vector4f QuadTreeNode::ConvertAABBWithLooseness(QuadTreeNode& aQuadTree, const CU::Vector4f& aAABB, const bool aInversed)
{
	CU::Vector4f tempAABB(aAABB);

	CU::Vector2f size((tempAABB.x2 - tempAABB.x1), (tempAABB.y2 - tempAABB.y1));
	size /= 2.0f;

	if (aInversed == false)
	{
		aQuadTree.SetSize(size);
	}
	else
	{
		size = aQuadTree.GetSize() * -1.0f;
	}

	tempAABB.x1 -= size.x;
	tempAABB.x2 += size.x;
	tempAABB.y1 -= size.y;
	tempAABB.y2 += size.y;

	return tempAABB;
}

bool QuadTreeNode::PointFitsInsideAnyChildren(const CU::Vector2f& aPoint, CU::GrowingArray<unsigned short>& aChildIndex) const
{
	for (unsigned short childIndex(0); childIndex < myChildren.Size(); ++childIndex)
	{
		if (myChildren[childIndex]->PointFitsInside(aPoint) == true)
		{
			aChildIndex.Add(childIndex);
		}
	}

	if (aChildIndex.Size() > 0)
	{
		return true;
	}

	return false;
}

bool QuadTreeNode::PointFitsInside(const CU::Vector2f& aPoint) const
{
	if (((aPoint.x > myAABB.x1) && (aPoint.y > myAABB.y1))
		&& ((aPoint.x < myAABB.x2) && (aPoint.y < myAABB.y2)))
	{
		return true;
	}

	return false;
}

bool QuadTreeNode::AABBFitsInsideAnyChildren(const CU::Vector4f& aAABB, CU::GrowingArray<unsigned short>& aChildIndex) const
{
	for (unsigned short childIndex(0); childIndex < myChildren.Size(); ++childIndex)
	{
		if (myChildren[childIndex]->AABBFitsInside(aAABB) == true)
		{
			aChildIndex.Add(childIndex);
		}
	}

	if (aChildIndex.Size() > 0)
	{
		return true;
	}

	return false;
}

bool QuadTreeNode::AABBFitsInside(const CU::Vector4f& aAABB) const
{
	if (((myAABB.x1 <= aAABB.x2) && (aAABB.x1 <= myAABB.x2))
		&& ((myAABB.y1 <= aAABB.y2) && (aAABB.y1 <= myAABB.y2)))
	{
		return true;
	}

	return false;
}

bool QuadTreeNode::ObjectFitsInside(const QuadTreeData& aObject) const
{
	const CU::Vector2f pointX1Y1((aObject.myCenterPosition.x - (aObject.mySize.x / 2.0f)), (aObject.myCenterPosition.y - (aObject.mySize.y / 2.0f)));
	const CU::Vector2f pointX2Y2((aObject.myCenterPosition.x + (aObject.mySize.x / 2.0f)), (aObject.myCenterPosition.y + (aObject.mySize.y / 2.0f)));
	const CU::Vector2f aaBBX1Y1(myAABB.x1, myAABB.y1);
	const CU::Vector2f aaBBX2Y2(myAABB.x2, myAABB.y2);

	if (((pointX1Y1.x < aaBBX2Y2.x) && (pointX1Y1.y < aaBBX2Y2.y))
		&& ((pointX2Y2.x > aaBBX1Y1.x) && (pointX2Y2.y > aaBBX1Y1.y)))
	{
		return true;
	}

	return false;
}

bool QuadTreeNode::ObjectFitsInsideAChild(const QuadTreeData& aObject, unsigned short& aChildIndex) const
{
	CU::Vector2f childsSize(myChildren[0]->GetSize());

	unsigned short childClosestIndex(aChildIndex);
	float leastDistance(-1.0f);

	const CU::Vector2f& objectSize(aObject.mySize);

	if ((objectSize.x < childsSize.x) && (objectSize.y < childsSize.y))
	{
		for (unsigned short childIndex(0); childIndex < myChildren.Size(); ++childIndex)
		{
			QuadTreeNode* currentChild(myChildren[childIndex]);
			const CU::Vector4f currentAABB(ConvertAABBWithLooseness(*currentChild, currentChild->GetAABB(), true));

			const CU::Vector2f centerPosition((currentAABB.x + childsSize.x), (currentAABB.y + childsSize.y));

			float currentDistance((aObject.myCenterPosition - centerPosition).Length2());
			if ((leastDistance == -1.0f) || (currentDistance < leastDistance))
			{
				childClosestIndex = childIndex;
				leastDistance = currentDistance;
			}
		}

		aChildIndex = childClosestIndex;
		return true;
	}

	return false;
}
