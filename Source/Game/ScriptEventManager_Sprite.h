#pragma once

class CScriptEventManager;

class CScriptEventManager_Sprite
{
public:
	static bool InitFunctions(CScriptEventManager& aScriptEventManager);

private:
	CScriptEventManager_Sprite() = delete;
	~CScriptEventManager_Sprite() = delete;

};
