#include "stdafx.h"
#include "Level.h"
#include "MapReader/MapReader.h"
#include "../EngineCore/WorkerPool/WorkerPool.h"
#include "../CommonUtilities/Stopwatch.h"
#include "EntityFactory.h"
#include "../EngineCore/FileFinder/FileFinder.h"
#include "GameSystems/ScriptSystem.h"
#include "GameSystems/ExposedFunctions/GameLogicFunctions.h"
#include "EntitySystemLink/EntitySystemLink.h"
#include "GameSystems/GameSystems.h"
#include "../GraphicsEngine/Model/Loading/ModelFactory.h"
#include "NavigationMesh/NavigationMeshWrapper.h"
#include "GameSystems/RenderSystem.h"
#include "AudioWrapper.h"

CLevel::CLevel()
	: myTimeSpentOnLevel(0.0)
	, myIsLoading(false)
	, myMapLoaded(false)
	, myIWantToSwitchLevel(false)
	, myQueuedGridAdd(0, SGridData::Hash)
{
	static_assert(std::is_same<decltype(SLevelInitFunction::myOnInitFunction), CEntityFactory::SEntityCreationData::SScriptData::OnInitFunction>::value, "Pls fix");

	CNavigationMeshWrapper::Create();
}

CLevel::~CLevel()
{
}

bool CLevel::Init(const char* aLevelPath)
{
	CMapReader mapReader;
	CMapData* mapData(sce_new(CMapData));

// 	auto future = WORKER_POOL_QUEUE_WORK_FUTURE(&CMapReader::LoadMap, &mapReader, aLevelPath, std::ref<CMapData>(*mapData), -1l);
// 	// Do stuff while level is loading from disk

	myLevelToSwitchTo = "";
	myIWantToSwitchLevel = false;

	float readMapFromDiskTime;
	START_TIMER(readMapFromDisk);
	if (mapReader.LoadMap(aLevelPath, *mapData) == false)
	{
		mapData->Destroy();
		sce_delete(mapData);
		GAME_LOG("ERROR! Failed to read level \"%s\""
			, aLevelPath);
		return false;
	}
	END_TIMER_F(readMapFromDisk, readMapFromDiskTime);
	GAME_LOG("Level \"%s\" took %f ms to read from disk"
		, aLevelPath, readMapFromDiskTime);

	const std::string levelPath(aLevelPath);
	if (levelPath == "Data/Levels/ChurchLevel.dataMap")
	{
		myEnvLight.Init("Data/Models/Misc/skybox_test_dark_cube_radiance.dds");
		mySkybox = sce::gfx::CModelFactory::Get()->CreateSkyboxCube("Data/Models/Misc/skybox_test_dark_cube_radiance.dds");
	}
	else if (levelPath == "Data/Levels/CityLevel.dataMap")
	{
		myEnvLight.Init("Data/Models/Misc/skybox_test_dark_cube_radiance.dds");
		mySkybox = sce::gfx::CModelFactory::Get()->CreateSkyboxCube("Data/Models/Misc/skybox_test_dark_cube_radiance.dds");
	}
	else if (levelPath == "Data/Levels/Cube.dataMap")
	{
		myEnvLight.Init("Data/Models/Misc/Material_Preview_Skybox.dds");
		mySkybox = sce::gfx::CModelFactory::Get()->CreateSkyboxCube("Data/Models/Misc/skybox_test_dark_cube_radiance.dds");
	}
	else
	{
		myEnvLight.Init("Data/Models/Misc/Cube.dds");
		mySkybox = sce::gfx::CModelFactory::Get()->CreateSkyboxCube("Data/Models/Misc/skybox_test_dark_cube_radiance.dds");
	}

	myEnvLight.UseForRendering();
	mySkybox.Init();

	myCurrentLevelName = aLevelPath;
	myTimeSpentOnLevel = 0.0f;
	myIsLoading = false;
	myMapLoaded = false;
//	myMapLoaded = future.get();

	CAudioWrapper::EmptyPostedEventContainer();

	myIsLoading = true;
	WORKER_POOL_QUEUE_WORK(&CLevel::LoadLevel, this, mapData);

	return true;
}

void CLevel::Destroy()
{
	LoadLevel(nullptr);
	while (IsLoading())
	{
		std::this_thread::sleep_for(std::chrono::milliseconds(1));
	}
	LoadLevelPreCleanup();

	for (const auto& object : myQueuedGridAdd)
	{
		CScriptSystem::KillEntity(object.myEntityUniqueIndex);
	}

	myQueuedGridAdd.clear();
	myQueuedGridRemove.clear();
	myQueuedGridUpdate.clear();
	myQueuedOnGridLeaveCallback.clear();

	CNavigationMeshWrapper::Destroy();
}

void CLevel::QueueLevelSwitch(const std::string& aLevelPath)
{
	myLevelToSwitchTo = aLevelPath;
	myIWantToSwitchLevel = true;
}

bool CLevel::WantToSwitchLevel(std::string& aLevelPathIfSwitching)
{
	if (myIWantToSwitchLevel == true)
	{
		aLevelPathIfSwitching = myLevelToSwitchTo;
		return true;
	}
	
	return false;
}

bool CLevel::Update(const float aDeltaTime)
{
	if (myIsLoading == true)
	{
		return true;
	}

	myTimeSpentOnLevel += aDeltaTime;
	myEnvLight.UseForRendering();
	mySkybox.Render();

#ifndef _RETAIL
	// 	static float size(0.0f);
	// 	const CU::Vector4f cullingAABB(-size, -size, size, size);
	// 	size += aDeltaTime;
	// 	if (size > 200.0f)
	// 	{
	// 		size = 100.0f;
	// 	}

	myStaticQuadTree.DebugRenderNodes();
	//myStaticQuadTree.DebugRenderObjects(cullingAABB);

	myDynamicGrid.DebugRender();
#endif // !_RETAIL

	const UniqueIndexType playerUniqueIndex(CGameLogicFunctions::GetPlayerUniqueIndex());
	if (playerUniqueIndex != EntityHandleINVALID.myUniqueIndex)
	{
		const CU::Vector3f playerPosition3f(CGameLogicFunctions::GetPlayerPosition());
		const CU::Vector2f playerPosition(playerPosition3f.x, playerPosition3f.z);

		auto scriptSystem(CGameSystems::Get()->GetSystem<CScriptSystem>());
		EManager& entityManager(scriptSystem->myEntityManager);

		CU::GrowingArray<UniqueIndexType> triggersNear;
		if (GetDynamicObjectsNear(playerPosition, EObjectType::Trigger, triggersNear) == true)
		{
			for (const auto& currentTrigger : triggersNear)
			{
				auto& triggerComponent(entityManager.GetComponent<CompTrigger>(SEntityHandle(currentTrigger)));

				if (triggerComponent.myContainsPlayer)
				{
					if (((triggerComponent.myMin.x < playerPosition3f.x) && (triggerComponent.myMin.y < playerPosition3f.y) && (triggerComponent.myMin.z < playerPosition3f.z))
						&& ((triggerComponent.myMax.x > playerPosition3f.x) && (triggerComponent.myMax.y > playerPosition3f.y) && (triggerComponent.myMax.z > playerPosition3f.z)))
					{
						scriptSystem->CallScriptEventOnTarget(currentTrigger, "OnTriggerStay", playerUniqueIndex);
					}
					else
					{
						scriptSystem->CallScriptEventOnTarget(currentTrigger, "OnTriggerExit", playerUniqueIndex);
						triggerComponent.myContainsPlayer = false;
					}
				}
				else
				{
					if (((triggerComponent.myMin.x < playerPosition3f.x) && (triggerComponent.myMin.y < playerPosition3f.y) && (triggerComponent.myMin.z < playerPosition3f.z))
						&& ((triggerComponent.myMax.x > playerPosition3f.x) && (triggerComponent.myMax.y > playerPosition3f.y) && (triggerComponent.myMax.z > playerPosition3f.z)))
					{
						scriptSystem->CallScriptEventOnTarget(currentTrigger, "OnTriggerEnter", playerUniqueIndex);
						triggerComponent.myContainsPlayer = true;
					}
				}
			}
		}
	}

	return true;
}

bool CLevel::PostUpdate()
{
	if (myMapLoaded == false)
	{
		return true;
	}

	std::unique_lock<std::mutex> lgAdd(myQueuedGridAddMutex, std::defer_lock);
	std::unique_lock<std::mutex> lgRemove(myQueuedGridRemoveMutex, std::defer_lock);
	std::unique_lock<std::mutex> lgUpdate(myQueuedGridUpdateMutex, std::defer_lock);
	std::unique_lock<std::mutex> lgSetOnGridLeaveCallback(myQueuedOnGridLeaveCallbackMutex, std::defer_lock);
	std::lock(lgAdd, lgRemove, lgUpdate, lgSetOnGridLeaveCallback);

	for (const auto& gridData : myQueuedGridAdd)
	{
		myDynamicGrid.AddObject(gridData);
	}

	for (const auto& uniqueIndex : myQueuedGridRemove)
	{
		myDynamicGrid.RemoveObject(uniqueIndex);
		myQueuedGridUpdate.erase(uniqueIndex);
	}

	for (const auto& uniqueIndexPositionPair : myQueuedGridUpdate)
	{
		myDynamicGrid.UpdateObjectGridPosition(uniqueIndexPositionPair.first, uniqueIndexPositionPair.second);
	}

	for (const auto& uniqueIndexCallbbackPair : myQueuedOnGridLeaveCallback)
	{
		myDynamicGrid.SetObjectOnGridLeaveCallback(uniqueIndexCallbbackPair.first, uniqueIndexCallbbackPair.second);
	}

	myQueuedGridAdd.clear();
	myQueuedGridRemove.clear();
	myQueuedGridUpdate.clear();
	myQueuedOnGridLeaveCallback.clear();

	return true;
}

void CLevel::QueueAddToActiveObjects(const SGridData& aGridData)
{
	std::unique_lock<std::mutex> lg(myQueuedGridAddMutex);
	myQueuedGridAdd.insert(aGridData);
}

void CLevel::QueueRemoveFromActiveObjects(const UniqueIndexType& aEntityUniqueIndex)
{
	std::unique_lock<std::mutex> lg(myQueuedGridRemoveMutex);
	myQueuedGridRemove.insert(aEntityUniqueIndex);
}

void CLevel::QueueUpdateActiveObject(const UniqueIndexType& aEntityUniqueIndex, const CU::Vector3f& aNewPosition)
{
	std::unique_lock<std::mutex> lg(myQueuedGridUpdateMutex);
	myQueuedGridUpdate[aEntityUniqueIndex] = CU::Vector2f(aNewPosition.x, aNewPosition.z);
}

void CLevel::QueueSetOnLeaveGridCallback(const UniqueIndexType & aEntityUniqueIndex, const std::function<void()>& aOnLeaveGridCallback)
{
	std::unique_lock<std::mutex> lg(myQueuedOnGridLeaveCallbackMutex);
	myQueuedOnGridLeaveCallback[aEntityUniqueIndex] = aOnLeaveGridCallback;
}

const char* CLevel::GetCurrentLevelName() const
{
	return myCurrentLevelName.c_str();
}

double CLevel::GetTimeSpentOnLevel() const
{
	if (myIsLoading == true)
	{
		return -1.0;
	}

	return myTimeSpentOnLevel;
}

bool CLevel::GetStaticObjectsInside(const CU::Vector4f& aAABB, const EObjectType& aStaticObjectTypeToGet, CU::GrowingArray<UniqueIndexType>& aArrayToFill)
{
	if (myIsLoading == true)
	{
		return false;
	}

	if (aStaticObjectTypeToGet == EObjectType::None)
	{
		return false;
	}

	CU::GrowingArray<QuadTreeData> objectsFound;
	myStaticQuadTree.GetObjectsCloseToAABB(aAABB, objectsFound);

	return GetObjectsImpl(objectsFound, aStaticObjectTypeToGet, aArrayToFill);
}

bool CLevel::GetStaticObjectsNear(const CU::Vector2f& aPoint, const EObjectType& aStaticObjectTypeToGet, CU::GrowingArray<UniqueIndexType>& aArrayToFill)
{
	if (myIsLoading == true)
	{
		return false;
	}

	if (aStaticObjectTypeToGet == EObjectType::None)
	{
		return false;
	}

	CU::GrowingArray<QuadTreeData> objectsFound;
	myStaticQuadTree.GetObjectsCloseToPoint(aPoint, objectsFound);

	return GetObjectsImpl(objectsFound, aStaticObjectTypeToGet, aArrayToFill);
}

bool CLevel::GetDynamicObjectsInside(const CU::Vector4f& aAABB, const EObjectType& aDynamicObjectTypeToGet, CU::GrowingArray<UniqueIndexType>& aArrayToFill)
{
	if (myIsLoading == true)
	{
		return false;
	}

	if (aDynamicObjectTypeToGet == EObjectType::None)
	{
		return false;
	}

	CU::GrowingArray<SGridData> objectsFound;
	myDynamicGrid.GetObjectsCloseToAABB(aAABB, objectsFound);

	return GetObjectsImpl(objectsFound, aDynamicObjectTypeToGet, aArrayToFill);
}

bool CLevel::GetDynamicObjectsNear(const CU::Vector2f& aPoint, const EObjectType& aDynamicObjectTypeToGet, CU::GrowingArray<UniqueIndexType>& aArrayToFill)
{
	if (myIsLoading == true)
	{
		return false;
	}

	if (aDynamicObjectTypeToGet == EObjectType::None)
	{
		return false;
	}

	CU::GrowingArray<SGridData> objectsFound;
	myDynamicGrid.GetObjectsCloseToPoint(aPoint, objectsFound);

	return GetObjectsImpl(objectsFound, aDynamicObjectTypeToGet, aArrayToFill);
}

void CLevel::LoadLevel(CMapData* aMapData)
{
	static std::mutex isLoadingMutex;
	static bool localIsLoading(false);
	static volatile bool forceQuitLevelLoading(true);
	static bool mapWasNullptr(false);

	std::unique_lock<std::mutex> uniqueLockLoading(isLoadingMutex);

	mapWasNullptr = (aMapData == nullptr);
	if (mapWasNullptr == true)
	{
		forceQuitLevelLoading = true;
		return;
	}

	if (localIsLoading == true)
	{
		forceQuitLevelLoading = true;
		while (localIsLoading == true)
		{
			uniqueLockLoading.unlock();
			std::this_thread::sleep_for(std::chrono::milliseconds(1));
			uniqueLockLoading.lock();
		}
	}

	localIsLoading = true;
	forceQuitLevelLoading = false;
	std::string nameOfLevel(myCurrentLevelName);
	CGameSystems::Get()->GetSystem<CRenderSystem>()->CancelCanvasFade();

	uniqueLockLoading.unlock();

	START_TIMER(loadTimer);
	int time(0);

	LoadLevelPreCleanup();

	const CMapData::SLevelInfo& levelInfo(aMapData->GetLevelInfo());

	const CMapData::SLevelInfo::SNavMesh* navMesh(levelInfo.GetNavMesh());
	if (navMesh != nullptr)
	{
		myNavMesh.Init(*navMesh);
	}
	else
	{
		GAME_LOG("ERROR! Failed to load data for navigation mesh.");
	}

	const auto& objectsReadFromMap(aMapData->GetObjects());
	const int totalAmountOfObjects(levelInfo.GetTotalAmountOfObjects());

	const auto& levelSize(levelInfo.GetMapSize());

	constexpr unsigned short quadTreeMaxAmountOfObjectsInNode(32);
	constexpr float gridCellSize(10.0f);

	constexpr float sizeMargin(100.0f); // TODO: Remove when size is read from Unity
	const CU::Vector4f containerSize(std::floorf(levelSize.myMin.x) - sizeMargin, std::floorf(levelSize.myMin.z) - sizeMargin
		, std::ceilf(levelSize.myMax.x) + sizeMargin, std::ceilf(levelSize.myMax.z) + sizeMargin);

	if (navMesh != nullptr)
	{
		CNavigationMeshWrapper::Get().BindNavigationMesh(myNavMesh, containerSize);
	}
	myStaticQuadTree.Init(quadTreeMaxAmountOfObjectsInNode, containerSize);
	myDynamicGrid.Init(gridCellSize, containerSize);

	myScriptInitsToBeInitiated.SetRawDataReserve(sce_newArray(SLevelInitFunction, totalAmountOfObjects), totalAmountOfObjects, 0);

	std::unordered_map<int, UniqueIndexType> unityToEntityBindMap;

	std::map<std::string, std::string> modelFilesInFolder;
	sce::CFileFinder::FindFilesInFolder(modelFilesInFolder, "fbx", "Data");
	for (auto& currentPair : modelFilesInFolder)
	{
		currentPair.second = "Data\\" + currentPair.second;
	}

	std::map<std::string, std::string> particleFilesInFolder;
	sce::CFileFinder::FindFilesInFolder(particleFilesInFolder, "particle", "Data");
	for (auto& currentPair : particleFilesInFolder)
	{
		currentPair.second = "Data\\" + currentPair.second;
	}

	for (int objectIndex = 0; objectIndex < levelInfo.GetRootAmountOfObjects(); ++objectIndex)
	{
		const CMapDataObject& currentDataObject(objectsReadFromMap[objectIndex]);

		LoadGameObject(&currentDataObject, &unityToEntityBindMap, &modelFilesInFolder, &particleFilesInFolder);

		if (forceQuitLevelLoading == true)
		{
			break;
		}
	}

	if (forceQuitLevelLoading == false)
	{
		for (auto& initFunction : myScriptInitsToBeInitiated)
		{
			for (auto entityIndex(0); entityIndex < initFunction.myAmountOfIndicesVariablesAdded; ++entityIndex)
			{
				auto foundIterator(unityToEntityBindMap.find(static_cast<int>(initFunction.myIndicesVariables[entityIndex])));

				if (foundIterator == unityToEntityBindMap.end())
				{
					GAME_LOG("WARNING! Level \"%s\": An object couldn't find bound object ID(C++ NaN C# %i) to a script index. Things will probably go wrong."
						, myCurrentLevelName.c_str(), initFunction.myIndicesVariables[entityIndex]);
				}
				else
				{
					initFunction.myIndicesVariables[entityIndex] = foundIterator->second;
				}
			}

			if (initFunction.myOnInitFunction(
				initFunction.myIndicesVariables, initFunction.myAmountOfIndicesVariablesAdded
				, initFunction.myNumberVariables, initFunction.myAmountOfNumberVariablesAdded
				, initFunction.myStringVariables, initFunction.myAmountOfStringVariablesAdded) == false)
			{
				GAME_LOG("ERROR! Level: Failed to do connecting script init on a object");
			}
		}
		myScriptInitsToBeInitiated.RemoveAll();
	}

	aMapData->Destroy();
	sce_delete(aMapData);
	END_TIMER(loadTimer, time);
	GAME_LOG("Level \"%s\" took %i ms to load"
		, myCurrentLevelName.c_str(), time);

	if (forceQuitLevelLoading == false)
	{
		myIsLoading = false;
	}
	else
	{
		if (mapWasNullptr == true)
		{
			myIsLoading = false;
		}
		GAME_LOG("Level \"%s\" loading was stopped"
			, nameOfLevel.c_str());
	}
	uniqueLockLoading.lock();
	localIsLoading = false;
	uniqueLockLoading.unlock();
}

void CLevel::LoadLevelPreCleanup()
{
	auto initRawData(myScriptInitsToBeInitiated.GetRawData());
	sce_delete(initRawData);
	myScriptInitsToBeInitiated.SetRawData(nullptr, 0);

	for (const auto& notCullable : myStaticUniqueIndicesNotCullable)
	{
		CScriptSystem::KillEntity_ThreadSafe(notCullable);
	}
	myStaticUniqueIndicesNotCullable.RemoveAll();

	CU::GrowingArray<QuadTreeData> quadTreeData;
	if (myStaticQuadTree.GetObjects(quadTreeData) == true)
	{
		for (const auto& quadData : quadTreeData)
		{
			CScriptSystem::KillEntity_ThreadSafe(quadData.myEntityUniqueIndex);
		}
	}

	myStaticQuadTree.Clear();

	const UniqueIndexType playerUniqueIndex(CGameLogicFunctions::GetPlayerUniqueIndex());

	CU::GrowingArray<UniqueIndexType> gridData;
	if (myDynamicGrid.GetObjects(gridData) == true)
	{
		UniqueIndexType index(0);
		for (const auto& uniqueIndex : gridData)
		{
			++index;
// 			if (uniqueIndex == playerUniqueIndex)
// 			{
//				// Save stuff?
//			}

			CScriptSystem::KillEntity_ThreadSafe(uniqueIndex);
		}
	}

	myDynamicGrid.Clear();

	myMapLoaded = true;
}

template<typename Variable, typename DataPair>
void LoadScriptVariable(CLevel& aLevel, const CMapDataObject& aDataObject, const char* aScriptPath, Variable*& aVariablesToSet, const DataPair* aVariablesData, const std::size_t& aAmountOfVariables, const Variable& aDefaultValue)
{
	aLevel; aDataObject; aScriptPath;

	if (aAmountOfVariables == 0)
	{
		return;
	}

	aVariablesToSet = sce_newArray(Variable, aAmountOfVariables);
	std::fill(aVariablesToSet, &aVariablesToSet[aAmountOfVariables], aDefaultValue);

	for (auto variableIndex(0); variableIndex < aAmountOfVariables; ++variableIndex)
	{
		const auto& currentVariable(aVariablesData[variableIndex]);

		if (currentVariable.HasValidValue() == true)
		{
			aVariablesToSet[variableIndex] = static_cast<Variable>(currentVariable.GetValue());
		}
		else
		{
			GAME_LOG("WARNING! Level \"%s\": GameObject(\"%s\", %i) has no bound value to script variable \"%s\", using script \"%s\". Things will probably go wrong."
				, aLevel.myCurrentLevelName.c_str(), aDataObject.GetName(), aDataObject.GetInstanceID(), currentVariable.GetName(), aScriptPath);
		}
	}
}

void CLevel::LoadGameObject(const void* aCurrentDataObject, void* aUnityToEntityBindMap, void* aModelFilesInFolder, void* aParticleFilesInFolder)
{
	enum class ECullableType : unsigned char
	{
		Static,
		Dynamic,
		StaticNotCullable,
		DynamicNotCullable,
	};

	const CMapDataObject& currentDataObject(*reinterpret_cast<const CMapDataObject*>(aCurrentDataObject));
	std::unordered_map<int, UniqueIndexType>& unityToEntityBindMap(*reinterpret_cast<std::unordered_map<int, UniqueIndexType>*>(aUnityToEntityBindMap));
	std::map<std::string, std::string>& modelFilesInFolder(*reinterpret_cast<std::map<std::string, std::string>*>(aModelFilesInFolder));
	std::map<std::string, std::string>& particleFilesInFolder(*reinterpret_cast<std::map<std::string, std::string>*>(aParticleFilesInFolder));

	bool scriptExists(false);
	ECullableType cullableType(ECullableType::Static);

	CEntityFactory::SEntityCreationData objectData;

	const CMapDataObject::STransform* const transform(currentDataObject.GetTransform());
	objectData.myPosition = transform->myPosition;
	objectData.myRotation = transform->myRotation;
	objectData.myScale = transform->myScale;

	if (currentDataObject.GetMeshName() != nullptr)
	{
		const std::string meshName(currentDataObject.GetMeshName() + std::string(".fbx"));
		const auto iterator(modelFilesInFolder.lower_bound(meshName));
		if ((iterator != modelFilesInFolder.end()) && (iterator->first == meshName))
		{
			objectData.myModelFilePath = iterator->second;
		}
		else
		{
			GAME_LOG("WARNING! Level \"%s\": GameObject(\"%s\", C# %i) model(\"%s\") doesn't exist"
				, myCurrentLevelName.c_str(), currentDataObject.GetName(), currentDataObject.GetInstanceID(), meshName.c_str());
		}
	}

	if (currentDataObject.GetMaterialName() != nullptr)
	{
		objectData.myMaterialFilePath = std::string("Data/Materials/") + currentDataObject.GetMaterialName();
	}
	else
	{
		objectData.myMaterialFilePath = "Data/Materials/DEFAULT.material";
	}

	if (currentDataObject.GetBoxCollider() != nullptr)
	{
		const auto& boxCollider(*currentDataObject.GetBoxCollider());

		if (boxCollider.myIsTrigger == true)
		{
			objectData.myTriggerBox.myIsValid = true;
			objectData.myTriggerBox.myMin = boxCollider.myMin;
			objectData.myTriggerBox.myMax = boxCollider.myMax;
			objectData.myPosition = objectData.myTriggerBox.myMin + ((objectData.myTriggerBox.myMax - objectData.myTriggerBox.myMin) / 2.0f);
		}
		else
		{
			GAME_LOG("WARNING! Level \"%s\": GameObject(\"%s\", %i) has a boxCollider component that is not marked as a trigger, this is not supported"
				, myCurrentLevelName.c_str(), currentDataObject.GetName(), currentDataObject.GetInstanceID());
		}
	}

	const auto& lights = currentDataObject.GetLights();
	if (lights.Empty() == false)
	{
		const auto& light = lights[0];
		objectData.myLightData.myColor = *light.myColor;
		objectData.myLightData.myIntensity = *light.myIntensity;

		switch (*light.myLightType)
		{
		case CMapDataObject::SLight::eLightType::DIRECTIONAL:
			objectData.myLightData.myDL.myDirection = light.myDirectionalLightData->myDirection;
			objectData.myLightData.myLightType = CEntityFactory::SEntityCreationData::SLightData::ELightType::DIRECTIONAL;
			cullableType = ECullableType::StaticNotCullable;
			break;
		case CMapDataObject::SLight::eLightType::POINT:
			objectData.myLightData.myPL.aRange = light.myPointLightData->aRange;
			objectData.myLightData.myLightType = CEntityFactory::SEntityCreationData::SLightData::ELightType::POINT;

			break;
		case CMapDataObject::SLight::eLightType::SPOT:
			objectData.myLightData.mySL.myRange = light.mySpotLightData->myRange;
			objectData.myLightData.mySL.myAngle = light.mySpotLightData->myAngle;
			objectData.myLightData.mySL.myDirection = light.mySpotLightData->myDirection;
			objectData.myLightData.myLightType = CEntityFactory::SEntityCreationData::SLightData::ELightType::SPOT;
			break;
		}
	}

	if (currentDataObject.GetParticleName() != nullptr)
	{
		const std::string particleName(currentDataObject.GetParticleName() + std::string(".particle"));
		const auto iterator(particleFilesInFolder.lower_bound(particleName));
		if ((iterator != particleFilesInFolder.end()) && (iterator->first == particleName))
		{
			objectData.myParticleData.myParticlePath = iterator->second;
			objectData.myParticleData.myPositionOffset = { 0.0f, 0.0f, 0.0f };
			objectData.myParticleData.myIsValid = true;
		}
		else
		{
			GAME_LOG("WARNING! Level \"%s\": GameObject(\"%s\", C# %i) particle(\"%s\") doesn't exist"
				, myCurrentLevelName.c_str(), currentDataObject.GetName(), currentDataObject.GetInstanceID(), particleName.c_str());
		}
	}

	const CMapDataObject::SScriptData& scriptData(currentDataObject.GetScript());
	scriptExists = scriptData.IsValid();

	SLevelInitFunction newInitFunction;

	if (scriptExists == true)
	{
		objectData.myScriptData.myScriptFilePath = scriptData.GetPath();

		if (objectData.myScriptData.myScriptFilePath.empty() == true)
		{
			GAME_LOG("WARNING! Level \"%s\": GameObject(\"%s\", %i) has a script component added but no script path assigned"
				, myCurrentLevelName.c_str(), currentDataObject.GetName(), currentDataObject.GetInstanceID());
			scriptExists = false;
		}

		const auto& amountOfIDVariables(scriptData.GetAmountOfIDVariables());
		const auto& amountOfNumberVariables(scriptData.GetAmountOfNumberVariables());
		const auto& amountOfStringVariables(scriptData.GetAmountOfStringVariables());
		newInitFunction.myAmountOfIndicesVariablesAdded = amountOfIDVariables;
		newInitFunction.myAmountOfNumberVariablesAdded = amountOfNumberVariables;
		newInitFunction.myAmountOfStringVariablesAdded = amountOfStringVariables;
		const auto totalAmountOfVariables(amountOfIDVariables + amountOfNumberVariables + amountOfStringVariables);

		if (totalAmountOfVariables > 0)
		{
			if (totalAmountOfVariables > CScriptSystem::MaxAmountOfAttachedVariables)
			{
				GAME_LOG("ERROR! Level \"%s\": GameObject(\"%s\", %i) has too many variables bound to its script"
					, myCurrentLevelName.c_str(), currentDataObject.GetName(), currentDataObject.GetInstanceID());
				assert("Check console/log" && (false));
			}

			LoadScriptVariable(*this, currentDataObject, scriptData.GetPath(), newInitFunction.myIndicesVariables, scriptData.GetIDVariableDatas(), amountOfIDVariables, EntityHandleINVALID.myUniqueIndex);
			objectData.myScriptData.myIndices = newInitFunction.myIndicesVariables;

			LoadScriptVariable(*this, currentDataObject, scriptData.GetPath(), newInitFunction.myNumberVariables, scriptData.GetNumberVariableDatas(), amountOfNumberVariables, -1);
			objectData.myScriptData.myNumbers = newInitFunction.myNumberVariables;

			LoadScriptVariable(*this, currentDataObject, scriptData.GetPath(), newInitFunction.myStringVariables, scriptData.GetStringVariableDatas(), amountOfStringVariables, std::string());
			objectData.myScriptData.myStrings = newInitFunction.myStringVariables;
		}
	}

	CU::Vector2f size(1.0f, 1.0f);

	if (currentDataObject.GetModelSize() != nullptr)
	{
		const CU::Vector3f& sizeRef(*currentDataObject.GetModelSize());
		size = { sizeRef.x, sizeRef.z };

		float largestComponent = (sizeRef.x > sizeRef.y) ? sizeRef.x : sizeRef.y;
		objectData.myRadius = (largestComponent > sizeRef.z) ? largestComponent : sizeRef.z;
	}

	SEntityHandle objectHandle(CEntityFactory::CreateEntity(objectData));

	if ((cullableType != ECullableType::DynamicNotCullable) && (cullableType != ECullableType::StaticNotCullable))
	{
		if (*currentDataObject.GetIsStatic() == 1)
		{
			cullableType = ECullableType::Static;
		}
		else
		{
			cullableType = ECullableType::Dynamic;
		}
	}

	{
		switch (cullableType)
		{
		case ECullableType::Static:
		{
			QuadTreeData data;
			data.myCenterPosition = { objectData.myPosition.x, objectData.myPosition.z };
			data.mySize = size;
			data.myEntityUniqueIndex = objectHandle.myUniqueIndex;
			data.myIsTrigger = objectData.myTriggerBox.myIsValid;

			myStaticQuadTree.AddObject(data);
			break;
		}
		case ECullableType::Dynamic:
		{
			SGridData data;
			data.myCenterPosition = { objectData.myPosition.x, objectData.myPosition.z };
			data.mySize = size;
			data.myEntityUniqueIndex = objectHandle.myUniqueIndex;
			data.myIsTrigger = objectData.myTriggerBox.myIsValid;

			QueueAddToActiveObjects(data);
			break;
		}
		case ECullableType::StaticNotCullable:
		{
			myStaticUniqueIndicesNotCullable.Add(objectHandle.myUniqueIndex);
			break;
		}
		case ECullableType::DynamicNotCullable:
		{
			assert("Dynamic not cullable not implemented, yet" && false);
			break;
		}
		default:
			assert("Cullable type has gone wrong" && false);
			break;
		}
	}

	unityToEntityBindMap.insert({ currentDataObject.GetInstanceID(), objectHandle.myUniqueIndex });

	if (scriptExists == true)
	{
		newInitFunction.myOnInitFunction = objectData.myScriptData.myOnInit;

		myScriptInitsToBeInitiated.Add(newInitFunction);
	}

	const auto& children(currentDataObject.GetChildren());
	for (auto& currentChild : children)
	{
		LoadGameObject(&currentChild, aUnityToEntityBindMap, aModelFilesInFolder, aParticleFilesInFolder);
	}
}

bool CLevel::GetObjectsImpl(const CU::GrowingArray<QuadTreeData>& aQuadTreeDatas, const EObjectType& aStaticObjectTypeToGet, CU::GrowingArray<UniqueIndexType>& aArrayToFill)
{
	const auto staticObjectComparer(
		[&aStaticObjectTypeToGet](const QuadTreeData& aQuadTreeData) -> bool
	{
		if (aStaticObjectTypeToGet == EObjectType::Trigger)
		{
			if (aQuadTreeData.myIsTrigger == true)
			{
				return true;
			}
		}
		else if (aStaticObjectTypeToGet == EObjectType::NoTrigger)
		{
			if (aQuadTreeData.myIsTrigger == false)
			{
				return true;
			}
		}
		else if (aStaticObjectTypeToGet == EObjectType::All)
		{
			return true;
		}

		return false;
	});

	for (const auto& currentObject : aQuadTreeDatas)
	{
		if (staticObjectComparer(currentObject) == true)
		{
			aArrayToFill.Add(currentObject.myEntityUniqueIndex);
		}
	}

	if ((aStaticObjectTypeToGet == EObjectType::All) || (aStaticObjectTypeToGet == EObjectType::NoTrigger))
	{
		aArrayToFill.Add(myStaticUniqueIndicesNotCullable);
	}

	return (aArrayToFill.Empty() == false);
}

bool CLevel::GetObjectsImpl(const CU::GrowingArray<SGridData>& aGridDatas, const EObjectType& aDynamicObjectTypeToGet, CU::GrowingArray<UniqueIndexType>& aArrayToFill)
{
	const auto dynamicObjectComparer(
		[&aDynamicObjectTypeToGet](const SGridData& aGridData) -> bool
	{
		if (aDynamicObjectTypeToGet == EObjectType::Trigger)
		{
			if (aGridData.myIsTrigger == true)
			{
				return true;
			}
		}
		else if (aDynamicObjectTypeToGet == EObjectType::NoTrigger)
		{
			if (aGridData.myIsTrigger == false)
			{
				return true;
			}
		}
		else if (aDynamicObjectTypeToGet == EObjectType::All)
		{
			return true;
		}

		return false;
	});

	for (const auto& currentObject : aGridDatas)
	{
		if (dynamicObjectComparer(currentObject) == true)
		{
			aArrayToFill.Add(currentObject.myEntityUniqueIndex);
		}
	}

// 	if ((aDynamicObjectTypeToGet == EObjectType::All) || (aDynamicObjectTypeToGet == EObjectType::NoTrigger))
// 	{
// 		aArrayToFill.Add(myStaticUniqueIndicesNotCullable);
// 	}

	return (aArrayToFill.Empty() == false);
}
