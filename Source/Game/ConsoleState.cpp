#include "stdafx.h"
#include "ConsoleState.h"

#include "../Script/ScriptManager.h"
#include "..\CommonUtilities\LevenshteinDistance.h"
#include "..\GraphicsEngine\ImGUI\imgui.h"
#include "..\GraphicsEngine\GraphicsEngineInterface.h"
#include <mutex>
#include <cctype>
#include <string>

static std::mutex myCommandLock;
static std::mutex myCommandListLock;
static std::string myCommandField = "";
static volatile unsigned short myCurrSelectedCmdIndex = USHRT_MAX;
static volatile bool myExecuteCommand = false;
static volatile bool myCommandWasExecuted = false;
static volatile bool myConsoleWindowWasJustOpened = false;


CU::GrowingArray<SInGameConsoleCommand, unsigned short> CConsoleState::mySimilarCommandList;
CU::GrowingArray<SInGameConsoleCommand, unsigned short> CConsoleState::myExecutedCommandList(256);

#define ADD_STATE(state) (myConsoleState = EState(myConsoleState | (state)));
#define HAS_STATE(state) ((myConsoleState & (state)) != 0)

CConsoleState::CConsoleState(EManager& aEntityManager)
	: CState(aEntityManager)
	, myCommand("")
	, myConsoleState(EState::eNothing)
{
}

CConsoleState::~CConsoleState()
{
	DetachFromEventReceiving();
}

void CConsoleState::Init()
{
	// Collect commands
	if (CScriptManager::Get() == nullptr)
	{
		RESOURCE_LOG("[CConsoleState] ERROR! Failed to get the list of exposed function because Script Manager was invalid (nullptr).");
		return;
	}

	const auto funcList(CScriptManager::Get()->GetRegisteredFunctionNamesOnlyConsole());
	myCommandList.Init((unsigned short)funcList.Size());
	mySimilarCommandList.Init((unsigned short)funcList.Size());
	SInGameConsoleCommand cmdToAdd;

	for (size_t i(0); i < funcList.Size(); ++i)
	{
		cmdToAdd.myCommand = funcList[i];
		cmdToAdd.myFuncName = funcList[i];

		myCommandList.Add(cmdToAdd);
	}

	myCommandField = "";
	myCurrSelectedCmdIndex = USHRT_MAX;
	myConsoleState = EState::eNothing;
	myExecuteCommand = false;
}

void CConsoleState::Update(const float aDeltaTime)
{
	aDeltaTime;

	if (HAS_STATE(EState::eCmdChanged))
	{
		myCurrSelectedCmdIndex = USHRT_MAX;

		if (myCommandField.size() > 0 && myCommandList.Size() > 0)
		{
			std::string commandField("");
			{
				std::lock_guard<std::mutex> lock(myCommandLock);
				commandField = myCommandField;
			}

			std::lock_guard<std::mutex> lock(myCommandListLock);
			mySimilarCommandList.RemoveAll();

			for (unsigned short i(0); i < myCommandList.Size(); ++i)
			{
				if (std::search(myCommandList[i].myCommand.begin(), myCommandList[i].myCommand.end(), commandField.begin(), commandField.end(),
					[](char ch1, char ch2) { return std::toupper(ch1) == std::toupper(ch2); }) != myCommandList[i].myCommand.end())
				{
					if (myCommandList[i].myCommand.size() != commandField.size())
					{
						mySimilarCommandList.Add(myCommandList[i]);
					}
				}
			}

			for (unsigned short i(0); i < mySimilarCommandList.Size(); ++i)
			{
				mySimilarCommandList[i].mySimilarityVal = CU::LevenshteinDistance(commandField, mySimilarCommandList[i].myCommand);
			}

			std::sort(mySimilarCommandList.GetRawData(), mySimilarCommandList.GetRawData() + mySimilarCommandList.Size(), CompareCommands);
		}
	}

	if (HAS_STATE(EState::eMoveSuggestionUp))
	{
		if (myCurrSelectedCmdIndex == USHRT_MAX)
		{
			myCurrSelectedCmdIndex = 0;
		}
		else if (myCurrSelectedCmdIndex <= 0)
		{
			myCurrSelectedCmdIndex = mySimilarCommandList.Size() - 1;
		}
		else
		{
			--myCurrSelectedCmdIndex;
		}
	}

	if (HAS_STATE(EState::eMoveSuggestionDown))
	{
		if (myCurrSelectedCmdIndex == USHRT_MAX)
		{
			myCurrSelectedCmdIndex = 0;
		}
		else if (myCurrSelectedCmdIndex >= mySimilarCommandList.Size() - 1)
		{
			myCurrSelectedCmdIndex = 0;
		}
		else
		{
			++myCurrSelectedCmdIndex;
		}
	}

	if (myConsoleState != EState::eNothing)
	{
		myConsoleState = EState::eNothing;
	}
}

void CConsoleState::Render()
{
}

void CConsoleState::OnActivation()
{
	myLog = "";
	myConsoleWindowWasJustOpened = true;
	sce::gfx::CGraphicsEngineInterface::SetConsoleRenderCallback(std::bind(&CConsoleState::ConsoleRenderCallback, this));
	AttachToEventReceiving(Event::ESubscribedEvents(Event::eKeyInput));
}

void CConsoleState::OnDeactivation()
{
	sce::gfx::CGraphicsEngineInterface::SetConsoleRenderCallback(nullptr);
	DetachFromEventReceiving();
}

void CConsoleState::ReceiveEvent(const Event::SEvent& aEvent)
{
	if (aEvent.myType == Event::EEventType::eKeyInput)
	{
		const Event::SKeyEvent* keyMsgPtr = reinterpret_cast<const Event::SKeyEvent*>(&aEvent);

		if (keyMsgPtr->state == EButtonState::Pressed)
		{
			if (keyMsgPtr->key == CU::CKeyBinds::GetHotKeys().myEscape || keyMsgPtr->key == CU::CKeyBinds::GetHotKeys().myTilde)
			{
				myState = CState::EStateCommand::eRemoveState;
			}
			else if (keyMsgPtr->key == CU::CKeyBinds::GetHotKeys().myEnterInput)
			{
				myExecuteCommand = true;
			}

			if (mySimilarCommandList.Size() > 0 && myCommand[0] > 0)
			{
				if (keyMsgPtr->key == CU::CKeyBinds::GetHotKeys().myArrowUp)
				{
					ADD_STATE(EState::eMoveSuggestionUp);
				}
				else if (keyMsgPtr->key == CU::CKeyBinds::GetHotKeys().myArrowDown)
				{
					ADD_STATE(EState::eMoveSuggestionDown);
				}
				else if (keyMsgPtr->key == CU::CKeyBinds::GetHotKeys().myTab)
				{
					if (myCurrSelectedCmdIndex == USHRT_MAX)
					{
						myCurrSelectedCmdIndex = 0;
					}

					myExecuteCommand = true;
				}
			}
		}
		else if (keyMsgPtr->state == EButtonState::Released)
		{
			if (keyMsgPtr->key == CU::CKeyBinds::GetHotKeys().myTilde)
			{
				myConsoleWindowWasJustOpened = false;
			}
		}
	}
	if (aEvent.myType == Event::EEventType::eMouseClickInput)
	{
		//const Event::SMouseClickEvent* clickPtr = reinterpret_cast<const Event::SMouseClickEvent*>(&aEvent);


	}
}

/* PRIVATE FUNCTIONS */

void CConsoleState::ConsoleRenderCallback()
{
	ImGui::SetNextWindowPos(ImVec2(10, 0));
	ImGui::SetNextWindowSize(ImVec2(sce::gfx::CGraphicsEngineInterface::GetFullWindowedResolution().x, 700), ImGuiSetCond_Appearing);
	ImGui::Begin("Console", 0);
	ImGui::BeginChild("Scrolling", { sce::gfx::CGraphicsEngineInterface::GetFullWindowedResolution().x - 40, 630}, true, ImGuiWindowFlags_ChildWindow);
	ImGui::TextUnformatted(myLog.c_str());

	ImGui::SetFontSize(ImGui::GetFontSize() * 1.5f);	

	if (myCommandWasExecuted)
	{
		if (ImGui::GetTextLineHeightWithSpacing() * myExecutedCommandList.size() > ImGui::GetWindowHeight())
		{
			ImGui::SetScrollY(ImGui::GetScrollY() + ImGui::GetTextLineHeightWithSpacing());
		}
	}

	for (unsigned short i(0); i < myExecutedCommandList.Size(); ++i)
	{
		ImGui::TextColored(ImVec4(1.0f, 1.0f, 1.0f, 1.0f), myExecutedCommandList[i].myCommand.c_str());
	}

	if (mySimilarCommandList.Size() > 0 && myCommand[0] > 0)
	{
		std::lock_guard<std::mutex> lock(myCommandListLock);

		ImGui::SetNextWindowPos(ImVec2(30, 450));
		ImGui::BeginChild("Suggestions", { sce::gfx::CGraphicsEngineInterface::GetFullWindowedResolution().x - 600, 200 }, true, ImGuiWindowFlags_ChildWindow);

		ImGui::SetFontSize(ImGui::GetFontSize() * 1.5f);
		for (unsigned short i(0); i < mySimilarCommandList.Size(); ++i)
		{
			if (i == myCurrSelectedCmdIndex)
			{
				ImGui::TextColored(ImVec4(0.0f, 1.0f, 0.0f, 1.0f), mySimilarCommandList[i].myCommand.c_str());
			}
			else
			{
				if (i == 0)
				{
					ImGui::TextColored(ImVec4(1.0f, 1.0f, 1.0f, 1.0f), mySimilarCommandList[i].myCommand.c_str());
				}
				else
				{
					ImGui::TextColored(ImVec4(0.5f, 0.5f, 0.5f, 1.0f), mySimilarCommandList[i].myCommand.c_str());
				}
			}
		}

		ImGui::EndChild();
	}

	myCommandWasExecuted = false;
	ImGui::EndChild();
	ImGui::Separator();

	{
		auto callbackFunc = [](ImGuiTextEditCallbackData* data)
		{
			if (myExecuteCommand)
			{
				data->DeleteChars(0, data->BufTextLen);

				if (myCurrSelectedCmdIndex != USHRT_MAX)
				{
					std::lock_guard<std::mutex> lock1(myCommandLock);
					std::lock_guard<std::mutex> lock2(myCommandListLock);

					myCommandField = mySimilarCommandList[(unsigned short)myCurrSelectedCmdIndex].myCommand;
					data->InsertChars(0, myCommandField.c_str(), myCommandField.c_str() + myCommandField.size());
				}
			}

			return 0;
		};

		if (!myConsoleWindowWasJustOpened)
		{
			ImGui::SetFontSize(ImGui::GetFontSize() * 1.5f);
			ImGui::SetKeyboardFocusHere();
			if (ImGui::InputText("command", myCommand, (sizeof(myCommand) / sizeof(myCommand[0])), ImGuiInputTextFlags_CallbackAlways, callbackFunc))
			{
				if (myExecuteCommand)
				{
					myExecuteCommand = false;

					if (myCurrSelectedCmdIndex == USHRT_MAX)
					{
						if (myExecutedCommandList.Size() == myExecutedCommandList.Capacity())
						{
							myExecutedCommandList.RemoveCyclicAtIndex(0);
						}

						myExecutedCommandList.Add(SInGameConsoleCommand(myCommandField, myCommandField, 0));

						if ((myCommandField == "help") || (myCommandField == "?"))
						{
							GAME_LOG("Commands available:");
							for (const auto& command : myCommandList)
							{
								GAME_LOG(command.myCommand.c_str());
								myExecutedCommandList.Add(command);
							}
						}
						else
						{
							std::string commandUsed(myCommandField);
							if (CallCommand(commandUsed) == false)
							{
								GAME_LOG("Command: Failed to call \"%s\""
									, commandUsed.c_str());
							}

							myCommandWasExecuted = true;

							GAME_LOG("Command: Pushed command: \"%s\""
								, commandUsed.c_str());
						}
					}
					else
					{
						myCurrSelectedCmdIndex = USHRT_MAX;
					}
				}
				else
				{
					std::lock_guard<std::mutex> lock(myCommandLock);
					myCommandField = std::string(myCommand);
				}

				ADD_STATE(EState::eCmdChanged);
			}
		}
	}

	ImGui::End();
}

bool CConsoleState::CallCommand(std::string& aCommandField)
{
	CScriptManager* scriptManager(CScriptManager::Get());
	if (scriptManager == nullptr)
	{
		return false;
	}

	std::string& convertedCommandField(aCommandField);
	
	auto firstSpace(convertedCommandField.find_first_of(' '));
	if (firstSpace != convertedCommandField.npos)
	{
		convertedCommandField[firstSpace] = '(';

		auto firstQuote(convertedCommandField.find_first_of('\"'));
		
		while ((firstSpace = convertedCommandField.find_first_of(' ', firstSpace)) != convertedCommandField.npos)
		{
			if (firstSpace < firstQuote)
			{
				convertedCommandField[firstSpace] = ',';
			}
			else
			{
				firstSpace = convertedCommandField.find_first_of('\"', (firstQuote + 1));
				firstQuote = convertedCommandField.find_first_of('\"', (firstSpace + 1));
			}
		}
	}
	else
	{
		convertedCommandField += '(';
	}

	convertedCommandField += ')';

	const bool success(scriptManager->CallCPPFunctionAsIfFromLuaSTRING(convertedCommandField));
	return success;
}

const bool CConsoleState::CompareCommands(const SInGameConsoleCommand& aCmd1, const SInGameConsoleCommand& aCmd2)
{
	return aCmd1.mySimilarityVal < aCmd2.mySimilarityVal;
}
