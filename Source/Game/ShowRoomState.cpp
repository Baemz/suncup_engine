#include "stdafx.h"
#include "ShowRoomState.h"

#include "..\EngineCore\FileFinder\FileFinder.h"
#include "..\EngineCore\Logger\Logger.h"

#include "..\GraphicsEngine\Model\ModelInstance.h"
#include "..\GraphicsEngine\Model\Loading\ModelFactory.h"
#include "..\GraphicsEngine\Camera\CameraInstance.h"
#include "..\GraphicsEngine\Camera\CameraFactory.h"
#include "..\GraphicsEngine\Lights\EnvironmentalLight.h"
#include "..\GraphicsEngine\Lights\DirectionalLight.h"
#include "..\GraphicsEngine\GraphicsEngineInterface.h"
#include "..\GraphicsEngine\Text\Text.h"

#include <sys\types.h>
#include <sys\stat.h>
#include <Windows.h>
#include "..\GraphicsEngine\DirectXFramework\Direct3D11.h"

#include "..\GraphicsEngine\DebugTools.h"

#define DEFAULT_DISTANCE_BETWEEN_OBJECTS (50.0f)
#define MIN_DISTANCE_BETWEEN_OBJECTS (10.0f)
#define MIN_ZOOM_DISTANCE (5.0f)
#define INCREASE_OBJ_ROTATION_SPEED_MOD (1.2f)
#define DECREASE_OBJ_ROTATION_SPEED_MOD (0.8f)
#define MOUSE_OBJ_ROTATION_MOD (0.001f)
#define MOUSE_SCROLL_ZOOM_SPEED_MOD (100.0f)
#define CAM_ZOOM_SPEED_MOD (0.001f)
#define CAM_MOVE_SPEED_MOD (0.001f)

#define GET_CURR_MODEL_CAM_POSITION (myCurrModelPtr->GetPosition() - CU::Vector3f( 0.0f, 0.0f, MIN_ZOOM_DISTANCE + myCurrModelPtr->GetRadius() ))

CShowRoomState::CShowRoomState(EManager& aEntityManager, const bool aRunInDebugMode)
	: CState(aEntityManager)
	, myModelList(nullptr)
	, myCurrModelPtr(nullptr)
	, mySkyboxPtr(nullptr)
	, myCamInstancePtr(nullptr)
	, myEnvLightPtr(nullptr)
	, myDirLightPtr(nullptr)
	, myModelDescTextPtr(nullptr)
	, myCommandState(ECommands::eNone)
	, myObjRotationSpeed(5.0f)
	, myCamMoveSpeed(0.0f)
	, myCamZoomSpeed(0.0f)
	, myZoomDirectionFromMouse(0.0f)
	, myModelsCount(0)
	, myMaxModelsCount(0)
	, myPrevModelIndex(0)
	, myCurrModelIndex(0)
	, myMode(EMoveMode::eStatic)
	, myDoneReordering(false)
	, myRunInDebugMode(aRunInDebugMode)
{
}

CShowRoomState::~CShowRoomState()
{
	sce::gfx::CGraphicsEngineInterface::DisableCursor();
	DetachFromEventReceiving();

	myCurrModelPtr = nullptr;
	sce_delete(myModelDescTextPtr);
	sce_delete(myDirLightPtr);
	sce_delete(myEnvLightPtr);
	sce_delete(myCamInstancePtr);
	sce_delete(mySkyboxPtr);
	DeleteAllModels();
}

void CShowRoomState::Init()
{
	sce::gfx::CGraphicsEngineInterface::EnableCursor();
	AttachToEventReceiving(Event::ESubscribedEvents(Event::eKeyInput | Event::eMouseClickInput | Event::eMouseInput));

	sce_delete(myCamInstancePtr);
	myCamInstancePtr = sce_new(sce::gfx::CCameraInstance);
	*myCamInstancePtr = sce::gfx::CCameraFactory::Get()->CreateCameraAtPosition({ 0.0f, 0.0f, 0.0f });

	sce_delete(mySkyboxPtr);
	mySkyboxPtr = sce_new(sce::gfx::CModelInstance);
	*mySkyboxPtr = sce::gfx::CModelFactory::Get()->CreateSkyboxCube();
	mySkyboxPtr->Init();

	sce_delete(myEnvLightPtr);
	myEnvLightPtr = sce_new(sce::gfx::CEnvironmentalLight);
	myEnvLightPtr->Init("Data/Models/Misc/test_cubemap1.dds");
	myEnvLightPtr->UseForRendering();

	sce_delete(myDirLightPtr);
	myDirLightPtr = sce_new(sce::gfx::CDirectionalLight);
	myDirLightPtr->myLightData.myToLightDirection = {0.0f, 0.0f, -1.0f, 0.0f};
	myDirLightPtr->myLightData.myColor = { 1.0f, 1.0f, 1.0f, 1.0f };
	myDirLightPtr->myLightData.myIntensity = 1.0f;

	sce_delete(myModelDescTextPtr);
	myModelDescTextPtr = sce_new(sce::gfx::CText);
	myModelDescTextPtr->Init(
		/*"Comic Sans MS"*/"Data/Fonts/Adventure Subtitles.ttf",
		"  NONE SELECTED",
		24.0f,
		CU::Vector2f(0.0f),
		CU::Vector2f(0.99f, 0.01f),
		sce::gfx::CText::ColorFloatToUint(1.0f, 0.0f, 0.0f, 1.0f),
		sce::gfx::EFontAlignment::eRight
	);

	LoadAllModels();

	myDoneReordering = false;
	myPrevModelIndex = USHRT_MAX;
}

void CShowRoomState::Update(const float aDeltaTime)
{
	ReorderModelsWhenReady();

	if (myPrevModelIndex != myCurrModelIndex || (myCurrModelPtr != nullptr && myModelList[myCurrModelIndex].myPrevLODIndex != myModelList[myCurrModelIndex].myCurrLODIndex))
	{
		if (myPrevModelIndex != myCurrModelIndex)
		{
			myModelList[myCurrModelIndex].myCurrLODIndex = 0;
		}

		myPrevModelIndex = myCurrModelIndex;
		myModelList[myCurrModelIndex].myPrevLODIndex = myModelList[myCurrModelIndex].myCurrLODIndex;
		myCurrModelPtr = &myModelList[myCurrModelIndex].myLODList[myModelList[myCurrModelIndex].myCurrLODIndex];

		myCamTargetPosition = GET_CURR_MODEL_CAM_POSITION;

		const auto& model(myModelList[myCurrModelIndex]);
		const auto& text(model.myPathList[model.myCurrLODIndex]);
		myModelDescTextPtr->SetText(text);
	}

	MoveCameraToTargetPosition(aDeltaTime);
	UpdateControls(aDeltaTime);
	UpdateStates();

	myCommandState = ECommands::eNone;
	myCamInstancePtr->Update(aDeltaTime);
}

void CShowRoomState::Render()
{
	//printf("%i - Cam pos = x: %f  y: %f  z: %f\n", myCurrModelIndex, myCamInstancePtr->GetPosition().x, myCamInstancePtr->GetPosition().y, myCamInstancePtr->GetPosition().z);

	if (mySkyboxPtr->IsLoaded() && mySkyboxPtr->GetModelID() != UINT_MAX)
	{
		mySkyboxPtr->Render();
	}

	myCamInstancePtr->UseForRendering();
	myEnvLightPtr->UseForRendering();
	myDirLightPtr->UseForRendering();

	if (myRunInDebugMode)
	{
		myModelDescTextPtr->Render();
	}

	for (unsigned short i(0); i < myModelsCount; ++i)
	{
		for (unsigned short j(0); j < myModelList[i].myLODsCount; ++j)
		{
			if (myModelList[i].myLODList[j].IsLoaded() && myModelList[i].myLODList[j].GetModelID() != UINT_MAX)
			{
				myModelList[i].myLODList[j].Render();
			}
		}
	}
}

void CShowRoomState::OnActivation()
{
	sce::gfx::CGraphicsEngineInterface::ToggleLinearFog();
}

void CShowRoomState::OnDeactivation()
{
	sce::gfx::CGraphicsEngineInterface::ToggleLinearFog();
}

void CShowRoomState::ReceiveEvent(const Event::SEvent& aEvent)
{
	switch (aEvent.myType)
	{
	case Event::EEventType::eKeyInput:
	{
		const Event::SKeyEvent* keyMsgPtr = reinterpret_cast<const Event::SKeyEvent*>(&aEvent);

		if (keyMsgPtr->state == EButtonState::Pressed)
		{
			if (keyMsgPtr->key == VK_F1) // USE OTHER KEY, F1 IS USED FOR TOGGLING THE WIREFRAME.
			{
				myMode = (myMode == EMoveMode::eStatic) ? EMoveMode::eFree : EMoveMode::eStatic;
			}

			if (keyMsgPtr->key == VK_F1)
			{
				if (sce::gfx::CDirect3D11::GetAPI() != nullptr)
				{
					sce::gfx::CDirect3D11::GetAPI()->SetShouldRenderWireFrame();
				}
				else
				{
					GAME_LOG("[CShowRoomState] ERROR! direct 3d11 was not initialized! Idk how tf...");
				}
			}
			else if (keyMsgPtr->key == VK_LEFT)
			{
				if (myCurrModelIndex > 0)
				{
					--myCurrModelIndex;
				}
				else
				{
					myCurrModelIndex = myModelsCount - 1;
				}
			}
			else if (keyMsgPtr->key == VK_RIGHT)
			{
				if (myCurrModelIndex < myModelsCount - 1)
				{
					++myCurrModelIndex;
				}
				else
				{
					myCurrModelIndex = 0;
				}
			}

			if (myCurrModelPtr != nullptr)
			{
				if (keyMsgPtr->key == VK_DOWN)
				{
					if (myModelList[myCurrModelIndex].myCurrLODIndex > 0)
					{
						--myModelList[myCurrModelIndex].myCurrLODIndex;
					}
					else
					{
						myModelList[myCurrModelIndex].myCurrLODIndex = myModelList[myCurrModelIndex].myLODsCount - 1;
					}
				}
				else if (keyMsgPtr->key == VK_UP)
				{
					if (myModelList[myCurrModelIndex].myCurrLODIndex < myModelList[myCurrModelIndex].myLODsCount - 1)
					{
						++myModelList[myCurrModelIndex].myCurrLODIndex;
					}
					else
					{
						myModelList[myCurrModelIndex].myCurrLODIndex = 0;
					}
				}
			}

			if (keyMsgPtr->key == VK_RETURN)
			{
				myCommandState |= ECommands::eResetRotation;
			}
			else if (keyMsgPtr->key == VK_BACK)
			{
				myCommandState |= ECommands::eResetCamPosition;
			}
			else if (keyMsgPtr->key == VK_OEM_PLUS)
			{
				myCommandState |= ECommands::eIncreaseRotationSpeed;
			}
			else if (keyMsgPtr->key == VK_OEM_MINUS)
			{
				myCommandState |= ECommands::eDecreaseRotationSpeed;
			}
		}

		if (keyMsgPtr->state == EButtonState::Down)
		{
			if (keyMsgPtr->key == VK_CONTROL)
			{
				myCommandState |= ECommands::eMoveCamera;
			}

			if (keyMsgPtr->key == 'A' || keyMsgPtr->key == VK_NUMPAD4)
			{
				myCommandState |= ECommands::eRotateXClockWise;
			}
			else if (keyMsgPtr->key == 'D' || keyMsgPtr->key == VK_NUMPAD6)
			{
				myCommandState |= ECommands::eRotateXCClockWise;
			}
			if (keyMsgPtr->key == 'W' || keyMsgPtr->key == VK_NUMPAD8)
			{
				myCommandState |= ECommands::eRotateYClockWise;
			}
			else if (keyMsgPtr->key == 'S' || keyMsgPtr->key == VK_NUMPAD2)
			{
				myCommandState |= ECommands::eRotateYCClockWise;
			}
			if (keyMsgPtr->key == 'Q' || keyMsgPtr->key == VK_NUMPAD7)
			{
				myCommandState |= ECommands::eRotateZClockWise;
			}
			else if (keyMsgPtr->key == 'E' || keyMsgPtr->key == VK_NUMPAD9)
			{
				myCommandState |= ECommands::eRotateZCClockWise;
			}
		}
		break;
	}
	case Event::EEventType::eMouseClickInput:
	{
		const Event::SMouseClickEvent* mouseMsgPtr = reinterpret_cast<const Event::SMouseClickEvent*>(&aEvent);

		if (mouseMsgPtr->key == 1 && mouseMsgPtr->state == EButtonState::Down)
		{
			myDeltaFromMouse.x += -(float)mouseMsgPtr->deltaX;
			myDeltaFromMouse.y += -(float)mouseMsgPtr->deltaY;
		}
		if (mouseMsgPtr->key == 2 && mouseMsgPtr->state == EButtonState::Down)
		{
			myZoomDirectionFromMouse += (float)(mouseMsgPtr->deltaX + mouseMsgPtr->deltaY);
		}
		break;
	}
	case Event::EEventType::eMouseInput:
	{
		const Event::SMouseDataEvent* mouseMsgPtr = reinterpret_cast<const Event::SMouseDataEvent*>(&aEvent);

		if (mouseMsgPtr->scrollDelta != 0)
		{
			myZoomDirectionFromMouse += (float)mouseMsgPtr->scrollDelta * MOUSE_SCROLL_ZOOM_SPEED_MOD;
		}

		break;
	}
	}
}

/* PRIVATE FUNCTIONS */

void CShowRoomState::ReorderModelsWhenReady()
{
	if (myDoneReordering) return;

	myDoneReordering = true;
	for (unsigned short i(0); i < myModelsCount; ++i)
	{
		for (unsigned short j(0); j < myModelList[i].myLODsCount; ++j)
		{
			if (!myModelList[i].myLODList[j].IsLoaded() && !myModelList[i].myLODList[j].LoadingFailed())
			{
				myDoneReordering = false;
				break;
			}
		}
	}

	if (myDoneReordering)
	{
		unsigned short lastLODIndex(0);
		sce::gfx::CModelInstance* currModelPtr(nullptr);

		// Remove unloaded models before reordering.
		for (unsigned short i(myModelsCount); i-- > 0;)
		{
			for (unsigned short j(myModelList[i].myLODsCount); j-- > 0;)
			{
				currModelPtr = &myModelList[i].myLODList[j];

				if (currModelPtr->LoadingFailed())
				{
					if (myModelList[i].myCurrLODIndex == j && (myModelList[i].myPrevLODIndex != USHRT_MAX || myModelList[i].myCurrLODIndex != 0))
					{
						myModelList[i].myPrevLODIndex = USHRT_MAX;
						myModelList[i].myCurrLODIndex = 0;
					}

					--myModelList[i].myLODsCount;
					lastLODIndex = myModelList[i].myLODsCount;

					if (lastLODIndex > 0)
					{
						myModelList[i].myLODList[j] = myModelList[i].myLODList[lastLODIndex];
						myModelList[i].myLODList[lastLODIndex] = *currModelPtr;
					}
					else
					{
						--myModelsCount;
						SModels modelToRemove = myModelList[i];
						myModelList[i] = myModelList[myModelsCount];
						myModelList[myModelsCount] = modelToRemove;

						if (myCurrModelIndex == i && myCurrModelIndex != 0)
						{
							myCurrModelIndex = 0;
						}
					}
				}
			}
		}

		float xOffset(0.0f);
		float yOffset(0.0f);
		for (unsigned short i(0); i < myModelsCount; ++i)
		{
			yOffset = 0.0f;

			for (unsigned short j(0); j < myModelList[i].myLODsCount; ++j)
			{
				currModelPtr = &myModelList[i].myLODList[j];

				yOffset += currModelPtr->GetRadius();
				xOffset += currModelPtr->GetRadius();

				currModelPtr->SetPosition({ (MIN_DISTANCE_BETWEEN_OBJECTS + xOffset) * i, (MIN_DISTANCE_BETWEEN_OBJECTS + yOffset) * j, 0.0f });
			}
		}

		myPrevModelIndex = USHRT_MAX;
	}
}

void CShowRoomState::MoveCameraToTargetPosition(const float aDeltaTime)
{
	if (myCamTargetPosition.x == 0.0f && myCamTargetPosition.y == 0.0f && myCamTargetPosition.z == 0.0f) return;

//#ifdef DEBUG_SHOWROOM
	myCamInstancePtr->SetPosition(myCamTargetPosition);
	myCamTargetPosition.x = 0.0f;
	myCamTargetPosition.y = 0.0f;
	myCamTargetPosition.z = 0.0f;
	aDeltaTime;
//#endif
//#ifndef DEBUG_SHOWROOM
//	myCamTargetDirection = myCamTargetPosition - myCamInstancePtr->GetPosition();
//
//	const float targetDistance(myCamTargetDirection.Length2());
//	if (targetDistance > 8.0f)
//	{
//		myCurrCamMoveSpeed = targetDistance;
//	}
//
//	if (targetDistance > 0.01f)
//	{
//		myCamTargetDirection.Normalize();
//
//		myCamInstancePtr->Move(myCamTargetDirection, myCurrCamMoveSpeed * aDeltaTime);
//	}
//	else
//	{
//		myCamInstancePtr->SetPosition(myCamTargetPosition);
//		myCamTargetPosition.x = 0.0f;
//		myCamTargetPosition.y = 0.0f;
//		myCamTargetPosition.z = 0.0f;
//		myCurrCamMoveSpeed = 0.0f;
//	}
//#endif
}

void CShowRoomState::UpdateStates()
{
	// Rotation reset
	if ((myCommandState & ECommands::eResetRotation) != 0)
	{
		CU::Vector3f prevPosition(myCurrModelPtr->GetPosition());

		myCurrModelPtr->SetOrientation(CU::Matrix44f::Identity);
		myCurrModelPtr->SetPosition(prevPosition);
	}

	if ((myCommandState & ECommands::eIncreaseRotationSpeed) != 0)
	{
		myObjRotationSpeed *= INCREASE_OBJ_ROTATION_SPEED_MOD;
	}
	if ((myCommandState & ECommands::eDecreaseRotationSpeed) != 0)
	{
		myObjRotationSpeed *= DECREASE_OBJ_ROTATION_SPEED_MOD;
	}
}

void CShowRoomState::UpdateControls(const float aDeltaTime)
{
	//if (myCurrCamMoveSpeed != 0.0f) return;

	if (myZoomDirectionFromMouse != 0.0f)
	{
		if (myCurrModelPtr != nullptr)
		{
			myCamZoomSpeed = CAM_ZOOM_SPEED_MOD * myCurrModelPtr->GetRadius();
		}
		else
		{
			myCamZoomSpeed = 0.0f;
		}

		myCamInstancePtr->Move({ 0.0f, 0.0f, myZoomDirectionFromMouse }, myCamZoomSpeed);
		myZoomDirectionFromMouse = 0.0f;
	}

	if (myCamInstancePtr->GetPosition().z > MIN_ZOOM_DISTANCE + myCurrModelPtr->GetRadius())
	{
		myCamInstancePtr->SetPosition(CU::Vector3f(myCamInstancePtr->GetPosition().x, myCamInstancePtr->GetPosition().y, MIN_ZOOM_DISTANCE + myCurrModelPtr->GetRadius()));
	}

	// Rotation
	if ((myCommandState & ECommands::eRotateXClockWise) != 0)
	{
		myCurrObjRotation.x = 1.0f;
	}
	else if ((myCommandState & ECommands::eRotateXCClockWise) != 0)
	{
		myCurrObjRotation.x = -1.0f;
	}
	if ((myCommandState & ECommands::eRotateYClockWise) != 0)
	{
		myCurrObjRotation.y = 1.0f;
	}
	else if ((myCommandState & ECommands::eRotateYCClockWise) != 0)
	{
		myCurrObjRotation.y = -1.0f;
	}
	if ((myCommandState & ECommands::eRotateZClockWise) != 0)
	{
		myCurrObjRotation.z = 1.0f;
	}
	else if ((myCommandState & ECommands::eRotateZCClockWise) != 0)
	{
		myCurrObjRotation.z = -1.0f;
	}

	if (myCurrObjRotation.x != 0.0f || myCurrObjRotation.y != 0.0f || myCurrObjRotation.z != 0.0f)
	{
		myCurrModelPtr->Rotate(myCurrObjRotation * myObjRotationSpeed * aDeltaTime);
		myCurrObjRotation.x = 0.0f;
		myCurrObjRotation.y = 0.0f;
		myCurrObjRotation.z = 0.0f;
	}

	// Move
	if (myDeltaFromMouse.x != 0.0f || myDeltaFromMouse.y != 0.0f || myDeltaFromMouse.z != 0.0f)
	{
		if ((myCommandState & ECommands::eMoveCamera) != 0)
		{
			if (myCurrModelPtr != nullptr)
			{
				myCamMoveSpeed = CAM_MOVE_SPEED_MOD * myCurrModelPtr->GetRadius();
			}
			else
			{
				myCamMoveSpeed = 0.0f;
			}

			myCamInstancePtr->Move(CU::Vector3f(myDeltaFromMouse.x, -myDeltaFromMouse.y, myDeltaFromMouse.z), myCamMoveSpeed);
		}
		else
		{
			myCurrModelPtr->Rotate(myDeltaFromMouse * myObjRotationSpeed * MOUSE_OBJ_ROTATION_MOD);
		}

		myDeltaFromMouse.x = 0.0f;
		myDeltaFromMouse.y = 0.0f;
		myDeltaFromMouse.z = 0.0f;
	}

	if ((myCommandState & ECommands::eResetCamPosition) != 0)
	{
		if (myCurrModelPtr != nullptr)
		{
			myCamInstancePtr->SetPosition(GET_CURR_MODEL_CAM_POSITION);
		}
	}
}

void CShowRoomState::LoadAllModels()
{
	CU::GrowingArray<CU::GrowingArray<std::string>> filePathList;
	unsigned short latestModLODIndex;
	filePathList.Init(32);

	GetEveryModelPathAndLatestModelIndex(filePathList, myCurrModelIndex, latestModLODIndex);

	if (filePathList.Size() <= 0 && myCurrModelIndex == UINT_MAX)
	{
		GAME_LOG("[CShowRoomState] WARNING! Could not find any models inside model folder.");
		return;
	}
	else if (filePathList.Size() > 0 && myCurrModelIndex == UINT_MAX)
	{
		GAME_LOG("[CShowRoomState] ERROR! Failed to get latest modified model even though there are models inside model folder.");
	}

	DeleteAllModels();
	myModelsCount = filePathList.Size();
	myMaxModelsCount = myModelsCount;
	myModelList = sce_newArray(SModels, myModelsCount);
	
	for (unsigned short i(0); i < myModelsCount; ++i)
	{
		myModelList[i].myLODsCount = filePathList[i].Size();
		myModelList[i].myLODList = sce_newArray(sce::gfx::CModelInstance, myModelList[i].myLODsCount);
		myModelList[i].myPathList = sce_newArray(std::string, myModelList[i].myLODsCount);

		for (unsigned short j(0); j < myModelList[i].myLODsCount; ++j)
		{
			if (i == myCurrModelIndex && j == latestModLODIndex)
			{
				myModelList[i].myCurrLODIndex = latestModLODIndex;
			}

			auto& currPathText_ref(myModelList[i].myPathList[j]);
			currPathText_ref = filePathList[i][j];

			auto& currLOD_ref(myModelList[i].myLODList[j]);
			currLOD_ref.SetModelPath(filePathList[i][j]);
			currLOD_ref.SetPosition({ DEFAULT_DISTANCE_BETWEEN_OBJECTS * i, DEFAULT_DISTANCE_BETWEEN_OBJECTS * j, 0.0f });
			currLOD_ref.Init();
		}
	}
}

void CShowRoomState::GetEveryModelPathAndLatestModelIndex(CU::GrowingArray<CU::GrowingArray<std::string>>& aAllFilePathList,
	unsigned short& aLatestModModelIndexRef, unsigned short& aLatestModLODIndexRef)
{
	CU::GrowingArray<std::string> allFilePathList;
	allFilePathList.Init(16);

	sce::CFileFinder::GetFilePathsRecursive(allFilePathList, ".fbx", "Data\\Models\\");

	CU::GrowingArray<std::string> filePathLODList;
	filePathLODList.Init(16);
	std::string cutLODName("");
	size_t namePos(std::string::npos);
	size_t typePos(std::string::npos);
	bool foundPlaceForLODPath(false);

	for (unsigned short i(0); i < allFilePathList.Size(); ++i)
	{
		if (allFilePathList[i].find_last_of('@') != std::string::npos)
		{
			continue;
		}

		namePos = allFilePathList[i].find_last_of('\\');
		if (namePos != std::string::npos)
		{
			typePos = allFilePathList[i].rfind("_LOD");
			if (typePos != std::string::npos && typePos > namePos)
			{
				cutLODName = allFilePathList[i].substr(namePos, typePos - namePos);
				foundPlaceForLODPath = false;

				for (unsigned short j(0); j < aAllFilePathList.Size(); ++j)
				{
					if (aAllFilePathList[j][0].find(cutLODName) != std::string::npos)
					{
						aAllFilePathList[j].Add(allFilePathList[i]);
						foundPlaceForLODPath = true;
						break;
					}
				}

				if (!foundPlaceForLODPath)
				{
					filePathLODList.Add(allFilePathList[i]);
				}
			}
			else
			{
				aAllFilePathList.Add(CU::GrowingArray<std::string>());
				aAllFilePathList.GetLast().Init(8);
				aAllFilePathList.GetLast().Add(allFilePathList[i]);
			}
		}
		else
		{
			GAME_LOG("[CShowRoomState] ERROR! Could not find \"\\\" in a file path: \"%s\"", allFilePathList[i].c_str());
		}
	}

	for (unsigned short i(0); i < filePathLODList.Size(); ++i)
	{
		namePos = filePathLODList[i].find_last_of('\\');
		typePos = filePathLODList[i].rfind("_LOD");
		cutLODName = filePathLODList[i].substr(namePos, typePos - namePos);
		foundPlaceForLODPath = false;

		for (unsigned short j(0); j < aAllFilePathList.Size(); ++j)
		{
			if (aAllFilePathList[j][0].find(cutLODName) != std::string::npos)
			{
				aAllFilePathList[j].Add(filePathLODList[i]);
				foundPlaceForLODPath = true;
				break;
			}
		}

		if (!foundPlaceForLODPath)
		{
			GAME_LOG("[CShowRoomState] WARNING! Found a LOD model without main model. It will not be loaded. Path: \"%s\".", filePathLODList[i].c_str());
		}
	}
	allFilePathList.RemoveAll();
	filePathLODList.RemoveAll();

	if (myRunInDebugMode)
	{
		long long latestTime(0);
		struct _stat result;

		for (unsigned short i(0); i < aAllFilePathList.Size(); ++i)
		{
			for (unsigned short j(0); j < aAllFilePathList[i].Size(); ++j)
			{
				if (_stat(aAllFilePathList[i][j].c_str(), &result) == 0)
				{
					if (latestTime < result.st_mtime)
					{
						latestTime = result.st_mtime;
						aLatestModModelIndexRef = i;
						aLatestModLODIndexRef = j;
					}
				}
			}
		}
	}
	else
	{
		aLatestModModelIndexRef = 0;
		aLatestModLODIndexRef = 0;
	}
}

void CShowRoomState::DeleteAllModels()
{
	for (unsigned short i(0); i < myMaxModelsCount; ++i)
	{
		sce_delete(myModelList[i].myPathList);
		sce_delete(myModelList[i].myLODList);
		myModelList[i].myLODsCount = 0;
	}
	sce_delete(myModelList);
	myModelsCount = 0;
}
