#include "stdafx.h"
#include "EntityFactory.h"
#include "EntitySystemLink/EntitySystemLink.h"
#include "../Script/ScriptManager.h"
#include "GameSystems/ScriptSystem.h"
#include "../GraphicsEngine/Camera/CameraFactory.h"

#define ourEntityManager (*(EManager**)(&ourEntityManagerVoid))

EntityManager* CEntityFactory::ourEntityManagerVoid = nullptr;

SEntityHandle CEntityFactory::CreateEntity(SEntityCreationData& aEntityCreationData)
{
	if (ourEntityManager == nullptr)
	{
		static SEntityHandle invalid;
		GAME_LOG("ERROR! Entity factory has no entity manager to draw from.");
		return invalid;
	}

	SEntityHandle entityHandle(ourEntityManager->CreateHandle());

	CU::Matrix44f entityOrientation;
	entityOrientation.SetPosition(aEntityCreationData.myPosition);

	CU::Quaternion rotation;
	rotation.RotateWorld(aEntityCreationData.myRotation);
	entityOrientation.SetRotation(rotation.GenerateMatrix());

	entityOrientation.ScaleBy(aEntityCreationData.myScale);

	ourEntityManager->AddComponent<CompTransform>(entityHandle, entityOrientation);
	CompTransform& transform(ourEntityManager->GetComponent<CompTransform>(entityHandle));
	transform.myLookRotation = rotation;
	transform.myRadius = aEntityCreationData.myRadius;

	if (aEntityCreationData.myModelFilePath.empty() == false)
	{
		if (InitModel(entityHandle, aEntityCreationData.myModelFilePath, aEntityCreationData.myMaterialFilePath) == false)
		{
			GAME_LOG("ERROR! Something went wrong when loading model \"%s\""
				, aEntityCreationData.myModelFilePath.c_str());
		}
	}
	if (aEntityCreationData.myScriptData.myScriptFilePath.empty() == false)
	{
		if (InitScript(entityHandle, aEntityCreationData.myScriptData, rotation) == false)
		{
			GAME_LOG("ERROR! Something went wrong when loading script \"%s\""
				, aEntityCreationData.myScriptData.myScriptFilePath.c_str());
		}
	}
	if (aEntityCreationData.myTriggerBox.myIsValid == true)
	{
		if (InitTrigger(entityHandle, aEntityCreationData.myTriggerBox) == false)
		{
			GAME_LOG("ERROR! Something went wrong when loading a trigger");
		}
	}
	if (aEntityCreationData.myLightData.myLightType == SEntityCreationData::SLightData::ELightType::DIRECTIONAL)
	{
		if (InitDirectionalLight(entityHandle, aEntityCreationData.myLightData.myDL.myDirection,
			aEntityCreationData.myLightData.myColor,
			aEntityCreationData.myLightData.myIntensity) == false)
		{
			GAME_LOG("ERROR! Something went wrong when loading directional light");
		}
	}
	else if (aEntityCreationData.myLightData.myLightType == SEntityCreationData::SLightData::ELightType::POINT)
	{
		if (InitPointLight(entityHandle, aEntityCreationData.myPosition,
			aEntityCreationData.myLightData.myColor,
			aEntityCreationData.myLightData.myPL.aRange,
			aEntityCreationData.myLightData.myIntensity) == false)
		{
			GAME_LOG("ERROR! Something went wrong when loading point light");
		}
	}
	else if (aEntityCreationData.myLightData.myLightType == SEntityCreationData::SLightData::ELightType::SPOT)
	{
		if (InitSpotLight(entityHandle, aEntityCreationData.myPosition,
			aEntityCreationData.myLightData.myColor,
			aEntityCreationData.myLightData.mySL.myDirection,
			aEntityCreationData.myLightData.mySL.myRange,
			aEntityCreationData.myLightData.mySL.myAngle,
			aEntityCreationData.myLightData.myIntensity) == false)
		{
			GAME_LOG("ERROR! Something went wrong when loading point light");
		}
	}
	else if (aEntityCreationData.myParticleData.myIsValid == true)
	{
		if (InitParticle(entityHandle, aEntityCreationData.myParticleData) == false)
		{
			GAME_LOG("ERROR! Something went wrong when loading particle");
		}
	}

	return entityHandle;
}

SEntityHandle CEntityFactory::CreateEntityCamera(const CU::Vector3f& aPosition, const CU::Vector3f& aRotation)
{
	SEntityHandle camera(ourEntityManager->CreateHandle());
	ourEntityManager->ResetCounter(camera);
	ourEntityManager->AddComponent<CompCameraInstance>(camera);
	ourEntityManager->AddTag<TagCamera>(camera);

	auto initCamera = [&aPosition, &aRotation](auto&, CompCameraInstance& aCameraComponent)
	{
		aCameraComponent.myInstance = sce::gfx::CCameraFactory::Get()->CreateCameraAtPosition(aPosition);
		aCameraComponent.myInstance.SetPosition(aPosition);
		aCameraComponent.myInstance.RotateWorld(aRotation);
	};

	ourEntityManager->RunIfMatching<SigCamera>(camera, initCamera);

	return camera;
}



bool CEntityFactory::InitModel(SEntityHandle& aEntityHandle, const std::string& aModelPath, const std::string& aMaterialPath)
{
	CompModelInstance& model = ourEntityManager->AddComponent<CompModelInstance>(aEntityHandle);
	model.myModel.SetModelPath(aModelPath);
	if (aMaterialPath.empty() == false)
	{
		model.myModel.SetMaterialPath(aMaterialPath);
	}

	return true;
}

template<typename Function, typename ArgsPointer, typename... ArgsUnpacked>
inline std::enable_if_t < CScriptSystem::MaxAmountOfAttachedVariables < sizeof...(ArgsUnpacked), void>
	ExpandPointer(Function&, const ArgsPointer, const std::size_t&, std::size_t&, ArgsUnpacked&&...)
{
	assert("Too many objects attached to an object, pls tell a progg" && (false));
}

template<typename Function, typename ArgsPointer, typename... ArgsUnpacked>
inline std::enable_if_t<CScriptSystem::MaxAmountOfAttachedVariables >= sizeof...(ArgsUnpacked), void>
ExpandPointer(Function& aFunction, const ArgsPointer aArgsPointer, const std::size_t& aArgsAmountMaxIndex, std::size_t& aArgsAmount, ArgsUnpacked&&... aArgsUnpacked)
{
	if (aArgsAmount == 0)
	{
		aFunction(std::forward<ArgsUnpacked>(aArgsUnpacked)...);
	}
	else
	{
		--aArgsAmount;
		ExpandPointer(aFunction, aArgsPointer, aArgsAmountMaxIndex, aArgsAmount, std::forward<ArgsUnpacked>(aArgsUnpacked)..., aArgsPointer[aArgsAmountMaxIndex - aArgsAmount]);
	}
}

bool CEntityFactory::InitScript(SEntityHandle& aEntityHandle, SEntityCreationData::SScriptData& aScriptData, const CU::Quaternion& aQuaternion)
{
	SControllerWrapperInit initData(SControllerInit(), EController::Script, SScriptInit());
	CompController& controllerComponent = ourEntityManager->AddComponent<CompController>(aEntityHandle, initData);
	ourEntityManager->AddTag<TagScriptController>(aEntityHandle);
	auto& controller(controllerComponent.Get());
	controller.myScriptFilePath = aScriptData.myScriptFilePath;
	controller.myScriptIndices = aScriptData.myIndices;
	controller.myScriptNumbers = aScriptData.myNumbers;
	controller.myScriptStrings = aScriptData.myStrings;
	controller.myLoaded = CController::ELoadState::Loading;
	controller.myTargetLookRotation = aQuaternion;

	CScriptManager* scriptManager(CScriptManager::Get());
	if (scriptManager == nullptr)
	{
		GAME_LOG("ERROR! No scriptmanager found!");
		return false;
	}

	auto initScriptComponent(
		[scriptManager, aEntityHandle, &controllerComponent, aScriptData](const UniqueIndexType* aEntityUniqueIndices, const std::size_t& aAmountOfUniqueIndicesAdded, const int* aNumbers, const std::size_t& aAmountOfNumbers, const std::string* aStrings, const std::size_t& aAmountOfStrings) -> bool
	{
		auto onReloadScript(
			[scriptManager, aEntityHandle, &controllerComponent, aEntityUniqueIndices, aAmountOfUniqueIndicesAdded, aNumbers, aAmountOfNumbers, aStrings, aAmountOfStrings](const CScriptManager::StateIndexType& aStateIndex) -> void
		{
			controllerComponent.Get().myLoaded = CController::ELoadState::Loading;

			constexpr char* initFunctionName("Init");

			auto callScriptInit(
				[&scriptManager, &aStateIndex, &controllerComponent, &initFunctionName, &uniqueIndex = aEntityHandle.myUniqueIndex](auto... aArgs)
			{
				auto initiatedCallback(
					[&controllerComponent](const CScriptManager::StateIndexType& aStateIndex)
				{
					aStateIndex;
					controllerComponent.Get().myLoaded = CController::ELoadState::Loaded;
				});

				scriptManager->CallFunction(aStateIndex, initiatedCallback, initFunctionName, uniqueIndex, std::forward<decltype(aArgs)>(aArgs)...);
			});

			auto expandNumbers(
				[&callScriptInit, &aNumbers, &aAmountOfNumbers](auto... aArgs)
			{
				std::size_t amountOfNumbers(aAmountOfNumbers);
				ExpandPointer(callScriptInit, aNumbers, (amountOfNumbers - 1), amountOfNumbers, std::forward<decltype(aArgs)>(aArgs)...);
			});

			auto expandStrings(
				[&expandNumbers, &aStrings, &aAmountOfStrings](auto... aArgs)
			{
				std::size_t amountOfStrings(aAmountOfStrings);
				ExpandPointer(expandNumbers, aStrings, (amountOfStrings - 1), amountOfStrings, std::forward<decltype(aArgs)>(aArgs)...);
			});

			std::size_t amountOfUniqueIndices(aAmountOfUniqueIndicesAdded);
			ExpandPointer(expandStrings, aEntityUniqueIndices, (amountOfUniqueIndices - 1), amountOfUniqueIndices);
		});

		if (scriptManager->NewState(aScriptData.myScriptFilePath, controllerComponent.Get().myScriptStateIndex, onReloadScript) == false)
		{
			GAME_LOG("ERROR! Could not initate script: %s", aScriptData.myScriptFilePath.c_str());
			return false;
		}

		CScriptSystem::RegisterScriptComponent(controllerComponent.Get().myScriptStateIndex, aEntityHandle);
		onReloadScript(controllerComponent.Get().myScriptStateIndex);

		return true;
	});

	aScriptData.myOnInit = initScriptComponent;

	return true;
}

bool CEntityFactory::InitTrigger(SEntityHandle& aEntityHandle, const SEntityCreationData::STriggerBox& aTriggerBox)
{
	CompTrigger& trigger(ourEntityManager->AddComponent<CompTrigger>(aEntityHandle));
	trigger.myMin = aTriggerBox.myMin;
	trigger.myMax = aTriggerBox.myMax;

	return true;
}

bool CEntityFactory::InitPointLight(SEntityHandle& aEntityHandle, const CU::Vector3f& aPosition, const CU::Vector3f& aColor, const float aRange, const float aIntensity)
{
	CompPointLight& pl = ourEntityManager->AddComponent<CompPointLight>(aEntityHandle);
	pl.myPointLight.Init(aPosition, aColor, aRange, aIntensity);
	return true;
}

bool CEntityFactory::InitSpotLight(SEntityHandle& aEntityHandle, const CU::Vector3f& aPosition, const CU::Vector3f& aColor, const CU::Vector3f& aDirection, const float aRange, const float aAngle, const float aIntensity)
{
	CompSpotLight& sl = ourEntityManager->AddComponent<CompSpotLight>(aEntityHandle);
	sl.mySpotLight.Init(aPosition, aColor, aDirection, aAngle, aRange, aIntensity);
	return true;
}

bool CEntityFactory::InitDirectionalLight(SEntityHandle& aEntityHandle, const CU::Vector3f& aDirection, const CU::Vector3f& aColor, const float aIntensity)
{
	CompDirectionalLight& dl = ourEntityManager->AddComponent<CompDirectionalLight>(aEntityHandle);
	CU::Vector3f color = aColor;
	dl.myDirLight.myLightData.myToLightDirection = CU::Vector4f(aDirection.x, aDirection.y, aDirection.z, 0.0f);
	dl.myDirLight.myLightData.myColor = CU::Vector4f(color.x, color.y, color.z, 1.0f);
	dl.myDirLight.myLightData.myIntensity = aIntensity;
	return true;
}

bool CEntityFactory::InitParticle(SEntityHandle& aEntityHandle, SEntityCreationData::SParticleData& aParticleData)
{
	sce::gfx::CParticleInterface* particleInterface(sce::gfx::CParticleInterface::Get());

	if (particleInterface == nullptr)
	{
		return false;
	}

	CompParticleEmitter& pe(ourEntityManager->AddComponent<CompParticleEmitter>(aEntityHandle, aParticleData.myParticlePath.c_str(), aParticleData.myPositionOffset));
	if (pe.myParticleID == sce::gfx::ParticleIDTypeINVALID)
	{
		ourEntityManager->DelComponent<CompParticleEmitter>(aEntityHandle);
		aParticleData.myIsValid = false;
		return false;
	}

	return true;
}
