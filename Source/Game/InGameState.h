#pragma once
#include "State.h"
#include "../EngineCore/EventSystem/EventReceiver.h"
#include "../GraphicsEngine/Camera/CameraInstance.h"
#include "../GraphicsEngine/Model/ModelInstance.h"
#include "Level.h"

#include "../EntitySystem/Handle.h"

#define USE_GRID_CULLING

namespace sce { namespace gfx {
	class CText;
}}  


class CInGameState: private CState, public sce::CEventReceiver
{
public:
	CInGameState(EManager& aEntityManager);
	~CInGameState();

	void Init() override;
	void Update(const float aDeltaTime) override;
	void Render() override;

	void OnActivation() override;
	void OnDeactivation() override;
	void ReceiveEvent(const Event::SEvent& aEvent) override;

private:
	bool RegisterFunctionsFunction();

	void GetEntitiesToUse();

	SEntityHandle myCamera;
	const CU::Matrix44f* myCameraProjection;

	//SEntityHandle myInputCharacter;

	CU::Vector3f myCameraEulerRotation;
	float myDeltaTime;

	//sce::gfx::CCameraInstance myCamera;
	//sce::gfx::CModelInstance myModel;

	CLevel myLevel;

	float myRefreshEntitiesTimer;
	static constexpr float myRefreshEntitesFrequency = 0.5f;

	CU::Vector4f myFrustumViewAABB;
	CU::GrowingArray<CU::Vector3f> myFrustumIntersections;
	bool myShouldRenderFrustumColliders;

	CU::GrowingArray<UniqueIndexType> myEntities;

};