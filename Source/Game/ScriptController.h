#pragma once
#include "Controller.h"

class CScriptController : public CController
{
public:
	enum class ELoadState
	{
		Unloaded,
		Loading,
		Loaded
	};

	CScriptController() { myControllerType = EController::Script; }
	CScriptController(const SControllerInit& aInitData, const SScriptInit& aScriptInitData);

	//std::string myScriptFilePath;
	//unsigned int myScriptStateIndex;
	//ELoadState myLoaded;
};