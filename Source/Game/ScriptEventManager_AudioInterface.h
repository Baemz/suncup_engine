#pragma once

class CScriptEventManager;

class CScriptEventManager_AudioInterface
{
public:
	static bool InitFunctions(CScriptEventManager& aScriptEventManager);

private:
	CScriptEventManager_AudioInterface() = delete;
	~CScriptEventManager_AudioInterface() = delete;

};
