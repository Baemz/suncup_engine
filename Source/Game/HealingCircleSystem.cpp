#include "stdafx.h"
#include "HealingCircleSystem.h"
#include "GameSystems\ExposedFunctions\GameLogicFunctions.h"
#include "GameSystems\ScriptSystem.h"
#include "..\GraphicsEngine\DebugTools.h"
#include "GameSystems\GameSystems.h"
#include "DamageZoneSystem.h"


CHealingCircleSystem::CHealingCircleSystem(EManager& aEntityManager)
	: CSystemAbstract<CHealingCircleSystem>(aEntityManager)
{
}

void CHealingCircleSystem::Activate()
{
}

void CHealingCircleSystem::Deactivate()
{
}

void CHealingCircleSystem::Update(const float aDeltaTime, const CU::GrowingArray<const CU::GrowingArray<UniqueIndexType>*>& aEntities)
{
	aEntities;
	auto gameSystem(CGameSystems::Get());

	if (gameSystem == nullptr)
	{
		GAME_LOG("ERROR! GameSystem has not been activated")
			return;
	}

	auto damageZoneSystem(gameSystem->GetSystem<CDamageZoneSystem>());

	if (damageZoneSystem == nullptr)
	{
		GAME_LOG("ERROR! DamageZoneSystem has not been activated")
			return;
	}

	std::unique_lock<std::mutex> tossedUniqueLock(myTossedHealingMutex);
	for (unsigned short i = myTossedHealings.Size(); i-- > 0;)
	{
		if (myTossedHealings[i].myFlightTime >= 0.f)
		{	
			myTossedHealings[i].myDamageZoneSpawnInterval -= aDeltaTime;
			myTossedHealings[i].myFlightTime -= aDeltaTime;
			myTossedHealings[i].myPosition += (myTossedHealings[i].myDirection * myTossedHealings[i].myFlightSpeed) * aDeltaTime;
			while (myTossedHealings[i].myDamageZoneSpawnInterval <= 0.f)
			{
				myTossedHealings[i].myDamageZoneSpawnInterval += myTossedHealings[i].myDamageZoneSpawnIntervalOriginal;
				damageZoneSystem->CreateDamageZone(myTossedHealings[i].myPosition, myTossedHealings[i].myDamageLifeTime, myTossedHealings[i].myDamageCircleRadius, myTossedHealings[i].myDamagePerSecond);
				CGameLogicFunctions::SpawnParticleOnPosition("healDmg", myTossedHealings[i].myPosition);
				CGameLogicFunctions::SpawnParticleOnPosition("healDmgGlow", myTossedHealings[i].myPosition);
				CGameLogicFunctions::SpawnParticleOnPosition("healDmgRain", { myTossedHealings[i].myPosition.x, myTossedHealings[i].myPosition.y + 1.f, myTossedHealings[i].myPosition.z });
			}
			//DT_DRAW_SPHERE_DEF_COLOR_1_ARGS(myTossedHealings[i].myPosition);
		}
		else
		{
			CreateHealingCircle(myTossedHealings[i].myToPos, myTossedHealings[i].myHealLifeTime, myTossedHealings[i].myHealCircleRadius, myTossedHealings[i].myHealingPerTick, myTossedHealings[i].myHealingTickInterval);
			CGameLogicFunctions::SpawnParticleOnPosition("healWings", myTossedHealings[i].myToPos);
			CGameLogicFunctions::SpawnParticleOnPosition("healGlow", myTossedHealings[i].myToPos);
			CGameLogicFunctions::SpawnParticleOnPosition("healBeaming", { myTossedHealings[i].myToPos.x, myTossedHealings[i].myToPos.y + 1.f, myTossedHealings[i].myToPos.z });
			myTossedHealings.RemoveCyclicAtIndex(i);
			continue;
		}
	}
	tossedUniqueLock.unlock();


	const UniqueIndexType playerIndex = CGameLogicFunctions::GetPlayerUniqueIndex();
	const UniqueIndexType supportIndex = CGameLogicFunctions::GetSupportUniqueIndex();
	if (playerIndex == EntityHandleINVALID.myUniqueIndex || supportIndex == EntityHandleINVALID.myUniqueIndex)
	{
		return;
	}

	const CU::Vector3f playerPos = myEntityManager.GetComponent<CompTransform>(SEntityHandle(playerIndex)).myPosition;
	const CU::Vector3f suppPos = myEntityManager.GetComponent<CompTransform>(SEntityHandle(supportIndex)).myPosition;

	std::unique_lock<std::mutex> circleUniqueLock(myHealingCircleMutex);
	for (unsigned short i = myHealingCircles.Size(); i-- > 0;)
	{
		myHealingCircles[i].myLifeTime -= aDeltaTime;
		if (myHealingCircles[i].myLifeTime <= 0.0f)
		{
			myHealingCircles.RemoveCyclicAtIndex(i);
			continue;
		}
		myHealingCircles[i].myHealingTickInterval -= aDeltaTime;
		CU::Vector3f color(0.f, 1.f, 0.f);
		//DRAW_SPHERE_WITH_A_FREAKING_COLOR(myHealingCircles[i].myPosition, myHealingCircles[i].myRadius, color);
		const float playerDistToCircleSquared = (myHealingCircles[i].myPosition - playerPos).Length2();
		const float supportDistToCircleSquared = (myHealingCircles[i].myPosition - suppPos).Length2();

		if (playerDistToCircleSquared <= (myHealingCircles[i].myRadius * myHealingCircles[i].myRadius))
		{
			while (myHealingCircles[i].myHealingTickInterval <= 0.f)
			{
				myHealingCircles[i].myHealingTickInterval += myHealingCircles[i].myHealingTickIntervalOriginal;
				CScriptSystem::CallScriptEventOnTarget(playerIndex, "OnDamageTaken", (-myHealingCircles[i].myHealingPerTick));
			}
			
		}
		if (supportDistToCircleSquared <= (myHealingCircles[i].myRadius * myHealingCircles[i].myRadius))
		{
			//supporttakedmg -dmg (heal)
		}
	}
	circleUniqueLock.unlock();
}

void CHealingCircleSystem::TossHealing(const CU::Vector3f& aFromPos, const CU::Vector3f& aToPos, 
	const float aHealLifeTime, const float aHealCircleRadius, const float aHealingPerTick, const float aHealingTickInterval,
	const float aDamageLifeTime, const float aDamageCircleRadius, const float aDamagePerSecond, const float aDamageZoneSpawnInterval, const float aFlightSpeed)
{
	const CU::Vector3f entityToMouse = aToPos - aFromPos;
	const float distance = entityToMouse.Length();
	const float flightTime = distance / aFlightSpeed;
	const CU::Vector3f direction = entityToMouse.GetNormalized();
	const STossedHealing tossedHealing = STossedHealing(aFromPos, aToPos, direction, aHealLifeTime, aHealCircleRadius, aHealingPerTick, aHealingTickInterval, aDamageLifeTime, aDamageCircleRadius, aDamagePerSecond, aDamageZoneSpawnInterval, flightTime, aFlightSpeed);
	std::unique_lock<std::mutex> circleUniqueLock(myTossedHealingMutex);
	myTossedHealings.Add(tossedHealing);
}

void CHealingCircleSystem::CreateHealingCircle(const CU::Vector3f& aPosition, const float aLifeTime, const float aRadius, const float aHealingPerTick, const float aHealingPerTickInterval)
{
	const SHealingCircle healingCircle = SHealingCircle(aPosition, aLifeTime, aRadius, aHealingPerTick, aHealingPerTickInterval);
	std::unique_lock<std::mutex> circleUniqueLock(myHealingCircleMutex);
	myHealingCircles.Add(healingCircle);
}


