#include "stdafx.h"
#include "TestScene.h"
#include "..\GraphicsEngine\Camera\CameraFactory.h"
#include "..\GraphicsEngine\GraphicsEngineInterface.h"
#include "..\GraphicsEngine\Model\Loading\ModelFactory.h"
#include "..\GraphicsEngine\DirectXFramework\Direct3D11.h"
#include "States\ConsoleState.h"
#include "StateStack.h"
#include "..\GraphicsEngine\Lights\SpotLightInstance.h"


CTestScene::CTestScene(EManager& aEntityManager)
	: CState(aEntityManager)
	, usePointLight(false)
{
	AttachToEventReceiving(Event::ESubscribedEvents(Event::eKeyInput));
}


CTestScene::~CTestScene()
{
	DetachFromEventReceiving();
}

void CTestScene::Init()
{
	sce::gfx::CGraphicsEngineInterface::EnableCursor();
	myCamInst = sce::gfx::CCameraFactory::Get()->CreateCameraAtPosition({ 0.f, 0.f, -10.f });
	myCamInst.SetPosition({ 0.f, 20.f, -10.f });
	myCamInst.RotateLocal(CU::Vector3f(45.f, 0.f, 0.f));
	mymdl.SetModelPath("Data/Models/Player/Player/mdl_player.fbx");
	mymdl.SetMaterialPath("Data/Materials/player.material");
	mymdl.SetPosition({ 0.f, -0.1f, 0.f });
	mymdl.SetIsCullable(false);
	mymdl.Init();

	myFloor1.SetModelPath("Data/Models/Misc/plane_debug.fbx");
	myFloor1.SetMaterialPath("Data/Materials/cobblestone.material");
	myFloor1.SetScale({ 10.f, 10.f, 10.f });
	myFloor1.SetPosition({ -5.f, -1.3f, 0.f });
	myFloor1.SetIsCullable(false);
	myFloor1.Init();
	myFloor2.SetModelPath("Data/Models/Misc/plane_debug.fbx");
	myFloor2.SetMaterialPath("Data/Materials/cobblestone.material");
	myFloor2.SetScale({ 10.f, 10.f, 10.f });
	myFloor2.SetPosition({ 5.f, -1.3f, 0.f });
	myFloor2.SetIsCullable(false);
	myFloor2.Init();
	myFloor3.SetModelPath("Data/Models/Misc/plane_debug.fbx");
	myFloor3.SetMaterialPath("Data/Materials/cobblestone.material");
	myFloor3.SetScale({ 10.f, 10.f, 10.f });
	myFloor3.SetPosition({ -5.f, -1.3f, 10.f });
	myFloor3.SetIsCullable(false);
	myFloor3.Init();
	myFloor4.SetModelPath("Data/Models/Misc/plane_debug.fbx");
	myFloor4.SetMaterialPath("Data/Materials/cobblestone.material");
	myFloor4.SetScale({ 10.f, 10.f, 10.f });
	myFloor4.SetPosition({ 5.f, -1.3f, 10.f });
	myFloor4.SetIsCullable(false);
	myFloor4.Init();


	knife.SetModelPath("Data/Models/models/Dagger_N5.fbx");
	knife.SetMaterialPath("Data/Materials/dagger.material");
	knife.SetScale({ 0.1f, 0.1f, 0.1f });
	knife.SetPosition({ 4.f, 2.f, 0.f });
	knife.SetIsCullable(false);
	knife.Init(); 

	couch.SetModelPath("Data/Models/enemy_robot_walk.fbx");
	couch.SetMaterialPath("Data/Materials/robot.material");
	couch.SetPosition({ -3.f, -1.0f, 0.f });
	couch.SetIsCullable(false);
	couch.Init();

	robot2.SetModelPath("Data/Models/ugly_geo1@bind.fbx");
	robot2.SetMaterialPath("Data/Materials/chrome.material");
	robot2.SetPosition({ 4.f, -1.0f, 4.f });
	robot2.SetIsCullable(false);
	robot2.Init(); 
	
	robot3.SetModelPath("Data/Models/enemy_robot_walk.fbx");
	robot3.SetMaterialPath("Data/Materials/robot.material");
	robot3.SetPosition({ -3.f, -1.0f, 4.f });
	robot3.SetIsCullable(false);
	robot3.Init();
	
	myEnvLight.Init("Data/Models/Misc/lake_skybox.dds");
	mySpotLight.Init({ -3.f, 1.5f, 0.f }, { 0.f, 0.f, 1.f }, {0.f, -1.f, 0.f}, 10.f, 5.f, 50.f);
	myPointLight2.Init({ 4.f, 1.0f, 0.f }, { 1.f, 0.f, 0.f }, 4.f, 4.f);
	myPointLight3.Init({ 4.f, -0.5f, 4.f }, { 0.f, 1.f, 0.f }, 4.f, 4.f);
	myPointLight4.Init({ -3.f, -0.5f, 4.f }, { 1.f, 1.f, 0.f }, 4.f, 4.f);

	myskybox = sce::gfx::CModelFactory::Get()->CreateSkyboxCube("Data/Models/Misc/lake_skybox.dds");
}

static CU::Vector3f spPos(-3.f, 1.5f, 0.f);
static CU::Vector3f plPos2(4.f, 0.0f, 0.f);
static CU::Vector3f plPos3(4.f, -0.5f, 4.f);
static CU::Vector3f plPos4(-3.f, -0.5f, 4.f);
static float angle = 0.f;
void CTestScene::Update(const float aDeltaTime)
{
	dt = aDeltaTime;
	//knife.Update(aDeltaTime + (0.1f * aDeltaTime));
	//couch.Update(aDeltaTime + (0.2f * aDeltaTime));
	robot3.Update(aDeltaTime + (0.8f * aDeltaTime));
	robot2.Update(aDeltaTime + (0.4f * aDeltaTime));
	if (usePointLight)
	{
		angle += 4.f * aDeltaTime;

		CU::Vector3f pos;
		pos.x = sinf(angle);
		pos.z = cosf(angle);
		pos *= 0.5f;
		mySpotLight.SetPosition(spPos + pos);

		pos.x = sinf(angle + 10.f);
		pos.z = cosf(angle + 10.f);
		pos *= 2.0f;
		myPointLight2.SetPosition(plPos2 + pos);

		pos.x = sinf(angle - 2.f);
		pos.z = cosf(angle - 2.f);
		pos *= 1.0f;
		myPointLight3.SetPosition(plPos3 + pos);

		pos.x = sinf(angle - 6.f);
		pos.z = cosf(angle - 6.f);
		pos *= 1.2f;
		myPointLight4.SetPosition(plPos4 + pos);
	}
}

void CTestScene::Render()
{
	myCamInst.UseForRendering(); 
	myEnvLight.UseForRendering();
	if (usePointLight)
	{
		mySpotLight.Render();
		myPointLight2.Render();
		myPointLight3.Render();
		myPointLight4.Render();
	}
	myskybox.Render();
	mymdl.Render();
	myFloor1.Render();
	myFloor2.Render();
	myFloor3.Render();
	myFloor4.Render();
	knife.Render();
	couch.Render();
	robot3.Render();
	robot2.Render();
	myDirLight.UseForRendering();

}

void CTestScene::OnActivation()
{
}

void CTestScene::OnDeactivation()
{
}

static unsigned int ind = 0;

void CTestScene::ReceiveEvent(const Event::SEvent& aEvent)
{
	if (aEvent.myType == Event::EEventType::eKeyInput)
	{
		const Event::SKeyEvent* keyMsgPtr = reinterpret_cast<const Event::SKeyEvent*>(&aEvent);

		if (keyMsgPtr->state == EButtonState::Down)
		{
			if (keyMsgPtr->key == 'W')
			{
				myCamInst.Move(CU::Vector3f::Forward, dt);
			}
			else if (keyMsgPtr->key == 'Z')
			{
				myCamInst.RotateLocal(CU::Vector3f(1.f, 0.f, 0.f) * dt);
			}
			else if (keyMsgPtr->key == 'X')
			{
				myCamInst.RotateLocal(CU::Vector3f(-1.f, 0.f, 0.f) * dt);
			}
			else if (keyMsgPtr->key == 'S')
			{
				myCamInst.Move(CU::Vector3f::Forward, -dt);
			}
			else if (keyMsgPtr->key == 'A')
			{
				myCamInst.Move(CU::Vector3f::Right, -dt);
			}
			else if (keyMsgPtr->key == 'D')
			{
				myCamInst.Move(CU::Vector3f::Right, dt);
			}
			else if (keyMsgPtr->key == 'Q')
			{
				knife.Rotate(CU::Vector3f(0.0f, 0.4f, 0.f) * dt);
				//myCamInst.RotateLocal(CU::Vector3f(0.f, -1.f, 0.f) * dt);
			}
			else if (keyMsgPtr->key == 'E')
			{
				knife.Rotate(CU::Vector3f(0.0f, -0.4f, 0.f) * dt);
				//myCamInst.RotateLocal(CU::Vector3f(0.f, 1.f, 0.f) * dt);
			}
			else if (keyMsgPtr->key == 0x26)
			{
				CU::Matrix44f mat = CU::Matrix44f::CreateRotateAroundX(0.5f * dt);
				myDirLight.myLightData.myToLightDirection = mat * myDirLight.myLightData.myToLightDirection;
			}
			else if (keyMsgPtr->key == 0x28)
			{
				CU::Matrix44f mat = CU::Matrix44f::CreateRotateAroundX(0.5f * -dt);
				myDirLight.myLightData.myToLightDirection = mat * myDirLight.myLightData.myToLightDirection;
			}
			else if (keyMsgPtr->key == 0x25)
			{
				CU::Matrix44f mat = CU::Matrix44f::CreateRotateAroundZ(0.5f * dt);
				myDirLight.myLightData.myToLightDirection = mat * myDirLight.myLightData.myToLightDirection;
			}
			else if (keyMsgPtr->key == 0x27)
			{
				CU::Matrix44f mat = CU::Matrix44f::CreateRotateAroundZ(0.5f * -dt);
				myDirLight.myLightData.myToLightDirection = mat * myDirLight.myLightData.myToLightDirection;
				//myCamInst.Rotate(CU::Vector3f(0.f, 1.f, 0.f) * dt);
			}
			else if (keyMsgPtr->key == '1')
			{
				myCamInst.Move(CU::Vector3f(0.f, -1.f, 0.f), 5.f * dt);
			}
			else if (keyMsgPtr->key == '2')
			{
				myCamInst.Move(CU::Vector3f(0.f, 1.f, 0.f), 5.f * dt);
			}
			else if (keyMsgPtr->key == 'M')
			{
				//knife.Rotate(CU::Vector3f(0.0f, 0.4f, 0.f) * dt);
				myFloor1.Move(CU::Vector3f(0.f, -1.f, 0.f) * dt);
				myFloor2.Move(CU::Vector3f(0.f, -1.f, 0.f) * dt);
				myFloor3.Move(CU::Vector3f(0.f, -1.f, 0.f) * dt);
				myFloor4.Move(CU::Vector3f(0.f, -1.f, 0.f) * dt);
			}
			else if (keyMsgPtr->key == 'N')
			{
				//knife.Rotate(CU::Vector3f(0.0f, -0.4f, 0.f) * dt);
				myFloor1.Move(CU::Vector3f(0.f, 1.f, 0.f) * dt);
				myFloor2.Move(CU::Vector3f(0.f, 1.f, 0.f) * dt);
				myFloor3.Move(CU::Vector3f(0.f, 1.f, 0.f) * dt);
				myFloor4.Move(CU::Vector3f(0.f, 1.f, 0.f) * dt);
			}
		}
		else if (keyMsgPtr->state == EButtonState::Pressed)
		{
			if (keyMsgPtr->key == '3')
			{
				sce::gfx::CGraphicsEngineInterface::ToggleFXAA();
			}
			else if (keyMsgPtr->key == 'H')
			{
				robot2.SetAnimationWithIndex(ind++);
				if (ind > 5)
				{
					ind = 0;
				}
			}
			else if (keyMsgPtr->key == '4')
			{
				sce::gfx::CGraphicsEngineInterface::ToggleSSAO();
			}
			else if (keyMsgPtr->key == '5')
			{
				sce::gfx::CGraphicsEngineInterface::ToggleBloom();
			}
			else if (keyMsgPtr->key == 'P')
			{
				usePointLight = !usePointLight;
			}
		}
		if (keyMsgPtr->key == CU::CKeyBinds::GetHotKeys().myTilde && keyMsgPtr->state == EButtonState::Pressed)
		{
			CStateStack::PushMainStateOnTop(sce_new(CConsoleState(myEntityManager)));
		}
	}
}
