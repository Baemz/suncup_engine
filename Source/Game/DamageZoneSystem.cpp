#include "stdafx.h"
#include "DamageZoneSystem.h"
#include "..\GraphicsEngine\DebugTools.h"
#include "GameSystems\ScriptSystem.h"


CDamageZoneSystem::CDamageZoneSystem(EManager& aEntityManager)
	: CSystemAbstract<CDamageZoneSystem>(aEntityManager)
{
}

void CDamageZoneSystem::Activate()
{
}

void CDamageZoneSystem::Deactivate()
{
}

void CDamageZoneSystem::Update(const float aDeltaTime, const CU::GrowingArray<const CU::GrowingArray<UniqueIndexType>*>& aEntities)
{
	for (unsigned short i = myDamageZones.Size(); i-- > 0;)
	{
		SDamageZone& damageZone(myDamageZones[i]);

		damageZone.myLifeTime -= aDeltaTime;
		if (damageZone.myLifeTime <= 0.0f)
		{
			myDamageZones.RemoveCyclicAtIndex(i);
			continue;
		}

		//DT_DRAW_SPHERE_DEF_COLOR_2_ARGS(damageZone.myPosition, damageZone.myRadius);

		auto entitiesInZone(
			[&myEntityManager = myEntityManager, &damageZone, &aDeltaTime](auto& aEntityIndex)
		{
			const SEntityHandle handle(myEntityManager.GetComponent<CompHandle>(aEntityIndex).myHandle);

			const CU::Vector3f entityPos = myEntityManager.GetComponent<CompTransform>(handle).myPosition;
			const float entityDistToZoneSquared = (damageZone.myPosition - entityPos).Length2();

			if (entityDistToZoneSquared <= (damageZone.myRadius * damageZone.myRadius))
			{
				CScriptSystem::CallScriptEventOnTarget(handle.myUniqueIndex, "OnDamageTaken", (damageZone.myDmgPerSecond * aDeltaTime));
			}
		});

		for (const auto& entities : aEntities)
		{
			myEntityManager.ForEntitiesMatching<SigEnemyTag>(entitiesInZone, *entities);
		}
	}
}

void CDamageZoneSystem::CreateDamageZone(CU::Vector3f aPos, const float aLifeTime, const float aDamageRadius, const float aDmgPerSecond)
{
	SDamageZone damageZone = SDamageZone(aPos, aLifeTime, aDamageRadius, aDmgPerSecond);
	std::unique_lock<std::mutex> uniqueLock(myDamageZoneMutex);
	myDamageZones.Add(damageZone);
}
