#include "stdafx.h"
#include "NavigationMeshWrapper.h"
#include "NavigationMesh.h"

#define MAX_OBJECTS_IN_NODE (512)

using namespace NavMesh;

CNavigationMeshWrapper* CNavigationMeshWrapper::myInstance_ptr = nullptr;

CNavigationMeshWrapper::CNavigationMeshWrapper()
	: myNavMesh_ptr(nullptr)
{
}

CNavigationMeshWrapper::~CNavigationMeshWrapper()
{
	myNavMesh_ptr = nullptr;
}

void CNavigationMeshWrapper::Create()
{
	if (myInstance_ptr != nullptr)
	{
		GAME_LOG("WARNING! [CNavigationMeshWrapper] Failed to create new instance of navigation mesh wrapper because it was already created.");
		return;
	}

	myInstance_ptr = sce_new(CNavigationMeshWrapper());

	if (myInstance_ptr == nullptr)
	{
		GAME_LOG("ERROR! [CNavigationMeshWrapper] Failed to create new instance of navigation mesh wrapper because the program could not allocate new memory for it.");
	}
}

void CNavigationMeshWrapper::Destroy()
{
	if (myInstance_ptr == nullptr)
	{
		GAME_LOG("WARNING! [CNavigationMeshWrapper] Tried to destroy navigation mesh wrapper instance before creating it.");
		return;
	}

	sce_delete(myInstance_ptr);
}

void CNavigationMeshWrapper::DebugRender()
{
	//myStaticQuadTree.DebugRenderNodes();
	//myStaticQuadTree.DebugRenderObjects();
}

void CNavigationMeshWrapper::BindNavigationMesh(CNavigationMesh& aNavMesh, const CU::Vector4f& aLevelSize)
{
	myNavMesh_ptr = &aNavMesh;
	myStaticQuadTree.Init(MAX_OBJECTS_IN_NODE, aLevelSize);

	QuadTreeData quadTreeData;

	for (IndexType i(0); i < myNavMesh_ptr->myTriangleList.size(); ++i)
	{
		const auto& triangle(myNavMesh_ptr->myTriangleList[i]);
		const auto& vertex1(myNavMesh_ptr->myVertexList[triangle.myVertices[0]].myPosition);
		const auto& vertex2(myNavMesh_ptr->myVertexList[triangle.myVertices[1]].myPosition);
		const auto& vertex3(myNavMesh_ptr->myVertexList[triangle.myVertices[2]].myPosition);

		quadTreeData.myTriangleIndex = i;
		quadTreeData.myCenterPosition.x = vertex1.x;
		quadTreeData.myCenterPosition.y = vertex1.z;
		quadTreeData.mySize = GetBiggestSize(vertex1, vertex2, vertex3) * 2.0f;

		myStaticQuadTree.AddObject(quadTreeData);

		quadTreeData.myCenterPosition.x = vertex2.x;
		quadTreeData.myCenterPosition.y = vertex2.z;
		quadTreeData.mySize = GetBiggestSize(vertex2, vertex1, vertex3) * 2.0f;

		myStaticQuadTree.AddObject(quadTreeData);

		quadTreeData.myCenterPosition.x = vertex3.x;
		quadTreeData.myCenterPosition.y = vertex3.z;
		quadTreeData.mySize = GetBiggestSize(vertex3, vertex2, vertex1) * 2.0f;

		myStaticQuadTree.AddObject(quadTreeData);
	}

	myStaticQuadTree.ToggleShouldRenderNodes();
}

const bool CNavigationMeshWrapper::SetNodeTypeInSphere(const CU::Collision::Sphere& aSphere, const MeshComponents::ENodeType aOriginalNodeType, const MeshComponents::ENodeType aNewNodeType)
{
	return myNavMesh_ptr->SetNodeTypeInSphere(aSphere, aOriginalNodeType, aNewNodeType);
}

const bool CNavigationMeshWrapper::ResetNodeTypeInSphere(const CU::Collision::Sphere& aSphere)
{
	return myNavMesh_ptr->ResetNodeTypeInSphere(aSphere);
}

const bool CNavigationMeshWrapper::SetNodeTypeInAABB(const CU::Vector3f& aMin, const CU::Vector3f& aMax, const MeshComponents::ENodeType aOriginalNodeType, const MeshComponents::ENodeType aNewNodeType)
{
	return myNavMesh_ptr->SetNodeTypeInAABB(aMin, aMax, aOriginalNodeType, aNewNodeType);
}

const bool CNavigationMeshWrapper::ResetNodeTypeInAABB(const CU::Vector3f& aMin, const CU::Vector3f& aMax)
{
	return myNavMesh_ptr->ResetNodeTypeInAABB(aMin, aMax);
}

const bool CNavigationMeshWrapper::GetIntersectionWithNavMesh(const CU::Collision::Ray& aRay, CU::Vector3f& aIntersectionPoint_ref) const
{
	CU::GrowingArray<QuadTreeData> dataArray(MAX_OBJECTS_IN_NODE << 4);
	if (GetDataListFromIntersectedNodes(aRay, dataArray) == false)
	{
		return false;
	}

	CU::Collision::Triangle triColli;
	for (unsigned short i(0); i < dataArray.size(); ++i)
	{
		const auto& triangle(myNavMesh_ptr->myTriangleList[dataArray[i].myTriangleIndex]);

		triColli.myPoints[0] = myNavMesh_ptr->myVertexList[triangle.myVertices[0]].myPosition;
		triColli.myPoints[1] = myNavMesh_ptr->myVertexList[triangle.myVertices[1]].myPosition;
		triColli.myPoints[2] = myNavMesh_ptr->myVertexList[triangle.myVertices[2]].myPosition;
		triColli.myNormal = triangle.myNormal;

		if (CU::Collision::IntersectionTriangleRay(triColli, aRay, aIntersectionPoint_ref))
		{
			return true;
		}
	}

	return false;
}

const bool CNavigationMeshWrapper::IsIntersectingWithNavMesh(const CU::Collision::Ray& aRay) const
{
	const bool isIntersecting(GetIntersectionWithNavMesh(aRay, CU::Vector3f()));
	return isIntersecting;
}

/* NEAREST END EDGE */

const bool CNavigationMeshWrapper::GetDirectionToNearestEndEdge(const CU::Collision::Ray& aRay, CU::Vector3f& aDirection_ref) const
{
	CU::Vector3f objNavMeshPos;
	CU::Vector3f nearestEndEdgePos;
	const bool result(GetPositionOfNearestEndEdge(aRay, objNavMeshPos, nearestEndEdgePos));

	if (result == true)
	{
		aDirection_ref = (nearestEndEdgePos - objNavMeshPos).GetNormalized();
	}

	return result;
}

const bool CNavigationMeshWrapper::GetPositionOfNearestEndEdge(const CU::Collision::Ray& aRay, CU::Vector3f& aPosition_ref) const
{
	const bool result(GetPositionOfNearestEndEdge(aRay, CU::Vector3f(), aPosition_ref));
	return result;
}

const bool CNavigationMeshWrapper::GetOppositePositionOfNearestEndEdge(const CU::Collision::Ray& aRay, CU::Vector3f& aPosition_ref) const
{
	CU::Vector3f objNavMeshPos;
	CU::Vector3f nearestEndEdgePos;
	const bool result(GetPositionOfNearestEndEdge(aRay, objNavMeshPos, nearestEndEdgePos));

	if (result == true)
	{
 		aPosition_ref = objNavMeshPos + (objNavMeshPos - nearestEndEdgePos).GetNormalized();
	}

	return result;
}

/* PRIVATE FUNCTIONS */

const bool CNavigationMeshWrapper::GetPositionOfNearestEndEdge(const CU::Collision::Ray& aRay, CU::Vector3f& aObjPosition_ref, CU::Vector3f& aTargetPosition_ref) const
{
	CU::GrowingArray<QuadTreeData> dataArray(MAX_OBJECTS_IN_NODE << 4);
	if (GetDataListFromIntersectedNodes(aRay, dataArray) == false)
	{
		return false;
	}

	CU::GrowingArray<bool> alreadyCheckedEdges;
	alreadyCheckedEdges.Resize(static_cast<IndexType>(myNavMesh_ptr->myEdgeList.Size()));
	for (auto& edge : alreadyCheckedEdges)
	{
		edge = false;
	}

	CU::Vector3f A;
	CU::Vector3f B;
	CU::Vector3f AP;
	CU::Vector3f AB;

	CU::Vector3f nearestEndEdgePosition;
	CU::Vector3f currEndEdgePosition;
	float shortestDistance(FLT_MAX);

	for (unsigned short i(0); i < dataArray.size(); ++i)
	{
		const auto& triangle(myNavMesh_ptr->myTriangleList[dataArray[i].myTriangleIndex]);

		for (IndexType j(0); j < 3; ++j)
		{
			const IndexType edgeIndex(triangle.myEdges[j]);

			if (alreadyCheckedEdges[edgeIndex] == true)
			{
				continue;
			}
			alreadyCheckedEdges[edgeIndex] = true;

			const auto& edge(myNavMesh_ptr->myEdgeList[edgeIndex]);

			// Start calculating projection.

			A = myNavMesh_ptr->myVertexList[edge.myVertices[0]].myPosition;
			B = myNavMesh_ptr->myVertexList[edge.myVertices[1]].myPosition;

			AP = aRay.myOrigin - A;
			AB = B - A;

			const float AP_AB_dot(AP.Dot(AB));
			const float AB_AB_dot(AB.Dot(AB));
			float t(AP_AB_dot / AB_AB_dot);

			if (t < 0.0f) // Is over A.
			{
				const float floor(std::floor(-t));
				t = -t - floor;
			}
			else if (t > 1.0f) // Is over B.
			{
				const float ceil(std::ceilf(t));
				t = -t + ceil;
			}

			currEndEdgePosition = A + t * AB;

			const float distance((currEndEdgePosition - aRay.myOrigin).Length());
			if (isnan(distance) == false && (shortestDistance > distance))
			{
				shortestDistance = distance;
				nearestEndEdgePosition = currEndEdgePosition;
			}
		}
	}

	const bool result(shortestDistance != FLT_MAX);

	if (result == true)
	{
		aObjPosition_ref.x = aRay.myOrigin.x;
		aObjPosition_ref.y = nearestEndEdgePosition.y;
		aObjPosition_ref.z = aRay.myOrigin.z;
		aTargetPosition_ref = nearestEndEdgePosition;
	}

	return result;
}

const bool CNavigationMeshWrapper::GetDataListFromIntersectedNodes(const CU::Collision::Ray& aRay, CU::GrowingArray<QuadTreeData>& aArray_ref) const
{
	const CU::Collision::Plane quadTreePlane({ myStaticQuadTree.GetAABB().x, 0.0f, myStaticQuadTree.GetAABB().y }, { 0.0f, 1.0f, 0.0 });

	CU::Vector3f intersectionPoint;
	if (CU::Collision::IntersectionRayPlane(aRay, quadTreePlane, intersectionPoint) == false)
	{
		return false;
	}

	CU::GrowingArray<const QuadTreeNode*> quadTreeNodeList(16);
	if (myStaticQuadTree.GetNodeListInside({ intersectionPoint.x, intersectionPoint.z }, quadTreeNodeList) == false)
	{
		return false;
	}

	for (unsigned short i(0); i < quadTreeNodeList.size(); ++i)
	{
		quadTreeNodeList[i]->GetObjects(aArray_ref, true);
	}

	const bool result(aArray_ref.size() > 0);
	return result;
}

const CU::Vector2f CNavigationMeshWrapper::GetBiggestSize(const CU::Vector3f& aStartPos, const CU::Vector3f& aOther1Pos, const CU::Vector3f& aOther2Pos) const
{
	CU::Vector2f size;
	float otherVal(0.0f);

	size.x = std::abs(aStartPos.x - aOther1Pos.x);
	otherVal = std::abs(aStartPos.x - aOther2Pos.x);

	if (size.x < otherVal)
	{
		size.x = otherVal;
	}

	size.y = std::abs(aStartPos.z - aOther1Pos.z);
	otherVal = std::abs(aStartPos.z - aOther2Pos.z);

	if (size.y < otherVal)
	{
		size.y = otherVal;
	}

	//const float distance1((aStartPos - aOther1Pos).Length());
	//const float distance2((aStartPos - aOther2Pos).Length());
	//
	//if (distance1 > distance2)
	//{
	//	return distance1;
	//}

	return size;
}
