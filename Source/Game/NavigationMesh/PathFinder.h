#pragma once
#include "../CommonUtilities/GrowingArray.h"
#include "../CommonUtilities/Vector3.h"
#include "NavMeshStructs.h"

namespace CommonUtilities
{
	namespace Collision
	{
		struct LineSegment;
	}
}

class CPathFinder
{
private:
	struct SInputMileStone
	{
		SInputMileStone() = default;
		SInputMileStone(const CU::Vector3f& aPosition, const NavMesh::IndexType aEdgeIndex)
			:myPosition(aPosition), myEdgeIndex(aEdgeIndex)
		{}

		CU::Vector3f myPosition;
		NavMesh::IndexType myEdgeIndex;
	};

	typedef CU::GrowingArray<SInputMileStone, NavMesh::IndexType> InputPath;

public:
	CPathFinder(EdgeList& aEdgeList_ref, VertexList& aVertexList_ref, TriangleList& aTriangleList_ref);
	~CPathFinder();

	void ReInit();

	void SetPath(const NavMesh::IndexType aBeginNodeIndex, const NavMesh::IndexType aEndNodeIndex, const MeshComponents::ENodeType aTargetNodeType, const MeshComponents::ENodeType aAcceptableNodeType);
	const bool GetFirstEndEdgeIntersection(const CU::Vector3f& aBeginPosition, const CU::Vector3f& aEndPosition,
		CU::Vector3f& aIntersection_ref, const NavMesh::IndexType aBeginNodeIndex) const;
	inline const NavMesh::Path& GetPath() const { return myPath; }

private:
	const bool GetEdgeWithLeastCost(NavMesh::IndexType& aNearestEdgeIndex_ref);
	const bool CheckConnectionToEnd(const NavMesh::IndexType aEndTriangleIndex, const NavMesh::IndexType aCurrEdgeIndex) const;
	void OpenAdjacentEdges(const CU::Vector3f& aEndPosition, const NavMesh::IndexType aBegTriangleIndex, const NavMesh::IndexType aMainEdgeIndex,
		const MeshComponents::ENodeType aTargetNodeType, const MeshComponents::ENodeType aAcceptableNodeType);
	const IndexList GetAdjacentIndices(const NavMesh::IndexType aBegTriangleIndex, const NavMesh::IndexType aCurrEdgeIndex,
		const MeshComponents::ENodeType aTargetNodeType, const MeshComponents::ENodeType aAcceptableNodeType) const;
	void PinEdgePosition(const CU::Vector3f& aOtherPosition, MeshComponents::SEdge& aEdgeToPin) const;

	void CreatePath(const NavMesh::IndexType aBeginNodeIndex, const NavMesh::IndexType aEndNodeIndex);
	void Clear(const NavMesh::IndexType aBeginNodeIndex, const NavMesh::IndexType aEndNodeIndex);
	const bool PathSegmentIsClear(IndexList& aAlreadyCheckedEdgesIndexList_ref, const CU::Collision::LineSegment& aPathLineSeg,
		const NavMesh::IndexType aBeginEdgeIndex, const NavMesh::IndexType aEndEdgeIndex) const;

	inline const CU::Vector3f GetTrianglePosition(const MeshComponents::STriangle& aTriangle) const
	{
		const auto& vertex1(myVertexList_ref[aTriangle.myVertices[0]]);
		const auto& vertex2(myVertexList_ref[aTriangle.myVertices[1]]);
		const auto& vertex3(myVertexList_ref[aTriangle.myVertices[2]]);

		const CU::Vector3f pos1({ vertex1.myPosition.x, vertex1.myPosition.y, 0.0f });
		const CU::Vector3f pos2({ vertex2.myPosition.x, vertex2.myPosition.y, 0.0f });
		const CU::Vector3f pos3({ vertex3.myPosition.x, vertex3.myPosition.y, 0.0f });

		const CU::Vector3f middlePos(pos1 + (pos2 - pos1) / 2.0f);
		return (middlePos + (pos3 - middlePos) / 2.0f);
	}
	inline const CU::Vector3f GetEdgePosition(const MeshComponents::SEdge& aEdge) const
	{
		const CU::Vector3f pos1(myVertexList_ref[aEdge.myVertices[0]].myPosition.x, myVertexList_ref[aEdge.myVertices[0]].myPosition.y, 0.0f);
		const CU::Vector3f pos2(myVertexList_ref[aEdge.myVertices[1]].myPosition.x, myVertexList_ref[aEdge.myVertices[1]].myPosition.y, 0.0f);

		return (pos1 + (pos2 - pos1) / 2.0f);
	}
	inline const NavMesh::CostType GetDistance(const CU::Vector3f& aPos1, const CU::Vector3f& aPos2) const
	{
		return (NavMesh::CostType)((aPos1 - aPos2).Length());
	}

	template<typename ListType, typename ObjType, typename IndexType = IndexType>
	inline void SwapBackToFront(ListType& aList_ref)
	{
		ObjType leftObj;
		for (IndexType i(0); i < aList_ref.size() / (IndexType)2; ++i)
		{
			leftObj = aList_ref[i];
			aList_ref[i] = aList_ref[aList_ref.size() - 1 - i];
			aList_ref[aList_ref.size() - 1 - i] = leftObj;
		}
	}

private:
	EdgeList& myEdgeList_ref;
	VertexList& myVertexList_ref;
	TriangleList& myTriangleList_ref;
	IndexList myOpenList;
	IndexList myClosedList;
	NavMesh::Path myPath;
	NavMesh::IndexType myPrevBeginIndex;
	NavMesh::IndexType myPrevEndIndex;
};