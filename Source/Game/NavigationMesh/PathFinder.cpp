#include "stdafx.h"
#include "PathFinder.h"
#include "NavigationMesh.h"
#include "../CommonUtilities/Stopwatch.h"
#include "../CommonUtilities/Intersection.h"

using namespace NavMesh;

#define EDGE_NODE_TYPE_IS_ACCEPTABLE(aEdgeNodeType) (aEdgeNodeType == aTargetNodeType || aEdgeNodeType == aAcceptableNodeType)
#define GET_MIDDLE_VALUE(aVal1, aVal2) (aVal2 + (aVal1 - aVal2) * 0.5f)
#define GET_DIFFERENCE(aVal1, aVal2) (std::abs(aVal1 - aVal2))

#define HEIGHT_LIMIT (0.1f)

CPathFinder::CPathFinder(EdgeList& aEdgeList_ref, VertexList& aVertexList_ref, TriangleList& aTriangleList_ref)
	: myEdgeList_ref(aEdgeList_ref), myVertexList_ref(aVertexList_ref), myTriangleList_ref(aTriangleList_ref),
	myOpenList(128), myClosedList(128), myPath(128), myPrevBeginIndex(MAX_INDEX), myPrevEndIndex(MAX_INDEX)
{
}

CPathFinder::~CPathFinder()
{
}

void CPathFinder::ReInit()
{
	myPrevBeginIndex = MAX_INDEX;
	myPrevEndIndex = MAX_INDEX;

	myOpenList.RemoveAll();
	myClosedList.RemoveAll();
	myPath.RemoveAll();
}

void CPathFinder::SetPath(const IndexType aBeginNodeIndex, const IndexType aEndNodeIndex, const MeshComponents::ENodeType aTargetNodeType, const MeshComponents::ENodeType aAcceptableNodeType)
{
	START_TIMER(pathFinderTimer);
	Clear(aBeginNodeIndex, aEndNodeIndex);
	const CU::Vector3f& endPosition(myTriangleList_ref[aEndNodeIndex].myClickedPosition);

	if (aBeginNodeIndex == aEndNodeIndex)
	{
		myPath.Add(endPosition);
	}
	else
	{
		const CU::Vector3f& beginPosition(myTriangleList_ref[aBeginNodeIndex].myClickedPosition);

		for (IndexType i(0); i < 3; ++i) // First edges will be connected to begin triangle node.
		{
			const auto& edgeIndex(myTriangleList_ref[aBeginNodeIndex].myEdges[i]);
			auto& edge_ref(myEdgeList_ref[edgeIndex]);

			for (IndexType j(0); j < 2; ++j)
			{
				bool addToClosedList(false);

				if (EDGE_NODE_TYPE_IS_ACCEPTABLE(edge_ref.myNodeData.myTargetType))
				{
					if (edge_ref.myTriangles[j] != aBeginNodeIndex && edge_ref.myTriangles[j] != MAX_INDEX)
					{
						PinEdgePosition(beginPosition, edge_ref);

						auto& node_ref(edge_ref.myNodeData);
						node_ref.myG = GetDistance(beginPosition, node_ref.myPinnedPosition) + node_ref.myWeight;
						node_ref.myH = GetDistance(endPosition, node_ref.myPinnedPosition);
						node_ref.myCost = node_ref.myG + node_ref.myH;

						node_ref.myState = MeshComponents::ENodeState::eOpen;
						node_ref.myPredecessorIndex = aBeginNodeIndex;
						node_ref.myIsLastInQue = true;

						myOpenList.Add(edgeIndex);
					}
					else if (edge_ref.myTriangles[j] == MAX_INDEX) // Other is begin triangle.
					{
						addToClosedList = true;
					}
				}
				else
				{
					addToClosedList = true;
				}

				if (addToClosedList)
				{
					edge_ref.myNodeData.myState = MeshComponents::ENodeState::eClosed;
					myClosedList.Add(edgeIndex);
				}
			}
		}

		IndexType currEdgeIndex(MAX_INDEX);
		IndexType loopsCounted(0);
		while (myOpenList.Size() > 0)
		{
			if (GetEdgeWithLeastCost(currEdgeIndex) == false)
			{
				break;
			}

			if (CheckConnectionToEnd(aEndNodeIndex, currEdgeIndex))
			{
				break;
			}

			myOpenList.RemoveCyclic(currEdgeIndex);
			OpenAdjacentEdges(endPosition, aBeginNodeIndex, currEdgeIndex, aTargetNodeType, aAcceptableNodeType);
			myClosedList.Add(currEdgeIndex);

			++loopsCounted;
		}

		auto& endTriangle_ref(myTriangleList_ref[aEndNodeIndex]);
		const bool isConnectedToEndTriangle(endTriangle_ref.myEdges[0] == currEdgeIndex || endTriangle_ref.myEdges[1] == currEdgeIndex || endTriangle_ref.myEdges[2] == currEdgeIndex);

		if (isConnectedToEndTriangle)
		{
			const bool isEndNodeTypeSameAsTarget(endTriangle_ref.myNodeData.myTargetType == aTargetNodeType);

			if (isEndNodeTypeSameAsTarget)
			{
				endTriangle_ref.myNodeData.myPredecessorIndex = currEdgeIndex; // Connect end triangle to last edge.
				CreatePath(aBeginNodeIndex, aEndNodeIndex);
			}
		}
	}

	float timeElapsed(0.0f);
	END_TIMER_F(pathFinderTimer, timeElapsed);
	//printf("--------------------------------------\n");
	//printf("Path took %f ms to calculate.\n", timeElapsed);
}

const bool CPathFinder::GetFirstEndEdgeIntersection(const CU::Vector3f& aBeginPosition, const CU::Vector3f& aEndPosition,
	CU::Vector3f& aIntersection_ref, const NavMesh::IndexType aBeginNodeIndex) const
{
	IndexList alreadyCheckedEdgeList(128);
	const CU::Collision::LineSegment beginningAndEndLineSegment(aBeginPosition, aEndPosition);
	IndexType currTriIndex(aBeginNodeIndex);
	bool foundNoEndEdge(false);

	do
	{
		for (IndexType i(0); i < 3; ++i)
		{
			const IndexType currEdgeIndex(myTriangleList_ref[currTriIndex].myEdges[i]);

			if (alreadyCheckedEdgeList.Find(currEdgeIndex) == alreadyCheckedEdgeList.FoundNone)
			{
				const auto& currEdge(myEdgeList_ref[currEdgeIndex]);
				const CU::Collision::LineSegment currEdgeLineSegment(myVertexList_ref[currEdge.myVertices[0]].myPosition, myVertexList_ref[currEdge.myVertices[1]].myPosition);

				if (CU::Collision::IntersectionLineLine(beginningAndEndLineSegment, currEdgeLineSegment, aIntersection_ref))
				{
					const IndexType triIndex1(currEdge.myTriangles[0]);
					const IndexType triIndex2(currEdge.myTriangles[1]);

					if (triIndex1 == MAX_INDEX || triIndex2 == MAX_INDEX)
					{
						return true; // Found end edge.
					}
					else
					{
						if (triIndex1 != currTriIndex)
						{
							currTriIndex = triIndex1;
						}
						else if (triIndex2 != currTriIndex)
						{
							currTriIndex = triIndex2;
						}
						else
						{
							assert("This should not happen..." && false);
						}

						alreadyCheckedEdgeList.Add(currEdgeIndex);
						break;
					}
				}
			}

			if (i == 2)
			{
				foundNoEndEdge = true;
			}
		}
	} while (!foundNoEndEdge);

	return false;
}

/* PRIVATE FUNCTIONS */

const bool CPathFinder::GetEdgeWithLeastCost(IndexType& aNearestEdgeIndex_ref)
{
	CostType leastCost(MAX_COST);
	bool result(false);

	for (IndexType i(0); i < myOpenList.Size(); ++i)
	{
		auto& currNode_ref(myEdgeList_ref[myOpenList[i]].myNodeData);

		if (currNode_ref.myCost < leastCost)
		{
			leastCost = currNode_ref.myCost;
			aNearestEdgeIndex_ref = myOpenList[i];
			result = true;
		}
	}

	return result;
}

const bool CPathFinder::CheckConnectionToEnd(const IndexType aEndTriangleIndex, const IndexType aCurrEdgeIndex) const
{
	if (myEdgeList_ref[aCurrEdgeIndex].myTriangles[0] == aEndTriangleIndex)
	{
		return true;
	}

	if (myEdgeList_ref[aCurrEdgeIndex].myTriangles[1] == aEndTriangleIndex)
	{
		return true;
	}

	return false;
}

void CPathFinder::OpenAdjacentEdges(const CU::Vector3f& aEndPosition, const IndexType aBegTriangleIndex, const IndexType aMainEdgeIndex,
	const MeshComponents::ENodeType aTargetNodeType, const MeshComponents::ENodeType aAcceptableNodeType)
{
	const auto adjacentIndexList(GetAdjacentIndices(aBegTriangleIndex, aMainEdgeIndex, aTargetNodeType, aAcceptableNodeType));
	auto& mainNode_ref(myEdgeList_ref[aMainEdgeIndex].myNodeData);

	for (IndexType i(0); i < adjacentIndexList.Size(); ++i)
	{
		const auto currAdjacentEdge_index(adjacentIndexList[i]);

		auto currAdjacentEdge_copy(myEdgeList_ref[currAdjacentEdge_index]);
		PinEdgePosition(mainNode_ref.myPinnedPosition, currAdjacentEdge_copy);
		auto currAdjacentNode_copy(currAdjacentEdge_copy.myNodeData);

		if (currAdjacentNode_copy.myPredecessorIndex == aMainEdgeIndex && mainNode_ref.myPredecessorIndex == currAdjacentEdge_index)
		{
			// Skip current adjacent node if it and the main node point to each other.
			continue;
		}

		currAdjacentNode_copy.myPredecessorIndex = aMainEdgeIndex;
		currAdjacentNode_copy.myG = GetDistance(mainNode_ref.myPinnedPosition, currAdjacentNode_copy.myPinnedPosition) + mainNode_ref.myG + currAdjacentNode_copy.myWeight;
		currAdjacentNode_copy.myH = GetDistance(aEndPosition, currAdjacentNode_copy.myPinnedPosition);
		currAdjacentNode_copy.myCost = currAdjacentNode_copy.myG + currAdjacentNode_copy.myH;

		const auto indexInOpenList(myOpenList.Find(currAdjacentEdge_index));
		const auto indexInClosedList(myClosedList.Find(currAdjacentEdge_index));

		if (indexInOpenList != myOpenList.FoundNone)
		{
			const auto& nodeInOpenList(myEdgeList_ref[myOpenList[indexInOpenList]].myNodeData);

			if (currAdjacentNode_copy.myCost >= nodeInOpenList.myCost)
			{
				continue;
			}
		}

		if (indexInClosedList != myClosedList.FoundNone)
		{
			const auto& nodeInClosedList(myEdgeList_ref[myClosedList[indexInClosedList]].myNodeData);

			if (currAdjacentNode_copy.myCost >= nodeInClosedList.myCost)
			{
				continue;
			}

			myClosedList.RemoveCyclicAtIndex(indexInClosedList);
		}

		//currNode_copy.myState = ENodeState::eUnvisited;
		currAdjacentNode_copy.myState = MeshComponents::ENodeState::eOpen;
		myEdgeList_ref[currAdjacentEdge_index].myNodeData = currAdjacentNode_copy;
		myOpenList.Add(currAdjacentEdge_index);
	}

	mainNode_ref.myState = MeshComponents::ENodeState::eClosed;
}

const IndexList CPathFinder::GetAdjacentIndices(const IndexType aBegTriangleIndex, const IndexType aCurrEdgeIndex,
	const MeshComponents::ENodeType aTargetNodeType, const MeshComponents::ENodeType aAcceptableNodeType) const
{
	IndexList indexList(6);
	const auto& mainEdge_ref(myEdgeList_ref[aCurrEdgeIndex]);

	for (IndexType i(0); i < 2; ++i)
	{
		const auto currTriangle_index(mainEdge_ref.myTriangles[i]);

		if (currTriangle_index != aBegTriangleIndex && currTriangle_index != MAX_INDEX)
		{
			const auto edgeIndex1(myTriangleList_ref[currTriangle_index].myEdges[0]);
			const auto edgeIndex2(myTriangleList_ref[currTriangle_index].myEdges[1]);
			const auto edgeIndex3(myTriangleList_ref[currTriangle_index].myEdges[2]);

			const auto edgeNodeData1(myEdgeList_ref[edgeIndex1].myNodeData);
			const auto edgeNodeData2(myEdgeList_ref[edgeIndex2].myNodeData);
			const auto edgeNodeData3(myEdgeList_ref[edgeIndex3].myNodeData);

			if (edgeIndex1 != aCurrEdgeIndex && EDGE_NODE_TYPE_IS_ACCEPTABLE(edgeNodeData1.myTargetType))
			{
				indexList.Add(edgeIndex1);
			}

			if (edgeIndex2 != aCurrEdgeIndex && EDGE_NODE_TYPE_IS_ACCEPTABLE(edgeNodeData2.myTargetType))
			{
				indexList.Add(edgeIndex2);
			}

			if (edgeIndex3 != aCurrEdgeIndex && EDGE_NODE_TYPE_IS_ACCEPTABLE(edgeNodeData3.myTargetType))
			{
				indexList.Add(edgeIndex3);
			}
		}
	}
	
	return indexList;
}

void CPathFinder::PinEdgePosition(const CU::Vector3f& aOtherPosition, MeshComponents::SEdge& aEdgeToPin) const
{
	const CU::Vector3f& A(myVertexList_ref[aEdgeToPin.myVertices[0]].myPosition);
	const CU::Vector3f& B(myVertexList_ref[aEdgeToPin.myVertices[1]].myPosition);
	
	const CU::Vector3f AP(aOtherPosition - A);
	const CU::Vector3f AB(B - A);
	
	const float AP_AB_dot(AP.Dot(AB));
	const float AB_AB_dot(AB.Dot(AB));
	float t(AP_AB_dot / AB_AB_dot);
	
	//if (t < 0.0f || t > 1.0f)
	//{
	//	printf("Index: %i     ", aEdgeIndex);
	//	//t = AP_AB_dot / AP_AP_dot;
	//}
	
	if (t < 0.0f) // Is over A.
	{
		//printf("Over A - Before: %f, ", t);
		const float floor = std::floor(-t);
		t = -t - floor;
		//printf("After: %f\n", t);
	}
	else if (t > 1.0f) // Is over B.
	{
		//printf("Over B - Before: %f, ", t);
		t = -t + std::ceilf(t);
		//printf("After: %f\n", t);
	}
	
	aEdgeToPin.myNodeData.myPinnedPosition = A + t * AB;

	//const CU::Vector3f& vertex1(myVertexList_ref[aEdgeToPin.myVertices[0]].myPosition);
	//const CU::Vector3f& vertex2(myVertexList_ref[aEdgeToPin.myVertices[1]].myPosition);
	//
	//aEdgeToPin.myNodeData.myPinnedPosition = vertex1 + (vertex2 - vertex1) * 0.5f;
}

/* PATH CREATION / CLEARING */

void CPathFinder::CreatePath(const IndexType aBeginTriIndex, const IndexType aEndTriIndex)
{
	// Regular path creation:

	//IndexType currIndex(aEndTriIndex);
	//IndexType loopsCounted(0);
	//
	//myPath.Add(myTriangleList_ref[currIndex].myClickedPosition);
	//currIndex = myTriangleList_ref[currIndex].myNodeData.myPredecessorIndex;
	//
	//while (currIndex < myEdgeList_ref.Size() && loopsCounted < myEdgeList_ref.size())
	//{
	//	++loopsCounted;
	//	auto& currNode_ref(myEdgeList_ref[currIndex].myNodeData);
	//
	//	currNode_ref.myState = MeshComponents::ENodeState::eSelected;
	//	myPath.Add(currNode_ref.myPinnedPosition);
	//
	//	currIndex = currNode_ref.myPredecessorIndex;
	//	if (currNode_ref.myIsLastInQue)
	//	{
	//		break;
	//	}
	//}
	//
	//if (loopsCounted < myEdgeList_ref.size())
	//{
	//	myPath.Add(myTriangleList_ref[currIndex].myClickedPosition);
	//}

	// Smooth path creation:

	// 1. Create normal path.

	IndexType currIndex(myTriangleList_ref[aEndTriIndex].myNodeData.myPredecessorIndex);
	IndexType loopsCounted(0);
	InputPath inputPath(128);
	
	//printf("-------------------------------------------------------\n");
	inputPath.Add(SInputMileStone(myTriangleList_ref[aEndTriIndex].myClickedPosition, MAX_INDEX));
	while (currIndex < myEdgeList_ref.size() && loopsCounted < myEdgeList_ref.size())
	{
		++loopsCounted;
		auto& currEdge_ref(myEdgeList_ref[currIndex].myNodeData);
	
		currEdge_ref.myState = MeshComponents::ENodeState::eConsidered;
		inputPath.Add(SInputMileStone(currEdge_ref.myPinnedPosition, currIndex));

		currIndex = currEdge_ref.myPredecessorIndex;
		//printf("Index: %i\n", currIndex);

		//const auto& currEdgeNode_obj(myEdgeList_ref[currIndex].myNodeData);

		if (currEdge_ref.myIsLastInQue)
		{
			break;
		}
	}
	inputPath.Add(SInputMileStone(myTriangleList_ref[aBeginTriIndex].myClickedPosition, MAX_INDEX));
	
	SwapBackToFront<InputPath, SInputMileStone>(inputPath);
	
	// 2. Create smoothed path.

	myPath.Add(inputPath.GetFirst().myPosition);
	
	if (inputPath.size() > 3)
	{
		IndexList helperIndexList(inputPath.size());
		CU::Collision::LineSegment pathLineSeg;
		IndexType latestEdgeIndex(inputPath[1].myEdgeIndex);
	
		currIndex = 3;
	
		while (currIndex < inputPath.size())
		{
			pathLineSeg.myP0 = myPath.GetLast();
			pathLineSeg.myP1 = inputPath[currIndex].myPosition;
			pathLineSeg.myP0.y = 0.0f;
			pathLineSeg.myP1.y = 0.0f;
	
			// HACK ! Hard coded for y (special case).
			//if (currIndex == inputPath.size() - 1)
			//{
			//	pathLineSeg.myP1.y = pathLineSeg.myP0.y;
			//}
			//else if (myPath.size() == 1)
			//{
			//	pathLineSeg.myP0.y = pathLineSeg.myP1.y;
			//}
	
			if (!PathSegmentIsClear(helperIndexList, pathLineSeg, latestEdgeIndex, inputPath[currIndex].myEdgeIndex))
			{
				myPath.Add(inputPath[currIndex - 1].myPosition);
				latestEdgeIndex = inputPath[currIndex - 1].myEdgeIndex;
				myEdgeList_ref[latestEdgeIndex].myNodeData.myState = MeshComponents::ENodeState::eSelected;
			}
	
			++currIndex;
		}
	}

	myPath.Add(inputPath.GetLast().myPosition);
	SwapBackToFront<Path, CU::Vector3f>(myPath);
	myPath.RemoveCyclicAtIndex(myPath.size() - 1);
}

void CPathFinder::Clear(const IndexType aBeginNodeIndex, const IndexType aEndNodeIndex)
{
	myOpenList.RemoveAll();
	myClosedList.RemoveAll();
	myPath.RemoveAll();

	if (myPrevBeginIndex != MAX_INDEX)
	{
		myTriangleList_ref[myPrevBeginIndex].myNodeData.ResetNodeData();
	}

	if (myPrevEndIndex != MAX_INDEX)
	{
		myTriangleList_ref[myPrevEndIndex].myNodeData.ResetNodeData();
	}

	for (IndexType i(0); i < myEdgeList_ref.Size(); ++i)
	{
		myEdgeList_ref[i].myNodeData.ResetNodeData();
	}

	myPrevBeginIndex = aBeginNodeIndex;
	myPrevEndIndex = aEndNodeIndex;
}

const bool CPathFinder::PathSegmentIsClear(IndexList& aAlreadyCheckedEdgesIndexList_ref, const CU::Collision::LineSegment& aPathLineSeg,
	const NavMesh::IndexType aBeginEdgeIndex, const NavMesh::IndexType aEndEdgeIndex) const
{
	//printf("Begin edge index: %i\n", aBeginEdgeIndex);
	//printf("End edge index: %i\n\n", aEndEdgeIndex);

	aAlreadyCheckedEdgesIndexList_ref.Add(aBeginEdgeIndex);

	//CU::Collision::LineSegment pathLineSeg_copy(aPathLineSeg);
	//pathLineSeg_copy.myP0.y = 0.0f;
	//pathLineSeg_copy.myP1.y = 0.0f;

	CU::Vector3f pointOfCollision;
	IndexType lastCheckedEdgeIndex(aBeginEdgeIndex);
	const float pathLineMiddleHeight(GET_MIDDLE_VALUE(aPathLineSeg.myP0.y, aPathLineSeg.myP1.y));
	bool pathIsClear(true);

	while (true)
	{
		//printf("Curr edge index: %i\n", lastCheckedEdgeIndex);

		bool foundIntersectedEdge(false);

		for (IndexType triIndex(0); triIndex < 2; ++triIndex)
		{
			if (myEdgeList_ref[lastCheckedEdgeIndex].myTriangles[triIndex] == MAX_INDEX)
			{
				pathIsClear = false;
				goto escapeFromForLoops;
			}

			IndexType currTri_index(myEdgeList_ref[lastCheckedEdgeIndex].myTriangles[triIndex]);
			const MeshComponents::STriangle& currTri_obj(myTriangleList_ref[currTri_index]);

			for (IndexType edgeIndex(0); edgeIndex < 3; ++edgeIndex)
			{
				IndexType currEdge_index(currTri_obj.myEdges[edgeIndex]);
				if (aAlreadyCheckedEdgesIndexList_ref.Find(currEdge_index) != aAlreadyCheckedEdgesIndexList_ref.FoundNone) continue;

				if (currEdge_index == aEndEdgeIndex)
				{
					//printf("Found end edge: %i.\n", currEdge_index);
					lastCheckedEdgeIndex = currEdge_index;
					goto escapeFromForLoops;
				}

				CU::Collision::LineSegment edgeLineSegment;

				const MeshComponents::SEdge& currEdge_obj(myEdgeList_ref[currEdge_index]);
				edgeLineSegment.myP0 = myVertexList_ref[currEdge_obj.myVertices[0]].myPosition;
				edgeLineSegment.myP1 = myVertexList_ref[currEdge_obj.myVertices[1]].myPosition;
				edgeLineSegment.myP0.y = 0.0f;
				edgeLineSegment.myP1.y = 0.0f;

				//if (GET_DIFFERENCE(GET_MIDDLE_VALUE(edgeLineSegment.myP0.y, edgeLineSegment.myP1.y), pathLineMiddleHeight) <= HEIGHT_LIMIT)
				//{
				//	pathLineSeg_copy.myP0.y = 0.0f;
				//	pathLineSeg_copy.myP1.y = 0.0f;
				//	edgeLineSegment.myP0.y = 0.0f;
				//	edgeLineSegment.myP1.y = 0.0f;
				//}
				//else
				//{
				//	pathLineSeg_copy.myP0.y = aPathLineSeg.myP0.y;
				//	pathLineSeg_copy.myP1.y = aPathLineSeg.myP1.y;
				//}

				if (CU::Collision::IntersectionLineLine(aPathLineSeg, edgeLineSegment, pointOfCollision))
				{
					if (currEdge_obj.myTriangles[0] == MAX_INDEX || currEdge_obj.myTriangles[1] == MAX_INDEX)
					{
						//printf("Path is not clear.\n");
						pathIsClear = false;
					}
					else
					{
						aAlreadyCheckedEdgesIndexList_ref.Add(currEdge_index);
						lastCheckedEdgeIndex = currEdge_index;
						foundIntersectedEdge = true;
					}

					goto escapeFromForLoops;
				}
			}
		}

	escapeFromForLoops:

		if (lastCheckedEdgeIndex == aEndEdgeIndex || !pathIsClear)
		{
			break;
		}

		if (!foundIntersectedEdge && aEndEdgeIndex != MAX_INDEX)
		{
			// Found a "dead loop".
			//printf("Found a dead loop, setting path as not clear.\n");
			pathIsClear = false;
			break;
		}
		else if (!foundIntersectedEdge && aEndEdgeIndex == MAX_INDEX)
		{
			//printf("Is at the end of the path.\n");
			break;
		}
	}
	aAlreadyCheckedEdgesIndexList_ref.RemoveAll();

	//printf("----\n");

	return pathIsClear;
}
