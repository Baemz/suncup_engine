#pragma once
#include "../CommonUtilities/GrowingArray.h"
#include "../CommonUtilities/Vector3.h"
//#include "../CommonUtilities/Random.h"
#include <limits>

namespace NavMesh
{
	typedef unsigned short IndexType;
	typedef float CostType;
	typedef float WeightType;
	typedef CU::GrowingArray<CU::Vector3f, IndexType> Path;
}

#define MAX_INDEX (USHRT_MAX)
#define MAX_COST (FLT_MAX)
#define MAX_WEIGHT (FLT_MAX)
#define DEFAULT_WEIGHT ((NavMesh::WeightType)1)

namespace MeshComponents
{
	enum class ENodeState // May be used for debugging (different states will draw different debug stuff).
	{
		eSelected
		, eOpen
		, eClosed
		, eConsidered
		, eUnvisited
	};

	enum class ENodeType: unsigned char
	{
		eNot_Set
		, eWalk_Dash
		, eDash
		, eNothing
	};

	struct SNode
	{
		SNode() : myWeight(DEFAULT_WEIGHT/*(WeightType)CU::GetRandomInRange(0.0f, 1000.0f)*/), myCost(MAX_COST), myPredecessorIndex(MAX_INDEX), myG(MAX_COST), myH(MAX_COST)
			, myState(ENodeState::eUnvisited), myIsLastInQue(false)
		{
		}

		inline void ResetNodeData()
		{
			myPinnedPosition = { 0.0f, 0.0f, 0.0f };
			myCost = MAX_COST;
			myG = MAX_COST;
			myH = MAX_COST;
			myPredecessorIndex = MAX_INDEX;
			myState = ENodeState::eUnvisited;
			myIsLastInQue = false;
		}

		CU::Vector3f myPinnedPosition;
		NavMesh::WeightType myWeight;
		NavMesh::CostType myCost;
		NavMesh::CostType myG;
		NavMesh::CostType myH;
		NavMesh::IndexType myPredecessorIndex;
		ENodeState myState; // May be use for debugging (different states will draw different debug stuff).
		ENodeType myTargetType;
		ENodeType myOriginalType;
		bool myIsLastInQue;
	};

	struct SVertex
	{
		const bool operator==(const SVertex& aVertex) const
		{
			return myPosition == aVertex.myPosition;
		}

		SVertex() = default;
		SVertex(const float aX, const float aY, const float aZ) : myPosition(aX, aY, aZ) {}
		SVertex(const CU::Vector3f& aPosition) : myPosition(aPosition) {}

		CU::Vector3f myPosition;
	};

	struct SEdge
	{
		const bool operator==(const SEdge& aEdge) const
		{
			return myVertices[0] == aEdge.myVertices[0] && myVertices[1] == aEdge.myVertices[1] &&
				myTriangles[0] == aEdge.myTriangles[0] && myTriangles[1] == aEdge.myTriangles[1];
		}

		SEdge()
		{
			myTriangles[0] = MAX_INDEX; myTriangles[1] = MAX_INDEX;
			myVertices[0] = MAX_INDEX; myVertices[1] = MAX_INDEX;
		}
		SEdge(const NavMesh::IndexType aVertexIndex1, const NavMesh::IndexType aVertexIndex2, const NavMesh::IndexType aTriangleIndex1, const NavMesh::IndexType aTriangleIndex2)
		{
			myVertices[0] = aVertexIndex1; myVertices[1] = aVertexIndex2;
			myTriangles[0] = aTriangleIndex1; myTriangles[1] = aTriangleIndex2;
		}

		NavMesh::IndexType myVertices[2];
		NavMesh::IndexType myTriangles[2];
		SNode myNodeData;
	};

	struct STriangle
	{
		STriangle()
		{
			myEdges[0] = MAX_INDEX; myEdges[1] = MAX_INDEX; myEdges[2] = MAX_INDEX;
			myVertices[0] = MAX_INDEX; myVertices[1] = MAX_INDEX; myVertices[2] = MAX_INDEX;
		}
		STriangle(const NavMesh::IndexType aEdgeIndex1, const NavMesh::IndexType aEdgeIndex2, const NavMesh::IndexType aEdgeIndex3,
			const NavMesh::IndexType aVertexIndex1, const NavMesh::IndexType aVertexIndex2, const NavMesh::IndexType aVertexIndex3, const CU::Vector3f& aNormal = CU::Vector3f())
		{
			myEdges[0] = aEdgeIndex1; myEdges[1] = aEdgeIndex2; myEdges[2] = aEdgeIndex3;
			myVertices[0] = aVertexIndex1; myVertices[1] = aVertexIndex2; myVertices[2] = aVertexIndex3;
			myNormal = aNormal;
		}

		NavMesh::IndexType myEdges[3];
		NavMesh::IndexType myVertices[3];
		SNode myNodeData;
		CU::Vector3f myNormal;
		CU::Vector3f myClickedPosition;
	};

	struct SIntersectedEdge
	{
		SIntersectedEdge() : myIndex(MAX_INDEX) {}
		SIntersectedEdge(const CU::Vector3f& aIntersectionPos, const NavMesh::IndexType aIndex)
			:myIntersectionPos(aIntersectionPos), myIndex(aIndex) {}

		CU::Vector3f myIntersectionPos;
		NavMesh::IndexType myIndex;
	};
};

typedef CU::GrowingArray<NavMesh::IndexType, NavMesh::IndexType> IndexList;
typedef CU::GrowingArray<MeshComponents::SEdge, NavMesh::IndexType> EdgeList;
typedef CU::GrowingArray<MeshComponents::SVertex, NavMesh::IndexType> VertexList;
typedef CU::GrowingArray<MeshComponents::STriangle, NavMesh::IndexType> TriangleList;
typedef CU::GrowingArray<MeshComponents::SIntersectedEdge, NavMesh::IndexType> IntersectedEdgeList;