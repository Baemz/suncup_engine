#pragma once
#include "QuadTreeNode.h"

class CNavigationMesh;

namespace CommonUtilities
{
	namespace Collision
	{
		struct Ray;
		struct Sphere;
	};
};
namespace CU = CommonUtilities;

class CNavigationMeshWrapper
{
public:
	~CNavigationMeshWrapper();

	static void Create();
	static void Destroy();
	static inline CNavigationMeshWrapper& Get() { return *myInstance_ptr; }

	void DebugRender();

	void BindNavigationMesh(CNavigationMesh& aNavMesh, const CU::Vector4f& aLevelSize);

	const bool SetNodeTypeInSphere(const CU::Collision::Sphere& aSphere, const MeshComponents::ENodeType aOriginalNodeType, const MeshComponents::ENodeType aNewNodeType);
	const bool ResetNodeTypeInSphere(const CU::Collision::Sphere& aSphere);

	const bool SetNodeTypeInAABB(const CU::Vector3f& aMin, const CU::Vector3f& aMax, const MeshComponents::ENodeType aOriginalNodeType, const MeshComponents::ENodeType aNewNodeType);
	const bool ResetNodeTypeInAABB(const CU::Vector3f& aMin, const CU::Vector3f& aMax);

	const bool GetIntersectionWithNavMesh(const CU::Collision::Ray& aRay, CU::Vector3f& aIntersectionPoint_ref) const;
	const bool IsIntersectingWithNavMesh(const CU::Collision::Ray& aRay) const;

	const bool GetDirectionToNearestEndEdge(const CU::Collision::Ray& aRay, CU::Vector3f& aDirection_ref) const;
	const bool GetPositionOfNearestEndEdge(const CU::Collision::Ray& aRay, CU::Vector3f& aPosition_ref) const;
	const bool GetOppositePositionOfNearestEndEdge(const CU::Collision::Ray& aRay, CU::Vector3f& aPosition_ref) const;

private:
	const bool GetPositionOfNearestEndEdge(const CU::Collision::Ray& aRay, CU::Vector3f& aObjPosition_ref, CU::Vector3f& aTargetPosition_ref) const;
	const bool GetDataListFromIntersectedNodes(const CU::Collision::Ray& aRay, CU::GrowingArray<QuadTreeData>& aArray_ref) const;
	const CU::Vector2f GetBiggestSize(const CU::Vector3f& aStartPos, const CU::Vector3f& aOther1Pos, const CU::Vector3f& aOther2Pos) const;

private:
	CNavigationMeshWrapper();
	CNavigationMeshWrapper(CNavigationMeshWrapper&& aNavigationMeshWrapper) = delete;
	CNavigationMeshWrapper(const CNavigationMeshWrapper& aNavigationMeshWrapper) = delete;
	void operator=(const CNavigationMeshWrapper& aNavigationMeshWrapper) = delete;

private:
	QuadTreeNode myStaticQuadTree;

	static CNavigationMeshWrapper* myInstance_ptr;
	CNavigationMesh* myNavMesh_ptr;
};

