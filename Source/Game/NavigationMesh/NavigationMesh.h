#pragma once
#include "../CommonUtilities/GrowingArray.h"
#include "../CommonUtilities/Intersection.h"
#include "MapReader/MapData.h"
#include "NavMeshStructs.h"
#include "PathFinder.h"
#include <string>

#define NAVMESH_HEIGHT_LIMIT 2.5f
//#define RENDER_WEIGHT
//#define RENDER_EDGE_DEPENDENCY
#define NO_NAV_MESH_EDITING

class CNavigationMesh
{
	friend class CNavigationMeshWrapper;

public:
	static constexpr float NodeRadius = 1.0f;

	CNavigationMesh();
	~CNavigationMesh();

	void Init(const CMapData::SLevelInfo::SNavMesh& aNavMeshData);

#ifndef NO_NAV_MESH_EDITING
	void Split(const CU::Vector3f& aFirstVertexPosition, const CU::Vector3f& aSecondVertexPosition);
#endif

	void Reset();
	void Render();

	void SelectBeginNode(const CU::Collision::Ray& aRay, const bool aUseRayAsReference = false, const float aHeightDifferenceLimit = -1.0f);
	void SelectEndNode(const CU::Collision::Ray& aRay, const bool aUseRayAsReference = false, const float aHeightDifferenceLimit = -1.0f);

	// Target node type is the preferred end node type and one that allows the algorithm to process.
	// Acceptable node type is an additional node type that allows the algorithm to process.
	const bool CalculatePath(const MeshComponents::ENodeType aTargetNodeType, const MeshComponents::ENodeType aAcceptableNodeType);
	const bool CalculatePath(const MeshComponents::ENodeType aTargetNodeType);
	const bool CalculatePath();

	const bool GetFirstEndEdgeIntersection(const CU::Collision::Ray& aBeginRay, const CU::Collision::Ray& aEndRay, CU::Vector3f& aIntersection_ref) const;
	const bool CheckNodeTypeOnRayCast(const CU::Collision::Ray& aRay, const MeshComponents::ENodeType aNodeType, const bool aUseRayAsReference = false, const float aHeightDifferenceLimit = -1.0f) const;

	const bool SetNodeTypeInSphere(const CU::Collision::Sphere& aSphere, const MeshComponents::ENodeType aOriginalNodeType, const MeshComponents::ENodeType aNewNodeType);
	const bool ResetNodeTypeInSphere(const CU::Collision::Sphere& aSphere);

	const bool SetNodeTypeInAABB(const CU::Vector3f& aMin, const CU::Vector3f& aMax, const MeshComponents::ENodeType aOriginalNodeType, const MeshComponents::ENodeType aNewNodeType);
	const bool ResetNodeTypeInAABB(const CU::Vector3f& aMin, const CU::Vector3f& aMax);

	inline const NavMesh::Path GetPathCopy() const 
	{
		return myPathLineList;
	}
	inline const NavMesh::Path& GetPath() const
	{
		return myPathLineList;
	}
	inline const CU::Vector3f& GetPositionWithNodeIndex(const NavMesh::IndexType aIndex) const
	{
		return myTriangleList[aIndex].myClickedPosition;
	}

	const bool IsIntersectingWithNavMesh(const CU::Collision::Ray& aRay) const;
	const bool GetIntersection(const CU::Collision::Ray& aRay, CU::Vector3f& aIntersectionPoint) const;

	inline const bool IsBeginSelected() const { return myBeginNodeIndex != MAX_INDEX; }
	inline const bool IsEndSelected() const { return myEndNodeIndex != MAX_INDEX; }
	inline void DeselectBeginAndEnd() { myBeginNodeIndex = MAX_INDEX; myEndNodeIndex = MAX_INDEX; }
	inline const CU::Vector3f GetEndNodeClickedPosition() const
	{
		if (myEndNodeIndex == MAX_INDEX)
		{
			return CU::Vector3f();
		}

		return myTriangleList[myEndNodeIndex].myClickedPosition;
	}

	inline void ToggleShouldRender() { myShouldRender = !myShouldRender; }

private:
#ifndef NO_NAV_MESH_EDITING
	void SplitTriangles(const MeshComponents::SIntersectedEdge& aIntersectedEdge, const MeshComponents::SEdge& aLeftEdge, const MeshComponents::SEdge& aRightEdge,
		const NavMesh::IndexType aTriangleIndex1, const NavMesh::IndexType aTriangleIndex2);
#endif

	void SetEdgeNodeTypeInTriangle(MeshComponents::STriangle& aTriangle_ref, const MeshComponents::ENodeType aOriginalNodeType, const MeshComponents::ENodeType aNewNodeType);
	const NavMesh::IndexType GetIntersectingNodeIndex(const CU::Collision::Ray& aRay, const bool aUseRayAsReference = false, const float aHeightDifferenceLimit = -1.0f) const;
	const NavMesh::IndexType GetIntersectingNodeIndex(const CU::Collision::Ray& aRay, CU::Vector3f& aIntersectionPoint_ref, const bool aUseRayAsReference = false, const float aHeightDifferenceLimit = -1.0f) const;

	const bool GetTriangleAdress(MeshComponents::STriangle*& aT1Ptr, MeshComponents::STriangle*& aT2Ptr, const NavMesh::IndexType aT1Index, const NavMesh::IndexType aT2Index);
	void GatherIntersectedEdges(IntersectedEdgeList& aEdgeList, const CU::Vector3f& aFirstPos, const CU::Vector3f& aSecondPos);
	const bool GetEdgeIntersection(const MeshComponents::STriangle& aTriangle, const CU::Vector3f& aFirstPos, const CU::Vector3f& aSecondPos, CU::Vector3f& aHitPos,
		NavMesh::IndexType& aEdgeIdex, const bool aPointIsFirst) const;

	// Debug

	inline void GetTrianglePosition(CU::Vector3f& aPos0_ref, CU::Vector3f& aPos1_ref, CU::Vector3f& aPos2_ref, const NavMesh::IndexType aTriangleIndex) const
	{
		if (aTriangleIndex == MAX_INDEX)
		{
			return;
		}

		aPos0_ref = myVertexList[myTriangleList[aTriangleIndex].myVertices[0]].myPosition;
		aPos1_ref = myVertexList[myTriangleList[aTriangleIndex].myVertices[1]].myPosition;
		aPos2_ref = myVertexList[myTriangleList[aTriangleIndex].myVertices[2]].myPosition;
	}

	// For loading

	void ReInit(const CMapData::SLevelInfo::SNavMesh& aNavMeshData);
	void GetMatchingEdgeIndices(NavMesh::IndexType& aEdgeIndex1_ref, NavMesh::IndexType& aEdgeIndex2_ref, NavMesh::IndexType& aEdgeIndex3_ref,
		const NavMesh::IndexType aVertexIndex1, const NavMesh::IndexType aVertexIndex2, const NavMesh::IndexType aVertexIndex3, const NavMesh::IndexType aCurrTriIndex);

private:
	CMapData::SLevelInfo::SNavMesh myNavigationMeshData;

	NavMesh::Path myPathLineList;

	NavMesh::IndexType myBeginNodeIndex;
	NavMesh::IndexType myEndNodeIndex;

	VertexList myVertexList;
	EdgeList myEdgeList;
	TriangleList myTriangleList;

	CPathFinder myPathFinder;
	NavMesh::WeightType myMaxWeight;
	bool myShouldRender;

};