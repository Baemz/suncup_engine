#include "stdafx.h"
#include "NavigationMesh.h"
#include "../CommonUtilities/Intersection.h"
#include "../CommonUtilities/Vector3.h"
#include "../CommonUtilities/Random.h"
#include "../GraphicsEngine/DebugTools.h"
#include "../GraphicsEngine/GraphicsEngineInterface.h"

#define VERTEX_COLOR { 1.0f, 0.0f, 0.0f }
#define PATH_COLOR { 0.0f, 1.0f, 1.0f }

#define BEGIN_NODE_COLOR { 0.0f, 1.0f, 0.0f }
#define END_NODE_COLOR { 1.0f, 0.0f, 0.0f }

#define ONE_PARENT_EDGE_COLOR { 1.0f, 0.0f, 0.0f }
#define FIRST_EDGE_COLOR { 0.0f, 1.0f, 0.0f }
#define SECOND_EDGE_COLOR { 0.0f, 0.0f, 1.0f }

#define SELECTED_EDGE_COLOR { 1.0f, 0.0f, 1.0f }
#define OPEN_EGDE_COLOR { 0.0f, 0.0f, 1.0f }
#define CLOSED_EGDE_COLOR { 0.0f, 1.0f, 0.0f }
#define CONSIDERED_EGDE_COLOR { 1.0f, 1.0f, 0.0f }
#define DEFAULT_EDGE_COLOR { 1.0f, 1.0f, 1.0f }

using namespace NavMesh;

CNavigationMesh::CNavigationMesh()
	: myPathFinder(myEdgeList, myVertexList, myTriangleList)
	, myBeginNodeIndex(MAX_INDEX)
	, myEndNodeIndex(MAX_INDEX)
	, myMaxWeight(DEFAULT_WEIGHT)
	, myShouldRender(false)
{
	myPathLineList.Init(128);
}

CNavigationMesh::~CNavigationMesh()
{
}

void CNavigationMesh::Init(const CMapData::SLevelInfo::SNavMesh& aNavMeshData)
{
	myNavigationMeshData = aNavMeshData;
	Reset();
}

#ifndef NO_NAV_MESH_EDITING
void CNavigationMesh::Split(const CU::Vector3f& aFirstVertexPosition, const CU::Vector3f& aSecondVertexPosition)
{
	IntersectedEdgeList edgeList;
	edgeList.Init(64);
	GatherIntersectedEdges(edgeList, aFirstVertexPosition, aSecondVertexPosition);

	MeshComponents::SEdge* currEdgePtr(nullptr);
	MeshComponents::SEdge replaceEdge1;
	MeshComponents::SEdge replaceEdge2;

	for (IndexType i(0); i < edgeList.Size(); ++i)
	{
		currEdgePtr = &myEdgeList[edgeList[i].myIndex];
		replaceEdge1 = *currEdgePtr;
		replaceEdge2 = *currEdgePtr;

		myVertexList.Add(MeshComponents::SVertex(edgeList[i].myIntersectionPos));

		// --------------------------------------------------------------- add two new edges for replacement
		
		replaceEdge1.myVertices[1] = myVertexList.Size() - 1;
		replaceEdge2.myVertices[0] = myVertexList.Size() - 1;

		// --------------------------------------------------------------- add two new triangles for replacement and an edge between them

		SplitTriangles(edgeList[i], replaceEdge1, replaceEdge2, currEdgePtr->myTriangles[0], currEdgePtr->myTriangles[1]);
	}

	for (IndexType i(0); i < myVertexList.Size(); ++i)
	{
		printf("Vertex %i - x = %f    y = %f   z = %f\n", i, myVertexList[i].myPosition.x, myVertexList[i].myPosition.y, myVertexList[i].myPosition.z);
	}
}
#endif

void CNavigationMesh::Reset()
{
	myPathFinder.ReInit();
	myVertexList.RemoveAll();
	myEdgeList.RemoveAll();
	myTriangleList.RemoveAll();
	myBeginNodeIndex = MAX_INDEX;
	myEndNodeIndex = MAX_INDEX;

	ReInit(myNavigationMeshData);
}

void CNavigationMesh::Render()
{
	if (!myShouldRender)
	{
		return;
	}

	if (myBeginNodeIndex != MAX_INDEX)
	{
		sce::gfx::CDebugTools::Get()->DrawCube(myTriangleList[myBeginNodeIndex].myClickedPosition, 0.1f, BEGIN_NODE_COLOR);
	}
	if (myEndNodeIndex != MAX_INDEX)
	{
		sce::gfx::CDebugTools::Get()->DrawCube(myTriangleList[myEndNodeIndex].myClickedPosition, 0.1f, END_NODE_COLOR);
	}
	
	CU::Vector3f from;
	CU::Vector3f to;
	CU::Vector3f color;
	for (IndexType i = 0; i < myEdgeList.Size(); ++i)
	{
		from = myVertexList[myEdgeList[i].myVertices[0]].myPosition;
		to = myVertexList[myEdgeList[i].myVertices[1]].myPosition;

		if (myEdgeList[i].myTriangles[0] == MAX_INDEX || myEdgeList[i].myTriangles[1] == MAX_INDEX)
		{
			color = ONE_PARENT_EDGE_COLOR;
		}
		else if (myEdgeList[i].myNodeData.myState == MeshComponents::ENodeState::eSelected)
		{
			color = SELECTED_EDGE_COLOR;
		}
		else if (myEdgeList[i].myNodeData.myState == MeshComponents::ENodeState::eOpen /*|| myEdgeList[i].myNodeData.myType == MeshComponents::ENodeType::eDash*/)
		{
			color = OPEN_EGDE_COLOR;
		}
		else if (myEdgeList[i].myNodeData.myState == MeshComponents::ENodeState::eClosed)
		{
			color = CLOSED_EGDE_COLOR;
		}
		else if (myEdgeList[i].myNodeData.myState == MeshComponents::ENodeState::eConsidered)
		{
			color = CONSIDERED_EGDE_COLOR;
		}
		else
		{
			color = DEFAULT_EDGE_COLOR;
		}

		if (myEdgeList[i].myTriangles[0] != MAX_INDEX)
		{
			const auto triangleNodeData(myTriangleList[myEdgeList[i].myTriangles[0]].myNodeData);
			if (triangleNodeData.myTargetType == MeshComponents::ENodeType::eDash)
			{
				color = SELECTED_EDGE_COLOR;
			}
			else if (triangleNodeData.myTargetType == MeshComponents::ENodeType::eNothing)
			{
				color = CLOSED_EGDE_COLOR;
			}
		}
		
		if (myEdgeList[i].myTriangles[1] != MAX_INDEX)
		{
			const auto triangleNodeData(myTriangleList[myEdgeList[i].myTriangles[1]].myNodeData);
			if (triangleNodeData.myTargetType == MeshComponents::ENodeType::eDash)
			{
				color = SELECTED_EDGE_COLOR;
			}
			else if (triangleNodeData.myTargetType == MeshComponents::ENodeType::eNothing)
			{
				color = CLOSED_EGDE_COLOR;
			}
		}

#ifdef RENDER_WEIGHT
		const float val(myEdgeList[i].myNodeData.myWeight / myMaxWeight);
		color = { val, 0.25f, 0.0f };
#endif
		DT_DRAW_LINE_3_ARGS(from, to, color);

#ifdef RENDER_EDGE_DEPENDENCY
		CU::Vector3f vertexPos0;
		CU::Vector3f vertexPos1;
		CU::Vector3f vertexPos2;
		const CU::Vector3f edgePos = from + (to - from) / 2.0f;

		if (myEdgeList[i].myTriangles[0] != MAX_INDEX)
		{
			GetTrianglePosition(vertexPos0, vertexPos1, vertexPos2, myEdgeList[i].myTriangles[0]);
			const CU::Vector3f triPos = (vertexPos0 + vertexPos1 + vertexPos2) / 3.0f;

			sce::gfx::CDebugTools::Get()->DrawCube(triPos, 0.01f, FIRST_EDGE_COLOR);
			DT_DRAW_LINE_3_ARGS(edgePos, triPos, FIRST_EDGE_COLOR);
		}

		if (myEdgeList[i].myTriangles[1] != MAX_INDEX)
		{
			GetTrianglePosition(vertexPos0, vertexPos1, vertexPos2, myEdgeList[i].myTriangles[1]);
			const CU::Vector3f triPos = (vertexPos0 + vertexPos1 + vertexPos2) / 3.0f;

			sce::gfx::CDebugTools::Get()->DrawCube(triPos, 0.01f, SECOND_EDGE_COLOR);
			DT_DRAW_LINE_3_ARGS(edgePos, triPos, SECOND_EDGE_COLOR);
		}
#endif
	}

	for (IndexType i(0); i < myVertexList.Size(); ++i)
	{
		sce::gfx::CDebugTools::Get()->DrawCube(myVertexList[i].myPosition, 0.01f, VERTEX_COLOR);
	}

	for (IndexType i(0); i < myPathLineList.Size(); ++i)
	{
		if (i + 1 < myPathLineList.Size())
		{
			DT_DRAW_LINE_3_ARGS(myPathLineList[i], myPathLineList[i + 1], PATH_COLOR);
		}
	}
}

void CNavigationMesh::SelectBeginNode(const CU::Collision::Ray& aRay, const bool aUseRayAsReference, const float aHeightDifferenceLimit)
{
	CU::Vector3f intersection;
	myBeginNodeIndex = GetIntersectingNodeIndex(aRay, intersection, aUseRayAsReference, aHeightDifferenceLimit);

	if (myBeginNodeIndex == MAX_INDEX)
	{
		//GAME_LOG("ERROR! [CNavigationMesh] Failed to select a node of the beginning.");
	}
	else
	{
		myTriangleList[myBeginNodeIndex].myClickedPosition = intersection;
	}
}

void CNavigationMesh::SelectEndNode(const CU::Collision::Ray& aRay, const bool aUseRayAsReference, const float aHeightDifferenceLimit)
{
	CU::Vector3f intersection;
	myEndNodeIndex = GetIntersectingNodeIndex(aRay, intersection, aUseRayAsReference, aHeightDifferenceLimit);

	if (myEndNodeIndex == MAX_INDEX)
	{
		//GAME_LOG("ERROR! [CNavigationMesh] Failed to select a node of the end.");
	}
	else
	{
		myTriangleList[myEndNodeIndex].myClickedPosition = intersection;
	}
}

const bool CNavigationMesh::CalculatePath(const MeshComponents::ENodeType aTargetNodeType, const MeshComponents::ENodeType aAcceptableNodeType)
{
	if (myBeginNodeIndex == MAX_INDEX || myEndNodeIndex == MAX_INDEX)
	{
		//GAME_LOG("ERROR! [CNavigationMesh] Failed to calculate new path because begin or end node index was invalid. Begin index: %i. End index: %i.", myBeginNodeIndex, myEndNodeIndex);
		return false;
	}

	myPathFinder.SetPath(myBeginNodeIndex, myEndNodeIndex, aTargetNodeType, aAcceptableNodeType);
	myPathLineList = myPathFinder.GetPath();

	if (myPathLineList.size() <= 0)
	{
		return false;
	}

	return true;
}

const bool CNavigationMesh::CalculatePath(const MeshComponents::ENodeType aTargetNodeType)
{
	return CalculatePath(aTargetNodeType, MeshComponents::ENodeType::eWalk_Dash);
}

const bool CNavigationMesh::CalculatePath()
{
	return CalculatePath(MeshComponents::ENodeType::eWalk_Dash, MeshComponents::ENodeType::eWalk_Dash);
}

const bool CNavigationMesh::GetFirstEndEdgeIntersection(const CU::Collision::Ray& aBeginRay, const CU::Collision::Ray& aEndRay, CU::Vector3f& aIntersection_ref) const
{
	CU::Vector3f beginNavMeshPos;
	const IndexType beginNodeIndex(GetIntersectingNodeIndex(aBeginRay, beginNavMeshPos, true));

	if (beginNodeIndex == MAX_INDEX)
	{
		printf("ERROR! [CNavigationMesh] Failed to get first edge intersection because the begin index was not selected.\n");
		return false;
	}
	
	CU::Vector3f endNavMeshPos;
	if (!CU::Collision::IntersectionRayPlane(aEndRay, CU::Collision::Plane(beginNavMeshPos, CU::Vector3f(0.0f, 1.0f, 0.0f)), endNavMeshPos))
	{
		printf("ERROR! [CNavigationMesh] Failed to get first edge intersection because the end index was not selected.\n");
		return false;
	}

	if (beginNavMeshPos.y != endNavMeshPos.y)
	{
		const float heightDiff(std::abs(beginNavMeshPos.y - endNavMeshPos.y));

		if (heightDiff < 0.001f)
		{
			const float stableHeight(myVertexList[myTriangleList[beginNodeIndex].myVertices[0]].myPosition.y);
			beginNavMeshPos.y = stableHeight;
			endNavMeshPos.y = stableHeight;
		}
		else
		{
			//printf("Is much bigger which means that the terrain is supposed to have different height levels.\n");
			assert("Didn't expect the terrain to have different height levels." && false);
		}
	}

	const bool result(myPathFinder.GetFirstEndEdgeIntersection(beginNavMeshPos, endNavMeshPos, aIntersection_ref, beginNodeIndex));

	if (result == true)
	{
		const CU::Vector3f endToBeginDir((beginNavMeshPos - endNavMeshPos).GetNormalized());
		aIntersection_ref += endToBeginDir * 0.5f;
	}

	return result;
}

const bool CNavigationMesh::CheckNodeTypeOnRayCast(const CU::Collision::Ray& aRay, const MeshComponents::ENodeType aNodeType, const bool aUseRayAsReference, const float aHeightDifferenceLimit) const
{
	CU::Vector3f intersection;
	const IndexType intersectedTriangleIndex(GetIntersectingNodeIndex(aRay, intersection, aUseRayAsReference, aHeightDifferenceLimit));
	bool result(false);

	if (intersectedTriangleIndex != MAX_INDEX)
	{
		result = myTriangleList[intersectedTriangleIndex].myNodeData.myTargetType == aNodeType;
	}

	return result;
}

const bool CNavigationMesh::SetNodeTypeInSphere(const CU::Collision::Sphere& aSphere, const MeshComponents::ENodeType aOriginalNodeType, const MeshComponents::ENodeType aNewNodeType)
{
	bool triangleIsInside(false);
	bool foundAtleastOneTriangle(false);

	for (IndexType i(0); i < myTriangleList.Size(); ++i)
	{
		triangleIsInside = false;

		auto& currTriangle_ref(myTriangleList[i]);
		const CU::Vector3f vertex0( myVertexList[currTriangle_ref.myVertices[0]].myPosition );
		const CU::Vector3f vertex1( myVertexList[currTriangle_ref.myVertices[1]].myPosition );
		const CU::Vector3f vertex2( myVertexList[currTriangle_ref.myVertices[2]].myPosition );

		if (CU::Collision::PointInsideSphere(aSphere, vertex0))
		{
			triangleIsInside = true;
		}
		else if (CU::Collision::PointInsideSphere(aSphere, vertex1))
		{
			triangleIsInside = true;
		}
		else if (CU::Collision::PointInsideSphere(aSphere, vertex2))
		{
			triangleIsInside = true;
		}

		if (triangleIsInside)
		{
			if (currTriangle_ref.myNodeData.myOriginalType == aOriginalNodeType)
			{
				foundAtleastOneTriangle = true;

				SetEdgeNodeTypeInTriangle(currTriangle_ref, aOriginalNodeType, aNewNodeType);
				currTriangle_ref.myNodeData.myTargetType = aNewNodeType;
			}
		}
	}

	return foundAtleastOneTriangle;
}

const bool CNavigationMesh::ResetNodeTypeInSphere(const CU::Collision::Sphere& aSphere)
{
	bool triangleIsInside(false);
	bool foundAtleastOneTriangle(false);

	for (IndexType i(0); i < myTriangleList.Size(); ++i)
	{
		triangleIsInside = false;

		auto& currTriangle_ref(myTriangleList[i]);
		const CU::Vector3f vertex0(myVertexList[currTriangle_ref.myVertices[0]].myPosition);
		const CU::Vector3f vertex1(myVertexList[currTriangle_ref.myVertices[1]].myPosition);
		const CU::Vector3f vertex2(myVertexList[currTriangle_ref.myVertices[2]].myPosition);

		if (CU::Collision::PointInsideSphere(aSphere, vertex0))
		{
			triangleIsInside = true;
		}
		else if (CU::Collision::PointInsideSphere(aSphere, vertex1))
		{
			triangleIsInside = true;
		}
		else if (CU::Collision::PointInsideSphere(aSphere, vertex2))
		{
			triangleIsInside = true;
		}

		if (triangleIsInside)
		{
			if (currTriangle_ref.myNodeData.myTargetType != currTriangle_ref.myNodeData.myOriginalType)
			{
				foundAtleastOneTriangle = true;

				SetEdgeNodeTypeInTriangle(currTriangle_ref, currTriangle_ref.myNodeData.myOriginalType, currTriangle_ref.myNodeData.myOriginalType);
				currTriangle_ref.myNodeData.myTargetType = currTriangle_ref.myNodeData.myOriginalType;
			}
		}
	}

	return foundAtleastOneTriangle;
}

const bool CNavigationMesh::SetNodeTypeInAABB(const CU::Vector3f& aMin, const CU::Vector3f& aMax, const MeshComponents::ENodeType aOriginalNodeType, const MeshComponents::ENodeType aNewNodeType)
{
	bool triangleIsInside(false);
	bool foundAtleastOneTriangle(false);

	for (IndexType i(0); i < myTriangleList.Size(); ++i)
	{
		triangleIsInside = false;

		auto& currTriangle_ref(myTriangleList[i]);
		const CU::Vector3f vertex0(myVertexList[currTriangle_ref.myVertices[0]].myPosition);
		const CU::Vector3f vertex1(myVertexList[currTriangle_ref.myVertices[1]].myPosition);
		const CU::Vector3f vertex2(myVertexList[currTriangle_ref.myVertices[2]].myPosition);

		if (CU::Collision::PointInsideAABB(CU::Collision::AABB3D(aMin, aMax), vertex0))
		{
			triangleIsInside = true;
		}
		else if (CU::Collision::PointInsideAABB(CU::Collision::AABB3D(aMin, aMax), vertex1))
		{
			triangleIsInside = true;
		}
		else if (CU::Collision::PointInsideAABB(CU::Collision::AABB3D(aMin, aMax), vertex2))
		{
			triangleIsInside = true;
		}

		if (triangleIsInside)
		{
			if (currTriangle_ref.myNodeData.myOriginalType == aOriginalNodeType)
			{
				foundAtleastOneTriangle = true;

				SetEdgeNodeTypeInTriangle(currTriangle_ref, aOriginalNodeType, aNewNodeType);
				currTriangle_ref.myNodeData.myTargetType = aNewNodeType;
			}
		}
	}

	return foundAtleastOneTriangle;
}

const bool CNavigationMesh::ResetNodeTypeInAABB(const CU::Vector3f& aMin, const CU::Vector3f& aMax)
{
	bool triangleIsInside(false);
	bool foundAtleastOneTriangle(false);

	for (IndexType i(0); i < myTriangleList.Size(); ++i)
	{
		triangleIsInside = false;

		auto& currTriangle_ref(myTriangleList[i]);
		const CU::Vector3f vertex0(myVertexList[currTriangle_ref.myVertices[0]].myPosition);
		const CU::Vector3f vertex1(myVertexList[currTriangle_ref.myVertices[1]].myPosition);
		const CU::Vector3f vertex2(myVertexList[currTriangle_ref.myVertices[2]].myPosition);

		if (CU::Collision::PointInsideAABB(CU::Collision::AABB3D(aMin, aMax), vertex0))
		{
			triangleIsInside = true;
		}
		else if (CU::Collision::PointInsideAABB(CU::Collision::AABB3D(aMin, aMax), vertex1))
		{
			triangleIsInside = true;
		}
		else if (CU::Collision::PointInsideAABB(CU::Collision::AABB3D(aMin, aMax), vertex2))
		{
			triangleIsInside = true;
		}

		if (triangleIsInside)
		{
			if (currTriangle_ref.myNodeData.myTargetType != currTriangle_ref.myNodeData.myOriginalType)
			{
				foundAtleastOneTriangle = true;

				SetEdgeNodeTypeInTriangle(currTriangle_ref, currTriangle_ref.myNodeData.myOriginalType, currTriangle_ref.myNodeData.myOriginalType);
				currTriangle_ref.myNodeData.myTargetType = currTriangle_ref.myNodeData.myOriginalType;
			}
		}
	}

	return foundAtleastOneTriangle;
}

/* INTERSECTION */

const bool CNavigationMesh::IsIntersectingWithNavMesh(const CU::Collision::Ray& aRay) const
{
	const IndexType index(GetIntersectingNodeIndex(aRay));

	if (index == MAX_INDEX)
	{
		return false;
	}

	return true;
}

const bool CNavigationMesh::GetIntersection(const CU::Collision::Ray& aRay, CU::Vector3f& aIntersectionPoint_ref) const
{
	const IndexType index(GetIntersectingNodeIndex(aRay, aIntersectionPoint_ref));

	if (index == MAX_INDEX)
	{
		return false;
	}

	return true;
}

/* PRIVATE FUNCTIONS */

#ifndef NO_NAV_MESH_EDITING
void CNavigationMesh::SplitTriangles(const MeshComponents::SIntersectedEdge& aIntersectedEdge, const MeshComponents::SEdge& aLeftEdge, const MeshComponents::SEdge& aRightEdge,
	const IndexType aTriangleIndex1, const IndexType aTriangleIndex2)
{
	MeshComponents::STriangle* t1_ptr(nullptr);
	MeshComponents::STriangle* t2_ptr(nullptr);
	if (GetTriangleAdress(t1_ptr, t2_ptr, aTriangleIndex1, aTriangleIndex2) == false)
	{
		printf("Both triangles are invalid!\n");
		return;
	}

	IndexType t1e1_IntersectionIndex	= MAX_INDEX;
	IndexType t1e2_Index				= MAX_INDEX;
	IndexType t1e3_Index				= MAX_INDEX;
	IndexType t1v1_IntersectionIndex	= MAX_INDEX;
	IndexType t1v2_IntersectionIndex	= MAX_INDEX;
	IndexType t1v3_Index				= MAX_INDEX;

	IndexType t2e1_IntersectionIndex	= MAX_INDEX;
	IndexType t2e2_Index				= MAX_INDEX;
	IndexType t2e3_Index				= MAX_INDEX;
	IndexType t2v1_IntersectionIndex	= MAX_INDEX;
	IndexType t2v2_IntersectionIndex	= MAX_INDEX;
	IndexType t2v3_Index				= MAX_INDEX;

	for (IndexType i(0); i < 3; ++i)
	{
		if (aTriangleIndex1 != MAX_INDEX)
		{
			MeshComponents::SEdge& edge1_ref(myEdgeList[t1_ptr->myEdges[i]]);
			MeshComponents::SVertex& vertex1_ref(myVertexList[t1_ptr->myVertices[i]]);

			if (edge1_ref == myEdgeList[aIntersectedEdge.myIndex])
			{
				t1e1_IntersectionIndex = i;
			}

			if (vertex1_ref == myVertexList[aLeftEdge.myVertices[0]])
			{
				t1v1_IntersectionIndex = i;
			}
			else if (vertex1_ref == myVertexList[aRightEdge.myVertices[1]])
			{
				t1v2_IntersectionIndex = i;
			}
			else
			{
				t1v3_Index = i;
			}
		}

		if (aTriangleIndex2 != MAX_INDEX)
		{
			MeshComponents::SEdge& edge2_ref(myEdgeList[t2_ptr->myEdges[i]]);
			MeshComponents::SVertex& vertex2_ref(myVertexList[t2_ptr->myVertices[i]]);

			if (edge2_ref == myEdgeList[aIntersectedEdge.myIndex])
			{
				t2e1_IntersectionIndex = i;
			}

			if (vertex2_ref == myVertexList[aLeftEdge.myVertices[0]])
			{
				t2v1_IntersectionIndex = i;
			}
			else if (vertex2_ref == myVertexList[aRightEdge.myVertices[1]])
			{
				t2v2_IntersectionIndex = i;
			}
			else
			{
				t2v3_Index = i;
			}
		}
	}
	for (IndexType i(0); i < 3; ++i)
	{
		if (aTriangleIndex1 != MAX_INDEX)
		{
			if (i != t1e1_IntersectionIndex)
			{
				MeshComponents::SEdge& edge_ref(myEdgeList[t1_ptr->myEdges[i]]);
				MeshComponents::SVertex& eVertex1_ref(myVertexList[edge_ref.myVertices[0]]);
				MeshComponents::SVertex& eVertex2_ref(myVertexList[edge_ref.myVertices[1]]);

				MeshComponents::SVertex& tVertex1_ref(myVertexList[t1_ptr->myVertices[t1v3_Index]]);
				MeshComponents::SVertex& tIntersectionVertex1_ref(myVertexList[t1_ptr->myVertices[t1v1_IntersectionIndex]]);
				MeshComponents::SVertex& tIntersectionVertex2_ref(myVertexList[t1_ptr->myVertices[t1v2_IntersectionIndex]]);

				if (eVertex1_ref == tVertex1_ref && eVertex2_ref == tIntersectionVertex1_ref)
				{
					t1e2_Index = i;
				}
				else if (eVertex2_ref == tVertex1_ref && eVertex1_ref == tIntersectionVertex1_ref)
				{
					t1e2_Index = i;
				}
				if (eVertex1_ref == tVertex1_ref && eVertex2_ref == tIntersectionVertex2_ref)
				{
					t1e3_Index = i;
				}
				else if (eVertex2_ref == tVertex1_ref && eVertex1_ref == tIntersectionVertex2_ref)
				{
					t1e3_Index = i;
				}
			}
		}

		if (aTriangleIndex2 != MAX_INDEX)
		{
			if (i != t2e1_IntersectionIndex)
			{
				MeshComponents::SEdge& edge_ref(myEdgeList[t2_ptr->myEdges[i]]);
				MeshComponents::SVertex& eVertex1_ref(myVertexList[edge_ref.myVertices[0]]);
				MeshComponents::SVertex& eVertex2_ref(myVertexList[edge_ref.myVertices[1]]);

				MeshComponents::SVertex& tVertex1_ref(myVertexList[t2_ptr->myVertices[t2v3_Index]]);
				MeshComponents::SVertex& tIntersectionVertex1_ref(myVertexList[t2_ptr->myVertices[t2v1_IntersectionIndex]]);
				MeshComponents::SVertex& tIntersectionVertex2_ref(myVertexList[t2_ptr->myVertices[t2v2_IntersectionIndex]]);

				if (eVertex1_ref == tVertex1_ref && eVertex2_ref == tIntersectionVertex1_ref)
				{
					t2e2_Index = i;
				}
				else if (eVertex2_ref == tVertex1_ref && eVertex1_ref == tIntersectionVertex1_ref)
				{
					t2e2_Index = i;
				}
				if (eVertex1_ref == tVertex1_ref && eVertex2_ref == tIntersectionVertex2_ref)
				{
					t2e3_Index = i;
				}
				else if (eVertex2_ref == tVertex1_ref && eVertex1_ref == tIntersectionVertex2_ref)
				{
					t2e3_Index = i;
				}
			}
		}
	}

	if (aTriangleIndex1 != MAX_INDEX)
	{
		if (t1e1_IntersectionIndex > 2 || t1e2_Index > 2 || t1e3_Index > 2 || t1v1_IntersectionIndex > 2 || t1v2_IntersectionIndex > 2 || t1v3_Index > 2)
		{
			printf("One or more indices from t1 are corrupted!\n");
			return;
		}
	}

	if (aTriangleIndex2 != MAX_INDEX)
	{
		if (t2e1_IntersectionIndex > 2 || t2e2_Index > 2 || t2e3_Index > 2 || t2v1_IntersectionIndex > 2 || t2v2_IntersectionIndex > 2 || t2v3_Index > 2)
		{
			printf("One or more indices from t2 are corrupted!\n");
			return;
		}
	}
	
	// Copy and add new triangles

	MeshComponents::STriangle* t1_2ptr(nullptr);
	MeshComponents::STriangle* t2_2ptr(nullptr);
	IndexType t1_2Index(MAX_INDEX);
	IndexType t2_2Index(MAX_INDEX);
	{
		MeshComponents::STriangle t1_copy(*t1_ptr);
		MeshComponents::STriangle t2_copy(*t2_ptr);

		if (aTriangleIndex1 != MAX_INDEX)
		{
			myTriangleList.Add(t1_copy);
			t1_2Index = myTriangleList.Size() - 1;
		}

		if (aTriangleIndex2 != MAX_INDEX)
		{
			myTriangleList.Add(t2_copy);
			t2_2Index = myTriangleList.Size() - 1;
		}

		if (aTriangleIndex1 != MAX_INDEX)
		{
			t1_2ptr = &myTriangleList[t1_2Index]; // T1_2 will be modified later.
		}
		else if (aTriangleIndex2 != MAX_INDEX)
		{
			t1_2ptr = t2_2ptr;
		}

		if (aTriangleIndex2 != MAX_INDEX)
		{
			t2_2ptr = &myTriangleList[t2_2Index]; // T2_2 will be modified later.
		}
		else if (aTriangleIndex1 != MAX_INDEX)
		{
			t2_2ptr = t1_2ptr;
		}
	}

	GetTriangleAdress(t1_ptr, t2_ptr, aTriangleIndex1, aTriangleIndex2);

	// Create new edges for t1/t1_2 and t2/t2_2.

	IndexType t1_middleEdgeIndex(MAX_INDEX);
	IndexType t2_middleEdgeIndex(MAX_INDEX);
	{
		MeshComponents::SEdge newEdge;

		if (aTriangleIndex1 != MAX_INDEX)
		{
			newEdge = MeshComponents::SEdge(aLeftEdge.myVertices[1], t1_ptr->myVertices[t1v3_Index], aTriangleIndex1, t1_2Index);
			myEdgeList.Add(newEdge); // Creates new edge between t1 and t1_2.
			t1_middleEdgeIndex = myEdgeList.Size() - 1;
		}

		if (aTriangleIndex2 != MAX_INDEX)
		{
			newEdge = MeshComponents::SEdge(aLeftEdge.myVertices[1], t2_ptr->myVertices[t2v3_Index], aTriangleIndex2, t2_2Index);
			myEdgeList.Add(newEdge); // Creates new edge between t2 and t2_2.
			t2_middleEdgeIndex = myEdgeList.Size() - 1;
		}
	}

	// Re-shape t1 and t2.

	{
		IndexType intersectedEdgeIndex(MAX_INDEX);

		if (aTriangleIndex1 != MAX_INDEX)
		{
			t1_ptr->myVertices[t1v2_IntersectionIndex] = aLeftEdge.myVertices[1]; // Re-shape t1 vertex form.
			t1_ptr->myEdges[t1e3_Index] = t1_middleEdgeIndex; // Re-shape t1 edge form.
			intersectedEdgeIndex = t1_ptr->myEdges[t1e1_IntersectionIndex];
		}

		if (aTriangleIndex2 != MAX_INDEX)
		{
			t2_ptr->myVertices[t2v2_IntersectionIndex] = aLeftEdge.myVertices[1]; // Re-shape t2 vertex form.
			t2_ptr->myEdges[t2e2_Index] = t2_middleEdgeIndex; // Re-shape t2 edge form.
			intersectedEdgeIndex = t2_ptr->myEdges[t2e1_IntersectionIndex];
		}

		myEdgeList[intersectedEdgeIndex] = aLeftEdge; // Changes intersected edge to left edge. Modifies both for t1 and t2.
	}

	// Re-shape t1_2 and t2_2.

	myEdgeList.Add(aRightEdge);

	if (aTriangleIndex1 != MAX_INDEX)
	{
		t1_2ptr->myVertices[t1v1_IntersectionIndex] = aRightEdge.myVertices[0]; // Re-shape t1_2 vertex form.
		t1_2ptr->myEdges[t1e2_Index] = t1_middleEdgeIndex; // Re-shape t1_2 edge form.
		t1_2ptr->myEdges[t1e1_IntersectionIndex] = myEdgeList.Size() - 1; // Re-shape t1_2 edge form.
	}

	if (aTriangleIndex2 != MAX_INDEX)
	{
		t2_2ptr->myVertices[t2v1_IntersectionIndex] = aRightEdge.myVertices[0]; // Re-shape t2_2 vertex form.
		t2_2ptr->myEdges[t2e3_Index] = t2_middleEdgeIndex; // Re-shape t2_2 edge form.
		t2_2ptr->myEdges[t2e1_IntersectionIndex] = myEdgeList.Size() - 1; // Re-shape t2_2 edge form.
	}

	// Modify edges to refer to t1_2 and t2_2.

	{
		MeshComponents::SEdge& rightEdge_ref(myEdgeList.GetLast());

		if (aTriangleIndex1 != MAX_INDEX)
		{
			MeshComponents::SEdge& outerEdge_ref(myEdgeList[t1_2ptr->myEdges[t1e3_Index]]);
			if (outerEdge_ref.myTriangles[0] == aTriangleIndex1)
			{
				outerEdge_ref.myTriangles[0] = t1_2Index;
			}
			else
			{
				outerEdge_ref.myTriangles[1] = t1_2Index;
			}
			rightEdge_ref.myTriangles[0] = t1_2Index;
		}

		if (aTriangleIndex2 != MAX_INDEX)
		{
			MeshComponents::SEdge& outerEdge1_ref( myEdgeList[t2_ptr->myEdges[t2e3_Index]] );
			if (outerEdge1_ref.myTriangles[0] == aTriangleIndex2)
			{
				outerEdge1_ref.myTriangles[0] = t2_2Index;
			}
			else
			{
				outerEdge1_ref.myTriangles[1] = t2_2Index;
			}
			const IndexType t2e_IndxeTmp = t2_ptr->myEdges[t2e3_Index];
			t2_ptr->myEdges[t2e3_Index] = t2_2ptr->myEdges[t2e2_Index];
			t2_2ptr->myEdges[t2e2_Index] = t2e_IndxeTmp;

			rightEdge_ref.myTriangles[1] = t2_2Index;
		}
	}
}
#endif

void CNavigationMesh::SetEdgeNodeTypeInTriangle(MeshComponents::STriangle& aTriangle_ref, const MeshComponents::ENodeType aOriginalNodeType, const MeshComponents::ENodeType aNewNodeType)
{
	for (IndexType i(0); i < 3; ++i)
	{
		auto& edgeNodeData_ref(myEdgeList[aTriangle_ref.myEdges[i]].myNodeData);

		if (edgeNodeData_ref.myOriginalType == aOriginalNodeType)
		{
			edgeNodeData_ref.myTargetType = aNewNodeType;
		}
	}
}

const NavMesh::IndexType CNavigationMesh::GetIntersectingNodeIndex(const CU::Collision::Ray& aRay, const bool aUseRayAsReference, const float aHeightDifferenceLimit) const
{
	CU::Vector3f intersectionPoint_ref;
	return GetIntersectingNodeIndex(aRay, intersectionPoint_ref, aUseRayAsReference, aHeightDifferenceLimit);
}

const NavMesh::IndexType CNavigationMesh::GetIntersectingNodeIndex(const CU::Collision::Ray& aRay, CU::Vector3f& aIntersectionPoint_ref, const bool aUseRayAsReference, const float aHeightDifferenceLimit) const
{
	CU::Collision::Triangle triColli;
	CU::Vector3f intersectionPoint;
	IndexType intersectedTriangleIndex(MAX_INDEX);

	for (IndexType i(0); i < myTriangleList.Size(); ++i)
	{
		const auto& currTriangle(myTriangleList[i]);
		triColli.myPoints[0] = myVertexList[currTriangle.myVertices[0]].myPosition;
		triColli.myPoints[1] = myVertexList[currTriangle.myVertices[1]].myPosition;
		triColli.myPoints[2] = myVertexList[currTriangle.myVertices[2]].myPosition;
		triColli.myNormal = currTriangle.myNormal;

		if (CU::Collision::IntersectionTriangleRay(triColli, aRay, intersectionPoint))
		{
			const CU::Vector3f& vertex1Position(triColli.myPoints[0]);

			if (std::abs(vertex1Position.y - intersectionPoint.y) > 1.0f)
			{
				printf("WTF\n");
			}

			if (aUseRayAsReference)
			{
				if (intersectionPoint.y <= aRay.myOrigin.y && (intersectedTriangleIndex == MAX_INDEX || intersectionPoint.y > aIntersectionPoint_ref.y))
				{
					if (aHeightDifferenceLimit < 0.0f || std::abs(intersectionPoint.y - aRay.myOrigin.y) <= aHeightDifferenceLimit)
					{
						intersectedTriangleIndex = i;
						aIntersectionPoint_ref = intersectionPoint;
					}
				}
			}
			else
			{
				intersectedTriangleIndex = i;
				aIntersectionPoint_ref = intersectionPoint;
				break;
			}
		}
	}

	return intersectedTriangleIndex;
}

const bool CNavigationMesh::GetTriangleAdress(MeshComponents::STriangle*& aT1Ptr, MeshComponents::STriangle*& aT2Ptr, const IndexType aT1Index, const IndexType aT2Index)
{
	if (aT1Index != MAX_INDEX)
	{
		aT1Ptr = &myTriangleList[aT1Index];
	}
	else if (aT2Index != MAX_INDEX)
	{
		aT1Ptr = &myTriangleList[aT2Index];
	}

	if (aT2Index != MAX_INDEX)
	{
		aT2Ptr = &myTriangleList[aT2Index];
	}
	else if (aT1Index != MAX_INDEX)
	{
		aT2Ptr = &myTriangleList[aT1Index];
	}

	return (aT1Ptr != nullptr || aT2Ptr != nullptr);
}

void CNavigationMesh::GatherIntersectedEdges(IntersectedEdgeList& aEdgeList, const CU::Vector3f& aFirstPos, const CU::Vector3f& aSecondPos)
{
	CU::Collision::LineSegment mainLineSeg(aFirstPos, aSecondPos);
	CU::Vector3f intersection;

	for (IndexType i(0); i < myEdgeList.Size(); ++i)
	{
		const CU::Vector3f& vertexStartPosition = myVertexList[myEdgeList[i].myVertices[0]].myPosition;
		const CU::Vector3f& vertexEndPosition = myVertexList[myEdgeList[i].myVertices[1]].myPosition;

		CU::Collision::LineSegment currLineSeg(vertexStartPosition, vertexEndPosition);

		if (CU::Collision::IntersectionLineLine(mainLineSeg, currLineSeg, intersection))
		{
			aEdgeList.Add(MeshComponents::SIntersectedEdge(intersection, i));
		}
	}

	IndexType edgeIndexOfIntersection;
	IndexType edgeIndexInEdgeList;

	for (IndexType i(0); i < myTriangleList.Size(); ++i)
	{
		if (GetEdgeIntersection(myTriangleList[i], aFirstPos, aSecondPos, intersection, edgeIndexOfIntersection, true))
		{
			edgeIndexInEdgeList = myTriangleList[i].myEdges[edgeIndexOfIntersection];
			aEdgeList.Add(MeshComponents::SIntersectedEdge(intersection, edgeIndexInEdgeList));
		}

		if (GetEdgeIntersection(myTriangleList[i], aFirstPos, aSecondPos, intersection, edgeIndexOfIntersection, false))
		{
			edgeIndexInEdgeList = myTriangleList[i].myEdges[edgeIndexOfIntersection];
			aEdgeList.Add(MeshComponents::SIntersectedEdge(intersection, edgeIndexInEdgeList));
		}
	}
}

const bool CNavigationMesh::GetEdgeIntersection(const MeshComponents::STriangle& aTriangle, const CU::Vector3f& aFirstPos, const CU::Vector3f& aSecondPos, CU::Vector3f& aHitPos,
	IndexType& aEdgeIdex, const bool aPointIsFirst) const
{
	CU::Collision::Triangle tri;
	tri.myPoints[0] = myVertexList[aTriangle.myVertices[0]].myPosition;
	tri.myPoints[1] = myVertexList[aTriangle.myVertices[1]].myPosition;
	tri.myPoints[2] = myVertexList[aTriangle.myVertices[2]].myPosition;

	CU::Collision::LineSegment mainLineSeg;
	if (aPointIsFirst)
	{
		mainLineSeg.myP0 = aFirstPos;
		mainLineSeg.myP1 = aFirstPos + (aFirstPos - aSecondPos).GetNormalized() * 30000.0f;
	}
	else
	{
		mainLineSeg.myP0 = aSecondPos;
		mainLineSeg.myP1 = aSecondPos + (aSecondPos - aFirstPos).GetNormalized() * 30000.0f;
	}

	if (CU::Collision::IntersectionTrianglePoint(tri, mainLineSeg.myP0))
	{
		CU::Collision::LineSegment currLineSeg;

		for (IndexType i(0); i < 3; ++i)
		{
			currLineSeg.myP0 = myVertexList[myEdgeList[aTriangle.myEdges[i]].myVertices[0]].myPosition;
			currLineSeg.myP1 = myVertexList[myEdgeList[aTriangle.myEdges[i]].myVertices[1]].myPosition;

			if (CU::Collision::IntersectionLineLine(mainLineSeg, currLineSeg, aHitPos))
			{
				aEdgeIdex = i;
				return true;
			}
		}
	}

	return false;
}

/* LOADING */

void CNavigationMesh::ReInit(const CMapData::SLevelInfo::SNavMesh& aNavMeshData)
{
	const int triangleListSize((IndexType)aNavMeshData.myIndices.Size() / 3);
	if (triangleListSize <= 0)
	{
		GAME_LOG("ERROR! Failed to load navigation mesh because there was no data to use.");
		return;
	}

	myTriangleList.Init((IndexType)triangleListSize);
	myVertexList.Init((IndexType)(triangleListSize * 3));
	myEdgeList.Init((IndexType)(triangleListSize * 3));
	myMaxWeight = DEFAULT_WEIGHT;

	for (int vertexIndex(0); vertexIndex < aNavMeshData.myVertices.Size(); ++vertexIndex)
	{
		myVertexList.Add(MeshComponents::SVertex(aNavMeshData.myVertices[vertexIndex]));
		//myVertexList.GetLast().myPosition.y += 1;
	}

	for (int triangleIndex(0); triangleIndex < triangleListSize; ++triangleIndex)
	{
		const IndexType index0((IndexType)aNavMeshData.myIndices[(triangleIndex * 3) + 0]);
		const IndexType index1((IndexType)aNavMeshData.myIndices[(triangleIndex * 3) + 1]);
		const IndexType index2((IndexType)aNavMeshData.myIndices[(triangleIndex * 3) + 2]);

		IndexType edgeIndex0(MAX_INDEX);
		IndexType edgeIndex1(MAX_INDEX);
		IndexType edgeIndex2(MAX_INDEX);
		const MeshComponents::ENodeType nodeType(static_cast<MeshComponents::ENodeType>(aNavMeshData.myTriangleTypes[triangleIndex]));

		GetMatchingEdgeIndices(edgeIndex0, edgeIndex1, edgeIndex2, index0, index1, index2, (IndexType)triangleIndex);

		if (edgeIndex0 == MAX_INDEX)
		{
			MeshComponents::SEdge newEdge(MeshComponents::SEdge(index0, index1, (IndexType)triangleIndex, MAX_INDEX));
			newEdge.myNodeData.myTargetType = nodeType;
			newEdge.myNodeData.myOriginalType = nodeType;

			edgeIndex0 = myEdgeList.Size();
			myEdgeList.Add(newEdge);
		}

		if (edgeIndex1 == MAX_INDEX)
		{
			MeshComponents::SEdge newEdge(MeshComponents::SEdge(index1, index2, (IndexType)triangleIndex, MAX_INDEX));
			newEdge.myNodeData.myTargetType = nodeType;
			newEdge.myNodeData.myOriginalType = nodeType;

			edgeIndex1 = myEdgeList.Size();
			myEdgeList.Add(newEdge);
		}

		if (edgeIndex2 == MAX_INDEX)
		{
			MeshComponents::SEdge newEdge(MeshComponents::SEdge(index2, index0, (IndexType)triangleIndex, MAX_INDEX));
			newEdge.myNodeData.myTargetType = nodeType;
			newEdge.myNodeData.myOriginalType = nodeType;

			edgeIndex2 = myEdgeList.Size();
			myEdgeList.Add(newEdge);
		}
		 
		const auto& point0(myVertexList[index0].myPosition);
		const auto& point1(myVertexList[index1].myPosition);
		const auto& point2(myVertexList[index2].myPosition);
		const CU::Vector3f normal((point1 - point0).Cross(point2 - point1).GetNormalized());

		myTriangleList.Add(MeshComponents::STriangle(edgeIndex0, edgeIndex1, edgeIndex2, index0, index1, index2, normal));
		
		myTriangleList.GetLast().myNodeData.myWeight = (WeightType)1;
		myTriangleList.GetLast().myNodeData.myTargetType = nodeType;
		myTriangleList.GetLast().myNodeData.myOriginalType = nodeType;
	}

	//printf("-Max Weight: %f\n", myMaxWeight);
}

void CNavigationMesh::GetMatchingEdgeIndices(IndexType& aEdgeIndex1_ref, IndexType& aEdgeIndex2_ref, IndexType& aEdgeIndex3_ref,
	const IndexType aVertexIndex0, const IndexType aVertexIndex1, const IndexType aVertexIndex2, const IndexType aCurrTriIndex)
{
	for (IndexType i(0); i < myEdgeList.Size(); ++i)
	{
		IndexType& secondTriIndex_ref(myEdgeList[i].myTriangles[1]);
		if (secondTriIndex_ref != MAX_INDEX)
		{
			continue;
		}

		const IndexType currVertex0(myEdgeList[i].myVertices[0]);
		const IndexType currVertex1(myEdgeList[i].myVertices[1]);

		if ((currVertex0 == aVertexIndex0 && currVertex1 == aVertexIndex1) || (currVertex0 == aVertexIndex1 && currVertex1 == aVertexIndex0))
		{
			aEdgeIndex1_ref = i;
			secondTriIndex_ref = aCurrTriIndex;
		}
		else if ((currVertex0 == aVertexIndex1 && currVertex1 == aVertexIndex2) || (currVertex0 == aVertexIndex2 && currVertex1 == aVertexIndex1))
		{
			aEdgeIndex2_ref = i;
			secondTriIndex_ref = aCurrTriIndex;
		}
		else if ((currVertex0 == aVertexIndex2 && currVertex1 == aVertexIndex0) || (currVertex0 == aVertexIndex0 && currVertex1 == aVertexIndex2))
		{
			aEdgeIndex3_ref = i;
			secondTriIndex_ref = aCurrTriIndex;
		}
	}
}
