#pragma once

class CPathFindingWrapper
{
public:
	~CPathFindingWrapper();

private:
	CPathFindingWrapper();
	CPathFindingWrapper(CPathFindingWrapper&& aPathFindingWrapper) = delete;
	CPathFindingWrapper(const CPathFindingWrapper& aPathFindingWrapper) = delete;
	void operator=(const CPathFindingWrapper& aPathFindingWrapper) = delete;

private:
	static CPathFindingWrapper* myInstance_ptr;
};