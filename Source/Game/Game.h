#pragma once
#include "..\EngineCore\EventSystem\EventReceiver.h"

using EntityManager = void;
using Cursors = void;

class CGame : public sce::CEventReceiver
{
public:
	CGame();
	~CGame() override;

	void Init();
	void Update(const float aDeltaTime);
	void Shutdown();

	void ReceiveEvent(const Event::SEvent& aEvent) override;

private:
	enum class ECursors : unsigned char
	{
		Default,
		MouseDown,
		SIZE,
	};

	EntityManager* myEntityManagerVoid;
	Cursors* myCursors;

};

