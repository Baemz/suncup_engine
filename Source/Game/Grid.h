#pragma once
#include "GridCell.h"
#include "..\CommonUtilities\GrowingArray.h"
#include "..\CommonUtilities\Vector.h"

class CGrid
{
	using CellCountType = unsigned int;

public:
	CGrid();
	~CGrid();

	void Init(const float& aCellSize, const CU::Vector4f& aAABB);

	void AddObject(const SGridData& aEntity);
	void RemoveObject(const UniqueIndexType& aEntityIndex);
	void UpdateObjectGridPosition(const UniqueIndexType& aEntityIndex, const CU::Vector2f aNewCenterPosition);
	void SetObjectOnGridLeaveCallback(const UniqueIndexType& aEntityIndex, std::function<void()> aOnGridLeaveCallback);

	bool GetObjects(CU::GrowingArray<UniqueIndexType>& aArrayToFill);

	void Clear();

	bool GetObjectsCloseToPoint(const CU::Vector2f& aPoint, CU::GrowingArray<SGridData>& aArrayToFill) const;
	bool GetObjectsCloseToAABB(const CU::Vector4f& aAABB, CU::GrowingArray<SGridData>& aArrayToFill) const;

	void DebugRender();

	inline const CU::Vector4f& GetAABB() const { return myAABB; }
	inline void ToggleShouldRenderGrid() { myShouldRenderGrid = !myShouldRenderGrid; }

private:
	static constexpr CellCountType InvalidCellIndex = std::numeric_limits<CellCountType>::max();

	CellCountType GetCellIndexOver(const CU::Vector2f& aPosition, const bool aValidOutside, const bool aRoundUp) const;

	void ForeachCell(const CellCountType& aMinGrid, const CellCountType& aMaxGrid, const std::function<void(const CellCountType, SGridCell&)>& aFunction);
	void ForeachCell(const CellCountType& aMinGrid, const CellCountType& aMaxGrid, const std::function<void(const CellCountType, const SGridCell&)>& aFunction) const;

	CellCountType myRows;
	CellCountType myColumns;
	float myCellSize;

	CU::Vector4f myAABB;
	CU::GrowingArray<SGridCell, CellCountType> myCells;
	std::unordered_multimap<UniqueIndexType, CellCountType> myEntityIndices;

	bool myShouldRenderGrid;

};
