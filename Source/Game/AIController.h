#pragma once
#include "Controller.h"

class CAIController : public CController
{
public: 
	
	CAIController() { myControllerType = EController::AI; }
	CAIController(const SControllerInit& aInitData, const SAIInit& aAIInitData);

	eWorldInterfaceTag myWorldInterfaceTag;
};