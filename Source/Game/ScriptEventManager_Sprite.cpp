#include "stdafx.h"
#include "ScriptEventManager_Sprite.h"
#include "EntitySystemLink\RenderComponents.h"
#include "ScriptEventManager.h"
#include "GameSystems\ExposedFunctions\GameLogicFunctions.h"

#define CHECK_IF_STATE_IS_REMOVED(aStateIndex, aReturnValue)																\
std::shared_lock<std::shared_mutex> sharedLock(aScriptEventManager.myStatesToRemoveMutex);									\
if (aScriptEventManager.myStatesToRemove.Find(aStateIndex) != CU::GrowingArray<CScriptManager::StateIndexType>::FoundNone)	\
return aReturnValue;

bool CScriptEventManager_Sprite::InitFunctions(CScriptEventManager& aScriptEventManager)
{
	CScriptManager& myScriptManager(aScriptEventManager.myScriptManager);

	std::function<CompHUDElements::IndexType(const CScriptManager::StateIndexType, const char*)> addHUDElementBind(
		[&aScriptEventManager](const CScriptManager::StateIndexType aStateIndex, const char* aSpritePath) -> CompHUDElements::IndexType
	{
		CHECK_IF_STATE_IS_REMOVED(aStateIndex, CompHUDElements::invalid;);

		UniqueIndexType uniqueIndex(aScriptEventManager.GetEntityUniqueIndex(aStateIndex));
		return CGameLogicFunctions::AddHUDElement(uniqueIndex, aSpritePath);
	});
	myScriptManager.RegisterFunctionCallWithStateIndex("AddHUDElement", addHUDElementBind
		, "String"
		, "Adds the sprite with the [path] to the entity's HUD display");

	std::function<void(const CScriptManager::StateIndexType, CompHUDElements::IndexType, float, float)> setHUDElementPositionBind(
		[&aScriptEventManager](const CScriptManager::StateIndexType aStateIndex, CompHUDElements::IndexType aHUDIndex, float aPositionX, float aPositionY)
	{
		CHECK_IF_STATE_IS_REMOVED(aStateIndex, ;);

		UniqueIndexType uniqueIndex(aScriptEventManager.GetEntityUniqueIndex(aStateIndex));
		const CU::Vector2f position(aPositionX, aPositionY);
		CGameLogicFunctions::SetHUDElementPosition(uniqueIndex, aHUDIndex, position);
	});
	myScriptManager.RegisterFunctionCallWithStateIndex("SetHUDElementPosition", setHUDElementPositionBind
		, "Number, Number, Number"
		, "With HUD element [Index] set position to [x], [y]");

	std::function<void(const CScriptManager::StateIndexType, CompHUDElements::IndexType, float, float)> setHUDElementScaleBind(
		[&aScriptEventManager](const CScriptManager::StateIndexType aStateIndex, CompHUDElements::IndexType aHUDIndex, float aScaleX, float aScaleY)
	{
		CHECK_IF_STATE_IS_REMOVED(aStateIndex, ;);

		UniqueIndexType uniqueIndex(aScriptEventManager.GetEntityUniqueIndex(aStateIndex));
		const CU::Vector2f scale(aScaleX, aScaleY);
		CGameLogicFunctions::SetHUDElementScale(uniqueIndex, aHUDIndex, scale);
	});
	myScriptManager.RegisterFunctionCallWithStateIndex("SetHUDElementScale", setHUDElementScaleBind
		, "Number, Number, Number"
		, "With HUD element [Index] set scale to [x], [y]");

	std::function<void(const CScriptManager::StateIndexType, CompHUDElements::IndexType, float, float, float, float)> setHUDElementCropBind(
		[&aScriptEventManager](const CScriptManager::StateIndexType aStateIndex, CompHUDElements::IndexType aHUDIndex, float aCropX, float aCropY, float aCropZ, float aCropW)
	{
		CHECK_IF_STATE_IS_REMOVED(aStateIndex, ;);

		UniqueIndexType uniqueIndex(aScriptEventManager.GetEntityUniqueIndex(aStateIndex));
		const CU::Vector4f crop(aCropX, aCropY, aCropZ, aCropW);
		CGameLogicFunctions::SetHUDElementCrop(uniqueIndex, aHUDIndex, crop);
	});
	myScriptManager.RegisterFunctionCallWithStateIndex("SetHUDElementCrop", setHUDElementCropBind
		, "Number, Number, Number, Number, Number"
		, "With HUD element [Index] set crop to [startClampX], [startClampY], [endClampX], [endClampY]");

	std::function<void(const CScriptManager::StateIndexType, CompHUDElements::IndexType, float, float)> setHUDElementPivotBind(
		[&aScriptEventManager](const CScriptManager::StateIndexType aStateIndex, CompHUDElements::IndexType aHUDIndex, float aPivotX, float aPivotY)
	{
		CHECK_IF_STATE_IS_REMOVED(aStateIndex, ;);

		UniqueIndexType uniqueIndex(aScriptEventManager.GetEntityUniqueIndex(aStateIndex));
		CGameLogicFunctions::SetHUDElementPivot(uniqueIndex, aHUDIndex, CU::Vector2f(aPivotX, aPivotY));
	});
	myScriptManager.RegisterFunctionCallWithStateIndex("SetHUDElementPivot", setHUDElementPivotBind
		, "Number, Number, Number"
		, "With HUD element [Index] set pivot point to [pivotX], [pivotY]");

	std::function<CU::Vector3f(const CScriptManager::StateIndexType, CompHUDElements::IndexType)> getHUDElementSpriteSizeBind(
		[&aScriptEventManager](const CScriptManager::StateIndexType aStateIndex, CompHUDElements::IndexType aHUDIndex) -> CU::Vector3f
	{
		CHECK_IF_STATE_IS_REMOVED(aStateIndex, CU::Vector3f(););

		UniqueIndexType uniqueIndex(aScriptEventManager.GetEntityUniqueIndex(aStateIndex));
		
		const CU::Vector2f spriteSize(CGameLogicFunctions::GetHUDElementSpriteSize(uniqueIndex, aHUDIndex));
		return CU::Vector3f(spriteSize.x, spriteSize.y, -1);
	});
	myScriptManager.RegisterFunctionCallWithStateIndex("GetHUDElementSpriteSize", getHUDElementSpriteSizeBind
		, "Number"
		, "Gets the sprite size as a vector3 (with z invalid) of HUD element [Index]");

	std::function<CU::Vector3f(float, float, float)> getProjectedScreenPositionBind(
		[](float aX, float aY, float aZ) -> CU::Vector3f
	{
		const CU::Vector2f projectedScreenPos(CGameLogicFunctions::GetPositionProjectedToScreen(CU::Vector3f(aX, aY, aZ)));
		return CU::Vector3f(projectedScreenPos.x, projectedScreenPos.y, -1);
	});
	myScriptManager.RegisterFunction("GetProjectedScreenPosition", getProjectedScreenPositionBind
		, "Number, Number, Number"
		, "Gets the position projected onto the screen as a vector3 (with z invalid)");

	std::function<void(float)> setBlackBarsRadiusBind(
		[](float aRadius)
	{
		CGameLogicFunctions::SetBlackBarsRadius(aRadius);
	});
	myScriptManager.RegisterFunction("SetBlackBarsRadius", setBlackBarsRadiusBind
		, "Number"
		, "Sets the screenspace radius of the enveloping black bars.");

	std::function<const char*(UniqueIndexType)> getEnemyNameBind(
		[](UniqueIndexType aEntityUniqueIndex) -> const char*
	{
		return CGameLogicFunctions::GetEnemyName(aEntityUniqueIndex).c_str();
	});
	myScriptManager.RegisterFunction("GetEnemyName", getEnemyNameBind
		, "Number"
		, "Gets the general name of the object entered");


	return true;
}
