#pragma once
#include "State.h"
#include "Menu.h"
#include "../GraphicsEngine/Sprite/Sprite.h"

class CMainMenuState : private CState
{
public:
	CMainMenuState(EManager& aEntityManager);
	~CMainMenuState();

	void Init() override;
	void Update(const float aDeltaTime) override;
	void Render() override;

	void OnActivation() override;
	void OnDeactivation() override;

private:
	void UpdateFadeAndStartGameOnCommand(const float aDeltaTime);

private:
	CMenu myMenu;
	sce::gfx::CSprite myFadeSprite; // HACK (all this fade)
	float myCurrFadeTime;
	bool myIsFadingIn;
	bool myShouldFade;
	bool myPressedStart;
	bool myPressedCredits;
};