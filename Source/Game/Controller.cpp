#include "stdafx.h"
#include "Controller.h"

CController::CController()
	: myMaxSpeed(0.0f)
	, myMaxForce(0.0f)
	, myRotationSpeed(0.0f)
	, myControllerType(EController::NONE)
	, myScriptStateIndex(static_cast<decltype(myScriptStateIndex)>(-1))
	, myScriptIndices(nullptr)
	, myScriptNumbers(nullptr)
	, myScriptStrings(nullptr)
	, myLoaded(ELoadState::Unloaded)
{
	myCurrentPath.Reserve(64);
}

CController::CController(const SControllerInit & aInitData)
	: myMaxSpeed(aInitData.myMaxSpeed)
	, myMaxForce(aInitData.myMaxForce)
	, myRotationSpeed(aInitData.myRotationSpeed)
	, myControllerType(EController::NONE)
	, myScriptStateIndex(static_cast<decltype(myScriptStateIndex)>(-1))
	, myScriptIndices(nullptr)
	, myScriptNumbers(nullptr)
	, myScriptStrings(nullptr)
	, myLoaded(ELoadState::Unloaded)
{
	myCurrentPath.Reserve(64);
}

CController::~CController()
{
	if (myScriptIndices != nullptr) sce_delete(myScriptIndices);
	if (myScriptNumbers != nullptr) sce_delete(myScriptNumbers);
	if (myScriptStrings != nullptr) sce_delete(myScriptStrings);
}
