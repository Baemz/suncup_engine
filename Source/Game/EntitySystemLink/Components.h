#pragma once
#include "../../EntitySystem/ESSettingsLight.h"
#include "../../EntitySystem/Handle.h"
#include "CommonComponents.h"
#include "RenderComponents.h"
#include "PlayerComponents.h"
#include "SteeringComponents.h"

//-------------------------------------LIST-----------------------------------
using MyComponents = ComponentList
<
	CompTransform,
	CompController,
	CompHandle,
	CompModelInstance,
	CompHealth,
	CompPlayerData,
	CompHUDElements,
	CompCameraInstance,
	CompCameraControl,
	CompPointLight,
	CompSpotLight,
	CompDirectionalLight,
	CompParticleEmitter,
	CompArriveBehavior,
	CompEvadeBehavior,
	CompWanderBehavior,
	CompTrigger
>;
