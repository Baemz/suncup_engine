#pragma once
#include "Components.h"
#include "Tags.h"
#include "../../EntitySystem/ESSettingsLight.h"

using SigRender = Signature<CompTransform, CompModelInstance>;
using SigRenderPointLight = Signature<CompTransform, CompPointLight>;
using SigRenderSpotLight = Signature<CompTransform, CompSpotLight>;
using SigRenderDirectionalLight = Signature<CompDirectionalLight>;
using SigRenderParticles = Signature<CompParticleEmitter>;

using SigCamera = Signature<CompCameraInstance, TagCamera>;

using SigRenderDebugCubes = Signature<CompTransform, CompController>;

using SigController = Signature<CompTransform, CompController>;
using SigInputController = Signature<CompTransform, CompController, TagInputController>;
using SigAIController = Signature<CompTransform, CompController, TagAIController>;
using SigScriptController = Signature<CompTransform, CompController, TagScriptController>;

using SigRenderSimpleGrid = Signature<CompSimpleAABBGrid>;

using SigInitScript = Signature<CompController, TagScriptController>;

using SigSeekBehavior = Signature<CompTransform, CompController, CompSeekBehavior>;
using SigFleeBehavior = Signature<CompTransform, CompController, CompFleeBehavior>;
using SigArriveBehavior = Signature<CompTransform, CompController, CompArriveBehavior>;
using SigEvadeBehavior = Signature<CompTransform, CompController, CompEvadeBehavior>;
using SigWanderBehavior = Signature<CompTransform, CompController, CompWanderBehavior>;

using SigRenderChildrenDebug = Signature<CompTransform, TagChild>;

using SigTrigger = Signature<CompTrigger>;
using SigEnemyTag = Signature<TagEnemy>;
using SigPaintingTag = Signature<TagPainting>;

using SigProjectileCollisions = Signature<CompTransform, TagProjectile>;
using SigHealthPickupCollisions = Signature<CompTransform, TagHealthPickup>;
using SigCheckpointCollisions = Signature<CompTransform, TagCheckpoint>;

using MySignatures = SignatureList
<
	SigRender,
	SigCamera,
	SigRenderPointLight,
	SigRenderSpotLight,
	SigRenderDirectionalLight,
	SigRenderParticles,
	SigRenderDebugCubes,
	SigController,
	SigInputController,
	SigAIController,
	SigScriptController,
	SigInitScript,
	SigSeekBehavior,
	SigFleeBehavior,
	SigArriveBehavior,
	SigEvadeBehavior,
	SigWanderBehavior,
	SigRenderChildrenDebug,
	SigTrigger,
	SigEnemyTag,
	SigPaintingTag,
	SigProjectileCollisions,
	SigHealthPickupCollisions,
	SigCheckpointCollisions
>;

//Parameter list for all signature lambda functions:
//1: Index of this entity
//Rest: The required components, in the order listed in the template arguments above.
//Capture list is optional, you can pass things like deltatime or the manager, for killing of objects. Remember to specify the entity manager as a reference to member entitymanager.

//auto LAMBDAFUNCTION = [&myEntityManager = myEntityManager, aDeltaTime] (auto& ENTITYINDEX, auto& COMPONENT1, auto& COMPONENT2, auto& COMPONENT...)
//{
//	//code to run
//
//	if (aComponent1.myTimer <= 0) myEntityManager.Kill(aEntityIndex);
//};

//aEntityManager.ForEntitiesMatching<SIGNATURE>(LAMBDAFUNCTION);