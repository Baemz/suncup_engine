#pragma once

#include "CommonComponents.h"

struct CompInput
{
	CU::Matrix44f myCameraChildOffset;

};

struct CompCameraControl
{
	CU::Quaternion myOrientation;
	CU::Vector3f myRotationDelta;
	CU::Vector3f myMaxSpeeds;
	CU::Vector3f myAccelerationTimes;
	CU::Vector3f myCurrentSpeeds;
	CU::Vector3f mySpeedAccumulations;
	float myElapsedTime;
	float myNoClipSpeedMultiplier;
	bool myIsNoClip;

	CompCameraControl()
		: myMaxSpeeds({ 2.0f, 2.0f, 2.0f })
		, myAccelerationTimes({ 0.25f, 0.25f, 0.25f })
		, myElapsedTime(0.0f)
		, myNoClipSpeedMultiplier(1.2f)
	{
	}
};

struct CompInputController
{
	CU::Vector3f myTarget;
	float mySpeed = 2.0f;
};