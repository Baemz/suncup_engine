#pragma once
#include "../../CommonUtilities/Vector.h"
#include "../../CommonUtilities/Matrix.h"
#include "../../CommonUtilities/Quaternion.h"

#include "../../CommonUtilities/Intersection.h"
#include "ControllerWrapper.h"
#include "../EntitySystem/Handle.h"


struct CompTransform
{
	CompTransform()
		: myMatrix()
	{};

	CompTransform(const CU::Matrix44f& aMatrix)
		: myMatrix(aMatrix)
	{}

	CompTransform(const CU::Vector3f& aPosition)
		: myPosition(aPosition)

	{}

	~CompTransform()
	{}

	void AddChild(SEntityHandle aHandle)
	{
		if (myChildrenCount < MaxChildren)
		{
			myChildHandles[myChildrenCount] = aHandle;
			++myChildrenCount;
		}
	}

	static constexpr std::size_t MaxChildren = 8;

	CU::Quaternion myLookRotation;
	CU::Vector3f myWorldPos;
	char myChildrenCount = 0;
	SEntityHandle myChildHandles[MaxChildren];
	float myRadius;

	union
	{
#pragma warning(push)
#pragma warning(disable : 4201)
		struct  
		{
			CU::Matrix44f myMatrix;
		};
		struct 
		{
			struct 
			{
				float _myTrash[12];
			};
			union
			{
				struct
				{
					CU::Vector4f myPosition4;
				};
				struct
				{
					CU::Vector3f myPosition;
					float myPositionW;
				};
			};
		};
#pragma warning(pop)
	};

};

struct CompHandle
{
	CompHandle() = default;
	CompHandle(const SEntityHandle& aHandle)
		: myHandle(aHandle)
	{}
	SEntityHandle myHandle;
};

struct CompHealth 
{ 
	CompHealth()
		: myHealth(-1)
		, myMaxHealth(-1)
	{ }

	inline float& Get() { return myHealth; }
	float myHealth; 
	float myMaxHealth;
};

struct CompSimpleAABBGrid
{
	CompSimpleAABBGrid()
		: myCellSize(0)
	{}

	CompSimpleAABBGrid(const float aCellSize, const CU::Vector2f& aGridWorldSize)
		: myCellSize(aCellSize)
	{
		const unsigned short cellAmountX = (unsigned short)std::ceil(aGridWorldSize.x / aCellSize);
		const unsigned short cellAmountY = (unsigned short)std::ceil(aGridWorldSize.y / aCellSize);
		
		myCells.Resize(cellAmountX * cellAmountY);

		for (unsigned short i = 0; i < cellAmountX * cellAmountY; ++i)
		{
			myCells[i].myMin = { (i % cellAmountX) * aCellSize, -0.1f, (i / cellAmountY) * aCellSize };
			myCells[i].myMax = { ((i % cellAmountX) + 1) * aCellSize, 0.0f, ((i / cellAmountY) + 1) * aCellSize };
			myCells[i].myIsTargeted = false;

			myCells[i].myMin.x -= cellAmountX * myCellSize * 0.5f;
			myCells[i].myMin.z -= cellAmountY * myCellSize * 0.5f;
			myCells[i].myMax.x -= cellAmountX * myCellSize * 0.5f;
			myCells[i].myMax.z -= cellAmountY * myCellSize * 0.5f;
		}
	}

	CompSimpleAABBGrid& operator=(const CompSimpleAABBGrid& aCompSimpleAABBGrid)
	{
		assert("can't just assign a grid to another grid like that, for real." && myCellSize == aCompSimpleAABBGrid.myCellSize);
		myCells = aCompSimpleAABBGrid.myCells;
		return *this;
	}

	const float myCellSize;
	CU::GrowingArray<CU::Collision::AABB3D> myCells;
};

struct CompController
{
	CompController() = default;
	CompController(const SControllerWrapperInit& aInitData)
		: myController(aInitData)
	{
	}

	CController& Get() { return myController.Get(); }
	const CController& Get() const { return myController.Get(); }
	CInputController& GetInput() { return myController.GetInput(); }
	const CInputController& GetInput() const { return myController.GetInput(); }
	CAIController& GetAI() { return myController.GetAI(); }
	const CAIController& GetAI() const { return myController.GetAI(); }
	CScriptController& GetScript() { return myController.GetScript(); }
	const CScriptController& GetScript() const { return myController.GetScript(); }

	CControllerWrapper myController;
};

struct CompParent
{
	CompParent() = default;
	CompParent(const CU::Vector3f& aOffSetPosition)
		: myPositionOffSet(aOffSetPosition)
	{
	}

	CU::Vector3f myPositionOffSet;
	SEntityHandle myParentHandle;
};

struct CompChildren
{
	CompChildren() = default;
	CompChildren(const SEntityHandle& aChildHandle/*const CU::GrowingArray<int>& aCildrenIndices*/)
		: myChildHandle(aChildHandle)
	{
	}
	const SEntityHandle myChildHandle;
	//CU::GrowingArray<int> myChildrenIndices;
};

struct CompTrigger
{
	CompTrigger() : myContainsPlayer(false) { }

	CU::Vector3f myMin;
	CU::Vector3f myMax;

	bool myContainsPlayer;
};

struct CompPlayerData
{
	UniqueIndexType myCurrentCheckpointIndex;
	float myHealAbilityCooldown;
};