#pragma once
#include "../../EntitySystem/ESSettingsLight.h"

struct TagPlayer {};
struct TagCompanion {};
struct TagEnemy {};
struct TagCamera {};
struct TagParticle {};
struct TagMenu {};
struct TagButton {};

struct TagInputController {};
struct TagAIController {};
struct TagScriptController {};
struct TagChild {};
struct TagPainting {};
struct TagBoss {};
struct TagProjectile {};
struct TagHealthPickup {};
struct TagCheckpoint {};

using MyTags = TagList 
<
	TagPlayer,
	TagCompanion,
	TagEnemy,
	TagCamera,
	TagParticle,
	TagInputController,
	TagAIController,
	TagScriptController,
	TagChild,
	TagPainting,
	TagBoss,
	TagProjectile,
	TagHealthPickup,
	TagCheckpoint
>;