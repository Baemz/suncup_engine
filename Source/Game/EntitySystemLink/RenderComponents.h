#pragma once
#include "CommonComponents.h"

#include "../../GraphicsEngine/Sprite/Sprite.h"
#include "../../GraphicsEngine/Model/ModelInstance.h"
#include "../../GraphicsEngine/Camera/CameraInstance.h"
#include "../../GraphicsEngine/Lights/PointLightInstance.h"
#include "../../GraphicsEngine/Lights/SpotLightInstance.h"
#include "../../GraphicsEngine/Particles/ParticleEmitterInstance.h"
#include "../../GraphicsEngine/Particles/StreakEmitterInstance.h"
#include "../GraphicsEngine/Lights/DirectionalLight.h"
#include "../GraphicsEngine/Particles/ParticleEngine/ParticleInterface.h"
#include "../GraphicsEngine/Sprite/Sprite3D.h"

struct CompModelInstance
{
	sce::gfx::CModelInstance myModel;
	sce::gfx::CModelInstance myLODModel;
	bool myIsCullable;

	CompModelInstance()
	{
		myIsCullable = false;
	}

	CompModelInstance(const std::string& aFilePath)
	{
		myModel.SetModelPath(aFilePath);
		myIsCullable = false;
	}
};

struct CompCameraInstance
{
	sce::gfx::CCameraInstance myInstance;
	bool myFreeCamera = true;
};

struct CompPointLight
{
	sce::gfx::CPointLightInstance myPointLight;
	CompPointLight() = default;
	CompPointLight(
		const CU::Vector3f& aPosition,
		const CU::Vector3f& aColor = CU::Vector3f(1.0f),
		float aRange = 100.0f, float aIntensity = 1.0f)
	{
		myPointLight.Init(aPosition, aColor, aRange, aIntensity);
	}
};

struct CompSpotLight
{
	sce::gfx::CSpotLightInstance mySpotLight;
	CompSpotLight() = default;
	CompSpotLight(
		const CU::Vector3f& aPosition,
		const CU::Vector3f& aColor = CU::Vector3f(1.0f),
		const CU::Vector3f& aDirection = CU::Vector3f(0.0f, 0.0f, -1.0f),
		float aAngle = 90.0f, float aRange = 100.0f, float aIntensity = 1.0f)
	{
		mySpotLight.Init(aPosition, aColor, aDirection, aAngle, aRange, aIntensity);
	}
};

struct CompDirectionalLight
{
	CompDirectionalLight() = default;
	sce::gfx::CDirectionalLight myDirLight;
};

struct CompParticleEmitter
{
	CompParticleEmitter() : myParticleID(sce::gfx::ParticleIDTypeINVALID)
	{
	};

	CompParticleEmitter(const char* aJsonPath, const CU::Vector3f& aBoxOffset)
		: myParticleID(sce::gfx::CParticleInterface::Get()->QueueCreateParticle(aJsonPath, aBoxOffset))
	{
	}

	~CompParticleEmitter()
	{
		sce::gfx::CParticleInterface::Get()->QueueDestroyParticle(myParticleID);
	}

	sce::gfx::ParticleIDType myParticleID;
};

struct CompStreakEmitter
{
	//sce::gfx::CStreakEmitterInstance myStreakEmitter;
};

struct CompHUDElements
{
	using IndexType = unsigned char;
	static constexpr IndexType invalid = static_cast<IndexType>(-1);

	CompHUDElements()
	{
		mySprites.SetRawData(nullptr, 0);
		myFreeIDs.SetRawData(nullptr, 0);
		myUsedIDs.SetRawData(nullptr, 0);
	}

	CompHUDElements(const IndexType& aAmountOfSprites)
	{
		auto dataSprites(sce_newArray(decltype(mySprites)::value_type, aAmountOfSprites));
		mySprites.SetRawData(dataSprites, aAmountOfSprites);

		auto dataFree(sce_newArray(decltype(myFreeIDs)::value_type, aAmountOfSprites));
		myFreeIDs.SetRawData(dataFree, aAmountOfSprites);
		
		auto dataUsed(sce_newArray(decltype(myUsedIDs)::value_type, aAmountOfSprites));
		myUsedIDs.SetRawDataReserve(dataUsed, aAmountOfSprites, 0);

		for (IndexType index(0); index < aAmountOfSprites; ++index)
		{
			myFreeIDs[index] = index;
		}
	}

	~CompHUDElements()
	{
		auto dataSprites(mySprites.GetRawData());
		sce_delete(dataSprites);
		auto dataFree(myFreeIDs.GetRawData());
		sce_delete(dataFree);
		auto dataUsed(myUsedIDs.GetRawData());
		sce_delete(dataUsed);

		mySprites.SetRawData(nullptr, 0);
		myFreeIDs.SetRawData(nullptr, 0);
		myUsedIDs.SetRawData(nullptr, 0);
	}

	IndexType Add(const char* aSpritePath)
	{
		IndexType freeID(myFreeIDs.GetLast());
		myFreeIDs.RemoveCyclicAtIndex(myFreeIDs.GetLastIndex());
		myUsedIDs.Add(freeID);
		mySprites[freeID].Init(aSpritePath);

		return freeID;
	}

	void Remove(const IndexType& aIndex)
	{
		IndexType foundIndex(myUsedIDs.Find(aIndex));
		if (foundIndex != myUsedIDs.FoundNone)
		{
			myUsedIDs.RemoveCyclicAtIndex(foundIndex);
			myFreeIDs.Add(aIndex);
		}
	}

	bool GetAllUsedSprites(CU::GrowingArray<sce::gfx::CSprite*, IndexType>& aArrayToFill)
	{
		for (const auto& index : myUsedIDs)
		{
			aArrayToFill.Add(&mySprites[index]);
		}

		return (aArrayToFill.Empty() == false);
	}

	sce::gfx::CSprite& Get(const IndexType& aIndex)
	{
		assert("Tried to get invalid sprite in HUD component!" && (myUsedIDs.Find(aIndex) != myUsedIDs.FoundNone));
		return mySprites[aIndex];
	}
	
	const sce::gfx::CSprite& Get(const IndexType& aIndex) const
	{
		assert("Tried to get invalid sprite in HUD component!" && (myUsedIDs.Find(aIndex) != myUsedIDs.FoundNone));
		return mySprites[aIndex];
	}

	CU::GrowingArray<sce::gfx::CSprite, IndexType> mySprites;
	CU::GrowingArray<IndexType, IndexType> myFreeIDs;
	CU::GrowingArray<IndexType, IndexType> myUsedIDs;
};
