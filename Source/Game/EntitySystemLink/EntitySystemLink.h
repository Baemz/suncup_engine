#pragma once
#include "../../EntitySystem/EntityManager.h"
#include "Components.h"
#include "Tags.h"
#include "Signatures.h"

using EntitySystemSettings = SESSettings<MyComponents, MyTags, MySignatures>;
using EManager = CEntityManager<EntitySystemSettings>;
