#pragma once
//#include "../AIEvents.h"

//#include "PathFinderWrapper.h"

// struct CompPathFinder
// {
// 	CompPathFinder() = default;
// 	CompPathFinder(const SControllerWrapperInit& aInitData)
// 		: myController(aInitData)
// 	{
// 	}
// 
// 	CController& Get() { return myController.Get(); }
// 	const CController& Get() const { return myController.Get(); }
// 	CInputController& GetInput() { return myController.GetInput(); }
// 	const CInputController& GetInput() const { return myController.GetInput(); }
// 	CAIController& GetAI() { return myController.GetAI(); }
// 	const CAIController& GetAI() const { return myController.GetAI(); }
// 	CScriptController& GetScript() { return myController.GetScript(); }
// 	const CScriptController& GetScript() const { return myController.GetScript(); }
// 
// 	CControllerWrapper myController;
// };

struct CompSeekBehavior
{
	CompSeekBehavior()
		: myWeight(0.0f)
	{
	}

	CompSeekBehavior(const float aWeight)
		: myWeight(aWeight)
	{
	}

	CompSeekBehavior(const CU::Vector3f& aPositionToSeekTo, const float aWeight)
		: myPositionToSeekTo(aPositionToSeekTo)
		, myWeight(aWeight)
	{
	}

	static constexpr float ArriveDistance = 0.1f;

	CU::Vector3f myPositionToSeekTo;
	float myWeight = 1.0f;
};

struct CompFleeBehavior
{
	CompFleeBehavior()
		: myFleeRadius(3.0f)
		, myWeight(1.0f)
	{
		myPositionsToFleeFrom.Init(16);
	}

	CompFleeBehavior(const float aWeight)
		: myFleeRadius(3.0f)
		, myWeight(aWeight)
	{
		myPositionsToFleeFrom.Init(16);
	}

	CU::GrowingArray<CU::Vector3f> myPositionsToFleeFrom;
	float myFleeRadius;
	float myWeight;
};

struct CompArriveBehavior
{
	CompArriveBehavior()
		: mySlowdownRadius(2.0f), myWeight(1.0f) {}

	CompArriveBehavior(const CU::Vector3f& aTarget, const float aSlowdownRadius, const float aWeight)
		: myPositionToArriveTo(aTarget)
		, mySlowdownRadius(aSlowdownRadius)
		, myWeight(aWeight) 
	{ }

	static constexpr float ArriveDistance = 0.1f;
	
	CU::Vector3f myPositionToArriveTo;
	const float mySlowdownRadius;
	float myWeight;
};

struct CompWanderBehavior
{
	CompWanderBehavior()
		: myWanderAngle(0.0f)
		, myWeight(1.0f)
	{
	}

	CompWanderBehavior(const float aWeight)
		: myWanderAngle(0.0f)
		, myWeight(aWeight)
	{
	}

	static constexpr float CircleOffset = 1.5f;
	static constexpr float CircleRadius = 7.0f;
	static constexpr float AngleChange = 0.5f;

	float myWanderAngle;
	float myWeight;

};

struct CompPursuitBehavior
{
	CompPursuitBehavior()
		: myWeight(1.0f)
	{
	}

	CompPursuitBehavior(const float aWeight)
		: myWeight(aWeight)
	{
	}

	CU::Vector3f myPositionToPursue;
	CU::Vector3f myPursuerVelocity;
	float myWeight;
};

struct CompEvadeBehavior
{
	CompEvadeBehavior()
		: myWeight(1.0f)
		, myEvadeRadius(3.0f)
	{
		myPositionsToEvade.Init(16);
		myEvaderVelocities.Init(16);
		myEvadersUniqueIndices.Init(16);
	}

	CompEvadeBehavior(const float aWeight)
		: myWeight(aWeight)
		, myEvadeRadius(1.0f)
	{
		myPositionsToEvade.Init(32);
		myEvaderVelocities.Init(32);
		myEvadersUniqueIndices.Init(32);
	}

	CU::GrowingArray<CU::Vector3f> myPositionsToEvade;
	CU::GrowingArray<CU::Vector3f> myEvaderVelocities;
	CU::GrowingArray<UniqueIndexType> myEvadersUniqueIndices;
	float myEvadeRadius;
	float myWeight;
};