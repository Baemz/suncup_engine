#pragma once
#include "State.h"
#include "Menu.h"

class COptionMenuState: private CState
{
public:
	COptionMenuState(EManager& aEntityManager);
	~COptionMenuState() {}

	void Init() override;
	void Update(const float aDeltaTime) override;
	void Render() override;

	void OnActivation() override;
	void OnDeactivation() override;

private:
	void UpdateFullscreenState();
	void UpdateWindowResolution();
	const unsigned short GetUsedResolutionIndex() const;

private:
	CMenu myMenu;
	unsigned int myPrevResolutionIndex;
	unsigned int myCurrResolutionIndex;
	bool myPrevFullScreenState;
};

