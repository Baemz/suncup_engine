#pragma once
#include "../EngineCore/EventSystem/EventReceiver.h"
#include "../GraphicsEngine/Sprite/Sprite.h"
#include "../CommonUtilities/GrowingArray.h"
#include "../GraphicsEngine/ShaderEffects/ShaderEffectInstance.h"
#include <functional>

class CMenu;

namespace MenuComponents
{
	struct SGraphic
	{
		SGraphic()
			: myName("")
			, myCanRender(true)
		{}

	private:
		friend class ::CMenu;

		sce::gfx::CSprite mySprite;
		std::string myName;
		bool myCanRender;
	};
	struct SButton
	{
		enum EButtonState
		{
			eHover,
			eNoHover,
			eOneSprite,
		};

		sce::gfx::CSprite myIdleSprite;
		sce::gfx::CSprite myHoverSprite;
		std::string myName;
		CU::Vector2f myCollisionSize;

	private:
		friend class ::CMenu;
		friend struct SSlider;

		EButtonState myState;
		bool myIsPressed;
		bool myIsHeld;
	};
	struct SCheckBox
	{
		enum ECheckBoxState
		{
			eHover,
			eNoHover,
			eHoverSelected,
			eSelected,
		};

	private:
		friend class ::CMenu;

		sce::gfx::CSprite myIdleSprite;
		sce::gfx::CSprite myHoverSprite;
		sce::gfx::CSprite mySelectedSprite;
		sce::gfx::CSprite myHoverSelectedSprite;
		std::string myName;
		CU::Vector2f myCollisionSize;
		ECheckBoxState myState;
		bool myIsPressed;
		bool myCanDeselect;
	};
	struct SNavigationButton
	{
		CU::GrowingArray<sce::gfx::CSprite, unsigned short> mySpriteList;
		SButton myPrevButton;
		SButton myNextButton;
		std::string myName;
		unsigned short myCurrIndex;
	};
	struct SSlider
	{
	private:
		friend class ::CMenu;

		SButton myButton;
		sce::gfx::CSprite myBackgroundSprite;
		sce::gfx::CSprite myForegroundSprite;
		std::string myName;
		CU::Vector2f myCollisionSize;
		float myMinX;
		float myMaxX;
		float myValue;
	};
};

class CMenu: public sce::CEventReceiver
{
private:
	enum class EMenuState
	{
		eExitMenu,
		eNothing,
	};

public:
	CMenu();
	~CMenu() override;

	void AddGraphic(const std::string& aTexturePath, const CU::Vector2f& aPosition,
		const CU::Vector2f& aScale = CU::Vector2f(1.0f),
		const CU::Vector4f& aColor = CU::Vector4f(1.0f), const std::string& aName = std::string(""));
	// aColliSize is relative to sprite. aPosition is relative to screen.
	void AddButton(const std::string& aTexturePath, const std::string& aName, const CU::Vector2f& aPosition,
		const CU::Vector2f& aColliSize, const CU::Vector2f& aScale = CU::Vector2f(1.0f));
	void AddButton(const std::string& aIdleTexPath, const std::string& aHoverTexPath, const std::string& aName,
		const CU::Vector2f& aPosition, const CU::Vector2f& aColliSize, const CU::Vector2f& aScale = CU::Vector2f(1.0f));
	void AddSlider(const std::string& aBackgroundTexPath, const std::string& aForegroundTexPath, const std::string& aName, const MenuComponents::SButton& aButton,
		const CU::Vector2f& aPosition, const CU::Vector3f& aColliSize, const CU::Vector2f& aScale = CU::Vector2f(1.0f));
	void AddCheckBox(const std::string& aIdleTexPath, const std::string& aHoverTexPath, const std::string& aSelectedTexPath, const std::string& aHoverSelectedTexPath,
		const std::string& aName, const CU::Vector2f& aPosition, const CU::Vector2f& aColliSize, const CU::Vector2f& aScale = CU::Vector2f(1.0f), const bool aCanDeselect = true);
	void AddNavigationButton(const CU::GrowingArray<std::string>& aSpritePathList, const std::string& aName, const MenuComponents::SButton& aPrevButton,
		const MenuComponents::SButton& aNextButton, const CU::Vector2f& aPosition, const unsigned short aStartingIndex, const CU::Vector2f& aScale = CU::Vector2f(1.0f));

	void SetButtonOnEnterSoundFunc(const std::function<void()>& aFunction);
	void SetSliderMoveSoundFunc(const std::function<void()>& aFunction);

	// Returns false if the menu should be destroyed.
	const bool Update(const float aDeltaTime);
	void Render(const bool aShouldRenderShader);
	const bool IsEnabled() const;
	void Enable();
	void Disable();

	const unsigned short GetNavigationButtonIndex(const std::string& aNavigationButtonName) const;

	void SetSliderValue(const std::string& aSliderName, float aValue);
	const float GetSliderValue(const std::string& aSliderName) const;

	void SetSliderMoveSoundPlayTime(const float aTime);
	void SetGraphicRenderState(const std::string& aGraphicName, const bool aCanRender);

	void SelectCheckBox(const std::string& aCheckBoxName);
	void DeselectCheckBox(const std::string& aCheckBoxName);
	void DeselectEveryCheckBox();
	const bool CheckHoveredCheckBoxOnEnter(const std::string& aCheckBoxName) const;
	const bool CheckHoveredCheckBoxOnLeave(const std::string& aCheckBoxName) const;
	const bool CheckHoveredCheckBox(const std::string& aCheckBoxName) const;
	const bool CheckSelectedCheckBox(const std::string& aCheckBoxName) const;
	const bool CheckPressedCheckBox(const std::string& aCheckBoxName) const;

	const bool CheckHoveredButtonOnEnter(const std::string& aButtonName) const;
	const bool CheckHoveredButtonOnLeave(const std::string& aButtonName) const;
	const bool CheckHoveredButton(const std::string& aButtonName) const;
	const bool CheckPressedButton(const std::string& aButtonName) const;
	const bool CheckHeldButton(const std::string& aButtonName) const;

	void ReceiveEvent(const Event::SEvent& aEvent) override;

private:
	void UpdateCurrentlyHeldSlider();
	void UpdateCurrentlyPressedNavigationButton();
	void SetSlidersValueAndSetButtonsPosition(MenuComponents::SSlider* aSliderPtr, float aFixedValue);
	void CheckEveryComponentForIntersection();
	void SwapAndResetEveryHoveredComponent();
	const bool CheckMouseVSButtonIntersection(sce::gfx::CSprite* aSpritePtr, MenuComponents::SButton& aButtonRef,
		const CU::Vector2f& aMousePosition, const CU::Vector2f& aWindowDiff, const CU::Vector2f& aWindowRes,
		CU::Vector2f& aPositionRef, CU::Vector2f& aSizeRef, CU::Vector2f& aCollisionSizeRef) const;

	void PlayHoverSoundOnHoverEnter();
	void PlaySliderSoundOnMove();

private:
	CU::GrowingArray<MenuComponents::SGraphic, unsigned short> myGraphicList;
	CU::GrowingArray<MenuComponents::SButton, unsigned short> myButtonList;
	CU::GrowingArray<MenuComponents::SSlider, unsigned short> mySliderList;
	CU::GrowingArray<MenuComponents::SCheckBox, unsigned short> myCheckBoxList;
	CU::GrowingArray<MenuComponents::SNavigationButton, unsigned short> myNavigationButtonList;
	MenuComponents::SButton* myPrevHoveredButtonPtr;
	MenuComponents::SButton* myCurrHoveredButtonPtr;
	MenuComponents::SSlider* myCurrHoveredSliderPtr;
	MenuComponents::SCheckBox* myPrevHoveredCheckBoxPtr;
	MenuComponents::SCheckBox* myCurrHoveredCheckBoxPtr;
	CU::Vector2f myMousePosition;
	EMenuState myMenuState;

	std::function<void()> myHoverPlayFunc;
	std::function<void()> mySliderPlayFunc;

	sce::gfx::CShaderEffectInstance myShaderEffect;

	float myCurrFixedSliderValue;
	float myPrevFixedSliderValue;
	float mySliderSoundPlayTimer;
	float mySliderSoundPlayMaxTime;
	bool myIsEnabled;
};