#include "stdafx.h"
#include "Menu.h"
#include "../GraphicsEngine/GraphicsEngineInterface.h"
#include <intsafe.h>

typedef MenuComponents::SButton::EButtonState B_State;
typedef MenuComponents::SCheckBox::ECheckBoxState CB_State;

#define sc_f(var) static_cast<float>(var)
#define sc_us(var) static_cast<unsigned short>(var)

#define RENDER_BUTTON(aButton)												\
if ((aButton).myState == B_State::eHover) (aButton).myHoverSprite.Render();	\
else (aButton).myIdleSprite.Render();
#define RENDER_CHECKBOX(aCheckBox)																			\
if ((aCheckBox).myState == CB_State::eHoverSelected) (aCheckBox).myHoverSelectedSprite.Render();			\
else if ((aCheckBox).myState == CB_State::eHover) (aCheckBox).myHoverSprite.Render();						\
else if ((aCheckBox).myState == CB_State::eSelected) (aCheckBox).mySelectedSprite.Render();					\
else (aCheckBox).myIdleSprite.Render();

#define GET_RESETED_BUTTON_STATE(aButtonPtr) (((aButtonPtr)->myState == B_State::eOneSprite) ? (B_State::eOneSprite) : (B_State::eNoHover))
#define GET_HOVERED_BUTTON_STATE(aButtonPtr) (((aButtonPtr)->myState == B_State::eOneSprite) ? (B_State::eOneSprite) : (B_State::eHover))
#define GET_ACTIVE_BUTTON_SPRITE(aButton) (((aButton).myState == B_State::eHover) ? ((aButton).myHoverSprite) : ((aButton).myIdleSprite))

#define GET_RESETED_CHECKBOX_HOVER(aCheckBoxPtr) (((aCheckBoxPtr)->myState == CB_State::eHoverSelected) ? (CB_State::eSelected) : (CB_State::eNoHover))
#define GET_RESETED_CHECKBOX_SELECT(aCheckBoxPtr) (((aCheckBoxPtr)->myState == CB_State::eHoverSelected) ? (CB_State::eHover) : (CB_State::eNoHover))
#define GET_HOVERED_CHECKBOX_STATE(aCheckBoxPtr) (((aCheckBoxPtr)->myState == CB_State::eSelected) ? (CB_State::eHoverSelected) : (CB_State::eHover))
#define GET_CHECKBOX_SELECTED(aCheckBoxPtr) (((aCheckBoxPtr)->myState == CB_State::eHover) ? (CB_State::eHoverSelected) : (CB_State::eSelected))

#define GET_ACTIVE_CHECKBOX_SPRITE(aCheckBox)																				\
(((aCheckBox).myState == CB_State::eHover)				? ((aCheckBox).myHoverSprite) :										\
(((aCheckBox).myState == CB_State::eHoverSelected)		? ((aCheckBox).myHoverSelectedSprite) :								\
(((aCheckBox).myState == CB_State::eSelected)			? ((aCheckBox).mySelectedSprite) : ((aCheckBox).myIdleSprite))))

#define SLIDER_MIN_VALUE_TO_SNAP 0.03f
#define SLIDER_MAX_VALUE_TO_SNAP 0.99f

#define GET_WINDOW_SIZE (sce::gfx::CGraphicsEngineInterface::GetResolution())
#define WINDOW_DIFFERENCE CU::Vector2f(																						\
sce::gfx::CGraphicsEngineInterface::GetResolution().x / sce::gfx::CGraphicsEngineInterface::GetTargetResolution().x,		\
sce::gfx::CGraphicsEngineInterface::GetResolution().y / sce::gfx::CGraphicsEngineInterface::GetTargetResolution().y)


CMenu::CMenu()
{
	myGraphicList.Init(8);
	myButtonList.Init(8);
	mySliderList.Init(2);
	myCheckBoxList.Init(8);
	myNavigationButtonList.Init(2);
	myCurrHoveredSliderPtr = nullptr;
	myPrevHoveredButtonPtr = nullptr;
	myCurrHoveredButtonPtr = nullptr;
	myPrevHoveredCheckBoxPtr = nullptr;
	myCurrHoveredCheckBoxPtr = nullptr;
	myMenuState = EMenuState::eNothing;
	myCurrFixedSliderValue = 0.0f;
	myPrevFixedSliderValue = 0.0f;
	mySliderSoundPlayTimer = 0.0f;
	mySliderSoundPlayMaxTime = -1.0f;
	myIsEnabled = false;

	AttachToEventReceiving(Event::ESubscribedEvents(Event::eKeyInput | Event::eMouseInput | Event::eMouseClickInput));

	SGlitchScreenData data;
	//20, -20000 -> VHS Lines
	data.myResolution = { 20.0f, -23465536.0f };
	data.myBlockThresholdMultiplier = 0.5f;
	data.myLineThresholdMultiplier = 0.5f;
	data.myUVNoiseDistance = 0.1f;
	data.myNoiseAbberationAlpha = 0.055f;

	myShaderEffect.InitGlitchScreen(data);
}

CMenu::~CMenu()
{
	myCurrHoveredCheckBoxPtr = nullptr;
	myPrevHoveredCheckBoxPtr = nullptr;
	myCurrHoveredSliderPtr = nullptr;
	myPrevHoveredButtonPtr = nullptr;
	myCurrHoveredButtonPtr = nullptr;
	myNavigationButtonList.RemoveAll();
	myCheckBoxList.RemoveAll();
	mySliderList.RemoveAll();
	myButtonList.RemoveAll();
	myGraphicList.RemoveAll();

	DetachFromEventReceiving();
	// HACK ! Is out commented because we have no in-game cursor.
	//sce::gfx::CGraphicsEngineInterface::DisableCursor();
}

void CMenu::AddGraphic(const std::string& aTexturePath, const CU::Vector2f& aPosition, const CU::Vector2f& aScale, const CU::Vector4f& aColor, const std::string& aName)
{
	MenuComponents::SGraphic newGraphic;

	newGraphic.myName = aName;
	newGraphic.mySprite.Init(aTexturePath.c_str());
	newGraphic.mySprite.SetScale(aScale);
	newGraphic.mySprite.SetPosition(aPosition);
	newGraphic.mySprite.SetColor({ aColor.x, aColor.y, aColor.z });
	newGraphic.mySprite.SetAlpha(aColor.w);

	myGraphicList.Add(newGraphic);
}

void CMenu::AddButton(const std::string& aTexturePath, const std::string& aName, const CU::Vector2f& aPosition,
	const CU::Vector2f& aColliSize, const CU::Vector2f& aScale)
{
	AddButton(aTexturePath, "", aName, aPosition, aColliSize, aScale);
}

void CMenu::AddButton(const std::string& aIdleTexPath, const std::string& aHoverTexPath, const std::string& aName,
	const CU::Vector2f& aPosition, const CU::Vector2f& aColliSize, const CU::Vector2f& aScale)
{
	MenuComponents::SButton newButton;

	newButton.myName = aName;
	newButton.myIsPressed = false;
	newButton.myIsHeld = false;
	newButton.myCollisionSize = aColliSize;
	newButton.myIdleSprite.Init(aIdleTexPath.c_str());
	newButton.myIdleSprite.SetPosition(aPosition);
	newButton.myIdleSprite.SetScale(aScale);

	if (aHoverTexPath != "")
	{
		newButton.myHoverSprite.Init(aHoverTexPath.c_str());
		newButton.myHoverSprite.SetPosition(aPosition);
		newButton.myHoverSprite.SetScale(aScale);
		newButton.myState = B_State::eNoHover;
	}
	else
	{
		newButton.myState = B_State::eOneSprite;
	}

	myButtonList.Add(newButton);
}

void CMenu::AddSlider(const std::string& aBackgroundTexPath, const std::string& aForegroundTexPath, const std::string& aName, const MenuComponents::SButton& aButton,
	const CU::Vector2f& aPosition, const CU::Vector3f& aColliSize, const CU::Vector2f& aScale)
{
	MenuComponents::SSlider newSlider;

	newSlider.myBackgroundSprite.Init(aBackgroundTexPath.c_str());
	newSlider.myBackgroundSprite.SetPosition(aPosition);
	newSlider.myBackgroundSprite.SetScale(aScale);

	newSlider.myForegroundSprite.Init(aForegroundTexPath.c_str());
	newSlider.myForegroundSprite.SetPosition(aPosition);
	newSlider.myForegroundSprite.SetScale(aScale);

	float sizeX((newSlider.myBackgroundSprite.GetOriginalSize().x * aScale.x));
	sizeX *= WINDOW_DIFFERENCE.x;
	float maxSizeXRelativeToScreen = 0.0f;//((sizeX - (sizeX * aColliSize.x)) / GET_WINDOW_SIZE.x) / 2.0f;
	CU::Vector2f newPosition(aPosition);
	newPosition.x += aColliSize.z;

	newSlider.myCollisionSize =			{ aColliSize.x, aColliSize.y };
	newSlider.myMinX =					newPosition.x + maxSizeXRelativeToScreen;
	newSlider.myMaxX =					newSlider.myMinX + (sizeX / GET_WINDOW_SIZE.x) * 0.5f - maxSizeXRelativeToScreen * 2.0f;

	newSlider.myName =					aName;
	newSlider.myValue =					1.0f;
	newSlider.myButton =				aButton;
	newSlider.myButton.myIsPressed =	false;
	newSlider.myButton.myIsHeld =		false;
	
	if (newSlider.myButton.myHoverSprite.IsInitialized())
	{
		sizeX = newSlider.myButton.myHoverSprite.GetOriginalSize().x * newSlider.myButton.myHoverSprite.GetScale().x;
		sizeX /= 2.0f;
		sizeX *= WINDOW_DIFFERENCE.x;
		sizeX /= GET_WINDOW_SIZE.x;

		newSlider.myButton.myState = B_State::eNoHover;
		newSlider.myButton.myHoverSprite.SetPosition({ newSlider.myMaxX - sizeX, aPosition.y });
	}
	else
	{
		newSlider.myButton.myState = B_State::eOneSprite;
	}

	sizeX = newSlider.myButton.myIdleSprite.GetOriginalSize().x * newSlider.myButton.myIdleSprite.GetScale().x;
	sizeX /= 2.0f;
	sizeX *= WINDOW_DIFFERENCE.x;
	sizeX /= GET_WINDOW_SIZE.x;

	newSlider.myButton.myIdleSprite.SetPosition({ newSlider.myMaxX - sizeX, aPosition.y });

	mySliderList.Add(newSlider);
}

void CMenu::AddCheckBox(const std::string& aIdleTexPath, const std::string& aHoverTexPath, const std::string& aSelectedTexPath, const std::string& aHoverSelectedTexPath,
	const std::string& aName, const CU::Vector2f& aPosition, const CU::Vector2f& aColliSize, const CU::Vector2f& aScale, const bool aCanDeselect)
{
	MenuComponents::SCheckBox newCheckBox;

	newCheckBox.myName = aName;
	newCheckBox.myCollisionSize = aColliSize;
	newCheckBox.myState = CB_State::eNoHover;
	newCheckBox.myIsPressed = false;
	newCheckBox.myCanDeselect = aCanDeselect;

	newCheckBox.myIdleSprite.Init(aIdleTexPath.c_str());
	newCheckBox.myIdleSprite.SetPosition(aPosition);
	newCheckBox.myIdleSprite.SetScale(aScale);

	newCheckBox.myHoverSprite.Init(aHoverTexPath.c_str());
	newCheckBox.myHoverSprite.SetPosition(aPosition);
	newCheckBox.myHoverSprite.SetScale(aScale);

	newCheckBox.mySelectedSprite.Init(aSelectedTexPath.c_str());
	newCheckBox.mySelectedSprite.SetPosition(aPosition);
	newCheckBox.mySelectedSprite.SetScale(aScale);

	newCheckBox.myHoverSelectedSprite.Init(aHoverSelectedTexPath.c_str());
	newCheckBox.myHoverSelectedSprite.SetPosition(aPosition);
	newCheckBox.myHoverSelectedSprite.SetScale(aScale);

	myCheckBoxList.Add(newCheckBox);
}

void CMenu::AddNavigationButton(const CU::GrowingArray<std::string>& aSpritePathList, const std::string& aName,
	const MenuComponents::SButton& aPrevButton, const MenuComponents::SButton& aNextButton, const CU::Vector2f& aPosition,
	const unsigned short aStartingIndex, const CU::Vector2f& aScale)
{
	MenuComponents::SNavigationButton newNavigationButton;
	newNavigationButton.myPrevButton = aPrevButton;
	newNavigationButton.myNextButton = aNextButton;
	newNavigationButton.myName = aName;
	newNavigationButton.mySpriteList.Init(sc_us(aSpritePathList.Size()));

	CU::Vector2f prevButtonSize(aPrevButton.myIdleSprite.GetOriginalSize() * aScale);
	prevButtonSize.x *= WINDOW_DIFFERENCE.x;
	prevButtonSize.y *= WINDOW_DIFFERENCE.y;
	prevButtonSize.x /= GET_WINDOW_SIZE.x;
	prevButtonSize.y /= GET_WINDOW_SIZE.y;
	CU::Vector2f middleSpritePosition(aPosition);
	//middleSpritePosition.x += prevButtonSize.x * 0.25f;

	CU::Vector2f middleSpriteLargestXSize;
	sce::gfx::CSprite* currSpritePtr(nullptr);
	for (unsigned short i(0); i < sc_us(aSpritePathList.Size()); ++i)
	{
		newNavigationButton.mySpriteList.Add(sce::gfx::CSprite());
		currSpritePtr = &newNavigationButton.mySpriteList[i];

		currSpritePtr->Init(aSpritePathList[i].c_str());
		currSpritePtr->SetPosition(middleSpritePosition);
		currSpritePtr->SetScale(aScale);

		if (currSpritePtr->GetOriginalSize().x > middleSpriteLargestXSize.x)
		{
			middleSpriteLargestXSize = currSpritePtr->GetOriginalSize();
		}
	}
	currSpritePtr = nullptr;

	if (aSpritePathList.Size() > 0)
	{
		middleSpriteLargestXSize = middleSpriteLargestXSize * aScale;
		middleSpriteLargestXSize.x *= WINDOW_DIFFERENCE.x;
		middleSpriteLargestXSize.y *= WINDOW_DIFFERENCE.y;
		middleSpriteLargestXSize.x /= GET_WINDOW_SIZE.x;
		middleSpriteLargestXSize.y /= GET_WINDOW_SIZE.y;
	}

	CU::Vector2f nextButtonPosition(middleSpritePosition);
	nextButtonPosition.x += middleSpriteLargestXSize.x * 0.88f;

	newNavigationButton.myPrevButton.myIdleSprite.SetPosition(aPosition);
	newNavigationButton.myPrevButton.myHoverSprite.SetPosition(aPosition);
	newNavigationButton.myNextButton.myIdleSprite.SetPosition(nextButtonPosition);
	newNavigationButton.myNextButton.myHoverSprite.SetPosition(nextButtonPosition);

	newNavigationButton.myPrevButton.myIdleSprite.SetScale(aScale);
	newNavigationButton.myPrevButton.myHoverSprite.SetScale(aScale);
	newNavigationButton.myNextButton.myIdleSprite.SetScale(aScale);
	newNavigationButton.myNextButton.myHoverSprite.SetScale(aScale);

	newNavigationButton.myPrevButton.myState = B_State::eNoHover;
	newNavigationButton.myPrevButton.myIsPressed = false;
	newNavigationButton.myPrevButton.myIsHeld = false;

	newNavigationButton.myNextButton.myState = B_State::eNoHover;
	newNavigationButton.myNextButton.myIsPressed = false;
	newNavigationButton.myNextButton.myIsHeld = false;

	newNavigationButton.myCurrIndex = (aStartingIndex < sc_us(aSpritePathList.Size())) ? aStartingIndex : sc_us(aSpritePathList.Size());

	myNavigationButtonList.Add(newNavigationButton);
}

void CMenu::SetButtonOnEnterSoundFunc(const std::function<void()>& aFunction)
{
	myHoverPlayFunc = aFunction;
}

void CMenu::SetSliderMoveSoundFunc(const std::function<void()>& aFunction)
{
	mySliderPlayFunc = aFunction;
}

const bool CMenu::Update(const float aDeltaTime)
{
	if (!myIsEnabled) Enable();

	if (mySliderSoundPlayMaxTime >= 0.0f)
	{
		mySliderSoundPlayTimer += aDeltaTime;
	}

	PlayHoverSoundOnHoverEnter();

	if (myCurrHoveredSliderPtr != nullptr)
	{
		if (myCurrHoveredSliderPtr->myButton.myIsHeld)
		{
			UpdateCurrentlyHeldSlider();
		}
	}
	else if (myCurrHoveredButtonPtr != nullptr)
	{
		if (myCurrHoveredButtonPtr->myIsPressed)
		{
			UpdateCurrentlyPressedNavigationButton();
		}
	}

	SGlitchScreenData& data = myShaderEffect.GetGlitchScreenData();
	data.myElapsedTime += (data.myElapsedTime > 15.0f) ? -data.myElapsedTime : aDeltaTime * 5.0f;

	return myMenuState == EMenuState::eNothing;
}

void CMenu::Render(const bool aShouldRenderShader)
{
	for (unsigned short i(0); i < myGraphicList.Size(); ++i)
	{
		MenuComponents::SGraphic& graphic_ref(myGraphicList[i]);

		if (graphic_ref.myCanRender)
		{
			graphic_ref.mySprite.Render();
		}
	}

	// If menu is not enabled then the user will not see the buttons, sliders, navigation buttons or the checkboxes.
	if (!myIsEnabled) return;

	for (unsigned short i(0); i < myButtonList.Size(); ++i)
	{
		RENDER_BUTTON(myButtonList[i]);
	}

	for (unsigned short i(0); i < mySliderList.Size(); ++i)
	{
		mySliderList[i].myBackgroundSprite.Render();
		mySliderList[i].myForegroundSprite.Render();
		RENDER_BUTTON(mySliderList[i].myButton);
	}

	for (unsigned short i(0); i < myCheckBoxList.Size(); ++i)
	{
		RENDER_CHECKBOX(myCheckBoxList[i]);
	}

	for (unsigned short i(0); i < myNavigationButtonList.Size(); ++i)
	{
		RENDER_BUTTON(myNavigationButtonList[i].myPrevButton);
		RENDER_BUTTON(myNavigationButtonList[i].myNextButton);

		myNavigationButtonList[i].mySpriteList[myNavigationButtonList[i].myCurrIndex].Render();
	}

	if (aShouldRenderShader)
	{
		myShaderEffect.Render();
	}
}

const bool CMenu::IsEnabled() const
{
	return myIsEnabled;
}

void CMenu::Enable()
{
	myIsEnabled = true;
	sce::gfx::CGraphicsEngineInterface::EnableCursor();
}

void CMenu::Disable()
{
	myIsEnabled = false;
	// HACK ! Is out commented because we have no in-game cursor.
	//sce::gfx::CGraphicsEngineInterface::DisableCursor();

	SwapAndResetEveryHoveredComponent();
	myPrevHoveredButtonPtr = nullptr;
}

const unsigned short CMenu::GetNavigationButtonIndex(const std::string& aNavigationButtonName) const
{
	for (unsigned short i(0); i < myNavigationButtonList.Size(); ++i)
	{
		if (myNavigationButtonList[i].myName == aNavigationButtonName)
		{
			return myNavigationButtonList[i].myCurrIndex;
		}
	}

	return USHORT_MAX;
}

void CMenu::SetSliderValue(const std::string& aSliderName, float aValue)
{
	for (unsigned short i(0); i < mySliderList.Size(); ++i)
	{
		if (mySliderList[i].myName == aSliderName)
		{
			if (aValue < 0.0f || aValue > 1.0f)
			{
				GAME_LOG("[CMenu] WARNING! Sent value to slider \"%s\" is not in the range (sent value: %f). Continuing with clamping the value.", mySliderList[i].myName.c_str(), aValue);
				aValue = (aValue < 0.0f) ? (0.0f) : (1.0f);
			}

			SetSlidersValueAndSetButtonsPosition(&mySliderList[i], aValue);
			return;
		}
	}
}

const float CMenu::GetSliderValue(const std::string& aSliderName) const
{
	for (unsigned short i(0); i < mySliderList.Size(); ++i)
	{
		if (mySliderList[i].myName == aSliderName)
		{
			return mySliderList[i].myValue;
		}
	}

	return 0.0f;
}

void CMenu::SetSliderMoveSoundPlayTime(const float aTime)
{
	mySliderSoundPlayMaxTime = aTime;
}

void CMenu::SetGraphicRenderState(const std::string& aGraphicName, const bool aCanRender)
{
	for (unsigned short i(0); i < myGraphicList.Size(); ++i)
	{
		MenuComponents::SGraphic& graphic_ref(myGraphicList[i]);

		if (graphic_ref.myName == aGraphicName)
		{
			graphic_ref.myCanRender = aCanRender;
		}
	}
}

void CMenu::SelectCheckBox(const std::string& aCheckBoxName)
{
	for (unsigned short i(0); i < myCheckBoxList.Size(); ++i)
	{
		if (myCheckBoxList[i].myName == aCheckBoxName)
		{
			myCheckBoxList[i].myState = GET_CHECKBOX_SELECTED(&myCheckBoxList[i]);
			return;
		}
	}
}

void CMenu::DeselectCheckBox(const std::string& aCheckBoxName)
{
	for (unsigned short i(0); i < myCheckBoxList.Size(); ++i)
	{
		if (myCheckBoxList[i].myName == aCheckBoxName)
		{
			myCheckBoxList[i].myState = GET_RESETED_CHECKBOX_SELECT(&myCheckBoxList[i]);
			return;
		}
	}
}

void CMenu::DeselectEveryCheckBox()
{
	for (unsigned short i(0); i < myCheckBoxList.Size(); ++i)
	{
		myCheckBoxList[i].myState = GET_RESETED_CHECKBOX_SELECT(&myCheckBoxList[i]);
	}
}

const bool CMenu::CheckHoveredCheckBoxOnEnter(const std::string& aCheckBoxName) const
{
	if (myCurrHoveredCheckBoxPtr == nullptr) return false;

	if (myPrevHoveredCheckBoxPtr != nullptr)
	{
		if (myPrevHoveredCheckBoxPtr->myName != myCurrHoveredCheckBoxPtr->myName)
		{
			if (myCurrHoveredCheckBoxPtr->myName == aCheckBoxName) return true;
		}

		return false;
	}

	return (myCurrHoveredCheckBoxPtr->myName == aCheckBoxName);
}

const bool CMenu::CheckHoveredCheckBoxOnLeave(const std::string& aCheckBoxName) const
{
	if (myPrevHoveredCheckBoxPtr == nullptr) return false;

	if (myCurrHoveredCheckBoxPtr != nullptr)
	{
		if (myCurrHoveredCheckBoxPtr->myName != myPrevHoveredCheckBoxPtr->myName)
		{
			if (myPrevHoveredCheckBoxPtr->myName == aCheckBoxName) return true;
		}

		return false;
	}

	return (myPrevHoveredCheckBoxPtr->myName == aCheckBoxName);
}

const bool CMenu::CheckHoveredCheckBox(const std::string& aCheckBoxName) const
{
	if (myCurrHoveredCheckBoxPtr == nullptr) return false;

	return (myCurrHoveredCheckBoxPtr->myName == aCheckBoxName);
}

const bool CMenu::CheckSelectedCheckBox(const std::string& aCheckBoxName) const
{
	if (myCurrHoveredCheckBoxPtr == nullptr) return false;

	return (myCurrHoveredCheckBoxPtr->myName == aCheckBoxName &&
		(myCurrHoveredCheckBoxPtr->myState == CB_State::eSelected || myCurrHoveredCheckBoxPtr->myState == CB_State::eHoverSelected));
}

const bool CMenu::CheckPressedCheckBox(const std::string& aCheckBoxName) const
{
	if (myCurrHoveredCheckBoxPtr == nullptr) return false;

	return (myCurrHoveredCheckBoxPtr->myName == aCheckBoxName && myCurrHoveredCheckBoxPtr->myIsPressed);
}

const bool CMenu::CheckHoveredButtonOnEnter(const std::string& aButtonName) const
{
	if (myCurrHoveredButtonPtr == nullptr) return false;

	if (myPrevHoveredButtonPtr != nullptr)
	{
		if (myPrevHoveredButtonPtr->myName != myCurrHoveredButtonPtr->myName)
		{
			if (myCurrHoveredButtonPtr->myName == aButtonName) return true;
		}

		return false;
	}

	return (myCurrHoveredButtonPtr->myName == aButtonName);
}

const bool CMenu::CheckHoveredButtonOnLeave(const std::string& aButtonName) const
{
	if (myPrevHoveredButtonPtr == nullptr) return false;

	if (myCurrHoveredButtonPtr != nullptr)
	{
		if (myCurrHoveredButtonPtr->myName != myPrevHoveredButtonPtr->myName)
		{
			if (myPrevHoveredButtonPtr->myName == aButtonName) return true;
		}

		return false;
	}

	return (myPrevHoveredButtonPtr->myName == aButtonName);
}

const bool CMenu::CheckHoveredButton(const std::string& aButtonName) const
{
	if (myCurrHoveredButtonPtr == nullptr) return false;

	return (myCurrHoveredButtonPtr->myName == aButtonName);
}

const bool CMenu::CheckPressedButton(const std::string& aButtonName) const
{
	if (myCurrHoveredButtonPtr == nullptr) return false;

	return (myCurrHoveredButtonPtr->myName == aButtonName && myCurrHoveredButtonPtr->myIsPressed);
}

const bool CMenu::CheckHeldButton(const std::string& aButtonName) const
{
	if (myCurrHoveredButtonPtr == nullptr) return false;

	return (myCurrHoveredButtonPtr->myName == aButtonName && myCurrHoveredButtonPtr->myIsHeld);
}

void CMenu::ReceiveEvent(const Event::SEvent& aEvent)
{
	if (!myIsEnabled) return;

	switch (aEvent.myType)
	{
	case Event::EEventType::eKeyInput:
	{
		const Event::SKeyEvent* keyMsgPtr = reinterpret_cast<const Event::SKeyEvent*>(&aEvent);

		if (keyMsgPtr->key == CU::CKeyBinds::GetHotKeys().myPause && keyMsgPtr->state == EButtonState::Pressed)
		{
			myMenuState = EMenuState::eExitMenu;
		}
		break;
	}
	case Event::EEventType::eMouseInput:
	{
		const Event::SMouseDataEvent* mouseMsgPtr = reinterpret_cast<const Event::SMouseDataEvent*>(&aEvent);
		myMousePosition.x = sc_f(mouseMsgPtr->x);
		myMousePosition.y = sc_f(mouseMsgPtr->y);

		CheckEveryComponentForIntersection();
		break;
	}
	case Event::EEventType::eMouseClickInput:
	{
		if (myCurrHoveredButtonPtr == nullptr && myCurrHoveredCheckBoxPtr == nullptr) break;

		const Event::SMouseClickEvent* mouseMsgPtr = reinterpret_cast<const Event::SMouseClickEvent*>(&aEvent);

		if (mouseMsgPtr->key == 1 && mouseMsgPtr->state == EButtonState::Pressed)
		{
			if (myCurrHoveredButtonPtr != nullptr) myCurrHoveredButtonPtr->myIsPressed = true;
			else if (myCurrHoveredCheckBoxPtr != nullptr)
			{
				if (!myCurrHoveredCheckBoxPtr->myCanDeselect && myCurrHoveredCheckBoxPtr->myState != CB_State::eHoverSelected)
				{
					myCurrHoveredCheckBoxPtr->myIsPressed = true;
					myCurrHoveredCheckBoxPtr->myState = CB_State::eHoverSelected;
				}
				else if (myCurrHoveredCheckBoxPtr->myCanDeselect)
				{
					myCurrHoveredCheckBoxPtr->myIsPressed = true;
					if (myCurrHoveredCheckBoxPtr->myState != CB_State::eHoverSelected) myCurrHoveredCheckBoxPtr->myState = CB_State::eHoverSelected;
					else if (myCurrHoveredCheckBoxPtr->myState == CB_State::eHoverSelected) myCurrHoveredCheckBoxPtr->myState = CB_State::eHover;
				}
			}
		}
		else if (mouseMsgPtr->key == 1 && mouseMsgPtr->state == EButtonState::Down)
		{
			if (myCurrHoveredButtonPtr != nullptr) myCurrHoveredButtonPtr->myIsHeld = true;
		}
	}
	default: // Do nothing.
		break;
	}
}

/* PRIVATE FUNCTIONS */

void CMenu::UpdateCurrentlyHeldSlider()
{
	const float mouseX( myMousePosition.x / GET_WINDOW_SIZE.x );
	if (mouseX < myCurrHoveredSliderPtr->myMinX || mouseX > myCurrHoveredSliderPtr->myMaxX) return;

	myPrevFixedSliderValue = myCurrFixedSliderValue;
	myCurrFixedSliderValue = (mouseX - myCurrHoveredSliderPtr->myMinX) / (myCurrHoveredSliderPtr->myMaxX - myCurrHoveredSliderPtr->myMinX);

	PlaySliderSoundOnMove();
	SetSlidersValueAndSetButtonsPosition(myCurrHoveredSliderPtr, myCurrFixedSliderValue);
}

void CMenu::UpdateCurrentlyPressedNavigationButton()
{
	for (unsigned short i(0); i < myNavigationButtonList.Size(); ++i)
	{
		if (myCurrHoveredButtonPtr->myName == myNavigationButtonList[i].myPrevButton.myName)
		{
			if (myNavigationButtonList[i].myCurrIndex > 0) --myNavigationButtonList[i].myCurrIndex;
		}
		else if (myCurrHoveredButtonPtr->myName == myNavigationButtonList[i].myNextButton.myName)
		{
			if (myNavigationButtonList[i].myCurrIndex < myNavigationButtonList[i].mySpriteList.Size() - 1) ++myNavigationButtonList[i].myCurrIndex;
		}
	}
}

void CMenu::SetSlidersValueAndSetButtonsPosition(MenuComponents::SSlider* aSliderPtr, float aFixedValue)
{
	float side(-1.0f);

	if (aFixedValue < SLIDER_MIN_VALUE_TO_SNAP || aFixedValue > SLIDER_MAX_VALUE_TO_SNAP)
	{
		aFixedValue = (aFixedValue < SLIDER_MIN_VALUE_TO_SNAP) ? (0.0f) : (1.0f);
		//side = (aFixedValue < SLIDER_MIN_VALUE_TO_SNAP) ? (-1.0f) : (-1.0f);
	}
	aSliderPtr->myValue = aFixedValue;

	float buttonSizeX( aSliderPtr->myButton.myIdleSprite.GetOriginalSize().x * aSliderPtr->myButton.myIdleSprite.GetScale().x );
	buttonSizeX /= 2.0f;
	buttonSizeX *= WINDOW_DIFFERENCE.x;
	buttonSizeX /= GET_WINDOW_SIZE.x;
	buttonSizeX *= side;
	const float xToAdd( (aSliderPtr->myMaxX - aSliderPtr->myMinX) * aFixedValue );

	aSliderPtr->myButton.myIdleSprite.SetPosition({
		aSliderPtr->myMinX + xToAdd + buttonSizeX,
		aSliderPtr->myButton.myIdleSprite.GetPosition().y
	});

	if (aSliderPtr->myButton.myHoverSprite.IsInitialized())
	{
		buttonSizeX = aSliderPtr->myButton.myHoverSprite.GetOriginalSize().x * aSliderPtr->myButton.myHoverSprite.GetScale().x;
		buttonSizeX /= 2.0f;
		buttonSizeX *= WINDOW_DIFFERENCE.x;
		buttonSizeX /= GET_WINDOW_SIZE.x;
		buttonSizeX *= side;

		aSliderPtr->myButton.myHoverSprite.SetPosition({
			aSliderPtr->myMinX + xToAdd + buttonSizeX,
			aSliderPtr->myButton.myHoverSprite.GetPosition().y
		});
	}
}

void CMenu::CheckEveryComponentForIntersection()
{
	if (myButtonList.Size() <= 0 && mySliderList.Size() <= 0 && myCheckBoxList.Size() <= 0 && myNavigationButtonList.Size() <= 0) return;

	SwapAndResetEveryHoveredComponent();
	sce::gfx::CSprite* spritePtr(nullptr);
	const CU::Vector2f windowRes(GET_WINDOW_SIZE);
	const CU::Vector2f windowDiff(WINDOW_DIFFERENCE);
	CU::Vector2f position;
	CU::Vector2f colliSize;
	CU::Vector2f size;
	CU::Vector2f mousePos = myMousePosition;
	mousePos.x /= windowRes.x;
	mousePos.y /= windowRes.y;

	for (unsigned short i(0); i < myButtonList.Size(); ++i)
	{
		if (!CheckMouseVSButtonIntersection(spritePtr, myButtonList[i], mousePos, windowDiff, windowRes, position, size, colliSize)) continue;

		myCurrHoveredButtonPtr = &myButtonList[i];
		myCurrHoveredButtonPtr->myState = GET_HOVERED_BUTTON_STATE(myCurrHoveredButtonPtr);

		//if (myPrevHoveredButtonPtr != nullptr)
		//{
		//	if (myPrevHoveredButtonPtr->myName != myCurrHoveredButtonPtr->myName)
		//	{
		//		AI::PlayAudio(myButtonHoverSoundID);
		//	}
		//}
		//else AI::PlayAudio(myButtonHoverSoundID);

		return;
	}

	for (unsigned short i(0); i < mySliderList.Size(); ++i)
	{
		spritePtr = &mySliderList[i].myBackgroundSprite;

		size.y = spritePtr->GetOriginalSize().y * spritePtr->GetScale().y;
		colliSize.x = mySliderList[i].myMaxX - mySliderList[i].myMinX;
		colliSize.y = (mySliderList[i].myCollisionSize.y * size.y);
		position.x = mySliderList[i].myMinX;
		position.y = spritePtr->GetPosition().y;

		colliSize.y *= windowDiff.y;
		colliSize.y /= windowRes.y;

		if (position.x > mousePos.x) continue;
		if (position.y > mousePos.y) continue;
		if (position.x + colliSize.x < mousePos.x) continue;
		if (position.y + colliSize.y < mousePos.y) continue;

		myCurrHoveredSliderPtr = &mySliderList[i];
		myCurrHoveredButtonPtr = &mySliderList[i].myButton;
		myCurrHoveredButtonPtr->myState = GET_HOVERED_BUTTON_STATE(myCurrHoveredButtonPtr);
		return;
	}

	for (unsigned short i(0); i < myCheckBoxList.Size(); ++i)
	{
		spritePtr = &GET_ACTIVE_CHECKBOX_SPRITE(myCheckBoxList[i]);

		size = spritePtr->GetOriginalSize() * spritePtr->GetScale();
		colliSize = (myCheckBoxList[i].myCollisionSize * size);
		position = spritePtr->GetPosition();

		colliSize.x *= windowDiff.x;
		colliSize.y *= windowDiff.y;
		colliSize.x /= windowRes.x;
		colliSize.y /= windowRes.y;

		if (position.x > mousePos.x) continue;
		if (position.y > mousePos.y) continue;
		if (position.x + colliSize.x < mousePos.x) continue;
		if (position.y + colliSize.y < mousePos.y) continue;

		myCurrHoveredCheckBoxPtr = &myCheckBoxList[i];
		myCurrHoveredCheckBoxPtr->myState = GET_HOVERED_CHECKBOX_STATE(myCurrHoveredCheckBoxPtr);
		return;
	}

	for (unsigned short i(0); i < myNavigationButtonList.Size(); ++i)
	{
		if (CheckMouseVSButtonIntersection(spritePtr, myNavigationButtonList[i].myPrevButton, mousePos, windowDiff, windowRes, position, size, colliSize))
		{
			myCurrHoveredButtonPtr = &myNavigationButtonList[i].myPrevButton;
			myCurrHoveredButtonPtr->myState = GET_HOVERED_BUTTON_STATE(myCurrHoveredButtonPtr);
			return;
		}
		else if (CheckMouseVSButtonIntersection(spritePtr, myNavigationButtonList[i].myNextButton, mousePos, windowDiff, windowRes, position, size, colliSize))
		{
			myCurrHoveredButtonPtr = &myNavigationButtonList[i].myNextButton;
			myCurrHoveredButtonPtr->myState = GET_HOVERED_BUTTON_STATE(myCurrHoveredButtonPtr);
			return;
		}
	}
}

void CMenu::SwapAndResetEveryHoveredComponent()
{
	myPrevHoveredCheckBoxPtr = myCurrHoveredCheckBoxPtr;
	myCurrHoveredCheckBoxPtr = nullptr;

	if (myPrevHoveredCheckBoxPtr != nullptr)
	{
		myPrevHoveredCheckBoxPtr->myIsPressed = false;
		myPrevHoveredCheckBoxPtr->myState = GET_RESETED_CHECKBOX_HOVER(myPrevHoveredCheckBoxPtr);
	}

	//myCurrHoveredButtonPtr->myPreviousState = myCurrHoveredButtonPtr->myState;
	myCurrHoveredSliderPtr = nullptr;

	myPrevHoveredButtonPtr = myCurrHoveredButtonPtr;
	myCurrHoveredButtonPtr = nullptr;

	if (myPrevHoveredButtonPtr != nullptr)
	{
		myPrevHoveredButtonPtr->myState = GET_RESETED_BUTTON_STATE(myPrevHoveredButtonPtr);
		myPrevHoveredButtonPtr->myIsPressed = false;
		myPrevHoveredButtonPtr->myIsHeld = false;
	}
}

const bool CMenu::CheckMouseVSButtonIntersection(sce::gfx::CSprite* aSpritePtr, MenuComponents::SButton& aButtonRef,
	const CU::Vector2f& aMousePosition, const CU::Vector2f& aWindowDiff, const CU::Vector2f& aWindowRes,
	CU::Vector2f& aPositionRef, CU::Vector2f& aSizeRef, CU::Vector2f& aCollisionSizeRef) const
{
	aSpritePtr = &GET_ACTIVE_BUTTON_SPRITE(aButtonRef);

	aSizeRef = aSpritePtr->GetOriginalSize() * aSpritePtr->GetScale();
	aCollisionSizeRef = (aButtonRef.myCollisionSize * aSizeRef);
	aPositionRef = aSpritePtr->GetPosition();
	aCollisionSizeRef.x *= aWindowDiff.x;
	aCollisionSizeRef.y *= aWindowDiff.y;
	aCollisionSizeRef.x /= aWindowRes.x;
	aCollisionSizeRef.y /= aWindowRes.y;

	if (aPositionRef.x > aMousePosition.x) return false;
	if (aPositionRef.y > aMousePosition.y) return false;
	if (aPositionRef.x + aCollisionSizeRef.x < aMousePosition.x) return false;
	if (aPositionRef.y + aCollisionSizeRef.y < aMousePosition.y) return false;

	return true;
}

void CMenu::PlayHoverSoundOnHoverEnter()
{
	// Button
	if (myPrevHoveredButtonPtr != nullptr && myCurrHoveredButtonPtr != nullptr)
	{
		if (myPrevHoveredButtonPtr->myName != myCurrHoveredButtonPtr->myName)
		{
			myHoverPlayFunc();
		}
	}
	else if (myCurrHoveredButtonPtr != nullptr)
	{
		myHoverPlayFunc();
	}

	// Checkbox
	if (myPrevHoveredCheckBoxPtr != nullptr && myCurrHoveredCheckBoxPtr != nullptr)
	{
		if (myPrevHoveredCheckBoxPtr->myName != myCurrHoveredCheckBoxPtr->myName)
		{
			myHoverPlayFunc();
		}
	}
	else if (myCurrHoveredCheckBoxPtr != nullptr)
	{
		myHoverPlayFunc();
	}
}

void CMenu::PlaySliderSoundOnMove()
{
	if (mySliderSoundPlayMaxTime >= 0.0f && myCurrFixedSliderValue != myPrevFixedSliderValue)
	{
		if (mySliderSoundPlayTimer >= mySliderSoundPlayMaxTime)
		{
			mySliderSoundPlayTimer = 0.0f;

			mySliderPlayFunc();
		}
	}
}
