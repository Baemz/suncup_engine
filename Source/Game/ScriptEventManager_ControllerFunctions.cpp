#include "stdafx.h"
#include "ScriptEventManager_ControllerFunctions.h"
#include "EntitySystemLink\RenderComponents.h"
#include "ScriptEventManager.h"
#include "GameSystems\ExposedFunctions\GameLogicFunctions.h"

#define CHECK_IF_STATE_IS_REMOVED(aStateIndex, aReturnValue)																\
std::shared_lock<std::shared_mutex> sharedLock(aScriptEventManager.myStatesToRemoveMutex);									\
if (aScriptEventManager.myStatesToRemove.Find(aStateIndex) != CU::GrowingArray<CScriptManager::StateIndexType>::FoundNone)	\
return aReturnValue;

bool CScriptEventManager_ControllerFunctions::InitFunctions(CScriptEventManager& aScriptEventManager)
{
	CScriptManager& myScriptManager(aScriptEventManager.myScriptManager);

	std::function<void(const CScriptManager::StateIndexType, bool)> makeEnemyBind(
		[&aScriptEventManager](const CScriptManager::StateIndexType aStateIndex, bool aMovement)
	{
		CHECK_IF_STATE_IS_REMOVED(aStateIndex, ;);

		const SEntityHandle& handle(aScriptEventManager.GetEntityHandle(aStateIndex));

		CGameLogicFunctions::MakeEnemy(handle.myUniqueIndex, aMovement);
	});
	myScriptManager.RegisterFunctionCallWithStateIndex("MakeEnemy", makeEnemyBind
		, ""
		, "Attaches an AI controller to the entity.");

	std::function<void(const CScriptManager::StateIndexType)> makeProjectileBind(
		[&aScriptEventManager](const CScriptManager::StateIndexType aStateIndex)
	{
		CHECK_IF_STATE_IS_REMOVED(aStateIndex, ;);
		const SEntityHandle& handle(aScriptEventManager.GetEntityHandle(aStateIndex));
		CGameLogicFunctions::MakeProjectile(handle.myUniqueIndex);
	});
	myScriptManager.RegisterFunctionCallWithStateIndex("MakeProjectile", makeProjectileBind
		, ""
		, "Attaches a projectile collider to the object.");

	std::function<void(const CScriptManager::StateIndexType)> makeHealthPickupBind(
		[&aScriptEventManager](const CScriptManager::StateIndexType aStateIndex)
	{
		CHECK_IF_STATE_IS_REMOVED(aStateIndex, ;);
		const SEntityHandle& handle(aScriptEventManager.GetEntityHandle(aStateIndex));
		CGameLogicFunctions::MakeHealthPickup(handle.myUniqueIndex);
	});
	myScriptManager.RegisterFunctionCallWithStateIndex("MakeHealthPickup", makeHealthPickupBind
		, ""
		, "Attaches a health pickup collider to the object.");

	std::function<void(const CScriptManager::StateIndexType)> makeCheckpointBind(
		[&aScriptEventManager](const CScriptManager::StateIndexType aStateIndex)
	{
		CHECK_IF_STATE_IS_REMOVED(aStateIndex, ;);
		const SEntityHandle& handle(aScriptEventManager.GetEntityHandle(aStateIndex));
		CGameLogicFunctions::MakeCheckpoint(handle.myUniqueIndex);
	});
	myScriptManager.RegisterFunctionCallWithStateIndex("MakeCheckpoint", makeCheckpointBind
		, ""
		, "Attaches a checkpoint tag to the object.");

	std::function<void(const CScriptManager::StateIndexType)> makePlayerBind(
		[&aScriptEventManager](const CScriptManager::StateIndexType aStateIndex)
	{
		CHECK_IF_STATE_IS_REMOVED(aStateIndex, ;);

		const SEntityHandle& handle(aScriptEventManager.GetEntityHandle(aStateIndex));
		CGameLogicFunctions::MakePlayer(handle.myUniqueIndex);
	});
	myScriptManager.RegisterFunctionCallWithStateIndex("MakePlayer", makePlayerBind
		, ""
		, "Attaches an Input controller to the entity.");

	std::function<void(const CScriptManager::StateIndexType)> makeCompanionBind(
		[&aScriptEventManager](const CScriptManager::StateIndexType aStateIndex)
	{
		CHECK_IF_STATE_IS_REMOVED(aStateIndex, ;);

		const SEntityHandle& handle(aScriptEventManager.GetEntityHandle(aStateIndex));
		CGameLogicFunctions::MakeCompanion(handle.myUniqueIndex);
	});
	myScriptManager.RegisterFunctionCallWithStateIndex("MakeCompanion", makeCompanionBind
		, ""
		, "Attaches a companion controller to the entity.");

	std::function<void(const CScriptManager::StateIndexType, float)> setMovementSpeedBind(
		[&aScriptEventManager](const CScriptManager::StateIndexType aStateIndex, float aMovementSpeed)
	{
		CHECK_IF_STATE_IS_REMOVED(aStateIndex, ;);
		CGameLogicFunctions::SetMovementSpeed(aScriptEventManager.GetEntityUniqueIndex(aStateIndex), aMovementSpeed);
	});
	myScriptManager.RegisterFunctionCallWithStateIndex("SetMovementSpeed", setMovementSpeedBind
		, "Number"
		, "Sets the object's max movement speed to the [value]");

	std::function<void(const CScriptManager::StateIndexType, float)> setSteeringSpeedBind(
		[&aScriptEventManager](const CScriptManager::StateIndexType aStateIndex, float aSteeringSpeed)
	{
		CHECK_IF_STATE_IS_REMOVED(aStateIndex, ;);
		CGameLogicFunctions::SetAcceleration(aScriptEventManager.GetEntityUniqueIndex(aStateIndex), aSteeringSpeed);
	});
	myScriptManager.RegisterFunctionCallWithStateIndex("SetAcceleration", setSteeringSpeedBind
		, "Number"
		, "Sets the object's acceleration speed to the [value]");

	std::function<void(const CScriptManager::StateIndexType, float)> setRotationSpeedBind(
		[&aScriptEventManager](const CScriptManager::StateIndexType aStateIndex, float aRotationSpeed)
	{
		CHECK_IF_STATE_IS_REMOVED(aStateIndex, ;);

		CGameLogicFunctions::SetRotationSpeed(aScriptEventManager.GetEntityUniqueIndex(aStateIndex), aRotationSpeed);
	});
	myScriptManager.RegisterFunctionCallWithStateIndex("SetRotationSpeed", setRotationSpeedBind
		, "Number"
		, "Sets the object's rotation speed to the [value] radians per second");


	std::function<void(const CScriptManager::StateIndexType)> clearPathBind(
		[&aScriptEventManager](const CScriptManager::StateIndexType aStateIndex)
	{
		CHECK_IF_STATE_IS_REMOVED(aStateIndex, ;);

		CGameLogicFunctions::ClearNavMeshPath(aScriptEventManager.GetEntityUniqueIndex(aStateIndex));
	});
	myScriptManager.RegisterFunctionCallWithStateIndex("ClearPath", clearPathBind
		, ""
		, "Clears the object's path made from navigation mesh.");

	std::function<void(const CScriptManager::StateIndexType)> makeSpawnerPaintingBind(
		[&aScriptEventManager](const CScriptManager::StateIndexType aStateIndex)
	{
		CHECK_IF_STATE_IS_REMOVED(aStateIndex, ;);

		CGameLogicFunctions::MakeSpawnerPainting(aScriptEventManager.GetEntityUniqueIndex(aStateIndex));
	});
	myScriptManager.RegisterFunctionCallWithStateIndex("MakeSpawnerPainting", makeSpawnerPaintingBind
		, ""
		, "Attaches an AI controller to the entity.");

	std::function<void(const CScriptManager::StateIndexType)> makeBossBind(
		[&aScriptEventManager](const CScriptManager::StateIndexType aStateIndex)
	{
		CHECK_IF_STATE_IS_REMOVED(aStateIndex, ;);

		CGameLogicFunctions::MakeBoss(aScriptEventManager.GetEntityUniqueIndex(aStateIndex));
	});
	myScriptManager.RegisterFunctionCallWithStateIndex("MakeBoss", makeBossBind
		, ""
		, "Attaches a boss to the entity.");

	return true;
}
