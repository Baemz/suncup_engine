#pragma once
#include "State.h"

namespace sce
{
	namespace gfx
	{
		class CSprite;
		class CText;
	}
}

class CCreditsState : private CState
{
public:
	CCreditsState(EManager& aEntityManager, bool aExitMainstate);
	~CCreditsState();

	void Init() override;
	void Update(const float aDeltaTime) override;
	void Render() override;

	void OnActivation() override;
	void OnDeactivation() override;

private:
	sce::gfx::CSprite* myBlackCanvasSprite;
	sce::gfx::CSprite* myCreditsImage;
	sce::gfx::CText* myText;

	const float myTimeToRollCredits;
	float myElapsedTime;
	bool myExitMainstate;
};