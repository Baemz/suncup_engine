#include "stdafx.h"
#include "PauseMenuState.h"
#include "OptionMenuState.h"
#include "ControlMenuState.h"
#include "StateStack.h"

CPauseMenuState::CPauseMenuState(EManager& aEntityManager)
	: CState(aEntityManager)
{
	//AI::PauseAllAudio();
	myMenu.Disable();
}

CPauseMenuState::~CPauseMenuState()
{
	//AI::ResumeAllAudio();
}

void CPauseMenuState::Init()
{
}

void CPauseMenuState::Update(const float aDeltaTime)
{
	aDeltaTime;
}

void CPauseMenuState::Render()
{
	//myMenu.Render(myState == CState::EStateCommand::eActive);
}

void CPauseMenuState::OnActivation()
{
	myMenu.Enable();
}

void CPauseMenuState::OnDeactivation()
{
}
