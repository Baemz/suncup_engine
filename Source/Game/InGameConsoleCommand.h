#pragma once
#include <string>

struct SInGameConsoleCommand
{
	SInGameConsoleCommand() = default;
	SInGameConsoleCommand(const std::string& aCommand, const std::string& aFuncName, const size_t aSimilarityVal)
		: myCommand(aCommand), myFuncName(aFuncName), mySimilarityVal(aSimilarityVal)
	{}

	std::string myCommand;
	std::string myFuncName; // HACK, should be something else that we can work better with.
	size_t mySimilarityVal;
};