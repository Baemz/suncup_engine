#pragma once

class CScriptEventManager;

class CScriptEventManager_MoveObject
{
public:
	static bool InitFunctions(CScriptEventManager& aScriptEventManager);

private:
	CScriptEventManager_MoveObject() = delete;
	~CScriptEventManager_MoveObject() = delete;

};
