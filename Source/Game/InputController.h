#pragma once
#include "Controller.h"

class CInputController : public CController
{
public:
	CInputController() { myControllerType = EController::Input; }
	CInputController(const SControllerInit& aInitData, const SInputInit& aInputInitData);

	const CU::Vector3f myCameraOffset;
};