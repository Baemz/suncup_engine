#pragma once
#include "..\CommonUtilities\Vector3.h"
#include "EntitySystemLink\EntitySystemLink.h"
#include "GameSystems\SystemAbstract.h"
class CDamageZoneSystem : public CSystemAbstract<CDamageZoneSystem>
{
public:
	CDamageZoneSystem(EManager& aEntityManager);
	~CDamageZoneSystem() override = default;

	void Activate();
	void Deactivate();

	void Update(const float aDeltaTime, const CU::GrowingArray<const CU::GrowingArray<UniqueIndexType>*>& aEntities) override;
	void CreateDamageZone(CU::Vector3f aPos, const float aLifeTime, const float aDamageRadius, const float aDmgPerSecond);


private:
	struct SDamageZone
	{
		SDamageZone()
			: myLifeTime(0.f)
			, myRadius(0.f)
			, myDmgPerSecond(0.f)
		{}
		SDamageZone(CU::Vector3f aPos, const float aLifeTime, const float aDamageRadius, const float aDmgPerSecond)
			: myPosition(aPos)
			, myLifeTime(aLifeTime)
			, myRadius(aDamageRadius)
			, myDmgPerSecond(aDmgPerSecond)
		{
		}
		~SDamageZone() = default;

		CU::Vector3f myPosition;
		float myLifeTime;
		float myRadius;
		float myDmgPerSecond;

	};

	CU::GrowingArray<SDamageZone> myDamageZones;
	std::mutex myDamageZoneMutex;
};

