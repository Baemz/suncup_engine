#pragma once
#include "State.h"
#include "Menu.h"

class CPauseMenuState: private CState
{
public:
	CPauseMenuState(EManager& aEntityManager);
	~CPauseMenuState();

	void Init() override;
	void Update(const float aDeltaTime) override;
	void Render() override;

	void OnActivation() override;
	void OnDeactivation() override;

private:
	CMenu myMenu;
};

