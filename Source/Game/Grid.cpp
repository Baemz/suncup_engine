#include "stdafx.h"
#include "Grid.h"
#include "..\GraphicsEngine\DebugTools.h"
#include "..\CommonUtilities\Macros.h"

#ifdef max
#undef max
#endif // max

CGrid::CGrid()
	: myRows(0)
	, myColumns(0)
{
}

CGrid::~CGrid()
{
}

void CGrid::Init(const float& aCellSize, const CU::Vector4f& aAABB)
{
	myAABB = aAABB;
	const CU::Vector2f totalSize((aAABB.z - aAABB.x), (aAABB.w - aAABB.y));

	myCellSize = aCellSize;

	myRows = static_cast<CellCountType>(std::ceilf(totalSize.x / myCellSize));
	myColumns = static_cast<CellCountType>(std::ceilf(totalSize.y / myCellSize));

	const unsigned cellAmount(myRows * myColumns);
	myCells.Resize(cellAmount);

	for (CellCountType cellIndex(0); cellIndex < cellAmount; ++cellIndex)
	{
		auto& currentCell(myCells[cellIndex]);

		currentCell.myPosition.x = aAABB.x + myCellSize * (cellIndex % myRows);
		currentCell.myPosition.y = aAABB.y + myCellSize * (cellIndex / myRows);
	}
}

void CGrid::AddObject(const SGridData& aEntity)
{
	const CU::Vector2f minPosition(aEntity.myCenterPosition - (aEntity.mySize / 2.0f));
	const CU::Vector2f maxPosition(aEntity.myCenterPosition + (aEntity.mySize / 2.0f));

	const CellCountType minGrid(GetCellIndexOver(minPosition, false, false));
	const CellCountType maxGrid(GetCellIndexOver(maxPosition, false, true));

	if (minGrid == InvalidCellIndex || maxGrid == InvalidCellIndex)
	{
		if (aEntity.myOnOutsideOfGridCallback != nullptr)
		{
			aEntity.myOnOutsideOfGridCallback();
			return;
		}
	}

	ForeachCell(minGrid, maxGrid
		, [&entityIndices = myEntityIndices, &aEntity](const CellCountType aCellIndex, SGridCell& aCell)
	{
		aCell.myEntities.insert(aEntity);
		entityIndices.insert({ aEntity.myEntityUniqueIndex, aCellIndex });
	});
}

void CGrid::RemoveObject(const UniqueIndexType& aEntityIndex)
{
	auto entityIterator(myEntityIndices.equal_range(aEntityIndex));

	if (entityIterator.first == entityIterator.second)
	{
		GAME_LOG("WARNING! Can't remove entity(%llu) from grid, doesn't exist inside!"
			, aEntityIndex);
		return;
	}

	SGridData gridEntity;
	gridEntity.myEntityUniqueIndex = aEntityIndex;

	for (auto value(entityIterator.first); value != entityIterator.second; ++value)
	{
		auto& cellEntities(myCells[value->second].myEntities);
		cellEntities.erase(gridEntity);
	}

	myEntityIndices.erase(entityIterator.first, entityIterator.second);
}

void CGrid::UpdateObjectGridPosition(const UniqueIndexType& aEntityIndex, const CU::Vector2f aNewCenterPosition)
{
	auto entityIterator(myEntityIndices.equal_range(aEntityIndex));

	if (entityIterator.first == entityIterator.second)
	{
		GAME_LOG("WARNING! Can't update entity(%llu) in grid, doesn't exist inside!"
			, aEntityIndex);
		return;
	}

	SGridData gridEntity;
	gridEntity.myEntityUniqueIndex = aEntityIndex;

	auto& entities(myCells[entityIterator.first->second].myEntities);
	auto entityFound(entities.find(gridEntity));
	if (entityFound == entities.end())
	{
		assert("Something has gone wrong with the grid" && (false));
		return;
	}

	gridEntity.mySize = entityFound->mySize;
	gridEntity.myCenterPosition = aNewCenterPosition;
	gridEntity.myIsTrigger = entityFound->myIsTrigger;
	gridEntity.myOnOutsideOfGridCallback = entityFound->myOnOutsideOfGridCallback;

	RemoveObject(aEntityIndex);

	AddObject(gridEntity);
}

bool CGrid::GetObjects(CU::GrowingArray<UniqueIndexType>& aArrayToFill)
{
	if (myEntityIndices.empty() == false)
	{
		for (auto pair : myEntityIndices)
		{
			aArrayToFill.AddUnique(pair.first);
		}

		return true;
	}

	return false;
}

void CGrid::SetObjectOnGridLeaveCallback(const UniqueIndexType & aEntityIndex, std::function<void()> aOnGridLeaveCallback)
{
	auto entityIterator(myEntityIndices.equal_range(aEntityIndex));

	if (entityIterator.first == entityIterator.second)
	{
		GAME_LOG("WARNING! Can't update entity(%llu) in grid, doesn't exist inside!"
			, aEntityIndex);
	}

	SGridData gridEntity;
	gridEntity.myEntityUniqueIndex = aEntityIndex;

	auto& entities(myCells[entityIterator.first->second].myEntities);
	auto entityFound(entities.find(gridEntity));
	if (entityFound == entities.end())
	{
		assert("Something has gone wrong with the grid" && (false));
	}

	entityFound->myOnOutsideOfGridCallback = aOnGridLeaveCallback;
}

void CGrid::Clear()
{
	for (auto& cell : myCells)
	{
		cell.myEntities.clear();
	}
	myEntityIndices.clear();
}

bool CGrid::GetObjectsCloseToPoint(const CU::Vector2f& aPoint, CU::GrowingArray<SGridData>& aArrayToFill) const
{
	CellCountType cellIndex(GetCellIndexOver(aPoint, true, false));

	assert("Entity doesn't fit inside grid" && (cellIndex != InvalidCellIndex));

	for (const auto& cellData : myCells[cellIndex].myEntities)
	{
		aArrayToFill.Add(cellData);
	}

	return true;
}

bool CGrid::GetObjectsCloseToAABB(const CU::Vector4f& aAABB, CU::GrowingArray<SGridData>& aArrayToFill) const
{
	CellCountType minIndex(GetCellIndexOver({ aAABB.x, aAABB.y }, true, false));
	CellCountType maxIndex(GetCellIndexOver({ aAABB.z, aAABB.w }, true, true));

	ForeachCell(minIndex, maxIndex
		, [&aArrayToFill](const CellCountType, const SGridCell& aCell)
	{
		for (const auto& cellData : aCell.myEntities)
		{
			aArrayToFill.AddUnique(cellData);
		}
	});

	return true;
}

void CGrid::DebugRender()
{
	if (myShouldRenderGrid == true)
	{
		sce::gfx::CDebugTools* debugDrawer(sce::gfx::CDebugTools::Get());

		for (auto& currentCell : myCells)
		{
			const CU::Vector3f cellColor(0.0f, 0.0f, 1.0f * currentCell.myEntities.size());
			const CU::Vector3f topLeft(currentCell.myPosition.x, 0.0f, currentCell.myPosition.y);
			const CU::Vector3f topRight((currentCell.myPosition.x + myCellSize), 0.0f, currentCell.myPosition.y);
			const CU::Vector3f bottomLeft(currentCell.myPosition.x, 0.0f, (currentCell.myPosition.y + myCellSize));
			const CU::Vector3f bottomRight((currentCell.myPosition.x + myCellSize), 0.0f, (currentCell.myPosition.y + myCellSize));

			debugDrawer->DrawLine(topLeft, topRight, cellColor, cellColor);
			debugDrawer->DrawLine(topLeft, bottomLeft, cellColor, cellColor);
			debugDrawer->DrawLine(topRight, bottomRight, cellColor, cellColor);
			debugDrawer->DrawLine(bottomLeft, bottomRight, cellColor, cellColor);
		}
	}
}

CGrid::CellCountType CGrid::GetCellIndexOver(const CU::Vector2f& aPosition, const bool aValidOutside, const bool aRoundUp) const
{
	CellCountType nodeArrayIndex(InvalidCellIndex);

	const SGridCell& firstCell(myCells[0]);
	const SGridCell& lastCell(myCells[(myRows * myColumns) - 1]);

	CU::Vector2f gridSize((lastCell.myPosition + CU::Vector2f(myCellSize, myCellSize)) - firstCell.myPosition);
	gridSize.x = abs(gridSize.x);
	gridSize.y = abs(gridSize.y);

	CU::Vector2f positionToCheck(aPosition - firstCell.myPosition);

	aRoundUp;
// 	if (aRoundUp == true)
// 	{
// 		positionToCheck.x = ceilf(positionToCheck.x);
// 		positionToCheck.x = ceilf(positionToCheck.y);
// 	}
// 	else
// 	{
// 		positionToCheck.x = floorf(positionToCheck.x);
// 		positionToCheck.x = floorf(positionToCheck.y);
// 	}

	if (aValidOutside == true)
	{
		positionToCheck.x = CLAMP(positionToCheck.x, 0.0f, gridSize.x);
		positionToCheck.y = CLAMP(positionToCheck.y, 0.0f, gridSize.y);
	}

	if (((positionToCheck.x >= 0.0f)
		&& (positionToCheck.y >= 0.0f))
		&& ((positionToCheck.x <= gridSize.x)
			&& (positionToCheck.y <= gridSize.y)))
	{
		CellCountType nodeRowIndex = static_cast<CellCountType>(positionToCheck.x / myCellSize);
		CellCountType nodeColumnIndex = static_cast<CellCountType>(positionToCheck.y / myCellSize);

		nodeRowIndex = CLAMP(nodeRowIndex, 0, (myRows - 1));
		nodeColumnIndex = CLAMP(nodeColumnIndex, 0, (myColumns - 1));

		nodeArrayIndex = (nodeRowIndex + (nodeColumnIndex * myRows));
	}

	return nodeArrayIndex;
}

void CGrid::ForeachCell(const CellCountType& aMinGrid, const CellCountType& aMaxGrid, const std::function<void(const CellCountType, SGridCell&)>& aFunction)
{
	assert("Entity doesn't fit inside grid" && ((aMinGrid != InvalidCellIndex) && (aMaxGrid != InvalidCellIndex)));

	const CellCountType minRow(aMinGrid % myRows);
	const CellCountType minColumn(aMinGrid / myRows);

	const CellCountType maxRow(aMaxGrid % myRows);
	const CellCountType maxColumn(aMaxGrid / myRows);

	for (CellCountType currentColumn(minColumn); currentColumn <= maxColumn; ++currentColumn)
	{
		for (CellCountType currentRow(minRow); currentRow <= maxRow; ++currentRow)
		{
			const CellCountType cellIndex((currentColumn * myRows) + currentRow);
			aFunction(cellIndex, myCells[cellIndex]);
		}
	}
}

void CGrid::ForeachCell(const CellCountType& aMinGrid, const CellCountType& aMaxGrid, const std::function<void(const CellCountType, const SGridCell&)>& aFunction) const
{
	assert("Entity doesn't fit inside grid" && ((aMinGrid != InvalidCellIndex) && (aMaxGrid != InvalidCellIndex)));

	const CellCountType minRow(aMinGrid % myRows);
	const CellCountType minColumn(aMinGrid / myRows);

	const CellCountType maxRow(aMaxGrid % myRows);
	const CellCountType maxColumn(aMaxGrid / myRows);

	for (CellCountType currentColumn(minColumn); currentColumn <= maxColumn; ++currentColumn)
	{
		for (CellCountType currentRow(minRow); currentRow <= maxRow; ++currentRow)
		{
			const CellCountType cellIndex((currentColumn * myRows) + currentRow);
			aFunction(cellIndex, myCells[cellIndex]);
		}
	}
}
