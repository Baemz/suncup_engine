#include "stdafx.h"
#include "ScriptEventManager_WorldInterface.h"
#include "ScriptEventManager.h"
#include "GameSystems\ExposedFunctions\GameLogicFunctions.h"
#include "NavigationMesh\NavigationMesh.h"

bool CScriptEventManager_WorldInterface::InitFunctions(CScriptEventManager& aScriptEventManager)
{
	CScriptManager& myScriptManager(aScriptEventManager.myScriptManager);

	std::function<CU::Vector3f(UniqueIndexType)> getEntityPositionBind(
		[&aScriptEventManager](UniqueIndexType aEntity) -> CU::Vector3f
	{
		return CGameLogicFunctions::GetEntityPosition(aEntity);
	});
	myScriptManager.RegisterFunction("GetEntityPosition", getEntityPositionBind
		, "Index"
		, "Returns the x, y and z components of entity with [ID]");

	std::function<CU::Vector3f(UniqueIndexType)> getEntityDirectionBind(
		[&aScriptEventManager](UniqueIndexType aEntity) -> CU::Vector3f
	{
		return CGameLogicFunctions::GetEntityDirection(aEntity);
	});
	myScriptManager.RegisterFunction("GetEntityForwardDirection", getEntityDirectionBind
		, "Index"
		, "Returns the x, y and z direction components of entity with [ID]");

	std::function<CU::Vector3f(UniqueIndexType)> getEntityRotationBind(
		[&aScriptEventManager](UniqueIndexType aEntity) -> CU::Vector3f
	{
		return CGameLogicFunctions::GetRotation(aEntity);
	});
	myScriptManager.RegisterFunction("GetEntityRotation", getEntityRotationBind
		, "Index"
		, "Returns the x, y and z Rotation of entity with [ID]");

	std::function<CU::Vector3f(UniqueIndexType, float, float)> GetOffsetEntityPositionPolarForm(
		[&aScriptEventManager](UniqueIndexType aEntity, float aAngle, float aDistance) -> CU::Vector3f
	{
		return CGameLogicFunctions::GetPositionFromAngleAndDistanceFromEntity(aEntity, aAngle, aDistance);
	});
	myScriptManager.RegisterFunction("GetOffsetEntityPositionPolarForm", GetOffsetEntityPositionPolarForm
		, "Index, Angle, Distance"
		, "Returns the [Entity] position with [Angle] pointing from the object a [Distance] amount");

	std::function<UniqueIndexType()> getPlayerUniqueIndexBind(
		[&aScriptEventManager]() -> UniqueIndexType
	{
		return CGameLogicFunctions::GetPlayerUniqueIndex();
	});
	myScriptManager.RegisterFunction("GetPlayerEntityIndex", getPlayerUniqueIndexBind
		, ""
		, "Gets the player's entity index, if you need to call events on the player"
		, true);

	std::function<void(const char*)> switchLevelToBind(
		[&aScriptEventManager](const char* aLevelPath)
	{
		std::string levelPath(aLevelPath);
		CGameLogicFunctions::SwitchLevelTo(levelPath);
	});
	myScriptManager.RegisterFunction("SwitchLevelTo", switchLevelToBind
		, "String"
		, "Sets next level to [path], will start the switch at the start of next frame");
	std::function<CU::Vector3f(UniqueIndexType)> drawLineFromEntityToMouseBind(
		[&aScriptEventManager](UniqueIndexType aUniqueEntityIndex) -> CU::Vector3f
	{
		return CGameLogicFunctions::DrawLineFromEntityToMouse(aUniqueEntityIndex);
	});
	myScriptManager.RegisterFunction("DrawLineFromEntityToMouse", drawLineFromEntityToMouseBind
		, "Index"
		, "Draws a line from the entity to mouse position, returns mousePos");

	std::function<void(float)> setHealAbilityCooldownBind(
		[&aScriptEventManager](float aCooldown)
	{
		UniqueIndexType playerUniqueIndex(CGameLogicFunctions::GetPlayerUniqueIndex());
		if (!CGameLogicFunctions::IsAlive(playerUniqueIndex))
		{
			return;
		}

		aScriptEventManager.myEntityManager.GetComponent<CompPlayerData>(SEntityHandle(playerUniqueIndex)).myHealAbilityCooldown = aCooldown;
	});

	myScriptManager.RegisterFunction("SetHealAbilityCooldown", setHealAbilityCooldownBind,
		"Number",
		"Sets the heal ability's cooldown percentage");

	std::function<float()> getHealAbilityCooldownBind(
		[&aScriptEventManager]() -> float
	{
		UniqueIndexType playerUniqueIndex(CGameLogicFunctions::GetPlayerUniqueIndex());
		if (!CGameLogicFunctions::IsAlive(playerUniqueIndex))
		{
			return 0.0f;
		}

		return aScriptEventManager.myEntityManager.GetComponent<CompPlayerData>(SEntityHandle(playerUniqueIndex)).myHealAbilityCooldown;
	});

	myScriptManager.RegisterFunction("GetHealAbilityCooldown", getHealAbilityCooldownBind,
		"",
		"Returns via the heal ability's cooldown");

	std::function<void(const UniqueIndexType, const float, const float, const float, const float, const float, const float, const float, const float, const float, const float, const float, const float)> tossHealingAbilityBind(
		[&aScriptEventManager](const UniqueIndexType& aEntityUniqueIndex, const float aHealPosX, const float aHealPosY, const float aHealPosZ,
			const float aHealingLifeTime, const float aHealingCircleRadius, const float aHealingPerTick, const float aHealingTickInterval,
			const float aDamageLifeTime, const float aDamageCircleRadius, const float aDamagePerSecond, const float aDamageZoneSpawnInterval, const float aFlightSpeed)
	{
		return CGameLogicFunctions::TossHealingAbility(aEntityUniqueIndex, { aHealPosX, aHealPosY, aHealPosZ },
			aHealingLifeTime, aHealingCircleRadius, aHealingPerTick, aHealingTickInterval,
			aDamageLifeTime, aDamageCircleRadius, aDamagePerSecond, aDamageZoneSpawnInterval, aFlightSpeed);
	});
	myScriptManager.RegisterFunction("TossHealingAbility", tossHealingAbilityBind
		, "Index, TargetPosX, TargetPosY, TargetPosZ, HealLifeTime, HealRadius, HealPerSecond, DamageLifeTime, DamageRadius, DamagePerSecond, FlightSpeed"
		, "Tosses Healing Ability from support(companion) to mousePos that deals dmg in flight and heals upon landing");

	std::function<bool(const CScriptManager::StateIndexType, float, float, float)> isPositionOnNavMeshBind(
		[&aScriptEventManager](const CScriptManager::StateIndexType aStateIndex, float aX, float aY, float aZ) -> bool
	{
		const UniqueIndexType entityUniqueIndex(aScriptEventManager.GetEntityUniqueIndex(aStateIndex));
		const float entityRadius(CGameLogicFunctions::GetEntityRadius(entityUniqueIndex));

		const CU::Collision::Ray posRay(CU::Vector3f(aX, aY + entityRadius, aZ), CU::Vector3f(0.0f, -1.0f, 0.0f));
		const bool result(CGameLogicFunctions::GetNavMesh()->IsIntersectingWithNavMesh(posRay));

		return result;
	});
	myScriptManager.RegisterFunctionCallWithStateIndex("IsPositionOnNavMesh", isPositionOnNavMeshBind
		, "Number, Number, Number"
		, "Checks if the position ([x], [y] and [z]) lies on the navigation mesh. Returns true if the position is on the mesh, in different scenario it returns false.");

	std::function<bool(const CScriptManager::StateIndexType, float, float, float)> isPositionOnWalkableNavMeshBind(
		[&aScriptEventManager](const CScriptManager::StateIndexType aStateIndex, float aX, float aY, float aZ) -> bool
	{
		const UniqueIndexType entityUniqueIndex(aScriptEventManager.GetEntityUniqueIndex(aStateIndex));
		const float entityRadius(CGameLogicFunctions::GetEntityRadius(entityUniqueIndex));

		const CU::Collision::Ray posRay(CU::Vector3f(aX, aY + entityRadius, aZ), CU::Vector3f(0.0f, -1.0f, 0.0f));
		const bool result(CGameLogicFunctions::GetNavMesh()->CheckNodeTypeOnRayCast(posRay, MeshComponents::ENodeType::eWalk_Dash, true, 2.0f));

		return result;
	});
	myScriptManager.RegisterFunctionCallWithStateIndex("IsPositionOnWalkableNavMesh", isPositionOnWalkableNavMeshBind
		, "Number, Number, Number"
		, "Checks if the position ([x], [y] and [z]) lies on walkable part of the navigation mesh. Returns true if the position is on the mesh, in different scenario it returns false.");

	std::function<void(const CScriptManager::StateIndexType, const char*, const char*)> setNavMeshTypeUnderEntityBind(
		[&aScriptEventManager](const CScriptManager::StateIndexType aStateIndex, const char* aNavMeshTypeReplace, const char* aNavMeshTypeWith)
	{
		const std::string navMeshTypeReplace(aNavMeshTypeReplace);
		const std::string navMeshTypeWith(aNavMeshTypeWith);
		return CGameLogicFunctions::SetNavMeshTypeUnderEntity(aScriptEventManager.GetEntityUniqueIndex(aStateIndex), navMeshTypeReplace, navMeshTypeWith);
	});
	myScriptManager.RegisterFunctionCallWithStateIndex("SetNavMeshTypeUnderEntity", setNavMeshTypeUnderEntityBind
		, "Index, String, String"
		, "Sets NavMesh triangles found under [Index] from [String] to [String].");

	std::function<void(const CScriptManager::StateIndexType)> resetNavMeshTypeUnderEntityBind(
		[&aScriptEventManager](const CScriptManager::StateIndexType aStateIndex)
	{
		return CGameLogicFunctions::ResetNavMeshTypeUnderEntity(aScriptEventManager.GetEntityUniqueIndex(aStateIndex));
	});
	myScriptManager.RegisterFunctionCallWithStateIndex("ResetNavMeshTypeUnderEntity", resetNavMeshTypeUnderEntityBind
		, "Index"
		, "Resets NavMesh triangles found under [Index].");

	return true;
}
