#pragma once
#include "../CommonUtilities/GrowingArray.h"
#include "../CommonUtilities/Vector.h"

class CMapReader;
class CMapData;

namespace MapDataHelpers
{
	template<typename StorageType>
	struct SArray
	{
		friend CMapData;

	public:
		inline const StorageType& operator[](const int& aIndex) const { return myData[aIndex]; };
		int Size() const { return mySize; };

		StorageType* myData = nullptr;
		int mySize = 0;

	};
}

class CMapDataObject
{
	friend CMapReader;
	friend CMapData;
	friend bool GetMapDataObject(char*, long long&, const long long&, CMapDataObject&);
	friend bool GetChildren(char*, long long&, const long long&, CMapDataObject&);

public:
	struct STransform
	{
		CU::Vector3f myPosition;
		CU::Vector3f myRotation;
		CU::Vector3f myScale;
	};

	struct SLight
	{
		enum class eLightType : char
		{
			NOT_SET,
			POINT,
			DIRECTIONAL,
			SPOT,
		};
		struct SPointLight
		{
			float aRange;
		};
		struct SDirectionalLight
		{
			CU::Vector3f myDirection;
		};
		struct SSpotLight
		{
			CU::Vector3f myDirection;
			float myAngle;
			float myRange;
		};
		union
		{
			SPointLight* myPointLightData = nullptr;
			SDirectionalLight* myDirectionalLightData;
			SSpotLight* mySpotLightData;
		};

		SLight()
			: myLightType(nullptr)
			, myColor(nullptr)
			, myIntensity(nullptr)
		{};

		eLightType* myLightType;
		CU::Vector4f* myColor;
		float* myIntensity;

	};

	struct SScriptData
	{
	private:
		friend bool GetScript(char* aData, long long& aDataIndex, const long long& aDataIndexMax, CMapDataObject::SScriptData& aScriptToFill);

		template<typename T>
		struct SVariableData
		{
			friend bool GetScript(char* aData, long long& aDataIndex, const long long& aDataIndexMax, CMapDataObject::SScriptData& aScriptToFill);

		public:
			inline const char* const GetName() const { return myName; }

			inline bool HasValidValue() const
			{
				if (myPair == nullptr)
				{
					return false;
				}

				return !!myPair->myIsValid;
			}

			inline const T& GetValue() const
			{
				return myPair->myValue;
			}

		private:
			struct SPair
			{
				int myIsValid;
				T myValue;
			};

			const char* myName = nullptr;
			SPair* myPair = nullptr;

		};

	public:
		bool IsValid() const;

		const char* const GetPath() const;
		const int& GetAmountOfIDVariables() const;
		const int& GetAmountOfNumberVariables() const;
		const int& GetAmountOfStringVariables() const;
		const SVariableData<int>* const GetIDVariableDatas() const;
		const SVariableData<int>* const GetNumberVariableDatas() const;
		const SVariableData<const char*>* const GetStringVariableDatas() const;

	private:
		const char* myScriptPath = nullptr;
		int* myAmountOfIDVariables = nullptr;
		int* myAmountOfNumberVariables = nullptr;
		int* myAmountOfStringVariables = nullptr;
		
		SVariableData<int>* myIDVariables = nullptr;
		SVariableData<int>* myNumberVariables = nullptr;
		SVariableData<const char*>* myStringVariables = nullptr;

	};

	struct SBoxCollider
	{
		CU::Vector3f myMin;
		CU::Vector3f myMax;
		bool myIsTrigger;
		bool _myTrash[3];
	};

	CMapDataObject();
	~CMapDataObject() = default;

	const char* const GetName() const;
	const STransform* const GetTransform() const;
	const CU::GrowingArray<CMapDataObject, unsigned int>& GetChildren() const;
	const CU::GrowingArray<SLight, unsigned int>& GetLights() const;

	const char* const GetTag() const;
	int GetInstanceID() const;
	const char* const GetMeshName() const;
	const char* const GetMaterialName() const;
	const CU::Vector3f* const GetModelSize() const;
	const SScriptData& GetScript() const;
	const SBoxCollider* GetBoxCollider() const;
	const int* GetIsStatic() const;
	const char* const GetParticleName() const;

private:
	const char* myName;
	STransform* myTransform;
	CU::GrowingArray<CMapDataObject, unsigned int> myChildren;
	CU::GrowingArray<SLight, unsigned int> myLights;

	const char* myTag;
	int* myInstanceID;
	const char* myMeshName;
	const char* myMaterialName;
	CU::Vector3f* myModelSize;
	SScriptData myScript;
	SBoxCollider* myBoxCollider;
	int* myIsStatic;
	const char* myParticleName;

};

class CMapData
{
	friend CMapReader;

public:
	struct SLevelInfo
	{
	private:
		friend CMapData;
		friend CMapReader;
		friend bool GetLevelInfo(char* aData, long long& aDataIndex, const long long& aDataIndexMax, CMapData::SLevelInfo& aLevelInfo);

	public:
		struct SNavMesh
		{
			enum class EAreaType : unsigned char
			{
				NOT_SET,
				WALK_DASH,
				DASH,
				NOTHING,
			};

			MapDataHelpers::SArray<int> myIndices;
			MapDataHelpers::SArray<CU::Vector3f> myVertices;
			MapDataHelpers::SArray<EAreaType> myTriangleTypes;
		};

		struct SMapSize
		{
			CU::Vector3f myMin;
			CU::Vector3f myMax;
		};

	private:
		friend bool GetNavMesh(char* aData, long long& aDataIndex, const long long& aDataIndexMax, CMapData::SLevelInfo::SNavMesh*& aNavMesh);

	public:
		const SNavMesh* const GetNavMesh() const;
		const int& GetRootAmountOfObjects() const;
		const int& GetTotalAmountOfObjects() const;
		const SMapSize& GetMapSize() const;

	private:
		SLevelInfo()
			: myNavMesh(nullptr)
		{};

		SNavMesh* myNavMesh;
		SMapSize* myMapSize;
		int* myRootAmountOfObjects;
		int* myTotalAmountOfObjects;

	};

	CMapData();
	~CMapData() = default;

	void Destroy();

	const SLevelInfo& GetLevelInfo() const;
	const CU::GrowingArray<CMapDataObject, unsigned int>& GetObjects() const;

private:
	void DestroyDataObject(CMapDataObject& aMapDataObject);

	char* myRawData;
	long long myRawDataSize;

	SLevelInfo myLevelInfo;
	CU::GrowingArray<CMapDataObject, unsigned int> myObjects;

};
