#include "stdafx.h"
#include "MapReader.h"
#include <fstream>

#ifdef max
#undef max
#endif // max

#define ALLOWED_TO_READ(aDataIndex, aDataIndexMax, aSize)						\
if (AllowedToReadData((aDataIndex), (aDataIndexMax), (aSize)) == false)			\
{																				\
	return false;																\
}

#define NEXT_IS_TEXT_LOADMAP(aText)						\
NextIsText(*rawData, dataIndex, byteLength, aText)
#define NEXT_IS_TEXT_OTHER(aText)						\
NextIsText(aData, aDataIndex, aDataIndexMax, aText)

#define STOP_THIS "STOP_THIS"

bool OpenFile(const char* aFilePath, std::ifstream& aFileStream, std::ios_base::openmode aOpenMode);

bool GetLevelInfo(char* aData, long long& aDataIndex, const long long& aDataIndexMax, CMapData::SLevelInfo& aLevelInfo);
bool GetNavMesh(char* aData, long long& aDataIndex, const long long& aDataIndexMax, CMapData::SLevelInfo::SNavMesh*& aNavMesh);

bool GetMapDataObject(char* aData, long long& aDataIndex, const long long& aDataIndexMax, CMapDataObject& aMapDataObject);
bool GetChildren(char* aData, long long& aDataIndex, const long long& aDataIndexMax, CMapDataObject& aMapDataObject);

bool NextIsText(char* aData, long long& aDataIndex, const long long& aDataIndexMax, const char* aText);

bool GetIntString(char* aData, long long& aDataIndex, const long long& aDataIndexMax, const char*& aNameToFill);
bool GetTransform(char* aData, long long& aDataIndex, const long long& aDataIndexMax, CMapDataObject::STransform*& aTransform);
bool GetScript(char* aData, long long& aDataIndex, const long long& aDataIndexMax, CMapDataObject::SScriptData& aScriptToFill);
bool GetBoxCollider(char* aData, long long& aDataIndex, const long long& aDataIndexMax, CMapDataObject::SBoxCollider*& aBoxColliderToFill);
bool GetLights(char* aData, long long& aDataIndex, const long long& aDataIndexMax, CU::GrowingArray<CMapDataObject::SLight, unsigned int>& aLightsToFill);
bool GetParticle(char* aData, long long& aDataIndex, const long long& aDataIndexMax, const char*& aParticlePath);

bool GetInt(char* aData, long long& aDataIndex, const long long& aDataIndexMax, int** aIntToFill);
bool GetString(char* aData, long long& aDataIndex, const long long& aDataIndexMax, const char*& aStringToFill, const int aSizeToGet);
bool GetIntString(char* aData, long long& aDataIndex, const long long& aDataIndexMax, const char*& aNameToFill);
bool GetChar(char* aData, long long& aDataIndex, const long long& aDataIndexMax, char** aCharToFill);
bool GetFloat(char* aData, long long& aDataIndex, const long long& aDataIndexMax, float** aFloatToFill);
bool GetBool(char* aData, long long& aDataIndex, const long long& aDataIndexMax, bool** aBoolToFill);
bool GetVector3(char* aData, long long& aDataIndex, const long long& aDataIndexMax, CU::Vector3f** aVectorToFill);

bool MovePointerSize(long long& aDataIndex, const long long& aDataIndexMax);

bool AllowedToReadData(const long long& aDataIndex, const long long& aDataIndexMax, const long long& aSize);

constexpr long long sizeOfInt = sizeof(int);
constexpr long long sizeOfFloat = sizeof(float);
constexpr long long sizeOfPointer = sizeof(void*);
constexpr long long sizeOfChar = sizeof(char);
constexpr long long sizeOfBool = sizeof(bool);
constexpr long long sizeOfVector3f = sizeof(CU::Vector3f);

CMapReader::CMapReader()
{
}

CMapReader::~CMapReader()
{
}

bool CMapReader::LoadMap(const char* aMapPath, CMapData& aMapDataToFill, const long long aMaxBytesToRead) const
{
	assert("Tried to load a map into an already used CMapData" && (aMapDataToFill.myObjects.Size() == 0));

	std::ifstream mapFile;
	if (OpenFile(aMapPath, mapFile, std::ifstream::in | std::ifstream::binary | std::ifstream::ate) == false)
	{
		return false;
	}
	
	const std::streamsize byteLength = mapFile.tellg();

	if ((aMaxBytesToRead < byteLength) && (aMaxBytesToRead != -1))
	{
		return false;
	}

	mapFile.seekg(0, std::ios_base::beg);

	aMapDataToFill.myRawData = sce_newArray(char, byteLength);
	aMapDataToFill.myRawDataSize = byteLength;

	char** rawData(&aMapDataToFill.myRawData);

	mapFile.read(*rawData, byteLength);
	mapFile.close();

	long long dataIndex(0);

	if (GetLevelInfo(*rawData, dataIndex, byteLength, aMapDataToFill.myLevelInfo) == false)
	{
		return false;
	}

	{
		const int& rootAmountOfObjects(aMapDataToFill.myLevelInfo.GetRootAmountOfObjects());
		CMapDataObject* mapDataObjects(sce_newArray(CMapDataObject, rootAmountOfObjects));
		aMapDataToFill.myObjects.SetRawData(mapDataObjects, rootAmountOfObjects);
	}

	unsigned int objectIndex(0);
	while (NEXT_IS_TEXT_LOADMAP("OBJECT") == true)
	{
		if (GetMapDataObject(*rawData, dataIndex, byteLength, aMapDataToFill.myObjects[objectIndex++]) == false)
		{
			return false;
		}
	}

	return (dataIndex == byteLength);
}

bool OpenFile(const char* aFilePath, std::ifstream& aFileStream, std::ios_base::openmode aOpenMode)
{
	if (aFilePath == nullptr)
	{
		return false;
	}

	aFileStream.open(aFilePath, aOpenMode);

	return aFileStream.good();
}

bool GetLevelInfo(char* aData, long long& aDataIndex, const long long& aDataIndexMax, CMapData::SLevelInfo& aLevelInfo)
{
	if (NEXT_IS_TEXT_OTHER("LEVELINFO") == false)
	{
		return false;
	}

	if (NEXT_IS_TEXT_OTHER("NAVMESH") == true)
	{
		if (GetNavMesh(aData, aDataIndex, aDataIndexMax, aLevelInfo.myNavMesh) == false)
		{
			return false;
		}
	}

	constexpr long long sizeOfMapSize(sizeof(CMapData::SLevelInfo::SMapSize));
	ALLOWED_TO_READ(aDataIndex, aDataIndexMax, sizeOfMapSize);

	aLevelInfo.myMapSize = (CMapData::SLevelInfo::SMapSize*)(&aData[aDataIndex]);

	aDataIndex += sizeOfMapSize;

	if (GetInt(aData, aDataIndex, aDataIndexMax, &aLevelInfo.myRootAmountOfObjects) == false)
	{
		return false;
	}

	if (GetInt(aData, aDataIndex, aDataIndexMax, &aLevelInfo.myTotalAmountOfObjects) == false)
	{
		return false;
	}

	return true;
}

bool GetNavMesh(char* aData, long long& aDataIndex, const long long& aDataIndexMax, CMapData::SLevelInfo::SNavMesh*& aNavMesh)
{
	int* amountToReadInBytes(nullptr);
	if (GetInt(aData, aDataIndex, aDataIndexMax, &amountToReadInBytes) == false)
	{
		return false;
	}

	ALLOWED_TO_READ(aDataIndex, aDataIndexMax, static_cast<long long>(*amountToReadInBytes));

	aNavMesh = (CMapData::SLevelInfo::SNavMesh*)(&aData[aDataIndex]);

	constexpr unsigned long long firstOffset(44);
	const unsigned long long secondOffset(firstOffset + aNavMesh->myIndices.mySize * sizeOfInt);
	const unsigned long long thirdOffset(secondOffset + aNavMesh->myVertices.mySize * sizeOfVector3f);

	aNavMesh->myIndices.myData = (int*)(&aData[aDataIndex + firstOffset]);
	aNavMesh->myVertices.myData = (CU::Vector3f*)(&aData[aDataIndex + secondOffset]);
	aNavMesh->myTriangleTypes.myData = (CMapData::SLevelInfo::SNavMesh::EAreaType*)(&aData[aDataIndex + thirdOffset]);

	aDataIndex += static_cast<long long>(*amountToReadInBytes);

	return true;
}

bool GetMapDataObject(char* aData, long long& aDataIndex, const long long& aDataIndexMax, CMapDataObject& aMapDataObject)
{
	CMapDataObject& newObject(aMapDataObject);

	if (GetIntString(aData, aDataIndex, aDataIndexMax, newObject.myName) == false)
	{
		return false;
	}
	if (GetInt(aData, aDataIndex, aDataIndexMax, &newObject.myInstanceID) == false)
	{
		return false;
	}
	if (GetIntString(aData, aDataIndex, aDataIndexMax, newObject.myTag) == false)
	{
		return false;
	}
	if (GetInt(aData, aDataIndex, aDataIndexMax, &newObject.myIsStatic) == false)
	{
		return false;
	}

	if (NEXT_IS_TEXT_OTHER("TRANSFORM") == false)
	{
		return false;
	}
	
	if (GetTransform(aData, aDataIndex, aDataIndexMax, newObject.myTransform) == false)
	{
		return false;
	}

	if (NEXT_IS_TEXT_OTHER("MESHNAME") == true)
	{
		if (GetIntString(aData, aDataIndex, aDataIndexMax, newObject.myMeshName) == false)
		{
			return false;
		}
	}

	if (NEXT_IS_TEXT_OTHER("MATERIALNAME") == true)
	{
		if (GetIntString(aData, aDataIndex, aDataIndexMax, newObject.myMaterialName) == false)
		{
			return false;
		}
	}

	if (NEXT_IS_TEXT_OTHER("MODELSIZE") == true)
	{
		if (GetVector3(aData, aDataIndex, aDataIndexMax, &newObject.myModelSize) == false)
		{
			return false;
		}
	}

	bool wroteSomething(true);
	while (wroteSomething == true)
	{
		wroteSomething = false;

		if (NEXT_IS_TEXT_OTHER(STOP_THIS) == true)
		{
			// Do nothing
		}
		else if (NEXT_IS_TEXT_OTHER("SCRIPT") == true)
		{
			if (GetScript(aData, aDataIndex, aDataIndexMax, newObject.myScript) == false)
			{
				return false;
			}
			wroteSomething = true;
		}
		else if (NEXT_IS_TEXT_OTHER("BOXCOLLIDER") == true)
		{
			if (GetBoxCollider(aData, aDataIndex, aDataIndexMax, newObject.myBoxCollider) == false)
			{
				return false;
			}
			wroteSomething = true;
		}
		else if (NEXT_IS_TEXT_OTHER("LIGHT") == true)
		{
			if (GetLights(aData, aDataIndex, aDataIndexMax, newObject.myLights) == false)
			{
				return false;
			}
			wroteSomething = true;
		}
		else if (NEXT_IS_TEXT_OTHER("PARTICLE") == true)
		{
			if (GetParticle(aData, aDataIndex, aDataIndexMax, newObject.myParticleName) == false)
			{
				return false;
			}
			wroteSomething = true;
		}
		else if (NEXT_IS_TEXT_OTHER("CHILD") == true)
		{
			if (GetChildren(aData, aDataIndex, aDataIndexMax, newObject) == false)
			{
				return false;
			}
			wroteSomething = true;
		}
	}

	return true;
}

bool GetChildren(char* aData, long long& aDataIndex, const long long& aDataIndexMax, CMapDataObject& aMapDataObject)
{
	int* amountOfChildren(nullptr);
	if (GetInt(aData, aDataIndex, aDataIndexMax, &amountOfChildren) == false)
	{
		return false;
	}

	{
		CMapDataObject* mapDataObjects(sce_newArray(CMapDataObject, *amountOfChildren));
		aMapDataObject.myChildren.SetRawData(mapDataObjects, static_cast<unsigned int>(*amountOfChildren));
	}

	for (unsigned int childIndex = 0; childIndex < aMapDataObject.myChildren.Size(); ++childIndex)
	{
		if ((NEXT_IS_TEXT_OTHER("OBJECT") && GetMapDataObject(aData, aDataIndex, aDataIndexMax, aMapDataObject.myChildren[childIndex])) == false)
		{
			return false;
		}
	}

	return true;
}

bool NextIsText(char* aData, long long& aDataIndex, const long long& aDataIndexMax, const char* aText)
{
	const std::string textToCompare(aText);
	const long long sizeOfText(textToCompare.size());

	ALLOWED_TO_READ(aDataIndex, aDataIndexMax, sizeOfText);

	if (std::string(&aData[aDataIndex], sizeOfText) == textToCompare)
	{
		aDataIndex += sizeOfText + 1;
		return true;
	}

	return false;
}

bool GetTransform(char* aData, long long& aDataIndex, const long long& aDataIndexMax, CMapDataObject::STransform*& aTransform)
{
	const long long amountOfVectorsInTransform(3);
	const long long amountOfFloatsInVector(3);
	const long long amountToReadInBytes(amountOfVectorsInTransform * (amountOfFloatsInVector * sizeOfFloat));
	ALLOWED_TO_READ(aDataIndex, aDataIndexMax, amountToReadInBytes);

	aTransform = (CMapDataObject::STransform*)(&aData[aDataIndex]);

	aDataIndex += amountToReadInBytes;

	return true;
}

bool GetScript(char* aData, long long& aDataIndex, const long long& aDataIndexMax, CMapDataObject::SScriptData& aScriptToFill)
{
	if (GetIntString(aData, aDataIndex, aDataIndexMax, aScriptToFill.myScriptPath) == false)
	{
		return false;
	}

	if (GetInt(aData, aDataIndex, aDataIndexMax, &aScriptToFill.myAmountOfIDVariables) == false)
	{
		return false;
	}

	constexpr long long variableSize(sizeof(std::remove_pointer_t<decltype(aScriptToFill.myIDVariables)>));
	long long amountToReadInBytesVariables(variableSize * *aScriptToFill.myAmountOfIDVariables);
	ALLOWED_TO_READ(aDataIndex, aDataIndexMax, amountToReadInBytesVariables);

	aScriptToFill.myIDVariables = (decltype(aScriptToFill.myIDVariables))(&aData[aDataIndex]);

	aDataIndex += amountToReadInBytesVariables;

	for (auto variableIndex(0); variableIndex < *aScriptToFill.myAmountOfIDVariables; ++variableIndex)
	{
		auto& currentVariableData(aScriptToFill.myIDVariables[variableIndex]);

		if (GetIntString(aData, aDataIndex, aDataIndexMax, currentVariableData.myName) == false)
		{
			return false;
		}

		constexpr long long pairSize(sizeof(std::remove_pointer_t<decltype(aScriptToFill.myIDVariables->myPair)>));
		constexpr long long amountToReadInBytesPair(pairSize);

		ALLOWED_TO_READ(aDataIndex, aDataIndexMax, amountToReadInBytesPair);

		currentVariableData.myPair = (decltype(aScriptToFill.myIDVariables->myPair))(&aData[aDataIndex]);

		aDataIndex += amountToReadInBytesPair;
	}

	if (GetInt(aData, aDataIndex, aDataIndexMax, &aScriptToFill.myAmountOfNumberVariables) == false)
	{
		return false;
	}

	constexpr long long variableNumberSize(sizeof(std::remove_pointer_t<decltype(aScriptToFill.myNumberVariables)>));
	amountToReadInBytesVariables = variableNumberSize * *aScriptToFill.myAmountOfNumberVariables;
	ALLOWED_TO_READ(aDataIndex, aDataIndexMax, amountToReadInBytesVariables);

	aScriptToFill.myNumberVariables = (decltype(aScriptToFill.myNumberVariables))(&aData[aDataIndex]);

	aDataIndex += amountToReadInBytesVariables;

	for (auto variableIndex(0); variableIndex < *aScriptToFill.myAmountOfNumberVariables; ++variableIndex)
	{
		auto& currentVariableData(aScriptToFill.myNumberVariables[variableIndex]);

		if (GetIntString(aData, aDataIndex, aDataIndexMax, currentVariableData.myName) == false)
		{
			return false;
		}

		constexpr long long pairSize(sizeof(std::remove_pointer_t<decltype(aScriptToFill.myNumberVariables->myPair)>));
		constexpr long long amountToReadInBytesPair(pairSize);

		ALLOWED_TO_READ(aDataIndex, aDataIndexMax, amountToReadInBytesPair);

		currentVariableData.myPair = (decltype(aScriptToFill.myNumberVariables->myPair))(&aData[aDataIndex]);

		aDataIndex += amountToReadInBytesPair;
	}

	if (GetInt(aData, aDataIndex, aDataIndexMax, &aScriptToFill.myAmountOfStringVariables) == false)
	{
		return false;
	}

	constexpr long long variableStringSize(sizeof(std::remove_pointer_t<decltype(aScriptToFill.myStringVariables)>));
	amountToReadInBytesVariables = variableStringSize * *aScriptToFill.myAmountOfStringVariables;
	ALLOWED_TO_READ(aDataIndex, aDataIndexMax, amountToReadInBytesVariables);

	aScriptToFill.myStringVariables = (decltype(aScriptToFill.myStringVariables))(&aData[aDataIndex]);

	aDataIndex += amountToReadInBytesVariables;

	for (auto variableIndex(0); variableIndex < *aScriptToFill.myAmountOfStringVariables; ++variableIndex)
	{
		auto& currentVariableData(aScriptToFill.myStringVariables[variableIndex]);

		if (GetIntString(aData, aDataIndex, aDataIndexMax, currentVariableData.myName) == false)
		{
			return false;
		}

		ALLOWED_TO_READ(aDataIndex, aDataIndexMax, 8);

		currentVariableData.myPair = (decltype(aScriptToFill.myStringVariables->myPair))(&aData[aDataIndex]);

		aDataIndex += 4;

		int amountToRead(*(int*)(&aData[aDataIndex]));

		aDataIndex += 4;

		ALLOWED_TO_READ(aDataIndex, aDataIndexMax, (sizeOfPointer + (amountToRead + 1)));

		aDataIndex += sizeOfPointer;

		currentVariableData.myPair->myValue = (const char*)(&aData[aDataIndex]);

		aDataIndex += (amountToRead + 1);
	}

	return true;
}

bool GetBoxCollider(char* aData, long long& aDataIndex, const long long& aDataIndexMax, CMapDataObject::SBoxCollider*& aBoxColliderToFill)
{
	constexpr long long amountToReadInBytes(sizeof(CMapDataObject::SBoxCollider));

	ALLOWED_TO_READ(aDataIndex, aDataIndexMax, amountToReadInBytes);

	aBoxColliderToFill = (CMapDataObject::SBoxCollider*)(&aData[aDataIndex]);

	aDataIndex += amountToReadInBytes;

	return true;
}

bool GetLights(char* aData, long long& aDataIndex, const long long& aDataIndexMax, CU::GrowingArray<CMapDataObject::SLight, unsigned int>& aLightsToFill)
{
	int* amountOfLights(nullptr);
	if (GetInt(aData, aDataIndex, aDataIndexMax, &amountOfLights) == false)
	{
		return false;
	}

	if (*amountOfLights > 0)
	{
		{
			CMapDataObject::SLight* lights(sce_newArray(CMapDataObject::SLight, *amountOfLights));
			aLightsToFill.SetRawData(lights, static_cast<unsigned int>(*amountOfLights));
		}

		for (unsigned int lightIndex = 0; lightIndex < aLightsToFill.Size(); ++lightIndex)
		{
			CMapDataObject::SLight& newLight(aLightsToFill[lightIndex]);

			if (GetChar(aData, aDataIndex, aDataIndexMax, (char**)(&newLight.myLightType)) == false)
			{
				return false;
			}

			const long long floatsInVector4(4);
			const long long amountToReadInBytes(sizeOfFloat * floatsInVector4);
			ALLOWED_TO_READ(aDataIndex, aDataIndexMax, amountToReadInBytes);

			newLight.myColor = ((CU::Vector4f*)(&aData[aDataIndex]));

			aDataIndex += amountToReadInBytes;

			if (GetFloat(aData, aDataIndex, aDataIndexMax, &newLight.myIntensity) == false)
			{
				return false;
			}

			if (*newLight.myLightType == CMapDataObject::SLight::eLightType::POINT)
			{
				const long long rangeSizeInBytes(sizeOfFloat);
				const long long pointLightSizeInBytes(rangeSizeInBytes);
				ALLOWED_TO_READ(aDataIndex, aDataIndexMax, pointLightSizeInBytes);

				newLight.myPointLightData = ((CMapDataObject::SLight::SPointLight*)(&aData[aDataIndex]));

				aDataIndex += pointLightSizeInBytes;
			}
			else if (*newLight.myLightType == CMapDataObject::SLight::eLightType::DIRECTIONAL)
			{
				const long long floatsInVector3(3);
				const long long directionSizeInBytes(sizeOfFloat * floatsInVector3);
				const long long directionalLightSizeInBytes(directionSizeInBytes);
				ALLOWED_TO_READ(aDataIndex, aDataIndexMax, directionalLightSizeInBytes);

				newLight.myDirectionalLightData = ((CMapDataObject::SLight::SDirectionalLight*)(&aData[aDataIndex]));

				aDataIndex += directionalLightSizeInBytes;
			}
			else if (*newLight.myLightType == CMapDataObject::SLight::eLightType::SPOT)
			{
				const long long rangeSizeInBytes(sizeOfFloat);
				const long long angleSizeInBytes(sizeOfFloat);
				const long long floatsInVector3(3);
				const long long directionSizeInBytes(sizeOfFloat * floatsInVector3);
				const long long spotLightSizeInBytes(directionSizeInBytes + rangeSizeInBytes + angleSizeInBytes);
				ALLOWED_TO_READ(aDataIndex, aDataIndexMax, spotLightSizeInBytes);

				newLight.mySpotLightData = ((CMapDataObject::SLight::SSpotLight*)(&aData[aDataIndex]));

				aDataIndex += spotLightSizeInBytes;
			}
		}
	}
	else
	{
		return false;
	}

	return true;
}

bool GetParticle(char* aData, long long& aDataIndex, const long long& aDataIndexMax, const char*& aParticlePath)
{
	if (GetIntString(aData, aDataIndex, aDataIndexMax, aParticlePath) == false)
	{
		return false;
	}

	return true;
}

bool GetInt(char* aData, long long& aDataIndex, const long long& aDataIndexMax, int** aIntToFill)
{
	ALLOWED_TO_READ(aDataIndex, aDataIndexMax, sizeOfInt);

	*aIntToFill = (int*)(&aData[aDataIndex]);

	aDataIndex += sizeOfInt;

	return true;
}

bool GetString(char* aData, long long& aDataIndex, const long long& aDataIndexMax, const char*& aStringToFill, const int aSizeToGet)
{
	ALLOWED_TO_READ(aDataIndex, aDataIndexMax, aSizeToGet);

	aStringToFill = &aData[aDataIndex];

	aDataIndex += aSizeToGet + 1;

	return true;
}

bool GetIntString(char* aData, long long& aDataIndex, const long long& aDataIndexMax, const char*& aNameToFill)
{
	int* sizeOfName(nullptr);
	if (GetInt(aData, aDataIndex, aDataIndexMax, &sizeOfName) == false)
	{
		return false;
	}

	if (GetString(aData, aDataIndex, aDataIndexMax, aNameToFill, *sizeOfName) == false)
	{
		return false;
	}

	return true;
}

bool GetChar(char* aData, long long& aDataIndex, const long long& aDataIndexMax, char** aCharToFill)
{
	ALLOWED_TO_READ(aDataIndex, aDataIndexMax, sizeOfChar);

	*aCharToFill = (char*)(&aData[aDataIndex]);

	aDataIndex += sizeOfChar;

	return true;
}

bool GetFloat(char* aData, long long& aDataIndex, const long long& aDataIndexMax, float** aFloatToFill)
{
	ALLOWED_TO_READ(aDataIndex, aDataIndexMax, sizeOfFloat);

	*aFloatToFill = (float*)(&aData[aDataIndex]);

	aDataIndex += sizeOfFloat;

	return true;
}

bool GetBool(char * aData, long long & aDataIndex, const long long & aDataIndexMax, bool ** aBoolToFill)
{
	ALLOWED_TO_READ(aDataIndex, aDataIndexMax, sizeOfBool);

	*aBoolToFill = (bool*)(&aData[aDataIndex]);

	aDataIndex += sizeOfBool;

	return true;
}

bool GetVector3(char* aData, long long& aDataIndex, const long long& aDataIndexMax, CU::Vector3f** aVectorToFill)
{
	ALLOWED_TO_READ(aDataIndex, aDataIndexMax, sizeOfVector3f);

	*aVectorToFill = (CU::Vector3f*)(&aData[aDataIndex]);

	aDataIndex += sizeOfVector3f;

	return true;
}

bool MovePointerSize(long long& aDataIndex, const long long& aDataIndexMax)
{
	ALLOWED_TO_READ(aDataIndex, aDataIndexMax, sizeOfPointer);

	aDataIndex += sizeOfPointer;

	return true;
}

bool AllowedToReadData(const long long& aDataIndex, const long long& aDataIndexMax, const long long& aSize)
{
	return ((aDataIndex + aSize) <= aDataIndexMax);
}
