#include "stdafx.h"
#include "MapData.h"

CMapDataObject::CMapDataObject()
	: myName(nullptr)
	, myTransform(nullptr)
	, myTag(nullptr)
	, myInstanceID(nullptr)
	, myMeshName(nullptr)
	, myMaterialName(nullptr)
	, myModelSize(nullptr)
	, myBoxCollider(nullptr)
	, myIsStatic(nullptr)
	, myParticleName(nullptr)
{
}

const char* const CMapDataObject::GetName() const
{
	return myName;
}

const CMapDataObject::STransform* const CMapDataObject::GetTransform() const
{
	return myTransform;
}

const CU::GrowingArray<CMapDataObject, unsigned int>& CMapDataObject::GetChildren() const
{
	return myChildren;
}

const CU::GrowingArray<CMapDataObject::SLight, unsigned int>& CMapDataObject::GetLights() const
{
	return myLights;
}

const char* const CMapDataObject::GetTag() const
{
	return myTag;
}

int CMapDataObject::GetInstanceID() const
{
	return *myInstanceID;
}

const char* const CMapDataObject::GetMeshName() const
{
	return myMeshName;
}

const char * const CMapDataObject::GetMaterialName() const
{
	return myMaterialName;
}

const CU::Vector3f* const CMapDataObject::GetModelSize() const
{
	return myModelSize;
}

const CMapDataObject::SScriptData& CMapDataObject::GetScript() const
{
	return myScript;
}

const CMapDataObject::SBoxCollider* CMapDataObject::GetBoxCollider() const
{
	return myBoxCollider;
}

const int* CMapDataObject::GetIsStatic() const
{
	return myIsStatic;
}

const char* const CMapDataObject::GetParticleName() const
{
	return myParticleName;
}

CMapData::CMapData()
	: myRawData(nullptr)
	, myRawDataSize(0)
{
}

void CMapData::Destroy()
{
	if (myRawData != nullptr)
	{
		for (unsigned int objectIndex = 0; objectIndex < myObjects.Size(); ++objectIndex)
		{
			CMapDataObject& currentObject(myObjects[objectIndex]);
			DestroyDataObject(currentObject);
		}

		auto objectsDataPointer(myObjects.GetRawData());
		sce_delete(objectsDataPointer);

		sce_delete(myRawData);
		myRawDataSize = 0;
	}
}

const CMapData::SLevelInfo& CMapData::GetLevelInfo() const
{
	return myLevelInfo;
}

const CU::GrowingArray<CMapDataObject, unsigned int>& CMapData::GetObjects() const
{
	return myObjects;
}

void CMapData::DestroyDataObject(CMapDataObject& aMapDataObject)
{
	auto lightDataPointer(aMapDataObject.myLights.GetRawData());
	sce_delete(lightDataPointer);

	for (unsigned int childObjectIndex = 0; childObjectIndex < aMapDataObject.myChildren.Size(); ++childObjectIndex)
	{
		DestroyDataObject(aMapDataObject.myChildren[childObjectIndex]);
	}

	auto childrenDataPointer(aMapDataObject.myChildren.GetRawData());
	sce_delete(childrenDataPointer);
}

const CMapData::SLevelInfo::SNavMesh* const CMapData::SLevelInfo::GetNavMesh() const
{
	return myNavMesh;
}

const int& CMapData::SLevelInfo::GetRootAmountOfObjects() const
{
	return *myRootAmountOfObjects;
}

const int& CMapData::SLevelInfo::GetTotalAmountOfObjects() const
{
	return *myTotalAmountOfObjects;
}

const CMapData::SLevelInfo::SMapSize& CMapData::SLevelInfo::GetMapSize() const
{
	return *myMapSize;
}

bool CMapDataObject::SScriptData::IsValid() const
{
	return (myScriptPath != nullptr);
}

const char* const CMapDataObject::SScriptData::GetPath() const
{
	return myScriptPath;
}

const int& CMapDataObject::SScriptData::GetAmountOfIDVariables() const
{
	return *myAmountOfIDVariables;
}

const int& CMapDataObject::SScriptData::GetAmountOfNumberVariables() const
{
	return *myAmountOfNumberVariables;
}

const int& CMapDataObject::SScriptData::GetAmountOfStringVariables() const
{
	return *myAmountOfStringVariables;
}

const CMapDataObject::SScriptData::SVariableData<int>* const CMapDataObject::SScriptData::GetIDVariableDatas() const
{
	return myIDVariables;
}

const CMapDataObject::SScriptData::SVariableData<int>* const CMapDataObject::SScriptData::GetNumberVariableDatas() const
{
	return myNumberVariables;
}

const CMapDataObject::SScriptData::SVariableData<const char*>* const CMapDataObject::SScriptData::GetStringVariableDatas() const
{
	return myStringVariables;
}
