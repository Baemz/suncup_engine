#include "stdafx.h"
#include "MouseInterface.h"
#include "..\EngineCore\Logger\Logger.h"
#include "..\EngineCore\MemoryPool\MemoryPool.h"
#include "../CommonUtilities/Matrix.h"
#include "..\EngineCore\EventSystem\EventStructs.h"
#include "..\GraphicsEngine\GraphicsEngineInterface.h"


CMouseInterface* CMouseInterface::ourInstance = nullptr;

CMouseInterface::CMouseInterface(EManager* aEntityManager, const SEntityHandle& aCameraHandle, const CU::Matrix44f* aCameraProj)
	: myEntityManager(aEntityManager)
	, myCameraHandle(aCameraHandle)
	, myCameraProjection(aCameraProj)
{
	AttachToEventReceiving(Event::ESubscribedEvents(Event::eMouseInput));
}

void CMouseInterface::ReceiveEvent(const Event::SEvent& aEvent)
{
	if (aEvent.myType == Event::EEventType::eMouseInput)
	{
		const Event::SMouseDataEvent* mouseData(reinterpret_cast<const Event::SMouseDataEvent*>(&aEvent));
		CalculateRay(mouseData);
	}
}

CMouseInterface::~CMouseInterface()
{
	DetachFromEventReceiving();
}

bool CMouseInterface::Create(EManager* aEntityManager, const SEntityHandle& aCameraHandle, const CU::Matrix44f* aCameraProj)
{

	if (ourInstance != nullptr)
	{
		GAME_LOG("FAIL! Failed to create new instance of MouseInterface because it was already created.");
		return false;
	}

	ourInstance = sce_new(CMouseInterface(aEntityManager, aCameraHandle, aCameraProj));

	if (ourInstance == nullptr)
	{
		GAME_LOG("FAIL! Failed to allocate memory for MouseInterface.");
		return false;
	}

	return true;
}

void CMouseInterface::Destroy()
{
	sce_delete(ourInstance);
}

CMouseInterface* CMouseInterface::Get()
{
	return ourInstance;
}

const CU::Collision::Ray& CMouseInterface::GetMouseToNavmeshRay()
{
	return myMouseRay;
}

void CMouseInterface::CalculateRay(const Event::SMouseDataEvent* aMouseData)
{
	const CU::Matrix44f& camOrientation(myEntityManager->GetComponent<CompCameraInstance>(myEntityManager->GetEntityIndex(myCameraHandle)).myInstance.GetOrientation());
	CU::Vector3f cameraLook = GetCameraLook({ static_cast<float>(aMouseData->x), static_cast<float>(aMouseData->y) });

	const CU::Collision::Ray cursorWorldRay(camOrientation.GetPosition(), cameraLook);
	myMouseRay = cursorWorldRay;

	//if (CGameLogicFunctions::GetPlayerUniqueIndex() == EntityHandleINVALID.myUniqueIndex)
	//{
	//	return;
	//}
	//
	//const CU::Matrix44f& camOrientation(myEntityManager->GetComponent<CompCameraInstance>(myCameraHandle).myInstance.GetOrientation());
	//const CU::Vector3f cameraLook = GetCameraLook({ static_cast<float>(aMouseData->x), static_cast<float>(aMouseData->y) });
	//
	//CU::Collision::Ray cursorWorldRay(camOrientation.GetPosition(), cameraLook);
	//
	//constexpr float playerPlaneMarginY(0.1f);
	//
	//const CompTransform& playerTransform(myEntityManager->GetComponent<CompTransform>(SEntityHandle(CGameLogicFunctions::GetPlayerUniqueIndex())));
	//const CU::Collision::Plane playerPlane(playerTransform.myPosition - CU::Vector3f(0.0f, playerPlaneMarginY, 0.0f), { 0.0f, 1.0f, 0.0f });
	//
	//CU::Vector3f playerPlaneIntersectionPoint;
	//CU::Collision::IntersectionRayPlane(cursorWorldRay, playerPlane, playerPlaneIntersectionPoint);
	//
	//cursorWorldRay = CU::Collision::Ray(playerPlaneIntersectionPoint, -cameraLook);
	//
	//myMouseRay = cursorWorldRay;

}

const CU::Vector3f CMouseInterface::GetCameraLook(const CU::Vector2f aMousePixelPos)
{
	const CU::Vector2f resolution = sce::gfx::CGraphicsEngineInterface::GetResolution();
	const CU::Matrix44f& camOrientation(myEntityManager->GetComponent<CompCameraInstance>(myEntityManager->GetEntityIndex(myCameraHandle)).myInstance.GetOrientation());

	float normalizedMouseX = (((static_cast<float>(aMousePixelPos.x) * 2.0f) / resolution.x) - 1.0f);
	float normalizedMouseY = (((static_cast<float>(aMousePixelPos.y) * 2.0f) / resolution.y) - 1.0f);

	CU::Vector2f projPos = { normalizedMouseX, normalizedMouseY };
	projPos.x /= (*myCameraProjection)[0];
	projPos.y /= (*myCameraProjection)[5];

	CU::Vector3f screenPosition =
	{
		(projPos.x * camOrientation[0]) + (-projPos.y * camOrientation[4]) + camOrientation[8],
		(projPos.x * camOrientation[1]) + (-projPos.y * camOrientation[5]) + camOrientation[9],
		(projPos.x * camOrientation[2]) + (-projPos.y * camOrientation[6]) + camOrientation[10]
	};

	return screenPosition.GetNormalized();
}
