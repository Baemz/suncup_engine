#pragma once
#include "../CommonUtilities/Intersection.h"
#include "../Game/EntitySystemLink/EntitySystemLink.h"
#include "../EntitySystem/Handle.h"
#include "../EngineCore/EventSystem/EventReceiver.h"


class CMouseInterface : public sce::CEventReceiver
{
public:
	~CMouseInterface();

	static bool Create(EManager* aEntityManager, const SEntityHandle& aCameraHandle, const CU::Matrix44f* aCameraProj);
	static void Destroy();
	static CMouseInterface* Get();
	const CU::Collision::Ray& GetMouseToNavmeshRay();

private:
	CMouseInterface(EManager* aEntityManager, const SEntityHandle& aCameraHandle, const CU::Matrix44f* aCameraProj);

	void ReceiveEvent(const Event::SEvent& aEvent) override;
	void CalculateRay(const Event::SMouseDataEvent* aMouseData);
	const CU::Vector3f GetCameraLook(const CU::Vector2f aMousePixelPos);
	static CMouseInterface* ourInstance;
	CU::Collision::Ray myMouseRay;
	EManager* myEntityManager;
	SEntityHandle myCameraHandle;
	const CU::Matrix44f* myCameraProjection;
};
