#include "stdafx.h"
#include "ScriptEventManager_MoveObject.h"
#include "ScriptEventManager.h"
#include "GameSystems\ExposedFunctions\GameLogicFunctions.h"
#include <shared_mutex>

#define CHECK_IF_STATE_IS_REMOVED(aStateIndex, aReturnValue)																\
std::shared_lock<std::shared_mutex> sharedLock(aScriptEventManager.myStatesToRemoveMutex);									\
if (aScriptEventManager.myStatesToRemove.Find(aStateIndex) != CU::GrowingArray<CScriptManager::StateIndexType>::FoundNone)	\
return aReturnValue;

bool CScriptEventManager_MoveObject::InitFunctions(CScriptEventManager& aScriptEventManager)
{
	CScriptManager& myScriptManager(aScriptEventManager.myScriptManager);

	std::function<void(const CScriptManager::StateIndexType, float, float, float)> teleportToPositionBind(
		[&aScriptEventManager](const CScriptManager::StateIndexType aStateIndex, float aX, float aY, float aZ)
	{
		CHECK_IF_STATE_IS_REMOVED(aStateIndex, ;);
		auto index(aScriptEventManager.GetEntityUniqueIndex(aStateIndex));
		CGameLogicFunctions::TeleportToPosition(index, CU::Vector3f(aX, aY, aZ));
	});
	myScriptManager.RegisterFunctionCallWithStateIndex("TeleportToPosition", teleportToPositionBind
		, "Number, Number, Number"
		, "Sets the entities position to ( [aX], [aY], [aZ] ).");

	std::function<void(const CScriptManager::StateIndexType, float, float, float)> arriveAtPositionBind(
		[&aScriptEventManager](const CScriptManager::StateIndexType aStateIndex, float aX, float aY, float aZ)
	{
		CHECK_IF_STATE_IS_REMOVED(aStateIndex, ;);
		auto index(aScriptEventManager.GetEntityUniqueIndex(aStateIndex));
		CGameLogicFunctions::ArriveAtPosition(index, CU::Vector3f(aX, aY, aZ));
	});
	myScriptManager.RegisterFunctionCallWithStateIndex("ArriveAtPosition", arriveAtPositionBind
		, "Number, Number, Number"
		, "Move entity to ( [aX], [aY], [aZ] ) with the entities speed.");

	std::function<void(const CScriptManager::StateIndexType, UniqueIndexType)> teleportToEntityBind(
		[&aScriptEventManager](const CScriptManager::StateIndexType aStateIndex, UniqueIndexType aEntityUniqueIndex)
	{
		CHECK_IF_STATE_IS_REMOVED(aStateIndex, ;);
		auto index(aScriptEventManager.GetEntityUniqueIndex(aStateIndex));
		CGameLogicFunctions::TeleportToEntity(index, aEntityUniqueIndex);
	});
	myScriptManager.RegisterFunctionCallWithStateIndex("TeleportToEntity", teleportToEntityBind
		, "Number"
		, "Sets the entities position to the [target]'s position.");

	std::function<void(const CScriptManager::StateIndexType, UniqueIndexType)> arriveAtEntityBind(
		[&aScriptEventManager](const CScriptManager::StateIndexType aStateIndex, UniqueIndexType aEntityUniqueIndex)
	{
		CHECK_IF_STATE_IS_REMOVED(aStateIndex, ;);
		auto index(aScriptEventManager.GetEntityUniqueIndex(aStateIndex));
		CGameLogicFunctions::ArriveAtEntity(index, aEntityUniqueIndex);
	});
	myScriptManager.RegisterFunctionCallWithStateIndex("ArriveAtEntity", arriveAtEntityBind
		, "Number"
		, "Moves entity to [target] with the entities speed.");

	std::function<void(const CScriptManager::StateIndexType)> startWanderingBind(
		[&aScriptEventManager](const CScriptManager::StateIndexType aStateIndex)
	{
		CHECK_IF_STATE_IS_REMOVED(aStateIndex, ;);
		auto index(aScriptEventManager.GetEntityUniqueIndex(aStateIndex));
		CGameLogicFunctions::Wander(index);
	});
	myScriptManager.RegisterFunctionCallWithStateIndex("Wander", startWanderingBind
		, ""
		, "Let's the entity roam about freely");

	std::function<void(const CScriptManager::StateIndexType)> stopWanderingBind(
		[&aScriptEventManager](const CScriptManager::StateIndexType aStateIndex)
	{
		CHECK_IF_STATE_IS_REMOVED(aStateIndex, ;);
		auto index(aScriptEventManager.GetEntityUniqueIndex(aStateIndex));
		CGameLogicFunctions::StopWander(index);
	});
	myScriptManager.RegisterFunctionCallWithStateIndex("StopWander", stopWanderingBind
		, ""
		, "Stops the entity roaming about freely");

	return true;
}
