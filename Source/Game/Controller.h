#pragma once
#include "ControllerInitData.h"
#include "../CommonUtilities/Vector3.h"
#include <string>
#include "../CommonUtilities/Quaternion.h"
#include "NavigationMesh/NavMeshStructs.h"
#include "../EntitySystem/Entity.h"

//The controller has once more lost control!
class CController
{
public:
	struct SInitNavigationData
	{
		SInitNavigationData()
			: myShouldInitializePath(false) { }

		CU::Vector3f myTargetPosition;
		bool myShouldInitializePath;
	};

	enum class ELoadState
	{
		Unloaded,
		Loading,
		Loaded
	};

	CController();
	CController(const SControllerInit& aInitData);
	~CController();

//Access is wild and heavy, for now.
//protected:
public:
	NavMesh::Path myCurrentPath;

	CU::Vector3f myTarget;
	CU::Vector3f myVelocity;
	CU::Vector3f mySteering;

	CU::Quaternion myTargetLookRotation;

	float myMaxSpeed;
	float myMaxForce;
	float myRotationSpeed;

	EController myControllerType;

	std::string myScriptFilePath;
	unsigned int myScriptStateIndex;
	UniqueIndexType* myScriptIndices;
	int* myScriptNumbers;
	std::string* myScriptStrings;
	ELoadState myLoaded;

	SInitNavigationData myInitNavigationData;
};