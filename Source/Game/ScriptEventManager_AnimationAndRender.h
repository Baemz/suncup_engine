#pragma once

class CScriptEventManager;

class CScriptEventManager_AnimationAndRender
{
public:
	static bool InitFunctions(CScriptEventManager& aScriptEventManager);

private:
	CScriptEventManager_AnimationAndRender() = delete;
	~CScriptEventManager_AnimationAndRender() = delete;

};
