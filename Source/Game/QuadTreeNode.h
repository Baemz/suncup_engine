#pragma once
#include "..\CommonUtilities\Vector.h"
#include "..\CommonUtilities\GrowingArray.h"
#include <limits>
#include "..\EntitySystem\Handle.h"
#include "NavigationMesh\NavMeshStructs.h"

#ifdef max
#pragma push_macro("max")
#undef max
#define MAX_UNDEFINED
#endif // max

struct QuadTreeData
{
	QuadTreeData() : myEntityUniqueIndex(EntityHandleINVALID.myUniqueIndex), myIsTrigger(false) {};

	// For QuadTree
	CU::Vector2f myCenterPosition;
	CU::Vector2f mySize;

	// User data
	UniqueIndexType myEntityUniqueIndex;
	bool myIsTrigger;

	// Navigation mesh data
	NavMesh::IndexType myTriangleIndex;
};

class QuadTreeNode
{
public:
	QuadTreeNode(const unsigned short aLoosenessFactor = 2);
	~QuadTreeNode();

	void Init(const unsigned short aMaxAmountOfObjectsInEachNode, const CU::Vector4f& aAABB);

	void AddObject(const QuadTreeData& aObject);
	bool GetObjects(CU::GrowingArray<QuadTreeData>& aArrayToFill, const bool aIgnoreChildren = false) const;

	void Clear();

	inline void ToggleShouldRenderNodes() { myShouldRenderNodes = !myShouldRenderNodes; }

	const CU::GrowingArray<QuadTreeNode*>& GetChildren() const;
	bool GetObjectsCloseToPoint(const CU::Vector2f& aPoint, CU::GrowingArray<QuadTreeData>& aArrayToFill) const;
	bool GetObjectsCloseToAABB(const CU::Vector4f& aAABB, CU::GrowingArray<QuadTreeData>& aArrayToFill) const;
	QuadTreeNode* GetNodeInside(const CU::Vector2f& aPoint);
	const QuadTreeNode* GetNodeInside(const CU::Vector2f& aPoint) const;
	const bool GetNodeListInside(const CU::Vector2f& aPoint, CU::GrowingArray<const QuadTreeNode*>& aArrayToFill) const;

	const CU::Vector4f& GetAABB() const;

	unsigned short GetLoosenessFactor() const;
	CU::Vector2f GetSize() const;

#ifndef _RETAIL
	void DebugRenderNodes();
	void DebugRenderNodes(const CU::Vector4f& aAffectedAABB);
	void DebugRenderObjects();
	void DebugRenderObjects(const CU::Vector4f& aAffectedAABB);
#else
	void DebugRenderNodes() {};
	void DebugRenderNodes(const CU::Vector4f&) {};
	void DebugRenderObjects() {};
	void DebugRenderObjects(const CU::Vector4f&) {};
#endif // _RETAIL

private:
#ifndef _RETAIL
	enum class eDebugRenderColor : unsigned char
	{
		START = 0,
		Red = 0,
		Orange,
		Yellow,
		Lime,
		Green,
		Cyan,
		LightBlue,
		DarkBlue,
		Magenta,
		MagentaPink,
		Pink,
		END,
	};

	void DebugRenderNodesImpl(const eDebugRenderColor& aDebugColor, const CU::Vector4f& aAffectedAABB, const float aRenderPositionY);
	void DebugRenderObjectsImpl(const eDebugRenderColor& aDebugColor, const CU::Vector4f& aAffectedAABB, const float aScaleY);
#endif // !_RETAIL

	static CU::Vector4f ConvertAABBWithLooseness(QuadTreeNode& aQuadTree, const CU::Vector4f& aAABB, const bool aInversed = false);

	void SetSize(const CU::Vector2f& aSize);

	void SetAABB(const CU::Vector4f& aAABB);
		
	bool PointFitsInsideAnyChildren(const CU::Vector2f& aPoint, CU::GrowingArray<unsigned short>& aChildIndex) const;
	bool PointFitsInside(const CU::Vector2f& aPoint) const;
	bool AABBFitsInsideAnyChildren(const CU::Vector4f& aAABB, CU::GrowingArray<unsigned short>& aChildIndex) const;
	bool AABBFitsInside(const CU::Vector4f& aAABB) const;
	bool ObjectFitsInside(const QuadTreeData& aObject) const;

	bool ObjectFitsInsideAChild(const QuadTreeData& aObject, unsigned short& aChildIndex) const;

	static const unsigned short ourAmountOfChildren;

	static const CU::Vector3f ourDebugColors[];

	CU::GrowingArray<QuadTreeNode*> myChildren;

	CU::GrowingArray<QuadTreeData> myObjects;

	CU::Vector4f myAABB;
	CU::Vector2f mySize;
	unsigned short myMaxAmountOfObjects;
	const unsigned short myLoosenessFactor;

	bool myShouldRenderNodes;

};

#ifdef MAX_UNDEFINED
#undef MAX_UNDEFINED
#pragma pop_macro("max")
#endif // MAX_UNDEFINED
