#pragma once
#include "SystemAbstract.h"
#include "../EngineCore/EventSystem/EventReceiver.h"
#include "../../EntitySystem/Handle.h"
#include "NavigationMesh/NavMeshStructs.h"

class CNavigationMesh;

class CControllerSystem : public CSystemAbstract<CControllerSystem>, public sce::CEventReceiver
{
public:
	CControllerSystem(EManager& aManager);
	~CControllerSystem() override = default;

	void Activate(const SEntityHandle& aCameraHandle, CNavigationMesh& aNavMesh);
	void Deactivate();

	//Update every signature with anything menu-related
	void Update(const float aDeltaTime, const CU::GrowingArray<const CU::GrowingArray<UniqueIndexType>*>& aEntities) override;

	void ReceiveEvent(const Event::SEvent& aEvent) override;

private:

	void UpdateModels(const CU::GrowingArray<UniqueIndexType>& aEntities, const float aDeltaTime);
	void UpdateParticles(const CU::GrowingArray<UniqueIndexType>& aEntities);
	void UpdateActiveAStarPaths(const CU::GrowingArray<UniqueIndexType>& aEntities);
	void UpdateCameraMovement(const float aDeltaTime);
	void CheckPlayerToAICollision(const CU::GrowingArray<UniqueIndexType>& aEntities);

	void SetPlayerPath(const CU::GrowingArray<const CU::GrowingArray<UniqueIndexType>*>& aEntities);
	void SetPlayerDashPosition();

	void CheckScreenToWorldRayCollision(const CU::Collision::Ray& aScreenToWorldRay, const CU::GrowingArray<UniqueIndexType>& aEntities);

	SEntityHandle myCameraHandle;
	CNavigationMesh* myNavMesh;
/*	NavMesh::Path myInputHandlePath;*/

	const CU::Matrix44f* myCameraProjection;

	static constexpr float TimeBetweenPathRecalculation = 0.1f;
	float myRecalculatePathTimer;

	bool mySetPlayerPath;
	Event::SMouseClickEvent myMouseClickMessage;

};