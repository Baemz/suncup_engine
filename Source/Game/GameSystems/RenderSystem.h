#pragma once
#include "SystemAbstract.h"

class CRenderSystem : public CSystemAbstract<CRenderSystem>
{
public:
	CRenderSystem(EManager& aManager) : CSystemAbstract<CRenderSystem>(aManager) { }

	//Load models etc.
	void Activate();
	void Deactivate();

	//Update every signature with anything render-related
	void Update(const float aDeltaTime, const CU::GrowingArray<const CU::GrowingArray<UniqueIndexType>*>& aEntities) override;

	void ActivateCanvasFade(const float aFadeTime);
	void ActivateCanvasFade(const float aFadeTime, std::function<void()> aOnEclipseCallback);
	void CancelCanvasFade();

	void SetBlackBars(const float aScreenspaceRadius);
	void CancelBlackBars();

	inline void ToggleRenderTriggers() { myRenderTriggers = !myRenderTriggers; }
	inline void ToggleShouldRenderObjectSpheres() { myRenderObjectSpheres = !myRenderObjectSpheres; }

	inline const bool IsFading() const { return myCanvasFadeTimer < myTotalCanvasFadeTime; }

private:
	void RenderModels(const float aDeltaTime, const CU::GrowingArray<UniqueIndexType>& aEntities);
	void RenderCamera(const CU::GrowingArray<UniqueIndexType>& aEntities);
	void RenderPointLights(const CU::GrowingArray<UniqueIndexType>& aEntities);
	void RenderSpotLights(const CU::GrowingArray<UniqueIndexType>& aEntities);
	void RenderDirectionalLights(const CU::GrowingArray<UniqueIndexType>& aEntities);
	void RenderParticles(const CU::GrowingArray<UniqueIndexType>& aEntities);
	void RenderHUDElements(const CU::GrowingArray<UniqueIndexType>& aEntities);

	void RenderTriggers(const CU::GrowingArray<UniqueIndexType>& aEntities);

	sce::gfx::CSprite* myBlackCanvas;

	static constexpr const char* myBlackCanvasFilepath = "Data/UI/ui_blackCanvas.dds";

	float myTotalCanvasFadeTime;
	float myCanvasFadeTimer;
	float myBlackBarsRadius;
	std::function<void()> myOnEclipseCallback;
	std::mutex myOnEclipseCallbackMutex;
	std::mutex myOnBarsDownMutex;

	bool myRenderTriggers;
	bool myRenderObjectSpheres;
};