#include "stdafx.h"
#include "GameSystems.h"
#include "GameSystems/GameSystemsInclude.h"
#include "../EngineCore/WorkerPool/WorkerPool.h"
#include "HealingCircleSystem.h"
#include "DamageZoneSystem.h"

#define ADD_AND_INIT(aSystem) Add<aSystem>(myEntityManager); InitSystem<aSystem>();

CSystemAbstractAbstract::SystemCounter CSystemAbstractAbstract::abstractSystemCount = 0;
CGameSystems* CGameSystems::ourInstance = nullptr;

void CGameSystems::QueueUpdateEntityUniqueIndices(const CU::GrowingArray<UniqueIndexType>* aEntities)
{
	myUpdateEntitiesQueued.Add(aEntities);
}

void CGameSystems::QueueRenderEntityUniqueIndices(const CU::GrowingArray<UniqueIndexType>* aEntities)
{
	myRenderEntitiesQueued.Add(aEntities);
}

void CGameSystems::QueuePostUpdateFunction(const PostUpdateFunction& aFunction)
{
	myPostUpdateFunctions.Add(aFunction);
}

void CGameSystems::RemoveQueuedUpdateEntities(const CU::GrowingArray<UniqueIndexType>* aEntities)
{
	myUpdateEntitiesQueued.RemoveCyclic(aEntities);
}

void CGameSystems::Create(EManager & aEntityManager)
{
	assert(ourInstance == nullptr && "Instance has already been created.");
	ourInstance = sce_new(CGameSystems(aEntityManager));
	ourInstance->Init();
}

void CGameSystems::Destroy()
{
	sce_delete(ourInstance);
}

void CGameSystems::Init()
{
	//myDuringUpdateFunctions.Init(256);
	Add<CMovementSystem>(myEntityManager);
	Add<CRenderSystem>(myEntityManager)->myUseRenderQueue = true;
	Add<CControllerSystem>(myEntityManager);
	Add<CAISystem>(myEntityManager);
	Add<CScriptSystem>(myEntityManager);
	Add<CHealingCircleSystem>(myEntityManager);
	Add<CDamageZoneSystem>(myEntityManager);
}

void CGameSystems::UpdateAllSystems(const float aDeltaTime)
{
	for (auto& pair : mySystems)
	{
		if ((pair.second->myIsActivated) && (pair.second->myUseRenderQueue == false))
		{
			pair.second->Update(aDeltaTime, myUpdateEntitiesQueued);
		}
	}

	auto scriptSystem(GetSystem<CScriptSystem>());

	scriptSystem->WaitUntilDone();
}

void CGameSystems::RenderAllSystems(const float aDeltaTime)
{
	auto postSuccessful(
		[&myPostUpdateFunctions = myPostUpdateFunctions]() -> bool
	{
		for (const auto& function : myPostUpdateFunctions)
		{
			if (function() == false)
			{
				return false;
			}
		}

		return true;
	});

	//auto postFuture = WORKER_POOL_QUEUE_WORK_FUTURE(postSuccessful);
	postSuccessful();

	for (auto& pair : mySystems)
	{
		if ((pair.second->myIsActivated) && (pair.second->myUseRenderQueue == true))
		{
			pair.second->Update(aDeltaTime, myRenderEntitiesQueued);
		}
	}

	//postFuture.wait();
	myPostUpdateFunctions.RemoveAll();

	myEntityManager.Refresh();
}

void CGameSystems::RemoveAllQueuedEntities()
{
	myUpdateEntitiesQueued.RemoveAll();
	myRenderEntitiesQueued.RemoveAll();
}
