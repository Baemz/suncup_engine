#include "stdafx.h"
#include "RenderSystem.h"

#include "../../GraphicsEngine/Camera/CameraFactory.h"
//#include "../../GraphicsEngine/Camera/CameraInstance.h"

#include "../../GraphicsEngine/DebugTools.h"

#include <iostream>
#include "ScriptSystem.h"
#include "../GraphicsEngine/GraphicsEngineInterface.h"
#include "ExposedFunctions/GameLogicFunctions.h"

void CRenderSystem::Activate()
{
	myBlackCanvas = sce_new(sce::gfx::CSprite());
	myBlackCanvas->Init(myBlackCanvasFilepath);
	const CU::Vector2f fullscreenResolution(sce::gfx::CGraphicsEngineInterface::GetFullResolution());
	myBlackCanvas->SetScale(fullscreenResolution / myBlackCanvas->GetPixelSize());
	myTotalCanvasFadeTime = -1.0f;
	myCanvasFadeTimer = 0.0f;
	myBlackBarsRadius = 0.0f;
	myRenderTriggers = false;
	myRenderObjectSpheres = false;
	myOnEclipseCallback = nullptr;
}

void CRenderSystem::Deactivate()
{
	myTotalCanvasFadeTime = 0.0f;
	sce_delete(myBlackCanvas);
}

void CRenderSystem::Update(const float aDeltaTime, const CU::GrowingArray<const CU::GrowingArray<UniqueIndexType>*>& aEntities)
{
	std::unique_lock<std::mutex> uniqueLock(myOnEclipseCallbackMutex);
	if (myCanvasFadeTimer <= myTotalCanvasFadeTime)
	{
		myCanvasFadeTimer += aDeltaTime;
		const float percent(myCanvasFadeTimer / myTotalCanvasFadeTime);
		const float interpolation = -(4 * percent * percent) + 4 * percent;

		if (myOnEclipseCallback != nullptr)
		{
			if (percent >= 0.5f)
			{
				myOnEclipseCallback();
				myOnEclipseCallback = nullptr;
			}
		}

		myBlackCanvas->SetAlpha(interpolation);
		myBlackCanvas->Render();
	}
	uniqueLock.unlock();

	std::unique_lock<std::mutex> uniqueLockBlackBars(myOnBarsDownMutex);
	if (myBlackBarsRadius > 0.0f)
	{
		myBlackCanvas->SetAlpha(1.0f);
		myBlackCanvas->SetPosition({ -1.0f + myBlackBarsRadius, 0.0f });
		myBlackCanvas->Render();
		myBlackCanvas->SetPosition({ 1.0f - myBlackBarsRadius, 0.0f });
		myBlackCanvas->Render();
		myBlackCanvas->SetPosition({ 0.0f, -1.0f + myBlackBarsRadius });
		myBlackCanvas->Render();
		myBlackCanvas->SetPosition({ 0.0f, 1.0f - myBlackBarsRadius });
		myBlackCanvas->Render();

		myBlackCanvas->SetPosition({ 0.0f });
	}
	uniqueLockBlackBars.unlock();

	//auto sendHpToHud = [myEntityManager](auto& aEntityIndex, auto& aHPComponent)
	//{
	//	float hp = aHPComponent.Hp;
	//	std::size_t enemyIndex = aEntityIndex;

	//	myEntityManager.ForEntitiesMatching<SigReceiveHP>([hp, enemyIndex](auto& aEntityIndex, auto& aHudHPComponent)
	//	{
	//		aHudHPComponent.myEnemyHPList[enemyIndex] = hp;
	//	});
	//};

	//myEntityManager.ForEntitiesMatching<SigSendHPToHUD>(sendHpToHud);

	for (const auto& entityArray : aEntities)
	{
		RenderModels(aDeltaTime, *entityArray);
		RenderCamera(*entityArray);
		RenderPointLights(*entityArray);
		RenderSpotLights(*entityArray);
		RenderDirectionalLights(*entityArray);
		RenderParticles(*entityArray);
		RenderHUDElements(*entityArray);

		if (myRenderTriggers)
		{
			RenderTriggers(*entityArray);
		}
	}

	sce::gfx::CDebugTools* debugDrawer(sce::gfx::CDebugTools::Get());

	auto renderObjectsAsDebugCubes = [&myEntityManager = myEntityManager, debugDrawer](auto& aEntityIndex, const CompTransform& aTransform, const CompController& aController)
	{
		if (myEntityManager.HasTag<TagInputController>(aEntityIndex))
		{
			debugDrawer->DrawCube(aTransform.myWorldPos, 1.0f, { 0.0f, 0.25f, 1.0f });
		}

		if (myEntityManager.HasTag<TagAIController>(aEntityIndex))
		{
			if (aController.GetAI().myWorldInterfaceTag == eWorldInterfaceTag::Polling)
			{
				debugDrawer->DrawCube(aTransform.myPosition, 1.0f, { 1.0f, 0.25f, 0.0f });
				//debugDrawer->DrawText2D("P", { (aTransform.myPosition.x / 20.0f) + 0.05f, (aTransform.myPosition.z / 80.0f) }, 14.0, { 1.0f, 0.25f, 0.0f });
			}
			if (aController.GetAI().myWorldInterfaceTag == eWorldInterfaceTag::Events)
			{
				debugDrawer->DrawCube(aTransform.myPosition, 1.0f, { 0.5f, 0.25f, 0.5f });
				//debugDrawer->DrawText2D("E", { (aTransform.myPosition.x / 80.0f) + 0.05f, (aTransform.myPosition.z / 60.0f) }, 14.0f, { 0.5f, 0.25f, 0.5f });
			}
		}

		if (myEntityManager.HasTag<TagScriptController>(aEntityIndex))
		{
			debugDrawer->DrawCube(aTransform.myPosition, 1.0f, { 1.0f, 0.0f, 1.0f });
		}
	};

	auto renderChildren = [&myEntityManager = myEntityManager, debugDrawer](auto&, CompTransform& aTransform)
	{
		debugDrawer->DrawCube((aTransform.myWorldPos), 1.f, { 1.f, 0.0f, 0.0f });
	};

	auto renderTriggers(
		[&myEntityManager = myEntityManager, debugDrawer](auto&, CompTrigger& aTrigger)
	{
		const CU::Vector3f size(aTrigger.myMax - aTrigger.myMin);
		debugDrawer->DrawRectangle3D((aTrigger.myMin + (size / 2.0f))
			, size
			, { 0.0f, 1.0f, 0.0f });
	});

	//myEntityManager.ForEntitiesMatching<SigRenderSimpleGrid>(renderSimpleGrid);
	
	//myEntityManager.ForEntitiesMatching<SigRenderDebugCubes>(renderObjectsAsDebugCubes);
	for (const auto& entityArray : aEntities)
	{
		myEntityManager.ForEntitiesMatching<SigRenderChildrenDebug>(renderChildren, *entityArray);

		if (myRenderTriggers)
		{
			myEntityManager.ForEntitiesMatching<SigTrigger>(renderTriggers, *entityArray);
		}
	}

	//auto renderModelBosses
	//auto renderModelShieldedBosses

	//myEntityManager.ForEntitiesMatching<SigRender, STBoss>(renderModelBosses);
	//myEntityManager.ForEntitiesMatching<SigRender, STBoss, SCShield>(renderModelShieldedBosses);
}

void CRenderSystem::ActivateCanvasFade(const float aFadeTime)
{
	if (aFadeTime <= 0.0f) { assert(false && "Now you fucked up."); return; }
	std::unique_lock<std::mutex> uniqueLock(myOnEclipseCallbackMutex);
	myTotalCanvasFadeTime = aFadeTime;
	myCanvasFadeTimer = 0;
}

void CRenderSystem::ActivateCanvasFade(const float aFadeTime, std::function<void()> aOnEclipseCallback)
{
	if (aFadeTime <= 0.0f) { assert(false && "Now you fucked up."); return; }
	std::unique_lock<std::mutex> uniqueLock(myOnEclipseCallbackMutex);
	myTotalCanvasFadeTime = aFadeTime;	
	myCanvasFadeTimer = 0;
	myOnEclipseCallback = aOnEclipseCallback;
}

void CRenderSystem::CancelCanvasFade()
{
	std::unique_lock<std::mutex> uniqueLock(myOnEclipseCallbackMutex);
	myCanvasFadeTimer = myTotalCanvasFadeTime;
	myOnEclipseCallback = nullptr;
}

void CRenderSystem::SetBlackBars(const float aScreenspaceRadius)
{
	std::unique_lock<std::mutex> uniqueLock(myOnBarsDownMutex);
	myBlackBarsRadius = aScreenspaceRadius / 2.0f;
}

void CRenderSystem::CancelBlackBars()
{
	std::unique_lock<std::mutex> uniqueLock(myOnBarsDownMutex);
	myBlackBarsRadius = 0.0f;
	myOnEclipseCallback = nullptr;
}

void CRenderSystem::RenderModels(const float aDeltaTime, const CU::GrowingArray<UniqueIndexType>& aEntities)
{
	auto renderModels = [this, &aDeltaTime](auto& aEntityIndex, CompTransform& aTransform, CompModelInstance& aRenderComponent)
	{
		auto& instance(aRenderComponent.myModel);

		if (myEntityManager.HasTag<TagInputController>(aEntityIndex))
		{
			int a = 5;
			a++;
			//ActivateCanvasFade();
			//myCanvasFadeTimer = 0.0f;
		}

		if (myEntityManager.HasTag<TagAIController>(aEntityIndex))
		{
			//sce::gfx::CDebugTools::Get()->DrawSphere(instance.GetFrustumCollider().GetPosition(), instance.GetFrustumCollider().GetRadius() * 0.75f, CU::Vector3f(1));
			//sce::gfx::CDebugTools::Get()->DrawSphere(aTransform.myPosition, aTransform.myRadius, CU::Vector3f(0.35f));
		}

		if (myRenderObjectSpheres)
		{
			sce::gfx::CDebugTools::Get()->DrawSphere(aTransform.myPosition, aTransform.myRadius, CU::Vector3f(0.35f));
		}

		if ((instance.IsLoaded() == false) && (instance.IsLoading() == false))
		{
			instance.Init();
			UniqueIndexType index = myEntityManager.GetComponent<CompHandle>(aEntityIndex).myHandle.myUniqueIndex;
			std::function<void(const unsigned short aEvent, const std::string& aAnimationName)> animEventCallback(
				[index](const unsigned short aEvent, const std::string& aAnimationName) -> void
			{
				const char* eventName = nullptr;
				switch (aEvent)
				{
				case 0: // START
					eventName = "OnAnimationStart";
					break;
				case 1: // MID
					eventName = "OnAnimationMid";
					break;
				case 2: // END
					eventName = "OnAnimationEnd";
					break;
				}
				CScriptSystem::CallScriptEventOnTarget(index, eventName, aAnimationName);
			});
			instance.SetAnimationEventCallback(animEventCallback);
		}
		if (instance.IsLoaded())
		{
			instance.SetOrientation(aTransform.myMatrix);
			//instance.Update(aDeltaTime);
			instance.Render();
		}
	};
	
	myEntityManager.ForEntitiesMatching<SigRender>(renderModels, aEntities);
}

void CRenderSystem::RenderCamera(const CU::GrowingArray<UniqueIndexType>& aEntities)
{
	auto createCamera = [](auto&, auto& aCameraComponent)
	{
		aCameraComponent.myInstance.UseForRendering();
	};

	myEntityManager.ForEntitiesMatching<SigCamera>(createCamera, aEntities);
}

void CRenderSystem::RenderPointLights(const CU::GrowingArray<UniqueIndexType>& aEntities)
{
	auto renderPointLights = [](auto&, CompTransform& aTransform, CompPointLight& aLightComponent)
	{
		auto& instance(aLightComponent.myPointLight);

		instance.SetPosition(aTransform.myPosition);
		instance.Render();
	};

	myEntityManager.ForEntitiesMatching<SigRenderPointLight>(renderPointLights, aEntities);
}

void CRenderSystem::RenderSpotLights(const CU::GrowingArray<UniqueIndexType>& aEntities)
{
	auto renderSpotLights = [](auto&, CompTransform& aTransform, CompSpotLight& aLightComponent)
	{
		auto& instance(aLightComponent.mySpotLight);

		instance.SetPosition(aTransform.myPosition);
		instance.Render();
	};

	myEntityManager.ForEntitiesMatching<SigRenderSpotLight>(renderSpotLights, aEntities);
}

void CRenderSystem::RenderDirectionalLights(const CU::GrowingArray<UniqueIndexType>& aEntities)
{
	auto renderDirLights = [](auto&, CompDirectionalLight& aLightComponent)
	{
		auto& instance(aLightComponent.myDirLight);

		instance.UseForRendering();
	};

	myEntityManager.ForEntitiesMatching<SigRenderDirectionalLight>(renderDirLights, aEntities);
}

void CRenderSystem::RenderParticles(const CU::GrowingArray<UniqueIndexType>& aEntities)
{
	auto particleEngine(sce::gfx::CParticleInterface::Get());
	
	if (particleEngine == nullptr)
	{
		return;
	}

	CU::GrowingArray<sce::gfx::ParticleIDType, sce::gfx::ParticleIDType> particlesToQueue(64);

	auto renderParticles = [&particlesToQueue](auto&, CompParticleEmitter& aParticleComponent)
	{
		particlesToQueue.Add(aParticleComponent.myParticleID);
	};

#pragma warning(disable: 4503) // If crash look into this <---
	myEntityManager.ForEntitiesMatching<SigRenderParticles>(renderParticles, aEntities);

	particleEngine->QueueRenderParticle(particlesToQueue);
	particleEngine->SubmitUpdateAndRenderQueue();
}

void CRenderSystem::RenderHUDElements(const CU::GrowingArray<UniqueIndexType>& /*aEntities*/)
{
	UniqueIndexType playerUniqueIndex(CGameLogicFunctions::GetPlayerUniqueIndex());

	if (playerUniqueIndex != EntityHandleINVALID.myUniqueIndex)
	{
		CompHUDElements& hudElements(myEntityManager.GetComponent<CompHUDElements>(SEntityHandle(playerUniqueIndex)));
		CU::GrowingArray<sce::gfx::CSprite*, CompHUDElements::IndexType> sprites(hudElements.mySprites.Size());
		hudElements.GetAllUsedSprites(sprites);

		for (const auto* sprite : sprites)
		{
			sprite->Render();
		}
	}

// 	auto renderEnemySprites(
// 		[&myEntityManager = myEntityManager](EntityIndex aIndex)
// 	{
// 		CompHUDElements& hudElements(myEntityManager.GetComponent<CompHUDElements>(aIndex));
// 		CU::GrowingArray<sce::gfx::CSprite*, CompHUDElements::IndexType> sprites(hudElements.mySprites.size());
// 		hudElements.GetAllUsedSprites(sprites);
// 
// 		for (const auto* sprite : sprites)
// 		{
// 			sprite->Render();
// 		}
// 	});
// 
// 	myEntityManager.ForEntitiesMatching<SigEnemyTag>(renderEnemySprites, aEntities);
}

void CRenderSystem::RenderTriggers(const CU::GrowingArray<UniqueIndexType>& aEntities)
{
	sce::gfx::CDebugTools* debugDrawer(sce::gfx::CDebugTools::Get());

	auto renderEm = [debugDrawer](auto&, const CompTrigger& aTrigger)
	{
		debugDrawer->DrawRectangle3D
		(
			(aTrigger.myMax + aTrigger.myMin) / 2.0f,
			{
				aTrigger.myMax.x - aTrigger.myMin.x,
				aTrigger.myMax.y - aTrigger.myMin.y,
				aTrigger.myMax.z - aTrigger.myMin.z,
			},
			{ 1.0f, 0.0f, 0.0f }
		);
	};

	myEntityManager.ForEntitiesMatching<SigTrigger>(renderEm, aEntities);
}
