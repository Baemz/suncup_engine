#include "stdafx.h"
#include "ControllerSystem.h"

#include "../../GraphicsEngine/GraphicsEngineInterface.h"
#include "../../GraphicsEngine/Camera/CameraInstance.h"

#include "../../GraphicsEngine/DebugTools.h"

#include "../../CommonUtilities/Intersection.h"
#include "../../CommonUtilities/Random.h"

#include "NavigationMesh/NavigationMesh.h"
#include "../GraphicsEngine/Model/ModelInstance.h"
#include "ScriptSystem.h"
#include "ExposedFunctions/GameLogicFunctions.h"
#include "MouseInterface.h"



CControllerSystem::CControllerSystem(EManager& aManager)
	: CSystemAbstract<CControllerSystem>(aManager)
	, myNavMesh(nullptr)
	, myCameraProjection(nullptr)
	, mySetPlayerPath(false)
{
}

void CControllerSystem::Activate(const SEntityHandle& aCameraHandle, CNavigationMesh& aNavMesh)
{
	myCameraHandle = aCameraHandle;
	myCameraProjection = &myEntityManager.GetComponent<CompCameraInstance>(myEntityManager.GetEntityIndex(myCameraHandle)).myInstance.GetProjection();

	myNavMesh = &aNavMesh;

	AttachToEventReceiving(Event::ESubscribedEvents(Event::eMouseClickInput | Event::eMouseInput));
}

void CControllerSystem::Deactivate()
{
	DetachFromEventReceiving();

	myCameraHandle = SEntityHandle();
	myNavMesh = nullptr;
	myCameraProjection = nullptr;
}

void CControllerSystem::Update(const float aDeltaTime, const CU::GrowingArray<const CU::GrowingArray<UniqueIndexType>*>& aEntities)
{
	myRecalculatePathTimer += (myRecalculatePathTimer < TimeBetweenPathRecalculation) ? aDeltaTime : 0.0f;

	UpdateCameraMovement(aDeltaTime);

	for (const auto& entityArray : aEntities)
	{
		UpdateModels(*entityArray, aDeltaTime);
		UpdateActiveAStarPaths(*entityArray);
		UpdateParticles(*entityArray);
		CheckPlayerToAICollision(*entityArray);
	}

	if (mySetPlayerPath == true)
	{
		mySetPlayerPath = false;

		SetPlayerPath(aEntities);
		myRecalculatePathTimer = 0;
	}
}

void CControllerSystem::ReceiveEvent(const Event::SEvent& aEvent)
{
	if (aEvent.myType == Event::EEventType::eMouseClickInput)
	{
		const Event::SMouseClickEvent* clickMsg = reinterpret_cast<const Event::SMouseClickEvent*>(&aEvent);
		if (clickMsg->key == 1)
		{
			if (myRecalculatePathTimer >= TimeBetweenPathRecalculation)
			{
				if (clickMsg->state == EButtonState::Pressed)
				{
					//Spawn move target particle
				}
				if (clickMsg->state == EButtonState::Down)
				{
					myMouseClickMessage.x = clickMsg->x;
					myMouseClickMessage.y = clickMsg->y;
					myMouseClickMessage.deltaX = clickMsg->deltaX;
					myMouseClickMessage.deltaY = clickMsg->deltaY;
					myMouseClickMessage.key = clickMsg->key;
					myMouseClickMessage.scrollDelta = clickMsg->scrollDelta;
					myMouseClickMessage.state = clickMsg->state;
					mySetPlayerPath = true;
				}
			}
		}
		else if (clickMsg->key == 2 && clickMsg->state == EButtonState::Pressed)
		{
			SetPlayerDashPosition();
		}
	}
	else if (aEvent.myType == Event::EEventType::eMouseInput)
	{
		const Event::SMouseDataEvent* mouseMsg = reinterpret_cast<const Event::SMouseDataEvent*>(&aEvent);
		mouseMsg;
		//sce::gfx::CDebugTools::Get()->DrawCube({mouseMsg->
	}
}

void CControllerSystem::UpdateModels(const CU::GrowingArray<UniqueIndexType>& aEntities, const float aDeltaTime)
{
	auto updateModels = [this, &aDeltaTime](auto& /*aEntityIndex*/, CompTransform& /*aTransform*/, CompModelInstance& aRenderComponent)
	{
		auto& instance(aRenderComponent.myModel);
		if (instance.IsLoaded())
		{
			instance.Update(aDeltaTime);
		}
	};

	myEntityManager.ForEntitiesMatching<SigRender>(updateModels, aEntities);
}

void CControllerSystem::UpdateParticles(const CU::GrowingArray<UniqueIndexType>& aEntities)
{
	auto particleEngine(sce::gfx::CParticleInterface::Get());

	if (particleEngine == nullptr)
	{
		return;
	}

	CU::GrowingArray<sce::gfx::SParticleUpdate, sce::gfx::ParticleIDType> particlesToQueue(64);

	auto renderParticles = [particleEngine, &myEntityManager = myEntityManager, &particlesToQueue](auto& aEntityIndex, CompParticleEmitter& aParticleComponent)
	{
		if (particleEngine->IsFinished(aParticleComponent.myParticleID))
		{
			CScriptSystem::KillEntity(myEntityManager.GetComponent<CompHandle>(aEntityIndex).myHandle.myUniqueIndex);
			return;
		}

		sce::gfx::SParticleUpdate particleUpdate;
		particleUpdate.myParticleID = aParticleComponent.myParticleID;
		particleUpdate.myPosition = myEntityManager.GetComponent<CompTransform>(aEntityIndex).myPosition;

		particlesToQueue.Add(particleUpdate);
	};

#pragma warning(disable: 4503) // If crash look into this <---
	myEntityManager.ForEntitiesMatching<SigRenderParticles>(renderParticles, aEntities);

	particleEngine->QueueUpdateParticle(particlesToQueue);
}

void CControllerSystem::UpdateActiveAStarPaths(const CU::GrowingArray<UniqueIndexType>& aEntities)
{
	sce::gfx::CDebugTools* debug(sce::gfx::CDebugTools::Get());
	debug;
	auto updatePathFinder = [&myEntityManager = myEntityManager, &myNavMesh = myNavMesh](auto& aEntityIndex, CompTransform& aTransform, CompController& aController)
	{
		//if (myEntityManager.HasTag<TagAIController>(aEntityIndex))
		//{
			CController& aiController(aController.Get());
			if (aiController.myInitNavigationData.myShouldInitializePath)
			{
				//Start navigation
				CU::Collision::Ray objectRay(aTransform.myPosition + CU::Vector3f(0.0f, aTransform.myRadius, 0.0f), CU::Vector3f(0.0f, -1.0f, 0.0f));
				CU::Collision::Ray targetRay(aiController.myInitNavigationData.myTargetPosition + CU::Vector3f(0.0f, aTransform.myRadius, 0.0f), CU::Vector3f(0.0f, -1.0f, 0.0f));

				myNavMesh->DeselectBeginAndEnd();
				myNavMesh->SelectBeginNode(objectRay, true, NAVMESH_HEIGHT_LIMIT);
				myNavMesh->SelectEndNode(targetRay, true, NAVMESH_HEIGHT_LIMIT);

				if (myNavMesh->CalculatePath())
				{
					aiController.myCurrentPath = myNavMesh->GetPathCopy();

					if (aiController.myCurrentPath.Size() > 0)
					{
						myEntityManager.AddComponent<CompArriveBehavior>(aEntityIndex, aiController.myCurrentPath.GetLast(), CompArriveBehavior::ArriveDistance, 1.0f); // HACK (should be seek)
						
						if (myEntityManager.HasTag<TagPlayer>(aEntityIndex) || myEntityManager.HasTag<TagCompanion>(aEntityIndex))
						{
							aiController.myTargetLookRotation = CU::Quaternion::LookAt(aiController.myInitNavigationData.myTargetPosition, aTransform.myPosition);
						}
						else
						{
							aiController.myTargetLookRotation = CU::Quaternion::LookAt(aTransform.myPosition, aiController.myInitNavigationData.myTargetPosition);
						}
					}
				}

				aiController.myInitNavigationData.myShouldInitializePath = false;
			}
		//}

		NavMesh::Path& controllerPath(aController.Get().myCurrentPath);

		if (controllerPath.Size() > 0)
		{
			const float distance((aTransform.myPosition - controllerPath.GetLast()).Length2());
			if (distance <= CNavigationMesh::NodeRadius)
			{
				controllerPath.RemoveCyclicAtIndex(controllerPath.Size() - 1);

				if (myEntityManager.HasTag<TagInputController>(aEntityIndex))
				{
					CScriptSystem::CallScriptEventEveryone("OnPlayerMoved");
				}

				//If last way point in path
				if (controllerPath.size() == 1)
				{
					myEntityManager.AddComponent<CompArriveBehavior>(aEntityIndex, controllerPath.GetLast(), 1.0f, 1.0f);
					if (myEntityManager.HasTag<TagPlayer>(aEntityIndex) || myEntityManager.HasTag<TagCompanion>(aEntityIndex))
					{
						aController.Get().myTargetLookRotation = CU::Quaternion::LookAt(controllerPath.GetLast(), aTransform.myPosition);
					}
					else
					{
						aController.Get().myTargetLookRotation = CU::Quaternion::LookAt(aTransform.myPosition, controllerPath.GetLast());
					}
				}
				else if (controllerPath.size() > 0)
				{
					myEntityManager.AddComponent<CompArriveBehavior>(aEntityIndex, controllerPath.GetLast(), 1.0f, 1.0f); // HACK (should be seek)
					if (myEntityManager.HasTag<TagPlayer>(aEntityIndex) || myEntityManager.HasTag<TagCompanion>(aEntityIndex))
					{
						aController.Get().myTargetLookRotation = CU::Quaternion::LookAt(controllerPath.GetLast(), aTransform.myPosition);
					}
					else
					{
						aController.Get().myTargetLookRotation = CU::Quaternion::LookAt(aTransform.myPosition, controllerPath.GetLast());
					}
				}
			}
		}
	};

	myEntityManager.ForEntitiesMatching<SigController>(updatePathFinder, aEntities);
}

void CControllerSystem::UpdateCameraMovement(const float aDeltaTime)
{
	CompCameraInstance& camInstance(myEntityManager.GetComponent<CompCameraInstance>(myCameraHandle));

	auto updateCameraWithRespectToPlayer(
		[&camInstance, &aDeltaTime, &myEntityManager = myEntityManager](auto&, CompTransform& aTransform, CompController& aController)
	{
		if (!camInstance.myFreeCamera)
		{
			const CInputController& controller(aController.GetInput());
			camInstance.myInstance.SetPosition(aTransform.myPosition + controller.myCameraOffset);
		}
	});

	if (CGameLogicFunctions::GetPlayerUniqueIndex() != EntityHandleINVALID.myUniqueIndex)
	{
		myEntityManager.RunIfMatching<SigInputController>(SEntityHandle(CGameLogicFunctions::GetPlayerUniqueIndex()), updateCameraWithRespectToPlayer);
	}
}

void CControllerSystem::CheckPlayerToAICollision(const CU::GrowingArray<UniqueIndexType>& aEntities)
{
	UniqueIndexType player(CGameLogicFunctions::GetPlayerUniqueIndex());
	if (player == EntityHandleINVALID.myUniqueIndex) return;

	auto checkIfPlayerCollidedWithEnemy = [&myEntityManager = myEntityManager, player]
	(EntityIndex aEntityIndex, CompTransform&, CompController&)
	{
		if (!myEntityManager.HasTag<TagEnemy>(aEntityIndex))
		{
			return;
		}

		const sce::gfx::CModelInstance& playerModelInstance(myEntityManager.GetComponent<CompModelInstance>(SEntityHandle(player)).myModel);
		const sce::gfx::CModelInstance& enemyModelInstance(myEntityManager.GetComponent<CompModelInstance>(aEntityIndex).myModel);
		
		const CU::Collision::Sphere playerSphere(playerModelInstance.GetFrustumCollider().GetPosition(), playerModelInstance.GetFrustumCollider().GetRadius() * 0.75f);
		const CU::Collision::Sphere enemySphere(enemyModelInstance.GetFrustumCollider().GetPosition(), enemyModelInstance.GetFrustumCollider().GetRadius() * 0.75f);
		
		if (CU::Collision::IntersectionSphereSphere(playerSphere, enemySphere))
		{
			UniqueIndexType uniqueIndex(myEntityManager.GetComponent<CompHandle>(aEntityIndex).myHandle.myUniqueIndex);
			CScriptSystem::CallScriptEventEveryone("OnPlayerCollidesWithEnemy", uniqueIndex);
		}
	};

	auto checkIfPlayerCollidedWithHealthPickup = [&myEntityManager = myEntityManager, player]
	(EntityIndex aEntityIndex, CompTransform&)
	{
		if (!myEntityManager.HasComponent<CompModelInstance>(aEntityIndex))
		{
			return;
		}

		const sce::gfx::CModelInstance& playerModelInstance(myEntityManager.GetComponent<CompModelInstance>(SEntityHandle(player)).myModel);
		const sce::gfx::CModelInstance& healthModelInstance(myEntityManager.GetComponent<CompModelInstance>(aEntityIndex).myModel);

		const CU::Collision::Sphere playerSphere(playerModelInstance.GetFrustumCollider().GetPosition(), playerModelInstance.GetFrustumCollider().GetRadius() * 0.75f);
		const CU::Collision::Sphere healthSphere(healthModelInstance.GetFrustumCollider().GetPosition(), healthModelInstance.GetFrustumCollider().GetRadius() * 0.75f);
		
		if (CU::Collision::IntersectionSphereSphere(playerSphere, healthSphere))
		{
			UniqueIndexType uniqueIndex(myEntityManager.GetComponent<CompHandle>(aEntityIndex).myHandle.myUniqueIndex);
			CScriptSystem::CallScriptEventOnTarget(uniqueIndex, "OnHealthReachedZero");
		}
	};

	myEntityManager.ForEntitiesMatching<SigAIController>(checkIfPlayerCollidedWithEnemy, aEntities);
	myEntityManager.ForEntitiesMatching<SigHealthPickupCollisions>(checkIfPlayerCollidedWithHealthPickup, aEntities);
}

void CControllerSystem::SetPlayerPath(const CU::GrowingArray<const CU::GrowingArray<UniqueIndexType>*>& aEntities)
{
	UniqueIndexType playerUniqueIndex(CGameLogicFunctions::GetPlayerUniqueIndex());
	if (playerUniqueIndex == EntityHandleINVALID.myUniqueIndex)
	{
		return;
	}

	auto setPlayerPath = [this, &aEntities]
	(auto& aEntityIndex, CompTransform& aTransform, CompController& aController)
	{
		if (aController.Get().myControllerType != EController::Input)
		{
			GAME_LOG("ERROR! Received controller wasn't of \"input\" type.\n");
			return;
		}

		CU::Collision::Ray cursorWorldRay = CMouseInterface::Get()->GetMouseToNavmeshRay();
		CU::Collision::Ray objectRay(aTransform.myPosition + CU::Vector3f(0.0f, aTransform.myRadius, 0.0f), CU::Vector3f(0.0f, -1.0f, 0.0f));

		myNavMesh->DeselectBeginAndEnd();
		myNavMesh->SelectBeginNode(objectRay, true, NAVMESH_HEIGHT_LIMIT);
		myNavMesh->SelectEndNode(cursorWorldRay, true);

		if (myNavMesh->CalculatePath())
		{
			auto& controllerPath(aController.Get().myCurrentPath);
			controllerPath = myNavMesh->GetPathCopy();

			if (controllerPath.Size() > 0)
			{
				myEntityManager.AddComponent<CompArriveBehavior>(aEntityIndex, controllerPath.GetLast(), CompArriveBehavior::ArriveDistance, 1.0f); // HACK (should be seek)

				//////////////////////////////////////////////////////////////////////////////////
				// USED TO SEND A CLICK-PARTICLE TO RENDERING
				const CU::Vector3f clickedPosition(controllerPath[0].x, controllerPath[0].y + 0.01f, controllerPath[0].z);
				CGameLogicFunctions::SpawnParticleOnPosition("clickCircle", clickedPosition);
				//////////////////////////////////////////////////////////////////////////////////

				aController.Get().myTargetLookRotation = CU::Quaternion::LookAt(myNavMesh->GetEndNodeClickedPosition(), aTransform.myPosition);
			}
		}

		for (const auto& entityArray : aEntities)
		{
			CheckScreenToWorldRayCollision(cursorWorldRay, *entityArray);
		}
	};

	myEntityManager.RunIfMatching<SigInputController>(SEntityHandle(playerUniqueIndex), setPlayerPath);
}

void CControllerSystem::SetPlayerDashPosition()
{
	UniqueIndexType playerUniqueIndex(CGameLogicFunctions::GetPlayerUniqueIndex());
	if (playerUniqueIndex == EntityHandleINVALID.myUniqueIndex)
	{
		return;
	}

	auto setDashPosition = [
		&myEntityManager = myEntityManager,
			&myNavMesh = myNavMesh,
			playerUniqueIndex
	](auto&, CompTransform& aTransform, CompController& aController)
	{
		if (aController.Get().myControllerType != EController::Input)
		{
			GAME_LOG("ERROR! Received controller wasn't of \"input\" type.\n");
			return;
		}

		const CU::Collision::Ray cursorWorldRay = CMouseInterface::Get()->GetMouseToNavmeshRay();
		const CU::Collision::Ray objectRay(aTransform.myPosition + CU::Vector3f(0.0f, aTransform.myRadius, 0.0f), CU::Vector3f(0.0f, -1.0f, 0.0f));

		CU::Collision::Ray targetDashRay;
		CU::Vector3f dashTargetPosition;
		bool gotEndEdgeIntersection(myNavMesh->GetFirstEndEdgeIntersection(objectRay, cursorWorldRay, dashTargetPosition));
		bool couldCalculatePath(false);

		targetDashRay.myOrigin = dashTargetPosition + CU::Vector3f(0.0f, aTransform.myRadius, 0.0f);
		targetDashRay.myDirection = CU::Vector3f(0.0f, -1.0f, 0.0f);

		if (!gotEndEdgeIntersection)
		{
			myNavMesh->DeselectBeginAndEnd();
			myNavMesh->SelectBeginNode(objectRay, true, NAVMESH_HEIGHT_LIMIT);
			myNavMesh->SelectEndNode(cursorWorldRay, true);

			if (!myNavMesh->IsBeginSelected())
			{
				if (myNavMesh->IsEndSelected())
				{
					couldCalculatePath = true;
					dashTargetPosition = myNavMesh->GetEndNodeClickedPosition();
				}
			}
			else if (myNavMesh->IsEndSelected())
			{
				if (myNavMesh->CalculatePath(MeshComponents::ENodeType::eWalk_Dash, MeshComponents::ENodeType::eDash) == true)
				{
					dashTargetPosition = myNavMesh->GetEndNodeClickedPosition();

					if (std::abs(dashTargetPosition.y - aTransform.myPosition.y) <= 0.5f)
					{
						couldCalculatePath = true;
					}
				}
			}
		}
		else if (myNavMesh->CheckNodeTypeOnRayCast(targetDashRay, MeshComponents::ENodeType::eWalk_Dash, true, 2.0f) == false)
		{
			gotEndEdgeIntersection = false;
		}

		if (gotEndEdgeIntersection || couldCalculatePath)
		{
			aController.Get().myVelocity = { 0 };
			aController.Get().mySteering = { 0 };
			CScriptSystem::CallScriptEventOnTarget(playerUniqueIndex, "OnPlayerRightClick", dashTargetPosition.x, dashTargetPosition.y, dashTargetPosition.z);
		}
	};

	myEntityManager.RunIfMatching<SigInputController>(SEntityHandle(playerUniqueIndex), setDashPosition);

}

void CControllerSystem::CheckScreenToWorldRayCollision(const CU::Collision::Ray& aScreenToWorldRay, const CU::GrowingArray<UniqueIndexType>& aEntities)
{
	bool clickedAnEnemy(false);

	auto checkIfPlayerClickedAnEnemy = [&myEntityManager = myEntityManager, &aScreenToWorldRay, &clickedAnEnemy]
	(EntityIndex aEntityIndex, CompTransform&, CompController&)
	{
		const sce::gfx::CModelInstance& modelInstance(myEntityManager.GetComponent<CompModelInstance>(aEntityIndex).myModel);
		const CU::Collision::Sphere enemySphere(modelInstance.GetFrustumCollider().GetPosition(), modelInstance.GetFrustumCollider().GetRadius() * 0.75f);
		if (myEntityManager.HasTag<TagEnemy>(aEntityIndex) && CU::Collision::IntersectionRaySphere(aScreenToWorldRay, enemySphere))
		{
			UniqueIndexType uniqueIndex(myEntityManager.GetComponent<CompHandle>(aEntityIndex).myHandle.myUniqueIndex);
			CScriptSystem::CallScriptEventEveryone("OnPlayerClickedEnemy", uniqueIndex);
			clickedAnEnemy = true;
			//GAME_LOG("Clicked enemy");
		}
	};

	auto checkIfPlayerClickedACheckpoint = [&myEntityManager = myEntityManager, &aScreenToWorldRay]
	(EntityIndex aEntityIndex, CompTransform&)
	{
		const CompTransform& checkpointTransform(myEntityManager.GetComponent<CompTransform>(aEntityIndex));
		const CU::Collision::Sphere checkpointSphere(checkpointTransform.myPosition + CU::Vector3f::Up * 1.5f, checkpointTransform.myRadius);
		if (CU::Collision::IntersectionRaySphere(aScreenToWorldRay, checkpointSphere))
		{
			UniqueIndexType uniqueIndex(myEntityManager.GetComponent<CompHandle>(aEntityIndex).myHandle.myUniqueIndex);
			CScriptSystem::CallScriptEventOnTarget(uniqueIndex, "OnWasClicked");
		}
	};

	myEntityManager.ForEntitiesMatching<SigAIController>(checkIfPlayerClickedAnEnemy, aEntities);
	myEntityManager.ForEntitiesMatching<SigCheckpointCollisions>(checkIfPlayerClickedACheckpoint, aEntities);

	if (!clickedAnEnemy)
	{
		CScriptSystem::CallScriptEventEveryone("OnPlayerClickedNotOnEnemy");
	}
}