#pragma once
#include "../EntitySystemLink/EntitySystemLink.h"
#include "SystemAbstract.h"

#include <unordered_map>
#include <memory>

//Warning: regular NEWs data-less types, get me dead if we overstep our bounds by like 20 bytes

//FWD
class CGame;

class CGameSystems
{
public:
	using PostUpdateFunction = std::function<bool()>;

	~CGameSystems() = default;

	static CGameSystems* Get() { return ourInstance; }

	template<typename System, typename... Args>
	void ActivateSystem(Args&&... aArgs)
	{
		std::shared_ptr<System> system(GetSystem<System>());
		system->Activate(std::forward<Args>(aArgs)...);
		system->myIsActivated = true;
	}

	template<typename System, typename... Args>
	void DeactivateSystem(Args&&... aArgs)
	{
		std::shared_ptr<System> system(GetSystem<System>());
		system->Deactivate(std::forward<Args>(aArgs)...);
		system->myIsActivated = false;
	}

	template<typename System>
	std::shared_ptr<System> GetSystem()
	{
		auto it = mySystems.find(System::myIndex);
		assert(it != mySystems.end());
		return std::shared_ptr<System>(std::static_pointer_cast<System>(it->second));
	}

	//Add new system
	template <typename System>
	std::shared_ptr<System> Add(EManager& aManager)
	{
		std::shared_ptr<System> system(new System(aManager));
		mySystems.insert(std::make_pair(System::NewSystemIndex(), system));
		return system;
	}

	//Delete system
	template <typename System>
	void Delete()
	{
		auto it = mySystems.find(System::myIndex);
		assert(it != mySystems.end());
		mySystems.erase(it->first);
	}

	void QueueUpdateEntityUniqueIndices(const CU::GrowingArray<UniqueIndexType>* aEntities);
	void QueueRenderEntityUniqueIndices(const CU::GrowingArray<UniqueIndexType>* aEntities);

	void QueuePostUpdateFunction(const PostUpdateFunction& aFunction);

	void RemoveQueuedUpdateEntities(const CU::GrowingArray<UniqueIndexType>* aEntities);

private:
	//Don't let anyone else operate the manager.
	friend CGame;
	static void Create(EManager& aEntityManager);
	static void Destroy();

	CGameSystems(EManager& aEntityManager)
		: myEntityManager(aEntityManager) { }

	void Init();

	void UpdateAllSystems(const float aDeltaTime);
	void RenderAllSystems(const float aDeltaTime);

	void RemoveAllQueuedEntities();

	static CGameSystems* ourInstance;

	EManager& myEntityManager;
	std::unordered_map<CSystemAbstractAbstract::SystemCounter, std::shared_ptr<CSystemAbstractAbstract>> mySystems;
	
	CU::GrowingArray<const CU::GrowingArray<UniqueIndexType>*> myUpdateEntitiesQueued;
	CU::GrowingArray<const CU::GrowingArray<UniqueIndexType>*> myRenderEntitiesQueued;

	CU::GrowingArray<PostUpdateFunction> myPostUpdateFunctions;

};