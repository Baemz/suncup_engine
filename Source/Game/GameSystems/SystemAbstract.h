#pragma once
#include "../EntitySystemLink/EntitySystemLink.h"

//Fairly self-explanatory
class CSystemAbstractAbstract
{
public:
	using SystemCounter = std::size_t;
	CSystemAbstractAbstract() = default;
	CSystemAbstractAbstract(EManager& aManager) : myEntityManager(aManager), myIsActivated(false), myUseRenderQueue(false) { }
	virtual ~CSystemAbstractAbstract() = default;

	static SystemCounter abstractSystemCount;

	template <typename... Args>
	void Activate(Args&&... aArgs) = 0;

	template <typename... Args>
	void Deactivate(Args&&... aArgs) = 0;

	virtual void Update(const float aDeltaTime, const CU::GrowingArray<const CU::GrowingArray<UniqueIndexType>*>& aEntities) = 0;
	EManager& myEntityManager;
	bool myIsActivated;
	bool myUseRenderQueue;
};


template <typename AbstractAbstract>
class CSystemAbstract : public CSystemAbstractAbstract
{
	friend class CGameSystems;

	static size_t NewSystemIndex()
	{
		static std::size_t count = abstractSystemCount++;
		myIndex = count;
		return count;
	}

	static SystemCounter myIndex;

public:
	CSystemAbstract(EManager& aManager) : CSystemAbstractAbstract(aManager) { }
	virtual ~CSystemAbstract() override = default;
};

template<typename System>
CSystemAbstractAbstract::SystemCounter CSystemAbstract<System>::myIndex = 0;

