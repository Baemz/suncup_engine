#pragma once
#include "SystemAbstract.h"

namespace sce
{
	namespace gfx
	{
		class CDebugTools;
	}
}
class CAIPollingStation;
class CMovementSystem : public CSystemAbstract<CMovementSystem>
{
public:
	CMovementSystem(EManager& aManager) : CSystemAbstract<CMovementSystem>(aManager) { }
	~CMovementSystem() override = default;

	void Activate();
	void Deactivate();

	void Update(const float aDeltaTime, const CU::GrowingArray<const CU::GrowingArray<UniqueIndexType>*>& aEntities) override;

private:
	void CombineSteering(const float aDeltaTime, sce::gfx::CDebugTools* debugDrawer, const CU::GrowingArray<UniqueIndexType>& aEntities);
	void SeekBehaviourAI(const float aDeltaTime, const CU::GrowingArray<UniqueIndexType>& aEntities);
	void ArriveBehaviourAI(const float aDeltaTime, const CU::GrowingArray<UniqueIndexType>& aEntities);
	void FleeBehaviourAI(const float aDeltaTime, const CU::GrowingArray<UniqueIndexType>& aEntities);
	void EvadeBehaviourAI(const float aDeltaTime, const CU::GrowingArray<UniqueIndexType>& aEntities);
	void WanderBehaviourAI(const float aDeltaTime, const CU::GrowingArray<UniqueIndexType>& aEntities);
	SEntityHandle myPlayerHandle;

};
