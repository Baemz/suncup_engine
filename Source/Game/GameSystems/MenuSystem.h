#pragma once
#include "SystemAbstract.h"

class CMenuSystem : public CSystemAbstract<CMenuSystem>
{
public:
	CMenuSystem(EManager& aManager) : CSystemAbstract<CMenuSystem>(aManager) { }

	void Activate() {}
	void Deactivate() {};

	//Update every signature with anything menu-related
	void Update(const float aDeltaTime, const CU::GrowingArray<const CU::GrowingArray<UniqueIndexType>*>& aEntities) override;

private:

	auto OnHoverButton();
};