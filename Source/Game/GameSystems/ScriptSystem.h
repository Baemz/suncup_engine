#pragma once
#include "SystemAbstract.h"
#include "ScriptEventManager.h"

class CScriptManager;
//class CScriptEventManager;

class CScriptSystem : public CSystemAbstract<CScriptSystem>
{
	friend class CEntityFactory;

public:
	static constexpr std::size_t MaxAmountOfAttachedVariables = 8; // Will cause huge compile time increases if raised!!!!!! (or infinite)

	CScriptSystem(EManager& aManager);
	~CScriptSystem() override;
	
	void Activate();
	void Deactivate();

	void Update(const float aDeltaTime, const CU::GrowingArray<const CU::GrowingArray<UniqueIndexType>*>& aEntities) override;

	void WaitUntilDone() const;

	static void SetIsActive(const bool aIsActive);

	static void KillEntity(const UniqueIndexType& aUniqueIndex);
	static void KillEntity_ThreadSafe(const UniqueIndexType& aUniqueIndex);

	template<typename... Args>
	static bool CallScriptEventEveryone(const char* aEventName, Args&&... aArgs);

	template<typename... Args>
	static bool CallScriptEventOnTarget(const UniqueIndexType& aEntityUniqueIndex, const char* aEventName, Args&&... aArgs);

private:
	static void RegisterScriptComponent(const unsigned int aStateIndex, const SEntityHandle& aEntityHandle);

	bool RegisterScriptFunctions() const;

	CScriptManager* myScriptManager;
	CScriptEventManager* myScriptEventManager;

	static CScriptEventManager* ourEventManagerInstance;
};

template<typename... Args>
inline bool CScriptSystem::CallScriptEventEveryone(const char* aEventName, Args&&... aArgs)
{
	if (ourEventManagerInstance == nullptr)
	{
		RESOURCE_LOG("[ScriptEventManager] ERROR! Script event manager was nullptr.");
		return false;
	}

	return ourEventManagerInstance->CallEventOnEveryone(CScriptManager::InvalidStateIndex, aEventName, std::forward<Args>(aArgs)...);
}

template<typename ...Args>
inline bool CScriptSystem::CallScriptEventOnTarget(const UniqueIndexType& aEntityUniqueIndex, const char* aEventName, Args&&... aArgs)
{
	if (ourEventManagerInstance == nullptr)
	{
		RESOURCE_LOG("[ScriptEventManager] ERROR! Script event manager was nullptr.");
		return false;
	}

	return ourEventManagerInstance->CallEventOnTarget(CScriptManager::InvalidStateIndex, aEventName, aEntityUniqueIndex, std::forward<Args>(aArgs)...);
}
