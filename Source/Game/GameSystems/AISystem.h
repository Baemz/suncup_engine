#pragma once
#include "SystemAbstract.h"
#include "../EngineCore/EventSystem/EventReceiver.h"

class CAISystem : public CSystemAbstract<CAISystem>, public sce::CEventReceiver
{
public:
	CAISystem(EManager& aManager);
	~CAISystem() override;

	void Activate();
	void Deactivate();

	void Update(const float aDeltaTime, const CU::GrowingArray<const CU::GrowingArray<UniqueIndexType>*>& aEntities) override;

	void ReceiveEvent(const Event::SEvent& aEvent) override;

private:

	void CheckProjectileCollisions(const CU::GrowingArray<UniqueIndexType>& aEntities);

	void EvadeOtherControllers();

	void CollectActiveControllers(const CU::GrowingArray<UniqueIndexType>& aEntities);
	void AddEvades(const CU::GrowingArray<UniqueIndexType>& aEntities);

	struct SAIEvadeData
	{
		SAIEvadeData()
			: myTransform(nullptr)
			, myController(nullptr)
			, myUniqueEntityIndex(EntityHandleINVALID.myUniqueIndex)
		{}

		SAIEvadeData(const CompTransform* aTransform, const CompController* aController, const UniqueIndexType& aUniqueEntityIndex)
			: myTransform(aTransform)
			, myController(aController)
			, myUniqueEntityIndex(aUniqueEntityIndex)
		{}

		const CompTransform* myTransform;
		const CompController* myController;
		UniqueIndexType myUniqueEntityIndex;
	};

	CU::GrowingArray<SAIEvadeData> myAIEvadeData;
};