#include "stdafx.h"
#include "GameLogicFunctions.h"
#include "..\ScriptSystem.h"
#include "..\GameSystems.h"
#include "..\RenderSystem.h"
#include "Level.h"
#include "EntityFactory.h"
#include "..\GraphicsEngine\Model\ModelInstance.h"
#include "EntitySystemLink\RenderComponents.h"
#include "..\GraphicsEngine\DebugTools.h"
#include "NavigationMesh\NavigationMeshWrapper.h"
#include "NavigationMesh\NavMeshStructs.h"
#include "MouseInterface.h"
#include "HealingCircleSystem.h"

#include "AudioWrapper.h"

EManager* CGameLogicFunctions::ourManager = nullptr;
CLevel* CGameLogicFunctions::ourLevel = nullptr;
UniqueIndexType CGameLogicFunctions::ourPlayerEntityIndex = EntityHandleINVALID.myUniqueIndex;
UniqueIndexType CGameLogicFunctions::ourSupportCompanionIndex = EntityHandleINVALID.myUniqueIndex;
CU::GrowingArray<UniqueIndexType> CGameLogicFunctions::ourEntities;
volatile bool CGameLogicFunctions::ourShouldGoToCredits = false;

#pragma warning(disable : 4127)

//If function doesn't receive entity indices, substitute with [0]
#define ERROR_CHECK_ENTITY_MANAGER(aReturnValue, index1, index2)								\
if (ourManager == nullptr)																		\
{																								\
	GAME_LOG("ERROR! EntityManager not bound to CGameLogicFunctions");							\
	return aReturnValue;																		\
}																								\
/* HACK */																						\
if (((index1 != 0) && (!ourManager->IsHandleValidNoAssert(SEntityHandle(index1))))				\
|| ((index2 != 0) && (!ourManager->IsHandleValidNoAssert(SEntityHandle(index2)))))				\
{																								\
	GAME_LOG("ERROR! Tried to call script function \"%s\" on invalid Entity.", __FUNCTION__);	\
		return aReturnValue;																	\
}

#define ERROC_CHECK_LEVEL(aReturnValue)							\
if (ourLevel == nullptr)										\
{																\
	GAME_LOG("ERROR! Level not bound to CGameLogicFunctions");	\
	return aReturnValue;										\
}

void CGameLogicFunctions::ArriveAtPosition(const UniqueIndexType& aEntityUniqueIndex, const CU::Vector3f& aPosition)
{
	ERROR_CHECK_ENTITY_MANAGER(; , aEntityUniqueIndex, 0);

	//if (!ourManager->HasTag<TagAIController>(SEntityHandle(aEntityUniqueIndex)))
	//{
	//	GAME_LOG("WARNING! Tried to call ArriveAtPosition on a non-player or a non-AI-controlled object: Invalid.");
	//}
	//else
	//{
		CController& aiController(ourManager->GetComponent<CompController>(SEntityHandle(aEntityUniqueIndex)).Get());

		aiController.myInitNavigationData.myShouldInitializePath = true;
		aiController.myInitNavigationData.myTargetPosition = aPosition;
	//}
}

void CGameLogicFunctions::Wander(const UniqueIndexType & aEntityUniqueIndex)
{
	ERROR_CHECK_ENTITY_MANAGER(; , aEntityUniqueIndex, 0);
	//Nail polish remover�
	return;

	//if (!ourManager->HasComponent<CompWanderBehavior>(aEntityUniqueIndex))
	//{
	//	ourManager->AddComponent<CompWanderBehavior>(SEntityHandle(aEntityUniqueIndex), 0.75f);
	//}
}

void CGameLogicFunctions::StopWander(const UniqueIndexType & aEntityUniqueIndex)
{
	ERROR_CHECK_ENTITY_MANAGER(; , aEntityUniqueIndex, 0);

	ourManager->DelComponent<CompWanderBehavior>(SEntityHandle(aEntityUniqueIndex));

}

void CGameLogicFunctions::LerpToPosition(const UniqueIndexType& aEntityUniqueIndex, const CU::Vector3f& aPosition, const float aPercentagePerFrame)
{
	aEntityUniqueIndex; aPosition; aPercentagePerFrame;
}

void CGameLogicFunctions::SmoothstepToPosition(const UniqueIndexType& aEntityUniqueIndex, const CU::Vector3f& aPosition, const float aTotalTime)
{
	aEntityUniqueIndex; aPosition; aTotalTime;
}

void CGameLogicFunctions::TeleportToPosition(const UniqueIndexType& aEntityUniqueIndex, const CU::Vector3f& aPosition)
{
	ERROR_CHECK_ENTITY_MANAGER(; , aEntityUniqueIndex, 0);

	ourManager->GetComponent<CompTransform>(SEntityHandle(aEntityUniqueIndex)).myPosition = aPosition;
	UpdateEntityPosition(aEntityUniqueIndex, aPosition);
}

void CGameLogicFunctions::ArriveAtEntity(const UniqueIndexType& aEntityUniqueIndex, const UniqueIndexType& aTargetEntityUniqueIndex)
{
	ERROR_CHECK_ENTITY_MANAGER(; , aEntityUniqueIndex, aTargetEntityUniqueIndex);

	const CompTransform& myTransform(ourManager->GetComponent<CompTransform>(SEntityHandle(aEntityUniqueIndex)));
	const CompTransform& targetTransform(ourManager->GetComponent<CompTransform>(SEntityHandle(aTargetEntityUniqueIndex)));

	constexpr float radiusOffsetModifier = 0.75f;

	const CU::Vector3f offsetTargetPosition(targetTransform.myPosition - (myTransform.myPosition - targetTransform.myPosition).GetNormalized() * targetTransform.myRadius * radiusOffsetModifier);

	if (!ourManager->HasTag<TagAIController>(SEntityHandle(aEntityUniqueIndex)))
	{
		GAME_LOG("WARNING! Tried to call ArriveAtPosition on a non-AI-controlled object: Invalid.");
	}
	else
	{
		CAIController& aiController(ourManager->GetComponent<CompController>(SEntityHandle(aEntityUniqueIndex)).GetAI());

		aiController.myInitNavigationData.myShouldInitializePath = true;
		aiController.myInitNavigationData.myTargetPosition = offsetTargetPosition;
	}
}

void CGameLogicFunctions::LerpToEntity(const UniqueIndexType& aEntityUniqueIndex, const UniqueIndexType& aTargetEntityUniqueIndex)
{
	aEntityUniqueIndex; aTargetEntityUniqueIndex;
}

void CGameLogicFunctions::SmoothstepToEntity(const UniqueIndexType& aEntityUniqueIndex, const UniqueIndexType& aTargetEntityUniqueIndex)
{
	aEntityUniqueIndex; aTargetEntityUniqueIndex;
}

void CGameLogicFunctions::TeleportToEntity(const UniqueIndexType& aEntityUniqueIndex, const UniqueIndexType& aTargetEntityUniqueIndex)
{
	ERROR_CHECK_ENTITY_MANAGER(; , aEntityUniqueIndex, aTargetEntityUniqueIndex);
	CU::Vector3f& myPosition(ourManager->GetComponent<CompTransform>(SEntityHandle(aEntityUniqueIndex)).myPosition);
	const CU::Vector3f& targetPosition(ourManager->GetComponent<CompTransform>(SEntityHandle(aTargetEntityUniqueIndex)).myPosition);

	myPosition = targetPosition;
	UpdateEntityPosition(aEntityUniqueIndex, myPosition);
}

void CGameLogicFunctions::ScaleBy(const UniqueIndexType & aEntityUniqueIndex, const CU::Vector3f & aScale)
{
	ERROR_CHECK_ENTITY_MANAGER(; , aEntityUniqueIndex, 0);
	ourManager->GetComponent<CompTransform>(SEntityHandle(aEntityUniqueIndex)).myMatrix.ScaleBy(aScale);
}

void CGameLogicFunctions::SetScale(const UniqueIndexType& aEntityUniqueIndex, const CU::Vector3f& aScale)
{
	CompTransform& transform(ourManager->GetComponent<CompTransform>(SEntityHandle(aEntityUniqueIndex)));
	const auto& controller(ourManager->GetComponent<CompController>(SEntityHandle(aEntityUniqueIndex)).Get());

	const auto originalMatrix(controller.myTargetLookRotation.GenerateMatrix());

	CU::Vector3f scale;
	scale.x = transform.myMatrix[0] / originalMatrix[0];
	scale.y = transform.myMatrix[5] / originalMatrix[5];
	scale.z = transform.myMatrix[10] / originalMatrix[10];
	transform.myMatrix.ScaleBy(1.0f / scale);
	
	transform.myMatrix.ScaleBy(aScale);
}

void CGameLogicFunctions::LookAtEntity(const UniqueIndexType & aFirstEntityUniqueIndex, const UniqueIndexType & aSecondEntityUniqueIndex)
{
	ERROR_CHECK_ENTITY_MANAGER(; , aFirstEntityUniqueIndex, aSecondEntityUniqueIndex);

	const CU::Vector3f& myPosition(ourManager->GetComponent<CompTransform>(SEntityHandle(aFirstEntityUniqueIndex)).myPosition);
	const CU::Vector3f& targetPosition(ourManager->GetComponent<CompTransform>(SEntityHandle(aSecondEntityUniqueIndex)).myPosition);

	CU::Quaternion quat(CU::Quaternion::LookAt(myPosition, targetPosition));
	if (isnan(quat.myRotationAmount)) return;

	ourManager->GetComponent<CompController>(SEntityHandle(aFirstEntityUniqueIndex)).Get().myTargetLookRotation = quat;
}

void CGameLogicFunctions::LookAtPosition(const UniqueIndexType & aEntityUniqueIndex, const CU::Vector3f & aPosition)
{
	ERROR_CHECK_ENTITY_MANAGER(; , aEntityUniqueIndex, 0);

	SEntityHandle handle(aEntityUniqueIndex);
	const CU::Vector3f& myPosition(ourManager->GetComponent<CompTransform>(handle).myPosition);

	CU::Quaternion quat(CU::Quaternion::LookAt(myPosition, aPosition));
	if (isnan(quat.myRotationAmount)) return;

	ourManager->GetComponent<CompController>(handle).Get().myTargetLookRotation = quat;
}


void CGameLogicFunctions::LookAtEntityInverse(const UniqueIndexType & aFirstEntityUniqueIndex, const UniqueIndexType & aSecondEntityUniqueIndex)
{
	ERROR_CHECK_ENTITY_MANAGER(; , aFirstEntityUniqueIndex, aSecondEntityUniqueIndex);

	const CU::Vector3f& myPosition(ourManager->GetComponent<CompTransform>(SEntityHandle(aFirstEntityUniqueIndex)).myPosition);
	const CU::Vector3f& targetPosition(ourManager->GetComponent<CompTransform>(SEntityHandle(aSecondEntityUniqueIndex)).myPosition);

	CU::Quaternion quat(CU::Quaternion::LookAt(targetPosition, myPosition));
	if (isnan(quat.myRotationAmount)) return;

	ourManager->GetComponent<CompController>(SEntityHandle(aFirstEntityUniqueIndex)).Get().myTargetLookRotation = quat;
}

void CGameLogicFunctions::LookAtPositionInverse(const UniqueIndexType & aEntityUniqueIndex, const CU::Vector3f & aPosition)
{
	ERROR_CHECK_ENTITY_MANAGER(; , aEntityUniqueIndex, 0);

	SEntityHandle handle(aEntityUniqueIndex);
	const CU::Vector3f& myPosition(ourManager->GetComponent<CompTransform>(handle).myPosition);

	CU::Quaternion quat(CU::Quaternion::LookAt(aPosition, myPosition));
	if (isnan(quat.myRotationAmount)) return;

	ourManager->GetComponent<CompController>(handle).Get().myTargetLookRotation = quat;
}


void CGameLogicFunctions::RotateModel(const UniqueIndexType& aEntityUniqueIndex, const CU::Vector3f aRad)
{
	ERROR_CHECK_ENTITY_MANAGER(; , aEntityUniqueIndex, 0);
	ourManager->GetComponent<CompModelInstance>(SEntityHandle(aEntityUniqueIndex)).myModel.Rotate(aRad);
}

void CGameLogicFunctions::SetNavMeshTypeUnderEntity(const UniqueIndexType& aEntityUniqueIndex, const std::string& aNavMeshTypeNameReplace, const std::string& aNavMeshTypeNameWith)
{
	ERROR_CHECK_ENTITY_MANAGER(; , aEntityUniqueIndex, 0);

	auto getNavMeshType(
		[&aEntityUniqueIndex](const std::string& aNavMeshTypeName) -> MeshComponents::ENodeType
	{
		MeshComponents::ENodeType newNodeType(MeshComponents::ENodeType::eNot_Set);
		if (aNavMeshTypeName == "WALK_DASH")
		{
			newNodeType = MeshComponents::ENodeType::eWalk_Dash;
		}
		else if (aNavMeshTypeName == "DASH")
		{
			newNodeType = MeshComponents::ENodeType::eDash;
		}
		else if (aNavMeshTypeName == "NOTHING")
		{
			newNodeType = MeshComponents::ENodeType::eNothing;
		}
		else
		{
			GAME_LOG("ERROR! Tried to set NavMesh to unknown type \"%s\", with entity unique index (%llu)"
				, aNavMeshTypeName.c_str(), aEntityUniqueIndex);
			return MeshComponents::ENodeType::eNot_Set;
		}

		return newNodeType;
	});

	const MeshComponents::ENodeType newNodeTypeReplace(getNavMeshType(aNavMeshTypeNameReplace));
	const MeshComponents::ENodeType newNodeTypeWith(getNavMeshType(aNavMeshTypeNameWith));
	
	if (ourManager->HasComponent<CompTrigger>(SEntityHandle(aEntityUniqueIndex)))
	{
		const CompTrigger& boxCollider(ourManager->GetComponent<CompTrigger>(SEntityHandle(aEntityUniqueIndex)));
		
		CNavigationMeshWrapper::Get().SetNodeTypeInAABB(boxCollider.myMin, boxCollider.myMax, newNodeTypeReplace, newNodeTypeWith);
	}
	else
	{
		const CompTransform& entityTransform(ourManager->GetComponent<CompTransform>(SEntityHandle(aEntityUniqueIndex)));
		const CU::Collision::Sphere entitySphere(entityTransform.myPosition, entityTransform.myRadius + 1.0f);

		CNavigationMeshWrapper::Get().SetNodeTypeInSphere(entitySphere, newNodeTypeReplace, newNodeTypeWith);
	}
}

void CGameLogicFunctions::ResetNavMeshTypeUnderEntity(const UniqueIndexType& aEntityUniqueIndex)
{
	ERROR_CHECK_ENTITY_MANAGER(;, aEntityUniqueIndex, 0);

	const CompTransform& entityTransform(ourManager->GetComponent<CompTransform>(SEntityHandle(aEntityUniqueIndex)));
	const CU::Collision::Sphere entitySphere(entityTransform.myPosition, entityTransform.myRadius + 1.0f);

	CNavigationMeshWrapper::Get().ResetNodeTypeInSphere(entitySphere);
}

const CU::Vector3f CGameLogicFunctions::GetPositionFromAngleAndDistanceFromEntity(const UniqueIndexType & aEntityUniqueIndex, const float aAngleFromForward, const float aDistanceFromTarget)
{
	ERROR_CHECK_ENTITY_MANAGER(CU::Vector3f();, aEntityUniqueIndex, 0);

	CompTransform& entityTransform = ourManager->GetComponent<CompTransform>(SEntityHandle(aEntityUniqueIndex));
// 
// 	CU::Matrix44f forwardMatrix(entityTransform.myLookRotation.GenerateMatrix());
// 	forwardMatrix *= CU::Matrix44f::CreateRotateAroundY(CU::ToRadians(aAngleFromForward));
// 
// 	CU::Vector3f newForward(forwardMatrix[8], forwardMatrix[9], forwardMatrix[10]);
// 	newForward.Normalize();

	CU::Vector3f newForward(entityTransform.myLookRotation.GetForward());
	newForward = CU::Matrix44f::CreateRotateAroundY(CU::ToRadians(aAngleFromForward)) * newForward;
	newForward.Normalize();

	CU::Vector3f pos = (newForward * aDistanceFromTarget) + entityTransform.myPosition;
	return pos;
}

void CGameLogicFunctions::MakePlayer(const UniqueIndexType & aEntityUniqueIndex)
{
	ERROR_CHECK_ENTITY_MANAGER(; , aEntityUniqueIndex, 0);

	SEntityHandle handle(aEntityUniqueIndex);

	CController& previousController(ourManager->GetComponent<CompController>(handle).Get());
	SControllerWrapperInit controllerInitData(SControllerInit(), EController::Input, SInputInit({ 0.0f, 5.f, -10.f }));

	UniqueIndexType* scriptIndices(previousController.myScriptIndices);
	int* scriptNumbers(previousController.myScriptNumbers);
	std::string* scriptStrings(previousController.myScriptStrings);

	previousController.myScriptIndices = nullptr;
	previousController.myScriptNumbers = nullptr;
	previousController.myScriptStrings = nullptr;

	CController previousControllerCopy(previousController);

	CController& controller(ourManager->AddComponent<CompController>(handle, controllerInitData).Get());
	CompTransform& transform(ourManager->GetComponent<CompTransform>(handle));
	CompHealth& health(ourManager->AddComponent<CompHealth>(handle));
	health.Get() = 1;
	CompPlayerData& playerData(ourManager->AddComponent<CompPlayerData>(handle));
	playerData.myCurrentCheckpointIndex = -1;

	CompHUDElements& playerHUD(ourManager->AddComponent<CompHUDElements>(handle, static_cast<CompHUDElements::IndexType>(16)));
	playerHUD;

	ourManager->AddTag<TagInputController>(handle);
	ourManager->AddTag<TagPlayer>(handle);

	controller.myLoaded = previousControllerCopy.myLoaded;
	controller.myScriptStateIndex = previousControllerCopy.myScriptStateIndex;
	controller.myScriptFilePath = previousControllerCopy.myScriptFilePath;
	controller.myTargetLookRotation = transform.myLookRotation;
	controller.myScriptIndices = scriptIndices;
	controller.myScriptNumbers = scriptNumbers;
	controller.myScriptStrings = scriptStrings;

	SetPlayerEntityIndex(aEntityUniqueIndex);
}

void CGameLogicFunctions::MakeEnemy(const UniqueIndexType & aEntityUniqueIndex, bool aMovement)
{
	ERROR_CHECK_ENTITY_MANAGER(; , aEntityUniqueIndex, 0);

	SEntityHandle handle(aEntityUniqueIndex);

	CController& previousController(ourManager->GetComponent<CompController>(handle).Get());
	SControllerWrapperInit controllerInitData(SControllerInit(), EController::AI, SAIInit(eWorldInterfaceTag::Events));

	UniqueIndexType* scriptIndices(previousController.myScriptIndices);
	int* scriptNumbers(previousController.myScriptNumbers);
	std::string* scriptStrings(previousController.myScriptStrings);

	previousController.myScriptIndices = nullptr;
	previousController.myScriptNumbers = nullptr;
	previousController.myScriptStrings = nullptr;

	CController previousControllerCopy(previousController);

	CController& controller(ourManager->AddComponent<CompController>(handle, controllerInitData).Get());
	CompTransform& transform(ourManager->GetComponent<CompTransform>(handle));
	transform.myRadius *= 2.0f;
	CompHealth& health(ourManager->AddComponent<CompHealth>(handle));
	health.Get() = 1;

	ourManager->AddTag<TagAIController>(handle);
	ourManager->AddTag<TagEnemy>(handle);

	controller.myLoaded = previousControllerCopy.myLoaded;
	controller.myScriptStateIndex = previousControllerCopy.myScriptStateIndex;
	controller.myScriptFilePath = previousControllerCopy.myScriptFilePath;
	controller.myTargetLookRotation = transform.myLookRotation;
	controller.myScriptIndices = scriptIndices;
	controller.myScriptNumbers = scriptNumbers;
	controller.myScriptStrings = scriptStrings;

	if (aMovement)
	{
		ourManager->AddComponent<CompEvadeBehavior>(handle, 1.f);
	}
}

void CGameLogicFunctions::MakeCompanion(const UniqueIndexType & aEntityUniqueIndex)
{
	ERROR_CHECK_ENTITY_MANAGER(; , aEntityUniqueIndex, 0);

	SEntityHandle handle(aEntityUniqueIndex);

	CController& previousController(ourManager->GetComponent<CompController>(handle).Get());
	SControllerWrapperInit controllerInitData(SControllerInit(), EController::AI, SAIInit(eWorldInterfaceTag::Events));

	UniqueIndexType* scriptIndices(previousController.myScriptIndices);
	int* scriptNumbers(previousController.myScriptNumbers);
	std::string* scriptStrings(previousController.myScriptStrings);

	previousController.myScriptIndices = nullptr;
	previousController.myScriptNumbers = nullptr;
	previousController.myScriptStrings = nullptr;

	CController previousControllerCopy(previousController);

	CController& controller(ourManager->AddComponent<CompController>(handle, controllerInitData).Get());
	CompTransform& transform(ourManager->GetComponent<CompTransform>(handle));
	CompHealth& health(ourManager->AddComponent<CompHealth>(handle));
	health.Get() = 10;

	ourManager->AddTag<TagAIController>(handle);
	ourManager->AddTag<TagCompanion>(handle);

	controller.myLoaded = previousController.myLoaded;
	controller.myScriptStateIndex = previousController.myScriptStateIndex;
	controller.myScriptFilePath = previousController.myScriptFilePath;
	controller.myTargetLookRotation = transform.myLookRotation;
	controller.myScriptIndices = scriptIndices;
	controller.myScriptNumbers = scriptNumbers;
	controller.myScriptStrings = scriptStrings;

	ourManager->AddComponent<CompEvadeBehavior>(handle, 1.5f);

	SetCompanionEntityIndex(aEntityUniqueIndex);
}

void CGameLogicFunctions::MakeSpawnerPainting(const UniqueIndexType& aEntityUniqueIndex)
{
	ERROR_CHECK_ENTITY_MANAGER(; , aEntityUniqueIndex, 0);

	SEntityHandle handle(aEntityUniqueIndex);

	CController& previousController(ourManager->GetComponent<CompController>(handle).Get());
	SControllerWrapperInit controllerInitData(SControllerInit(), EController::AI, SAIInit(eWorldInterfaceTag::Events));

	UniqueIndexType* scriptIndices(previousController.myScriptIndices);
	int* scriptNumbers(previousController.myScriptNumbers);
	std::string* scriptStrings(previousController.myScriptStrings);

	previousController.myScriptIndices = nullptr;
	previousController.myScriptNumbers = nullptr;
	previousController.myScriptStrings = nullptr;

	CController previousControllerCopy(previousController);

	CController& controller(ourManager->AddComponent<CompController>(handle, controllerInitData).Get());
	CompTransform& transform(ourManager->GetComponent<CompTransform>(handle));
	CompHealth& health(ourManager->AddComponent<CompHealth>(handle));
	health.Get() = 1;

	CompHUDElements& enemyHUD(ourManager->AddComponent<CompHUDElements>(handle, static_cast<CompHUDElements::IndexType>(2)));
	enemyHUD;

	ourManager->AddTag<TagPainting>(handle);

	controller.myLoaded = previousControllerCopy.myLoaded;
	controller.myScriptStateIndex = previousControllerCopy.myScriptStateIndex;
	controller.myScriptFilePath = previousControllerCopy.myScriptFilePath;
	controller.myTargetLookRotation = transform.myLookRotation;
	controller.myScriptIndices = scriptIndices;
	controller.myScriptNumbers = scriptNumbers;
	controller.myScriptStrings = scriptStrings;

}

void CGameLogicFunctions::MakeBoss(const UniqueIndexType & aEntityUniqueIndex)
{
	ERROR_CHECK_ENTITY_MANAGER(; , aEntityUniqueIndex, 0);

	SEntityHandle handle(aEntityUniqueIndex);

	ourManager->AddTag<TagPainting>(handle);
	ourManager->AddTag<TagBoss>(handle);
}

void CGameLogicFunctions::MakeProjectile(const UniqueIndexType & aEntityUniqueIndex)
{
	ERROR_CHECK_ENTITY_MANAGER(; , aEntityUniqueIndex, 0);

	SEntityHandle handle(aEntityUniqueIndex);

	ourManager->AddTag<TagProjectile>(handle);

	ourManager->GetComponent<CompTransform>(handle).myRadius = 0.3f;
}

void CGameLogicFunctions::MakeHealthPickup(const UniqueIndexType & aEntityUniqueIndex)
{
	ERROR_CHECK_ENTITY_MANAGER(; , aEntityUniqueIndex, 0);

	SEntityHandle handle(aEntityUniqueIndex);

	ourManager->AddTag<TagHealthPickup>(handle);

	ourManager->GetComponent<CompTransform>(handle).myRadius = 0.666f;
}

void CGameLogicFunctions::MakeCheckpoint(const UniqueIndexType & aEntityUniqueIndex)
{
	ERROR_CHECK_ENTITY_MANAGER(;, aEntityUniqueIndex, 0);
	SEntityHandle handle(aEntityUniqueIndex);
	ourManager->AddTag<TagCheckpoint>(handle);
	ourManager->GetComponent<CompTransform>(handle).myRadius = 1.5f;
}

void CGameLogicFunctions::UpdateEntityPosition(const UniqueIndexType& aEntityUniqueIndex, const CU::Vector3f& aNewPosition)
{
	ERROC_CHECK_LEVEL(;);
	ourLevel->QueueUpdateActiveObject(aEntityUniqueIndex, aNewPosition);
}

void CGameLogicFunctions::KillEntity(const UniqueIndexType& aEntityUniqueIndex)
{
	ERROR_CHECK_ENTITY_MANAGER(; , aEntityUniqueIndex, 0);

	SEntityHandle handle(aEntityUniqueIndex);
	ourManager->Kill(handle);
	if (ourLevel != nullptr)
	{
		ourLevel->QueueRemoveFromActiveObjects(handle.myUniqueIndex);
	}

	if (aEntityUniqueIndex == ourPlayerEntityIndex)
	{
		ourPlayerEntityIndex = EntityHandleINVALID.myUniqueIndex;
	}
}

void CGameLogicFunctions::SetToDestroyOnLeaveGrid(const UniqueIndexType & aEntityUniqueIndex, std::function<void()> aAdditionalRemoveObjectCode)
{
	ERROR_CHECK_ENTITY_MANAGER(;, aEntityUniqueIndex, 0);
	ERROC_CHECK_LEVEL(;);

	auto gridObjectOnLeaveCallback = [aEntityUniqueIndex, aAdditionalRemoveObjectCode]()
	{
		aAdditionalRemoveObjectCode();
		ourManager->Kill(SEntityHandle(aEntityUniqueIndex));
	};

	ourLevel->QueueSetOnLeaveGridCallback(aEntityUniqueIndex, gridObjectOnLeaveCallback);
}

void CGameLogicFunctions::KillEntity_ThreadSafe(const UniqueIndexType& aEntityUniqueIndex)
{
	ERROR_CHECK_ENTITY_MANAGER(; , aEntityUniqueIndex, 0);

	SEntityHandle handle(aEntityUniqueIndex);
	ourManager->Kill_ThreadSafe(handle);

	if (aEntityUniqueIndex == ourPlayerEntityIndex)
	{
		ourPlayerEntityIndex = EntityHandleINVALID.myUniqueIndex;
	}
}

void CGameLogicFunctions::Respawn()
{
	ERROR_CHECK_ENTITY_MANAGER(; , ourPlayerEntityIndex, 0);

	UniqueIndexType checkpoint(ourManager->GetComponent<CompPlayerData>(SEntityHandle(ourPlayerEntityIndex)).myCurrentCheckpointIndex);

	//Fade out, move to position, reset health
	auto resetPlayer(
		[checkpoint]()
	{
		TeleportToEntity(ourPlayerEntityIndex, checkpoint);
		//TeleportToEntity(ourSupportCompanionIndex, ourPlayerEntityIndex);
		CScriptSystem::CallScriptEventOnTarget(ourSupportCompanionIndex, "OnPlayerRespawn");

		SetHealth(ourPlayerEntityIndex, ourManager->GetComponent<CompHealth>(SEntityHandle(ourPlayerEntityIndex)).myMaxHealth);
		CScriptSystem::CallScriptEventOnTarget(ourPlayerEntityIndex, "OnPlayerRespawned");
		CScriptSystem::CallScriptEventOnTarget(ourPlayerEntityIndex, "OnFadeEclipse");
	});

	CGameSystems::Get()->GetSystem<CRenderSystem>()->ActivateCanvasFade(0.75f, resetPlayer);
}

void CGameLogicFunctions::SetCurrentCheckpoint(const UniqueIndexType & aEntityUniqueIndex)
{
	ERROR_CHECK_ENTITY_MANAGER(; , aEntityUniqueIndex, ourPlayerEntityIndex);

	ourManager->GetComponent<CompPlayerData>(SEntityHandle(ourPlayerEntityIndex)).myCurrentCheckpointIndex = aEntityUniqueIndex;
}

void CGameLogicFunctions::TakeDamage(const UniqueIndexType & aEntityUniqueIndex, const float aDamage)
{
	ERROR_CHECK_ENTITY_MANAGER(; , aEntityUniqueIndex, 0);
	CompHealth& healthComp(ourManager->GetComponent<CompHealth>(SEntityHandle(aEntityUniqueIndex)));
	float& health(healthComp.Get());

	if (health > 0)
	{
		if (health - aDamage <= 0)
		{
			CScriptSystem::CallScriptEventOnTarget(aEntityUniqueIndex, "OnHealthReachedZero");
			//ourManager->Kill(SEntityHandle(aEntityUniqueIndex));
			health = 0;
		}
		else
		{
			health -= aDamage;
		}
	}
	
	health = (health > healthComp.myMaxHealth) ? healthComp.myMaxHealth : health;

	//Call from script system instead so that callerID becomes available
	//CScriptSystem::CallScriptEventOnTarget(aEntityUniqueIndex, "OnDamageTaken", aDamage);
}

void CGameLogicFunctions::SetMaxHealth(const UniqueIndexType & aEntityUniqueIndex, const float aHealth)
{
	ERROR_CHECK_ENTITY_MANAGER(; , aEntityUniqueIndex, 0);
	CompHealth& health(ourManager->GetComponent<CompHealth>(SEntityHandle(aEntityUniqueIndex)));

	if (aHealth > 0)
	{
		health.myMaxHealth = aHealth;
		health.myHealth = aHealth;
	}
	else
	{
		GAME_LOG("WARNING! Tried to set max health of an object to a negative number.");
	}
}

void CGameLogicFunctions::SetHealth(const UniqueIndexType & aEntityUniqueIndex, const float aHealth)
{
	ERROR_CHECK_ENTITY_MANAGER(; , aEntityUniqueIndex, 0);
	float& health(ourManager->GetComponent<CompHealth>(SEntityHandle(aEntityUniqueIndex)).Get());
	health = (aHealth > 0) ? aHealth : 0;
}

const float CGameLogicFunctions::GetHealth(const UniqueIndexType & aEntityUniqueIndex)
{
	ERROR_CHECK_ENTITY_MANAGER(-1; , aEntityUniqueIndex, 0);
	return ourManager->GetComponent<CompHealth>(SEntityHandle(aEntityUniqueIndex)).Get();
}

const float CGameLogicFunctions::GetMaxHealth(const UniqueIndexType & aEntityUniqueIndex)
{
	ERROR_CHECK_ENTITY_MANAGER(-1;, aEntityUniqueIndex, 0);
	return ourManager->GetComponent<CompHealth>(SEntityHandle(aEntityUniqueIndex)).myMaxHealth;
}

bool CGameLogicFunctions::IsAlive(const UniqueIndexType & aEntityUniqueIndex)
{
	if (ourManager == nullptr)																	
	{																								
		GAME_LOG("ERROR! EntityManager not bound to CGameLogicFunctions");							
		return false;																		
	}																								
	if (!ourManager->IsHandleValidNoAssert(SEntityHandle(aEntityUniqueIndex)))									
	{																								
		return false;																	
	}

	if (ourManager->IsAlive(SEntityHandle(aEntityUniqueIndex)))
	{
		return ourManager->GetComponent<CompHealth>(SEntityHandle(aEntityUniqueIndex)).Get() > 0;
	}
	
	return false;
}

void CGameLogicFunctions::SetShouldGoToCredits(const bool aShouldGoToCredits)
{
	ourShouldGoToCredits = aShouldGoToCredits;
}

void CGameLogicFunctions::SpawnParticleOnEntity(const std::string& aParticleName, const UniqueIndexType& aEntityUniqueIndex)
{
	SpawnParticleOnPosition(aParticleName, ourManager->GetComponent<CompTransform>(SEntityHandle(aEntityUniqueIndex)).myPosition);
}

void CGameLogicFunctions::SpawnParticleOnPosition(const std::string& aParticleName, const CU::Vector3f& aPosition)
{
	ERROR_CHECK_ENTITY_MANAGER(; , 0, 0);
	ERROC_CHECK_LEVEL(;);

	CEntityFactory::SEntityCreationData initData;
	initData.myParticleData.myIsValid = true;
	initData.myParticleData.myParticlePath = "Data/Particles/" + aParticleName + ".particle";

	initData.myPosition = aPosition;

	SEntityHandle particle(CEntityFactory::CreateEntity(initData));

	if (initData.myParticleData.myIsValid == false)
	{
		ourManager->Kill(particle);
		return;
	}

	SGridData gridObject;
	gridObject.myEntityUniqueIndex = particle.myUniqueIndex;
	gridObject.mySize = 1.0f;
	gridObject.myCenterPosition = { initData.myPosition.x, initData.myPosition.z };

	ourLevel->QueueAddToActiveObjects(gridObject);
}

void CGameLogicFunctions::SpawnObjectOnEntity(const UniqueIndexType & aEntityUniqueIndex, const char * aScriptName)
{
	ERROR_CHECK_ENTITY_MANAGER(;, aEntityUniqueIndex, 0);
	ERROC_CHECK_LEVEL(;);

	const CU::Vector3f& myPosition(ourManager->GetComponent<CompTransform>(SEntityHandle(aEntityUniqueIndex)).myPosition);

	CEntityFactory::SEntityCreationData initData;

	initData.myPosition = myPosition;
	initData.myScriptData.myIndices = sce_newArray(UniqueIndexType, 1);
	initData.myScriptData.myIndices[0] = GetPlayerUniqueIndex();
	std::string fullPath(aScriptName);
	fullPath = "Data/Scripts/" + fullPath + ".lua";
	initData.myScriptData.myScriptFilePath = fullPath;

	SEntityHandle handle(CEntityFactory::CreateEntity(initData));

	initData.myScriptData.myOnInit(initData.myScriptData.myIndices, 1, nullptr, 0, nullptr, 0);

	SGridData gridData;
	gridData.myCenterPosition = { initData.myPosition.x, initData.myPosition.z };
	gridData.mySize = { 1.0f, 1.0f };
	gridData.myEntityUniqueIndex = handle.myUniqueIndex;

	ourLevel->QueueAddToActiveObjects(gridData);
}

const UniqueIndexType CGameLogicFunctions::SpawnModelAtPosition(const UniqueIndexType& aEntityUniqueIndex, const char* aModelName, const char* aMaterialName, const CU::Vector3f& aPosition)
{
	ERROR_CHECK_ENTITY_MANAGER(0;, aEntityUniqueIndex, 0);
	ERROC_CHECK_LEVEL(0;);

	CEntityFactory::SEntityCreationData initData;

	initData.myPosition = aPosition;
	initData.myModelFilePath = aModelName;
	initData.myMaterialFilePath = std::string("Data/Materials/") + aMaterialName + ".material";
	initData.myScale = CU::Vector3f(1.5f, 1.5f, 1.5f);

	SEntityHandle handle(CEntityFactory::CreateEntity(initData));

	SGridData gridData;
	gridData.myCenterPosition = { initData.myPosition.x, initData.myPosition.z };
	gridData.mySize = { 1.0f, 1.0f };
	gridData.myEntityUniqueIndex = handle.myUniqueIndex;

	ourLevel->QueueAddToActiveObjects(gridData);
	return handle.myUniqueIndex;
}

void CGameLogicFunctions::SpawnEnemy(const UniqueIndexType& aPaintingIndex, CU::Vector3f aPosition)
{

	ERROR_CHECK_ENTITY_MANAGER(;, 0, 0);
	ERROC_CHECK_LEVEL(;);

	CEntityFactory::SEntityCreationData objectData;

	objectData.myRadius = 1.f;

	objectData.myModelFilePath = "Data/Models/Enemies/mdl_e_hellhound.fbx";
	objectData.myScriptData.myScriptFilePath = "Data/Scripts/lungingEnemy.lua";
	objectData.myMaterialFilePath = "Data/Materials/hellhound.material";

	objectData.myPosition = aPosition;
	objectData.myScriptData.myIndices = sce_newArray(UniqueIndexType, 2);
	objectData.myScriptData.myIndices[0] = GetPlayerUniqueIndex();
	objectData.myScriptData.myIndices[1] = aPaintingIndex;

	SEntityHandle objectHandle(CEntityFactory::CreateEntity(objectData));

	objectData.myScriptData.myOnInit(objectData.myScriptData.myIndices, 2, nullptr, 0, nullptr, 0);

	SGridData gridData;
	gridData.myCenterPosition = { aPosition.x, aPosition.z };
	gridData.mySize = { 1.0f, 1.0f }; // TODO: Fix size
	gridData.myEntityUniqueIndex = objectHandle.myUniqueIndex;

	ourLevel->QueueAddToActiveObjects(gridData);
}

void CGameLogicFunctions::SpawnPainting(const CU::Vector3f& aPosition, const CU::Vector3f& aRotation)
{
	ERROR_CHECK_ENTITY_MANAGER(;, 0, 0);
	ERROC_CHECK_LEVEL(;);

	CEntityFactory::SEntityCreationData objectData;

	//objectData.myModelFilePath = "Data/Models/MasterWorks/Wave2/enemy_spawningFrame2.fbx";
	objectData.myScriptData.myScriptFilePath = "Data/Scripts/spawnerPainting.lua";
	//objectData.myMaterialFilePath = "Data/Materials/hellhound.material";

	objectData.myRadius = 1.f;

	objectData.myPosition = aPosition;
	objectData.myRotation = { CU::ToRadians(aRotation.x), CU::ToRadians(aRotation.y), CU::ToRadians(aRotation.z) };
	objectData.myScriptData.myIndices = sce_newArray(UniqueIndexType, 1);
	objectData.myScriptData.myIndices[0] = GetPlayerUniqueIndex();

	SEntityHandle objectHandle(CEntityFactory::CreateEntity(objectData));

	objectData.myScriptData.myOnInit(objectData.myScriptData.myIndices, 1, nullptr, 0, nullptr, 0);

	SGridData gridData;
	gridData.myCenterPosition = { aPosition.x, aPosition.z };
	gridData.mySize = { 1.0f, 1.0f }; // TODO: Fix size
	gridData.myEntityUniqueIndex = objectHandle.myUniqueIndex;

	ourLevel->QueueAddToActiveObjects(gridData);
}

void CGameLogicFunctions::SpawnProjectileDebris(const UniqueIndexType& aProjectileIndex)
{
	ERROR_CHECK_ENTITY_MANAGER(;, 0, 0);
	ERROC_CHECK_LEVEL(;);

	const CompTransform& projectileTransform(ourManager->GetComponent<CompTransform>(SEntityHandle(aProjectileIndex)));

	const CU::Vector3f backwards(projectileTransform.myLookRotation.GetBackward());

	CEntityFactory::SEntityCreationData objectData;

	objectData.myRadius = 1.f;

	objectData.myModelFilePath = "Data/Models/Player/SplashMesh.fbx";
	objectData.myScriptData.myScriptFilePath = "Data/Scripts/projectileSplash.lua";
	objectData.myMaterialFilePath = "Data/Materials/projectileSplash.material";

	objectData.myPosition = projectileTransform.myPosition;
	objectData.myRotation = backwards;

	SEntityHandle objectHandle(CEntityFactory::CreateEntity(objectData));

	ourManager->AddTag<TagProjectile>(objectHandle);

	objectData.myScriptData.myOnInit(nullptr, 0, nullptr, 0, nullptr, 0);

	SGridData gridData;
	gridData.myCenterPosition = { objectData.myPosition.x, objectData.myPosition.z };
	gridData.mySize = { 1.0f, 1.0f }; // TODO: Fix size
	gridData.myEntityUniqueIndex = objectHandle.myUniqueIndex;

	ourLevel->QueueAddToActiveObjects(gridData);
}

void CGameLogicFunctions::SetModelName(const UniqueIndexType & aEntityUniqueIndex, const char * aModelName)
{
	ERROR_CHECK_ENTITY_MANAGER(; , aEntityUniqueIndex, 0);

	CompModelInstance& model(ourManager->AddComponent<CompModelInstance>(SEntityHandle(aEntityUniqueIndex)));
	model.myModel.SetModelPath(aModelName);
}

void CGameLogicFunctions::SetMaterial(const UniqueIndexType & aEntityUniqueIndex, const char * aMaterialName)
{
	ERROR_CHECK_ENTITY_MANAGER(; , aEntityUniqueIndex, 0);

	CompModelInstance& model(ourManager->GetComponent<CompModelInstance>(SEntityHandle(aEntityUniqueIndex)));
	std::string path("Data/Materials/");
	path = path + aMaterialName;
	path = path + ".material";
	model.myModel.SetMaterialPath(path);
}

CompHUDElements::IndexType CGameLogicFunctions::AddHUDElement(const UniqueIndexType& aEntityUniqueIndex, const char* aSpritePath)
{
	ERROR_CHECK_ENTITY_MANAGER(CompHUDElements::invalid; , aEntityUniqueIndex, 0);

	//CompHUDElements& hudElements(ourManager->GetComponent<CompHUDElements>(SEntityHandle(aEntityUniqueIndex)));
	
	//CompHUDElements::IndexType i = hudElements.Add(aSpritePath);

	//hudElements.Get(i).SetOrientation(CU::Matrix44f::CreateRotateAroundY(0.15f));

	return 0;
}

void CGameLogicFunctions::SetHUDElementPosition(const UniqueIndexType& aEntityUniqueIndex, const CompHUDElements::IndexType& aHUDIndex, const CU::Vector2f& aPosition)
{
	ERROR_CHECK_ENTITY_MANAGER(; , aEntityUniqueIndex, 0);

	if (aHUDIndex == static_cast<CompHUDElements::IndexType>(-1))
	{
		GAME_LOG("WARNING! Tried to set position on invalid HUD element, owner index (%llu)"
			, aEntityUniqueIndex);
		return;
	}

	//CompHUDElements& hudElements(ourManager->GetComponent<CompHUDElements>(SEntityHandle(aEntityUniqueIndex)));

	//auto& sprite(hudElements.Get(aHUDIndex));
	//sprite.SetPosition(aPosition);
}

void CGameLogicFunctions::SetHUDElementScale(const UniqueIndexType& aEntityUniqueIndex, const CompHUDElements::IndexType& aHUDIndex, const CU::Vector2f& aScale)
{
	ERROR_CHECK_ENTITY_MANAGER(; , aEntityUniqueIndex, 0);

	//CompHUDElements& hudElements(ourManager->GetComponent<CompHUDElements>(SEntityHandle(aEntityUniqueIndex)));
	//
	//auto& sprite(hudElements.Get(aHUDIndex));
	//sprite.SetScale(aScale);
}

void CGameLogicFunctions::SetHUDElementCrop(const UniqueIndexType& aEntityUniqueIndex, const CompHUDElements::IndexType& aHUDIndex, const CU::Vector4f& aCrop)
{
	ERROR_CHECK_ENTITY_MANAGER(; , aEntityUniqueIndex, 0);

	//CompHUDElements& hudElements(ourManager->GetComponent<CompHUDElements>(SEntityHandle(aEntityUniqueIndex)));
	//
	//auto& sprite(hudElements.Get(aHUDIndex));
	//sprite.SetCrop(aCrop);
}

void CGameLogicFunctions::SetHUDElementPivot(const UniqueIndexType & aEntityUniqueIndex, const CompHUDElements::IndexType & aHUDIndex, const CU::Vector2f & aPivot)
{
	ERROR_CHECK_ENTITY_MANAGER(; , aEntityUniqueIndex, 0);

	//CompHUDElements& hudElements(ourManager->GetComponent<CompHUDElements>(SEntityHandle(aEntityUniqueIndex)));
	//
	//auto& sprite(hudElements.Get(aHUDIndex));
	//sprite.SetPivot(aPivot);
}

CU::Vector2f CGameLogicFunctions::GetPositionProjectedToScreen(const CU::Vector3f & aPosition)
{
	//HACKE HACKSPETT
	ERROR_CHECK_ENTITY_MANAGER(CU::Vector2f(), 0, 0);
	return ourManager->GetComponent<CompCameraInstance>(SEntityHandle(0)).myInstance.GetScreenPosition(aPosition);
}

const CU::Vector2f CGameLogicFunctions::GetHUDElementSpriteSize(const UniqueIndexType & aEntityUniqueIndex, const CompHUDElements::IndexType & aHUDIndex)
{
	ERROR_CHECK_ENTITY_MANAGER(CU::Vector2f(); , aEntityUniqueIndex, 0);

	//CompHUDElements& hudElements(ourManager->GetComponent<CompHUDElements>(SEntityHandle(aEntityUniqueIndex)));
	//
	//auto& sprite(hudElements.Get(aHUDIndex));
	//return sprite.GetScreenSpaceSize();
}

void CGameLogicFunctions::SetBlackBarsRadius(const float aRadius)
{
	CGameSystems::Get()->GetSystem<CRenderSystem>()->SetBlackBars(aRadius);
}

const std::string CGameLogicFunctions::GetEnemyName(const UniqueIndexType & aEntityUniqueIndex)
{
	ERROR_CHECK_ENTITY_MANAGER("", aEntityUniqueIndex, 0);

	SEntityHandle handle(aEntityUniqueIndex);

	if (ourManager->HasTag<TagEnemy>(handle))
	{
		if (ourManager->HasComponent<CompEvadeBehavior>(handle))
		{
			return "Caster";
		}
		else
		{
			return "Hellhound";
		}
	}
	if (ourManager->HasTag<TagPainting>(handle))
	{
		return "Painting";
	}
	if (ourManager->HasTag<TagBoss>(handle))
	{
		return "Painter";
	}

	assert(false && "Tried to get name of non-enemy");
	return std::string();
}

void CGameLogicFunctions::SetAnimation(const UniqueIndexType & aEntityUniqueIndex, const char * aAnimationTag)
{
	ERROR_CHECK_ENTITY_MANAGER(; , aEntityUniqueIndex, 0);

	ourManager->GetComponent<CompModelInstance>(SEntityHandle(aEntityUniqueIndex)).myModel.SetAnimationWithTag(aAnimationTag);
}

void CGameLogicFunctions::SetAnimationWithIndex(const UniqueIndexType & aEntityUniqueIndex, const unsigned int aAnimationIndex)
{
	ERROR_CHECK_ENTITY_MANAGER(;, aEntityUniqueIndex, 0);

	ourManager->GetComponent<CompModelInstance>(SEntityHandle(aEntityUniqueIndex)).myModel.SetAnimationWithIndex(aAnimationIndex);

}

void CGameLogicFunctions::SetAnimationSpeed(const UniqueIndexType & aEntityUniqueIndex, const float aAnimationSpeed)
{
	ERROR_CHECK_ENTITY_MANAGER(; , aEntityUniqueIndex, 0);
	ourManager->GetComponent<CompModelInstance>(SEntityHandle(aEntityUniqueIndex)).myModel.SetAnimationSpeed(aAnimationSpeed);
}

void CGameLogicFunctions::PauseAnimation(const UniqueIndexType & aEntityUniqueIndex)
{
	ERROR_CHECK_ENTITY_MANAGER(; , aEntityUniqueIndex, 0);
	ourManager->GetComponent<CompModelInstance>(SEntityHandle(aEntityUniqueIndex)).myModel.PauseAnimation();
}

void CGameLogicFunctions::PlayAnimation(const UniqueIndexType & aEntityUniqueIndex)
{
	ERROR_CHECK_ENTITY_MANAGER(; , aEntityUniqueIndex, 0);
	ourManager->GetComponent<CompModelInstance>(SEntityHandle(aEntityUniqueIndex)).myModel.PlayAnimation();
}

void CGameLogicFunctions::StopAnimation(const UniqueIndexType & aEntityUniqueIndex)
{
	ERROR_CHECK_ENTITY_MANAGER(; , aEntityUniqueIndex, 0);
	ourManager->GetComponent<CompModelInstance>(SEntityHandle(aEntityUniqueIndex)).myModel.StopAnimation();
}

const float CGameLogicFunctions::GetAnimationDuration(const UniqueIndexType& aEntityUniqueIndex)
{
	ERROR_CHECK_ENTITY_MANAGER(0.f; , aEntityUniqueIndex, 0);
	return ourManager->GetComponent<CompModelInstance>(SEntityHandle(aEntityUniqueIndex)).myModel.GetAnimationDuration();
}

void CGameLogicFunctions::RotateBridge(const UniqueIndexType & aEntityUniqueIndex)
{
	ERROR_CHECK_ENTITY_MANAGER(; , aEntityUniqueIndex, 0);
	auto& transform(ourManager->GetComponent<CompTransform>(SEntityHandle(aEntityUniqueIndex)));
	transform.myLookRotation.RotateLocalZ(CU::ToRadians(-90.f));
	ourManager->GetComponent<CompController>(SEntityHandle(aEntityUniqueIndex)).Get().myTargetLookRotation = transform.myLookRotation;
	transform.myMatrix.SetRotation(transform.myLookRotation.GenerateMatrix());
}

const CU::Vector4f CGameLogicFunctions::BridgeGetStart(const UniqueIndexType& aEntityUniqueIndex)
{
	ERROR_CHECK_ENTITY_MANAGER(CU::Vector4f(); , aEntityUniqueIndex, 0);
	auto& transform(ourManager->GetComponent<CompTransform>(SEntityHandle(aEntityUniqueIndex)));
	return CU::Vector4f(transform.myLookRotation.myRotation, transform.myLookRotation.myRotationAmount);
}

const CU::Vector4f CGameLogicFunctions::BridgeGetEnd(const UniqueIndexType& aEntityUniqueIndex)
{
	ERROR_CHECK_ENTITY_MANAGER(CU::Vector4f();, aEntityUniqueIndex, 0);
	CU::Quaternion preQuat(ourManager->GetComponent<CompTransform>(SEntityHandle(aEntityUniqueIndex)).myLookRotation);
	CU::Quaternion rotatedQuat;
	rotatedQuat.RotateLocalZ(CU::ToRadians(90.f));

	preQuat *= rotatedQuat;
	return CU::Vector4f(preQuat.myRotation, preQuat.myRotationAmount);
}

void CGameLogicFunctions::BridgeSlerp(const UniqueIndexType& aEntityUniqueIndex, const CU::Quaternion& aStartQuaternion, const CU::Quaternion& aEndQuaternion, const float aProgress)
{
	ERROR_CHECK_ENTITY_MANAGER(;, aEntityUniqueIndex, 0);
	auto& transform(ourManager->GetComponent<CompTransform>(SEntityHandle(aEntityUniqueIndex)));
	auto& controller(ourManager->GetComponent<CompController>(SEntityHandle(aEntityUniqueIndex)));

	CU::Quaternion result(CU::Quaternion::Slerp(aStartQuaternion, aEndQuaternion, aProgress));
	transform.myLookRotation = result;
	controller.Get().myTargetLookRotation = result;
	transform.myMatrix.SetRotation(transform.myLookRotation.GenerateMatrix());
}

float CGameLogicFunctions::GetDistanceBetweenEntitiesSquared(const UniqueIndexType& aFirstEntityUniqueIndex, const UniqueIndexType& aSecondEntityUniqueIndex)
{
	ERROR_CHECK_ENTITY_MANAGER(0; , aFirstEntityUniqueIndex, aSecondEntityUniqueIndex);

	const CU::Vector3f& firstPosition(ourManager->GetComponent<CompTransform>(SEntityHandle(aFirstEntityUniqueIndex)).myPosition);
	const CU::Vector3f& secondPosition(ourManager->GetComponent<CompTransform>(SEntityHandle(aSecondEntityUniqueIndex)).myPosition);

	return firstPosition.Distance2(secondPosition);
}

float CGameLogicFunctions::GetDistanceFromEntityToPositionSquared(const UniqueIndexType& aFirstEntityUniqueIndex, const CU::Vector3f& aPosition)
{
	ERROR_CHECK_ENTITY_MANAGER(0; , aFirstEntityUniqueIndex, 0);

	const CU::Vector3f& firstPosition(ourManager->GetComponent<CompTransform>(SEntityHandle(aFirstEntityUniqueIndex)).myPosition);
	return firstPosition.Distance2(aPosition);
}

const UniqueIndexType CGameLogicFunctions::GetClosestEnemy(const UniqueIndexType& aEntityUniqueIndex, const float aRadius)
{
	ERROR_CHECK_ENTITY_MANAGER(EntityHandleINVALID.myUniqueIndex; , aEntityUniqueIndex, 0)

	const CU::Vector3f& callerPosition(ourManager->GetComponent<CompTransform>(SEntityHandle(aEntityUniqueIndex)).myPosition);
	float closest(100.f);
	UniqueIndexType closestID(EntityHandleINVALID.myUniqueIndex);

	auto getClosest(
		[&ourManager = ourManager, &callerPosition, &closest, &closestID, &aRadius](auto& aIndex)
	{
		const CU::Vector3f& closestRef(ourManager->GetComponent<CompTransform>(aIndex).myPosition);
		float closestRefDistance((closestRef - callerPosition).Length2());
		if (closestRefDistance < (aRadius * aRadius))
		{
			if (closestRefDistance < closest)
			{
				closest = closestRefDistance;
				closestID = ourManager->GetComponent<CompHandle>(aIndex).myHandle.myUniqueIndex;
			}
		}
	});

	ourManager->ForEntitiesMatching<SigEnemyTag>(getClosest, ourEntities);

	return closestID;
}

const UniqueIndexType CGameLogicFunctions::GetClosestPainting(const UniqueIndexType& aEntityUniqueIndex, const float aRadius)
{
	ERROR_CHECK_ENTITY_MANAGER(EntityHandleINVALID.myUniqueIndex; , aEntityUniqueIndex, 0)

		const CU::Vector3f& callerPosition(ourManager->GetComponent<CompTransform>(SEntityHandle(aEntityUniqueIndex)).myPosition);
	float closest(1000.f);
	UniqueIndexType closestID(EntityHandleINVALID.myUniqueIndex);

	auto getClosest(
		[&ourManager = ourManager, &callerPosition, &closest, &closestID, &aRadius](auto& aIndex)
	{
		const CU::Vector3f& closestRef(ourManager->GetComponent<CompTransform>(aIndex).myPosition);
		float closestRefDistance((closestRef - callerPosition).Length2());
		if (closestRefDistance < (aRadius * aRadius))
		{
			if (closestRefDistance < closest)
			{
				closest = closestRefDistance;
				closestID = ourManager->GetComponent<CompHandle>(aIndex).myHandle.myUniqueIndex;
			}
		}
	});

	ourManager->ForEntitiesMatching<SigPaintingTag>(getClosest, ourEntities);

	return closestID;
}

const UniqueIndexType CGameLogicFunctions::GetInvalidEntityIndex()
{
	return EntityHandleINVALID.myUniqueIndex;
}

const bool CGameLogicFunctions::IsEntityValid(const UniqueIndexType& aEntityUniqueIndex)
{
	if (aEntityUniqueIndex == EntityHandleINVALID.myUniqueIndex)
	{
		return false;
	}

	return ourManager->IsHandleValidNoAssert(SEntityHandle(aEntityUniqueIndex));
}

void CGameLogicFunctions::SwitchLevelTo(const std::string& aLevelPath)
{
	ERROC_CHECK_LEVEL(;);

	ourLevel->QueueLevelSwitch(aLevelPath);
}

void CGameLogicFunctions::SetMovementSpeed(const UniqueIndexType & aEntityUniqueIndex, float aSpeed)
{
	ERROR_CHECK_ENTITY_MANAGER(; , aEntityUniqueIndex, 0);
	ourManager->GetComponent<CompController>(SEntityHandle(aEntityUniqueIndex)).Get().myMaxSpeed = aSpeed;
}

void CGameLogicFunctions::SetAcceleration(const UniqueIndexType & aEntityUniqueIndex, float aAcceleration)
{
	ERROR_CHECK_ENTITY_MANAGER(; , aEntityUniqueIndex, 0);
	ourManager->GetComponent<CompController>(SEntityHandle(aEntityUniqueIndex)).Get().myMaxForce = aAcceleration;
}

void CGameLogicFunctions::SetRotationSpeed(const UniqueIndexType & aEntityUniqueIndex, float aRotationSpeed)
{
	ERROR_CHECK_ENTITY_MANAGER(; , aEntityUniqueIndex, 0);
	ourManager->GetComponent<CompController>(SEntityHandle(aEntityUniqueIndex)).Get().myRotationSpeed = aRotationSpeed;
}

void CGameLogicFunctions::SetLevel(CLevel* aLevel)
{
	ourLevel = aLevel;
}

void CGameLogicFunctions::ClearNavMeshPath(const UniqueIndexType& aEntityUniqueIndex)
{
	ERROR_CHECK_ENTITY_MANAGER(; , aEntityUniqueIndex, 0);

	const SEntityHandle handle(aEntityUniqueIndex);
	ourManager->DelComponent<CompArriveBehavior>(handle);

	auto& path_ref(ourManager->GetComponent<CompController>(handle).Get().myCurrentPath);
	auto& velocity_ref(ourManager->GetComponent<CompController>(handle).Get().myVelocity);
	velocity_ref.x = 0.0f;
	velocity_ref.y = 0.0f;
	velocity_ref.z = 0.0f;

	path_ref.RemoveAll();
}

const CU::Vector3f CGameLogicFunctions::DrawLineFromEntityToMouse(const UniqueIndexType & aFromIndex)
{
	CU::Vector3f fromPos = ourManager->GetComponent<CompTransform>(aFromIndex).myPosition;
	CU::Vector3f mousePos;
	CNavigationMeshWrapper::Get().GetIntersectionWithNavMesh(CMouseInterface::Get()->GetMouseToNavmeshRay(), mousePos);
	DT_DRAW_LINE_2_ARGS(fromPos, mousePos);
	return mousePos;
}

void CGameLogicFunctions::TossHealingAbility(const UniqueIndexType& aEntityUniqueIndex, const CU::Vector3f aHealPos,
	const float aHealLifeTime, const float aHealCircleRadius, const float aHealingPerTick, const float aHealingTickInterval,
	const float aDamageLifeTime, const float aDamageCircleRadius, const float aDamagePerSecond, const float aDamageZoneSpawnInterval, const float aFlightSpeed)
{
	auto gameSystem(CGameSystems::Get());

	if (gameSystem == nullptr)
	{
		GAME_LOG("ERROR! GameSystem has not been activated")
		return;
	}

	auto healingCircleSystem(gameSystem->GetSystem<CHealingCircleSystem>());

	if (healingCircleSystem == nullptr)
	{
		GAME_LOG("ERROR! HealingCircleSystem has not been activated")
		return;
	}

	CU::Vector3f entityPosition = ourManager->GetComponent<CompTransform>(SEntityHandle(aEntityUniqueIndex)).myPosition;
	healingCircleSystem->TossHealing(entityPosition, aHealPos, aHealLifeTime, aHealCircleRadius, aHealingPerTick, aHealingTickInterval, 
									aDamageLifeTime, aDamageCircleRadius, aDamagePerSecond, aDamageZoneSpawnInterval, aFlightSpeed);
}

const UniqueIndexType CGameLogicFunctions::GetPlayerUniqueIndex()
{
	if (ourManager == nullptr)
	{
		GAME_LOG("ERROR! EntityManager not bound to CGameLogicFunctions");
		return EntityHandleINVALID.myUniqueIndex;
	}
	if (!ourManager->IsHandleValidNoAssert(SEntityHandle(ourPlayerEntityIndex))
		|| (!ourManager->IsHandleValidNoAssert(SEntityHandle(0))))
	{
		return EntityHandleINVALID.myUniqueIndex;
	}
	return ourPlayerEntityIndex;
}

const UniqueIndexType CGameLogicFunctions::GetSupportUniqueIndex()
{
	if (ourManager == nullptr)
	{
		GAME_LOG("ERROR! EntityManager not bound to CGameLogicFunctions");
		return EntityHandleINVALID.myUniqueIndex;
	}
	if (!ourManager->IsHandleValidNoAssert(SEntityHandle(ourSupportCompanionIndex))
		|| (!ourManager->IsHandleValidNoAssert(SEntityHandle(0))))
	{
		return EntityHandleINVALID.myUniqueIndex;
	}
	return ourSupportCompanionIndex;
}

const CU::Vector3f CGameLogicFunctions::GetPlayerPosition()
{
	ERROR_CHECK_ENTITY_MANAGER(CU::Vector3f(); , 0, 0);

	return ourManager->GetComponent<CompTransform>(SEntityHandle(ourPlayerEntityIndex)).myPosition;
}

const CU::Vector3f CGameLogicFunctions::GetPlayerFront()
{
	ERROR_CHECK_ENTITY_MANAGER(CU::Vector3f(); , 0, 0);

	return ourManager->GetComponent<CompTransform>(SEntityHandle(ourPlayerEntityIndex)).myMatrix.GetFront();
}

const CU::Vector3f CGameLogicFunctions::GetPlayerTop()
{
	ERROR_CHECK_ENTITY_MANAGER(CU::Vector3f(); , 0, 0);

	return ourManager->GetComponent<CompTransform>(SEntityHandle(ourPlayerEntityIndex)).myMatrix.GetTop();
}

const float CGameLogicFunctions::GetEntityRadius(const UniqueIndexType& aEntityUniqueIndex)
{
	ERROR_CHECK_ENTITY_MANAGER(0.0f;, aEntityUniqueIndex, 0);

	const float returnValue(ourManager->GetComponent<CompTransform>(SEntityHandle(ourPlayerEntityIndex)).myRadius);
	return returnValue;
}

const CU::Vector3f CGameLogicFunctions::GetEntityPosition(const UniqueIndexType& aEntityUniqueIndex)
{
	ERROR_CHECK_ENTITY_MANAGER(CU::Vector3f(); , aEntityUniqueIndex, 0);

	return ourManager->GetComponent<CompTransform>(SEntityHandle(aEntityUniqueIndex)).myPosition;
}

const CU::Vector3f CGameLogicFunctions::GetEntityFront(const UniqueIndexType& aEntityUniqueIndex)
{
	ERROR_CHECK_ENTITY_MANAGER(CU::Vector3f(); , aEntityUniqueIndex, 0);

	return ourManager->GetComponent<CompTransform>(SEntityHandle(aEntityUniqueIndex)).myMatrix.GetFront();
}

const CU::Vector3f CGameLogicFunctions::GetEntityTop(const UniqueIndexType& aEntityUniqueIndex)
{
	ERROR_CHECK_ENTITY_MANAGER(CU::Vector3f(); , aEntityUniqueIndex, 0);

	return ourManager->GetComponent<CompTransform>(SEntityHandle(aEntityUniqueIndex)).myMatrix.GetTop();
}

const CU::Vector3f CGameLogicFunctions::GetEntityDirection(const UniqueIndexType & aEntityUniqueIndex)
{
	ERROR_CHECK_ENTITY_MANAGER(CU::Vector3f(); , aEntityUniqueIndex, 0);
	return ourManager->GetComponent<CompTransform>(SEntityHandle(aEntityUniqueIndex)).myLookRotation.GetForward();
}

const CU::Vector3f CGameLogicFunctions::GetRotation(const UniqueIndexType & aEntityUniqueIndex)
{
	ERROR_CHECK_ENTITY_MANAGER(CU::Vector3f();, aEntityUniqueIndex, 0);
	CU::Vector3f entityLookQuat = ourManager->GetComponent<CompTransform>(SEntityHandle(aEntityUniqueIndex)).myLookRotation.ToEuler();
	//float xRot = entityLookQuat.GetRoll();
	//float yRot = entityLookQuat.GetPitch();
	//float zRot = entityLookQuat.GetYaw();
	
	//CU::Vector3f forwardDegrees({ CU::ToDegrees(xRot), CU::ToDegrees(yRot), CU::ToDegrees(zRot) });
	CU::Vector3f forwardDegrees({ CU::ToDegrees(entityLookQuat.x), CU::ToDegrees(entityLookQuat.y), CU::ToDegrees(entityLookQuat.z) });
	return (forwardDegrees);
}

const CNavigationMesh* CGameLogicFunctions::GetNavMesh()
{
	ERROC_CHECK_LEVEL(nullptr);
	return &ourLevel->GetNavMesh();
}

void CGameLogicFunctions::SetEntities(const CU::GrowingArray<UniqueIndexType>& aEntities)
{
	ourEntities.RemoveAll();
	ourEntities.Add(aEntities);
}

void CGameLogicFunctions::SetPlayerEntityIndex(UniqueIndexType aEntityUniqueIndex)
{
	ERROR_CHECK_ENTITY_MANAGER(; , aEntityUniqueIndex, 0);
	ourPlayerEntityIndex = aEntityUniqueIndex;
}

void CGameLogicFunctions::SetCompanionEntityIndex(UniqueIndexType aEntityUniqueIndex)
{
	ourSupportCompanionIndex = aEntityUniqueIndex;
}
