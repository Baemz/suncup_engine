#pragma once
#include "EntitySystemLink\EntitySystemLink.h"

class CLevel;
class CNavigationMesh;

class CGameLogicFunctions
{
	friend class CScriptSystem;

public:
#pragma region MOVEMENT_ROTATION
	static void ArriveAtPosition(const UniqueIndexType& aEntityUniqueIndex, const CU::Vector3f& aPosition);
	static void Wander(const UniqueIndexType& aEntityUniqueIndex);
	static void StopWander(const UniqueIndexType& aEntityUniqueIndex);
	static void LerpToPosition(const UniqueIndexType& aEntityUniqueIndex, const CU::Vector3f& aPosition, const float aPercentagePerFrame);
	static void SmoothstepToPosition(const UniqueIndexType& aEntityUniqueIndex, const CU::Vector3f& aPosition, const float aTotalTime);
	static void TeleportToPosition(const UniqueIndexType& aEntityUniqueIndex, const CU::Vector3f& aPosition);

	static void ArriveAtEntity(const UniqueIndexType& aEntityUniqueIndex, const UniqueIndexType& aTargetEntityUniqueIndex);
	static void LerpToEntity(const UniqueIndexType& aEntityUniqueIndex,	const UniqueIndexType& aTargetEntityUniqueIndex);
	static void SmoothstepToEntity(const UniqueIndexType& aEntityUniqueIndex, const UniqueIndexType& aTargetEntityUniqueIndex);
	static void TeleportToEntity(const UniqueIndexType& aEntityUniqueIndex, const UniqueIndexType& aTargetEntityUniqueIndex);

	static void ScaleBy(const UniqueIndexType& aEntityUniqueIndex, const CU::Vector3f& aScale);
	static void SetScale(const UniqueIndexType& aEntityUniqueIndex, const CU::Vector3f& aScale);
	static void LookAtEntity(const UniqueIndexType& aFirstEntityUniqueIndex, const UniqueIndexType& aSecondEntityUniqueIndex);
	static void LookAtPosition(const UniqueIndexType& aEntityUniqueIndex, const CU::Vector3f& aPosition);
	static void LookAtEntityInverse(const UniqueIndexType & aFirstEntityUniqueIndex, const UniqueIndexType & aSecondEntityUniqueIndex);
	static void LookAtPositionInverse(const UniqueIndexType & aEntityUniqueIndex, const CU::Vector3f & aPosition);
	static void RotateModel(const UniqueIndexType& aEntityUniqueIndex, const CU::Vector3f aRad);

	static void SetNavMeshTypeUnderEntity(const UniqueIndexType& aEntityUniqueIndex, const std::string& aNavMeshTypeNameReplace, const std::string& aNavMeshTypeNameWith);
	static void ResetNavMeshTypeUnderEntity(const UniqueIndexType& aEntityUniqueIndex);
#pragma endregion

	static const CU::Vector3f GetPositionFromAngleAndDistanceFromEntity(const UniqueIndexType& aEntityUniqueIndex, const float aAngleFromForward,  const float aDistanceFromTarget);

#pragma region ENTITY_STATE
	static void MakePlayer(const UniqueIndexType& aEntityUniqueIndex);
	static void MakeEnemy(const UniqueIndexType& aEntityUniqueIndex, bool aMovement);
	static void MakeCompanion(const UniqueIndexType& aEntityUniqueIndex);
	static void MakeSpawnerPainting(const UniqueIndexType& aEntityUniqueIndex);
	static void MakeBoss(const UniqueIndexType& aEntityUniqueIndex);
	static void MakeProjectile(const UniqueIndexType& aEntityUniqueIndex);
	static void MakeHealthPickup(const UniqueIndexType& aEntityUniqueIndex);
	static void MakeCheckpoint(const UniqueIndexType& aEntityUniqueIndex);

	static void UpdateEntityPosition(const UniqueIndexType& aEntityUniqueIndex, const CU::Vector3f& aNewPosition);
	static void KillEntity(const UniqueIndexType& aEntityUniqueIndex);
	static void KillEntity_ThreadSafe(const UniqueIndexType& aEntityUniqueIndex);
	static void SetToDestroyOnLeaveGrid(const UniqueIndexType& aEntityUniqueIndex, std::function<void()> aAdditionalRemoveObjectCode);
	static void Respawn();
	static void SetCurrentCheckpoint(const UniqueIndexType& aEntityUniqueIndex);

	static void TakeDamage(const UniqueIndexType& aEntityUniqueIndex, const float aDamage);
	static void SetMaxHealth(const UniqueIndexType& aEntityUniqueIndex, const float aHealth);
	static void SetHealth(const UniqueIndexType& aEntityUniqueIndex, const float aHealth);
	static const float GetHealth(const UniqueIndexType& aEntityUniqueIndex);
	static const float GetMaxHealth(const UniqueIndexType& aEntityUniqueIndex);

	static bool IsAlive(const UniqueIndexType& aEntityUniqueIndex);

	static void SetShouldGoToCredits(const bool aShouldGoToCredits);
#pragma endregion

#pragma region SPAWN
	static void SpawnParticleOnEntity(const std::string& aParticleName, const UniqueIndexType& aEntityUniqueIndex);
	static void SpawnParticleOnPosition(const std::string& aParticleName, const CU::Vector3f& aPosition);

	static void SpawnObjectOnEntity(const UniqueIndexType& aEntityUniqueIndex, const char* aScriptName);
	static const UniqueIndexType SpawnModelAtPosition(const UniqueIndexType& aEntityUniqueIndex, const char* aModelName, const char* aMaterialName, const CU::Vector3f& aPosition);
	static void SpawnEnemy(const UniqueIndexType& aPaintingIndex, CU::Vector3f aPosition/*, eEnemyType aEnemyType*/);
	static void SpawnPainting(const CU::Vector3f& aPosition, const CU::Vector3f& aRotation);

	static void SpawnProjectileDebris(const UniqueIndexType& aProjectileIndex);

#pragma endregion

#pragma region SPRITES
	static CompHUDElements::IndexType AddHUDElement(const UniqueIndexType& aEntityUniqueIndex, const char* aSpritePath);
	static void SetHUDElementPosition(const UniqueIndexType& aEntityUniqueIndex, const CompHUDElements::IndexType& aHUDIndex, const CU::Vector2f& aPosition);
	static void SetHUDElementScale(const UniqueIndexType& aEntityUniqueIndex, const CompHUDElements::IndexType& aHUDIndex, const CU::Vector2f& aScale);
	static void SetHUDElementCrop(const UniqueIndexType& aEntityUniqueIndex, const CompHUDElements::IndexType& aHUDIndex, const CU::Vector4f& aCrop);
	static void SetHUDElementPivot(const UniqueIndexType& aEntityUniqueIndex, const CompHUDElements::IndexType& aHUDIndex, const CU::Vector2f& aPivot);
	static CU::Vector2f GetPositionProjectedToScreen(const CU::Vector3f& aPosition);
	
	static const CU::Vector2f GetHUDElementSpriteSize(const UniqueIndexType& aEntityUniqueIndex, const CompHUDElements::IndexType& aHUDIndex);

	static void SetBlackBarsRadius(const float aRadius);

	static const std::string GetEnemyName(const UniqueIndexType& aEntityUniqueIndex);
	
#pragma endregion

#pragma region ANIMATIONS_AND_RENDER
	static void SetModelName(const UniqueIndexType& aEntityUniqueIndex, const char* aModelName);
	static void SetMaterial(const UniqueIndexType & aEntityUniqueIndex, const char * aMaterialName);
	static void SetAnimation(const UniqueIndexType& aEntityUniqueIndex, const char* aAnimationTag);
	static void SetAnimationWithIndex(const UniqueIndexType& aEntityUniqueIndex, const unsigned int aAnimationIndex);
	static void SetAnimationSpeed(const UniqueIndexType& aEntityUniqueIndex, const float aAnimationSpeed);
	static void PauseAnimation(const UniqueIndexType& aEntityUniqueIndex);
	static void PlayAnimation(const UniqueIndexType& aEntityUniqueIndex);
	static void StopAnimation(const UniqueIndexType& aEntityUniqueIndex);
	static const float GetAnimationDuration(const UniqueIndexType& aEntityUniqueIndex);
	static void RotateBridge(const UniqueIndexType& aEntityUniqueIndex);
	static const CU::Vector4f BridgeGetStart(const UniqueIndexType& aEntityUniqueIndex);
	static const CU::Vector4f BridgeGetEnd(const UniqueIndexType& aEntityUniqueIndex);
	static void BridgeSlerp(const UniqueIndexType& aEntityUniqueIndex, const CU::Quaternion& aStartQuaternion, const CU::Quaternion& aEndQuaternion, const float aProgress);

#pragma endregion
	static float GetDistanceBetweenEntitiesSquared(const UniqueIndexType& aFirstEntityUniqueIndex, const UniqueIndexType& aSecondEntityUniqueIndex);
	static float GetDistanceFromEntityToPositionSquared(const UniqueIndexType& aFirstEntityUniqueIndex, const CU::Vector3f& aPosition);

	static void SetMovementSpeed(const UniqueIndexType& aEntityUniqueIndex, float aSpeed);
	static void SetAcceleration(const UniqueIndexType& aEntityUniqueIndex, float aAcceleration);
	static void SetRotationSpeed(const UniqueIndexType& aEntityUniqueIndex, float aRotationSpeed);
	static void ClearNavMeshPath(const UniqueIndexType& aEntityUniqueIndex);
	static const CU::Vector3f DrawLineFromEntityToMouse(const UniqueIndexType& aFromIndex);
	static void TossHealingAbility(const UniqueIndexType& aEntityUniqueIndex, const CU::Vector3f aHealPos,
		const float aHealLifeTime, const float aHealCircleRadius, const float aHealingPerTick, const float aHealingTickInterval, 
		const float aDamageLifeTime, const float aDamageCircleRadius, const float aDamagePerSecond, const float aDamageZoneSpawnInterval, const float aFlightSpeed);

	static volatile bool GetShouldGoToCredits() { return ourShouldGoToCredits; }

	static const UniqueIndexType GetPlayerUniqueIndex();
	static const UniqueIndexType GetSupportUniqueIndex();

	static const CU::Vector3f GetPlayerPosition();
	static const CU::Vector3f GetPlayerFront();
	static const CU::Vector3f GetPlayerTop();

	static const float GetEntityRadius(const UniqueIndexType& aEntityUniqueIndex);
	static const CU::Vector3f GetEntityPosition(const UniqueIndexType& aEntityUniqueIndex);
	static const CU::Vector3f GetEntityFront(const UniqueIndexType& aEntityUniqueIndex);
	static const CU::Vector3f GetEntityTop(const UniqueIndexType& aEntityUniqueIndex);
	static const CU::Vector3f GetEntityDirection(const UniqueIndexType& aEntityUniqueIndex);
	static const CU::Vector3f GetRotation(const UniqueIndexType& aEntityUniqueIndex);


	static const UniqueIndexType GetClosestEnemy(const UniqueIndexType& aEntityUniqueIndex, const float aRadius);
	static const UniqueIndexType GetClosestPainting(const UniqueIndexType& aEntityUniqueIndex, const float aRadius);

	static const UniqueIndexType GetObjectIntersectingRay(const CU::Collision::Ray& aRay);

	static const UniqueIndexType GetInvalidEntityIndex();
	static const bool IsEntityValid(const UniqueIndexType& aEntityUniqueIndex);
	
	static void SwitchLevelTo(const std::string& aLevelPath);

	static void SetLevel(CLevel* aLevel);
	static const CNavigationMesh* GetNavMesh();

	static void SetEntities(const CU::GrowingArray<UniqueIndexType>& aEntities);

private:
	static void SetPlayerEntityIndex(UniqueIndexType aEntityUniqueIndex);
	static void SetCompanionEntityIndex(UniqueIndexType aEntityUniqueIndex);
	
	CGameLogicFunctions() = delete;
	~CGameLogicFunctions() = delete;

	static EManager* ourManager;

	static CU::GrowingArray<UniqueIndexType> ourEntities;
	static CLevel* ourLevel;
	static UniqueIndexType ourPlayerEntityIndex;
	static UniqueIndexType ourSupportCompanionIndex;
	static volatile bool ourShouldGoToCredits;

};

