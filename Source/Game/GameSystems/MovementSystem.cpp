#include "stdafx.h"
#include "MovementSystem.h"
#include "..\GraphicsEngine\DebugTools.h"

#include "../CommonUtilities/Random.h"
#include "ScriptSystem.h"
#include "ExposedFunctions\GameLogicFunctions.h"

void CMovementSystem::Activate(/* const SEntityHandle & aPlayerHandle */)
{
	//myPlayerHandle = aPlayerHandle;
}

void CMovementSystem::Deactivate()
{
	myPlayerHandle = SEntityHandle();
}

void CMovementSystem::Update(const float aDeltaTime, const CU::GrowingArray<const CU::GrowingArray<UniqueIndexType>*>& aEntities)
{
	sce::gfx::CDebugTools* debugDrawer(sce::gfx::CDebugTools::Get());

	for (const auto& entityArray : aEntities)
	{
		CombineSteering(aDeltaTime, debugDrawer, *entityArray);
		//SeekBehaviourAI(aDeltaTime, *entityArray);
		ArriveBehaviourAI(aDeltaTime, *entityArray);
		//FleeBehaviourAI(aDeltaTime, *entityArray);
		EvadeBehaviourAI(aDeltaTime, *entityArray);
	}
}

void CMovementSystem::CombineSteering(const float aDeltaTime, sce::gfx::CDebugTools* debugDrawer, const CU::GrowingArray<UniqueIndexType>& aEntities)
{
	auto combineSteering = [&myEntityManager = myEntityManager, aDeltaTime, &debugDrawer](auto& aEntityIndex, CompTransform& aTransform, CompController& aController)
	{
		CController& controller(aController.Get());

		controller.mySteering.Truncate(controller.myMaxForce);
		//controller.myVelocity += controller.mySteering * aDeltaTime;
		// This is what made Benghazi happen.
		controller.myVelocity += controller.mySteering; // *aDeltaTime;
		//controller.myVelocity.y = 0;
		controller.myVelocity.Truncate(controller.myMaxSpeed);
		aTransform.myPosition += controller.myVelocity * aDeltaTime;
		aTransform.myWorldPos += controller.myVelocity * aDeltaTime;

		if (myEntityManager.GetComponent<CompHandle>(aEntityIndex).myHandle.myUniqueIndex == CGameLogicFunctions::GetPlayerUniqueIndex() && controller.myVelocity.Length2() > 0.5f)
		{
			CScriptSystem::CallScriptEventEveryone("OnPlayerIsMoving");
		}
		controller.mySteering = 0;

		if (controller.myVelocity != CU::Vector3f::Zero)
		{
			CGameLogicFunctions::UpdateEntityPosition(myEntityManager.GetComponent<CompHandle>(aEntityIndex).myHandle.myUniqueIndex, aTransform.myPosition);
		}

		constexpr float NotIntertia = 0.97f;
		controller.myVelocity *= NotIntertia;

		if (CU::Quaternion::Angle(aTransform.myLookRotation, controller.myTargetLookRotation) > 1e-2f)
		{
			//constexpr float whoEvenUsesDegreesAnymore = CU::Pif / 180.0f; // FEL
			CU::Quaternion quat(CU::Quaternion::RotateTowards(aTransform.myLookRotation, controller.myTargetLookRotation, controller.myRotationSpeed * aDeltaTime));
			CU::Matrix44f preQuatMatrix = aTransform.myLookRotation.GenerateMatrix();

			aTransform.myLookRotation = quat;

			CU::Quaternion upRighter(CU::Quaternion::FromToRotation(aTransform.myLookRotation.GetUp(), { 0.0f, 1.0f, 0.0f }) * aTransform.myLookRotation);
			aTransform.myLookRotation = CU::Quaternion::Slerp(aTransform.myLookRotation, upRighter, 1.0f);
			
			CU::Vector3f scale;
			scale.x = aTransform.myMatrix[0] / preQuatMatrix[0];
			scale.y = aTransform.myMatrix[5] / preQuatMatrix[5];
			scale.z = aTransform.myMatrix[10] / preQuatMatrix[10];
			aTransform.myMatrix.SetRotation(aTransform.myLookRotation.GenerateMatrix());
			aTransform.myMatrix.ScaleBy(scale);
		}

		//HACK: Can't figure out where transform position becomes NaN. Could have something to do with the union?
		if (isnan(aTransform.myPosition.x))
		{
			aTransform.myLookRotation = CU::Quaternion();
			aTransform.myPosition = { 0 };
		}

		std::function<void(EntityIndex&, CompTransform&, CompController&)> setChildrenWorldPos;
		setChildrenWorldPos = [&myEntityManager, &setChildrenWorldPos](EntityIndex& aIndex, CompTransform& aChildTransform, CompController& aController) -> void
		{
			static CU::Vector3f parentWorldPos;
			static int childIndex = 0;
			++childIndex;

			aChildTransform.myWorldPos = parentWorldPos + aChildTransform.myPosition;
			for (int i = 0; i < aChildTransform.myChildrenCount; ++i)
			{
				CompTransform& child(myEntityManager.GetComponent<CompTransform>(aChildTransform.myChildHandles[i]));
				parentWorldPos = aChildTransform.myWorldPos;
				setChildrenWorldPos(aIndex, child, aController);
			}

			--childIndex;
			if (childIndex == 0)
			{
				parentWorldPos = CU::Vector3f::Zero;
			}
		};

		myEntityManager.RunIfMatching<SigController>(aEntityIndex, setChildrenWorldPos);

		const CU::Vector3f xVelPosition(aTransform.myPosition + CU::Vector3f(controller.myVelocity.x, 0, 0));
		const CU::Vector3f yVelPosition(aTransform.myPosition + CU::Vector3f(0, 0, controller.myVelocity.z));

		DT_DRAW_LINE_4_ARGS(aTransform.myPosition, xVelPosition, CU::Vector3f(1.0f, 0.0f, 0.0f), CU::Vector3f(1.0f, 0.0f, 0.0f));
		DT_DRAW_LINE_4_ARGS(aTransform.myPosition, yVelPosition, CU::Vector3f(0.0f, 1.0f, 0.0f), CU::Vector3f(0.0f, 1.0f, 0.0f));
	};

	myEntityManager.ForEntitiesMatching<SigController>(combineSteering, aEntities);
}

void CMovementSystem::SeekBehaviourAI(const float, const CU::GrowingArray<UniqueIndexType>&)
{
	//auto seekBehaviorAI = [aDeltaTime, &myEntityManager = myEntityManager](auto& aEntityIndex, CompTransform& aTransform, CompController& aController, CompSeekBehavior& aSeekBehavior)
	//{
	//	CController& controller(aController.Get());
	//
	//	const CU::Vector3f diff(aSeekBehavior.myPositionToSeekTo - aTransform.myPosition);
	//	if (diff.Length2() <= CompSeekBehavior::ArriveDistance)
	//	{
	//		myEntityManager.DelComponent<CompSeekBehavior>(aEntityIndex);
	//
	//		return;
	//	}
	//
	//	CU::Vector3f steering = (diff.GetNormalized() * controller.myMaxSpeed) - controller.myVelocity;
	//	controller.mySteering += steering * aDeltaTime * aSeekBehavior.myWeight;
	//};
	//myEntityManager.ForEntitiesMatching<SigSeekBehavior>(seekBehaviorAI, aEntities);
}

void CMovementSystem::ArriveBehaviourAI(const float aDeltaTime, const CU::GrowingArray<UniqueIndexType>& aEntities)
{
	auto arriveBehaviorAI = [&myEntityManager = myEntityManager, aDeltaTime](auto& aEntityIndex, CompTransform& aTransform, CompController& aController, CompArriveBehavior& aArriveBehavior)
	{
		CController& controller(aController.Get());

		const CU::Vector3f diff(aArriveBehavior.myPositionToArriveTo - aTransform.myPosition);
		const float distance(diff.Length());


		if (distance < CompArriveBehavior::ArriveDistance)
		{
			if (myEntityManager.HasTag<TagInputController>(aEntityIndex))
			{
				CScriptSystem::CallScriptEventEveryone("OnPlayerArrive");
			}
			if (myEntityManager.HasTag<TagScriptController>(aEntityIndex))
			{
				if (controller.myScriptStateIndex != EntityHandleINVALID.myUniqueIndex)
				{
					CScriptSystem::CallScriptEventOnTarget(myEntityManager.GetComponent<CompHandle>(aEntityIndex).myHandle.myUniqueIndex, "OnArrived");
				}
			}
			myEntityManager.DelComponent<CompArriveBehavior>(aEntityIndex);
			controller.mySteering = { 0 };
			controller.myVelocity = { 0 };
			return;
		}

		CU::Vector3f targetVelocity = diff.GetNormalized() * controller.myMaxSpeed;
		if (distance < aArriveBehavior.mySlowdownRadius)
		{
			targetVelocity *= (distance / aArriveBehavior.mySlowdownRadius);
		}

		CU::Vector3f steering = targetVelocity - controller.myVelocity;
		controller.mySteering += steering * aArriveBehavior.myWeight;

	};
	myEntityManager.ForEntitiesMatching<SigArriveBehavior>(arriveBehaviorAI, aEntities);
}

void CMovementSystem::FleeBehaviourAI(const float, const CU::GrowingArray<UniqueIndexType>&)
{
	//auto fleeBehaviorAI = [aDeltaTime](auto&, CompTransform& aTransform, CompController& aController, CompFleeBehavior& aFleeBehavior)
	//{
	//	CController& controller(aController.Get());
	//
	//	CU::Vector3f direction;
	//
	//	for (unsigned short i = 0; i < aFleeBehavior.myPositionsToFleeFrom.Size(); ++i)
	//	{
	//		CU::Vector3f current(aTransform.myPosition - aFleeBehavior.myPositionsToFleeFrom[i]);
	//		current.y = 0.0f;
	//		const float length(current.Length());
	//		if (length < aFleeBehavior.myFleeRadius)
	//		{
	//			direction += (1.0f / length) * current;
	//		}
	//	}
	//
	//	if (direction.Length2() == 0.0f)
	//	{
	//		controller.mySteering = { 0 };
	//		controller.myVelocity = { 0 };
	//		return;
	//	}
	//
	//	CU::Vector3f steering = (direction.GetNormalized() * controller.myMaxSpeed) - controller.myVelocity;
	//	controller.mySteering += steering * aDeltaTime * aFleeBehavior.myWeight;
	//};
	//myEntityManager.ForEntitiesMatching<SigFleeBehavior>(fleeBehaviorAI, aEntities);
}

void CMovementSystem::EvadeBehaviourAI(const float aDeltaTime, const CU::GrowingArray<UniqueIndexType>& aEntities)
{
	auto evadeBehaviorAI = [&myEntityManager = myEntityManager, aDeltaTime](auto& aEntityIndex, CompTransform& aTransform, CompController& aController, CompEvadeBehavior& aEvadeBehavior)
	{
		CController& controller(aController.Get());
		const UniqueIndexType entityUniqueIndex = myEntityManager.GetComponent<CompHandle>(aEntityIndex).myHandle.myUniqueIndex;
		const UniqueIndexType playerUniqueIndex = CGameLogicFunctions::GetPlayerUniqueIndex();
		const UniqueIndexType supportUniqueIndex = CGameLogicFunctions::GetSupportUniqueIndex();

		CU::Vector3f evadeDirection;
		const bool indexIsPlayerOrCompanion(
			entityUniqueIndex == playerUniqueIndex ||
			entityUniqueIndex == supportUniqueIndex
		);

		for (unsigned short i = 0; i < aEvadeBehavior.myPositionsToEvade.Size(); ++i)
		{
			const bool positionIsPlayerOrCompanion(
				aEvadeBehavior.myEvadersUniqueIndices[i] == playerUniqueIndex ||
				aEvadeBehavior.myEvadersUniqueIndices[i] == supportUniqueIndex
			);

			if (indexIsPlayerOrCompanion && positionIsPlayerOrCompanion)
			{
				continue;
			}
			else
			{
				const CU::Vector3f currentDiff(aTransform.myPosition - aEvadeBehavior.myPositionsToEvade[i]);
				const unsigned int framesToPredict(static_cast<unsigned int>(currentDiff.Length() / controller.myMaxSpeed));

				CU::Vector3f currentPredictedDiff(
					aTransform.myPosition - (aEvadeBehavior.myPositionsToEvade[i] + aEvadeBehavior.myEvaderVelocities[i] * static_cast<float>(framesToPredict))
				);
				currentPredictedDiff.y = 0;

				const CU::Vector3f color({ 1.0f, (1.0f / currentPredictedDiff.Length()), 1.0f });
				DT_DRAW_LINE_4_ARGS(
					aTransform.myPosition,
					(aEvadeBehavior.myPositionsToEvade[i] + aEvadeBehavior.myEvaderVelocities[i] * static_cast<float>(framesToPredict)),
					CU::Vector3f(1.0f),
					color
				);

				evadeDirection += (1.0f / currentPredictedDiff.Length()) * currentPredictedDiff;
			}
		}

		if (evadeDirection.Length2() != 0.0f)
		{
			CU::Vector3f steering = (evadeDirection.GetNormalized() * controller.myMaxSpeed) - controller.myVelocity;
			controller.mySteering += steering * aEvadeBehavior.myWeight;
		}
	};
	myEntityManager.ForEntitiesMatching<SigEvadeBehavior>(evadeBehaviorAI, aEntities);
}

void CMovementSystem::WanderBehaviourAI(const float aDeltaTime, const CU::GrowingArray<UniqueIndexType>& aEntities)
{
	auto wanderBehaviorAI = [aDeltaTime](auto&, CompTransform&, CompController& aController, CompWanderBehavior& aWanderBehavior)
	{
		CController& controller(aController.Get());

		CU::Vector3f circleCenter(controller.myVelocity);
		if (circleCenter.Length2() != 0.0f)
		{
			circleCenter.Normalize();
			circleCenter *= CompWanderBehavior::CircleOffset;
		}

		CU::Vector3f displacement({ std::cos(aWanderBehavior.myWanderAngle), 0.0f, std::sin(aWanderBehavior.myWanderAngle) });
		displacement.Normalize();
		displacement *= CompWanderBehavior::CircleRadius;

		aWanderBehavior.myWanderAngle += ((CU::GetRandomPercentage() / 100.0f) * CompWanderBehavior::AngleChange - CompWanderBehavior::AngleChange * 0.5f);

		CU::Vector3f steering(circleCenter + displacement);

		//DT_DRAW_LINE_2_ARGS(aPos.value, aPos.value + steering);
		controller.mySteering += steering * aDeltaTime * aWanderBehavior.myWeight;
	};
	myEntityManager.ForEntitiesMatching<SigWanderBehavior>(wanderBehaviorAI, aEntities);
}

