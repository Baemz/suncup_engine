#include "stdafx.h"
#include "ScriptSystem.h"
#include "../Script/ScriptManager.h"
#include "ScriptEventManager.h"
#include "ExposedFunctions\GameLogicFunctions.h"
#include "..\GraphicsEngine\DebugTools.h"
#include "../GraphicsEngine/GraphicsEngineInterface.h"

CScriptEventManager* CScriptSystem::ourEventManagerInstance = nullptr;

CScriptSystem::CScriptSystem(EManager& aManager)
	: CSystemAbstract<CScriptSystem>(aManager)
	, myScriptManager(nullptr)
	, myScriptEventManager(nullptr)
{
}

CScriptSystem::~CScriptSystem()
{
	sce_delete(myScriptEventManager);
	CScriptManager::Destroy();
}

void CScriptSystem::Activate()
{
	assert("Script system already activated!" && (myScriptManager == nullptr));

	CScriptManager::Create();
	myScriptManager = CScriptManager::Get();

	if (myScriptManager == nullptr)
	{
		GAME_LOG("ERROR! Failed to create ScriptManager");
		return;
	}

	const auto registerFunction(std::bind(&CScriptSystem::RegisterScriptFunctions, this));

	CScriptManager::StateIndexType stateIndex;
	// This is not an error, just compile and move on
	if (myScriptManager->Init(registerFunction, "Data/Scripts/initScript.lua", stateIndex, [](const CScriptManager::StateIndexType&) {}) == false)
	{
		GAME_LOG("Failed to init script manager");
		return;
	}

	sce_delete(myScriptEventManager);
	myScriptEventManager = sce_new(CScriptEventManager(*myScriptManager, myEntityManager));
	myScriptEventManager->Init();
	ourEventManagerInstance = myScriptEventManager;

	CGameLogicFunctions::ourManager = &myEntityManager;
}

void CScriptSystem::Deactivate()
{
}

void CScriptSystem::Update(const float aDeltaTime, const CU::GrowingArray<const CU::GrowingArray<UniqueIndexType>*>& aEntities)
{
	aEntities;

	if (myScriptEventManager == nullptr)
	{
		return;
	}

	myScriptEventManager->Update(aDeltaTime);
}

void CScriptSystem::WaitUntilDone() const
{
	myScriptManager->WaitUntilDone();
	myScriptEventManager->ClearRemovedStates();
}

void CScriptSystem::SetIsActive(const bool aIsActive)
{
	if (ourEventManagerInstance == nullptr)
	{
		RESOURCE_LOG("[ScriptEventManager] ERROR! Script event manager was null.");
		return;
	}

	ourEventManagerInstance->SetIsActive(aIsActive);
}

void CScriptSystem::KillEntity(const UniqueIndexType& aUniqueIndex)
{
	if (ourEventManagerInstance == nullptr)
	{
		RESOURCE_LOG("[ScriptEventManager] ERROR! Script event manager was null.");
		return;
	}

	ourEventManagerInstance->KillEntity(aUniqueIndex);
}

void CScriptSystem::KillEntity_ThreadSafe(const UniqueIndexType& aUniqueIndex)
{
	if (ourEventManagerInstance == nullptr)
	{
		RESOURCE_LOG("[ScriptEventManager] ERROR! Script event manager was null.");
		return;
	}

	ourEventManagerInstance->KillEntity_ThreadSafe(aUniqueIndex);
}

void CScriptSystem::RegisterScriptComponent(const unsigned int aStateIndex, const SEntityHandle & aEntityHandle)
{
	constexpr bool stateIndexTypeIsMatching = std::is_same<decltype(aStateIndex), const CScriptManager::StateIndexType>::value;
	static_assert(stateIndexTypeIsMatching, "Update the argument type for RegisterScriptComponent.");
	if (ourEventManagerInstance == nullptr)
	{
		RESOURCE_LOG("[ScriptEventManager] ERROR! Cannot register script component on LUA state index %i, script event manager was null.", aStateIndex);
		return;
	}

	ourEventManagerInstance->RegisterScriptComponent(aStateIndex, aEntityHandle);
}

bool CScriptSystem::RegisterScriptFunctions() const
{
	std::function<void(const char*)> scriptLogMessage(
		[](const char* aMessage) -> void
	{
		aMessage;
		GAME_LOG(aMessage);
	});

	if (myScriptManager->RegisterFunction("Print", scriptLogMessage
		, "String"
		, "Print to the console.", true) == false)
	{
		return false;
	}

	std::function<void(double aNumber)> scriptLogNumber(
		[](double aNumber) -> void
	{
		aNumber;
		GAME_LOG("%f", aNumber);
	});

	if (myScriptManager->RegisterFunction("PrintNumber", scriptLogNumber
		, "Number"
		, "Print to the console.") == false)
	{
		return false;
	}

	std::function<void(bool aState)> scriptLogBool(
		[](bool aState) -> void
	{
		aState;
		GAME_LOG("%s", (aState ? "true" : "false"));
	});

	if (myScriptManager->RegisterFunction("PrintBool", scriptLogBool
		, "Bool"
		, "Print to the console.") == false)
	{
		return false;
	}

	std::function<void(const UniqueIndexType)> printUniqueIndexBind(
		[](const UniqueIndexType& aUniqueIndex) -> void
	{
		aUniqueIndex;
		GAME_LOG("%llu", aUniqueIndex);
	});

	if (myScriptManager->RegisterFunction("PrintUniqueIndex", printUniqueIndexBind
		, "UniqueIndex"
		, "Print to the console."
		, true) == false)
	{
		return false;
	}

	std::function<UniqueIndexType(const double)> convertToUniqueIndexBind(
		[](const double& aToBeUniqueIndex) -> UniqueIndexType
	{
		return static_cast<UniqueIndexType>(aToBeUniqueIndex);
	});

	if (myScriptManager->RegisterFunction("ConvertToUniqueIndex", convertToUniqueIndexBind
		, "Number"
		, "Converts number to lua UniqueIndex. NOTE: Not guaranteed to work on big +- numbers"
		, true) == false)
	{
		return false;
	}

	std::function<double(const UniqueIndexType)> convertFromUniqueIndexBind(
		[](const UniqueIndexType& aToBeDouble) -> double
	{
		return static_cast<double>(aToBeDouble);
	});

	if (myScriptManager->RegisterFunction("ConvertFromUniqueIndex", convertFromUniqueIndexBind
		, "Number"
		, "Converts number from lua UniqueIndex."
		, true) == false)
	{
		return false;
	}

	//Teleport

	std::function<void(const float, const float, const float)> scriptTeleportPlayer(
		[](const float aPositionX, const float aPositionY, const float aPositionZ)
	{
		CU::Vector3f teleportPos(aPositionX, aPositionY, aPositionZ);
		CGameLogicFunctions::TeleportToPosition(CGameLogicFunctions::GetPlayerUniqueIndex(), teleportPos);
	});

	if (myScriptManager->RegisterFunction("TeleportPlayer", scriptTeleportPlayer
		, "PositionX, PositionY, PositionZ"
		, "Teleports the player to Position X,Y,Z"
		, true) == false)
	{
		return false;
	}

	std::function<void(const float, const float, const float)> scriptTeleportPriest(
		[](const float aPositionX, const float aPositionY, const float aPositionZ)
	{
		CU::Vector3f teleportPos(aPositionX, aPositionY, aPositionZ);
		CGameLogicFunctions::TeleportToPosition(CGameLogicFunctions::GetSupportUniqueIndex(), teleportPos);
	});

	if (myScriptManager->RegisterFunction("TeleportPriest", scriptTeleportPriest
		, "PositionX, PositionY, PositionZ"
		, "Teleports the Priest to Position X,Y,Z"
		, true) == false)
	{
		return false;
	}

	/////////

	// SSAO
	std::function<void(bool)> scriptToggleSSAO(
		[](bool aBool) -> void
	{
		sce::gfx::CGraphicsEngineInterface::SetSSAO(aBool);
	});

	if (myScriptManager->RegisterFunction("ToggleSSAO", scriptToggleSSAO
		, "Bool"
		, "Sets the SSAO on or off."
		, true) == false)
	{
		return false;
	}
	/////////////////////////////

	// Bloom
	std::function<void(bool)> scriptToggleBloom(
		[](bool aBool) -> void
	{
		sce::gfx::CGraphicsEngineInterface::SetBloom(aBool);
	});

	if (myScriptManager->RegisterFunction("ToggleBloom", scriptToggleBloom
		, "Bool"
		, "Sets the Bloom on or off."
		, true) == false)
	{
		return false;
	}
	/////////////////////////////

	// Linear Fog
	std::function<void(bool)> scriptLinearFog(
		[](bool aBool) -> void
	{
		sce::gfx::CGraphicsEngineInterface::SetLinearFog(aBool);
	});

	if (myScriptManager->RegisterFunction("ToggleLinearFog", scriptLinearFog
		, "Bool"
		, "Sets the Linear Fog on or off."
		, true) == false)
	{
		return false;
	}
	/////////////////////////////

	auto debugTools(sce::gfx::CDebugTools::Get());

	std::function<void(float, float, float, unsigned char, unsigned char, unsigned char)> debugDrawCubeFunction(std::bind(
		static_cast<void(sce::gfx::CDebugTools::*)(float, float, float, float, unsigned char, unsigned char, unsigned char)>(&sce::gfx::CDebugTools::DrawCube)
		, debugTools, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, 1.0f, std::placeholders::_4, std::placeholders::_5, std::placeholders::_6));
	if (myScriptManager->RegisterFunction("DrawCube", debugDrawCubeFunction
		, "Number, Number, Number, Number(0-255), Number(0-255), Number(0-255)"
		, "Draw cube in the world. Center position, color."
		, true) == false)
	{
		return false;
	}

	std::function<void(float, float, float, float, float, float, unsigned char, unsigned char, unsigned char)> debugDrawRectangle3DFunction(std::bind(
		static_cast<void(sce::gfx::CDebugTools::*)(float, float, float, float, float, float, unsigned char, unsigned char, unsigned char)>(&sce::gfx::CDebugTools::DrawRectangle3D)
		, debugTools, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5, std::placeholders::_6, std::placeholders::_7, std::placeholders::_8, std::placeholders::_9));
	if (myScriptManager->RegisterFunction("DrawRectangle3D", debugDrawRectangle3DFunction
		, "Number, Number, Number, Number, Number, Number, Number(0-255), Number(0-255), Number(0-255)"
		, "Draw 3D rectangle in the world. Center position, different sizes, color."
		, true) == false)
	{
		return false;
	}

	std::function<void(float, float, float, float, unsigned char, unsigned char, unsigned char, unsigned char, unsigned char, unsigned char)> debugDrawLine2DFunction(std::bind(
		static_cast<void(sce::gfx::CDebugTools::*)(float, float, float, float, unsigned char, unsigned char, unsigned char, unsigned char, unsigned char, unsigned char)>(&sce::gfx::CDebugTools::DrawLine2D)
		, debugTools, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5, std::placeholders::_6, std::placeholders::_7, std::placeholders::_8, std::placeholders::_9, std::placeholders::_10));
	if (myScriptManager->RegisterFunction("DrawLine2D", debugDrawLine2DFunction
		, "Number(0-1), Number(0-1), Number(0-1), Number(0-1), Number(0-255), Number(0-255), Number(0-255), Number(0-255), Number(0-255), Number(0-255)"
		, "Draw 2D line on the screen. Start and end position, start and end color."
		, true) == false)
	{
		return false;
	}

	std::function<void(float, float, float, float, float, float, unsigned char, unsigned char, unsigned char, unsigned char, unsigned char, unsigned char)> debugDrawLine3DFunction(std::bind(
		static_cast<void(sce::gfx::CDebugTools::*)(float, float, float, float, float, float, unsigned char, unsigned char, unsigned char, unsigned char, unsigned char, unsigned char, float, float)>(&sce::gfx::CDebugTools::DrawLine)
		, debugTools, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5, std::placeholders::_6, std::placeholders::_7, std::placeholders::_8, std::placeholders::_9, std::placeholders::_10, std::placeholders::_11, std::placeholders::_12, 1.0f, 1.0f));
	if (myScriptManager->RegisterFunction("DrawLine3D", debugDrawLine3DFunction
		, "Number, Number, Number, Number, Number, Number, Number(0-255), Number(0-255), Number(0-255), Number(0-255), Number(0-255), Number(0-255)"
		, "Draw 3D line in the world. Start and end position, start and end color."
		, true) == false)
	{
		return false;
	}

	std::function<void(std::string, float, float, float, unsigned char, unsigned char, unsigned char)> debugDrawText2DFunction(std::bind(
		static_cast<void(sce::gfx::CDebugTools::*)(std::string, float, float, float, unsigned char, unsigned char, unsigned char)>(&sce::gfx::CDebugTools::DrawText2D)
		, debugTools, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5, std::placeholders::_6, std::placeholders::_7));
	if (myScriptManager->RegisterFunction("DrawText2D", debugDrawText2DFunction
		, "String, Number(0-1), Number(0-1), Number(0-), Number(0-255), Number(0-255), Number(0-255)"
		, "Draw text on screen with top left position X, Y and Size and Color."
		, true) == false)
	{
		return false;
	}

	return true;
}
