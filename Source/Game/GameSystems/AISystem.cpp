#include "stdafx.h"
#include "AISystem.h"
#include "EntitySystemLink/RenderComponents.h"
#include "NavigationMesh/NavigationMeshWrapper.h"
#include "../GraphicsEngine/DebugTools.h"
#include "ScriptSystem.h"
#include "GameSystems.h"
#include "ExposedFunctions/GameLogicFunctions.h"

//HACK: Temp
#include "../GraphicsEngine/Model/ModelInstance.h"

CAISystem::CAISystem(EManager & aManager)
	: CSystemAbstract<CAISystem>(aManager)
{
	myAIEvadeData.Reserve(64);
}

CAISystem::~CAISystem()
{
}

void CAISystem::Activate()
{
	AttachToEventReceiving(Event::ESubscribedEvents(Event::eAIEvent));
}

void CAISystem::Deactivate()
{
	DetachFromEventReceiving();
	myAIEvadeData.clear();
}

void CAISystem::Update(const float, const CU::GrowingArray<const CU::GrowingArray<UniqueIndexType>*>& aEntities)
{
	for (const auto& entityArray : aEntities)
	{
		CollectActiveControllers(*entityArray);

		AddEvades(*entityArray);

		CheckProjectileCollisions(*entityArray);
	}

	CNavigationMeshWrapper::Get().DebugRender();
}

void CAISystem::ReceiveEvent(const Event::SEvent& /*aEvent*/)
{
	//assert("AI System should only receive AI events!" && aEvent.myType == Event::EEventType::eAIEvent);

	//const Event::SAIEvent* msg(reinterpret_cast<const Event::SAIEvent*>(&aEvent));
	//
	//if (msg->myMessage == Event::EAIEvents::ePlayerChangedNavmeshNode)
	//{
	//	if (myNavMesh != nullptr)
	//	{
	//		auto setMovementTarget = [&myEntityManager = myEntityManager, &myNavMesh = myNavMesh](auto&, CompTransform& aTransform, CompController& aController)
	//		{
	//			CAIController& controller(aController.GetAI());
	//
	//			if (controller.myWorldInterfaceTag == eWorldInterfaceTag::Events)
	//			{
	//				////Start navigation
	//				//CU::Collision::Ray objectRay(aTransform.myPosition + CU::Vector3f(0.0f, 100.0f, 0.0f), CU::Vector3f(0.0f, -1.0f, 0.0f));
	//				//CU::Collision::Ray targetRay(controller.myTarget + CU::Vector3f(0.0f, 100.0f, 0.0f), CU::Vector3f(0.0f, -1.0f, 0.0f));
	//				//
	//				//myNavMesh->DeselectBeginAndEnd();
	//				//myNavMesh->SelectBeginNode(objectRay);
	//				//myNavMesh->SelectEndNode(targetRay);
	//				//myNavMesh->CalculatePath();
	//				//controller.myCurrentPath = myNavMesh->GetPathCopy();
	//			}
	//		};
	//		//myEntityManager.ForEntitiesMatching<SigAIController>(setMovementTarget);
	//	}
	//}
}

void CAISystem::CheckProjectileCollisions(const CU::GrowingArray<UniqueIndexType>& aEntities)
{
	auto collisionCheck(
		[this, &aEntities](EntityIndex aEntityIndex, const CompTransform& aTransform)
	{
		auto forEveryModel(
			[this, &aEntityIndex, &aTransform](EntityIndex aOtherIndex, const CompTransform& aOtherTransform, const auto&)
		{
			if ((aEntityIndex == aOtherIndex)
				|| (myEntityManager.IsAlive(aOtherIndex) == false))
			{
				return;
			}

			if (aTransform.myPosition.Distance2(aOtherTransform.myPosition) < (aTransform.myRadius + aOtherTransform.myRadius) * (aTransform.myRadius + aOtherTransform.myRadius))
			{
				if (!myEntityManager.HasTag<TagEnemy>(aOtherIndex) && !myEntityManager.HasTag<TagProjectile>(aOtherIndex))
				{
					//HACK HACK HACK HACK HACK
					if (myEntityManager.HasComponent<CompModelInstance>(aOtherIndex))
					{
						const sce::gfx::CModelInstance& model(myEntityManager.GetComponent<CompModelInstance>(aOtherIndex).myModel);
						const std::size_t modelNameStartIndex(model.GetModelPath().find_last_of("\\") + 1);
						const std::string name(model.GetModelPath().substr(modelNameStartIndex, 5));
						if (name == "floor")
						{
							return;
						}
					}

					std::shared_ptr<CScriptSystem> scriptSystem(CGameSystems::Get()->GetSystem<CScriptSystem>());

					if (myEntityManager.HasTag<TagPlayer>(aOtherIndex))
					{
						scriptSystem->CallScriptEventOnTarget(CGameLogicFunctions::GetPlayerUniqueIndex(), "OnDamageTaken", 0.02);
					}
					if (myEntityManager.HasTag<TagCompanion>(aOtherIndex))
					{
						scriptSystem->CallScriptEventOnTarget(CGameLogicFunctions::GetSupportUniqueIndex(), "OnDamageTaken", 0.02);
					}

					scriptSystem->CallScriptEventOnTarget(myEntityManager.GetComponent<CompHandle>(aEntityIndex).myHandle.myUniqueIndex, "OnHealthReachedZero");
				}
			}
		});

		myEntityManager.ForEntitiesMatching<SigRender>(forEveryModel, aEntities);
	});

	myEntityManager.ForEntitiesMatching<SigProjectileCollisions>(collisionCheck, aEntities);
}

void CAISystem::EvadeOtherControllers()
{
}

void CAISystem::CollectActiveControllers(const CU::GrowingArray<UniqueIndexType>& aEntities)
{
	myAIEvadeData.clear();

	auto collectActiveControllers = [&myEntityManager = myEntityManager, &myAIEvadeData = myAIEvadeData]
	(EntityIndex& aEntityIndex, CompTransform& aTransform, CompController& aController)
	{
		const UniqueIndexType uniqueIndex(myEntityManager.GetComponent<CompHandle>(aEntityIndex).myHandle.myUniqueIndex);
		if (myEntityManager.HasTag<TagInputController>(aEntityIndex))
		{
			myAIEvadeData.Add(SAIEvadeData(&aTransform, &aController, uniqueIndex));
		}
		if (myEntityManager.HasTag<TagAIController>(aEntityIndex))
		{
			myAIEvadeData.Add(SAIEvadeData(&aTransform, &aController, uniqueIndex));
		}
	};
	myEntityManager.ForEntitiesMatching<SigController>(collectActiveControllers, aEntities);
}

void CAISystem::AddEvades(const CU::GrowingArray<UniqueIndexType>& aEntities)
{
	auto setFlockingPositionsEvade = [&myEntityManager = myEntityManager, &myAIEvadeData = myAIEvadeData](auto& aEntityIndex, CompTransform& aTransform, CompController& aController, CompEvadeBehavior& aEvadeBehavior)
	{
		aEvadeBehavior.myPositionsToEvade.RemoveAll();
		aEvadeBehavior.myEvaderVelocities.RemoveAll();
		aEvadeBehavior.myEvadersUniqueIndices.RemoveAll();
		aEvadeBehavior.myEvadeRadius = myEntityManager.GetComponent<CompModelInstance>(aEntityIndex).myModel.GetFrustumCollider().GetRadius() * 0.5f;

		const bool objHaveNoPathToFollow(aController.Get().myCurrentPath.size() <= 0);
		bool skipOtherEvades(false);

		if (objHaveNoPathToFollow)
		{
			const CU::Collision::Ray objRay(aTransform.myPosition + CU::Vector3f(0.0f, aTransform.myRadius, 0.0f), CU::Vector3f(0.0f, -1.0f, 0.0f));

			if (CNavigationMeshWrapper::Get().IsIntersectingWithNavMesh(objRay) == false)
			{
				CU::Vector3f evadePosition;
				if (CNavigationMeshWrapper::Get().GetOppositePositionOfNearestEndEdge(objRay, evadePosition))
				{
					aEvadeBehavior.myPositionsToEvade.Add(evadePosition);
					aEvadeBehavior.myEvaderVelocities.Add(CU::Vector3f());
					aEvadeBehavior.myEvadersUniqueIndices.Add(EntityHandleINVALID.myUniqueIndex);
					skipOtherEvades = true;
				}
				else
				{
					printf("Failed to get opposite end edge position.\n");
				}
			}
		}

		if (skipOtherEvades == false)
		{
			for (const SAIEvadeData& evadeData : myAIEvadeData)
			{
				if (evadeData.myTransform == &aTransform) continue;

				if ((evadeData.myTransform->myPosition - aTransform.myPosition).Length2() < aEvadeBehavior.myEvadeRadius * aEvadeBehavior.myEvadeRadius)
				{
					aEvadeBehavior.myPositionsToEvade.Add(evadeData.myTransform->myPosition);
					aEvadeBehavior.myEvaderVelocities.Add(evadeData.myController->Get().myVelocity);
					aEvadeBehavior.myEvadersUniqueIndices.Add(evadeData.myUniqueEntityIndex);
				}
			}
		}
	};

	myEntityManager.ForEntitiesMatching<SigEvadeBehavior>(setFlockingPositionsEvade, aEntities);
}
