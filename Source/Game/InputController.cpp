#include "stdafx.h"
#include "InputController.h"

CInputController::CInputController(const SControllerInit & aInitData, const SInputInit& aInputInitData)
	: CController(aInitData)
	, myCameraOffset(aInputInitData.myCameraOffset)
{
	myControllerType = EController::Input;
}
