#pragma once
#include "../GraphicsEngine/Sprite/Sprite.h"

enum class EUpdateMode
{
	HorizontalFade,
	VerticalFadeUp,
	VerticalFadeDown,
	TotalFade
};

class CUIElement
{
public:
	void Init(const char* aSpritePath);
	void Update(const float aPercentage, const EUpdateMode& aUpdateMode);
	void Render();

	inline void SetPosition(const CU::Vector2f& aPosition) { mySprite.SetPosition(aPosition); }
	inline void SetPivot(const CU::Vector2f& aPivot) { mySprite.SetPivot(aPivot); }

	inline const CU::Vector2f GetPosition() const { return mySprite.GetPosition(); }

private:
	sce::gfx::CSprite mySprite;
};