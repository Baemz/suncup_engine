#pragma once
#include "../CommonUtilities/Vector3.h"
#include "../EntitySystem/Handle.h"
#include "../CommonUtilities/Vector.h"
#include "../CommonUtilities/Quaternion.h"

using EntityManager = void;

class CEntityFactory
{
	friend class CGame;
public:
	struct SEntityCreationData
	{
		struct SScriptData
		{
			using OnInitFunction = std::function<bool(const UniqueIndexType*, const std::size_t&, const int*, const std::size_t&, const std::string*, const std::size_t&)>;

			SScriptData() : myIndices(nullptr), myNumbers(nullptr), myStrings(nullptr) {}
			~SScriptData() { myIndices = nullptr; };

			std::string myScriptFilePath;
			OnInitFunction myOnInit;
			UniqueIndexType* myIndices;
			int* myNumbers;
			std::string* myStrings;
		};

		struct SLightData
		{
			enum class ELightType
			{
				NONE,
				POINT,
				SPOT,
				DIRECTIONAL,
			};

			struct SPointLight
			{
				float aRange;
			};

			struct SDirectionalLight
			{
				CU::Vector3f myDirection;
			};

			struct SSpotLight
			{
				CU::Vector3f myDirection;
				float myAngle;
				float myRange;
			};

			SLightData(): myColor(1.0f, 1.0f, 1.0f, 1.0f), myLightType(ELightType::NONE){}
			~SLightData() = default;

			union
			{
				SDirectionalLight myDL;
				SPointLight myPL;
				SSpotLight mySL;
			};

			CU::Vector4f myColor;
			float myIntensity;
			ELightType myLightType;

		};

		struct STriggerBox
		{
			STriggerBox() : myIsValid(false) {};

			CU::Vector3f myMin;
			CU::Vector3f myMax;
			bool myIsValid;

		};

		struct SParticleData
		{
			SParticleData() : myIsValid(false) {};

			std::string myParticlePath;
			CU::Vector3f myPositionOffset;
			bool myIsValid;
		};

		SEntityCreationData() : myScale(1.0f, 1.0f, 1.0f), myRadius(0.0f) {};

		CU::Vector3f myPosition;
		CU::Vector3f myRotation;
		CU::Vector3f myScale;

		//Size as vector3? OBB?
		float myRadius;

		std::string myModelFilePath;
		std::string myMaterialFilePath;
		SScriptData myScriptData;
		SLightData myLightData;
		STriggerBox myTriggerBox;
		SParticleData myParticleData;

	};

	static SEntityHandle CreateEntity(SEntityCreationData& aEntityCreationData);
	static SEntityHandle CreateEntityCamera(const CU::Vector3f& aPosition, const CU::Vector3f& aRotation);

private:
	static bool InitModel(SEntityHandle& aEntityHandle, const std::string& aModelPath, const std::string& aMaterialPath);
	static bool InitScript(SEntityHandle& aEntityHandle, SEntityCreationData::SScriptData& aScriptData, const CU::Quaternion& aQuaternion);
	static bool InitTrigger(SEntityHandle& aEntityHandle, const SEntityCreationData::STriggerBox& aTriggerBox);
	static bool InitPointLight(SEntityHandle& aEntityHandle, const CU::Vector3f& aPosition, const CU::Vector3f& aColor, const float aRange, const float aIntensity);
	static bool InitSpotLight(SEntityHandle& aEntityHandle, const CU::Vector3f& aPosition, const CU::Vector3f& aColor, const CU::Vector3f& aDirection, const float aRange, const float aAngle, const float aIntensity);
	static bool InitDirectionalLight(SEntityHandle& aEntityHandle, const CU::Vector3f& aDirection, const CU::Vector3f& aColor, const float aIntensity);
	static bool InitParticle(SEntityHandle& aEntityHandle, SEntityCreationData::SParticleData& aParticleData);

	static EntityManager* ourEntityManagerVoid;
	CEntityFactory() = delete;
	~CEntityFactory() = delete;
};