#include "stdafx.h"
#include "StateStack.h"

#define sc_i(var)						static_cast<int>(var)
#define sc_ui(var)						static_cast<unsigned int>(var)
#define sc_us(var)						static_cast<unsigned short>(var)
#define DELETE_STATE(ptr)	\
if ((ptr) != nullptr)																								\
{																													\
	if (myInstancePtr->myCurrActiveStatePtr != nullptr && myInstancePtr->myCurrActiveStatePtr->myID == ptr->myID )	\
	{																												\
		myInstancePtr->myCurrActiveStatePtr = nullptr;																\
	}																												\
	ptr->OnDeactivation();																							\
	sce_delete(ptr);																								\
}
#define STATE_IS_VALID(aStatesState)			((aStatesState) == CState::EStateCommand::eKeepState || (aStatesState) == CState::EStateCommand::eActive)
#define STATE_CAN_BE_DEACTIVATED(aStatesState)	((aStatesState) != CState::EStateCommand::eRemoveMainState &&	\
												 (aStatesState) != CState::EStateCommand::eRemoveState &&		\
												 (aStatesState) == CState::EStateCommand::eActive)
#define STATE_IS_SET_FOR_REMOVAL(aStatesState)	((aStatesState) == CState::EStateCommand::eRemoveState || (aStatesState) == CState::EStateCommand::eRemoveMainState)

CStateStack* CStateStack::myInstancePtr = nullptr;

const bool CStateStack::Create()
{
	if (myInstancePtr != nullptr)
	{
		GAME_LOG("FAIL! Failed to create new instance of State Stack because it was already created.");
		return false;
	}

	myInstancePtr = sce_new(CStateStack());
	if (myInstancePtr == nullptr)
	{
		GAME_LOG("FAIL! Failed to allocate memory for the State Stack.");
		return false;
	}

	return true;
}

void CStateStack::Destroy()
{
	if (myInstancePtr == nullptr)
	{
		GAME_LOG("WARNING! Could not destroy State Stack instance because it does not exist.");
		return;
	}

	for (unsigned short i(0); i < myInstancePtr->myStates.Size(); ++i)
	{
		DELETE_STATE(myInstancePtr->myStates[i]);
	}

	myInstancePtr->myPrevActiveStatePtr = nullptr;
	myInstancePtr->myCurrActiveStatePtr = nullptr;
	myInstancePtr->myStates.RemoveAll();
	myInstancePtr->myNotInitializedStateList.RemoveAll();

	sce_delete(myInstancePtr);
}

void CStateStack::UpdateStateOnTop(const float aDeltaTime)
{
	if (myInstancePtr == nullptr)
	{
		GAME_LOG("WARNING! Trying to call State Stack's function before State Stack was created.");
		return;
	}

	for (int i(sc_i(myInstancePtr->myStates.Size()) - 1); i >= 0; --i)
	{
		if (myInstancePtr->myStates[sc_us(i)]->myState == CState::EStateCommand::eRemoveState ||
			myInstancePtr->myStates[sc_us(i)]->myState == CState::EStateCommand::eRemoveMainState)
		{
			
			DELETE_STATE(myInstancePtr->myStates[sc_us(i)]);
			myInstancePtr->myStates.RemoveCyclicAtIndex(sc_us(i));
			continue;
		}

		if (myInstancePtr->CheckUnderStatesForRemoval( myInstancePtr->myStates[sc_us(i)] ))
		{
			DELETE_STATE(myInstancePtr->myStates[sc_us(i)]);
			myInstancePtr->myStates.RemoveCyclicAtIndex(sc_us(i));
			continue;
		}
	}

	if (myInstancePtr->myStates.Size() == 0) return;
	myInstancePtr->UpdateStateOnTop(myInstancePtr->myStates.GetLast(), aDeltaTime);
}

void CStateStack::RenderApprovedStates()
{
	if (myInstancePtr == nullptr)
	{
		GAME_LOG("WARNING! Trying to call State Stack's function before State Stack was created.");
		return;
	}

	if (myInstancePtr->myStates.Size() == 0) return;
	myInstancePtr->RenderAvailableStates(myInstancePtr->myStates.GetLast());
}

const bool CStateStack::IsEmpty()
{
	if (myInstancePtr == nullptr)
	{
		GAME_LOG("WARNING! Trying to call State Stack's function before State Stack was created.");
		return true;
	}

	return myInstancePtr->myStates.Size() == 0;
}

void CStateStack::PushMainStateOnTop(void* aMainStatePtr)
{
	if (myInstancePtr == nullptr)
	{
		GAME_LOG("WARNING! Trying to call State Stack's function before State Stack was created.");
		return;
	}

	if (myInstancePtr->myFreeID == UINT_MAX)
	{
		GAME_LOG("ERROR! Created too many states. No more states will be created.");
		return;
	}

	CState* statePtr = (CState*)aMainStatePtr;
	statePtr->InitState(myInstancePtr->myFreeID, false);
	statePtr->myState = CState::EStateCommand::eMustInit;

	myInstancePtr->myStates.Add(statePtr);
	myInstancePtr->myNotInitializedStateList.Add(statePtr);

	myInstancePtr->myPrevActiveStatePtr = myInstancePtr->myCurrActiveStatePtr;
	myInstancePtr->myCurrActiveStatePtr = statePtr;

	if (myInstancePtr->myPrevActiveStatePtr != nullptr && STATE_CAN_BE_DEACTIVATED(myInstancePtr->myPrevActiveStatePtr->myState))
	{
		myInstancePtr->myStateToDeactivateList.Add(myInstancePtr->myPrevActiveStatePtr);
	}

	++myInstancePtr->myFreeID;
}

void CStateStack::PushUnderStateOnTop(void* aMainStatePtr, void* aUnderStatePtr, const bool aCanRenderMainState)
{
	if (myInstancePtr == nullptr)
	{
		GAME_LOG("WARNING! Trying to call State Stack's function before State Stack was created.");
		return;
	}

	if (myInstancePtr->myFreeID == UINT_MAX)
	{
		GAME_LOG("ERROR! Created too many states. No more states will be created.");
		return;
	}

	CState* mainStatePtr = (CState*)aMainStatePtr;
	CState* underStatePtr = (CState*)aUnderStatePtr;
	underStatePtr->InitState(myInstancePtr->myFreeID, aCanRenderMainState);
	underStatePtr->myState = CState::EStateCommand::eMustInit;

	mainStatePtr->myUnderStates.Add(underStatePtr);
	myInstancePtr->myNotInitializedStateList.Add(underStatePtr);

	myInstancePtr->myPrevActiveStatePtr = myInstancePtr->myCurrActiveStatePtr;
	myInstancePtr->myCurrActiveStatePtr = underStatePtr;

	if (myInstancePtr->myPrevActiveStatePtr != nullptr && STATE_CAN_BE_DEACTIVATED(myInstancePtr->myPrevActiveStatePtr->myState))
	{
		myInstancePtr->myStateToDeactivateList.Add(myInstancePtr->myPrevActiveStatePtr);
	}

	++myInstancePtr->myFreeID;
}

void CStateStack::InitStatesCreatedFromPreviousFrame()
{
	if (myInstancePtr == nullptr)
	{
		GAME_LOG("WARNING! Trying to call State Stack's function before State Stack was created.");
		return;
	}

	if (myInstancePtr->myNotInitializedStateList.Size() > 0)
	{
		for (unsigned short i(0); i < myInstancePtr->myNotInitializedStateList.Size(); ++i)
		{
			myInstancePtr->myNotInitializedStateList[i]->myState = CState::EStateCommand::eKeepState;
			myInstancePtr->myNotInitializedStateList[i]->Init();
		}
		myInstancePtr->myNotInitializedStateList.RemoveAll();
	}

	myInstancePtr->CheckStatesForDeactivation();
	myInstancePtr->CheckStateOnTopForActivation();
}

/* PRIVATE FUNCTIONS */

CStateStack::CStateStack()
{
	myStates.Init(16);
	myNotInitializedStateList.Init(16);
	myStateToDeactivateList.Init(16);
	myPrevActiveStatePtr = nullptr;
	myCurrActiveStatePtr = nullptr;
	myFreeID = 0;
}

const bool CStateStack::CheckUnderStatesForRemoval(CState*& aStatePtr)
{
	bool foundMainStateRemove(false);

	for (int i(sc_i(aStatePtr->myUnderStates.Size()) - 1); i >= 0; --i)
	{
		if (aStatePtr->myUnderStates[sc_us(i)]->myState == CState::EStateCommand::eRemoveState)
		{
			DELETE_STATE(aStatePtr->myUnderStates[sc_us(i)]);
			aStatePtr->myUnderStates.RemoveCyclicAtIndex(sc_us(i));
			continue;
		}
		else if (aStatePtr->myUnderStates[sc_us(i)]->myState == CState::EStateCommand::eRemoveMainState)
		{
			foundMainStateRemove = true;
		}

		if (CheckUnderStatesForRemoval(aStatePtr->myUnderStates[sc_us(i)]))
		{
			DELETE_STATE(aStatePtr->myUnderStates[sc_us(i)]);
			aStatePtr->myUnderStates.RemoveCyclicAtIndex(sc_us(i));
			continue;
		}
	}

	return foundMainStateRemove;
}

void CStateStack::UpdateStateOnTop(CState*& aStatePtr, const float aDeltaTime)
{
	if (aStatePtr->myUnderStates.Size() > 0)
	{
		UpdateStateOnTop(aStatePtr->myUnderStates.GetLast(), aDeltaTime);
	}
	else if (STATE_IS_VALID(aStatePtr->myState))
	{
		if ((myCurrActiveStatePtr == nullptr) || (myCurrActiveStatePtr->myID != aStatePtr->myID))
		{
			myPrevActiveStatePtr = myCurrActiveStatePtr;
			myCurrActiveStatePtr = aStatePtr;

			myCurrActiveStatePtr->myState = CState::EStateCommand::eActive;
			myCurrActiveStatePtr->OnActivation();
		}

		aStatePtr->Update(aDeltaTime);
	}
}

void CStateStack::RenderAvailableStates(CState*& aStatePtr)
{
	if (aStatePtr->myUnderStates.Size() > 0)
	{
		if (aStatePtr->myUnderStates.GetLast()->myCanRenderMainState)
		{
			aStatePtr->Render();
		}

		RenderAvailableStates(aStatePtr->myUnderStates.GetLast());
	}
	else if (STATE_IS_VALID(aStatePtr->myState))
	{
		aStatePtr->Render();
	}
}

void CStateStack::CheckStatesForDeactivation()
{
	if (myStateToDeactivateList.size() > 0)
	{
		for (unsigned short i(0); i < myStateToDeactivateList.size(); ++i)
		{
			CState* currStateToDeactivate_ptr(myStateToDeactivateList[i]);

			if (STATE_IS_SET_FOR_REMOVAL(currStateToDeactivate_ptr->myState) == false)
			{
				currStateToDeactivate_ptr->myState = CState::EStateCommand::eKeepState;
				currStateToDeactivate_ptr->OnDeactivation();
			}
		}

		myStateToDeactivateList.RemoveAll();
	}
}

void CStateStack::CheckStateOnTopForActivation()
{
	if (myCurrActiveStatePtr != nullptr)
	{
		if (STATE_IS_SET_FOR_REMOVAL(myCurrActiveStatePtr->myState) == false && myCurrActiveStatePtr->myState != CState::EStateCommand::eActive)
		{
			myCurrActiveStatePtr->myState = CState::EStateCommand::eActive;
			myCurrActiveStatePtr->OnActivation();
		}
	}
}
