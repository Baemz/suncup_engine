#pragma once
#include "ControllerIncludes.h"

//TODO: stop users from initing controllers incorrectly (i.e. an input controller with AI init data or a controller without a controller type).
#ifdef _DEBUG
#define CHECK_IS_RIGHT_CONTROLLER_TYPE (aInitStructName,  aMatchesInitStruct) \

#else

#endif

#define DEFINE_CONTROLLER_CONSTRUCTOR(aControllerInitStructType, aControllerInitStructArgument, aControllerMemberInitStruct) \
SControllerWrapperInit(const SControllerInit& aControllerInit, EController aControllerType, const aControllerInitStructType& aControllerInitStructArgument) \
	: myControllerBaseInitData(aControllerInit)										\
	, myControllerType(aControllerType)												\
	, aControllerMemberInitStruct(aControllerInitStructArgument)					\
	{																				\
		if (std::string(#aControllerInitStructType) == "SInputInit" && aControllerType != EController::Input)	\
		{																			\
			assert("Init data does not match controller type." && false);			\
		}																			\
		assert("Controller needs a controller type." && aControllerType != EController::NONE); \
	}																				\

#define DESTROY_CORRECT_INITDATA																								\
if (myControllerType == EController::NONE) assert("Tried to destroy controller init data without a controller type." && false); \
else if (myControllerType == EController::Input) myInputInitData.~SInputInit();													\
else if (myControllerType == EController::AI) myAIInitData.~SAIInit();															\
else if (myControllerType == EController::Script) myScriptInitData.~SScriptInit();												\

struct SControllerWrapperInit
{
	SControllerWrapperInit() = delete;
	~SControllerWrapperInit() { DESTROY_CORRECT_INITDATA }
	DEFINE_CONTROLLER_CONSTRUCTOR(SInputInit, aInputInitData, myInputInitData)
	DEFINE_CONTROLLER_CONSTRUCTOR(SAIInit, aAIInitData, myAIInitData)
	DEFINE_CONTROLLER_CONSTRUCTOR(SScriptInit, aScriptInitData, myScriptInitData)

	SControllerInit myControllerBaseInitData;
	EController myControllerType;
	union
	{
		SInputInit myInputInitData;
		SAIInit myAIInitData;
		SScriptInit myScriptInitData;
	};
};

class CControllerWrapper
{
public:
	CControllerWrapper();
	CControllerWrapper(const SControllerWrapperInit& aInitData);

	~CControllerWrapper();

	CController& Get();
	const CController& Get() const;
	CInputController& GetInput();
	const CInputController& GetInput() const;
	CAIController& GetAI();
	const CAIController& GetAI() const;
	CScriptController& GetScript();
	const CScriptController& GetScript() const;

private:
	CController* myController;
	EController myControllerType;
};