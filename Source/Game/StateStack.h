#pragma once
#include "State.h"

class CStateStack
{
public:
	~CStateStack() = default;
	
	static const bool Create();
	static void Destroy();

	static void UpdateStateOnTop(const float aDeltaTime);
	static void RenderApprovedStates();

	static const bool IsEmpty();
	static void PushMainStateOnTop(void* aMainStatePtr);
	static void PushUnderStateOnTop(void* aMainStatePtr, void* aUnderStatePtr, const bool aCanRenderMainState);
	static void InitStatesCreatedFromPreviousFrame();

private:
	CStateStack();
	CStateStack(const CStateStack& aStateStack) = delete;
	void operator=(const CStateStack& aStateStack) = delete;

	// Returns true if the main state should be removed.
	const bool CheckUnderStatesForRemoval(CState*& aStatePtr);
	void UpdateStateOnTop(CState*& aStatePtr, const float aDeltaTime);
	void RenderAvailableStates(CState*& aStatePtr);

	void CheckStatesForDeactivation();
	void CheckStateOnTopForActivation();

private:
	static CStateStack* myInstancePtr;

	CU::GrowingArray<CState*> myStates;
	CU::GrowingArray<CState*> myNotInitializedStateList;
	CU::GrowingArray<CState*> myStateToDeactivateList;
	CState* myPrevActiveStatePtr;
	CState* myCurrActiveStatePtr;
	unsigned int myFreeID;
};