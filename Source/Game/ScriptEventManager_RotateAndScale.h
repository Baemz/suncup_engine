#pragma once

class CScriptEventManager;

class CScriptEventManager_RotateAndScale
{
public:
	static bool InitFunctions(CScriptEventManager& aScriptEventManager);

private:
	CScriptEventManager_RotateAndScale() = delete;
	~CScriptEventManager_RotateAndScale() = delete;

};
