#pragma once
#include "State.h"
#include "..\EngineCore\EventSystem\EventReceiver.h"
#include "..\GraphicsEngine\Camera\CameraInstance.h"
#include "..\GraphicsEngine\Model\ModelInstance.h"
#include "..\GraphicsEngine\Lights\EnvironmentalLight.h"
#include "..\GraphicsEngine\Lights\DirectionalLight.h"

class CTestScene : private CState, public sce::CEventReceiver
{
public:
	CTestScene(EManager& aEntityManager);
	~CTestScene();

	void Init() override;
	void Update(const float aDeltaTime) override;
	void Render() override;

	void OnActivation() override;
	void OnDeactivation() override;

	void ReceiveEvent(const Event::SEvent& aEvent) override;

private:

	sce::gfx::CCameraInstance myCamInst;
	sce::gfx::CModelInstance mymdl;
	sce::gfx::CModelInstance myFloor1;
	sce::gfx::CModelInstance myFloor2;
	sce::gfx::CModelInstance myFloor3;
	sce::gfx::CModelInstance myFloor4;
	sce::gfx::CModelInstance knife;
	sce::gfx::CModelInstance couch;
	sce::gfx::CModelInstance robot2;
	sce::gfx::CModelInstance robot3;
	sce::gfx::CModelInstance myskybox;
	sce::gfx::CEnvironmentalLight myEnvLight;
	sce::gfx::CSpotLightInstance mySpotLight;
	sce::gfx::CPointLightInstance myPointLight2;
	sce::gfx::CPointLightInstance myPointLight3;
	sce::gfx::CPointLightInstance myPointLight4;
	sce::gfx::CDirectionalLight myDirLight;
	float dt;
	bool usePointLight;
};

