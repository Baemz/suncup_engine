#pragma once
#include "../CommonUtilities/Vector3.h"
#include "../AudioEngine/AudioEngineTypedefs.h"

//Also define in AudioWrapper.h!
#define AUDIO_ENGINE_OFF
#ifdef _MATEDIT
#define AUDIO_ENGINE_OFF
#endif
namespace AudioEngine
{
	class CAudioEngine;
};

class CAudioManager
{
public:
	~CAudioManager();

	static void Create(const char* aInitBankPath = "Data/Audio/Init.bnk");
	static void Destroy();
	static void RenderAudio();

	static void LoadBank(const char* aBankPath);
	static void RegisterObject(const AudioEngine::RegObjectID aObjectID);
	static void UnRegisterObject(const AudioEngine::RegObjectID aObjectID);

	static const AudioEngine::PlayingObjectID PostEventAndGetID(const char* aEventName, const AudioEngine::RegObjectID aObjectID);
	static void SetRTPC(const char* aRTPC_Name, const float aValue, const AudioEngine::RegObjectID aObjectID);
	static void SetRTPC(const char* aRTPC_Name, const float aValue);
	static void SetState(const char* aStateGroupName, const char* aStateName);

	static void SetListenerPositionAndOrientation(const CU::Vector3f& aPosition, const CU::Vector3f& aFront, const CU::Vector3f& aTop);
	static void SetObjectPositionAndOrientation(const CU::Vector3f& aPosition, const CU::Vector3f& aFront, const CU::Vector3f& aTop, const AudioEngine::RegObjectID aObjectID);

private:
	CAudioManager();
	CAudioManager(CAudioManager&& aAudioManager) = delete;
	CAudioManager(const CAudioManager& aAudioManager) = delete;
	void operator=(const CAudioManager& aAudioManager) = delete;

private:
	static CAudioManager* myInstancePtr;

	AudioEngine::CAudioEngine* myAudioEnginePtr;
};

