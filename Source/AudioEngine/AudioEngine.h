#pragma once
#include "AudioEngineTypedefs.h"
#include "../CommonUtilities/GrowingArray.h"

namespace AudioEngine
{
	class CAudioEngine
	{
	public:
		CAudioEngine();
		~CAudioEngine();
		const bool InitializeSystem(const char* aInitBankPath);
		void TermWwise();
		const bool RenderAudio() const;

		//Game functions
		const bool LoadBank(const char* aBankPath) const;
		const bool RegisterObject(const RegObjectID aObjectID);
		const bool UnRegisterObject(const RegObjectID aObjectID);

		const PlayingObjectID PostEvent(const char* aEventName, const RegObjectID aObjectID) const;
		const bool SetRTPC(const char* aRTPC_Name, const float aValue, const RegObjectID aObjectID) const;
		const bool SetRTPC(const char* aRTPC_Name, const float aValue) const;
		const bool SetState(const char* aStateGroupName, const char* aStateName) const;

		const bool SetListenerPositionAndOrientation(const float aX, const float aY, const float aZ,
			const float aFrontX, const float aFrontY, const float aFrontZ,
			const float aTopX, const float aTopY, const float aTopZ) const;
		const bool SetObjectPositionAndOrientation(const RegObjectID aObjectID,
			const float aX, const float aY, const float aZ,
			const float aFrontX, const float aFrontY, const float aFrontZ,
			const float aTopX, const float aTopY, const float aTopZ) const;
	private:
		const bool SetPositionAndOrientationOnCustomObject(const RegObjectID aObjectID,
			const float aX, const float aY, const float aZ,
			const float aFrontX, const float aFrontY, const float aFrontZ,
			const float aTopX, const float aTopY, const float aTopZ) const;

	private:
		CU::GrowingArray<RegObjectID, RegObjectID> myRegisteredObjList;
	};
};