#include "stdafx.h"
#include "AudioManager.h"

#ifndef AUDIO_ENGINE_OFF
#include "AudioEngine.h"
#include "../EngineCore/MemoryPool/MemoryPool.h"
#include "../EngineCore/Logger/Logger.h"
#endif

CAudioManager* CAudioManager::myInstancePtr = nullptr;

using namespace AudioEngine;

CAudioManager::CAudioManager()
	: myAudioEnginePtr(nullptr)
{
}

CAudioManager::~CAudioManager()
{
}

void CAudioManager::Create(const char* aInitBankPath)
{
#ifndef AUDIO_ENGINE_OFF
	if (myInstancePtr != nullptr)
	{
		AUDIO_LOG("[CAudioManager] WARNING! Failed to create new instance of Audio Manager because it was already created.");
		return;
	}

	myInstancePtr = sce_new(CAudioManager());
	if (myInstancePtr == nullptr)
	{
		AUDIO_LOG("[CAudioManager] ERROR! Failed to allocate memory for Audio Manager.");
		return;
	}

	myInstancePtr->myAudioEnginePtr = sce_new(CAudioEngine());
	if (myInstancePtr->myAudioEnginePtr == nullptr)
	{
		AUDIO_LOG("[CAudioManager] ERROR! Failed to allocate memory for Audio Engine.");
		return;
	}

	if (!myInstancePtr->myAudioEnginePtr->InitializeSystem(aInitBankPath))
	{
		AUDIO_LOG("[CAudioManager] ERROR! Failed to initialize Audio Engine using bank: \"%s\".", aInitBankPath);
		return;
	}

	AUDIO_LOG("[CAudioManager] SUCCESS! Successfully created Audio Manager.");
#endif
#ifdef AUDIO_ENGINE_OFF
	aInitBankPath;
#endif
}

void CAudioManager::Destroy()
{
#ifndef AUDIO_ENGINE_OFF
	if (myInstancePtr == nullptr)
	{
		AUDIO_LOG("[CAudioManager] WARNING! Could not destroy Audio Manager because it was nullptr.");
		return;
	}

	if (myInstancePtr->myAudioEnginePtr != nullptr)
	{
		myInstancePtr->myAudioEnginePtr->TermWwise();

		sce_delete(myInstancePtr->myAudioEnginePtr);
		myInstancePtr->myAudioEnginePtr = nullptr;
	}
	else
	{
		AUDIO_LOG("[CAudioManager] WARNING! Could not destroy Audio Engine because it was nullptr.");
	}

	sce_delete(myInstancePtr);
#endif
}

void CAudioManager::RenderAudio()
{
#ifndef AUDIO_ENGINE_OFF
	if (myInstancePtr == nullptr)
	{
		AUDIO_LOG("[CAudioManager] WARNING! Could not render audio because Audio Manager was nullptr.");
		return;
	}
	if (myInstancePtr->myAudioEnginePtr == nullptr)
	{
		AUDIO_LOG("[CAudioManager] WARNING! Could not render audio because Audio Engine was nullptr.");
		return;
	}

	if (!myInstancePtr->myAudioEnginePtr->RenderAudio())
	{
		AUDIO_LOG("[CAudioManager] ERROR! Failed to render audio.");
	}
#endif
}

void CAudioManager::LoadBank(const char* aBankPath)
{
#ifndef AUDIO_ENGINE_OFF
	if (myInstancePtr == nullptr)
	{
		AUDIO_LOG("[CAudioManager] WARNING! Could not load a sound bank (Path: \"%s\") because Audio Manager was nullptr.", aBankPath);
		return;
	}
	if (myInstancePtr->myAudioEnginePtr == nullptr)
	{
		AUDIO_LOG("[CAudioManager] WARNING! Could not load a sound bank (Path: \"%s\") because Audio Engine was nullptr.", aBankPath);
		return;
	}

	if (!myInstancePtr->myAudioEnginePtr->LoadBank(aBankPath))
	{
		AUDIO_LOG("[CAudioManager] ERROR! Failed to load a sound bank (Path: \"%s\").", aBankPath);
	}
#endif
#ifdef AUDIO_ENGINE_OFF
	aBankPath;
#endif
}

void CAudioManager::RegisterObject(const RegObjectID aObjectID)
{
#ifndef AUDIO_ENGINE_OFF
	if (myInstancePtr == nullptr)
	{
		AUDIO_LOG("[CAudioManager] WARNING! Could not register an object (ID: %i) because Audio Manager was nullptr.", aObjectID);
		return;
	}
	if (myInstancePtr->myAudioEnginePtr == nullptr)
	{
		AUDIO_LOG("[CAudioManager] WARNING! Could not register an object (ID: %i) because Audio Engine was nullptr.", aObjectID);
		return;
	}

	if (!myInstancePtr->myAudioEnginePtr->RegisterObject(aObjectID))
	{
		AUDIO_LOG("[CAudioManager] ERROR! Failed to register an object (ID: %i).", aObjectID);
	}
#endif
#ifdef AUDIO_ENGINE_OFF
	aObjectID;
#endif
}

void CAudioManager::UnRegisterObject(const RegObjectID aObjectID)
{
#ifndef AUDIO_ENGINE_OFF
	if (myInstancePtr == nullptr)
	{
		AUDIO_LOG("[CAudioManager] WARNING! Could not unregister an object (ID: %i) because Audio Manager was nullptr.", aObjectID);
		return;
	}
	if (myInstancePtr->myAudioEnginePtr == nullptr)
	{
		AUDIO_LOG("[CAudioManager] WARNING! Could not unregister an object (ID: %i) because Audio Engine was nullptr.", aObjectID);
		return;
	}

	if (!myInstancePtr->myAudioEnginePtr->UnRegisterObject(aObjectID))
	{
		AUDIO_LOG("[CAudioManager] ERROR! Failed to unregister an object (ID: %i).", aObjectID);
	}
#endif
#ifdef AUDIO_ENGINE_OFF
	aObjectID;
#endif
}

const PlayingObjectID CAudioManager::PostEventAndGetID(const char* aEventName, const RegObjectID aObjectID)
{
#ifndef AUDIO_ENGINE_OFF
	if (myInstancePtr == nullptr)
	{
		AUDIO_LOG("[CAudioManager] WARNING! Could not post event (Event name: %s, Object ID: %i) because Audio Manager was nullptr.", aEventName, aObjectID);
		return 0;
	}
	if (myInstancePtr->myAudioEnginePtr == nullptr)
	{
		AUDIO_LOG("[CAudioManager] WARNING! Could not post event (Event name: %s, Object ID: %i) because Audio Engine was nullptr.", aEventName, aObjectID);
		return 0;
	}

	const unsigned long id = myInstancePtr->myAudioEnginePtr->PostEvent(aEventName, aObjectID);
	if (id == 0)
	{
		AUDIO_LOG("[CAudioManager] ERROR! Failed to post event (Event name: %s, Object ID: %i).", aEventName, aObjectID);
	}

	return id;
#endif
#ifdef AUDIO_ENGINE_OFF
	aEventName;
	aObjectID;

	return 0;
#endif
}

void CAudioManager::SetRTPC(const char* aRTPC_Name, const float aValue, const RegObjectID aObjectID)
{
#ifndef AUDIO_ENGINE_OFF
	if (myInstancePtr == nullptr)
	{
		AUDIO_LOG("[CAudioManager] WARNING! Could not set RTPC (RTPC name: %s, Value: %f, Object ID: %i) because Audio Manager was nullptr.", aRTPC_Name, aValue, aObjectID);
		return;
	}
	if (myInstancePtr->myAudioEnginePtr == nullptr)
	{
		AUDIO_LOG("[CAudioManager] WARNING! Could not set RTPC (RTPC name: %s, Value: %f, Object ID: %i) because Audio Engine was nullptr.", aRTPC_Name, aValue, aObjectID);
		return;
	}

	if (!myInstancePtr->myAudioEnginePtr->SetRTPC(aRTPC_Name, aValue, aObjectID))
	{
		AUDIO_LOG("[CAudioManager] ERROR! Failed to set RTPC (RTPC name: %s, Value: %f, Object ID: %i).", aRTPC_Name, aValue, aObjectID);
	}
#endif
#ifdef AUDIO_ENGINE_OFF
	aRTPC_Name;
	aValue;
	aObjectID;
#endif
}

void CAudioManager::SetRTPC(const char* aRTPC_Name, const float aValue)
{
#ifndef AUDIO_ENGINE_OFF
	if (myInstancePtr == nullptr)
	{
		AUDIO_LOG("[CAudioManager] WARNING! Could not set RTPC for all audio objects (RTPC name: %s, Value: %f) because Audio Manager was nullptr.", aRTPC_Name, aValue);
		return;
	}
	if (myInstancePtr->myAudioEnginePtr == nullptr)
	{
		AUDIO_LOG("[CAudioManager] WARNING! Could not set RTPC for all audio objects (RTPC name: %s, Value: %f) because Audio Engine was nullptr.", aRTPC_Name, aValue);
		return;
	}

	if (!myInstancePtr->myAudioEnginePtr->SetRTPC(aRTPC_Name, aValue))
	{
		AUDIO_LOG("[CAudioManager] ERROR! Failed to set RTPC for all audio objects (RTPC name: %s, Value: %f).", aRTPC_Name, aValue);
	}
#endif
#ifdef AUDIO_ENGINE_OFF
	aRTPC_Name;
	aValue;
#endif
}

void CAudioManager::SetState(const char* aStateGroupName, const char* aStateName)
{
#ifndef AUDIO_ENGINE_OFF
	if (myInstancePtr == nullptr)
	{
		AUDIO_LOG("[CAudioManager] WARNING! Could not set state (State group name: %s, State name: %f) because Audio Manager was nullptr.", aStateGroupName, aStateName);
		return;
}
	if (myInstancePtr->myAudioEnginePtr == nullptr)
	{
		AUDIO_LOG("[CAudioManager] WARNING! Could not set state (State group name: %s, State name: %f) because Audio Engine was nullptr.", aStateGroupName, aStateName);
		return;
	}

	if (!myInstancePtr->myAudioEnginePtr->SetState(aStateGroupName, aStateName))
	{
		AUDIO_LOG("[CAudioManager] ERROR! Failed to set state (State group name: %s, State name: %f).", aStateGroupName, aStateName);
	}
#endif
#ifdef AUDIO_ENGINE_OFF
	aStateGroupName;
	aStateName;
#endif
}

void CAudioManager::SetListenerPositionAndOrientation(const CU::Vector3f& aPosition, const CU::Vector3f& aFront, const CU::Vector3f& aTop)
{
#ifndef AUDIO_ENGINE_OFF
	if (myInstancePtr == nullptr)
	{
		AUDIO_LOG("[CAudioManager] WARNING! Could not set listener's position and orientation because Audio Manager was nullptr.");
		return;
	}
	if (myInstancePtr->myAudioEnginePtr == nullptr)
	{
		AUDIO_LOG("[CAudioManager] WARNING! Could not set listener's position and orientation because Audio Engine was nullptr.");
		return;
	}

	if (!myInstancePtr->myAudioEnginePtr->SetListenerPositionAndOrientation(aPosition.x, aPosition.y, aPosition.z, aFront.x, aFront.y, aFront.z, aTop.x, aTop.y, aTop.z))
	{
		AUDIO_LOG("[CAudioManager] ERROR! Failed to set listener's position and orientation.");
	}
#endif
#ifdef AUDIO_ENGINE_OFF
	aPosition;
	aFront;
	aTop;
#endif
}

void CAudioManager::SetObjectPositionAndOrientation(const CU::Vector3f& aPosition, const CU::Vector3f& aFront, const CU::Vector3f& aTop, const RegObjectID aObjectID)
{
#ifndef AUDIO_ENGINE_OFF
	if (myInstancePtr == nullptr)
	{
		AUDIO_LOG("[CAudioManager] WARNING! Could not set object's position and orientation (Object ID: %i) because Audio Manager was nullptr.", aObjectID);
		return;
	}
	if (myInstancePtr->myAudioEnginePtr == nullptr)
	{
		AUDIO_LOG("[CAudioManager] WARNING! Could not set object's position and orientation (Object ID: %i) because Audio Engine was nullptr.", aObjectID);
		return;
	}

	if (!myInstancePtr->myAudioEnginePtr->SetObjectPositionAndOrientation(aObjectID, aPosition.x, aPosition.y, aPosition.z, aFront.x, aFront.y, aFront.z, aTop.x, aTop.y, aTop.z))
	{
		AUDIO_LOG("[CAudioManager] ERROR! Failed to set object's position and orientation (Object ID: %i).", aObjectID);
	}
#endif
#ifdef AUDIO_ENGINE_OFF
	aPosition;
	aFront;
	aTop;
#endif
}
