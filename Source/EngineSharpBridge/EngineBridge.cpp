#include "EngineBridge.h"
#include "..\EngineCore\Engine.h"
#include "MaterialPreviewer.h"
#include "..\GraphicsEngine\WindowHandler\WindowData.h"
#include <functional>

using namespace System::Runtime::InteropServices;



using namespace std::placeholders;
using namespace sce;

namespace sceWrap
{
	CEngineBridge::CEngineBridge()
		: myPreviewer(nullptr)
	{
		myEngine = new sce::CEngine();
	}

	CEngineBridge::~CEngineBridge()
	{
		delete myEngine;
		myEngine = nullptr;
		delete myPreviewer;
		myPreviewer = nullptr;
	}

	void sceWrap::CEngineBridge::InitMaterialPreview(System::IntPtr aHWND, unsigned short aWndWidth, unsigned short aWndHeight)
	{
		myPreviewer = new CMaterialPreviewer();
		CMaterialPreviewer& pre(*myPreviewer);

		SEngineStartParams startParams;
		startParams.myInitCallback = std::bind(&CMaterialPreviewer::Init, &pre);
		startParams.myUpdateCallback = std::bind(&CMaterialPreviewer::Update, &pre, _1);
		startParams.myShutdownCallback = nullptr;
		
		sce::gfx::SWindowData winData;
		winData.myWidth = aWndWidth;
		winData.myHeight = aWndHeight;
		winData.myUseBloom = true;
		winData.myUseSSAO = true;
		winData.myUseFXAA = true;
		winData.myUseVSync = true;
		winData.myStartInFullScreen = false;
		winData.myWindowName = "";
		winData.myHWND = aHWND.ToPointer();

		myEngine->InitWithWindowData(winData, startParams);
	}
	void CEngineBridge::Start()
	{
		myEngine->Start();
	}
	void CEngineBridge::Stop()
	{
		myEngine->Shutdown();
	}
	void CEngineBridge::RotateX(float aDelta)
	{
		myPreviewer->RotateX(aDelta);
	}
	void CEngineBridge::RotateY(float aDelta)
	{
		myPreviewer->RotateY(aDelta);
	}
	void CEngineBridge::ResetRotation()
	{
		myPreviewer->ResetRotation();
	}
	void CEngineBridge::SetModel(int aIndex)
	{
		myPreviewer->SetModel(aIndex);
	}
	void CEngineBridge::SetModel(System::String^ aIndex)
	{
		char* str = (char*)System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(aIndex).ToPointer();
		std::string string(str);
		myPreviewer->SetModel(string);
	}
	void CEngineBridge::Zoom(const float aDelta)
	{
		myPreviewer->Zoom(aDelta);
	}
}
