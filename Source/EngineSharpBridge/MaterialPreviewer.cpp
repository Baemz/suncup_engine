#include "MaterialPreviewer.h"
#include "..\GraphicsEngine\Model\ModelInstance.h"
#include "..\GraphicsEngine\Camera\CameraFactory.h"
#include "..\GraphicsEngine\Camera\CameraInstance.h"
#include "../EngineCore/MemoryPool/MemoryPool.h"
#include "..\GraphicsEngine\Model\Loading\ModelFactory.h"
#include "..\GraphicsEngine\Lights\DirectionalLight.h"
#include "..\GraphicsEngine\Lights\EnvironmentalLight.h"

sce::CMaterialPreviewer::CMaterialPreviewer()
	: myCam(nullptr)
	, myMdl(nullptr)
	, myBox(nullptr)
	, mySphere(nullptr)
	, myCylinder(nullptr)
	, myDirLight(nullptr)
	, myEnvLight(nullptr)
	, myCustomModel(nullptr)
{
}

sce::CMaterialPreviewer::~CMaterialPreviewer()
{
}

void sce::CMaterialPreviewer::Init()
{
	myCam = sce_new(gfx::CCameraInstance());
	*myCam = gfx::CCameraFactory::Get()->CreateCameraAtPosition({ 0.f, 0.f, -2.f });
	myCam->SetPosition({ 0.f, 0.f, -2.f });
	myBox = sce_new(gfx::CModelInstance());
	myBox->SetModelPath("Data/Models/Misc/nC_wMat.fbx");
	myBox->SetMaterialPath("Data/Materials/PreviewTempMaterial/temp_Material.material");
	myBox->SetScale({ 1.3f, 1.3f, 1.3f });
	myBox->Init();
	mySphere = sce_new(gfx::CModelInstance());
	mySphere->SetModelPath("Data/Models/Misc/material_preview_sphere.fbx");
	mySphere->SetMaterialPath("Data/Materials/PreviewTempMaterial/temp_Material.material");
	mySphere->Init();
	myCylinder = sce_new(gfx::CModelInstance());
	myCylinder->SetModelPath("Data/Models/Misc/material_preview_cylinder.fbx");
	myCylinder->SetMaterialPath("Data/Materials/PreviewTempMaterial/temp_Material.material");
	myCylinder->SetScale({ 0.8f, 0.8f, 0.8f });
	myCylinder->Init();
	myMdl = myBox;

	myEnvLight = sce_new(sce::gfx::CEnvironmentalLight);
	myEnvLight->Init("Data/Models/Misc/Material_Preview_Skybox.dds");
	myEnvLight->UseForRendering();

	mySky = sce_new(sce::gfx::CModelInstance);
	*mySky = sce::gfx::CModelFactory::Get()->CreateSkyboxCube("Data/Models/Misc/Material_Preview_Skybox.dds");
	mySky->Init();

	myDirLight = sce_new(sce::gfx::CDirectionalLight);
	myDirLight->myLightData.myToLightDirection = { 0.f, 0.f, -1.f, 0.f };
	myDirLight->myLightData.myColor = {1.f, 1.f, 1.f, 1.f};
}

void sce::CMaterialPreviewer::Update(const float aDeltaTime)
{
	myDeltaTime = aDeltaTime;
	myCam->UseForRendering();
	myMdl->Render();
	mySky->Render();
	myDirLight->UseForRendering();
	myEnvLight->UseForRendering();
}

void sce::CMaterialPreviewer::RotateX(float aDelta)
{
	CU::Matrix44f mat;
	mat = mat.CreateRotateAroundY(aDelta);
	CU::Matrix44f rotation = myMdl->GetOrientation() * mat;
	myMdl->SetOrientation(rotation);
}

void sce::CMaterialPreviewer::RotateY(float aDelta)
{
	CU::Matrix44f mat;
	mat = mat.CreateRotateAroundX(aDelta);
	CU::Matrix44f rotation = myMdl->GetOrientation() * mat;
	myMdl->SetOrientation(rotation);


}

void sce::CMaterialPreviewer::ResetRotation()
{
	myMdl->SetOrientation(CU::Matrix44f());
}

void sce::CMaterialPreviewer::SetModel(int aIndex)
{
	switch (aIndex)
	{
	case 0:
		myMdl = myBox;
		break;
	case 1:
		myMdl = mySphere;
		break;
	case 2:
		myMdl = myCylinder;
		break;
	default:
		myMdl = myBox;
		break;
	}
	myCam->SetPosition({ 0.f, 0.f, -2.f });
}

void sce::CMaterialPreviewer::SetModel(const std::string& aIndex)
{
	sce_delete(myCustomModel);
	myCustomModel = sce_new(sce::gfx::CModelInstance);
	myCustomModel->SetModelPath(aIndex.c_str());
	myCustomModel->SetMaterialPath("Data/Materials/PreviewTempMaterial/temp_Material.material");
	myCustomModel->SetAffectedByFog(false);
	myCustomModel->Init();
	myMdl = myCustomModel;
	FocusCamera();
}

void sce::CMaterialPreviewer::Zoom(const float aDelta)
{
	CU::Vector3f dir(myMdl->centerPos - myCam->GetPosition());
	dir.Normalize();
	dir *= aDelta;
	myCam->Move(dir, 0.1f);
}

void sce::CMaterialPreviewer::FocusCamera()
{
	while (myCustomModel->IsLoaded() == false)
	{
	}
	CU::Vector3f pos = myMdl->centerPos;
	pos.z -= myMdl->GetFrustumCollider().GetRadius();
	myCam->SetPosition(pos);
}
