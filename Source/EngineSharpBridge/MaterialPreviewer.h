#pragma once
#include <string>

namespace sce
{
	namespace gfx 
	{
		class CModelInstance;
		class CCameraInstance;
		class CDirectionalLight;
		class CEnvironmentalLight;
	}

	class CMaterialPreviewer
	{
	public:
		CMaterialPreviewer();
		~CMaterialPreviewer();

		void Init();
		void Update(const float aDeltaTime);

		void RotateX(float aDelta);
		void RotateY(float aDelta);
		void ResetRotation();
		void SetModel(int aIndex);
		void SetModel(const std::string& aIndex);
		void Zoom(const float aDelta);

	private:
		float myDeltaTime;
		gfx::CModelInstance* myMdl;
		gfx::CModelInstance* myBox;
		gfx::CModelInstance* mySphere;
		gfx::CModelInstance* myCylinder;
		gfx::CModelInstance* myCustomModel;
		gfx::CModelInstance* mySky;
		gfx::CCameraInstance* myCam;
		gfx::CDirectionalLight* myDirLight;
		gfx::CEnvironmentalLight* myEnvLight;


		void FocusCamera();
	};
}
