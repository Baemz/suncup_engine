#pragma once

#ifdef DLL_EXPORT
#define DLLAPI __declspec(dllexport)
#else
#define DLLAPI __declspec(dllimport)
#endif

namespace sce
{
	class CEngine;
	class CMaterialPreviewer;
}
namespace sceWrap
{
	public ref class CEngineBridge
	{
	public:
		CEngineBridge();
		~CEngineBridge();

		void InitMaterialPreview(System::IntPtr aHWND, unsigned short aWndWidth, unsigned short aWndHeight);
		void Start();
		void Stop();

		void RotateX(float aDelta);
		void RotateY(float aDelta);
		void ResetRotation();
		void SetModel(int aIndex);
		void SetModel(System::String^ aModel);
		void Zoom(const float aDelta);

	private:
		sce::CEngine* myEngine;
		sce::CMaterialPreviewer* myPreviewer;
	};

}