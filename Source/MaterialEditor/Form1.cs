﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.IO;
using System.Threading.Tasks;
using System.Threading;
using Newtonsoft.Json;



namespace MaterialEditor
{
    public partial class Form1 : Form
    {
        private struct MaterialData
        {
            public uint renderMode;
            public ShaderData vertexShader;
            public ShaderData pixelShader;
            public TextureData[] textureData;
        };
        private struct TextureData
        {
            public string path;
            public uint slot;
        }
        private struct ShaderData
        {
            public string path;
            public string entry;
        }
        private MaterialData matData = new MaterialData();
        private MaterialData tempmatData = new MaterialData();
        private MaterialData defmatData = new MaterialData();
        private int textureSelectedIndex = -1;
        private sceWrap.CEngineBridge engine;
        private Thread engineThread = null;
        public Form1()
        {
            InitializeComponent();
            this.MouseWheel += new MouseEventHandler(texturePreview_MouseWheel);

            groupBox3.Enabled = false;
            renderMode.SelectedIndex = 0;
            model.SelectedIndex = 0;
            textureList.Sorted = true;

            registerSlotLabel.Text = ": register(t0) in shader.";
            Color color = Color.FromArgb(0, 255, 0);
            sucessLabel.ForeColor = color;


            IntPtr hwnd = texturePreview.Handle;
            int wndWidth = texturePreview.Size.Width;
            int wndHeight = texturePreview.Size.Height;
            if (engine == null)
            {
                engine = new sceWrap.CEngineBridge();
            }

            LoadDefaultMaterialData();

            engine.InitMaterialPreview(hwnd, (ushort)wndWidth, (ushort)wndHeight);
            engineThread = new Thread(() => InitPreview());
            engineThread.Start();
        }

        private void LoadDefaultMaterialData()
        {
            ShaderData vshaderData;
            vshaderData.path = "Data\\Shaders\\PBL_ModelVS.hlsl";
            vshaderData.entry = "VSMain";
            defmatData.vertexShader = vshaderData;

            ShaderData pshaderData;
            pshaderData.path = "Data\\Shaders\\DeferredDataCollector.hlsl";
            pshaderData.entry = "PSMain";
            defmatData.pixelShader = pshaderData;

            defmatData.textureData = new TextureData[6];

            defmatData.textureData[0].slot = 0;
            defmatData.textureData[0].path = "Data\\Models\\Misc\\txt_PBRfiller_maybe.dds";
            defmatData.textureData[1].slot = 1;
            defmatData.textureData[1].path = "Data\\Models\\Misc\\txt_PBRfiller_normalMap.dds";
            defmatData.textureData[2].slot = 2;
            defmatData.textureData[2].path = "Data\\Models\\Misc\\txt_PBRfiller_maybe.dds";
            defmatData.textureData[3].slot = 3;
            defmatData.textureData[3].path = "Data\\Models\\Misc\\txt_PBRfiller_no.dds";
            defmatData.textureData[4].slot = 4;
            defmatData.textureData[4].path = "Data\\Models\\Misc\\txt_PBRfiller_no.dds";
            defmatData.textureData[5].slot = 5;
            defmatData.textureData[5].path = "Data\\Models\\Misc\\txt_PBRfiller_yes.dds";


            {
                JsonSerializer serializer = new JsonSerializer();
                Directory.CreateDirectory(Directory.GetCurrentDirectory() + "\\Data\\Materials\\PreviewTempMaterial");
                using (StreamWriter sw = new StreamWriter(Directory.GetCurrentDirectory() + "\\Data\\Materials\\PreviewTempMaterial\\temp_Material.material"))
                using (JsonWriter writer = new JsonTextWriter(sw))
                {
                    serializer.Serialize(writer, defmatData);
                }
            }
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            base.OnFormClosing(e);
            if (engine != null)
            {
                engine.Stop();
                engineThread.Join();
            }
        }

        private void InitPreview()
        {
            lock (texturePreview)
            {
                engine.Start();
            }
        }

        private void browseTex_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "Direct Draw Surface (.dds)|*.dds";
            dialog.InitialDirectory = Directory.GetCurrentDirectory() + "\\Data";
            DialogResult res = dialog.ShowDialog();
            if (res == DialogResult.OK)
            {
                texPath.Text = dialog.FileName;
                if (textureSelectedIndex >= 0)
                {
                    textureList.Items[textureSelectedIndex] = (texSlot.Value.ToString() + " - " + "[" + GetRelativePath(texPath.Text) + "]");
                }
            }
        }



        private string GetRelativePath(string path)
        {
            return path.Substring(path.IndexOf("Bin\\") + 4);
        }

        private void addTexture_Click(object sender, EventArgs e)
        {
            if (File.Exists(texPath.Text))
            {
                int index = textureList.FindString(texSlot.Value.ToString());
                if (index >= 0)
                {
                    textureList.Items[index] = (texSlot.Value.ToString() + " - " + "[" + GetRelativePath(texPath.Text) + "]");
                }
                else
                {
                    textureList.Items.Add(texSlot.Value.ToString() + " - " + "[" + GetRelativePath(texPath.Text) + "]");
                }
            }
            else
            {
                DialogResult res = MessageBox.Show(
                          "Texture-path not found!",
                          "Error",
                          MessageBoxButtons.OK,
                          MessageBoxIcon.Error);
            }
        }

        private void vsBrowse_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "HLSL |*.hlsl";
            dialog.InitialDirectory = Directory.GetCurrentDirectory() + "\\Data\\Shaders";
            DialogResult res = dialog.ShowDialog();
            if (res == DialogResult.OK)
            {
                vertexShader.Text = GetRelativePath(dialog.FileName);
            }
        }

        private void psBrowse_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "HLSL |*.hlsl";
            dialog.InitialDirectory = Directory.GetCurrentDirectory() + "\\Data\\Shaders";
            DialogResult res = dialog.ShowDialog();
            if (res == DialogResult.OK)
            {
                pixelShader.Text = GetRelativePath(dialog.FileName);
            }
        }

        private void texSlot_ValueChanged(object sender, EventArgs e)
        {
            registerSlotLabel.Text = ": register(t" + texSlot.Value.ToString() + ") in shader.";
        }

        private void addShaders_Click(object sender, EventArgs e)
        {
            matData.renderMode = (uint)renderMode.SelectedIndex;
            if (vertexShader.Text.Length > 0)
            {
                if (vsEntry.Text.Length > 0)
                {
                    if (Path.GetExtension(vertexShader.Text) == ".hlsl")
                    {
                        vshaderLabel.Text = "VertexShader: " + Path.GetFileName(vertexShader.Text);
                    }
                    else
                    {
                        DialogResult res = MessageBox.Show(
                               "The chosen vertex-shader is not specified as an HLSL-file, do you want to use it anyways?",
                               "Warning",
                               MessageBoxButtons.YesNo,
                               MessageBoxIcon.Warning);

                        if (res == DialogResult.Yes)
                        {
                            vshaderLabel.Text = "VertexShader: " + Path.GetFileName(vertexShader.Text);
                        }
                    }
                }
                else
                {
                    DialogResult res = MessageBox.Show(
                             "No entry-point specified for vertex-shader!",
                             "Error",
                             MessageBoxButtons.OK,
                             MessageBoxIcon.Error);
                }
            }
            if (pixelShader.Text.Length > 0)
            {
                if (psEntry.Text.Length > 0)
                {
                    if (Path.GetExtension(pixelShader.Text) == ".hlsl")
                    {
                        pshaderLabel.Text = "PixelShader: " + Path.GetFileName(pixelShader.Text);
                    }
                    else
                    {
                        DialogResult res = MessageBox.Show(
                               "The chosen pixel-shader is not specified as an HLSL-file, do you want to use it anyways?",
                               "Warning",
                               MessageBoxButtons.YesNo,
                               MessageBoxIcon.Warning);

                        if (res == DialogResult.Yes)
                        {
                            pshaderLabel.Text = "PixelShader: " + Path.GetFileName(pixelShader.Text);
                        }
                    }
                }
                else
                {
                    DialogResult res = MessageBox.Show(
                          "No entry-point specified for pixel-shader!",
                          "Error",
                          MessageBoxButtons.OK,
                          MessageBoxIcon.Error);
                }
            }
            AutoSave();
        }

        private void AutoSave()
        {
            tempmatData.renderMode = (uint)renderMode.SelectedIndex;
            ShaderData vshaderData;
            vshaderData.path = vertexShader.Text;
            vshaderData.entry = vsEntry.Text;
            tempmatData.vertexShader = vshaderData;

            ShaderData pshaderData;
            pshaderData.path = pixelShader.Text;
            pshaderData.entry = psEntry.Text;
            tempmatData.pixelShader = pshaderData;

            tempmatData.textureData = new TextureData[textureList.Items.Count];
            for (int i = 0; i < textureList.Items.Count; ++i)
            {
                tempmatData.textureData[i].slot = (uint)Int32.Parse(textureList.Items[i].ToString()[0].ToString());
                tempmatData.textureData[i].path = textureList.Items[i].ToString().Substring(textureList.Items[i].ToString().LastIndexOf('[') + 1);
                tempmatData.textureData[i].path = tempmatData.textureData[i].path.Substring(0, tempmatData.textureData[i].path.LastIndexOf(']'));
            }

            {
                JsonSerializer serializer = new JsonSerializer();
                using (StreamWriter sw = new StreamWriter(Directory.GetCurrentDirectory() + "\\Data\\Materials\\PreviewTempMaterial\\temp_Material.material"))
                using (JsonWriter writer = new JsonTextWriter(sw))
                {
                    serializer.Serialize(writer, tempmatData);
                }
            }
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            string matName = "";
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.Filter = "Material |*.material";
            dialog.InitialDirectory = Directory.GetCurrentDirectory() + "\\Data\\Materials";
            DialogResult res = dialog.ShowDialog();
            if (res == DialogResult.OK)
            {
                matName = dialog.FileName;
            }
            else
            {
                sucessLabel.Text = "";
                return;
            }


            matData.renderMode = (uint)renderMode.SelectedIndex;

            ShaderData vshaderData;
            vshaderData.path = vertexShader.Text;
            vshaderData.entry = vsEntry.Text;
            matData.vertexShader = vshaderData;

            ShaderData pshaderData;
            pshaderData.path = pixelShader.Text;
            pshaderData.entry = psEntry.Text;
            matData.pixelShader = pshaderData;

            matData.textureData = new TextureData[textureList.Items.Count];
            for (int i = 0; i < textureList.Items.Count; ++i)
            {
                matData.textureData[i].slot = (uint)Int32.Parse(textureList.Items[i].ToString()[0].ToString());
                matData.textureData[i].path = textureList.Items[i].ToString().Substring(textureList.Items[i].ToString().LastIndexOf('[') + 1);
                matData.textureData[i].path = matData.textureData[i].path.Substring(0, matData.textureData[i].path.LastIndexOf(']'));
            }

            {
                JsonSerializer serializer = new JsonSerializer();
                using (StreamWriter sw = new StreamWriter(matName))
                using (JsonWriter writer = new JsonTextWriter(sw))
                {
                    serializer.Serialize(writer, matData);
                    writer.Flush();
                }
            }

            sucessLabel.Text = "Saved!";
        }

        private void commonSurfaceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            renderMode.SelectedIndex = 0;
            vertexShader.Text = "Data\\Shaders\\PBL_ModelVS.hlsl";
            vsEntry.Text = "VSMain";
            vshaderLabel.Text = "VertexShader: " + Path.GetFileName(vertexShader.Text);
            pixelShader.Text = "Data\\Shaders\\DeferredDataCollector.hlsl";
            psEntry.Text = "PSMain";
            pshaderLabel.Text = "PixelShader: " + Path.GetFileName(pixelShader.Text);

            textureList.Items.Clear();
            textureList.Items.Add("0 - [ALBEDO]");
            textureList.Items.Add("1 - [NORMALMAP]");
            textureList.Items.Add("2 - [ROUGHNESS]");
            textureList.Items.Add("3 - [METALLIC]");
            textureList.Items.Add("4 - [EMISSIVE]");
            textureList.Items.Add("5 - [AMBIENT OCCLUSION]");
        }

        private void textureList_SelectedIndexChanged(object sender, EventArgs e)
        {
            texSlot.Value = textureList.SelectedIndex >= 0 ? textureList.SelectedIndex : 0;
            textureSelectedIndex = (int)texSlot.Value;
        }


        private int mouseXLastFrame = 0;
        private int mouseYLastFrame = 0;
        private bool isKeyPressed = false;
        private const float mdlRotationSpeed = 2.0f;
        private void texturePreview_MouseDown(object sender, MouseEventArgs e)
        {
            isKeyPressed = true;
            mouseXLastFrame = e.X;
            mouseYLastFrame = e.Y;
        }

        private void texturePreview_MouseMove(object sender, MouseEventArgs e)
        {
            if (isKeyPressed == false)
            {
                return;
            }
            float deltaX = e.X - mouseXLastFrame;
            float deltaY = e.Y - mouseYLastFrame;

            deltaX = (deltaX / Size.Width) * mdlRotationSpeed;
            deltaY = (deltaY / Size.Height) * mdlRotationSpeed;

            if (lockX.Checked == false && (Math.Abs(deltaX) > 0))
            {
                engine.RotateX(-deltaX);
            }
            if (lockY.Checked == false && (Math.Abs(deltaY) > 0))
            {
                engine.RotateY(-deltaY);
            }
            mouseXLastFrame = e.X;
            mouseYLastFrame = e.Y;
        }

        private void texturePreview_MouseUp(object sender, MouseEventArgs e)
        {
            isKeyPressed = false;
        }

        private void resetRotation_Click(object sender, EventArgs e)
        {
            engine.ResetRotation();
        }

        private void model_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (engine != null)
            {
                if (model.SelectedIndex == model.Items.Count - 1)
                {
                    groupBox3.Enabled = true;
                    if (customModelPath.Text.Length > 0)
                    {
                        engine.SetModel(customModelPath.Text);
                        engine.ResetRotation();
                    }
                }
                else
                {
                    groupBox3.Enabled = false;
                    engine.SetModel(model.SelectedIndex);
                    engine.ResetRotation();
                }
            }
        }

        private void compile_Click(object sender, EventArgs e)
        {
            AutoSave();
        }

        private void DelPressed()
        {
            if (textureList.SelectedItem != null)
            {
                textureList.Items.Remove(textureList.SelectedItem);
            }
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.Delete)
            {
                DelPressed();
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void transparentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            renderMode.SelectedIndex = 1;
            vertexShader.Text = "Data\\Shaders\\PBL_ModelVS.hlsl";
            vsEntry.Text = "VSMain";
            vshaderLabel.Text = "VertexShader: " + Path.GetFileName(vertexShader.Text);
            pixelShader.Text = "Data\\Shaders\\PBL_ModelPS.hlsl";
            psEntry.Text = "PSMain";
            pshaderLabel.Text = "PixelShader: " + Path.GetFileName(pixelShader.Text);

            textureList.Items.Clear();
            textureList.Items.Add("0 - [ALBEDO]");
        }

        private void openButton_Click(object sender, EventArgs e)
        {
            string materialFile = null;
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "Material |*.material";
            dialog.InitialDirectory = Directory.GetCurrentDirectory() + "\\Data\\Materials";
            DialogResult res = dialog.ShowDialog();
            if (res != DialogResult.OK)
            {
                return;
            }
            
            materialFile = dialog.FileName;
            {
                MaterialData data;
                using (StreamReader file = File.OpenText(materialFile))
                {
                    JsonSerializer serializer = new JsonSerializer();
                    data = (MaterialData)serializer.Deserialize(file, typeof(MaterialData));
                }
                
                renderMode.SelectedIndex = (int)data.renderMode;
                vertexShader.Text = data.vertexShader.path;
                vsEntry.Text = data.vertexShader.entry;
                vshaderLabel.Text = "VertexShader: " + Path.GetFileName(vertexShader.Text);
                pixelShader.Text = data.pixelShader.path;
                psEntry.Text = data.pixelShader.entry;
                pshaderLabel.Text = "PixelShader: " + Path.GetFileName(pixelShader.Text);

                textureList.Items.Clear();
                foreach (TextureData tex in data.textureData)
                {
                    string info = (tex.slot.ToString() + " - " + "[" + tex.path + "]");
                    textureList.Items.Add(info);
                }
            }
        }

        private void customModelBrowse_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "FBX |*.fbx";
            dialog.InitialDirectory = Directory.GetCurrentDirectory() + "\\Data\\Models";
            DialogResult res = dialog.ShowDialog();
            if (res == DialogResult.OK)
            {
                if (Path.GetExtension(dialog.FileName) != ".fbx")
                {
                    DialogResult result = MessageBox.Show(
                              "Invalid fbx-file chosen!",
                              "Error",
                              MessageBoxButtons.OK,
                              MessageBoxIcon.Error);
                    return;
                }
                customModelPath.Text = GetRelativePath(dialog.FileName);
                engine.SetModel(customModelPath.Text);
                engine.ResetRotation();
            }
        }

        private void texturePreview_MouseEnter(object sender, EventArgs e)
        {
            texturePreview.Focus();
        }

        private void texturePreview_MouseWheel(object sender, MouseEventArgs e)
        {
            float delta = 1.0f;
            if (e.Delta > 0)
            {
                engine.Zoom(delta);
            }
            else
            {
                engine.Zoom(-delta);
            }
        }

        private void particleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            renderMode.SelectedIndex = 2;
            vertexShader.Text = "Data\\Shaders\\ParticleVS.hlsl";
            vsEntry.Text = "VSMain";
            vshaderLabel.Text = "VertexShader: " + Path.GetFileName(vertexShader.Text);
            pixelShader.Text = "Data\\Shaders\\ParticlePS.hlsl";
            psEntry.Text = "PSMain";
            pshaderLabel.Text = "PixelShader: " + Path.GetFileName(pixelShader.Text);

            textureList.Items.Clear();
            textureList.Items.Add("0 - [TEXTURE]");
        }
    }
}
