﻿namespace MaterialEditor
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.registerSlotLabel = new System.Windows.Forms.Label();
            this.textureList = new System.Windows.Forms.ListBox();
            this.addTexture = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.texSlot = new System.Windows.Forms.NumericUpDown();
            this.browseTex = new System.Windows.Forms.Button();
            this.texPath = new System.Windows.Forms.TextBox();
            this.renderMode = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.psEntry = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.vsEntry = new System.Windows.Forms.TextBox();
            this.pshaderLabel = new System.Windows.Forms.Label();
            this.vshaderLabel = new System.Windows.Forms.Label();
            this.addShaders = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.psBrowse = new System.Windows.Forms.Button();
            this.pixelShader = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.vsBrowse = new System.Windows.Forms.Button();
            this.vertexShader = new System.Windows.Forms.TextBox();
            this.saveButton = new System.Windows.Forms.Button();
            this.openButton = new System.Windows.Forms.Button();
            this.sucessLabel = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.presetsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.commonSurfaceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.transparentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.texturePreview = new System.Windows.Forms.PictureBox();
            this.lockY = new System.Windows.Forms.CheckBox();
            this.lockX = new System.Windows.Forms.CheckBox();
            this.resetRotation = new System.Windows.Forms.Button();
            this.model = new System.Windows.Forms.ComboBox();
            this.compile = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.customModelBrowse = new System.Windows.Forms.Button();
            this.customModelPath = new System.Windows.Forms.TextBox();
            this.particleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.texSlot)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.texturePreview)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.registerSlotLabel);
            this.groupBox1.Controls.Add(this.textureList);
            this.groupBox1.Controls.Add(this.addTexture);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.texSlot);
            this.groupBox1.Controls.Add(this.browseTex);
            this.groupBox1.Controls.Add(this.texPath);
            this.groupBox1.Location = new System.Drawing.Point(12, 27);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(255, 281);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Textures";
            // 
            // registerSlotLabel
            // 
            this.registerSlotLabel.AutoSize = true;
            this.registerSlotLabel.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.registerSlotLabel.Location = new System.Drawing.Point(46, 78);
            this.registerSlotLabel.Name = "registerSlotLabel";
            this.registerSlotLabel.Size = new System.Drawing.Size(182, 15);
            this.registerSlotLabel.TabIndex = 7;
            this.registerSlotLabel.Text = ": register(t0) in shader.";
            // 
            // textureList
            // 
            this.textureList.FormattingEnabled = true;
            this.textureList.Location = new System.Drawing.Point(7, 141);
            this.textureList.Name = "textureList";
            this.textureList.Size = new System.Drawing.Size(243, 134);
            this.textureList.TabIndex = 6;
            this.textureList.SelectedIndexChanged += new System.EventHandler(this.textureList_SelectedIndexChanged);
            // 
            // addTexture
            // 
            this.addTexture.Location = new System.Drawing.Point(6, 101);
            this.addTexture.Name = "addTexture";
            this.addTexture.Size = new System.Drawing.Size(75, 23);
            this.addTexture.TabIndex = 5;
            this.addTexture.Text = "Add Texture";
            this.addTexture.UseVisualStyleBackColor = true;
            this.addTexture.Click += new System.EventHandler(this.addTexture_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 17);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Path:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 59);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(28, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Slot:";
            // 
            // texSlot
            // 
            this.texSlot.Location = new System.Drawing.Point(6, 75);
            this.texSlot.Maximum = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.texSlot.Name = "texSlot";
            this.texSlot.Size = new System.Drawing.Size(31, 20);
            this.texSlot.TabIndex = 2;
            this.texSlot.ValueChanged += new System.EventHandler(this.texSlot_ValueChanged);
            // 
            // browseTex
            // 
            this.browseTex.Location = new System.Drawing.Point(226, 33);
            this.browseTex.Name = "browseTex";
            this.browseTex.Size = new System.Drawing.Size(24, 19);
            this.browseTex.TabIndex = 1;
            this.browseTex.Text = "...";
            this.browseTex.UseVisualStyleBackColor = true;
            this.browseTex.Click += new System.EventHandler(this.browseTex_Click);
            // 
            // texPath
            // 
            this.texPath.Location = new System.Drawing.Point(6, 33);
            this.texPath.Name = "texPath";
            this.texPath.Size = new System.Drawing.Size(214, 20);
            this.texPath.TabIndex = 0;
            // 
            // renderMode
            // 
            this.renderMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.renderMode.FormattingEnabled = true;
            this.renderMode.Items.AddRange(new object[] {
            "Deferred",
            "Forward",
            "Particle"});
            this.renderMode.Location = new System.Drawing.Point(6, 33);
            this.renderMode.Name = "renderMode";
            this.renderMode.Size = new System.Drawing.Size(121, 21);
            this.renderMode.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 17);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(75, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Render Mode:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.psEntry);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.vsEntry);
            this.groupBox2.Controls.Add(this.pshaderLabel);
            this.groupBox2.Controls.Add(this.vshaderLabel);
            this.groupBox2.Controls.Add(this.addShaders);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.psBrowse);
            this.groupBox2.Controls.Add(this.pixelShader);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.vsBrowse);
            this.groupBox2.Controls.Add(this.vertexShader);
            this.groupBox2.Controls.Add(this.renderMode);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Location = new System.Drawing.Point(997, 27);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(255, 281);
            this.groupBox2.TabIndex = 9;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Shaders";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(3, 174);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(133, 13);
            this.label9.TabIndex = 21;
            this.label9.Text = "PixelShader entry-function:";
            // 
            // psEntry
            // 
            this.psEntry.Location = new System.Drawing.Point(6, 190);
            this.psEntry.Name = "psEntry";
            this.psEntry.Size = new System.Drawing.Size(138, 20);
            this.psEntry.TabIndex = 20;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(3, 96);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(141, 13);
            this.label8.TabIndex = 19;
            this.label8.Text = "VertexShader entry-function:";
            // 
            // vsEntry
            // 
            this.vsEntry.Location = new System.Drawing.Point(6, 112);
            this.vsEntry.Name = "vsEntry";
            this.vsEntry.Size = new System.Drawing.Size(138, 20);
            this.vsEntry.TabIndex = 18;
            // 
            // pshaderLabel
            // 
            this.pshaderLabel.AutoSize = true;
            this.pshaderLabel.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pshaderLabel.Location = new System.Drawing.Point(4, 234);
            this.pshaderLabel.Name = "pshaderLabel";
            this.pshaderLabel.Size = new System.Drawing.Size(98, 15);
            this.pshaderLabel.TabIndex = 17;
            this.pshaderLabel.Text = "PixelShader: ";
            // 
            // vshaderLabel
            // 
            this.vshaderLabel.AutoSize = true;
            this.vshaderLabel.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.vshaderLabel.Location = new System.Drawing.Point(4, 217);
            this.vshaderLabel.Name = "vshaderLabel";
            this.vshaderLabel.Size = new System.Drawing.Size(105, 15);
            this.vshaderLabel.TabIndex = 16;
            this.vshaderLabel.Text = "VertexShader: ";
            // 
            // addShaders
            // 
            this.addShaders.Location = new System.Drawing.Point(6, 252);
            this.addShaders.Name = "addShaders";
            this.addShaders.Size = new System.Drawing.Size(86, 23);
            this.addShaders.TabIndex = 15;
            this.addShaders.Text = "Add Shaders";
            this.addShaders.UseVisualStyleBackColor = true;
            this.addShaders.Click += new System.EventHandler(this.addShaders_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(2, 135);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(90, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "PixelShader path:";
            // 
            // psBrowse
            // 
            this.psBrowse.Location = new System.Drawing.Point(224, 151);
            this.psBrowse.Name = "psBrowse";
            this.psBrowse.Size = new System.Drawing.Size(24, 19);
            this.psBrowse.TabIndex = 13;
            this.psBrowse.Text = "...";
            this.psBrowse.UseVisualStyleBackColor = true;
            this.psBrowse.Click += new System.EventHandler(this.psBrowse_Click);
            // 
            // pixelShader
            // 
            this.pixelShader.Location = new System.Drawing.Point(6, 151);
            this.pixelShader.Name = "pixelShader";
            this.pixelShader.Size = new System.Drawing.Size(214, 20);
            this.pixelShader.TabIndex = 12;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 57);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(98, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "VertexShader path:";
            // 
            // vsBrowse
            // 
            this.vsBrowse.Location = new System.Drawing.Point(225, 73);
            this.vsBrowse.Name = "vsBrowse";
            this.vsBrowse.Size = new System.Drawing.Size(24, 19);
            this.vsBrowse.TabIndex = 10;
            this.vsBrowse.Text = "...";
            this.vsBrowse.UseVisualStyleBackColor = true;
            this.vsBrowse.Click += new System.EventHandler(this.vsBrowse_Click);
            // 
            // vertexShader
            // 
            this.vertexShader.Location = new System.Drawing.Point(6, 73);
            this.vertexShader.Name = "vertexShader";
            this.vertexShader.Size = new System.Drawing.Size(214, 20);
            this.vertexShader.TabIndex = 9;
            // 
            // saveButton
            // 
            this.saveButton.Location = new System.Drawing.Point(15, 618);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(81, 51);
            this.saveButton.TabIndex = 19;
            this.saveButton.Text = "Save";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // openButton
            // 
            this.openButton.Location = new System.Drawing.Point(102, 618);
            this.openButton.Name = "openButton";
            this.openButton.Size = new System.Drawing.Size(81, 51);
            this.openButton.TabIndex = 20;
            this.openButton.Text = "Open";
            this.openButton.UseVisualStyleBackColor = true;
            this.openButton.Click += new System.EventHandler(this.openButton_Click);
            // 
            // sucessLabel
            // 
            this.sucessLabel.AutoSize = true;
            this.sucessLabel.Location = new System.Drawing.Point(186, 429);
            this.sucessLabel.Name = "sucessLabel";
            this.sucessLabel.Size = new System.Drawing.Size(0, 13);
            this.sucessLabel.TabIndex = 21;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.presetsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1264, 24);
            this.menuStrip1.TabIndex = 22;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // presetsToolStripMenuItem
            // 
            this.presetsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.commonSurfaceToolStripMenuItem,
            this.transparentToolStripMenuItem,
            this.particleToolStripMenuItem});
            this.presetsToolStripMenuItem.Name = "presetsToolStripMenuItem";
            this.presetsToolStripMenuItem.Size = new System.Drawing.Size(56, 20);
            this.presetsToolStripMenuItem.Text = "Presets";
            // 
            // commonSurfaceToolStripMenuItem
            // 
            this.commonSurfaceToolStripMenuItem.Name = "commonSurfaceToolStripMenuItem";
            this.commonSurfaceToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.commonSurfaceToolStripMenuItem.Text = "CommonSurface";
            this.commonSurfaceToolStripMenuItem.Click += new System.EventHandler(this.commonSurfaceToolStripMenuItem_Click);
            // 
            // transparentToolStripMenuItem
            // 
            this.transparentToolStripMenuItem.Name = "transparentToolStripMenuItem";
            this.transparentToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.transparentToolStripMenuItem.Text = "Transparent";
            this.transparentToolStripMenuItem.Click += new System.EventHandler(this.transparentToolStripMenuItem_Click);
            // 
            // texturePreview
            // 
            this.texturePreview.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.texturePreview.Location = new System.Drawing.Point(273, 27);
            this.texturePreview.Name = "texturePreview";
            this.texturePreview.Size = new System.Drawing.Size(718, 642);
            this.texturePreview.TabIndex = 23;
            this.texturePreview.TabStop = false;
            this.texturePreview.MouseDown += new System.Windows.Forms.MouseEventHandler(this.texturePreview_MouseDown);
            this.texturePreview.MouseEnter += new System.EventHandler(this.texturePreview_MouseEnter);
            this.texturePreview.MouseMove += new System.Windows.Forms.MouseEventHandler(this.texturePreview_MouseMove);
            this.texturePreview.MouseUp += new System.Windows.Forms.MouseEventHandler(this.texturePreview_MouseUp);
            // 
            // lockY
            // 
            this.lockY.AutoSize = true;
            this.lockY.BackColor = System.Drawing.Color.Transparent;
            this.lockY.Checked = true;
            this.lockY.CheckState = System.Windows.Forms.CheckState.Checked;
            this.lockY.Location = new System.Drawing.Point(997, 538);
            this.lockY.Name = "lockY";
            this.lockY.Size = new System.Drawing.Size(81, 17);
            this.lockY.TabIndex = 25;
            this.lockY.Text = "Lock Y-axis";
            this.lockY.UseVisualStyleBackColor = false;
            // 
            // lockX
            // 
            this.lockX.AutoSize = true;
            this.lockX.BackColor = System.Drawing.Color.Transparent;
            this.lockX.Location = new System.Drawing.Point(997, 521);
            this.lockX.Name = "lockX";
            this.lockX.Size = new System.Drawing.Size(81, 17);
            this.lockX.TabIndex = 26;
            this.lockX.Text = "Lock X-axis";
            this.lockX.UseVisualStyleBackColor = false;
            // 
            // resetRotation
            // 
            this.resetRotation.Location = new System.Drawing.Point(997, 556);
            this.resetRotation.Name = "resetRotation";
            this.resetRotation.Size = new System.Drawing.Size(95, 28);
            this.resetRotation.TabIndex = 27;
            this.resetRotation.Text = "Reset Rotation";
            this.resetRotation.UseVisualStyleBackColor = true;
            this.resetRotation.Click += new System.EventHandler(this.resetRotation_Click);
            // 
            // model
            // 
            this.model.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.model.FormattingEnabled = true;
            this.model.Items.AddRange(new object[] {
            "Box",
            "Sphere",
            "Cylinder",
            "Custom..."});
            this.model.Location = new System.Drawing.Point(997, 586);
            this.model.Name = "model";
            this.model.Size = new System.Drawing.Size(95, 21);
            this.model.TabIndex = 28;
            this.model.SelectedIndexChanged += new System.EventHandler(this.model_SelectedIndexChanged);
            // 
            // compile
            // 
            this.compile.Location = new System.Drawing.Point(186, 618);
            this.compile.Name = "compile";
            this.compile.Size = new System.Drawing.Size(81, 51);
            this.compile.TabIndex = 29;
            this.compile.Text = "Compile";
            this.compile.UseVisualStyleBackColor = true;
            this.compile.Click += new System.EventHandler(this.compile_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.customModelBrowse);
            this.groupBox3.Controls.Add(this.customModelPath);
            this.groupBox3.Location = new System.Drawing.Point(997, 616);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(255, 53);
            this.groupBox3.TabIndex = 30;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "CustomModel";
            // 
            // customModelBrowse
            // 
            this.customModelBrowse.Location = new System.Drawing.Point(224, 20);
            this.customModelBrowse.Name = "customModelBrowse";
            this.customModelBrowse.Size = new System.Drawing.Size(24, 19);
            this.customModelBrowse.TabIndex = 22;
            this.customModelBrowse.Text = "...";
            this.customModelBrowse.UseVisualStyleBackColor = true;
            this.customModelBrowse.Click += new System.EventHandler(this.customModelBrowse_Click);
            // 
            // customModelPath
            // 
            this.customModelPath.Location = new System.Drawing.Point(9, 19);
            this.customModelPath.Name = "customModelPath";
            this.customModelPath.Size = new System.Drawing.Size(211, 20);
            this.customModelPath.TabIndex = 0;
            // 
            // particleToolStripMenuItem
            // 
            this.particleToolStripMenuItem.Name = "particleToolStripMenuItem";
            this.particleToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.particleToolStripMenuItem.Text = "Particle";
            this.particleToolStripMenuItem.Click += new System.EventHandler(this.particleToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1264, 681);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.compile);
            this.Controls.Add(this.model);
            this.Controls.Add(this.resetRotation);
            this.Controls.Add(this.lockX);
            this.Controls.Add(this.lockY);
            this.Controls.Add(this.texturePreview);
            this.Controls.Add(this.sucessLabel);
            this.Controls.Add(this.openButton);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(4096, 2160);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(1024, 512);
            this.Name = "Form1";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SCE Material Editor";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.texSlot)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.texturePreview)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button browseTex;
        private System.Windows.Forms.TextBox texPath;
        private System.Windows.Forms.ListBox textureList;
        private System.Windows.Forms.Button addTexture;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown texSlot;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox renderMode;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button psBrowse;
        private System.Windows.Forms.TextBox pixelShader;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button vsBrowse;
        private System.Windows.Forms.TextBox vertexShader;
        private System.Windows.Forms.Label registerSlotLabel;
        private System.Windows.Forms.Label pshaderLabel;
        private System.Windows.Forms.Label vshaderLabel;
        private System.Windows.Forms.Button addShaders;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.Button openButton;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox vsEntry;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox psEntry;
        private System.Windows.Forms.Label sucessLabel;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem presetsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem commonSurfaceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem transparentToolStripMenuItem;
        private System.Windows.Forms.PictureBox texturePreview;
        private System.Windows.Forms.CheckBox lockY;
        private System.Windows.Forms.CheckBox lockX;
        private System.Windows.Forms.Button resetRotation;
        private System.Windows.Forms.ComboBox model;
        private System.Windows.Forms.Button compile;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button customModelBrowse;
        private System.Windows.Forms.TextBox customModelPath;
        private System.Windows.Forms.ToolStripMenuItem particleToolStripMenuItem;
    }
}

