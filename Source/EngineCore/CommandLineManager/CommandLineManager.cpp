#include "stdafx.h"
#include "CommandLineManager.h"
#include <unordered_map>

using namespace sce;

static std::unordered_map<std::wstring, CU::GrowingArray<std::wstring>> myCommands;

void sce::CCommandLineManager::Init()
{
	wchar_t** argv(__wargv);
	const int argc(__argc);

	std::wstring lastCommandEntered;

	for (int i = 1; i < argc; ++i)
	{
		std::wstring arg(argv[i]);
		if (!arg.empty())
		{
			if (arg.front() != '-')
			{
				myCommands[lastCommandEntered].Add(arg);
			}
			else
			{
				if (myCommands.find(arg) == myCommands.end())
				{
					myCommands[arg];
					lastCommandEntered = arg;
				}
			}
		}
	}
}

bool sce::CCommandLineManager::HasParameter(const wchar_t* aParam)
{
	if (myCommands.find(aParam) != myCommands.end())
	{
		return true;
	}
	return false;
}

bool sce::CCommandLineManager::HasArgument(const wchar_t* aParam, const wchar_t* aArg)
{
	if (myCommands.find(aParam) != myCommands.end())
	{
		for (decltype(myCommands.begin()->second)::size_type index(0); index < myCommands[aParam].size(); ++index)
		{
			if (myCommands[aParam][index] == aArg)
			{
				return true;
			}
		}
	}
	return false;
}

const CU::GrowingArray<std::wstring>& sce::CCommandLineManager::GetArguments(const wchar_t* aParam)
{
	return myCommands.at(aParam);
}
