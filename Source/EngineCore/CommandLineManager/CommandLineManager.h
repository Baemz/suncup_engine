#pragma once
#include "..\CommonUtilities\GrowingArray.h"

namespace sce {
	class CCommandLineManager
	{
	public:
		static void Init();
		static bool HasParameter(const wchar_t* aParam);
		static bool HasArgument(const wchar_t* aParam, const wchar_t* aArg);
		static const CU::GrowingArray<std::wstring>& GetArguments(const wchar_t* aParam);

	};
}
