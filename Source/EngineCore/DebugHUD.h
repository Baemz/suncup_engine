#pragma once
#ifndef _RETAIL
#include "..\CommonUtilities\GrowingArray.h"
#include "..\CommonUtilities\Timer.h"
#endif // !_RETAIL
#include "DebugHUDGraph.h"

namespace sce
{
	namespace gfx
	{
		class CText;
	}

	class CDebugHUD
	{
	public:
		enum class eMSTexts : unsigned short
		{
			Info,
			Total,
			Max,
			Update,
			Render,
			Particle,
			SIZE,
		};

#ifdef _RETAIL
		CDebugHUD() = default;
		~CDebugHUD() = default;

		bool Init(double, double, double, double) { return true; };

		void Update(const float) {};
		void Render() {};

		static bool SetMS(const eMSTexts&) { return true; };

#else
		CDebugHUD();
		~CDebugHUD();

		bool Init(double aDelayBetweenFPSUpdate, double aFPSToShowYellow, double aFPSToShowRed, double aFPSToShowNotResponding);

		void Update(const float aDeltaTime);
		void Render();

		static bool SetMS(const eMSTexts& aIndexToSet);

	private:
		static CDebugHUD* ourInstance;

		struct SDebugText
		{
			SDebugText()
				: myText(nullptr)
				, myMilliseconds(0)
				, myTimeWhenUpdatedLastTime(0.0)
				, myFPSLastUpdate(0)
			{};

			SDebugText(gfx::CText* aText)
				: myText(aText)
				, myMilliseconds(0)
				, myTimeWhenUpdatedLastTime(0.0)
				, myFPSLastUpdate(0)
			{};

			gfx::CText* myText;
			std::string myTextText;
			double myMilliseconds;
			double myTimeWhenUpdatedLastTime;
			unsigned int myFPSLastUpdate;
		};
		
		void UpdateMemoryUsage(const float aDeltaTime);
		void UpdateDebugTexts();

		gfx::CText* myMemoryUsageText;
		std::string myMemoryUsageTextText;

		gfx::CText* myDebugTexts;
		CU::GrowingArray<SDebugText> myMSTexts;

		CU::Timer myTimer;
		double myDelayBetweenFPSUpdate;
		double myLastTimeFPSUpdate;
		double myTimeToShowYellow;
		double myTimeToShowRed;
		double myTimeToShowNotResponding;
		static constexpr float myDelayBetweenMemoryUpdate = 1.0f;
		float myLastTimeMemoryUpdate;

		CDebugHUDGraph myDebugGraph;
#endif // _RETAIL

	};
}
