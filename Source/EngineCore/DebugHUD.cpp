﻿#include "stdafx.h"
#include "DebugHUD.h"
#ifndef _RETAIL
#include "..\GraphicsEngine\Text\Text.h"
#include <mutex>
#include "..\CommonUtilities\Macros.h"

#define INDEX_TEXTS_START_AT 3

namespace sce
{
	CDebugHUD* CDebugHUD::ourInstance(nullptr);

	namespace DebugHUD🔒
	{
		std::mutex ourDebugHUDMutex;
	}

	CDebugHUD::CDebugHUD()
		: myMemoryUsageText(nullptr)
		, myDebugTexts(nullptr)
		, myDelayBetweenFPSUpdate(0.0)
		, myLastTimeFPSUpdate(0.0)
		, myTimeToShowYellow(0.0)
		, myTimeToShowRed(0.0)
		, myTimeToShowNotResponding(0.0)
		, myLastTimeMemoryUpdate(myDelayBetweenMemoryUpdate)
	{
		static_assert(static_cast<unsigned short>(eMSTexts::SIZE) >= INDEX_TEXTS_START_AT, "Not enough values in sce::CDebugHUD::eMSTexts");
	}

	CDebugHUD::~CDebugHUD()
	{
		if (ourInstance == this)
		{
			ourInstance = nullptr;
		}

		sce_delete(myMemoryUsageText);
		sce_delete(myDebugTexts);
	}

	bool CDebugHUD::Init(double aDelayBetweenFPSUpdate, double aFPSToShowYellow, double aFPSToShowRed, double aFPSToShowNotResponding)
	{
		std::unique_lock<std::mutex> lg(DebugHUD🔒::ourDebugHUDMutex);

		myDelayBetweenFPSUpdate = aDelayBetweenFPSUpdate;
		myTimeToShowYellow = (1.0 / aFPSToShowYellow) * 1000.0;
		myTimeToShowRed = (1.0 / aFPSToShowRed) * 1000.0;
		myTimeToShowNotResponding = (1.0 / aFPSToShowNotResponding) * 1000.0;

		const std::string debugFont("Comic Sans MS");
		constexpr float debugSize(18.0f);
		constexpr float debugPositionOffsetY(0.028f);

		myMemoryUsageTextText = "Memory Used: ";
		myMemoryUsageText = sce_new(gfx::CText);
		myMemoryUsageText->Init(debugFont, "", debugSize, { 0.0f }, { 0.0f, (debugPositionOffsetY * 0) });

		myDebugTexts = sce_newArray(gfx::CText, static_cast<unsigned short>(eMSTexts::SIZE));
		myMSTexts.Init(static_cast<unsigned short>(eMSTexts::SIZE));
		
		for (unsigned short textIndex = 0; textIndex < myMSTexts.Capacity(); ++textIndex)
		{
			myMSTexts.Add(SDebugText(&myDebugTexts[textIndex]));
		}

		myMSTexts[static_cast<unsigned short>(eMSTexts::Info)].myTextText = "FPS | Time spent in ms";
		myMSTexts[static_cast<unsigned short>(eMSTexts::Total)].myTextText = "Total: ";
		myMSTexts[static_cast<unsigned short>(eMSTexts::Max)].myTextText = "Min/Max: ";
		myMSTexts[static_cast<unsigned short>(eMSTexts::Update)].myTextText = "Update: ";
		myMSTexts[static_cast<unsigned short>(eMSTexts::Render)].myTextText = "Render: ";
		myMSTexts[static_cast<unsigned short>(eMSTexts::Particle)].myTextText = "Particle: ";

		for (unsigned short textIndex = 0; textIndex < myMSTexts.Size(); ++textIndex)
		{
			myMSTexts[textIndex].myText->Init(debugFont, myMSTexts[textIndex].myTextText, debugSize, { 0.0f }, { 0.0f, (debugPositionOffsetY * (1 + textIndex)) });
		}

		myDebugGraph.Init({ 0.7f, 0.0f }, { 1.0f, 0.1f }, 10);

		ourInstance = this;

		lg.unlock();

		return true;
	}

	void CDebugHUD::Update(const float aDeltaTime)
	{
		std::unique_lock<std::mutex> lg(DebugHUD🔒::ourDebugHUDMutex);
		myTimer.Update();

		UpdateMemoryUsage(aDeltaTime);

		UpdateDebugTexts();

		//myDebugGraph.Update(aDeltaTime);

		lg.unlock();
	}

	void CDebugHUD::Render()
	{
		std::unique_lock<std::mutex> lg(DebugHUD🔒::ourDebugHUDMutex);
		myMemoryUsageText->Render();

		for (unsigned short textIndex = 0; textIndex < myMSTexts.Size(); ++textIndex)
		{
			myMSTexts[textIndex].myText->Render();
		}

		//myDebugGraph.Render();

		lg.unlock();
	}

	bool CDebugHUD::SetMS(const eMSTexts& aIndexToSet)
	{
		const unsigned short indexNotAsEnum(static_cast<unsigned short>(aIndexToSet));
		if ((indexNotAsEnum < INDEX_TEXTS_START_AT)
			|| (indexNotAsEnum >= static_cast<unsigned short>(eMSTexts::SIZE)))
		{
			ENGINE_LOG("ERROR! Tried to update debug HUD with invalid eMSTexts ID %hu", indexNotAsEnum);
			return false;
		}

		if (ourInstance != nullptr)
		{
			CU::Timer& timer(ourInstance->myTimer);
			SDebugText& currentDebugText(ourInstance->myMSTexts[indexNotAsEnum]);

			std::unique_lock<std::mutex> lg(DebugHUD🔒::ourDebugHUDMutex);
			timer.Update();
			const double timeWhenUpdatedLastTime(currentDebugText.myTimeWhenUpdatedLastTime);
			currentDebugText.myTimeWhenUpdatedLastTime = timer.GetTotalTime();
			currentDebugText.myMilliseconds = 1000.0 * (currentDebugText.myTimeWhenUpdatedLastTime - timeWhenUpdatedLastTime);
			lg.unlock();
		}

		return true;
	}

	void CDebugHUD::UpdateMemoryUsage(const float aDeltaTime)
	{
		myLastTimeMemoryUpdate += aDeltaTime;

		if (myDelayBetweenMemoryUpdate < myLastTimeMemoryUpdate)
		{
			myLastTimeMemoryUpdate = 0.0f;

			const unsigned long long memoryUsed(CMemoryPool::GetUsedMemoryInBytes());
			const unsigned long long memoryMax(CMemoryPool::GetMaxMemoryInBytes());

			*myMemoryUsageText = myMemoryUsageTextText
				+ std::to_string(memoryUsed) + " B / " + std::to_string(memoryMax) + " B | "
				+ std::to_string(memoryUsed / (1024 * 1024)) + " MB / " + std::to_string(memoryMax / (1024 * 1024)) + " MB";
		}
	}

	void CDebugHUD::UpdateDebugTexts()
	{
		double millisecondsSpent(0.0);
		double millisecondsMax(0.0);

		bool updateFPS(false);
		if ((myLastTimeFPSUpdate + myDelayBetweenFPSUpdate) < myTimer.GetTotalTime())
		{
			updateFPS = true;
			myLastTimeFPSUpdate = myTimer.GetTotalTime();
		}

		for (unsigned short textIndex = INDEX_TEXTS_START_AT; textIndex < myMSTexts.Size(); ++textIndex)
		{
			SDebugText& currentDebugText(myMSTexts[textIndex]);

			const double timeSpent(currentDebugText.myMilliseconds);
			std::string extraText;
			if (timeSpent < myTimeToShowYellow)
			{
				currentDebugText.myText->SetColor(gfx::CText::ColorFloatToUint(1.0f, 1.0f, 1.0f));
			}
			else
			{
				const float amountToTimeout(static_cast<float>((timeSpent - myTimeToShowYellow) / (myTimeToShowRed - myTimeToShowYellow)));
				currentDebugText.myText->SetColor(gfx::CText::ColorFloatToUint(1.0f, (1.0f - amountToTimeout), 0.0f));
			}

			if (myTimeToShowNotResponding < ((myTimer.GetTotalTime() - currentDebugText.myTimeWhenUpdatedLastTime) * 1000.0))
			{
				extraText = " NOT RESPONDING";
				currentDebugText.myText->SetColor(gfx::CText::ColorFloatToUint(1.0f, 0.0f, 0.0f));
			}

			if (updateFPS == true)
			{
				currentDebugText.myFPSLastUpdate = static_cast<unsigned int>(1000.0 / timeSpent);
			}

			*currentDebugText.myText = currentDebugText.myTextText + std::to_string(currentDebugText.myFPSLastUpdate) + " fps | " + std::to_string(static_cast<unsigned int>(timeSpent)) + " ms (" + std::to_string(timeSpent) + ")" + extraText;

			millisecondsMax = MAX(timeSpent, millisecondsMax);

			millisecondsSpent += timeSpent;
		}

		SDebugText& totalDebugText(myMSTexts[static_cast<unsigned short>(eMSTexts::Total)]);
		*totalDebugText.myText = totalDebugText.myTextText + std::to_string(static_cast<unsigned int>(millisecondsSpent)) + " ms (" + std::to_string(millisecondsSpent) + ")";

		SDebugText& maxDebugText(myMSTexts[static_cast<unsigned short>(eMSTexts::Max)]);
		if (updateFPS == true)
		{
			maxDebugText.myFPSLastUpdate = static_cast<unsigned int>(1000.0 / millisecondsMax);
		}
		*maxDebugText.myText = maxDebugText.myTextText + std::to_string(maxDebugText.myFPSLastUpdate) + " fps | " + std::to_string(static_cast<unsigned int>(millisecondsMax)) + " ms (" + std::to_string(millisecondsMax) + ")";
	}
}
#endif // _RETAIL
