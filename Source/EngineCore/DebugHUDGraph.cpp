#include "stdafx.h"
#include "DebugHUDGraph.h"
#ifndef _RETAIL
#include "..\GraphicsEngine\DebugTools.h"
#include <sstream>
#endif // !_RETAIL

namespace sce
{
#ifndef _RETAIL
	CDebugHUDGraph::CDebugHUDGraph()
		: myTimeSpanInSeconds(0)
		, myGraphMinXMargin(0.05f)
		, myTopValue(0.0f)
		, myTimePassed(0.0f)
	{
	}

	CDebugHUDGraph::~CDebugHUDGraph()
	{
	}

	void CDebugHUDGraph::Init(const CU::Vector2f& aTopLeftPosition, const CU::Vector2f& aBottomRightPosition, const unsigned short aTimeSpanInSeconds)
	{
		assert("DebugHUDGraph didn't meet minimum x size requirement" && ((aBottomRightPosition.x - aTopLeftPosition.x) > myGraphMinXMargin));

		myTopLeftPosition = aTopLeftPosition;
		myBottomRightPosition = aBottomRightPosition;
		myTimeSpanInSeconds = aTimeSpanInSeconds;

		myText.Init("Comic Sans MS", "MAX\n0ms", 24.0f, { 0.0f }, CU::Vector2f(myTopLeftPosition.x + 0.001f, myTopLeftPosition.y), gfx::CText::ColorFloatToUint(1.0f, 1.0f, 1.0f));
	}

	void CDebugHUDGraph::Update(const float aDeltaTime)
	{
		if (myAddedPointsQueue.empty() == false)
		{
			myTimePassed += (aDeltaTime * (1.0f / myTimeSpanInSeconds));

			auto& currentPoint(myAddedPointsQueue.front());
			while ((currentPoint = myAddedPointsQueue.front()).x < myTimePassed)
			{
				bool removedTopValue(false);
				if (currentPoint.y == myTopValue)
				{
					removedTopValue = true;
				}

				myAddedPointsQueue.pop_front();

				if (removedTopValue == true)
				{
					myTopValue = 0.0f;
					for (const auto& currentPoint2 : myAddedPointsQueue)
					{
						if (myTopValue < currentPoint2.y)
						{
							myTopValue = currentPoint2.y;
						}
					}
				}

				if (myAddedPointsQueue.empty() == true)
				{
					break;
				}
			}
		}

		myAddedPointsQueue.emplace_back(CU::Vector2f(1.0f + myTimePassed, aDeltaTime));
		
		if (aDeltaTime > myTopValue)
		{
			myTopValue = aDeltaTime;
		}

		std::stringstream topValueStream;
		topValueStream.precision(2);
		topValueStream << std::fixed << myTopValue * 1000.0f;
		myText = "MAX\n" + topValueStream.str() + "ms";
	}

	void CDebugHUDGraph::Render()
	{
		const CU::Vector2f& x1y1(myTopLeftPosition);
		const CU::Vector2f x1y2(myTopLeftPosition.x, myBottomRightPosition.y);
		const CU::Vector2f x2y1(myBottomRightPosition.x, myTopLeftPosition.y);
		const CU::Vector2f& x2y2(myBottomRightPosition);

		const float graphMinX(myTopLeftPosition.x + myGraphMinXMargin);
		const CU::Vector2f middleX1Y1(graphMinX, myTopLeftPosition.y);
		const CU::Vector2f middleX1Y2(graphMinX, myBottomRightPosition.y);

		gfx::CDebugTools* debugTools(gfx::CDebugTools::Get());

		if (debugTools == nullptr)
		{
			return;
		}

		gfx::CDebugTools::Get()->DrawLine2D(x1y1, x1y2);
		gfx::CDebugTools::Get()->DrawLine2D(x1y1, x2y1);
		gfx::CDebugTools::Get()->DrawLine2D(x1y2, x2y2);
		gfx::CDebugTools::Get()->DrawLine2D(x2y1, x2y2);

		gfx::CDebugTools::Get()->DrawLine2D(middleX1Y1, middleX1Y2);

		myText.Render();

		const float topValueToUse((myTopValue < ourMinimumMaxMilliseconds) ? (ourMinimumMaxMilliseconds) : (myTopValue));
		CU::Vector2f lastPosition(middleX1Y2);
		for (const auto& currentPoint : myAddedPointsQueue)
		{
			const CU::Vector2f currentPointFixed((graphMinX + ((currentPoint.x - myTimePassed) * (myBottomRightPosition.x - graphMinX)))
				, ((myTopLeftPosition.y + ((topValueToUse - currentPoint.y) * (myBottomRightPosition.y - myTopLeftPosition.y))) / topValueToUse));

			gfx::CDebugTools::Get()->DrawLine2D(lastPosition, currentPointFixed);

			lastPosition = currentPointFixed;
		}
	}

	void CDebugHUDGraph::Clear()
	{
		myAddedPointsQueue.clear();
		myTopValue = 0.0f;
		myTimePassed = 0.0f;
	}
#endif // !_RETAIL
}
