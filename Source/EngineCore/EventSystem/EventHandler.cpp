#include "stdafx.h"
#include "EventHandler.h"
#include "../CommonUtilities/InputManager.h"

using namespace Event;

sce::CEventHandler* sce::CEventHandler::myInstance_ptr = nullptr;

sce::CEventHandler::CEventHandler()
	: myKeyEventList(256)
	, myEventDataList(128)
	, myReceiverList(64)
	, myInputManager_ptr(nullptr)
{
}

sce::CEventHandler::~CEventHandler()
{
	DeleteAllEvents();
	myKeyEventList.RemoveAll();
	myReceiverList.RemoveAll();

	myInputManager_ptr = nullptr;
}

void sce::CEventHandler::Create()
{
	if (myInstance_ptr != nullptr)
	{
		ENGINE_LOG("WARNING! [CEventHandler] Tried to create new instance of event handler after it was already created.");
		return;
	}

	myInstance_ptr = sce_new(CEventHandler());

	if (myInstance_ptr == nullptr)
	{
		ENGINE_LOG("ERROR! [CEventHandler] Failed to create new instance of event handler because memory allocation failed.");
	}
}

void sce::CEventHandler::Destroy()
{
	if (myInstance_ptr == nullptr)
	{
		ENGINE_LOG("WARNING! [CEventHandler] Tried to destroy an non-existing instance of event handler.");
		return;
	}

	sce_delete(myInstance_ptr);
}

void sce::CEventHandler::Update()
{
	UpdateInputManagerEvents();

	// Goes through all events that were sent.

	if (myKeyEventList.Size() > 0)
	{
		for (unsigned int i(0); i < myKeyEventList.Size(); ++i)
		{
			GiveEventToAllReceivers(myKeyEventList[i]);
		}

		myKeyEventList.RemoveAll();
	}

	if (myEventDataList.Size() > 0)
	{
		for (unsigned int i(0); i < myEventDataList.Size(); ++i)
		{
			GiveEventToAllReceivers(*myEventDataList[i]);
		}

		DeleteAllEvents();
	}

	// Stuff that (unfortunately) will have to loop through all receivers.

	const ESubscribedEvents mouseDataEventType(static_cast<ESubscribedEvents>(myMouseDataEvent.myType));
	const ESubscribedEvents mouseClickEventType(static_cast<ESubscribedEvents>(myMouseClickEventList[0].myType));

	const ESubscribedEvents rawMouseDataEventType(static_cast<ESubscribedEvents>(myRawMouseDataEvent.myType));

	for (unsigned int i(0); i < myReceiverList.Size(); ++i)
	{
		auto& receiver_ref(*myReceiverList[i]);

		if ((receiver_ref.mySubscribedEvents & mouseDataEventType) != 0)
		{
			receiver_ref.ReceiveEvent(myMouseDataEvent);
		}
		
		if ((receiver_ref.mySubscribedEvents & mouseClickEventType) != 0)
		{
			for (unsigned char mClickEventIndex(0); mClickEventIndex < 3; ++mClickEventIndex)
			{
				if (myMouseClickEventList[mClickEventIndex].state != EButtonState::NONE)
				{
					receiver_ref.ReceiveEvent(myMouseClickEventList[mClickEventIndex]);
				}
			}
		}

		if ((receiver_ref.mySubscribedEvents & rawMouseDataEventType) != 0)
		{
			receiver_ref.ReceiveEvent(myRawMouseDataEvent);
		}
	}
}

void sce::CEventHandler::OnApplicationExit() const
{
	assert("ERROR! Event handler still has attached event receivers." && myReceiverList.Size() <= 0);
}

/* PRIVATE FUNCTIONS */

void sce::CEventHandler::UpdateInputManagerEvents()
{
	if (myInputManager_ptr != nullptr && !myInputManager_ptr->LostFocus())
	{
		// Read keyboard data.

		for (unsigned char key = static_cast<unsigned char>(CU::Keys::START); key < static_cast<unsigned char>(CU::Keys::COUNT); ++key)
		{
			SKeyEvent event;
			event.key = key;

			if (myInputManager_ptr->WasKeyJustPressed(key))
			{
				event.state = EButtonState::Pressed;
			}
			else if (myInputManager_ptr->IsKeyDown(key))
			{
				event.state = EButtonState::Down;
			}
			else if (myInputManager_ptr->WasKeyJustReleased(key))
			{
				event.state = EButtonState::Released;
			}
			else if (myInputManager_ptr->IsKeyUp(key))
			{
				event.state = EButtonState::Up;
			}
			else
			{
				event.state = EButtonState::NONE;
			}

			if (event.state != EButtonState::NONE)
			{
				myKeyEventList.Add(event);
			}
		}

		// Read mouse data.

		myMouseDataEvent.x = myInputManager_ptr->GetMousePositionX();
		myMouseDataEvent.y = myInputManager_ptr->GetMousePositionY();
		myMouseDataEvent.deltaX = myInputManager_ptr->GetMouseXMovementSinceLastFrame();
		myMouseDataEvent.deltaY = myInputManager_ptr->GetMouseYMovementSinceLastFrame();
		myMouseDataEvent.scrollDelta = myInputManager_ptr->GetScrollMovementSinceLastFrame();

		myRawMouseDataEvent.x = myInputManager_ptr->GetRawMouseXMovementSinceLastFrame();
		myRawMouseDataEvent.y = myInputManager_ptr->GetRawMouseYMovementSinceLastFrame();
		// Read mouse click data.

		for (unsigned char mButtonIndex = 1; mButtonIndex <= 3; ++mButtonIndex)
		{
			auto& currMouseClickEvent_ref(myMouseClickEventList[mButtonIndex - 1]);

			currMouseClickEvent_ref.x = myMouseDataEvent.x;
			currMouseClickEvent_ref.y = myMouseDataEvent.y;
			currMouseClickEvent_ref.deltaX = myMouseDataEvent.deltaX;
			currMouseClickEvent_ref.deltaY = myMouseDataEvent.deltaY;
			currMouseClickEvent_ref.scrollDelta = myMouseDataEvent.scrollDelta;

			currMouseClickEvent_ref.key = mButtonIndex;

			if (myInputManager_ptr->WasMouseButtonJustPressed(mButtonIndex))
			{
				currMouseClickEvent_ref.state = EButtonState::Pressed;
			}
			else if (myInputManager_ptr->IsMouseButtonDown(mButtonIndex))
			{
				currMouseClickEvent_ref.state = EButtonState::Down;
			}
			else if (myInputManager_ptr->WasMouseButtonJustReleased(mButtonIndex))
			{
				currMouseClickEvent_ref.state = EButtonState::Released;
			}
			else if (currMouseClickEvent_ref.state != EButtonState::NONE)
			{
				currMouseClickEvent_ref.state = EButtonState::NONE;
			}
		}
	}
}

void sce::CEventHandler::GiveEventToAllReceivers(const SEvent& aEvent)
{
	const ESubscribedEvents eventType(static_cast<ESubscribedEvents>(aEvent.myType));

	for (unsigned int j(0); j < myReceiverList.Size(); ++j)
	{
		auto& receiver_ref(*myReceiverList[j]);

		if ((receiver_ref.mySubscribedEvents & eventType) != 0)
		{
			receiver_ref.ReceiveEvent(aEvent);
		}
	}
}

void sce::CEventHandler::DeleteAllEvents()
{
	for (unsigned int i(0); i < myEventDataList.Size(); ++i)
	{
		sce_delete(myEventDataList[i]);
	}

	myEventDataList.RemoveAll();
}
