#pragma once
#include "EventReceiver.h"
#include "../CommonUtilities/GrowingArray.h"

namespace CommonUtilities
{
	class InputManager;
};
namespace CU = CommonUtilities;

namespace sce
{
	class CEventHandler
	{
		friend class CEventReceiver;

	public:
		~CEventHandler();

		inline static CEventHandler& Get() { return *myInstance_ptr; }
		static void Create();
		static void Destroy();

		void Update();
		void OnApplicationExit() const;
		inline void AttachInputManager(const CU::InputManager& aInputManager_ref)
		{
			myInputManager_ptr = &aInputManager_ref;
		}
		inline void SendEvent(Event::SEvent* aEvent_ptr)
		{
			if (aEvent_ptr == nullptr)
			{
				printf("ERROR! Sent a event that was nullptr.\n");
				return;
			}

			myEventDataList.Add(aEvent_ptr);
		}

	private:
		CEventHandler();
		CEventHandler(CEventHandler&& aEventHandler) = delete;
		CEventHandler(const CEventHandler& aEventHandler) = delete;
		void operator=(const CEventHandler& aEventHandler) = delete;

	private:
		void UpdateInputManagerEvents();
		void GiveEventToAllReceivers(const Event::SEvent& aEvent);
		void DeleteAllEvents();
		inline static void AttachEventReceiver(CEventReceiver& aEventReceiver_ref)
		{
			const auto result(myInstance_ptr->myReceiverList.Find(&aEventReceiver_ref));

			if (result == myInstance_ptr->myReceiverList.FoundNone)
			{
				myInstance_ptr->myReceiverList.Add(&aEventReceiver_ref);
			}
			else
			{
				ENGINE_LOG("WARNING! [CEventHandler] Tried to attach an already existing event receiver. The operation will be ignored.\n");
			}
		}
		inline static void DetachEventReceiver(CEventReceiver& aEventReceiver_ref)
		{
			const auto result(myInstance_ptr->myReceiverList.Find(&aEventReceiver_ref));

			if (result != myInstance_ptr->myReceiverList.FoundNone)
			{
				myInstance_ptr->myReceiverList.RemoveCyclicAtIndex(result);
			}
			else
			{
				ENGINE_LOG("WARNING! [CEventHandler] Could not detach the receiver because it was not attached. The operation will be ignored.\n");
			}
		}

	private:
		static CEventHandler* myInstance_ptr;

		// Hard-coded list(s)/single(s).
		CU::GrowingArray<Event::SKeyEvent, unsigned int> myKeyEventList;
		Event::SMouseClickEvent myMouseClickEventList[3];
		Event::SMouseDataEvent myMouseDataEvent;
		Event::SRawMouseMoveEvent myRawMouseDataEvent;

		CU::GrowingArray<Event::SEvent*, unsigned int> myEventDataList;
		CU::GrowingArray<CEventReceiver*, unsigned int> myReceiverList;
		const CU::InputManager* myInputManager_ptr;
	};
};