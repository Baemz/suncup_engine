#include "stdafx.h"
#include "EventReceiver.h"
#include "EventHandler.h"

using namespace Event;

sce::CEventReceiver::CEventReceiver()
{
}

sce::CEventReceiver::~CEventReceiver()
{
}

void sce::CEventReceiver::AttachToEventReceiving(const ESubscribedEvents& aEventsToSubscribe)
{
	mySubscribedEvents = aEventsToSubscribe;
	CEventHandler::AttachEventReceiver(*this);
}

void sce::CEventReceiver::DetachFromEventReceiving()
{
	CEventHandler::DetachEventReceiver(*this);
}
