#pragma once
#include "../CommonUtilities/GrowingArray.h"
#include "../CommonUtilities/Vector3.h"

#include "../Game/NavigationMesh/NavMeshStructs.h"
#include "../EntitySystem/Entity.h"

enum class EButtonState
{
	Pressed,
	Released,
	Down,
	Up,
	NONE
};

namespace Event
{
	// Data components

	enum ESubscribedEvents // For storing multiple events.
	{
		eNONE = 0
		, eKeyInput = 1 << 1
		, eMouseInput = 1 << 2
		, eMouseClickInput = 1 << 3
		, eRawMouseInput = 1 << 4
		, eStoryEvent = 1 << 5
		, eAIEvent = 1 << 6
		, eNewPath = 1 << 7
		, eGameState = 1 << 8
	};
	enum class EEventType // For storing single event.
	{
		eNONE = 0
		, eKeyInput = ESubscribedEvents::eKeyInput
		, eMouseInput = ESubscribedEvents::eMouseInput
		, eMouseClickInput = ESubscribedEvents::eMouseClickInput
		, eRawMouseInput = ESubscribedEvents::eRawMouseInput
		, eStoryEvent = ESubscribedEvents::eStoryEvent
		, eAIEvent = ESubscribedEvents::eAIEvent
		, eNewPath = ESubscribedEvents::eNewPath
		, eGameState = ESubscribedEvents::eGameState
	};
	enum EAIEvents: unsigned int
	{
		eNone = 0,
		ePlayerChangedNavmeshNode = 1 << 0,
		ePlayerDied = 1 << 1
	};

	enum class EGameState
	{
		eKeep
		, eQuit
	};

	// Events

	struct SEvent
	{
		SEvent()
			: myType(EEventType::eNONE)
		{}

	protected:
		SEvent(const EEventType aType)
			: myType(aType)
		{}

	public:
		const EEventType myType;
	};

	struct SNewPathEvent: public SEvent
	{
		SNewPathEvent()
			: SEvent(EEventType::eNewPath)
		{}

	protected:
		SNewPathEvent(const EEventType aType)
			: SEvent(aType)
		{}

	public:
		NavMesh::Path myPath;
		UniqueIndexType myEntityIndex;
	};

	struct SInputEvent: public SEvent
	{
	protected:
		SInputEvent(const EEventType aType)
			: SEvent(aType)
		{}

	public:
		unsigned char key;
	};

	struct SKeyEvent: public SInputEvent
	{
		SKeyEvent()
			: SInputEvent(EEventType::eKeyInput)
		{}

		inline SKeyEvent& operator=(const SKeyEvent& aKeyEvent)
		{
			state = aKeyEvent.state;
			key = aKeyEvent.key;

			return *this;
		}

	protected:
		SKeyEvent(const EEventType aType)
			: SInputEvent(aType)
		{}

	public:
		EButtonState state = EButtonState::NONE;
	};

	struct SMouseDataEvent : public SInputEvent
	{
		SMouseDataEvent()
			: SInputEvent(EEventType::eMouseInput)
		{}

	protected:
		SMouseDataEvent(const EEventType aType)
			: SInputEvent(aType)
		{}

	public:
		unsigned long x;
		unsigned long y;
		long deltaX;
		long deltaY;
		long scrollDelta;
	};

	struct SMouseClickEvent: public SMouseDataEvent
	{
		SMouseClickEvent()
			: SMouseDataEvent(EEventType::eMouseClickInput)
		{}

		SMouseClickEvent(const SMouseClickEvent& aMouseDataMsg)
			: SMouseDataEvent(EEventType::eMouseClickInput)
		{
			x = aMouseDataMsg.x;
			y = aMouseDataMsg.y;
			deltaX = aMouseDataMsg.deltaX;
			deltaY = aMouseDataMsg.deltaY;
			scrollDelta = aMouseDataMsg.scrollDelta;
		}

	protected:
		SMouseClickEvent(const EEventType aType)
			: SMouseDataEvent(aType)
		{}

	public:
		EButtonState state = EButtonState::NONE;
	};

	struct SRawMouseMoveEvent: public SInputEvent
	{
		SRawMouseMoveEvent()
			: SInputEvent(EEventType::eRawMouseInput)
		{}

	protected:
		SRawMouseMoveEvent(const EEventType aType)
			: SInputEvent(aType)
		{}

	public:
		long x;
		long y;
	};

	struct SAIEvent: public SEvent
	{
		SAIEvent()
			: SEvent(EEventType::eAIEvent)
		{
			myMessage = EAIEvents::eNone;
			myArbitraryData = nullptr;
		}

		SAIEvent(const EAIEvents aEvent)
			: SEvent(EEventType::eAIEvent)
		{
			myMessage = aEvent;
			myArbitraryData = nullptr;
		}
	protected:
		SAIEvent(const EEventType aType)
			: SEvent(aType)
		{}

	public:
		union
		{
			CU::Vector3f myPosition;
			float myFloat;
			int myInteger;
			void* myArbitraryData;
		};
		EAIEvents myMessage;
	};

	struct SGameState: public SEvent
	{
		SGameState()
			: SEvent(EEventType::eGameState)
			, myState(EGameState::eKeep)
		{}

	protected:
		SGameState(const EEventType aType)
			: SEvent(aType)
			, myState(EGameState::eKeep)
		{}

	public:
		EGameState myState;
	};
};
