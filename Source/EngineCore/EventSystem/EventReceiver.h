#pragma once
#include "EventStructs.h"

namespace sce
{
	class CEventHandler;

	class CEventReceiver
	{
		friend class CEventHandler;

	public:
		CEventReceiver();
		virtual ~CEventReceiver();

		void AttachToEventReceiving(const Event::ESubscribedEvents& aEventsToSubscribe);
		void DetachFromEventReceiving();

		virtual void ReceiveEvent(const Event::SEvent& aEvent) { aEvent; };

	private:
		Event::ESubscribedEvents mySubscribedEvents;
	};
};
