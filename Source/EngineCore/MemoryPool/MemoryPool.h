#pragma once
#include <unordered_map>
#include <map>
#include <assert.h>

#pragma warning(disable : 4291) // Compiler thinks there are no corresponding deletes for the news

#ifdef _DEBUG
#define SCE_CMEMORYPOOL_EXTRA_ARGUMENTS , const char* aFileName, const char* aFunctionName, const int aCodeLine
#define SCE_CMEMORYPOOL_NEW_ADDITIONAL_CODE , aFileName, aFunctionName, aCodeLine
//#define SCE_CMEMORYPOOL_SAFEGUARD_BYTES 256 // Max 256
#else
#define SCE_CMEMORYPOOL_EXTRA_ARGUMENTS
#define SCE_CMEMORYPOOL_NEW_ADDITIONAL_CODE
#endif // _DEBUG

#ifdef SCE_CMEMORYPOOL_SAFEGUARD_BYTES
namespace std
{
	class thread;
}
#endif // SCE_CMEMORYPOOL_SAFEGUARD_BYTES

enum class ENewDelete : char
{
	FAILED = 0,
	GOOD = 1,
};

namespace sce
{
	namespace MemoryPoolHelper
	{
		template<typename Type>
		inline std::size_t GetSizeOf(Type*)
		{
			const unsigned long long sizeOfType(sizeof(std::remove_all_extents<Type>::type));

			return sizeOfType;
		}
	}
}

inline void* operator new(std::size_t aSize, ENewDelete aENew, bool aThis, bool aIs, int aNullptr)
{
	aSize; aENew; aThis; aIs; aNullptr;
	return nullptr;
}

#ifdef _DEBUG
// Use to create any type with any constructor, except arrays
#define sce_new(aPointer) new(ENewDelete::GOOD, 0, sce::MemoryPoolHelper::GetSizeOf(new(ENewDelete::GOOD, false, false, 0) aPointer), __FILE__, __FUNCTION__, __LINE__) aPointer
// Use to create a array of any type with ONLY default constructor
#define sce_newArray(aPointer, aAmount) new(ENewDelete::GOOD, aAmount, sce::MemoryPoolHelper::GetSizeOf(new(ENewDelete::GOOD, false, false, 0) aPointer[aAmount]) , __FILE__, __FUNCTION__, __LINE__) aPointer[aAmount]
#else
// Use to create any type with any constructor, except arrays
#define sce_new(aPointer) new(ENewDelete::GOOD, 0, sce::MemoryPoolHelper::GetSizeOf(new(ENewDelete::GOOD, false, false, 0) aPointer)) aPointer
// Use to create a array of any type with ONLY default constructor
#define sce_newArray(aPointer, aAmount) new(ENewDelete::GOOD, aAmount, sce::MemoryPoolHelper::GetSizeOf(new(ENewDelete::GOOD, false, false, 0) aPointer[aAmount])) aPointer[aAmount]
#endif // _DEBUG
// Use to delete all pointers, also sets nullptr
#define sce_delete(aPointer) sce::CMemoryPool::DeleteData(&aPointer)

inline void* operator new(std::size_t aSize, ENewDelete aENew, std::size_t aArraySize, std::size_t aSizeOfType SCE_CMEMORYPOOL_EXTRA_ARGUMENTS);
inline void* operator new[](std::size_t aSize, ENewDelete aENew, std::size_t aArraySize, std::size_t aSizeOfType SCE_CMEMORYPOOL_EXTRA_ARGUMENTS);
inline void operator delete(void* ptr, ENewDelete aENew) noexcept;

namespace sce
{
	class CMemoryPool
	{
		friend inline void* ::operator new(std::size_t aSize, ENewDelete aENew, std::size_t aArraySize, std::size_t aSizeOfType SCE_CMEMORYPOOL_EXTRA_ARGUMENTS);
		friend inline void* ::operator new[](std::size_t aSize, ENewDelete aENew, std::size_t aArraySize, std::size_t aSizeOfType SCE_CMEMORYPOOL_EXTRA_ARGUMENTS);
		friend inline void ::operator delete(void* ptr, ENewDelete aENew) noexcept;
		friend class CEngine;

	public:
		CMemoryPool();
		~CMemoryPool();

		bool Init(unsigned long long aMaxBytes);
		bool Destroy(bool aForced);

		static unsigned long long GetFreeMemoryInBytes();
		static unsigned long long GetUsedMemoryInBytes();
		static const unsigned long long& GetMaxMemoryInBytes();

		bool CreateData(void** aData, const std::size_t& aSize, std::size_t aArraySize, std::size_t aSizeOfType SCE_CMEMORYPOOL_EXTRA_ARGUMENTS);

		template<typename Type>
		static void DeleteData(Type** aPointer);

	private:
		struct SDataSaved
		{
			SDataSaved() : myId(0), mySize(0), mySizeOfType(0), mySizeDifference(0), myArraySize(0)
#ifdef _DEBUG
				, myFileName(nullptr), myFunctionName(nullptr), myLineNumber(0)
#endif // _DEBUG
			{};
			SDataSaved(unsigned long long aId, unsigned long long aSize, unsigned long long aSizeOfType, unsigned long long aSizeDifference, unsigned long long aArraySize SCE_CMEMORYPOOL_EXTRA_ARGUMENTS)
				: myId(aId), mySize(aSize), mySizeOfType(aSizeOfType), mySizeDifference(aSizeDifference), myArraySize(aArraySize)
#ifdef _DEBUG
				, myFileName(aFileName), myFunctionName(aFunctionName), myLineNumber(aCodeLine)
#endif // _DEBUG
			{};

			unsigned long long myId;
			unsigned long long mySize;
			unsigned long long mySizeOfType;
			unsigned long long mySizeDifference;
			unsigned long long myArraySize;

#ifdef _DEBUG
			const char* myFileName;
			const char* myFunctionName;
			int myLineNumber;
#endif // _DEBUG
		};

		template <class Type>
		static void RunAllDestructorsInArrayExceptFirst(Type* aArrayType);

		template<class DataType>
		static void RunDestructor(DataType** aData);

		bool RemoveData(void** aData);

		const unsigned long long GetAmountOfObjectsAtPointer(void* aData) const;

		static CMemoryPool* ourInstance;

		bool myIsInitiated;

		char* myDataMemory;
		unsigned long long mySize;

		std::unordered_map<void*, SDataSaved> myDataSaved;
		std::multimap<unsigned long long, unsigned long long> myDataFree; // { Size, IDs[]}

#ifdef SCE_CMEMORYPOOL_SAFEGUARD_BYTES
		void SafeGuardUpdate();

		std::thread* mySafeGuardThread;
		volatile bool mySafeGuardForceQuit;
#endif // SCE_CMEMORYPOOL_SAFEGUARD_BYTES

	};

	template<class Type>
	inline void CMemoryPool::RunAllDestructorsInArrayExceptFirst(Type* aArrayType)
	{
		if (aArrayType == nullptr)
		{
			return;
		}

		const unsigned long long amountOfObjects(ourInstance->GetAmountOfObjectsAtPointer(aArrayType));

		for (unsigned long long objectIndex = 1; objectIndex < amountOfObjects; ++objectIndex)
		{
			aArrayType[objectIndex].~Type();
		}
	}

	template<class DataType>
	inline void CMemoryPool::RunDestructor(DataType** aData)
	{
		if ((*aData) == nullptr)
		{
			return;
		}

		(*aData)->~DataType();
	}

	template<typename Type>
	inline void CMemoryPool::DeleteData(Type** aPointer)
	{
		if ((ourInstance == nullptr) || (aPointer == nullptr) || ((*aPointer) == nullptr))
		{
			return;
		}

		RunAllDestructorsInArrayExceptFirst(*aPointer);
		RunDestructor(aPointer);
		operator delete((*aPointer), ENewDelete::GOOD);
		(*aPointer) = nullptr;
	}
}

#define SCE_CMEMORYPOOL_NEW_FUNCTION_BODY																								\
assert("New not used correctly" && (aENew == ENewDelete::GOOD));																		\
																																		\
if (sce::CMemoryPool::ourInstance == nullptr)																							\
{																																		\
	return nullptr;																														\
}																																		\
void* newPointer;																														\
bool result(sce::CMemoryPool::ourInstance->CreateData(&newPointer, aSize, aArraySize, aSizeOfType SCE_CMEMORYPOOL_NEW_ADDITIONAL_CODE));\
assert("MemoryPool filled!" && (result == true));																						\
return newPointer;

inline void* operator new(std::size_t aSize, ENewDelete aENew, std::size_t aArraySize, std::size_t aSizeOfType SCE_CMEMORYPOOL_EXTRA_ARGUMENTS)
{
	assert("Tried to new an empty object!" && (aSize > 0));
	SCE_CMEMORYPOOL_NEW_FUNCTION_BODY;
	aENew; result;
}

inline void* operator new[](std::size_t aSize, ENewDelete aENew, std::size_t aArraySize, std::size_t aSizeOfType SCE_CMEMORYPOOL_EXTRA_ARGUMENTS)
{
	assert("Tried to new an empty object!" && (aSize > 0));
	SCE_CMEMORYPOOL_NEW_FUNCTION_BODY;
	aENew; result;
}

inline void operator delete(void* ptr, ENewDelete aENew) noexcept
{
	if (ptr == nullptr)
	{
		return;
	}

	aENew;
	assert("Delete not used correctly" && (aENew == ENewDelete::GOOD));
	bool result(sce::CMemoryPool::ourInstance->RemoveData(&ptr));
	result;
	assert("MemoryPool doesn't have this object!" && (result == true));
}

#undef SCE_CMEMORYPOOL_NEW_FUNCTION_BODY
#ifdef _DEBUG
#undef SCE_CMEMORYPOOL_EXTRA_ARGUMENTS
#undef SCE_CMEMORYPOOL_NEW_ADDITIONAL_CODE
#endif // _DEBUG
