#include "stdafx.h"
#include "AudioInterfaceWrapper.h"
#include "../Audio Engine/AudioManager.h"

const unsigned int AudioInterfaceWraper::LoadAudioAndGetID(const char* aAudioPath)
{
	return AE::CAudioManager::LoadAudioAndGetID(aAudioPath);
}

const bool AudioInterfaceWraper::PlayAudio(const unsigned int aAudioID)
{
	return AE::CAudioManager::PlayAudio(aAudioID);
}

const bool AudioInterfaceWraper::UnloadAudio(const unsigned int aAudioID)
{
	return AE::CAudioManager::UnloadAudio(aAudioID);
}

void AudioInterfaceWraper::SetSoundPosition(const unsigned int aAudioID, const float aPosX, const float aPosY, const float aPosZ)
{
	AE::CAudioManager::SetAudioPosition(aAudioID, aPosX, aPosY, aPosZ);
}

void AudioInterfaceWraper::SetSoundPosition(const unsigned int aAudioID, const float aPos[3])
{
	AE::CAudioManager::SetAudioPosition(aAudioID, aPos[0], aPos[1], aPos[2]);
}

void AudioInterfaceWraper::SetListenerPosition(const float aPosX, const float aPosY, const float aPosZ)
{
	AE::CAudioManager::SetListenerPosition(aPosX, aPosY, aPosZ);
}

void AudioInterfaceWraper::SetListenerPosition(const float aPos[3])
{
	AE::CAudioManager::SetListenerPosition(aPos[0], aPos[1], aPos[2]);
}
