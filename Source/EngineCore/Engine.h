#pragma once
#include <windows.h>
#include <functional>
#include <atomic>
#include <queue>
#include "FileWatcher\FileWatcherWrapper.h"
#include "MemoryPool\MemoryPool.h"

namespace std
{
	class thread;
}

namespace sce {

	using callback_function_init = std::function<void()>;
	using callback_function_update = std::function<void(const float aDeltaTime)>;
	using callback_function_shutdown = std::function<void()>;

	struct SEngineStartParams
	{
		callback_function_init myInitCallback;
		callback_function_update myUpdateCallback;
		callback_function_shutdown myShutdownCallback;
	};

	namespace gfx
	{
		struct SWindowData;
		class CGraphicsEngine;
	}
	
	class CEngine
	{
	private:
		CMemoryPool myMemoryPool; // IMPORTANT: Needs to be first or crash (for destructor order last)

	public:
		CEngine();
		~CEngine();

		void Start();
		void Shutdown();

		void Init(const SEngineStartParams& aStartParams);
		void InitWithWindowData(const gfx::SWindowData& aWindowData, const SEngineStartParams& aStartParams);

	private:
		template<class T>
		inline void Swap(T& aFirst, T& aSecond)
		{
			T temp = aFirst;
			aFirst = aSecond;
			aSecond = temp;
		}

		void SwapWriteMSGQueue();
		void SwapReadMSGQueue();
		void RenderLoop();
		bool LoadConfig(gfx::SWindowData* aWindowData); 

		std::queue<MSG> myMSGQueue[3];
		CFileWatcherWrapper myFileWatcher;
		std::atomic_bool myShouldRun;
		volatile bool myShouldLoadConfig;
		unsigned int myWriteIndex;
		unsigned int myReadIndex;
		unsigned int myFreeIndex;

		std::thread* myRenderThread;
		sce::gfx::CGraphicsEngine* graphicsEngine;

		SEngineStartParams myStartParams;

	};
}