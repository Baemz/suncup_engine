#include "stdafx.h"
#define FILEWATCHER_WRAPPER_USED
#include "FileWatcher.h"
#undef FILEWATCHER_WRAPPER_USED
#include <windows.h>
#include <codecvt>
#include "..\CommonUtilities\ThreadHelper.h"
#include "WorkerPool\WorkerPool.h"

namespace sce
{
	CFileWatcher::CFileWatcher()
		: myChangeHandle(nullptr)
		, myWatchingThreadForceQuit(false)
	{
	}

	CFileWatcher::~CFileWatcher()
	{
		myWatchingThreadForceQuit = true;
		myWatchingThread.join();

		FindCloseChangeNotification(myChangeHandle);
	}

	bool CFileWatcher::Init()
	{
		myFilesToUpdateWatch.Init(32);
		myFilesToAddToWatch.Init(32);
		myFilesToRemoveToWatch.Init(32);

		HMODULE hModule = GetModuleHandleW(nullptr);
		wchar_t path[MAX_PATH];
		unsigned long pathSize(GetModuleFileNameW(hModule, path, MAX_PATH));

		if (pathSize == 0)
		{
			return false;
		}

		const std::wstring workingDirectoryPathWStringWithExe(path);
		const std::wstring workingDirectoryPathWString(workingDirectoryPathWStringWithExe.substr(0, (workingDirectoryPathWStringWithExe.find_last_of('\\') + 1)));

		myChangeHandle = FindFirstChangeNotificationW(workingDirectoryPathWString.c_str(), TRUE, FILE_NOTIFY_CHANGE_LAST_WRITE);
		if ((myChangeHandle == INVALID_HANDLE_VALUE) || (myChangeHandle == nullptr))
		{
			return false;
		}

		myWatchingThread = std::thread(&CFileWatcher::WatchingUpdate, this, std::cref<const volatile bool>(myWatchingThreadForceQuit));

		std::string threadPriorityErrorMessage;
		if (CU::ThreadHelper::SetThreadPriority(myWatchingThread, CU::ThreadHelper::EThreadPriority::Lowest, threadPriorityErrorMessage) == false)
		{
			ENGINE_LOG("ERROR! Failed to set file watcher thread priority. Error: %s"
				, threadPriorityErrorMessage.c_str());
		}
		CU::ThreadHelper::SetThreadName(myWatchingThread, "SCE | FileWatcher: Checking&Callback thread");

		return true;
	}

	bool CFileWatcher::AddFileToWatch(void* aThisCallback, const char* aFilePath, FileWatchFunction aFileWatchFunctionCallback, const bool aAddIfNotExisting)
	{
		SWatchData watchData;
		watchData.myThisCallback = aThisCallback;
		watchData.myFilePath = aFilePath;
		watchData.myCallbackFunction = aFileWatchFunctionCallback;
		
		bool fileExists(true);

		struct stat result;
		if (stat(watchData.myFilePath.c_str(), &result) == 0)
		{
			watchData.myLastTimeChanged = result.st_mtime;
		}
		else if (aAddIfNotExisting == false)
		{
			return false;
		}
		else
		{
			fileExists = false;
		}

		std::unique_lock<std::mutex> uniqueLock(myFilesToAddToWatchMutex);
		myFilesToAddToWatch.Add(watchData);
		uniqueLock.unlock();

		return fileExists;
	}

	void CFileWatcher::RemoveFileToWatch(void* aThisCallback, const char* aFilePath, FileWatchFunction aFileWatchFunctionCallback)
	{
		SWatchData watchData;
		watchData.myThisCallback = aThisCallback;
		watchData.myFilePath = aFilePath;
		watchData.myCallbackFunction = aFileWatchFunctionCallback;

		std::unique_lock<std::mutex> uniqueLock(myFilesToRemoveToWatchMutex);
		myFilesToRemoveToWatch.Add(watchData);
		uniqueLock.unlock();
	}

	void CFileWatcher::WatchingUpdate(const volatile bool& aForceQuit)
	{
		CU::ThreadHelper::CFramerateLimiter framerateLimiter;
		framerateLimiter.Init(10.0f, 0.0f);

		while (aForceQuit == false)
		{
			framerateLimiter.Update();

			std::unique_lock<std::mutex> uniqueLockAdd(myFilesToAddToWatchMutex);
			myFilesToUpdateWatch.AddUnique(myFilesToAddToWatch);
			myFilesToAddToWatch.RemoveAll();
			uniqueLockAdd.unlock();

			std::unique_lock<std::mutex> uniqueLockRemove(myFilesToRemoveToWatchMutex);
			for (unsigned short removeIndex(0); removeIndex < myFilesToRemoveToWatch.Size(); ++removeIndex)
			{
				const auto& currentWatchData(myFilesToRemoveToWatch[removeIndex]);

				const auto foundIndex(myFilesToUpdateWatch.Find(currentWatchData));
				if (foundIndex != myFilesToUpdateWatch.FoundNone)
				{
					myFilesToUpdateWatch.RemoveCyclicAtIndex(foundIndex);
				}
			}
			myFilesToRemoveToWatch.RemoveAll();
			uniqueLockRemove.unlock();

			for (unsigned short fileIndex(0); fileIndex < myFilesToUpdateWatch.Size(); ++fileIndex)
			{
				SWatchData& watchData(myFilesToUpdateWatch[fileIndex]);

				if (CheckIfFileHasChanged(watchData) == false)
				{
					continue;
				}

				WORKER_POOL_QUEUE_WORK(watchData.myCallbackFunction, watchData.myThisCallback, watchData.myFilePath.c_str());
			}
		}
	}

	bool CFileWatcher::CheckIfFileHasChanged(SWatchData& aWatchData)
	{
		struct stat result;
		const bool fileExists(!stat(aWatchData.myFilePath.c_str(), &result));

		if (fileExists == false)
		{
			if (aWatchData.myLastTimeChanged != 0)
			{
				aWatchData.myLastTimeChanged = 0;
				return true;
			}
		}
		else 
		{
			if (aWatchData.myLastTimeChanged != result.st_mtime)
			{
				aWatchData.myLastTimeChanged = result.st_mtime;
				return true;
			}
		}

		return false;
	}
}
