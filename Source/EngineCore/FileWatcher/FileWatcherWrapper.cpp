#include "stdafx.h"
#include "FileWatcherWrapper.h"

//#if defined(_RETAIL) && !defined(_MATEDIT)
namespace sce
{
	CFileWatcherWrapper* CFileWatcherWrapper::ourInstance(nullptr);
}
//#else
//#define FILEWATCHER_WRAPPER_USED
//#include "FileWatcher.h"
//#undef FILEWATCHER_WRAPPER_USED
//
//namespace sce
//{
//	CFileWatcherWrapper* CFileWatcherWrapper::ourInstance(nullptr);
//
//	CFileWatcherWrapper::CFileWatcherWrapper()
//		: myFileWatcher(nullptr)
//	{
//	}
//
//	CFileWatcherWrapper::~CFileWatcherWrapper()
//	{
//		sce_delete(myFileWatcher);
//	}
//
//	bool CFileWatcherWrapper::Init()
//	{
//		if (myFileWatcher != nullptr)
//		{
//			return false;
//		}
//
//		myFileWatcher = sce_new(CFileWatcher());
//		return myFileWatcher->Init();
//	}
//
//	bool CFileWatcherWrapper::AddFileToWatch(void* aThisCallback, const char* aFilePath, bool(*aFileWatchFunctionCallback)(void*, const char*), const bool aAddIfNotExisting)
//	{
//		return ourInstance->myFileWatcher->AddFileToWatch(aThisCallback, aFilePath, aFileWatchFunctionCallback, aAddIfNotExisting);
//	}
//
//	void CFileWatcherWrapper::RemoveFileToWatch(void* aThisCallback, const char* aFilePath, bool(*aFileWatchFunctionCallback)(void*, const char*))
//	{
//		ourInstance->myFileWatcher->RemoveFileToWatch(aThisCallback, aFilePath, aFileWatchFunctionCallback);
//	}
//}
//#endif // !_RETAIL
