#pragma once
#ifdef FILEWATCHER_WRAPPER_USED
#include <thread>
#include "../CommonUtilities\GrowingArray.h"
#include <mutex>

namespace sce
{
	using FileWatchFunction = std::function<bool(void*, const char*)>;

	class CFileWatcher
	{
	public:
		CFileWatcher();
		~CFileWatcher();

		bool Init();

		bool AddFileToWatch(void* aThisCallback, const char* aFilePath, FileWatchFunction aFileWatchFunctionCallback, const bool aAddIfNotExisting = false);
		void RemoveFileToWatch(void* aThisCallback, const char* aFilePath, FileWatchFunction aFileWatchFunctionCallback);

	private:
		struct SWatchData
		{
		public:
			SWatchData()
				: myThisCallback(nullptr)
				, myCallbackFunction(nullptr)
				, myLastTimeChanged(0)
			{};

			inline bool operator==(const SWatchData& aOther) const
			{
				if ((myThisCallback == aOther.myThisCallback)
					&& (myFilePath == aOther.myFilePath)
					&& (GetFunctionAddress(myCallbackFunction) == GetFunctionAddress(aOther.myCallbackFunction)))
				{
					return true;
				}
				return false;
			};

			void* myThisCallback;
			std::string myFilePath;
			FileWatchFunction myCallbackFunction;
			long long myLastTimeChanged;

		private:
			template<typename T, typename... U>
			size_t GetFunctionAddress(const std::function<T(U...)>& aFunction) const
			{
				typedef T(fnType)(U...);
				fnType* const* fnPointer = aFunction.template target<fnType*>();
				return (std::size_t)*fnPointer;
			}

		};

		void WatchingUpdate(const volatile bool& aForceQuit);
		bool CheckIfFileHasChanged(SWatchData& aWatchData);

		CU::GrowingArray<SWatchData> myFilesToUpdateWatch;
		CU::GrowingArray<SWatchData> myFilesToAddToWatch;
		CU::GrowingArray<SWatchData> myFilesToRemoveToWatch;
		void* myChangeHandle;
		
		std::thread myWatchingThread;
		std::mutex myFilesToAddToWatchMutex;
		std::mutex myFilesToRemoveToWatchMutex;
		volatile bool myWatchingThreadForceQuit;

	};
}
#else
#error You need to use the FileWatcherWrapper instead
#endif // FILEWATCHER_WRAPPER_USED
