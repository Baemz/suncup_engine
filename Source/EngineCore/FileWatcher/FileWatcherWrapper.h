#pragma once

class CScriptManager;
class CScriptEventManager;

namespace sce
{
	class CEngine;

	namespace gfx
	{
		class CModelFactory;
		class CDX11Shader;
	}

	class CFileWatcher;

	class CFileWatcherWrapper
	{
		friend CEngine;
		friend gfx::CModelFactory;
		friend gfx::CDX11Shader;
		friend CScriptManager;
		friend CScriptEventManager;

//#if defined(_RETAIL) && !defined(_MATEDIT)
	public:
		CFileWatcherWrapper() = default;
		~CFileWatcherWrapper() = default;

		bool Init() { return true; };

	private:
		static bool AddFileToWatch(void*, const char*, bool(*)(void*, const char*), const bool = false) { return true; };
		static void RemoveFileToWatch(void*, const char*, bool(*)(void*, const char*)) { };

		static CFileWatcherWrapper* ourInstance;

		//#else
		//	public:
		//		CFileWatcherWrapper();
		//		~CFileWatcherWrapper();
		//
		//		bool Init();
		//
		//	private:
		//		static bool AddFileToWatch(void* aThisCallback, const char* aFilePath, bool(*aFileWatchFunctionCallback)(void*, const char*), const bool aAddIfNotExisting = false);
		//		static void RemoveFileToWatch(void* aThisCallback, const char* aFilePath, bool(*aFileWatchFunctionCallback)(void*, const char*));
		//
		//		static CFileWatcherWrapper* ourInstance;
		//
		//		CFileWatcher* myFileWatcher;
		//#endif // _RETAIL

	};
}
