#pragma once
#ifndef _RETAIL
#include <queue>
#include "..\CommonUtilities\GrowingArray.h"
#include "..\GraphicsEngine\Text\Text.h"
#endif // !_RETAIL
#include "..\CommonUtilities\Vector2.h"

namespace sce
{
	class CDebugHUDGraph
	{
	public:
#ifdef _RETAIL
		CDebugHUDGraph() = default;
		~CDebugHUDGraph() = default;

		void Init(const CU::Vector2f&, const CU::Vector2f&, const unsigned short) {};

		void Update(const float) {};
		void Render() {};

		void Clear() {};

#else
		CDebugHUDGraph();
		~CDebugHUDGraph();

		void Init(const CU::Vector2f& aTopLeftPosition, const CU::Vector2f& aBottomRightPosition, const unsigned short aTimeSpanInSeconds);

		void Update(const float aDeltaTime);
		void Render();
		
		void Clear();

	private:
		static constexpr float ourMinimumMaxMilliseconds = (10.0f /*<----*/ / 1000.0f);

		CU::Vector2f myTopLeftPosition;
		CU::Vector2f myBottomRightPosition;
		unsigned short myTimeSpanInSeconds;
		const float myGraphMinXMargin;
		gfx::CText myText;
		
		std::deque<CU::Vector2f> myAddedPointsQueue;
		float myTopValue;
		float myTimePassed;

#endif // !_RETAIL
	};
}
