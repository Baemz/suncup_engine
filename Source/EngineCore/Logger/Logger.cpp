﻿#include "stdafx.h"
#include "Logger.h"
#include "../CommandLineManager/CommandLineManager.h"
#include <stdarg.h>
#include <iostream>
#include <sstream>
#include <chrono>
#include <iomanip>
#include <shared_mutex>
// __________________________
// Console color include
#include <windows.h>
// __________________________

using namespace sce;
namespace sce
{
	namespace Logger🔒
	{
		std::mutex ourLoggerMutex;
	}
}

static std::string GetTime();

CLogger* CLogger::CLogger::ourInstance = nullptr;

bool sce::CLogger::Create()
{
	if (ourInstance != nullptr)
	{
		assert(false && "INSTANCE ALREADY CREATED!");
		return false;
	}
	ourInstance = sce_new(CLogger());
	return true;
}

void sce::CLogger::Destroy()
{
	SAFE_DELETE(ourInstance);
}

CLogger* sce::CLogger::Get()
{
	return ourInstance;
}

void sce::CLogger::SetActiveStreamBuffer(const SStreamBuffer& aStreamBuffer)
{
	if ((aStreamBuffer.myStreamBuffer == nullptr) || (aStreamBuffer.mySharedLock == nullptr))
	{
		assert("Tried to set loggers buffer to nullptr" && (false));
		return;
	}

	std::unique_lock<std::mutex> uniqueLock(Logger🔒::ourLoggerMutex);
	myConsoleStreamBuffer = aStreamBuffer;

	myConsoleStream.~basic_ostream();
	new (&myConsoleStream) decltype(myConsoleStream)(myConsoleStreamBuffer.myStreamBuffer);
}

void sce::CLogger::ActivateFilter(ELogFilter aType)
{
	int x = static_cast<int>(myActiveFilters);
	x |= static_cast<int>(aType);
	myActiveFilters = static_cast<ELogFilter>(x);

	switch (aType)
	{
	case Engine:
		myEngineFile.open(myEngineLogPath, std::ofstream::out | std::ofstream::app);
		break;
	case Resource:
		myResourceFile.open(myResourceLogPath, std::ofstream::out | std::ofstream::app);
		break;
	case Game:
		myGameFile.open(myGameLogPath, std::ofstream::out | std::ofstream::app);
		break;
	case Audio:
		myAudioFile.open(myAudioLogPath, std::ofstream::out | std::ofstream::app);
		break;
	case Script:
		myScriptFile.open(myScriptLogPath, std::ofstream::out | std::ofstream::app);
		break;
	}
}

void sce::CLogger::DeactivateFilter(ELogFilter aType)
{
	int x = static_cast<int>(myActiveFilters);
	x &=~ static_cast<int>(aType);
	myActiveFilters = static_cast<ELogFilter>(x);
	switch (aType)
	{
	case Engine:
		myEngineFile.close();
		break;
	case Resource:
		myResourceFile.close();
		break;
	case Game:
		myGameFile.close();
		break;
	case Audio:
		myAudioFile.close();
		break;
	case Script:
		myScriptFile.close();
		break;
	}
}

void sce::CLogger::WriteLog(ELogFilter aType, const ConsoleColors aColor, const char* aFormattedString, ...)
{
	if (myActiveFilters & aType)
	{
		va_list ap;
		va_start(ap, aFormattedString);
		char message[1024];
		_vsnprintf_s(message, 1023, aFormattedString, ap);
		std::string stringMessage(message);
		std::string finalString;
		std::ofstream* ostreamToUse(nullptr);
		switch (aType)
		{
		case Engine:
			finalString = "[" + GetTime() + "]" + "[Engine]" + " " + stringMessage;
			ostreamToUse = &myEngineFile;
			break;
		case Resource:
			finalString = "[" + GetTime() + "]" + "[Resource]" + " " + stringMessage;
			ostreamToUse = &myResourceFile;
			break;
		case Game:
			finalString = "[" + GetTime() + "]" + "[Game] " + stringMessage;
			ostreamToUse = &myGameFile;
			break;
		case Audio:
			finalString = "[" + GetTime() + "]" + "[Audio]" + " " + stringMessage;
			ostreamToUse = &myAudioFile;
			break;
		case Script:
			finalString = "[" + GetTime() + "]" + "[Script]" + " " + stringMessage;
			ostreamToUse = &myScriptFile;
			break;
		default:
			// Will crash later
			break;
		}

		std::unique_lock<std::mutex> lockGuard(Logger🔒::ourLoggerMutex, std::defer_lock);
		if (myHConsole != nullptr)
		{
			ConsoleColors colorToUse(aColor);
			if (stringMessage.find("SUCCESS") != std::string::npos)
			{
				colorToUse = ConsoleColors::Black_Green;
			}
			else if (stringMessage.find("WARNING") != std::string::npos)
			{
				colorToUse = ConsoleColors::Black_Yellow;
			}
			else if (stringMessage.find("ERROR") != std::string::npos)
			{
				colorToUse = ConsoleColors::Black_Red;
			}
			lockGuard.lock();
			SetConsoleTextAttribute(myHConsole, static_cast<unsigned short>(colorToUse));
		}
		else
		{
			lockGuard.lock();
		}

		if (myConsoleStreamBuffer.mySharedLock != nullptr)
		{
			std::shared_lock<std::shared_mutex> sharedLock(*myConsoleStreamBuffer.mySharedLock);
			myConsoleStream << finalString << std::endl;
		}
		std::setvbuf(stdout, nullptr, _IOFBF, BUFSIZ); // Faster printing
		std::cout << finalString << std::endl;
		std::setvbuf(stdout, nullptr, _IONBF, 0); // Reset so cout behaves normally again

		if (myHConsole != nullptr)
		{
			SetConsoleTextAttribute(myHConsole, static_cast<unsigned short>(ConsoleColors::Black_White));
		}
		(*ostreamToUse) << finalString << std::endl;
	}
}

std::string GetTime()
{
	const auto now = std::chrono::system_clock::now();
	const auto inTime_T = std::chrono::system_clock::to_time_t(now);
	struct tm timeInfo;
	localtime_s(&timeInfo, &inTime_T);
	std::stringstream timeToday;
	timeToday << std::put_time(&timeInfo, "%X");
	return timeToday.str();
}

CLogger::CLogger()
	: myActiveFilters(ELogFilter::None)
	, myHConsole(nullptr)
	, myConsoleStream(nullptr)
#ifndef _RETAIL
	, myEngineLogPath("log_engine.log")
	, myResourceLogPath("log_resource.log")
	, myGameLogPath("log_game.log")
	, myAudioLogPath("log_audio.log")
	, myScriptFile("log_script.log")
#endif
{
#ifndef _RETAIL
	myHConsole = GetStdHandle(STD_OUTPUT_HANDLE);

	if (CCommandLineManager::HasArgument(L"-activatelog", L"engine"))
	{
		ActivateFilter(Engine);
	}
	if (CCommandLineManager::HasArgument(L"-activatelog", L"resource"))
	{
		ActivateFilter(Resource);
	}
	if (CCommandLineManager::HasArgument(L"-activatelog", L"game"))
	{
		ActivateFilter(Game);
	}
	if (CCommandLineManager::HasArgument(L"-activatelog", L"audio"))
	{
		ActivateFilter(Audio);
	}
	if (CCommandLineManager::HasArgument(L"-activatelog", L"script"))
	{
		ActivateFilter(Script);
	}
#endif
}

CLogger::~CLogger()
{
	myEngineFile.close();
	myResourceFile.close();
	myGameFile.close();
	myAudioFile.close();
	myScriptFile.close();
}
