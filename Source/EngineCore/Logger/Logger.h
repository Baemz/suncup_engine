#pragma once
#include <fstream>

namespace std
{
	class shared_mutex;
}

// Texts with "WARNING" will be yellow, "ERROR" red, "SUCCESS" green

#define DL_WRITELOG(aLog, aColor, ...)  sce::CLogger::Get()->WriteLog(aLog, aColor,__VA_ARGS__);

//#define USE_FILTERLOG
//#ifdef USE_FILTERLOG  
//#define RESOURCE_LOG(...) DL_WRITELOG(sce::ELogFilter::Resource, sce::CLogger::ConsoleColors::Black_Aqua, __VA_ARGS__)
//#define ENGINE_LOG(...) DL_WRITELOG(sce::ELogFilter::Engine, sce::CLogger::ConsoleColors::Black_Purple, __VA_ARGS__);
//#define GAME_LOG(...) DL_WRITELOG(sce::ELogFilter::Game, sce::CLogger::ConsoleColors::Black_White, __VA_ARGS__);
//#define AUDIO_LOG(...) DL_WRITELOG(sce::ELogFilter::Audio, sce::CLogger::ConsoleColors::Black_LightGray, __VA_ARGS__);
//#define SCRIPT_LOG(...) DL_WRITELOG(sce::ELogFilter::Script, sce::CLogger::ConsoleColors::Black_LightGray, __VA_ARGS__);
//#else
#define RESOURCE_LOG(...)
#define ENGINE_LOG(...)
#define GAME_LOG(...)
#define AUDIO_LOG(...)
#define SCRIPT_LOG(...)
//#endif

namespace sce {
	
	enum ELogFilter
	{
		None = 0,
		Resource = 1 << 0,
		Engine = 1 << 1,
		Game = 1 << 2,
		Audio = 1 << 3,
		Script = 1 << 4
	};

	class CLogger
	{
	public:
		// Format is 0xBF, B = Background and F = Foreground color
		enum class ConsoleColors : unsigned short
		{
			Black_White = 0x0F,
			Black_Green = 0x0A,
			Black_Yellow = 0x0E,
			Black_Red = 0x0C,
			Black_Aqua = 0x0B,
			Black_Purple = 0x0D,
			Black_LightGray = 0x08
		};

		struct SStreamBuffer
		{
			std::basic_streambuf<char, std::char_traits<char>>* myStreamBuffer = nullptr;
			std::shared_mutex* mySharedLock = nullptr;
		};

		~CLogger();

		static bool Create();
		static void Destroy();
		static CLogger* Get();

		void SetActiveStreamBuffer(const SStreamBuffer& aStreamBuffer);

		void ActivateFilter(ELogFilter aType);
		void DeactivateFilter(ELogFilter aType);
		void WriteLog(ELogFilter aType, const ConsoleColors aColor, const char* aFormattedString, ...);

	private:
		CLogger();

		static CLogger* ourInstance;
		ELogFilter myActiveFilters;

		void* myHConsole;

		std::ostream myConsoleStream;
		SStreamBuffer myConsoleStreamBuffer;

		std::ofstream myEngineFile;
		std::ofstream myResourceFile;
		std::ofstream myGameFile;
		std::ofstream myAudioFile;
		std::ofstream myScriptFile;

		std::string myEngineLogPath;
		std::string myResourceLogPath;
		std::string myGameLogPath;
		std::string myAudioLogPath;
		std::string myScriptLogPath;
	};
}