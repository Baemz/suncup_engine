#pragma once
#include "../../CommonUtilities/GrowingArray.h"
#include <string>
#include <map>

namespace sce
{
	class CFileFinder
	{
	public:
		static const bool GetFilePathsRecursive(CU::GrowingArray<std::string>& aPathsToFilesFound, const char* aFileExtension, const char* aRootFolderSearchPath);
		static bool FindFilesInFolder(CU::GrowingArray<std::string>& aPathsToFilesFound, const char* aFileExtension, const char* aRootFolderSearchPath = nullptr);
		static bool FindFilesInFolder(std::map<std::string, std::string>& aPathsToFilesFound, const char* aFileExtension, const char* aRootFolderSearchPath = nullptr);
		static bool FindFilesThatContainsInFolder(CU::GrowingArray<std::string>& aPathsToFilesFound, const char* aStringToCompare, const char* aFileExtension, const char* aRootFolderSearchPath = nullptr);
		static bool FindFilesThatContainsInFolder(CU::GrowingArray<std::wstring>& aPathsToFilesFound, const wchar_t* aStringToCompare, const wchar_t* aFileExtension, const wchar_t* aRootFolderSearchPath = nullptr);
		static bool FindFilesInFolder(CU::GrowingArray<std::wstring>& aPathsToFilesFound, const wchar_t* aFileExtension, const wchar_t* aRootFolderSearchPath = nullptr);
		static bool FindFilesInFolder(std::map<std::wstring, std::wstring>& aPathsToFilesFound, const wchar_t* aFileExtension, const wchar_t* aRootFolderSearchPath = nullptr);
		static const std::string FindFilePath(const char* aFileName, const char* aFileExtension, const char* aRootFolderSearchPath = nullptr);

	private:
		static void GetPathsRecursive(CU::GrowingArray<std::string>& aPathsToFilesFound, const char* aFileExtension, const char* aRootFolderSearchPath, const wchar_t* aFolderPath);
		static bool ScanFolder(CU::GrowingArray<std::wstring>& aPathsToFilesFound, const wchar_t* aFileExtension, const wchar_t* aFolderSearchPath, const wchar_t* aFoldersIn);
		static void AddFiles(CU::GrowingArray<std::wstring>& aPathsToFilesFound, const wchar_t* aFilePath, const wchar_t* aFoldersIn);

	};
}
