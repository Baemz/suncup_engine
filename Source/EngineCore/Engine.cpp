#include "stdafx.h"
#include "Engine.h"
#include <shlobj.h>
#include <thread>
#include <mutex>
#include "../GraphicsEngine/GraphicsEngine.h"
#include "../GraphicsEngine/GraphicsEngineInterface.h"
#include "../CommonUtilities/Timer.h"
#include "../CommonUtilities/InputManager.h"
#include "EventSystem/EventHandler.h"

#include "JSON/json.hpp"
#include "WorkerPool/WorkerPool.h"
#include "../CommonUtilities/ThreadHelper.h"
#include "../CommonUtilities/Random.h"
#include "DebugHUD.h"

#include "../AudioEngine/AudioManager.h"
#include "../Game/AudioWrapper.h"

#include "../Script/ScriptManager.h"
#include "../GraphicsEngine/ImGUI/imgui.h"

#define GAME_NAME "Demons Run"

static float globalPrevCPUpercent = 0.0f; // HACK
static float globalCurrCPUpercent = 0.0f; // HACK


// Use this define if you want to disable threaded rendering.
// If enabled the rendering will occur on the same thread as the game-logic
// and will be executed after the logic.
//#define DISABLE_THREADED_RENDERING


using namespace sce;

using json = nlohmann::json;

void ErrorCallback(const char* aError)
{
#ifndef _RETAIL
	ENGINE_LOG(aError);
#else
	aError;
#endif
}

sce::gfx::SWindowData globalWindowData;
std::mutex ourMSGLock;

CEngine::CEngine()
	: myShouldRun(false)
	, myRenderThread(nullptr)
	, graphicsEngine(nullptr)
	, myShouldLoadConfig(true)
	, myFreeIndex(0)
	, myReadIndex(1)
	, myWriteIndex(2)
{
}


CEngine::~CEngine()
{
	if (myRenderThread != nullptr && myRenderThread->joinable())
	{
		myRenderThread->join();
	}
	sce_delete(myRenderThread);
	sce_delete(graphicsEngine);
	//CComponentFactory::Destroy();
#ifndef _MATEDIT
	CAudioWrapper::Destroy();
	CAudioManager::Destroy();
#endif
	CEventHandler::Destroy();
	CLogger::Destroy();
}


static bool IsAnyMouseButtonDown()
{
	ImGuiIO& io = ImGui::GetIO();
	for (int n = 0; n < ARRAYSIZE(io.MouseDown); n++)
		if (io.MouseDown[n])
			return true;
	return false;
}

static void CaptureConsoleInput(MSG& aMessage)
{
	ImGuiIO& io = ImGui::GetIO();
	switch (aMessage.message)
	{
	case WM_LBUTTONDOWN:
	case WM_RBUTTONDOWN:
	case WM_MBUTTONDOWN:
	{
		int button = 0;
		if (aMessage.message == WM_LBUTTONDOWN) button = 0;
		if (aMessage.message == WM_RBUTTONDOWN) button = 1;
		if (aMessage.message == WM_MBUTTONDOWN) button = 2;
		if (!IsAnyMouseButtonDown() && GetCapture() == NULL)
			SetCapture((HWND)globalWindowData.myHWND);
		io.MouseDown[button] = true;
		break;
	}
	case WM_LBUTTONUP:
	case WM_RBUTTONUP:
	case WM_MBUTTONUP:
	{
		int button = 0;
		if (aMessage.message == WM_LBUTTONUP) button = 0;
		if (aMessage.message == WM_RBUTTONUP) button = 1;
		if (aMessage.message == WM_MBUTTONUP) button = 2;
		io.MouseDown[button] = false;
		if (!IsAnyMouseButtonDown() && GetCapture() == (HWND)globalWindowData.myHWND)
			ReleaseCapture();
		break;
	}
	case WM_MOUSEWHEEL:
		io.MouseWheel += GET_WHEEL_DELTA_WPARAM(aMessage.wParam) > 0 ? +1.0f : -1.0f;
		break;
	case WM_MOUSEMOVE:
		io.MousePos.x = (signed short)(aMessage.lParam);
		io.MousePos.y = (signed short)(aMessage.lParam >> 16);
		break;
	case WM_KEYDOWN:
	case WM_SYSKEYDOWN:
		if (aMessage.wParam < 256)
			io.KeysDown[aMessage.wParam] = 1;
		break;
	case WM_KEYUP:
	case WM_SYSKEYUP:
		if (aMessage.wParam < 256)
			io.KeysDown[aMessage.wParam] = 0;
		break;
	case WM_CHAR:
		// You can also use ToAscii()+GetKeyboardState() to retrieve characters.
		if (aMessage.wParam > 0 && aMessage.wParam < 0x10000)
			io.AddInputCharacter((unsigned short)aMessage.wParam);
		break;
	}
}

#define KB * 1024llu
#define MB * 1024llu KB
#define GB * 1024llu MB

void CEngine::Start()
{
	myMemoryPool.Init(128 MB);
	CMemoryPool::ourInstance = &myMemoryPool;

	graphicsEngine = sce_new(gfx::CGraphicsEngine());

	CLogger::Create();
	ENGINE_LOG("Starting engine...");

	CWorkerPool workerPool;
	workerPool.Init();
	ENGINE_LOG("SUCCESS! Initiated worker pool");
	CWorkerPool::ourInstance = &workerPool;

	if (myShouldLoadConfig)
	{
		if (LoadConfig(&globalWindowData) == false)
		{
			globalWindowData.myWidth = 1280;
			globalWindowData.myHeight = 720;
			globalWindowData.myStartInFullScreen = false;
			globalWindowData.myWindowName = "[DEFAULT] Necrobyte Engine";
			globalWindowData.myUseBloom = true;
			globalWindowData.myUseFXAA = true;
			globalWindowData.myUseSSAO = true;
			globalWindowData.myUseVSync = false;
		}
	}

	myFileWatcher.Init();
	CFileWatcherWrapper::ourInstance = &myFileWatcher;

	CEventHandler::Create();
	CU::InputManager inputManager(true);
	CEventHandler::Get().AttachInputManager(inputManager);

	sce::gfx::CGraphicsEngineInterface::myGraphicsEngine = graphicsEngine;
	myShouldRun = graphicsEngine->Init(globalWindowData, myFileWatcher);

	CDebugHUD debugHUD;
	debugHUD.Init(0.5, 20.0, 1.0, 0.75);


#ifndef _MATEDIT
	CAudioManager::Create();
	CAudioWrapper::Create();
#endif
	ENGINE_LOG("SUCCESS! Sucessfully initialized engine.");

	CU::InitRandom();

	ENGINE_LOG("Initing game...");
	myStartParams.myInitCallback();
	ENGINE_LOG("SUCCESS! Sucessfully initialized game.");

	std::string threadPriorityErrorMessage;
	if (CU::ThreadHelper::SetThreadPriority(CU::ThreadHelper::EThreadPriority::Highest, threadPriorityErrorMessage) == false)
	{
		ENGINE_LOG("ERROR! Failed to set main thread priority. Error: %s"
			, threadPriorityErrorMessage.c_str());
	}
	CU::ThreadHelper::SetThreadName(std::string("Main / SCE | Update/Logic")
#ifdef DISABLE_THREADED_RENDERING
	+ " and Renderer");
#else
	);

	myRenderThread = sce_new(std::thread(&CEngine::RenderLoop, this));
	if (CU::ThreadHelper::SetThreadPriority(myRenderThread, CU::ThreadHelper::EThreadPriority::Highest, threadPriorityErrorMessage) == false)
	{
		ENGINE_LOG("ERROR! Failed to set render thread priority. Error: %s"
			, threadPriorityErrorMessage.c_str());
	}
	CU::ThreadHelper::SetThreadName(myRenderThread, "SCE | Renderer");
#endif

	CU::Timer timer;
	MSG windowsMsg = {};
	while (myShouldRun)
	{
		while (PeekMessage(&windowsMsg, nullptr, 0, 0, PM_REMOVE))
		{
#ifndef DISABLE_THREADED_RENDERING
			myMSGQueue[myWriteIndex].emplace(windowsMsg);
#else
			CaptureConsoleInput(windowsMsg);
#endif
			TranslateMessage(&windowsMsg);
			DispatchMessage(&windowsMsg);
			inputManager.HandleInput(windowsMsg.message, windowsMsg.wParam, windowsMsg.lParam);

			if (windowsMsg.message == WM_QUIT)
			{
				myShouldRun = false;
			}
		}
#ifndef DISABLE_THREADED_RENDERING
		SwapWriteMSGQueue();
#endif
		CEventHandler::Get().Update();

#ifndef _MATEDIT
		CAudioManager::RenderAudio();
#endif
		timer.Update();
		float deltaTime(timer.GetDeltaTime());
		if (deltaTime > 0.05f)
		{
			deltaTime = 0.05f;
		}

		myStartParams.myUpdateCallback(deltaTime);

 		debugHUD.Update(deltaTime);
 		debugHUD.Render();
		graphicsEngine->EndGameUpdateFrame();

#ifdef DISABLE_THREADED_RENDERING
		graphicsEngine->BeginFrame();
		graphicsEngine->RenderFrame(deltaTime, static_cast<float>(timer.GetTotalTime()));
		graphicsEngine->EndFrame();
#endif

		inputManager.Update();

		debugHUD.SetMS(CDebugHUD::eMSTexts::Update);
		//std::this_thread::yield();
	}

	if (myStartParams.myShutdownCallback != nullptr)
	{
		ENGINE_LOG("Asking game to prepare for shutdown...");
		myStartParams.myShutdownCallback();
		ENGINE_LOG("SUCCESS! Game has told me everything is gonna be alright");
	}

	ENGINE_LOG("Shutting down worker pool... (might take some time, something is wrong if it does)");
	workerPool.Destroy();
	ENGINE_LOG("SUCCESS! Worker pool shutdown");
}

void CEngine::Shutdown()
{
	myShouldRun = false;
}

void sce::CEngine::Init(const SEngineStartParams& aStartParams)
{
	myStartParams = aStartParams;
	myShouldRun = false;
}

void sce::CEngine::InitWithWindowData(const gfx::SWindowData& aWindowData, const SEngineStartParams& aStartParams)
{
	myShouldLoadConfig = false;
	globalWindowData = aWindowData;
	Init(aStartParams);
}

void sce::CEngine::SwapWriteMSGQueue()
{
	std::unique_lock<std::mutex> bufferLock(ourMSGLock);
	Swap(myWriteIndex, myFreeIndex);
	bufferLock.unlock();
}

void sce::CEngine::SwapReadMSGQueue()
{
	std::unique_lock<std::mutex> bufferLock(ourMSGLock);
	Swap(myReadIndex, myFreeIndex);
	bufferLock.unlock();
}

void sce::CEngine::RenderLoop()
{
	CU::Timer timer;
	while (myShouldRun)
	{
		while (myMSGQueue[myReadIndex].empty() == false)
		{
			MSG msg = myMSGQueue[myReadIndex].front();
			CaptureConsoleInput(msg);
			myMSGQueue[myReadIndex].pop();
		}
		SwapReadMSGQueue();

		float deltaTime(timer.GetDeltaTime());
		if (deltaTime > 0.05f)
		{
			deltaTime = 0.05f;
		}
		graphicsEngine->BeginFrame();
		graphicsEngine->RenderFrame(deltaTime, static_cast<float>(timer.GetTotalTime()));
		graphicsEngine->EndFrame(); 
		timer.Update();
	}
}

bool sce::CEngine::LoadConfig(gfx::SWindowData* aWindowData)
{
	//CHAR documentsPath[MAX_PATH];
	//HRESULT result = SHGetFolderPath(NULL, CSIDL_PERSONAL, NULL, SHGFP_TYPE_CURRENT, documentsPath);
	//if (FAILED(result))
	//{
	//	return false;
	//}

	std::string configPath("Data/config.json");
	std::ifstream configFile(configPath);

	if (configFile.good() == false)
	{
#ifdef _DEBUG
		printf("Didnt use documents-config.\n");
#endif
		configFile.open("Data/config.json");
		if (configFile.good() == false)
		{
			configFile.close();
			return false;
		}
	}

	json configJSON;
	configFile >> configJSON;
	configFile.close();

	try
	{
		aWindowData->myWindowName = configJSON["windowName"].get<std::string>();
		aWindowData->myWidth = configJSON["windowSize"]["width"];
		aWindowData->myHeight = configJSON["windowSize"]["height"];
		aWindowData->myStartInFullScreen = configJSON["startInFullscreen"];
		aWindowData->myUseBloom = configJSON["useBloom"];
		aWindowData->myUseSSAO = configJSON["useSSAO"];
		aWindowData->myUseVSync = configJSON["useVSync"];
		aWindowData->myUseFXAA = configJSON["useFXAA"];
	}
	catch (...)
	{
		std::cout << "Failed to load engine config, using default" << std::endl;
		std::cout << std::endl;
		return false;
	}

	return true;
}
