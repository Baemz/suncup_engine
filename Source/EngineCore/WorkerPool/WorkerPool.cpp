#include "stdafx.h"
#include "WorkerPool.h"
#include <thread>
#include "..\CommonUtilities\ThreadHelper.h"
#include "..\CommonUtilities\Timer.h"

#ifdef max
#undef max
#endif // max

namespace sce
{
	CWorkerPool* CWorkerPool::ourInstance(nullptr);

	CWorkerPool::CWorkerPool(WorkAmountType aMaxAmountOfWork)
		: myIsWorking(false)
		, myFunctionsQueueBuffer(nullptr)
		, myBossThreadForceQuit(false)
		, myAmountOfWorkers(0)
		, myWorkers(nullptr)
		, myWorkersFunctions(nullptr)
		, myWorkersStartBools(nullptr)
		, myWorkersForceQuit(false)
		, myMaxAmountOfWork(aMaxAmountOfWork)
	{
	}

	CWorkerPool::~CWorkerPool()
	{
	}

	bool CWorkerPool::Init(const WorkerAmountType aAmountOfWorkers, const WorkerAmountType aMinusWorkers)
	{
		if (myAmountOfWorkers != 0)
		{
			return false;
		}

		myAmountOfWorkers = aAmountOfWorkers;

		if (myAmountOfWorkers == 0)
		{
			WorkerAmountType workerAmount = (std::thread::hardware_concurrency() - aMinusWorkers);

			if (workerAmount > std::thread::hardware_concurrency() || workerAmount == 0)
			{
				workerAmount = 1;
			}

			myAmountOfWorkers = workerAmount;
		}

		myWorkers = sce_newArray(std::thread, myAmountOfWorkers);
		myWorkersFunctions = sce_newArray(SFunctionCallbackBase*, myAmountOfWorkers);
		myWorkersStartBools = new volatile bool[myAmountOfWorkers]();

		for (WorkerAmountType workerIndex = 0; workerIndex < myAmountOfWorkers; ++workerIndex)
		{
			myWorkersStartBools[workerIndex] = false;
			myWorkers[workerIndex] = std::thread(&CWorkerPool::WorkerThread, this
				, std::cref<const volatile bool>(myWorkersForceQuit)
				, std::ref<volatile bool>(myWorkersStartBools[workerIndex])
				, &myWorkersFunctions[workerIndex]);
			CU::ThreadHelper::SetThreadName(myWorkers[workerIndex], "SCE | WorkerPool: Worker " + std::to_string(workerIndex));
		}

		myFunctionsQueue1.Reserve(32);
		myFunctionsQueue2.Reserve(32);
		myFunctionsQueueBuffer = &myFunctionsQueue1;
		myBossThread = std::thread(&CWorkerPool::BossThread, this
			, std::cref<const volatile bool>(myBossThreadForceQuit));
		CU::ThreadHelper::SetThreadName(myBossThread, "SCE | WorkerPool: Boss");

		return true;
	}

	void CWorkerPool::Destroy()
	{
		if (myAmountOfWorkers == 0)
		{
			return;
		}

		if (ourInstance == this)
		{
			ourInstance = nullptr;
		}

		myBossThreadForceQuit = true;
		myBossThread.join();

		myWorkersForceQuit = true;

		for (WorkerAmountType workerIndex = 0; workerIndex < myAmountOfWorkers; ++workerIndex)
		{
			myWorkers[workerIndex].join();
		}
		myAmountOfWorkers = 0;

		myFunctionsQueueBuffer = nullptr;
		for (WorkAmountType functionIndex = 0; functionIndex < myFunctionsQueue1.Size(); ++functionIndex)
		{
			sce_delete(myFunctionsQueue1[functionIndex]);
		}
		myFunctionsQueue1.RemoveAll();
		for (WorkAmountType functionIndex = 0; functionIndex < myFunctionsQueue2.Size(); ++functionIndex)
		{
			sce_delete(myFunctionsQueue2[functionIndex]);
		}
		myFunctionsQueue2.RemoveAll();

		sce_delete(myWorkers);
		sce_delete(myWorkersFunctions);
		delete[] myWorkersStartBools;

		myWorkers = nullptr;
		myWorkersFunctions = nullptr;
		myWorkersStartBools = nullptr;
	}

	CWorkerPool::WorkAmountType CWorkerPool::GetAmountOfWorkInQueuedBuffer()
	{
		if (ourInstance == nullptr)
		{
			return WorkAmountType(-1);
		}
		std::lock_guard<std::mutex> lockGuard(ourInstance->myFunctionsQueueMutex);
		return ourInstance->myFunctionsQueueBuffer->Size();
	}

	bool CWorkerPool::GetHasWorkToDo()
	{
		if (ourInstance == nullptr)
		{
			return false;
		}
		std::lock_guard<std::mutex> lockGuard(ourInstance->myFunctionsQueueMutex);
		return ourInstance->myIsWorking;
	}

	void CWorkerPool::BossThread(const volatile bool& aForceQuit)
	{
		while (aForceQuit == false)
		{
			CU::GrowingArray<SFunctionCallbackBase*, WorkAmountType>* functionQueueFilled(myFunctionsQueueBuffer);

			std::unique_lock<std::mutex> lockGuard(myFunctionsQueueMutex);
			myFunctionsQueueBuffer = ((myFunctionsQueueBuffer == &myFunctionsQueue1) ? (&myFunctionsQueue2) : (&myFunctionsQueue1));
			myIsWorking = functionQueueFilled->Size() > 0;
			for (WorkerAmountType workerIndex = 0; workerIndex < myAmountOfWorkers; ++workerIndex)
			{
				if (myWorkersStartBools[workerIndex] == true)
				{
					myIsWorking = true;
					break;
				}
			}
			lockGuard.unlock();

			while ((functionQueueFilled->Size() > 0) && (aForceQuit == false))
			{
				const WorkerAmountType workerIndexMax(std::numeric_limits<WorkerAmountType>::max());

				WorkerAmountType choosenWorkerIndex(workerIndexMax);
				for (WorkerAmountType workerIndex = 0; workerIndex < myAmountOfWorkers; ++workerIndex)
				{
					if (myWorkersStartBools[workerIndex] == false)
					{
						choosenWorkerIndex = workerIndex;
						break;
					}
				}

				if (choosenWorkerIndex != workerIndexMax)
				{
					myWorkersFunctions[choosenWorkerIndex] = functionQueueFilled->GetLast();
					myWorkersStartBools[choosenWorkerIndex] = true;
					functionQueueFilled->RemoveCyclicAtIndex(functionQueueFilled->Size() - 1);
				}
				else
				{
					std::this_thread::yield();
				}
			}

			std::this_thread::yield();
		}
	}

	void CWorkerPool::WorkerThread(const volatile bool& aForceQuit, volatile bool& aShouldStart, SFunctionCallbackBase** aWorkFunctionPointer)
	{
		constexpr double timeWaitingToBeginSleepingInSeconds(5);

		CU::Timer timer;
		double timeSpentWaiting(0.0);

		timer.Update();
		while (aForceQuit == false)
		{
			if (aShouldStart == true)
			{
				(*(*aWorkFunctionPointer))();
				sce_delete(*aWorkFunctionPointer);
				aShouldStart = false;
				timeSpentWaiting = 0.0;
			}

			timer.Update();
			timeSpentWaiting += timer.GetDeltaTime();

			if (timeWaitingToBeginSleepingInSeconds < timeSpentWaiting)
			{
				std::this_thread::sleep_for(std::chrono::milliseconds(1));
			}
			else
			{
				std::this_thread::yield();
			}
		}
	}
}
