#pragma once
#include <future>
#include "..\CommonUtilities\GrowingArray.h"
#include "..\EngineCore\MemoryPool\MemoryPool.h"

#ifdef max
#pragma push_macro("max")
#undef max
#define MAX_UNDEFINED
#endif // max

#ifdef _DEBUG
// Don'THICC forget std::ref/std::cref on references/const references!
#define WORKER_POOL_QUEUE_WORK(aFunction, ...) sce::CWorkerPool::QueueWork(std::bind(aFunction, __VA_ARGS__), __FILE__, __FUNCTION__, __LINE__);
// Queue function with arguments and get a future to receive the return value
// Don'THICC forget std::ref/std::cref on references/const references!
#define WORKER_POOL_QUEUE_WORK_FUTURE(aFunction, ...) sce::CWorkerPool::QueueWorkFuture(std::bind(aFunction, __VA_ARGS__), aFunction, __FILE__, __FUNCTION__, __LINE__, __VA_ARGS__);
#else
// Don'THICC forget std::ref/std::cref on references/const references!
#define WORKER_POOL_QUEUE_WORK(aFunction, ...) sce::CWorkerPool::QueueWork(std::bind(aFunction, __VA_ARGS__));
// Queue function with arguments and get a future to receive the return value
// Don'THICC forget std::ref/std::cref on references/const references!
#define WORKER_POOL_QUEUE_WORK_FUTURE(aFunction, ...) sce::CWorkerPool::QueueWorkFuture(std::bind(aFunction, __VA_ARGS__), aFunction, __VA_ARGS__);
#endif // _DEBUG

namespace sce
{
	class CEngine;

	class CWorkerPool
	{
		using WorkerAmountType = unsigned int;
		using WorkAmountType = unsigned int;

		friend CEngine;

	public:
		CWorkerPool(WorkAmountType aMaxAmountOfWork = std::numeric_limits<WorkAmountType>::max());
		~CWorkerPool();

		bool Init(const WorkerAmountType aAmountOfWorkers = 0, const WorkerAmountType aMinusWorkers = 0);
		void Destroy();

		// Use the macro, don'THICC call this directly!
		template<typename BindType>
		static void QueueWork(BindType aBinder
#ifdef _DEBUG
			, const char* aFileName, const char* aFunctionName, const int aCodeLine
#endif // _DEBUG
		);

		// Use the macro, don'THICC call this directly!
		template<typename BindType, typename FunctionType, typename ...Arguments>
		static auto QueueWorkFuture(BindType aBinder, FunctionType
#ifdef _DEBUG
			, const char* aFileName, const char* aFunctionName, const int aCodeLine
#endif // _DEBUG
			, Arguments&&...
		);

		static WorkAmountType GetAmountOfWorkInQueuedBuffer();
		static bool GetHasWorkToDo();

	private:
		struct SFunctionCallbackBase
		{
			virtual void operator()() = 0;
#ifdef _DEBUG
			const char* myFileName;
			const char* myFunctionName;
			int myLineNumber;
#endif // _DEBUG
		};
		template<typename BindType>
		class SFunctionCallback : public SFunctionCallbackBase
		{
			friend CWorkerPool;

		public:
			SFunctionCallback(BindType aBinder)
				: myBinder(aBinder)
			{};

			void operator()() override
			{
				myBinder();
			}

		private:
			BindType myBinder;

		};
		template<typename BindType, typename PromiseType>
		class SFunctionCallbackFuture : public SFunctionCallbackBase
		{
			friend CWorkerPool;

		public:
			SFunctionCallbackFuture(BindType& aBinder)
				: myBinder(aBinder)
			{};

			void operator()() override
			{
				myPromise.set_value(myBinder());
			}

		private:
			BindType myBinder;
			PromiseType myPromise;

		};

		static CWorkerPool* ourInstance;

		void BossThread(const volatile bool& aForceQuit);
		void WorkerThread(const volatile bool& aForceQuit, volatile bool& aShouldStart, SFunctionCallbackBase** aWorkFunctionPointer);

		volatile bool myIsWorking;

		std::thread myBossThread;
		CU::GrowingArray<SFunctionCallbackBase*, WorkAmountType> myFunctionsQueue1;
		CU::GrowingArray<SFunctionCallbackBase*, WorkAmountType> myFunctionsQueue2;
		CU::GrowingArray<SFunctionCallbackBase*, WorkAmountType>* myFunctionsQueueBuffer;
		std::mutex myFunctionsQueueMutex;
		volatile bool myBossThreadForceQuit;

		std::thread* myWorkers;
		WorkerAmountType myAmountOfWorkers;
		SFunctionCallbackBase** myWorkersFunctions;
		volatile bool* myWorkersStartBools;
		volatile bool myWorkersForceQuit;

		WorkAmountType myMaxAmountOfWork;

	};

	template<typename BindType>
	inline void CWorkerPool::QueueWork(BindType aBinder
#ifdef _DEBUG
		, const char* aFileName, const char* aFunctionName, const int aCodeLine
#endif // _DEBUG
	)
	{
		SFunctionCallback<BindType>* newCallback(sce_new(SFunctionCallback<BindType>(aBinder)));
#ifdef _DEBUG
		newCallback->myFileName = aFileName;
		newCallback->myFunctionName = aFunctionName;
		newCallback->myLineNumber = aCodeLine;
#endif // _DEBUG

		std::unique_lock<std::mutex> lockGuard(ourInstance->myFunctionsQueueMutex);
		ourInstance->myFunctionsQueueBuffer->Add(newCallback);
		lockGuard.unlock();
	}

#define NEW_FUNCTION_CALLBACK_FUTURE SFunctionCallbackFuture<BindType, std::promise<ReturnType>>(aBinder)
	template<typename BindType, typename FunctionType, typename ...Arguments>
	inline auto CWorkerPool::QueueWorkFuture(BindType aBinder, FunctionType
#ifdef _DEBUG
		, const char* aFileName, const char* aFunctionName, const int aCodeLine
#endif // _DEBUG
		, Arguments&&...
	)
	{
		using ReturnType = std::result_of<FunctionType(Arguments...)>::type;
		SFunctionCallbackFuture<BindType, std::promise<ReturnType>>* newCallback(sce_new(NEW_FUNCTION_CALLBACK_FUTURE));
#ifdef _DEBUG
		newCallback->myFileName = aFileName;
		newCallback->myFunctionName = aFunctionName;
		newCallback->myLineNumber = aCodeLine;
#endif // _DEBUG
		
		auto future(newCallback->myPromise.get_future());

		std::unique_lock<std::mutex> lockGuard(ourInstance->myFunctionsQueueMutex);
		ourInstance->myFunctionsQueueBuffer->Add(newCallback);
		lockGuard.unlock();

		return future;
	}
#undef NEW_FUNCTION_CALLBACK_FUTURE
}

#ifdef MAX_UNDEFINED
#undef MAX_UNDEFINED
#pragma pop_macro("max")
#endif // MAX_UNDEFINED
