﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace StartupLauncher
{
    public partial class Form1 : Form
    {
        private struct WindowSize
        {
            public int width;
            public int height;
        }
        private struct StartParams
        {
            public string windowName;
            public bool startInFullscreen;
            public bool useBloom;
            public bool useFXAA;
            public bool useSSAO;
            public bool useVSync;
            public WindowSize windowSize;
        }

        public Form1()
        {
            InitializeComponent();
            resolutionBox.SelectedIndex = 2;
            comboBox1.SelectedIndex = 1;
            fullscreenCheck.Checked = true;
            bloomCheck.Checked = true;
            ssaoCheck.Checked = true;
            fxaaCheck.Checked = true;
        }

        private void playButton_Click(object sender, EventArgs e)
        {
            StartParams startParams = new StartParams();

            string width = resolutionBox.SelectedItem.ToString().Substring(0, resolutionBox.SelectedItem.ToString().LastIndexOf('x'));
            string height = resolutionBox.SelectedItem.ToString().Substring(resolutionBox.SelectedItem.ToString().LastIndexOf('x') + 1);

            startParams.windowName = "Demons Run";
            startParams.windowSize.width = Int32.Parse(width);
            startParams.windowSize.height = Int32.Parse(height);
            startParams.startInFullscreen = fullscreenCheck.Checked;
            startParams.useBloom = bloomCheck.Checked;
            startParams.useSSAO = ssaoCheck.Checked;
            startParams.useFXAA = fxaaCheck.Checked;
            startParams.useVSync = vsyncBox.Checked;

            JsonSerializer serializer = new JsonSerializer();
            serializer.Converters.Add(new JavaScriptDateTimeConverter());
            serializer.NullValueHandling = NullValueHandling.Ignore;

            string documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            documentsPath = documentsPath + "\\The Game Assembly\\Demons Run";
            Directory.CreateDirectory(documentsPath);
            documentsPath = documentsPath + "\\config.json";
            { 
                using (StreamWriter sw = new StreamWriter(documentsPath))
                using (JsonWriter writer = new JsonTextWriter(sw))
                {
                    serializer.Serialize(writer, startParams);
                }
            }

            Process process = new Process();
            process.StartInfo.FileName = "Game.exe";
            process.Start();

            Application.Exit();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (comboBox1.SelectedIndex)
            {
                case 0: // 4:3
                    SetResolutionsInBox(0);
                    break;
                default:
                case 1: // 16:9
                    SetResolutionsInBox(1);
                    break;
                case 2: // 16:10
                    SetResolutionsInBox(2);
                    break;
                case 3: // 21:9
                    SetResolutionsInBox(3);
                    break;
            }
        }

        private void SetResolutionsInBox(uint aAspectIndex)
        {
            resolutionBox.Items.Clear();

            switch (aAspectIndex)
            {
                case 0: // 4:3
                    resolutionBox.Items.Add("1600x1200");
                    resolutionBox.Items.Add("1400x1050");
                    resolutionBox.Items.Add("1280x960");
                    resolutionBox.Items.Add("1024x768");
                    resolutionBox.Items.Add("800x600");
                    resolutionBox.Items.Add("640x480");
                    resolutionBox.SelectedIndex = 3;
                    break;
                default:
                case 1: // 16:9
                    resolutionBox.Items.Add("3840x2160");
                    resolutionBox.Items.Add("2560x1440");
                    resolutionBox.Items.Add("1920x1080");
                    resolutionBox.Items.Add("1600x900");
                    resolutionBox.Items.Add("1366x768");
                    resolutionBox.Items.Add("1280x720");
                    resolutionBox.Items.Add("960x540");
                    resolutionBox.Items.Add("640x360");
                    resolutionBox.SelectedIndex = 2;
                    break;
                case 2: // 16:10
                    resolutionBox.Items.Add("3840x2400");
                    resolutionBox.Items.Add("2560x1600");
                    resolutionBox.Items.Add("1920x1200");
                    resolutionBox.Items.Add("1680x1050");
                    resolutionBox.Items.Add("1440x900");
                    resolutionBox.Items.Add("1280x800");
                    resolutionBox.Items.Add("640x400");
                    resolutionBox.SelectedIndex = 2;
                    break;
                case 3: // 21:9
                    resolutionBox.Items.Add("3440×1440");
                    resolutionBox.Items.Add("2560×1080");
                    resolutionBox.SelectedIndex = 1;
                    break;
            }
        }
    }
}
