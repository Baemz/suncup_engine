﻿namespace StartupLauncher
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.resolutionBox = new System.Windows.Forms.ComboBox();
            this.fullscreenCheck = new System.Windows.Forms.CheckBox();
            this.ssaoCheck = new System.Windows.Forms.CheckBox();
            this.fxaaCheck = new System.Windows.Forms.CheckBox();
            this.bloomCheck = new System.Windows.Forms.CheckBox();
            this.advancedOptions = new System.Windows.Forms.GroupBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.resolutionGroup = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.vsyncBox = new System.Windows.Forms.CheckBox();
            this.playButton = new System.Windows.Forms.Button();
            this.advancedOptions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.resolutionGroup.SuspendLayout();
            this.SuspendLayout();
            // 
            // resolutionBox
            // 
            this.resolutionBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.resolutionBox.FormattingEnabled = true;
            this.resolutionBox.Items.AddRange(new object[] {
            "3840x2160",
            "2560x1440",
            "1920x1080",
            "1600x900",
            "1366x768",
            "1280x720",
            "960x540",
            "640x360"});
            this.resolutionBox.Location = new System.Drawing.Point(7, 43);
            this.resolutionBox.Name = "resolutionBox";
            this.resolutionBox.Size = new System.Drawing.Size(159, 21);
            this.resolutionBox.TabIndex = 0;
            // 
            // fullscreenCheck
            // 
            this.fullscreenCheck.AutoSize = true;
            this.fullscreenCheck.Location = new System.Drawing.Point(6, 70);
            this.fullscreenCheck.Name = "fullscreenCheck";
            this.fullscreenCheck.Size = new System.Drawing.Size(74, 17);
            this.fullscreenCheck.TabIndex = 1;
            this.fullscreenCheck.Text = "Fullscreen";
            this.fullscreenCheck.UseVisualStyleBackColor = true;
            // 
            // ssaoCheck
            // 
            this.ssaoCheck.AutoSize = true;
            this.ssaoCheck.Location = new System.Drawing.Point(6, 19);
            this.ssaoCheck.Name = "ssaoCheck";
            this.ssaoCheck.Size = new System.Drawing.Size(55, 17);
            this.ssaoCheck.TabIndex = 2;
            this.ssaoCheck.Text = "SSAO";
            this.ssaoCheck.UseVisualStyleBackColor = true;
            // 
            // fxaaCheck
            // 
            this.fxaaCheck.AutoSize = true;
            this.fxaaCheck.Location = new System.Drawing.Point(6, 42);
            this.fxaaCheck.Name = "fxaaCheck";
            this.fxaaCheck.Size = new System.Drawing.Size(53, 17);
            this.fxaaCheck.TabIndex = 3;
            this.fxaaCheck.Text = "FXAA";
            this.fxaaCheck.UseVisualStyleBackColor = true;
            // 
            // bloomCheck
            // 
            this.bloomCheck.AutoSize = true;
            this.bloomCheck.Location = new System.Drawing.Point(6, 65);
            this.bloomCheck.Name = "bloomCheck";
            this.bloomCheck.Size = new System.Drawing.Size(77, 17);
            this.bloomCheck.TabIndex = 4;
            this.bloomCheck.Text = "Use Bloom";
            this.bloomCheck.UseVisualStyleBackColor = true;
            // 
            // advancedOptions
            // 
            this.advancedOptions.Controls.Add(this.ssaoCheck);
            this.advancedOptions.Controls.Add(this.fxaaCheck);
            this.advancedOptions.Controls.Add(this.bloomCheck);
            this.advancedOptions.Location = new System.Drawing.Point(206, 206);
            this.advancedOptions.Name = "advancedOptions";
            this.advancedOptions.Size = new System.Drawing.Size(139, 93);
            this.advancedOptions.TabIndex = 5;
            this.advancedOptions.TabStop = false;
            this.advancedOptions.Text = "Advanced";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(99, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(180, 180);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 6;
            this.pictureBox1.TabStop = false;
            // 
            // resolutionGroup
            // 
            this.resolutionGroup.Controls.Add(this.label1);
            this.resolutionGroup.Controls.Add(this.comboBox1);
            this.resolutionGroup.Controls.Add(this.vsyncBox);
            this.resolutionGroup.Controls.Add(this.resolutionBox);
            this.resolutionGroup.Controls.Add(this.fullscreenCheck);
            this.resolutionGroup.Location = new System.Drawing.Point(28, 206);
            this.resolutionGroup.Name = "resolutionGroup";
            this.resolutionGroup.Size = new System.Drawing.Size(172, 93);
            this.resolutionGroup.TabIndex = 7;
            this.resolutionGroup.TabStop = false;
            this.resolutionGroup.Text = "Resolution";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Aspect Ratio:";
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "4:3",
            "16:9",
            "16:10",
            "21:9"});
            this.comboBox1.Location = new System.Drawing.Point(92, 19);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(74, 21);
            this.comboBox1.TabIndex = 3;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // vsyncBox
            // 
            this.vsyncBox.AutoSize = true;
            this.vsyncBox.Location = new System.Drawing.Point(86, 70);
            this.vsyncBox.Name = "vsyncBox";
            this.vsyncBox.Size = new System.Drawing.Size(58, 17);
            this.vsyncBox.TabIndex = 2;
            this.vsyncBox.Text = "V-sync";
            this.vsyncBox.UseVisualStyleBackColor = true;
            // 
            // playButton
            // 
            this.playButton.Location = new System.Drawing.Point(141, 305);
            this.playButton.Name = "playButton";
            this.playButton.Size = new System.Drawing.Size(96, 44);
            this.playButton.TabIndex = 8;
            this.playButton.Text = "Play";
            this.playButton.UseVisualStyleBackColor = true;
            this.playButton.Click += new System.EventHandler(this.playButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(384, 361);
            this.Controls.Add(this.playButton);
            this.Controls.Add(this.resolutionGroup);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.advancedOptions);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(400, 400);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(400, 400);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Demons Run Launcher";
            this.advancedOptions.ResumeLayout(false);
            this.advancedOptions.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.resolutionGroup.ResumeLayout(false);
            this.resolutionGroup.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox resolutionBox;
        private System.Windows.Forms.CheckBox fullscreenCheck;
        private System.Windows.Forms.CheckBox ssaoCheck;
        private System.Windows.Forms.CheckBox fxaaCheck;
        private System.Windows.Forms.CheckBox bloomCheck;
        private System.Windows.Forms.GroupBox advancedOptions;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.GroupBox resolutionGroup;
        private System.Windows.Forms.Button playButton;
        private System.Windows.Forms.CheckBox vsyncBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBox1;
    }
}

