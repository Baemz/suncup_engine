#include "CommonStructs.hlsli"

cbuffer SpriteData : register(b0)
{
    float4 myPositionAndRotation;
    float4 myTextureSizeAndTargetSize;
	float4 myColor;
    float4 mySizeAndPivot;
    float4 myCrop;
}

float2x2 ComputeRotation(float aRotation)
{
    float c = cos(aRotation);
    float s = sin(aRotation);
    return float2x2(c, -s, s, c);
}

SpriteVertexToPixel VSMain(TexturedVertexInput input)
{
	SpriteVertexToPixel output;
    
    float2x2 rotation = ComputeRotation(myPositionAndRotation.z);

    float4 position = input.myPosition;
    position.w = 1.f;

    position.x -= (mySizeAndPivot.z * 2) - 1.f;
    position.y += (mySizeAndPivot.w * 2) - 1.f;

    position.xy *= float2(mySizeAndPivot.xy);
    position.xy = mul(rotation, position.xy);

    position.x *= myTextureSizeAndTargetSize.x / myTextureSizeAndTargetSize.z;
    position.y *= myTextureSizeAndTargetSize.y / myTextureSizeAndTargetSize.w;

    position.x += (myPositionAndRotation.x * 2) - 1.f;
    position.y += (myPositionAndRotation.y * 2) - 1.f;

    output.myPosition = position;
    output.myUV.x = min(max(input.myUV.x, myCrop.x), myCrop.z);
    output.myUV.y = min(max(input.myUV.y, myCrop.y), myCrop.w);

	output.myColor = myColor;

    return output;
};