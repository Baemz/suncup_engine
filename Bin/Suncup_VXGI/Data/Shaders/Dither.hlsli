#include "StandardBuffersPS.hlsli"

float nrand(in float2 n)
{
    return frac(sin(dot(n.xy, float2(12.9898f, 78.233f))) * 43758.5453f);
}

float n8rand(in float2 n)
{
    const float t = frac(threadTotalTime);
    const float nrnd0 = nrand(n + 0.07f * t);
    const float nrnd1 = nrand(n + 0.11f * t);
    const float nrnd2 = nrand(n + 0.13f * t);
    const float nrnd3 = nrand(n + 0.17f * t);
    const float nrnd4 = nrand(n + 0.19f * t);
    const float nrnd5 = nrand(n + 0.23f * t);
    const float nrnd6 = nrand(n + 0.29f * t);
    const float nrnd7 = nrand(n + 0.31f * t);
    
    return (nrnd0 + nrnd1 + nrnd2 + nrnd3 + nrnd4 + nrnd5 + nrnd6 + nrnd7) / 8.0f;
}

float4 Hash4(in float2 n)
{
    return float4(n8rand(n), n8rand(n), n8rand(n), n8rand(n));
}
float3 Hash3(in float2 n)
{
    return float3(n8rand(n), n8rand(n), n8rand(n));
}
float2 Hash2(in float2 n)
{
    return float2(n8rand(n), n8rand(n));
}
float Hash(in float2 n)
{
    return n8rand(n);
}

float4 Dither(in float4 aColor, in float2 aSeed)
{
    return aColor + Hash4(aSeed) / 255.f;
}

float Dither(in float aColor, in float2 aSeed)
{
    return aColor + Hash(aSeed) / 255.f;
}

float2 Dither(in float2 aColor, in float2 aSeed)
{
    return aColor + Hash2(aSeed) / 255.f;
}
float3 Dither(in float3 aColor, in float2 aSeed)
{
    return aColor + Hash3(aSeed) / 255.f;
}