#include "CommonStructs.hlsli"

SamplerState samplerState : register(s0);

Texture2D sceneTexture : register(t0);
Texture3D LUTTexture : register(t1);

cbuffer HalfPixelData : register(b0)
{
    float numberOfHalfPixels;
    float3 pad;
};

PixelOutput PS_FXColorGrading(TexturedVertexToPixel input)
{
    PixelOutput output;

    float4 sceneColor = sceneTexture.Sample(samplerState, input.myUV);
    float halfPixelSize = (1.0f / numberOfHalfPixels);
    sceneColor /= numberOfHalfPixels;
    sceneColor *= (numberOfHalfPixels - 2.0f);
    sceneColor += halfPixelSize;
    float4 correctedColor = LUTTexture.Sample(samplerState, sceneColor.rgb);
    
    output.myColor = float4(correctedColor.rgb, 1.f);
    return output;
}