#include "CommonStructs.hlsli"
#include "StandardBuffersVS.hlsli"

[maxvertexcount(24)]
void GSMain(point LineVertexToGeometry input[1], inout TriangleStream<LineVertexToGeometry> output)
{

	// calculate halfWidth and height to create cube vertices
	float halfWidth = 0.5f * input[0].myThickness;
	float halfHeight = 0.5f * input[0].myThickness;
	float halfDepth = 0.5f * input[0].myThickness;

	// create the 24 vertices of the cube
	float4 boxv[24];
	boxv[0] = float4(input[0].myPosition.x - halfWidth, input[0].myPosition.y + halfHeight, input[0].myPosition.z - halfDepth, 1.0f);
	boxv[1] = float4(input[0].myPosition.x + halfWidth, input[0].myPosition.y + halfHeight, input[0].myPosition.z - halfDepth, 1.0f);
	boxv[2] = float4(input[0].myPosition.x - halfWidth, input[0].myPosition.y - halfHeight, input[0].myPosition.z - halfDepth, 1.0f);
	boxv[3] = float4(input[0].myPosition.x + halfWidth, input[0].myPosition.y - halfHeight, input[0].myPosition.z - halfDepth, 1.0f);
	boxv[4] = float4(input[0].myPosition.x + halfWidth, input[0].myPosition.y + halfHeight, input[0].myPosition.z - halfDepth, 1.0f);
	boxv[5] = float4(input[0].myPosition.x + halfWidth, input[0].myPosition.y + halfHeight, input[0].myPosition.z + halfDepth, 1.0f);
	boxv[6] = float4(input[0].myPosition.x + halfWidth, input[0].myPosition.y - halfHeight, input[0].myPosition.z - halfDepth, 1.0f);
	boxv[7] = float4(input[0].myPosition.x + halfWidth, input[0].myPosition.y - halfHeight, input[0].myPosition.z + halfDepth, 1.0f);
	boxv[8] = float4(input[0].myPosition.x + halfWidth, input[0].myPosition.y + halfHeight, input[0].myPosition.z + halfDepth, 1.0f);
	boxv[9] = float4(input[0].myPosition.x - halfWidth, input[0].myPosition.y + halfHeight, input[0].myPosition.z + halfDepth, 1.0f);
	boxv[10] = float4(input[0].myPosition.x + halfWidth, input[0].myPosition.y - halfHeight, input[0].myPosition.z + halfDepth, 1.0f);
	boxv[11] = float4(input[0].myPosition.x - halfWidth, input[0].myPosition.y - halfHeight, input[0].myPosition.z + halfDepth, 1.0f);
	boxv[12] = float4(input[0].myPosition.x - halfWidth, input[0].myPosition.y + halfHeight, input[0].myPosition.z + halfDepth, 1.0f);
	boxv[13] = float4(input[0].myPosition.x - halfWidth, input[0].myPosition.y + halfHeight, input[0].myPosition.z - halfDepth, 1.0f);
	boxv[14] = float4(input[0].myPosition.x - halfWidth, input[0].myPosition.y - halfHeight, input[0].myPosition.z + halfDepth, 1.0f);
	boxv[15] = float4(input[0].myPosition.x - halfWidth, input[0].myPosition.y - halfHeight, input[0].myPosition.z - halfDepth, 1.0f);
	boxv[16] = float4(input[0].myPosition.x - halfWidth, input[0].myPosition.y + halfHeight, input[0].myPosition.z + halfDepth, 1.0f);
	boxv[17] = float4(input[0].myPosition.x + halfWidth, input[0].myPosition.y + halfHeight, input[0].myPosition.z + halfDepth, 1.0f);
	boxv[18] = float4(input[0].myPosition.x - halfWidth, input[0].myPosition.y + halfHeight, input[0].myPosition.z - halfDepth, 1.0f);
	boxv[19] = float4(input[0].myPosition.x + halfWidth, input[0].myPosition.y + halfHeight, input[0].myPosition.z - halfDepth, 1.0f);
	boxv[20] = float4(input[0].myPosition.x - halfWidth, input[0].myPosition.y - halfHeight, input[0].myPosition.z - halfDepth, 1.0f);
	boxv[21] = float4(input[0].myPosition.x + halfWidth, input[0].myPosition.y - halfHeight, input[0].myPosition.z - halfDepth, 1.0f);
	boxv[22] = float4(input[0].myPosition.x - halfWidth, input[0].myPosition.y - halfHeight, input[0].myPosition.z + halfDepth, 1.0f);
	boxv[23] = float4(input[0].myPosition.x + halfWidth, input[0].myPosition.y - halfHeight, input[0].myPosition.z + halfDepth, 1.0f);

	// indices 6 quads * 4 vertices
	int index[24] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23 };

	// after each quad, restart the strip
	LineVertexToGeometry vertex;
	for (int i = 1; i < 25; ++i)
	{
		vertex.myPosition.xyz = boxv[i - 1].xyz;
		vertex.myPosition.w = 1.f;
        vertex.myPosition = mul(cameraView, vertex.myPosition);
        vertex.myPosition = mul(cameraProjection, vertex.myPosition);
		vertex.myColor = input[0].myColor;
		vertex.myThickness = input[0].myThickness;
		output.Append(vertex);

		if (i % 4 == 0) output.RestartStrip();
	}
}






