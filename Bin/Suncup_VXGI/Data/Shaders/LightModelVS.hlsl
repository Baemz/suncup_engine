#include "CommonStructs.hlsli"
#include "StandardBuffersVS.hlsli"

LightVertexToPixel VSMain(LightVertexInputInstanced input)
{
    LightVertexToPixel output;

    float4x4 toWorld = float4x4(input.myToWorld, input.myToWorld2, input.myToWorld3, input.myToWorld4);
    output.myPosition = mul(cameraProjection, mul(cameraView, mul(float4(input.myPosition.xyz, 1.f), toWorld)));
    output.myUV = float4((float2(output.myPosition.x + output.myPosition.w, output.myPosition.w - output.myPosition.y)) / 2, output.myPosition.zw).xy;
    output.myInstancePosition = input.myToWorld4.xyz;
    output.myIntensity = input.myIntensity;
    output.myLightColor = input.myLightColor.rgb;
    output.myLightDirection = input.myLightDirection.xyz;
    output.myRange = input.myRange;
    output.myAngle = input.myAngle;

    return output;
};