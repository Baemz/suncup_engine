#include "GI_Decl.hlsli"

static const float3 CONES[] =
{
    float3(0.57735, 0.57735, 0.57735),
	float3(0.57735, -0.57735, -0.57735),
	float3(-0.57735, 0.57735, -0.57735),
	float3(-0.57735, -0.57735, 0.57735),
	float3(-0.903007, -0.182696, -0.388844),
	float3(-0.903007, 0.182696, 0.388844),
	float3(0.903007, -0.182696, 0.388844),
	float3(0.903007, 0.182696, -0.388844),
	float3(-0.388844, -0.903007, -0.182696),
	float3(0.388844, -0.903007, 0.182696),
	float3(0.388844, 0.903007, -0.182696),
	float3(-0.388844, 0.903007, 0.182696),
	float3(-0.182696, -0.388844, -0.903007),
	float3(0.182696, 0.388844, -0.903007),
	float3(-0.182696, 0.388844, 0.903007),
	float3(0.182696, -0.388844, 0.903007)
};

static const float MAX_DIST = 100.f;
static const float PI = 3.14159265359f;
SamplerState VXGISampler : register(s10);

inline float4 ConeTrace(in Texture3D<float4> voxels, in float3 aPosition, in float3 aNormal, in float3 aConeDirection, in float aConeAperture)
{
    float3 color = 0;
    float alpha = 0;
    float currentDistance = VoxelSize;
    const float3 startPos = aPosition + aNormal * VoxelSize * 2;
    const float maxDistance = MAX_DIST * VoxelSize;
    while (currentDistance < maxDistance && alpha < 1)
    {
        const float diameter = max(VoxelSize, 2 * aConeAperture * currentDistance);
        const float mip = log2(diameter * VoxelSizeInversed);

        float3 currentConePosition = startPos + aConeDirection * currentDistance;
        currentConePosition = (((((currentConePosition - VoxelCenter) * VoxelSizeInversed) * VoxelGridSizeInversed) * float3(0.5f, -0.5f, 0.5f)) + 0.5f);
        
        if (any(currentConePosition - saturate(currentConePosition)) || mip >= (float) VoxelRadianceMips)
        {
            break;
        }

        const float4 radianceSample = voxels.SampleLevel(VXGISampler, currentConePosition, mip);
        float a = 1 - alpha;
        color += a * radianceSample.rgb;
        alpha += a * radianceSample.a;
        currentDistance += diameter * 0.6f;
    }

    return float4(color, alpha);
}

inline float ConeTraceAO(in Texture3D<float4> voxels, in float3 aPosition, in float3 aNormal, in float3 aConeDirection, in float aConeAperture)
{
    float alpha = 0;
    float currentDistance = VoxelSize;
    const float3 startPos = aPosition + aNormal * VoxelSize * 2;
    const float maxDistance = MAX_DIST * VoxelSize;
    while (currentDistance < maxDistance && alpha < 1)
    {
        const float diameter = max(VoxelSize, 2 * aConeAperture * currentDistance);
        const float mip = log2(diameter * VoxelSizeInversed);

        float3 currentConePosition = startPos + aConeDirection * currentDistance;
        currentConePosition = (((((currentConePosition - VoxelCenter) * VoxelSizeInversed) * VoxelGridSizeInversed) * float3(0.5f, -0.5f, 0.5f)) + 0.5f);
        
        if (any(currentConePosition - saturate(currentConePosition)) || mip >= (float) VoxelRadianceMips)
        {
            break;
        }

        const float4 radianceSample = voxels.SampleLevel(VXGISampler, currentConePosition, mip);
        float a = 1 - alpha;
        alpha += a * radianceSample.a;
        currentDistance += diameter * 0.6f;
    }

    return currentDistance;
}

inline float4 ConeTraceRadiance(in Texture3D<float4> aVoxelRadiance, in float3 aPosition, in float3 aNormal)
{
    float4 radiance = 0;
    float ambientFactor = 0.f;
    [unroll]
    for (uint cone = 0; cone < VOXEL_CONE_QUALITY; ++cone)
    {
        float3 coneDirection = reflect(CONES[cone], aNormal);
        coneDirection *= (dot(coneDirection, aNormal) < 0) ? -1 : 1;
        radiance += ConeTrace(aVoxelRadiance, aPosition, aNormal, coneDirection, tan(PI * ConeAngleDiffuse));
        
    }
    
    ambientFactor = 1.0f - saturate(ambientFactor);

    radiance *= VOXEL_CONE_QUALITY_INVERSE;
    radiance.a = saturate(radiance.a);
    
    return max(0, radiance);
}

inline float4 ConeTraceReflection(in Texture3D<float4> aVoxelRadiance, in float3 aPosition, in float3 aNormal, in float3 aViewDir, in float aRoughness)
{
    const float aperture = tan(aRoughness * PI * ConeAngleSpecular);
    const float3 coneDirection = reflect(-aViewDir, aNormal);
    
    const float4 reflection = ConeTrace(aVoxelRadiance, aPosition, aNormal, coneDirection, aperture);
    return float4(max(0, reflection.rgb), saturate(reflection.a));
}

inline float ConeTraceAmbientOcclusion(in Texture3D<float4> aVoxelRadiance, in float3 aPosition, in float3 aNormal)
{
    float ao = 0;

    [unroll]
    for (uint cone = 0; cone < VOXEL_CONE_QUALITY; ++cone)
    {
        float3 coneDirection = reflect(CONES[cone], aNormal);
        coneDirection *= (dot(coneDirection, aNormal) < 0) ? -1 : 1;
        ao += ConeTraceAO(aVoxelRadiance, aPosition, aNormal, coneDirection, tan(PI * ConeAngleDiffuse));
        
    }
    ao = 1.0f - saturate(ao);
    ao *= VOXEL_CONE_QUALITY_INVERSE;
    ao = saturate(ao);
    
    return max(0, ao);
}

