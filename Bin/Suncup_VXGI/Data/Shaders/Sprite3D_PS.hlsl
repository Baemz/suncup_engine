#include "CommonStructs.hlsli"

Texture2D instanceTexture : register(t0);
SamplerState instanceSampler : register(s0);

cbuffer SpriteGlow : register(b0)
{
	float myGlowIntensity;
	float3 myPadding;
}

PixelOutput PSMain(SpriteVertexToPixel input)
{
    PixelOutput output;

	output.myColor = instanceTexture.Sample(instanceSampler, input.myUV);
	//output.myColor.rgb += input.myColor.rgb;
	//output.myColor.a *= input.myColor.a;
    //
	//if (output.myColor.a > 0.0f)
	//{
	//	output.myColor += myGlowIntensity;
	//}
    
    return output;
};