#include "CommonStructs.hlsli"

TextureCube skyboxTexture : register(t0);
SamplerState instanceSampler : register(s0);

PixelOutput PSMain(SkyboxVertexToPixel input)
{
    PixelOutput output;
    output.myColor = skyboxTexture.SampleLevel(instanceSampler, input.myToEye, 0);
    return output;
};