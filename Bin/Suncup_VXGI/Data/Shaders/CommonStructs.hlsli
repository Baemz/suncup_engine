
struct ParticleInput
{
	float4 myPosition : POSITION;
	float4 myColor : COLOR;
    float mySize : PSIZE;
    float myRotation : PROTATION;
	float myLifetime : EMPTYFLOAT;
    float3 myDirection : PARTICLEDIRECTION;
    bool myGradient : PARTICLEGRADIENT;
    float myPRotationVel : PARTICLERVELOCITY;
};

struct ParticleToGeometry
{
    float4 myPosition : SV_POSITION;
    float4 myWPosition : POSITION;
	float4 myColor : COLOR;
    float mySize : PSIZE;
    float myRotation : PROTATION;
};

struct GeometryToPixel
{
	float4 myPosition : SV_POSITION;
	float4 myColor : COLOR;
	float2 myUV : UV;
};

struct ColoredVertexInput
{
    float4 myPosition : POSITION;
    float4 myColor : COLOR;
};


struct LightVertexInputInstanced
{
    float4 myPosition : POSITION;
    float4 myColor : COLOR;
    float4 myToWorld : TEXCOORD1;
    float4 myToWorld2 : TEXCOORD2;
    float4 myToWorld3 : TEXCOORD3;
    float4 myToWorld4 : TEXCOORD4;
    float4 myLightColor : TEXCOORD5;
    float4 myLightDirection : TEXCOORD8;
    float myRange : TEXCOORD6;
    float myIntensity : TEXCOORD7;
    float myAngle : TEXCOORD9;
};

struct LightVertexToPixel
{
    float4 myPosition : SV_POSITION;
    float3 myInstancePosition : INSTANCEPOSITION;
    float3 myLightColor : INSTANCECOLOR;
    float3 myLightDirection : INSTANCEDIRECTION;
    float2 myUV : UV;
    float myRange : INSTANCERANGE;
    float myIntensity : INSTANCEINTENSITY;
    float myAngle : INSTANCEANGLE;
};

struct ColoredVertexToPixel
{
	float4 myPosition : SV_POSITION;
	float4 myColor : COLOR;
};

struct LineVertexInput
{
	float4 myPosition : POSITION;
	float4 myColor : COLOR;
	float myThickness : PSIZE;
};

struct LineVertexToGeometry
{
	float4 myPosition : SV_POSITION;
	float4 myColor : COLOR;
	float myThickness : PSIZE;
};

struct TexturedVertexInput
{
    float4 myPosition : POSITION;
    float2 myUV : UV;
};

struct TexturedVertexToPixel
{
    float4 myPosition : SV_POSITION;
    float2 myUV : UV;
};

struct PBLVertexInput
{
    float4 myPosition : POSITION;
    float4 myNormals : NORMAL;
    float4 myTangents : TANGENT;
    float4 myBinormal : BINORMAL;
    float2 myUV : UV;
    float4 myBones : BONE;
    float4 myWeights : WEIGHT;
};

struct PBLVertexToPixel
{
    float3x3 myTangentSpaceMatrix : TANGENTSPACEMATRIX;
    float4 myPosition : SV_POSITION;
    float3 myWPosition : POSITION;
    float3 myViewPos : VIEWPOS;
    float2 myUV : UV;
    bool myUseFog : USEFOG;
};

struct SkyboxVertexToPixel
{
    float4 myPosition : SV_POSITION;
	float3 myWPosition : POSITION;
    float3 myToEye : TOEYE;
    float2 myUV : UV;
};

struct SpriteVertexToPixel
{
	float4 myPosition : SV_POSITION;
	float4 myColor : COLOR;
	float2 myUV : UV;
};

struct PixelOutput
{
    float4 myColor : SV_Target;
};

struct DeferredDataOutput
{ 
    //RGB10A2
    float4 myNormal : SV_TARGET0; 
    //RGBA8
    float4 myAlbedoAndRoughness : SV_TARGET1;
    float4 myEmissiveAndMetallic : SV_TARGET2;
    float4 myAOAndFogFactor : SV_TARGET3;
};