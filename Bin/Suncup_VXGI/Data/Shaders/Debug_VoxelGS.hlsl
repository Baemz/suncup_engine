#include "GI_Decl.hlsli"
#include "Debug_Cube.hlsli"
#include "StandardBuffersVS.hlsli"

struct GSOutput
{
    float4 pos : SV_POSITION;
    float4 col : TEXCOORD;
};

[maxvertexcount(14)]
void GSMain(point GSOutput input[1], inout TriangleStream<GSOutput> output)
{
    float4x4 vpMat = mul(cameraProjection, cameraView);
    [branch]
    if (input[0].col.a > 0)
    {
        for (uint i = 0; i < 14; i++)
        {
            GSOutput element;
            element.pos = input[0].pos;
            element.col = input[0].col;

            element.pos.xyz = element.pos.xyz / VoxelGridSize * 2 - 1;
            element.pos.y = -element.pos.y;
            element.pos.xyz *= VoxelGridSize;
            element.pos.xyz += (CreateCube(i) - float3(0, 1, 0)) * 2;
            element.pos.xyz *= VoxelGridSize * VoxelSize / VoxelGridSize;
            
            element.pos = mul(vpMat, element.pos);

            output.Append(element);
        }
        output.RestartStrip();
    }
}