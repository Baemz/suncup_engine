#include "VoxelConeTrace.hlsli"
#include "Dither.hlsli"

SamplerState samplerState : register(s0);

Texture2D depthTexture : register(t0); // or Albedo
Texture2D normalTexture : register(t1); // or Normals
Texture2D diffuseAndRoughnessTexture : register(t2); // or Roughness
Texture2D emissiveAndMetallicTexture : register(t3); // or Metallic
Texture2D ambientOcclusionTexture : register(t4); // or Emissive
Texture3D<float4> voxelRadiance : register(t10);

struct PBRData
{
    float3 lightColor;
    float3 lambert;
    float3 fresnel;
    float3 reflectionFresnel;
    float distribution;
    float visibility;
};

struct Attributes
{
    float4 diffuseAndRoughness;
    float4 ambientOcclusionAndFogFactor;
    float4 metallic;
    float4 emissive;
    float4 objectNormal;
    float4 worldPosition;
    float4 toEye;
};
static const float3 ambientPower = float3(0.04f, 0.04f, 0.04f);

float3 GetLambert(in Attributes attributes, in float3 toLight)
{
	// Lambert
    const float3 lightDir = normalize(toLight.xyz);
    return (saturate(dot(attributes.objectNormal.xyz, lightDir)));
}


float3 GetFresnel(in Attributes attributes, in float3 toLight)
{
    const float3 substance = (ambientPower - (ambientPower * attributes.metallic.rgb)) + attributes.diffuseAndRoughness.rgb * attributes.metallic.rgb;
    const float3 halfvec = normalize(toLight + attributes.toEye.xyz);
    float LdotH = dot(toLight, halfvec);
    LdotH = pow(1.0f - saturate(LdotH), 5);
    float3 fresnel = LdotH * (1.f - substance);
    fresnel = substance + fresnel;

    return max(0.f, fresnel);
}

float3 GetReflectionFresnel(in Attributes attributes)
{
	// Reflection-fresnel
    float VdotN = dot(attributes.toEye.xyz, attributes.objectNormal.xyz);
    VdotN = pow(1.0f - saturate(VdotN), 5);

    const float3 substance = (ambientPower - (ambientPower * attributes.metallic.rgb)) + attributes.diffuseAndRoughness.rgb * attributes.metallic.rgb;
    float3 refFresnel = VdotN * (1.f - substance);
    refFresnel = refFresnel / (6 - 5 * attributes.diffuseAndRoughness.a);
    refFresnel = substance + refFresnel;

    return max(0.f, refFresnel);
}

float GetDistribution(in Attributes attributes, in float3 toLight)
{
	// Distribution
    const float3 halfvec = normalize(toLight + attributes.toEye.xyz);
    const float HdotN = saturate(dot(halfvec, attributes.objectNormal.xyz));
    const float m = attributes.diffuseAndRoughness.a * attributes.diffuseAndRoughness.a;
    const float m2 = m * m;
    const float denominator = HdotN * HdotN * (m2 - 1.f) + 1.f;
    const float distribution = m2 / (3.14159f * denominator * denominator);

    return distribution;
}

float GetVisibility(in Attributes attributes, in float3 toLight)
{
	// Visability
    const float roughnessRemapped = (attributes.diffuseAndRoughness.a + 1.f) / 2.f;
    const float NdotL = saturate(dot(attributes.objectNormal.xyz, toLight));
    const float NdotV = saturate(dot(attributes.objectNormal.xyz, attributes.toEye.xyz));
    const float k = roughnessRemapped * roughnessRemapped * 1.7724f;
    const float G1V = NdotV * (1.f - k) + k;
    const float G1L = NdotL * (1.f - k) + k;
    const float visability = (NdotV * NdotL) / (G1V * G1L);

    return visability;
}

float3 GetDirectDiffuse(in Attributes attributes, in PBRData pbrData)
{
    const float3 metallicAlbedo = attributes.diffuseAndRoughness.rgb - (attributes.diffuseAndRoughness.rgb * attributes.metallic.rgb);
    const float3 returnColor = metallicAlbedo * pbrData.lightColor * pbrData.lambert * (float3(1.f, 1.f, 1.f) - pbrData.fresnel);
    return saturate(returnColor);

}

float3 GetDirectSpecular(in Attributes attributes, in PBRData pbrData)
{
    return saturate(float3(pbrData.lightColor * pbrData.lambert * pbrData.fresnel * pbrData.distribution * pbrData.visibility));
}

inline void VoxelRadiance(in Attributes surface, in PBRData pbrData, inout float3 diffuse, inout float3 specular, inout float ao)
{
	[branch]
    if (VoxelGridSize != 0)
    {
        float3 voxelSpacePos = (surface.worldPosition.xyz - VoxelCenter);
        voxelSpacePos = (voxelSpacePos * VoxelSizeInversed) * VoxelGridSizeInversed;
        voxelSpacePos = saturate(abs(voxelSpacePos));
        const float blend = 1 - pow(max(voxelSpacePos.x, max(voxelSpacePos.y, voxelSpacePos.z)), 4);

        float4 radiance = ConeTraceRadiance(voxelRadiance, surface.worldPosition.xyz, surface.objectNormal.xyz);
        diffuse += lerp(0, radiance.rgb, blend);
        ao *= 1 - lerp(0, radiance.a, blend);
        
        float4 reflection = ConeTraceReflection(voxelRadiance, surface.worldPosition.xyz, surface.objectNormal.xyz, surface.toEye.xyz, surface.diffuseAndRoughness.a);
        specular = lerp(specular, reflection.rgb, reflection.a * blend);
    }
}