#include "CommonStructs.hlsli"
#include "StandardBuffersVS.hlsli"

cbuffer DepthMVP : register(b5)
{
    float4x4 depthProj;
    float4x4 depthView;
    float4x4 objInv;
}

TexturedVertexToPixel VSMain(PBLVertexInput input)
{
	TexturedVertexToPixel output;

    float4 vertexObjectPos;
    if (useAnimations == 1)
    {
        float4 vertexWeights = input.myWeights;
        uint4 vertexBones = uint4((uint) input.myBones.x, (uint) input.myBones.y, (uint) input.myBones.z, (uint) input.myBones.w);
        float4x4 finalMatrix = mul(modelBones[vertexBones.x], vertexWeights.x)
                             + mul(modelBones[vertexBones.y], vertexWeights.y)
                             + mul(modelBones[vertexBones.z], vertexWeights.z)
                             + mul(modelBones[vertexBones.w], vertexWeights.w);
        vertexObjectPos = mul(finalMatrix, float4(input.myPosition.xyz, 1.0f));
    }
    else
    {
        vertexObjectPos = float4(input.myPosition.xyz, 1);
    }

    output.myPosition = mul(depthProj, mul(depthView, mul(objInv, vertexObjectPos)));
	return output;
}

PixelOutput PSMain(TexturedVertexToPixel input)
{
	PixelOutput output;
    output.myColor.r = input.myPosition.z;
	output.myColor.g = 0.f;
	output.myColor.b = 0.f;
	output.myColor.a = 0.f;
	return output;
}