#include "CommonStructs.hlsli"
#include "StandardBuffersVS.hlsli"

TexturedVertexToPixel VSMain(TexturedVertexInput input)
{
    TexturedVertexToPixel output;
    float4 vertexProjectionPos = mul(cameraProjection, mul(cameraView, mul(toWorld, float4(input.myPosition.xyz, 1))));

    output.myPosition = vertexProjectionPos;
    output.myUV = input.myUV;

    return output;
};