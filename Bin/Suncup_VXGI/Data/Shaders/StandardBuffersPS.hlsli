#pragma once

cbuffer TimeData : register(b0)
{
    float threadDeltaTime;
    float threadTotalTime;
    float applicationTotalTime;
}

cbuffer CameraData : register(b1)
{
    float4x4 cameraOrientation;
    float4x4 cameraView;
    float4x4 cameraProjection;
    float4 cameraPosition;
}

cbuffer InverseCameraData : register(b2)
{
    float4x4 inverseCameraView;
    float4x4 inverseCameraProjection;
}