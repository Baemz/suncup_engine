#include "CommonStructs.hlsli"
#include "StandardBuffersVS.hlsli"

[maxvertexcount(2)]
void GSMain(line LineVertexToGeometry input[2], inout LineStream<LineVertexToGeometry> output)
{
	for (int i = 0; i < 2; i++)
	{
		LineVertexToGeometry vertex;
        vertex.myPosition = float4(input[i].myPosition.xyz, 1.f);
        vertex.myPosition = mul(cameraProjection, mul(cameraView, vertex.myPosition));
		vertex.myColor = input[i].myColor;
		vertex.myThickness = input[i].myThickness;

		output.Append(vertex);
	}

	output.RestartStrip();
}