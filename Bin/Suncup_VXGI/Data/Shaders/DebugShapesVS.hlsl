#include "CommonStructs.hlsli"

LineVertexToGeometry VSMain(LineVertexInput input)
{
	LineVertexToGeometry output;

	output.myPosition = input.myPosition;
	output.myColor = input.myColor;
	output.myThickness = input.myThickness;

	return output;
};