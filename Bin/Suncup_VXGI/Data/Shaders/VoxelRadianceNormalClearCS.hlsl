#include "GI_Decl.hlsli"
RWStructuredBuffer<VoxelizedEncodedData> encodedRadiance : register(u0);

[numthreads(256, 1, 1)]
void CSMain(uint3 dispThreadID : SV_DispatchThreadID)
{
    encodedRadiance[dispThreadID.x].norm = 0;
}