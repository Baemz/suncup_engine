#include "CommonStructs.hlsli"
#ifdef FX_TONEMAP
#include "Dither.hlsli"
#else
#include "StandardBuffersPS.hlsli"
#endif
Texture2D fullscreenTexture0 : register(t0);
Texture2D fullscreenTexture1 : register(t1);
SamplerState samplerState : register(s0);

cbuffer Resolution : register(b3)
{
	float2 myResolution;
}

cbuffer Settings : register(b4)
{
	float bloomCutoff;
    float bloomIntensity;
    uint blurLevel;
}

cbuffer GlitchData : register(b5)
{
	float2 resolution;
	float elapsedTime;
	float deltaTime;
}


static const float3 relativeLuminance = float3(0.2126f, 0.7152f, 0.0722f);

#ifdef FX_VERTEX
TexturedVertexToPixel VSMain(TexturedVertexInput input)
{
	TexturedVertexToPixel output;
	output.myPosition = input.myPosition;
	output.myUV = input.myUV;
	return output;
}
#endif

#ifdef FX_ADD
PixelOutput PS_FXAdd(TexturedVertexToPixel input)
{
    PixelOutput output;
    float3 tex1 = fullscreenTexture0.Sample(samplerState, input.myUV).rgb;
    float3 tex2 = fullscreenTexture1.Sample(samplerState, input.myUV).rgb;

    output.myColor = saturate(float4(tex1.rgb + tex2.rgb, 1.f));
    return output;
}
#endif

#ifdef FX_BLOOMADD
float4 AdjustSaturation(float4 color, float saturation)
{
    float grey = dot(color.rgb, float3(0.3, 0.59, 0.11));
   
    return lerp(grey, color, saturation);
}

PixelOutput PS_FXBloomAdd(TexturedVertexToPixel input)
{
	PixelOutput output;
	float3 glow = fullscreenTexture1.Sample(samplerState, input.myUV).rgb * bloomIntensity;
	float3 resource = fullscreenTexture0.Sample(samplerState, input.myUV).rgb * (1.0f - saturate(glow));
    output.myColor = float4(resource + (glow), 1.f);
	return output;
}
#endif


#ifdef FX_COPY
PixelOutput PS_FXCopy(TexturedVertexToPixel input)
{
	PixelOutput output;
	output.myColor.rgb = fullscreenTexture0.Sample(samplerState, input.myUV);
	return output;
}
#endif

#ifdef FX_LUMINANCE
PixelOutput PS_FXLuminance(TexturedVertexToPixel input)
{
	PixelOutput output;
	float3 resource = fullscreenTexture0.Sample(samplerState, input.myUV).rgb;

	float luminance = resource.r * relativeLuminance.r + resource.g * relativeLuminance.g + resource.b * relativeLuminance.b;
	luminance = saturate(luminance - bloomCutoff);

	output.myColor = float4(resource * luminance * (1 / bloomCutoff), 1.f);
	return output;
}
#endif

static const uint kernelSizes[3] = { 7, 13, 19 };
static const float weightList[57] =
{
    // Level 0
    0.121597, 0.142046, 0.155931, 0.160854, 0.155931, 0.142046, 0.121597, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,

    // Level 1
    0.002216, 0.008764, 0.026995, 0.064759, 0.120985, 0.176033, 0.199471, 0.176033, 0.120985, 0.064759, 0.026995, 0.008764, 0.002216, 0, 0, 0, 0, 0, 0, 
    
    // Level 2
    0.008162, 0.013846, 0.022072, 0.033065, 0.046546, 0.061573, 0.076542, 0.089414, 0.098154, 0.101253,
    0.098154, 0.089414, 0.076542, 0.061573, 0.046546, 0.033065, 0.022072, 0.013846, 0.008162
    
};
static const float texelOffsetList[57] =
{
    // Level 0
    -3, -2, -1, 0, +1, +2, +3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    
    // Level 1
    -6, -5, -4, -3, -2, -1, 0, +1, +2, +3, +4, +5, +6, 0, 0, 0, 0, 0, 0,
    
    // Level 2
    -9, -8, -7, -6, -5, -4, -3, -2, -1, 0, +1, +2, +3, +4, +5, +6, +7, +8, +9

};

#ifdef FX_GBLURH
static const float texelSizeH = 1.f / myResolution.x;
PixelOutput PS_FXGaussianBlurH(TexturedVertexToPixel input)
{
	PixelOutput output;
	float3 blurColor = (float3)0;
	for (uint i = 0; i < kernelSizes[blurLevel]; ++i)
	{
		float2 textureCoords = input.myUV;
        textureCoords.x = textureCoords.x + texelSizeH * texelOffsetList[(blurLevel * 19) + i];
		float3 resource = fullscreenTexture0.Sample(samplerState, textureCoords).rgb;
        blurColor += saturate(resource * (weightList[(blurLevel * 19) + i]));
    }

    output.myColor = float4(blurColor, 1.f);
	return output;
}
#endif

#ifdef FX_GBLURV
static const float texelSizeV = 1.f / myResolution.y;
PixelOutput PS_FXGaussianBlurV(TexturedVertexToPixel input)
{
	PixelOutput output;
	float3 blurColor = (float3)0;

    for (uint i = 0; i < kernelSizes[blurLevel]; ++i)
	{
		float2 textureCoords = input.myUV;
        textureCoords.y = textureCoords.y + texelSizeV * texelOffsetList[(blurLevel * 19) + i];
		float3 resource = fullscreenTexture0.Sample(samplerState, textureCoords).rgb;
        blurColor += saturate(resource * (weightList[(blurLevel * 19) + i]));
    }

    output.myColor = float4(blurColor, 1.f);
	return output;
}
#endif

#ifdef FX_TONEMAP
float CalculateLuminance(float3 aColor)
{
	float3 color = aColor * relativeLuminance;
	float luminance = max(0.f, (color.r + color.g + color.b));
	
	color = color * luminance;
	float lum = length(color);
	return lum;
};

float CalcLuminance(float3 color) {
    return max(dot(color, float3(0.299f, 0.587f, 0.114f)), 0.0001f);
}

static const float tonemapExposure = 20.f;
static const float2 uv = float2(0.5f, 0.5f);
PixelOutput PS_FXTonemap(TexturedVertexToPixel input)
{
    PixelOutput output;
    float4 textureSample = fullscreenTexture0.Sample(samplerState, input.myUV);
	float average = fullscreenTexture1.Sample(samplerState, uv).r;

	//Tonemap and exposure
	float LP = CalculateLuminance(textureSample.rgb) * tonemapExposure / average;
    output.myColor = float4((textureSample * (LP / (1 + LP))).rgb, 1.0f);
    output.myColor = Dither(output.myColor, input.myUV);

    return output;
}
#endif

#ifdef FX_TONEMAPLUM
PixelOutput PS_FXTonemapLuminance(TexturedVertexToPixel input)
{
    PixelOutput output;
	float3 resource = fullscreenTexture0.Sample(samplerState, input.myUV).rgb;
	float luminance = max(0.f, resource.r * relativeLuminance.r + resource.g * relativeLuminance.g + resource.b * relativeLuminance.b);
	output.myColor = float4((resource * luminance), 1.f);
	return output;
}
#endif