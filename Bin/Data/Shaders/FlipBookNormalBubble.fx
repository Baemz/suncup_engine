// ----------------------------------------- Header ------------------------------------------
#include "CommonStructs.hlsli"
#include "StandardBuffersVS.hlsli"

#ifndef SFX_HLSL_5
	#define SFX_HLSL_5
#endif 
#ifndef _MAYA_
	#define _MAYA_
#endif 



float2 ShadowFilterTaps[10]
<
	string UIWidget = "None";
> = {float2(-0.840520, -0.073954), float2(-0.326235, -0.405830), float2(-0.698464, 0.457259), float2(-0.203356, 0.620585), float2(0.963450, -0.194353), float2(0.473434, -0.480026), float2(0.519454, 0.767034), float2(0.185461, -0.894523), float2(0.507351, 0.064963), float2(-0.321932, 0.595435)};


cbuffer SHADERFX : register(b10)
{
    float FramesPerSec = 24.000000;
    int ClampDynamicLights = 99;
};

// ---------------------------------------- Textures -----------------------------------------
Texture2D BOBBELSdds : register(t0)
<
	string ResourceName = "BOBBELS.dds";
	string UIName = "BOBBELSdds";
	string ResourceType = "2D";
	string UIWidget = "FilePicker";
>;

SamplerState MMMLWWWSampler
{
	Filter = MIN_MAG_MIP_LINEAR;
	AddressU = WRAP;
	AddressV = WRAP;
	AddressW = WRAP;
};

Texture2D Light0ShadowMap : SHADOWMAP
<
	string ResourceName = "";
	string ResourceType = "2D";
	string UIWidget = "None";
	string Object =  "Light 0";
>;

Texture2D Light1ShadowMap : SHADOWMAP
<
	string ResourceName = "";
	string ResourceType = "2D";
	string UIWidget = "None";
	string Object =  "Light 1";
>;

Texture2D Light2ShadowMap : SHADOWMAP
<
	string ResourceName = "";
	string ResourceType = "2D";
	string UIWidget = "None";
	string Object =  "Light 2";
>;

SamplerState Light0ShadowMapSampler : SHADOWMAPSAMPLER
{
	Filter = MIN_MAG_MIP_POINT;
	AddressU = BORDER;
	AddressV = BORDER;
	AddressW = BORDER;
	BorderColor = float4(1.000000,1.000000,1.000000,1.000000);
};

Texture2D TransDepthTexture : transpdepthtexture
<
	string ResourceName = "";
	string UIName = "TransDepthTexture";
	string ResourceType = "2D";
	string UIWidget = "None";
>;

Texture2D OpaqueDepthTexture : opaquedepthtexture
<
	string ResourceName = "";
	string UIName = "OpaqueDepthTexture";
	string ResourceType = "2D";
	string UIWidget = "None";
>;

SamplerState MMMPBBW_255_255_255_255_Sampler
{
	Filter = MIN_MAG_MIP_POINT;
	AddressU = BORDER;
	AddressV = BORDER;
	AddressW = WRAP;
	BorderColor = float4(1.000000,1.000000,1.000000,1.000000);
};


// ---------------------------------------- Functions -----------------------------------------

struct FlipBookOutput 
{ 
	float2 UV; 
}; 

// HLSL fmod and GLSL mod are not identical, but since we only use it for positive number here, this is probably ok
#if defined(SFX_GLSL_1_2) || defined(SFX_GLSL_4) || defined(SFX_OGSFX)
	#define fmod mod
#endif

FlipBookOutput FlipBookFunc( float frame_per_sec, float time, float sprite_per_row, float sprite_per_col, float2 uv ) 
{ 
	FlipBookOutput OUT; 

	float current_frame = floor( fmod(frame_per_sec * time, sprite_per_row * sprite_per_col) );

	float spriteU = fmod(current_frame, sprite_per_row) / sprite_per_row;
	float spriteV = floor(current_frame / sprite_per_row) / sprite_per_col;

	// add local UV offset
	spriteU += uv.x / sprite_per_row;
	spriteV += uv.y / sprite_per_col;

	OUT.UV = float2( spriteU, spriteV );
	return OUT; 
} 


Texture2D GetLightShadowMap(int ActiveLightIndex) 
{ 
	if (ActiveLightIndex == 0) 
		return Light0ShadowMap; 
	else if (ActiveLightIndex == 1) 
		return Light1ShadowMap; 
	else 
		return Light2ShadowMap; 
}

float4 SampleFromShadowMap( int ActiveLightIndex, float2 UVs) 
{ 
	if (ActiveLightIndex == 0) 
		return Light0ShadowMap.SampleLevel(Light0ShadowMapSampler, UVs, 0); 
	else if (ActiveLightIndex == 1) 
		return Light1ShadowMap.SampleLevel(Light0ShadowMapSampler, UVs, 0); 
	else 
		return Light2ShadowMap.SampleLevel(Light0ShadowMapSampler, UVs, 0); 
}

float4 sampleTransDepthTex(float2 UV)
{
	float4 col = float4(0,0,0,0);

	#if defined(SFX_CGFX_3) || defined(SFX_HLSL_3)
		col = tex2D( MMMPBBW_255_255_255_255_Sampler, UV );
	#endif
	#ifdef SFX_HLSL_5
		#if defined(SFX_SWATCH) || defined(_3DSMAX_)
			col = TransDepthTexture.Sample( MMMPBBW_255_255_255_255_Sampler, UV );
		#else
			col = TransDepthTexture.Sample( MMMPBBW_255_255_255_255_Sampler, UV );
		#endif
	#endif
	#ifdef SFX_GLSL_4
		col = texture( TransDepthTexture, UV );
	#endif
	#ifdef SFX_OGSFX
		col = texture( MMMPBBW_255_255_255_255_Sampler, UV );
	#endif
	#ifdef SFX_GLSL_1_2
		col = texture2D( TransDepthTexture, UV );
	#endif

	return col;
}

float4 sampleOpaqueDepthTex(float2 UV)
{
	float4 col = float4(0,0,0,0);

	#if defined(SFX_CGFX_3) || defined(SFX_HLSL_3)
		col = tex2D( MMMPBBW_255_255_255_255_Sampler, UV );
	#endif
	#ifdef SFX_HLSL_5
		#if defined(SFX_SWATCH) || defined(_3DSMAX_)
			col = OpaqueDepthTexture.Sample( MMMPBBW_255_255_255_255_Sampler, UV );
		#else
			col = OpaqueDepthTexture.Sample( MMMPBBW_255_255_255_255_Sampler, UV );
		#endif
	#endif
	#ifdef SFX_GLSL_4
		col = texture( OpaqueDepthTexture, UV );
	#endif
	#ifdef SFX_OGSFX
		col = texture( MMMPBBW_255_255_255_255_Sampler, UV );
	#endif
	#ifdef SFX_GLSL_1_2
		col = texture2D( OpaqueDepthTexture, UV );
	#endif

	return col;
}

struct DepthPeelOutput 
{ 
	float4 LinearDepth; 
	float Peel;
}; 

DepthPeelOutput DepthPeelFunc( float3 worldPos, float4x4 view, float4x4 viewPrj ) 
{ 
	DepthPeelOutput OUT; 

	#ifdef SFX_CGFX_3
		float currZ = abs( mul( view, float4(worldPos, 1.0f) ).z );
		float4 Pndc  = mul( viewPrj, float4(worldPos, 1.0f) );
		float2 UV = Pndc.xy / Pndc.w * float2(0.5f, 0.5f) + 0.5f;

		float prevZ = sampleTransDepthTex(UV).r;
		float opaqZ = sampleOpaqueDepthTex(UV).r;
		float bias = 0.00002f;
		if (currZ < prevZ * (1.0f + bias) || currZ > opaqZ * (1.0f - bias))
		{
			discard;
		}

		float ld = abs( mul( view, float4(worldPos, 1.0f) ).z );
		OUT.LinearDepth = float4(ld, ld, ld, ld);
	#else
	#if defined(SFX_GLSL_1_2) || defined(SFX_GLSL_4) || defined(SFX_OGSFX)
		float currZ = abs( ( view * float4(worldPos, 1.0f) ).z );
		float4 Pndc  = viewPrj * float4(worldPos, 1.0f);
		float2 UV = Pndc.xy / Pndc.w * float2(0.5f, 0.5f) + 0.5f;

		float prevZ = sampleTransDepthTex(UV).r;
		float opaqZ = sampleOpaqueDepthTex(UV).r;
		float bias = 0.00002f;
		if (currZ < prevZ * (1.0f + bias) || currZ > opaqZ * (1.0f - bias))
		{
			discard;
		}

		float ld = abs( ( view * float4(worldPos, 1.0f) ).z );
		OUT.LinearDepth = float4(ld, ld, ld, ld);
	#else
		float currZ = abs( mul( float4(worldPos, 1.0f), view ).z );
		float4 Pndc  = mul( float4(worldPos, 1.0f), viewPrj );
		float2 UV = Pndc.xy / Pndc.w * float2(0.5f, -0.5f) + 0.5f;

		float prevZ = sampleTransDepthTex(UV).r;
		float opaqZ = sampleOpaqueDepthTex(UV).r;
		float bias = 0.00002f;
		if (currZ < prevZ * (1.0f + bias) || currZ > opaqZ * (1.0f - bias))
		{
			discard;
		}

		float ld = abs( mul( float4(worldPos, 1.0f), view ).z );
		OUT.LinearDepth = float4(ld, ld, ld, ld);
#endif
	#endif

	OUT.Peel = 1.0f;

	return OUT; 
} 



// -------------------------------------- GenerateFlipBookUVsFunction --------------------------------------
struct GenerateFlipBookUVsOutput
{
	float2 UVs;
};

GenerateFlipBookUVsOutput GenerateFlipBookUVsFunction(float FramesPerSec, float Time, float2 NumImages, float2 UVs)
{
	GenerateFlipBookUVsOutput OUT;

	FlipBookOutput CustomCode = FlipBookFunc(FramesPerSec, Time, NumImages.x, NumImages.y, UVs);
	OUT.UVs = CustomCode.UV;

	return OUT;
}

// -------------------------------------- AmbientLightFunction --------------------------------------
struct AmbientLightOutput
{
	float3 LightColor;
};

AmbientLightOutput AmbientLightFunction(int ActiveLightIndex, float3 AlbedoColor, float3 LightColor, float LightIntensity)
{
	AmbientLightOutput OUT;

	float3 MulOp = (LightIntensity * (AlbedoColor * LightColor));
	OUT.LightColor = MulOp;

	return OUT;
}

// -------------------------------------- GetLightVectorFunction --------------------------------------
struct GetLightVectorOutput
{
	float3 Result;
};

GetLightVectorOutput GetLightVectorFunction(int ActiveLightIndex, float3 LightPosition, float3 VertexWorldPosition, int LightType, float3 LightDirection)
{
	GetLightVectorOutput OUT;

	bool IsDirectionalLight = (LightType == 4);
	float3 LerpOp = lerp((LightPosition - VertexWorldPosition), -(LightDirection), IsDirectionalLight);
	OUT.Result = LerpOp;

	return OUT;
}

// -------------------------------------- LambertDiffuseFunction --------------------------------------
struct LambertDiffuseOutput
{
	float3 Color;
};

LambertDiffuseOutput LambertDiffuseFunction(int ActiveLightIndex, float3 AlbedoColor, float3 Normal, float3 LightVector)
{
	LambertDiffuseOutput OUT;

	float SatOp = saturate(dot(Normal, LightVector));
	float3 Diffuse = (AlbedoColor * SatOp);
	OUT.Color = Diffuse;

	return OUT;
}

// -------------------------------------- LightDecayFunction --------------------------------------
struct LightDecayOutput
{
	float Attenuation;
};

LightDecayOutput LightDecayFunction(int ActiveLightIndex, float3 LightVectorUN, float Attenuation)
{
	LightDecayOutput OUT;

	bool IsAttenuationUsed = (Attenuation > 0.001000);
	float DecayContribution463 = 0.0;
	if (IsAttenuationUsed)
	{
		float PowOp = pow(length(LightVectorUN), Attenuation);
		float DivOp = (1.000000 / PowOp);
		DecayContribution463 = DivOp;
	}
	else
	{
		DecayContribution463 = 1.000000;
	}
	OUT.Attenuation = DecayContribution463;

	return OUT;
}

// -------------------------------------- LightConeAngleFunction --------------------------------------
struct LightConeAngleOutput
{
	float ConeAngle;
};

LightConeAngleOutput LightConeAngleFunction(int ActiveLightIndex, float3 LightVector, float3 LightDirection, float ConeAngle, float ConeFalloff)
{
	LightConeAngleOutput OUT;

	float CosOp = cos(max(ConeFalloff, ConeAngle));
	float DotOp = dot(LightVector, -(LightDirection));
	float SmoothStepOp = smoothstep(CosOp, cos(ConeAngle), DotOp);
	OUT.ConeAngle = SmoothStepOp;

	return OUT;
}

// -------------------------------------- ShadowMapFunction --------------------------------------
struct ShadowMapOutput
{
	float LightGain;
};

ShadowMapOutput ShadowMapFunction(int ActiveLightIndex, float4x4 LightViewPrj, float ShadowMapBias, float3 VertexWorldPosition)
{
	ShadowMapOutput OUT;

	float IfElseOp546 = 0.0;
	float4 VectorConstruct = float4(VertexWorldPosition.x, VertexWorldPosition.y, VertexWorldPosition.z, 1.000000);
	float4 MulOp = mul(VectorConstruct, LightViewPrj);
	float3 DivOp = (MulOp.xyz / MulOp.w);
	if (DivOp.x > -1.000000 && DivOp.x < 1.000000 && DivOp.y > -1.000000 && DivOp.y < 1.000000 && DivOp.z > 0.000000 && DivOp.z < 1.000000)
	{
		float Val = 0.500000;
		float2 AddOp = ((DivOp.xy * Val) + Val);
		float SubOp = (DivOp.z - (ShadowMapBias / MulOp.w));
		float ShadowTotal = 0.000000;
		for(int i=0; i<10; i+=1)
		{
			Texture2D _LightShadowMap = GetLightShadowMap(ActiveLightIndex);
			float2 MulOp586 = (ShadowFilterTaps[i] * 0.000900);
			float2 AddOp587 = (float2(AddOp.x, 1.0-AddOp.y) + MulOp586);
			float4 Sampler = SampleFromShadowMap(ActiveLightIndex, AddOp587);
			float IfElseOp540 = ((SubOp - Sampler.x) >= 0.000000) ? (0.000000) : (0.100000);
			ShadowTotal += IfElseOp540;
		}
		IfElseOp546 = ShadowTotal;
	}
	else
	{
		IfElseOp546 = 1.000000;
	}
	OUT.LightGain = IfElseOp546;

	return OUT;
}

// -------------------------------------- LightContributionFunction --------------------------------------
struct LightContributionOutput
{
	float3 Light;
};

// -------------------------------------- BlinnSpecularFunction --------------------------------------
struct BlinnSpecularOutput
{
	float3 SpecularColor;
};

BlinnSpecularOutput BlinnSpecularFunction(int ActiveLightIndex, float3 LightVector, float3 Normal, float3 CameraVector, float SpecularPower, float3 SpecularColor)
{
	BlinnSpecularOutput OUT;

	float3 NormOp = normalize((LightVector + CameraVector));
	float SatOp = saturate(dot(Normal, NormOp));
	float3 BlinnSpec = (pow(SatOp, SpecularPower) * SpecularColor);
	float SatOp978 = saturate(dot(Normal, LightVector));
	float3 MulOp = (BlinnSpec * SatOp978);
	OUT.SpecularColor = MulOp;

	return OUT;
}

// -------------------------------------- DesaturateColorFunction --------------------------------------
struct DesaturateColorOutput
{
	float DesaturateColor;
};

DesaturateColorOutput DesaturateColorFunction(int ActiveLightIndex, float3 Color)
{
	DesaturateColorOutput OUT;

	float3 Col = float3(0.300008,0.600000,0.100008);
	float DotOp = dot(saturate(Color), Col.xyz);
	OUT.DesaturateColor = DotOp;

	return OUT;
}

// -------------------------------------- DesaturateColorFunction --------------------------------------
DesaturateColorOutput DesaturateColorFunction(float3 Color)
{
	DesaturateColorOutput OUT;

	float3 Col = float3(0.300008,0.600000,0.100008);
	float DotOp = dot(saturate(Color), Col.xyz);
	OUT.DesaturateColor = DotOp;

	return OUT;
}

// -------------------------------------- ShaderVertex --------------------------------------
struct APPDATA
{
	float3 Position : POSITION;
	float2 map1 : TEXCOORD0;
};

struct SHADERDATA
{
	float4 Position : SV_Position;
	float4 map1 : TEXCOORD0;
	float4 WorldPosition : TEXCOORD1;
	half3 FogFactor : TEXCOORD2;
};

SHADERDATA ShaderVertex(PBLVertexInput IN)
{
	SHADERDATA OUT;
    float4x4 viewProj = mul(cameraProjection, cameraView);
	OUT.Position = float4(IN.myPosition.xyz, 1);
    float4 OutUVs = float4(IN.myUV.x, IN.myUV.y, 0.000000, 0.000000);
	OUT.map1 = OutUVs;
    float4 WorldPos = mul(toWorld, OUT.Position);
	OUT.WorldPosition = WorldPos;
    OUT.WorldPosition = (mul(toWorld, float4(IN.myPosition.xyz, 1)));
    float4 _HPosition = mul(viewProj, float4(OUT.WorldPosition.xyz, 1));
	float fogFactor = 0.0;
	OUT.FogFactor = float3(fogFactor, fogFactor, fogFactor); 

    float4 WVSpace = mul(mul(viewProj, toWorld), OUT.Position);
	OUT.Position = WVSpace;
    
	return OUT;
}

// -------------------------------------- ShaderPixel --------------------------------------
struct PIXELDATA
{
	float4 Color : SV_Target;
};


PIXELDATA ShaderPixel(SHADERDATA IN, bool FrontFace : SV_IsFrontFace)
{
    PIXELDATA OUT;
    float InvertSatMask = (1.000000 - saturate(0.000000));
    float3 Color = float3(0.500000, 0.500000, 0.500000);
    float3 ReplaceDiffuseWithReflection = (InvertSatMask * Color.xyz);
    float2 Dimension = float2(8, 4);
    GenerateFlipBookUVsOutput GenerateFlipBookUVs1290 = GenerateFlipBookUVsFunction(24.f, threadTotalTime * 0.1f, Dimension, IN.map1.xy);
    float4 Sampler = BOBBELSdds.Sample(MMMLWWWSampler, float2(GenerateFlipBookUVs1290.UVs.x, 1 - GenerateFlipBookUVs1290.UVs.y));
    float3 FlippedNormals = lerp(-(Sampler.xyz), Sampler.xyz, FrontFace);
    float ClampOpacity = saturate(1.000000);
    float3 CameraPosition = cameraPosition;
    float3 CamVec = (CameraPosition - IN.WorldPosition.xyz);
    float3 CamVecNorm = normalize(CamVec);
    float4 LightLoopTotal11 = float4(0, 0, 0, 0);
    
    float3 NoReflection = float3(0.000000, 0.000000, 0.000000);
    float3 ReflectXmask = (0.000000 * NoReflection.xyz);
    float3 DefaultEmissiveColor = float3(0.000000, 0.000000, 0.000000);
    float3 DefaultIBLColor = float3(0.000000, 0.000000, 0.000000);
    float3 PreMultipliedAlpha = ((DefaultEmissiveColor.xyz + DefaultIBLColor.xyz) * ClampOpacity);
    float3 AddReflection = (ReflectXmask + PreMultipliedAlpha);
    DesaturateColorOutput DesaturateColor363 = DesaturateColorFunction(ReflectXmask);
    float OpacityAndReflection = (ClampOpacity + DesaturateColor363.DesaturateColor);
    float4 TotalAmbientAndOpacity = float4(AddReflection.x, AddReflection.y, AddReflection.z, OpacityAndReflection);
    float4 LightLoopAndAfterLoop = (LightLoopTotal11 + TotalAmbientAndOpacity);
    float SatOp = saturate(LightLoopAndAfterLoop.w);
    float4 VectorConstruct = float4(LightLoopAndAfterLoop.xyz.x, LightLoopAndAfterLoop.xyz.y, LightLoopAndAfterLoop.xyz.z, SatOp);
   

    OUT.Color = Sampler; //VectorConstruct;

	return OUT;
}

// -------------------------------------- ShaderPixelP1 --------------------------------------
struct PIXELDATAP1
{
	float4 Color0 : SV_Target0;
	float4 Color1 : SV_Target1;
};

PIXELDATAP1 ShaderPixelP1(SHADERDATA IN, bool FrontFace : SV_IsFrontFace)
{
	PIXELDATAP1 OUT;

    DepthPeelOutput DepthPeel = DepthPeelFunc(IN.WorldPosition.xyz, cameraView, mul(cameraView, cameraProjection));
	OUT.Color0 = ShaderPixel(IN, FrontFace).Color;
	OUT.Color1 = DepthPeel.LinearDepth;

	return OUT;
}

// -------------------------------------- ShaderPixelP2 --------------------------------------
struct PIXELDATAP2
{
	float4 Color0 : SV_Target0;
	float4 Color1 : SV_Target1;
};

PIXELDATAP2 ShaderPixelP2(SHADERDATA IN, bool FrontFace : SV_IsFrontFace)
{
	PIXELDATAP2 OUT;

    DepthPeelOutput DepthPeel = DepthPeelFunc(IN.WorldPosition.xyz, cameraView, mul(cameraView, cameraProjection));
	OUT.Color0 = ShaderPixel(IN, FrontFace).Color;
	OUT.Color1 = (DepthPeel.Peel * (OUT.Color0.w > 0.001f ? float4(1.0f, 1.0f, 1.0f, 1.0f) : float4(0.0f, 0.0f, 0.0f, 0.0f)));

	return OUT;
}

// -------------------------------------- ShaderPixelP3 --------------------------------------
struct PIXELDATAP3
{
	float4 Color0 : SV_Target0;
	float4 Color1 : SV_Target1;
};

PIXELDATAP3 ShaderPixelP3(SHADERDATA IN, bool FrontFace : SV_IsFrontFace)
{
	PIXELDATAP3 OUT;

	OUT.Color0 = ShaderPixel(IN, FrontFace).Color;
	OUT.Color1 = (OUT.Color0.w > 0.001f ? float4(1.0f, 1.0f, 1.0f, 1.0f) : float4(0.0f, 0.0f, 0.0f, 0.0f));

	return OUT;
}

// -------------------------------------- technique T0 ---------------------------------------
technique11 T0
<
	bool overridesDrawState = false;
	int isTransparent = 0;
>
{
	pass P0
	<
		string drawContext = "colorPass";
	>
	{
		SetVertexShader(CompileShader(vs_5_0, ShaderVertex()));
		SetPixelShader(CompileShader(ps_5_0, ShaderPixel()));
		SetHullShader(NULL);
		SetDomainShader(NULL);
		SetGeometryShader(NULL);
	}

	pass P1
	<
		string drawContext = "transparentPeel";
	>
	{
		SetVertexShader(CompileShader(vs_5_0, ShaderVertex()));
		SetPixelShader(CompileShader(ps_5_0, ShaderPixelP1()));
		SetHullShader(NULL);
		SetDomainShader(NULL);
		SetGeometryShader(NULL);
	}

	pass P2
	<
		string drawContext = "transparentPeelAndAvg";
	>
	{
		SetVertexShader(CompileShader(vs_5_0, ShaderVertex()));
		SetPixelShader(CompileShader(ps_5_0, ShaderPixelP2()));
		SetHullShader(NULL);
		SetDomainShader(NULL);
		SetGeometryShader(NULL);
	}

	pass P3
	<
		string drawContext = "transparentWeightedAvg";
	>
	{
		SetVertexShader(CompileShader(vs_5_0, ShaderVertex()));
		SetPixelShader(CompileShader(ps_5_0, ShaderPixelP3()));
		SetHullShader(NULL);
		SetDomainShader(NULL);
		SetGeometryShader(NULL);
	}

}

