#include "CommonStructs.hlsli"
#include "StandardBuffersVS.hlsli"

SkyboxVertexToPixel VSMain(ColoredVertexInput input)
{
    SkyboxVertexToPixel output;
    float4 vertexWorldPos = mul(toWorld, float4(input.myPosition.xyz, 1.f));
    float4 vertexProjectionPos = mul(cameraProjection, mul(cameraView, vertexWorldPos));
    vertexProjectionPos.z = vertexProjectionPos.w; // Set Z-value to w to make the depth-test always be furthest away from camera. (z/w == 1.f)

    output.myPosition = vertexProjectionPos;
    output.myToEye = normalize(vertexWorldPos - cameraPosition);
    return output;
};