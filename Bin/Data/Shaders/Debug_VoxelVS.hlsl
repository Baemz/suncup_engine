#include "GI_Decl.hlsli"

struct VSOut
{
    float4 pos : SV_POSITION;
    float4 col : TEXCOORD;
};

Texture3D<float4> voxelRadiance : register(t0);
VSOut VSMain(uint vertexID : SV_VERTEXID)
{
    VSOut o;

    uint3 coord = Unflatten3D(vertexID, uint3(VoxelGridSize, VoxelGridSize, VoxelGridSize));
    o.pos = float4(coord, 1);
    //uint sam
    o.col = voxelRadiance[coord];
    //o.col = DecodeColor(sam);
    return o;
}