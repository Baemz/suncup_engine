#include "DeferredLightFunc.hlsli"
#define SHADOW_BUFFER_SIZE_X 4096.f
#define SHADOW_BUFFER_SIZE_Y 4096.f
//Texture2D diffuseTexture = depthTexture; // Albedo
////Texture2D normalTexture : register(t1); // Normal
//Texture2D roughnessTexture = diffuseAndRoughnessTexture; // Roughness
//Texture2D metallicTexture = emissiveAndMetallicTexture; // Metallic
//Texture2D emissiveTexture = ambientOcclusionTexture; // Emissive
Texture2D shadowMap : register(t8);
RWStructuredBuffer<VoxelizedEncodedData> voxelRadianceOut : register(u0);
SamplerState shadowSamplerState : register(s1);

#define MAX_LIGHTS 8

cbuffer LightData : register(b3)
{
    float4 toLightDirectionAndMipCount;
    float4 lightColor;
    float dirLightintensity;
}
cbuffer shadowData : register(b4)
{
    float4x4 depthProj;
    float4x4 depthView;
    float4x4 objInv;
    float2 resolution;
}

cbuffer PointLightData : register(b5)
{
    uint PLNumLights;
    uint3 __PLPADDING__;
    struct PL
    {
        float4 PLPosition;
        float3 PLColor;
        float PLRange;
        float PLIntensity;
    } pointLights[MAX_LIGHTS];
}

cbuffer SpotLightData : register(b6)
{
    uint SLNumLights;
    uint3 __SLPADDING__;
    struct SL
    {
        float4 SLPosition;
        float3 SLColor;
        float SLAngle;
        float3 SLDirection;
        float SLRange;
        float SLIntensity;
    } spotLights[MAX_LIGHTS];

}

static const float texelSizeH = 1.f / SHADOW_BUFFER_SIZE_X;
static const float texelSizeV = 1.f / SHADOW_BUFFER_SIZE_Y;
static const float2 texelOffset[8] =
{
    float2(0, -texelSizeV),
										float2(texelSizeH, 0),
										float2(0, texelSizeV),
										float2(-texelSizeH, 0),
										float2(texelSizeH, texelSizeV),
										float2(-texelSizeH, -texelSizeV),
										float2(-texelSizeH, texelSizeV),
										float2(texelSizeH, -texelSizeV)
};
static const float bias = 0.00025f;
float DetermineShadow(float2 projectTexCoord)
{
    [branch]
    if (saturate(projectTexCoord.x) == projectTexCoord.x && saturate(projectTexCoord.y) == projectTexCoord.y)
    {
        float depthValue = 0.f;
        depthValue += shadowMap.Sample(shadowSamplerState, projectTexCoord).r;
        depthValue += shadowMap.Sample(shadowSamplerState, projectTexCoord + texelOffset[0]).r;
        depthValue += shadowMap.Sample(shadowSamplerState, projectTexCoord + texelOffset[1]).r;
        depthValue += shadowMap.Sample(shadowSamplerState, projectTexCoord + texelOffset[2]).r;
        depthValue += shadowMap.Sample(shadowSamplerState, projectTexCoord + texelOffset[3]).r;
        depthValue += shadowMap.Sample(shadowSamplerState, projectTexCoord + texelOffset[4]).r;
        depthValue += shadowMap.Sample(shadowSamplerState, projectTexCoord + texelOffset[5]).r;
        depthValue += shadowMap.Sample(shadowSamplerState, projectTexCoord + texelOffset[6]).r;
        depthValue += shadowMap.Sample(shadowSamplerState, projectTexCoord + texelOffset[7]).r;
    
        depthValue /= 9.0f;
        return depthValue;
    }
    return 99999.99f;
}

float4 GetDirectionalLightColor(Attributes attributes)
{
    const float4 vertexPos = mul(depthProj, mul(depthView, attributes.worldPosition));
    float2 projectTexCoord;
    projectTexCoord.x = vertexPos.x / vertexPos.w / 2.0f + 0.5f;
    projectTexCoord.y = -vertexPos.y / vertexPos.w / 2.0f + 0.5f;
    
    const float lightDepthValue = ((vertexPos.z / vertexPos.w) - bias);
    const float depthValue = DetermineShadow(projectTexCoord);
    [branch]
    if (lightDepthValue > depthValue)
    {
        return float4(0.f, 0.f, 0.f, 0.f);
    }
    else
    {
        const float3 lightDir = normalize(toLightDirectionAndMipCount.xyz);
        const float3 lambert = GetLambert(attributes, lightDir);
        const float3 fresnel = GetFresnel(attributes, lightDir);
        PBRData pbrData;
        pbrData.lightColor = lightColor.rgb * dirLightintensity;
        pbrData.fresnel = fresnel;
        pbrData.lambert = lambert;
    
        float3 directDiffuse = GetDirectDiffuse(attributes, pbrData);

        return float4(directDiffuse, 1.0f);
    }
}

float4 GetPointLightColor(Attributes attributes)
{
    float4 returnColor = float4(0.f, 0.f, 0.f, 1.f);
	[unroll]
    for (unsigned int index = 0; index < PLNumLights; ++index)
    {
        float3 toLight = pointLights[index].PLPosition.xyz - attributes.worldPosition.xyz;
        const float toLightDistance = distance(pointLights[index].PLPosition.xyz, attributes.worldPosition.xyz);
        toLight = normalize(toLight);
        const float lightRange = pointLights[index].PLRange * pointLights[index].PLRange;
        const float toLightDist2 = toLightDistance * toLightDistance;

        const float linearAttenuation = saturate(1.f - (toLightDist2 / lightRange));
        const float lambertAttenuation = saturate(dot(attributes.objectNormal.xyz, toLight));
        const float physicalAttenuation = 1.f / toLightDistance;

        const float attenuation = lambertAttenuation * linearAttenuation * physicalAttenuation;

        const float3 lightColor = (pointLights[index].PLColor.rgb * (pointLights[index].PLIntensity));
        float4 resColor = float4(attributes.diffuseAndRoughness.rgb * attenuation * lightColor, 1.0f);
        
        const float3 lightDir = toLight;
        const float3 lambert = GetLambert(attributes, lightDir);
        const float3 fresnel = GetFresnel(attributes, lightDir);

        PBRData pbrData;
        pbrData.lightColor = lightColor;
        pbrData.fresnel = fresnel;
        pbrData.lambert = lambert;
    
        const float3 directDiffuse = GetDirectDiffuse(attributes, pbrData);
        resColor *= float4(directDiffuse, 1.0f);
        returnColor.rgb += (resColor.rgb * 2.f);
    }

    return saturate(returnColor);
}

float4 GetSpotLightColor(Attributes attributes)
{
    float4 returnColor = float4(0.f, 0.f, 0.f, 1.f);
	[unroll]
    for (unsigned int index = 0; index < SLNumLights; ++index)
    {
        float3 toLight = spotLights[index].SLPosition.xyz - attributes.worldPosition.xyz;
        const float toLightDistance = distance(spotLights[index].SLPosition.xyz, attributes.worldPosition.xyz);
        toLight = normalize(toLight);
        const float lightRange = spotLights[index].SLRange * spotLights[index].SLRange;
        const float toLightDist2 = toLightDistance * toLightDistance;

        const float lambertAttenuation = saturate(dot(attributes.objectNormal.xyz, toLight));
        const float linearAttenuation = saturate(1.f - (toLightDist2 / lightRange));
        const float physicalAttenuation = 1.f / toLightDistance;
    
        const float cutPoint = 1.f - ((spotLights[index].SLAngle / 2.f) / 180.f);
        const float a = saturate(dot(normalize(-toLight), normalize(spotLights[index].SLDirection)));
        const float angleAttenuation = saturate((a - cutPoint) * (1.f / (1.f - cutPoint)));

        const float attenuation = lambertAttenuation * linearAttenuation * physicalAttenuation * angleAttenuation;
        const float3 lightColor = (spotLights[index].SLColor.rgb * (spotLights[index].SLIntensity));
        float4 resColor = float4(attributes.diffuseAndRoughness.rgb * attenuation * lightColor, 1.0f);
        
        const float3 lightDir = toLight;
        const float3 lambert = GetLambert(attributes, lightDir);
        const float3 fresnel = GetFresnel(attributes, lightDir);

        PBRData pbrData;
        pbrData.lightColor = lightColor;
        pbrData.fresnel = fresnel;
        pbrData.lambert = lambert;
    
        const float3 directDiffuse = GetDirectDiffuse(attributes, pbrData);
        resColor *= float4(directDiffuse, 1.0f);
        returnColor.rgb += (resColor.rgb * 5.0f);
    }

    return returnColor;
}

void PSMain(GI_ModelGeometryToPixel input)
{
    const float3 diff = ((input.myWPosition - VoxelCenter) / VoxelGridSize / VoxelSize) * float3(0.5f, -0.5f, 0.5f) + 0.5f;
    const uint3 writecoord = floor(diff * VoxelGridSize);

	[branch]
    if (writecoord.x > 0 && writecoord.x < VoxelGridSize
		&& writecoord.y > 0 && writecoord.y < VoxelGridSize
		&& writecoord.z > 0 && writecoord.z < VoxelGridSize)
    {
        Attributes attributes;
        attributes.diffuseAndRoughness.rgb = depthTexture.Sample(samplerState, input.myUV).rgb;
        const float3 normalSample = ((normalTexture.Sample(samplerState, input.myUV).xyz) * 2) - 1;
        attributes.objectNormal = float4(normalize(mul(normalSample, input.myTangentSpaceMatrix)), 1.0f);
        attributes.objectNormal = Dither(attributes.objectNormal, input.myUV);
        attributes.metallic = emissiveAndMetallicTexture.Sample(samplerState, input.myUV);
        attributes.emissive = ambientOcclusionTexture.Sample(samplerState, input.myUV);
        attributes.worldPosition = float4(input.myWPosition, 1.0f);
        attributes.toEye = normalize(cameraPosition - attributes.worldPosition);

        const float4 directionalLight = GetDirectionalLightColor(attributes);
        const float4 pointLight = GetPointLightColor(attributes) * 4.f;
        const float4 spotLight = GetSpotLightColor(attributes) * 4.f;

        float4 finalColor = (directionalLight + pointLight + spotLight);
        finalColor.rgb += attributes.diffuseAndRoughness.rgb * (attributes.emissive.rgb * 10.f);
        finalColor.a = 1.f;

        const uint encodedColor = EncodeColor(finalColor);
        const uint encodedNormal = EncodeNormal(attributes.objectNormal.xyz);
        const uint voxelID = Flatten3D(writecoord, uint3(VoxelGridSize, VoxelGridSize, VoxelGridSize));
        const uint currVal = voxelRadianceOut[voxelID].col;
        InterlockedMax(voxelRadianceOut[voxelID].col, encodedColor);

        // Only change the normal if this voxel-values are being used.
        [branch]
        if (currVal < encodedColor)
        {
            int outVal = 0;
            InterlockedExchange(voxelRadianceOut[voxelID].norm, encodedNormal, outVal);
            //InterlockedMax(voxelRadianceOut[voxelID].norm, encodedNormal);
        }
    }
}