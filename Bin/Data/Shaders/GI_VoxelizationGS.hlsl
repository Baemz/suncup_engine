#include "GI_Decl.hlsli"
#include "StandardBuffersVS.hlsli"

[maxvertexcount(3)]
void GSMain(triangle GI_ModelVertexToGeometry input[3], inout TriangleStream<GI_ModelGeometryToPixel> outputStream)
{
    GI_ModelGeometryToPixel output[3];

    const float3 faceNormal = abs(input[0].myNormal.xyz + input[1].myNormal.xyz + input[2].myNormal.xyz);
    uint maxFace = faceNormal[1] > faceNormal[0] ? 1 : 0;
    maxFace = faceNormal[2] > faceNormal[maxFace] ? 2 : maxFace;

	[unroll]
    for (uint i = 0; i < 3; ++i)
    {
        output[i].myPosition = float4((input[i].myWPosition.xyz - VoxelCenter) / VoxelSize, 1.f);
        
        output[i].myPosition.xyz = ((maxFace == 0) ? output[i].myPosition.zyx : ((maxFace == 1) ? output[i].myPosition.xzy : output[i].myPosition.xyz));
        
        output[i].myPosition.xy /= VoxelGridSize;
        output[i].myPosition.z = 1;
        output[i].myNormal = input[i].myNormal;
        output[i].myUV = input[i].myUV;
        output[i].myWPosition = input[i].myWPosition;
        output[i].myTangentSpaceMatrix = input[i].myTangentSpaceMatrix;
    }
    
    const float2 side0N = normalize(output[1].myPosition.xy - output[0].myPosition.xy);
    const float2 side1N = normalize(output[2].myPosition.xy - output[1].myPosition.xy);
    const float2 side2N = normalize(output[0].myPosition.xy - output[2].myPosition.xy);
    const float texelSize = VoxelGridSizeInversed;
    output[0].myPosition.xy += (normalize(-side0N + side2N) * texelSize);
    output[1].myPosition.xy += (normalize(side0N - side1N) * texelSize);
    output[2].myPosition.xy += (normalize(side1N - side2N) * texelSize);


	[unroll]
    for (uint j = 0; j < 3; j++)
    {
        outputStream.Append(output[j]);
    }

    outputStream.RestartStrip();
}