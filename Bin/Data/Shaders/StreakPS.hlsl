#include "CommonStructs.hlsli"

Texture2D instanceTexture : register(t0);
SamplerState instanceSampler : register(s0);

PixelOutput PSMain(GeometryToPixel input)
{
	PixelOutput output;
	output.myColor = instanceTexture.Sample(instanceSampler, input.myUV);
    output.myColor.rgb = input.myColor.rgb;
    output.myColor.a *= input.myColor.a;
	return output;
};