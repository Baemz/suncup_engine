#include "CommonStructs.hlsli"
#include "StandardBuffersVS.hlsli"

ColoredVertexToPixel VSMain(ColoredVertexInput input)
{
    ColoredVertexToPixel output;
    float4 vertexProjectionPos = mul(cameraProjection, mul(cameraView, mul(toWorld, float4(input.myPosition.xyz, 1))));

    output.myPosition = vertexProjectionPos;
    output.myColor = input.myColor;

    return output;
};