#include "CommonStructs.hlsli"
#include "StandardBuffersVS.hlsli"

float2x2 ComputeRotation(float aRotation)
{
    float c = cos(aRotation);
    float s = sin(aRotation);
    return float2x2(c, -s, s, c);
}

static const float2 uv[4] =
{
    { 0, 1 },
    { 1, 1 },
    { 0, 0 },
    { 1, 0 }
};

[maxvertexcount(4)]
void GSMain(point ParticleToGeometry input[1], inout TriangleStream<GeometryToPixel> output)
{
	const float4 offset[4] =
	{
        { -input[0].mySize, input[0].mySize, 0, 0 },
        { input[0].mySize, input[0].mySize, 0, 0 },
        { -input[0].mySize, -input[0].mySize, 0, 0 },
        { input[0].mySize, -input[0].mySize, 0, 0 }
    };
    
    float2x2 rotation = ComputeRotation(input[0].myRotation);
    for (int i = 0; i < 4; i++)
    {
        GeometryToPixel vertex;

        float2 totalOffset = mul(rotation, offset[i].xy);
        vertex.myPosition.w = 1.f;

#ifdef HORIZONTAL_PROJECTION
        vertex.myPosition.xyz = input[0].myWPosition.xyz;
        vertex.myPosition.xz += totalOffset;
        vertex.myPosition = mul(cameraView, vertex.myPosition);
#else
    #ifdef VERTICAL_PROJECTION
        vertex.myPosition.xyz = input[0].myWPosition.xyz;
        vertex.myPosition.xy += totalOffset;
        vertex.myPosition = mul(cameraView, vertex.myPosition);
    #else // VIEW_PROJECTION
        vertex.myPosition.xyz = input[0].myPosition.xyz;
        vertex.myPosition.xy += totalOffset;
    #endif
#endif

        vertex.myPosition = mul(cameraProjection, vertex.myPosition);
        vertex.myColor = input[0].myColor;
        vertex.myUV = uv[i];
        output.Append(vertex);
    }

	output.RestartStrip();
}