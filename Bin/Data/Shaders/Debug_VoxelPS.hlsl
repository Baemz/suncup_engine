float4 PSMain(float4 pos : SV_POSITION, float4 col : TEXCOORD) : SV_TARGET
{
    return saturate(col);
}