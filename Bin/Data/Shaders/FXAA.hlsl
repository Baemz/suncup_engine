#include "CommonStructs.hlsli"

#define FXAA_PC 1
#define FXAA_HLSL_5 1
#define FXAA_GREEN_AS_LUMA 1
#define FXAA_QUALITY__PRESET 39
#include "FXAA.hlsli"


Texture2D fullscreenTexture0 : register(t0);
Texture2D fullscreenTexture1 : register(t1);
SamplerState samplerState : register(s0);

cbuffer Resolution : register(b3)
{
    float2 myResolution;
}
////////////////////////////////////////////
/*                                        */
/*              FXAA_Pass                 */
/*   (Fast Approximation Anti-Aliasing)   */
/*                                        */
////////////////////////////////////////////

PixelOutput PS_FXAA(TexturedVertexToPixel input)
{
    //discard;
    PixelOutput output;
    static const float fxaaSubpix = 0.75;
    static const float fxaaEdgeThreshold = 0.166;
    static const float fxaaEdgeThresholdMin = 0.0833;

    FxaaTex tex = { samplerState, fullscreenTexture0 };

    output.myColor = FxaaPixelShader(input.myUV, 0, tex, tex, tex, 1 / myResolution, 0, 0, 0, fxaaSubpix, fxaaEdgeThreshold, fxaaEdgeThresholdMin, 0, 0, 0, 0);
    return output;
}




/*const float2 texelSize = float2((1.f / myResolution.x), (1.f / myResolution.y));
    const float FXAA_SPAN_MAX = 64.f;
    const float FXAA_REDUCE_MIN = (1 / 64.f);
    const float FXAA_REDUCE_MUL = 16;//(1 / 2.f);
    
    float3 luminance;
    luminance.r = 0.2126f;
    luminance.g = 0.7152f;
    luminance.b = 0.0722f;

    float lumTL = dot(luminance, fullscreenTexture0.Sample(samplerState, input.myUV + (float2(-1.f, -1.f) * texelSize)).rgb);
    float lumTR = dot(luminance, fullscreenTexture0.Sample(samplerState, input.myUV + (float2(1.f, -1.f) * texelSize)).rgb);
    float lumBL = dot(luminance, fullscreenTexture0.Sample(samplerState, input.myUV + (float2(-1.f, 1.f) * texelSize)).rgb);
    float lumBR = dot(luminance, fullscreenTexture0.Sample(samplerState, input.myUV + (float2(1.f, 1.f) * texelSize)).rgb);
    float lumMiddle = dot(luminance, fullscreenTexture0.Sample(samplerState, input.myUV).rgb);

    float2 dir;
    dir.x = -((lumTL + lumTR) - (lumBL + lumBR));
    dir.y = ((lumTL + lumBL) - (lumTR + lumBR));

    float dirReduction = min((lumTL + lumTR + lumBL + lumBR) * (FXAA_REDUCE_MUL * 0.25f), FXAA_REDUCE_MIN);
    float inverseDirAdjust = 1.f / (min(abs(dir.x), abs(dir.y)) + dirReduction);

    dir = min(float2(FXAA_SPAN_MAX, FXAA_SPAN_MAX), max(float2(-FXAA_SPAN_MAX, -FXAA_SPAN_MAX), dir * inverseDirAdjust)) * texelSize;

    float3 result1 = (1 / 2.f) * (
    fullscreenTexture0.Sample(samplerState, input.myUV + (dir * float2(1 / 3.f - 0.5f, 1 / 3.f - 0.5f))).rgb +
    fullscreenTexture0.Sample(samplerState, input.myUV + (dir * float2(2 / 3.f - 0.5f, 2 / 3.f - 0.5f))).rgb);

    float3 result2 = result1 * (1 / 2.f) + (1 / 4.f) * (
    fullscreenTexture0.Sample(samplerState, input.myUV + (dir * float2(0 / 3.f - 0.5f, 0 / 3.f - 0.5f))).rgb +
    fullscreenTexture0.Sample(samplerState, input.myUV + (dir * float2(3 / 3.f - 0.5f, 3 / 3.f - 0.5f))).rgb);

    float lumMin = min(lumMiddle, min(min(lumTL, lumTR), min(lumBL, lumBR)));
    float lumMax = max(lumMiddle, max(max(lumTL, lumTR), max(lumBL, lumBR)));
    float lumResult2 = dot(luminance, result2);

    if (lumResult2 < lumMin || lumResult2 > lumMax)
    {
        output.myColor = float4(result1, 1.f);
    }
    else
    {
        output.myColor = float4(result2, 1.f);
    }*/