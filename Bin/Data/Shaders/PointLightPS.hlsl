#include "CommonStructs.hlsli"
#include "DeferredLightFunc.hlsli"

static const float4 globalFogColor = float4(0.5f, 0.5f, 0.5f, 1.0f);

float4 ViewPosFromDepth(float depth, float2 TexCoord)
{
    float z = depth;
    float2 texpos = TexCoord * 2.0f - 1.0f;
    texpos.y *= -1.f;
    float4 clipSpacePosition = float4(texpos, z, 1.0f);
    float4 viewSpacePosition = mul(inverseCameraProjection, clipSpacePosition);
    viewSpacePosition /= viewSpacePosition.w;

    return viewSpacePosition;
}

float4 WorldPosFromDepth(float depth, float2 TexCoord)
{
    return mul(inverseCameraView, ViewPosFromDepth(depth, TexCoord));
}

PixelOutput PSMain(LightVertexToPixel input)
{
    PixelOutput output;
    Attributes attributes;
    input.myUV /= input.myPosition.w;
    attributes.ambientOcclusionAndFogFactor = ambientOcclusionTexture.Sample(samplerState, input.myUV);
    
    [branch]
    if (attributes.ambientOcclusionAndFogFactor.a <= 0.01f)
    {
        discard;
    }
    else if (UseGlobalIllumination == 2)
    {
        discard;
    }
    
    const float depth = depthTexture.Sample(samplerState, input.myUV).r;
    attributes.worldPosition = WorldPosFromDepth(depth, input.myUV);
    attributes.diffuseAndRoughness = diffuseAndRoughnessTexture.Sample(samplerState, input.myUV);
    attributes.metallic = emissiveAndMetallicTexture.Sample(samplerState, input.myUV).aaaa;
    attributes.objectNormal = normalTexture.Sample(samplerState, input.myUV) * 2.f - 1.f;
    attributes.objectNormal = Dither(attributes.objectNormal, input.myUV);
    attributes.toEye = normalize(cameraPosition - attributes.worldPosition);

    float3 toLight = input.myInstancePosition.xyz - attributes.worldPosition.xyz;
    const float toLightDistance = distance(input.myInstancePosition.xyz, attributes.worldPosition.xyz);
    toLight = normalize(toLight);
    const float lightRange = input.myRange * input.myRange;
    const float toLightDist2 = toLightDistance * toLightDistance;

    const float linearAttenuation = saturate(1.f - (toLightDist2 / lightRange));
    const float lambertAttenuation = saturate(dot(attributes.objectNormal.xyz, toLight));
    const float physicalAttenuation = 1.f / toLightDistance;

    const float attenuation = lambertAttenuation * linearAttenuation * physicalAttenuation;
    const float3 lightColor = (input.myLightColor.rgb * (input.myIntensity));
    float4 resColor = float4(attributes.diffuseAndRoughness.rgb * attenuation * lightColor, 1.0f);
    
    const float3 lightDir = toLight;
    const float3 lambert = GetLambert(attributes, lightDir);
    const float3 fresnel = GetFresnel(attributes, lightDir);
    const float distribution = GetDistribution(attributes, lightDir);
    const float visibility = GetVisibility(attributes, lightDir);

    PBRData pbrData;
    pbrData.lightColor = lightColor;
    pbrData.fresnel = fresnel;
    pbrData.lambert = lambert;
    pbrData.distribution = distribution;
    pbrData.visibility = visibility;
    
    const float3 directDiffuse = GetDirectDiffuse(attributes, pbrData);
    const float3 directSpecular = GetDirectSpecular(attributes, pbrData);
    
    resColor *= float4(directDiffuse + directSpecular, 1.0f);
    output.myColor = resColor * 2.f;
    return output;
}