#include "GI_Decl.hlsli"
RWStructuredBuffer<VoxelizedEncodedData> encodedRadiance : register(u0);
RWTexture3D<float4> outRadiance : register(u1);

[numthreads(256, 1, 1)]
void CSMain(uint3 dispThreadID : SV_DispatchThreadID)
{
    const VoxelizedEncodedData vData = encodedRadiance[dispThreadID.x];
    const float4 decodedColor = DecodeColor(vData.col);
    const uint3 writeCoord = Unflatten3D(dispThreadID.x, uint3(VoxelGridSize, VoxelGridSize, VoxelGridSize));
    
    [branch]
    if (decodedColor.a > 0)
    {
        [branch]
        if (VoxelCenterChangedThisFrame == 1)
        {
            outRadiance[writeCoord] = float4(decodedColor.rgb, 1);
        }
        else
        {
          outRadiance[writeCoord] = lerp(outRadiance[writeCoord], float4(decodedColor.rgb, 1), 0.25f);
        }
    }
    else
    {
        outRadiance[writeCoord] = float4(0.f, 0.f, 0.f, 0.f);
    }
    encodedRadiance[dispThreadID.x].col = 0;
}