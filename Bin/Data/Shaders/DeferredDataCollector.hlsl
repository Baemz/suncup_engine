#include "CommonStructs.hlsli"
#include "Dither.hlsli"

SamplerState samplerState : register(s0);

Texture2D diffuseTexture : register(t0);
Texture2D normalTexture : register(t1);
Texture2D roughnessTexture : register(t2);
Texture2D metallicTexture : register(t3);
Texture2D emissiveTexture : register(t4);
Texture2D ambientOcclusionTexture : register(t5);
TextureCube environmentalTexture : register(t6);

static const float globalFogStart = 10.f;
static const float globalFogEnd = 60.0f;
static const float globalFogDist = (globalFogEnd - globalFogStart);

struct Attributes
{
	float4 diffuseAndRoughness;
	float4 emissiveAndAO;
    float4 objectNormalAndMetallic;
};

DeferredDataOutput PSMain(PBLVertexToPixel input)
{
    DeferredDataOutput output;
	Attributes attributes;

    const float4 diffuseSample = diffuseTexture.Sample(samplerState, input.myUV);

    [branch]
    if(diffuseSample.a < 0.8f)
    {
        discard;
    }

	attributes.diffuseAndRoughness.rgb = diffuseSample.rgb;
    attributes.objectNormalAndMetallic.a = metallicTexture.Sample(samplerState, input.myUV).r;
    attributes.diffuseAndRoughness.a = roughnessTexture.Sample(samplerState, input.myUV).r;
    attributes.emissiveAndAO.a = ambientOcclusionTexture.Sample(samplerState, input.myUV).r;
    attributes.emissiveAndAO.rgb = emissiveTexture.Sample(samplerState, input.myUV).rgb;

    const float3 normalSample = (((normalTexture.Sample(samplerState, input.myUV).xyz) * 2) - 1);
    attributes.objectNormalAndMetallic.xyz = normalize(mul(normalSample, input.myTangentSpaceMatrix));

    
    float fogFactor = 1.0f;
    [branch]
    if (input.myUseFog)
    {
        fogFactor = saturate((globalFogEnd - input.myViewPos.z) / globalFogDist);
    }
    else
    {
        fogFactor = 1.0f;
    }

    //RGB10A2_UNORM
    output.myNormal = Dither(float4((attributes.objectNormalAndMetallic.xyz * 0.5f + 0.5f), 0.f), input.myUV);
    //RGBA8_UNORM
    output.myAlbedoAndRoughness = attributes.diffuseAndRoughness;
    output.myEmissiveAndMetallic = float4(attributes.emissiveAndAO.rgb, attributes.objectNormalAndMetallic.a);
    output.myAOAndFogFactor = float4(attributes.emissiveAndAO.aaa, fogFactor);

    return output;
}