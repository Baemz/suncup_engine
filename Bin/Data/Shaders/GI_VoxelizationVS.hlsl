#include "GI_Decl.hlsli"
#include "StandardBuffersVS.hlsli"

GI_ModelVertexToGeometry VSMain(GI_ModelVertexInput input)
{
    GI_ModelVertexToGeometry output;

    const float3x3 worldWithoutTranslation = (float3x3) toWorld;
    float3 normal;
    float3 binormal;
    float3 tangent;
    float4 vertexObjectPos;
    [branch]
    if (useAnimations == 1)
    {
        const float4 vertexWeights = input.myWeights;
        const uint4 vertexBones = uint4((uint) input.myBones.x, (uint) input.myBones.y, (uint) input.myBones.z, (uint) input.myBones.w);
        const float4x4 finalMatrix = mul(modelBones[vertexBones.x], vertexWeights.x)
                             + mul(modelBones[vertexBones.y], vertexWeights.y)
                             + mul(modelBones[vertexBones.z], vertexWeights.z)
                             + mul(modelBones[vertexBones.w], vertexWeights.w);
        vertexObjectPos = mul(finalMatrix, float4(input.myPosition.xyz, 1.0f));
        
        normal = normalize(mul((float3) input.myNormals, worldWithoutTranslation));
        binormal = normalize(mul((float3) input.myBinormal, worldWithoutTranslation));
        tangent = normalize(mul((float3) input.myTangents, worldWithoutTranslation));
    }
    else
    {
        vertexObjectPos = float4(input.myPosition.xyz, 1);
        normal = normalize(mul(worldWithoutTranslation, (float3) input.myNormals));
        binormal = normalize(mul(worldWithoutTranslation, (float3) input.myBinormal));
        tangent = normalize(mul(worldWithoutTranslation, (float3) input.myTangents));
    }
    
    const float4 vertexWorldPos = mul(toWorld, vertexObjectPos);
    const float3x3 tangentSpaceMatrix =
    {
        tangent.x, tangent.y, tangent.z,
		binormal.x, binormal.y, binormal.z,
		normal.x, normal.y, normal.z
    };

    output.myTangentSpaceMatrix = tangentSpaceMatrix;
    output.myNormal = float4(normalize(mul(worldWithoutTranslation, (float3) input.myNormals)), 1.f);
    output.myWPosition = vertexWorldPos;
    output.myUV = input.myUV;
    return output;
}