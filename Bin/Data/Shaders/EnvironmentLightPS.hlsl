#include "CommonStructs.hlsli"
#include "DeferredLightFunc.hlsli"

Texture2D ssaoTexture : register(t6);
TextureCube environmentalTexture : register(t7);

cbuffer LightData : register(b3)
{
    float4 fromLightDirectionAndMipCount;
    float4 lightColor;
    float dirLightintensity;
}

float4 ViewPosFromDepth(float depth, float2 TexCoord)
{
    float z = depth;
    float2 texpos = TexCoord * 2.0f - 1.0f;
    texpos.y *= -1.f;
    float4 clipSpacePosition = float4(texpos, z, 1.0f);
    float4 viewSpacePosition = mul(inverseCameraProjection, clipSpacePosition);
    viewSpacePosition /= viewSpacePosition.w;

    return viewSpacePosition;
}

float4 WorldPosFromDepth(float depth, float2 TexCoord)
{
    return mul(inverseCameraView, ViewPosFromDepth(depth, TexCoord));
}

float RoughToSPow(float roughness)
{
    const float val = (2.f / (roughness * roughness)) - 2.f;
    return val > 0 ? val : 0;
}

static const float k0 = 0.00098f;
static const float k1 = 0.9921f;
static const float fakeLysMaxSpecularPower = (2.f / (0.0014f * 0.0014f)) - 2.f;
static const float fMaxT = (exp2(-10.f / sqrt((2.f / (0.0014f * 0.0014f)) - 2.f)) - 0.00098f) / 0.9921f;
float GetSpecPowToMip(float fSpecPow, int nMips)
{
    const float fSmulMaxT = (exp2(-10.f / sqrt(fSpecPow)) - k0) / k1;
    return float(nMips - 1 - 0) * (1.0f - clamp(fSmulMaxT / fMaxT, 0.0f, 1.0f));
}

float3 GetAmbientDiffuse(Attributes attributes, PBRData pbrData)
{
    const float3 metallicAlbedo = attributes.diffuseAndRoughness.rgb - (attributes.diffuseAndRoughness.rgb * attributes.metallic.rgb);
    const float3 ambientLight = environmentalTexture.SampleLevel(samplerState, attributes.objectNormal.xyz, fromLightDirectionAndMipCount.w - 2).rgb;
    return (metallicAlbedo * ambientLight * (float3(1.f, 1.f, 1.f) - pbrData.reflectionFresnel));
}

float3 GetAmbientSpecular(Attributes attributes, PBRData pbrData)
{
    const float3 reflectionVector = -reflect(attributes.toEye.xyz, attributes.objectNormal.xyz);
    const float fakeLysSpecularPower = RoughToSPow(attributes.diffuseAndRoughness.a);
    const float lysMipMap = GetSpecPowToMip(fakeLysSpecularPower, (int)fromLightDirectionAndMipCount.w);
    const float3 ambientLight = environmentalTexture.SampleLevel(samplerState, reflectionVector.xyz, lysMipMap).xyz;
    return (ambientLight * pbrData.reflectionFresnel);

}

PixelOutput PSMain(TexturedVertexInput input)
{
    PixelOutput output;
    Attributes attributes;
    attributes.ambientOcclusionAndFogFactor = ambientOcclusionTexture.Sample(samplerState, input.myUV);
    
    [branch]
    if (attributes.ambientOcclusionAndFogFactor.a <= 0.01f)
    {
        output.myColor = float4(0.f, 0.f, 0.f, 0.f);
        return output;
    }

    const float depth = depthTexture.Sample(samplerState, input.myUV).r;
    attributes.worldPosition = WorldPosFromDepth(depth, input.myUV);
    attributes.diffuseAndRoughness = diffuseAndRoughnessTexture.Sample(samplerState, input.myUV);
    attributes.metallic = emissiveAndMetallicTexture.Sample(samplerState, input.myUV).aaaa;
    attributes.objectNormal = (normalTexture.Sample(samplerState, input.myUV) * 2.f - 1.f);

    attributes.toEye = normalize(cameraPosition - attributes.worldPosition);

    const float3 refFresnel = GetReflectionFresnel(attributes);
    PBRData pbrData;
    pbrData.reflectionFresnel = refFresnel;
    
    float combinedAO = attributes.ambientOcclusionAndFogFactor.r;
    float3 ambientDiffuse = float3(0.f, 0.f, 0.f);
    float3 ambientSpecular = float3(0.f, 0.f, 0.f);
    
    [branch]
    if (UseGlobalIllumination > 0)
    {
        VoxelRadiance(attributes, pbrData, ambientDiffuse, ambientSpecular, combinedAO);
    }


    output.myColor = float4((ambientDiffuse * combinedAO) + (ambientSpecular * combinedAO), 1.0f);
    return output;
}