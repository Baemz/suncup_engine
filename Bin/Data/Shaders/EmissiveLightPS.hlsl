#include "CommonStructs.hlsli"
#include "DeferredLightFunc.hlsli"

cbuffer settings : register(b11)
{
	float intensityMultiplier;
}

PixelOutput PSMain(TexturedVertexInput input)
{
    if (UseGlobalIllumination == 2)
    {
        discard;
    }
    PixelOutput output;
    float3 diffuse = diffuseAndRoughnessTexture.Sample(samplerState, input.myUV).rgb;
    float3 emissive = emissiveAndMetallicTexture.Sample(samplerState, input.myUV).rgb;
    emissive *= 6.f;
    output.myColor.rgb = emissive * diffuse;
    output.myColor.a = 1.0f;
    return output;
}