#include "CommonStructs.hlsli"
#include "StandardBuffersVS.hlsli"

ParticleToGeometry VSMain(ParticleInput input)
{
	ParticleToGeometry output;

	float4 vertexWorldPos = float4(input.myPosition.xyz, 1);
    float4 vertexViewPos = mul(cameraView, vertexWorldPos);

    output.myPosition = vertexViewPos;
    output.myWPosition = vertexWorldPos;
	output.mySize = input.mySize;
	output.myColor = input.myColor;
    output.myRotation = input.myRotation;

	return output;
}