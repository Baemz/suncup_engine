#include "CommonStructs.hlsli"

cbuffer CameraData : register(b0)
{
	float4x4 cameraOrientation;
	float4x4 toCamera;
	float4x4 projection;
	float4 camPosition;
}
cbuffer InstanceData : register(b1)
{
	float4x4 toWorld;
}

ParticleToGeometry VSMain(ParticleInput input)
{
	ParticleToGeometry output;

	float4 vertexObjectPos = float4(input.myPosition.xyz, 1);
	float4 vertexWorldPos = mul(toWorld, vertexObjectPos);
	float4 vertexViewPos = mul(toCamera, vertexWorldPos);

	output.myPosition = vertexViewPos;
	output.mySize = input.mySize;
	output.myColor = input.myColor;

	return output;
}