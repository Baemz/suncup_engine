#include "CommonStructs.hlsli"
#include "ForwardLightBuffers.hlsli"

Texture2D aoTexture : register(t5);
TextureCube environmentalTexture : register(t7);

static const float globalFogStart = 10.f;
static const float globalFogEnd = 60.0f;
static const float globalFogDist = (globalFogEnd - globalFogStart);

float RoughToSPow(float roughness)
{
    float val = (2.f / (roughness * roughness)) - 2.f;
    return val > 0 ? val : 0;
}

static const float k0 = 0.00098f;
static const float k1 = 0.9921f;
static const float fakeLysMaxSpecularPower = (2.f / (0.0014f * 0.0014f)) - 2.f;
static const float fMaxT = (exp2(-10.f / sqrt((2.f / (0.0014f * 0.0014f)) - 2.f)) - 0.00098f) / 0.9921f;
float GetSpecPowToMip(float fSpecPow, int nMips)
{
    float fSmulMaxT = (exp2(-10.f / sqrt(fSpecPow)) - k0) / k1;
    return float(nMips - 1 - 0) * (1.0f - clamp(fSmulMaxT / fMaxT, 0.0f, 1.0f));
}

float3 GetAmbientDiffuse(Attributes attributes, PBRData pbrData)
{
    float3 metallicAlbedo = attributes.diffuseAndRoughness.rgb - (attributes.diffuseAndRoughness.rgb * attributes.metallic.rgb);
    float3 ambientLight = environmentalTexture.SampleLevel(samplerState, attributes.objectNormal.xyz, toLightDirectionAndMipCount.w - 2).rgb;
    return (metallicAlbedo * ambientLight * attributes.ambientOcclusionAndFogFactor.rgb * (float3(1.f, 1.f, 1.f) - pbrData.reflectionFresnel));
}

float3 GetAmbientSpecular(Attributes attributes, PBRData pbrData)
{
    float3 reflectionVector = -reflect(attributes.toEye.xyz, attributes.objectNormal.xyz);
    float fakeLysSpecularPower = RoughToSPow(attributes.diffuseAndRoughness.a);
    float lysMipMap = GetSpecPowToMip(fakeLysSpecularPower, (int) toLightDirectionAndMipCount.w);

    float3 ambientLight = environmentalTexture.SampleLevel(samplerState, reflectionVector.xyz, lysMipMap).xyz;
    return (ambientLight * attributes.ambientOcclusionAndFogFactor.rgb * pbrData.reflectionFresnel);

}

float4 GetAmbientLightContribution(in Attributes attributes)
{
    const float3 refFresnel = GetReflectionFresnel(attributes);
    //
    PBRData pbrData;
    pbrData.reflectionFresnel = refFresnel;

    float combinedAO = attributes.ambientOcclusionAndFogFactor.r;
    
    float3 ambientDiffuse = 0.f;
    float3 ambientSpecular = 0.f;
    
    [branch]
    if (UseGlobalIllumination > 0)
    {
        VoxelRadiance(attributes, pbrData, ambientDiffuse, ambientSpecular, combinedAO);
    }

    return float4(((ambientDiffuse * combinedAO) + (ambientSpecular * combinedAO)), 1.0f);
}

PixelOutput PSMain(PBLVertexToPixel input)
{
    PixelOutput output;
    Attributes attributes;

    const float4 albedo = depthTexture.Sample(samplerState, input.myUV);
    attributes.diffuseAndRoughness.rgb = albedo.rgb;
    attributes.metallic = emissiveAndMetallicTexture.Sample(samplerState, input.myUV).rrrr;
    attributes.diffuseAndRoughness.a = diffuseAndRoughnessTexture.Sample(samplerState, input.myUV).r;
    attributes.ambientOcclusionAndFogFactor.rgb = aoTexture.Sample(samplerState, input.myUV).rrr;
    attributes.emissive.rgb = ambientOcclusionTexture.Sample(samplerState, input.myUV).rgb;
    attributes.emissive.a = 1.f;
    const float3 normalSample = ((normalTexture.Sample(samplerState, input.myUV).xyz) * 2) - 1;
    attributes.objectNormal = float4(normalize(mul(normalSample, input.myTangentSpaceMatrix)), 1.0f);
    attributes.objectNormal = Dither(attributes.objectNormal, input.myUV);
    attributes.worldPosition = float4(input.myWPosition, 1.0f);
    attributes.toEye = normalize(cameraPosition - attributes.worldPosition);

    const float4 directionalLight = GetDirectionalLightContribution(attributes);
    const float4 pointLight = GetPointLightColorPBR(attributes);
    const float4 spotLight = GetSpotLightColorPBR(attributes);
    const float4 ambientLight = GetAmbientLightContribution(attributes);

    float4 finalColor = directionalLight + pointLight + spotLight + ambientLight;
    finalColor.rgb += finalColor.rgb * (attributes.emissive.rgb * 10.f);
    finalColor.a = albedo.a;
    
    float fogFactor = 1.0f;
    fogFactor = saturate((globalFogEnd - input.myViewPos.z) / globalFogDist);

    float4 fogColor = float4(0.3f, 0.3f, 0.3f, 1.f); //float4(0.603f, 0.270f, 0.764f, 1.0f);
    
    output.myColor = fogFactor * finalColor + (1.0f - fogFactor) * fogColor;
    output.myColor = Dither(output.myColor, input.myUV);
    return output;
}