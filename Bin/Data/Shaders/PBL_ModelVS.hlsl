#include "CommonStructs.hlsli"
#include "StandardBuffersVS.hlsli"


PBLVertexToPixel VSMain(PBLVertexInput input)
{
    PBLVertexToPixel output;
    
    float4 vertexObjectPos;
    [branch]
    if (useAnimations == 1)
    {
        float4 vertexWeights = input.myWeights;
        uint4 vertexBones = uint4((uint) input.myBones.x, (uint) input.myBones.y, (uint) input.myBones.z, (uint) input.myBones.w);
        float4x4 finalMatrix = mul(modelBones[vertexBones.x], vertexWeights.x)
                             + mul(modelBones[vertexBones.y], vertexWeights.y)
                             + mul(modelBones[vertexBones.z], vertexWeights.z)
                             + mul(modelBones[vertexBones.w], vertexWeights.w);
        vertexObjectPos = mul(finalMatrix, float4(input.myPosition.xyz, 1.0f));
    }
    else
    {
        vertexObjectPos = float4(input.myPosition.xyz, 1);
    }
    
    float4 vertexWorldPos = mul(toWorld, vertexObjectPos);
    float4 vertexViewPos = mul(cameraView, vertexWorldPos);
    float4 vertexProjectionPos = mul(cameraProjection, vertexViewPos);

    float3x3 worldWithoutTranslation = (float3x3) toWorld;
    float3 normal = normalize(mul(worldWithoutTranslation, (float3) input.myNormals));
    float3 binormal = normalize(mul(worldWithoutTranslation, (float3) input.myBinormal));
    float3 tangent = normalize(mul(worldWithoutTranslation, (float3) input.myTangents));
    float3x3 tangentSpaceMatrix =
    {
        tangent.x, tangent.y, tangent.z,
		binormal.x, binormal.y, binormal.z,
		normal.x, normal.y, normal.z
    };

    output.myPosition = vertexProjectionPos;
    output.myViewPos = vertexViewPos;
    output.myWPosition = vertexWorldPos;
    output.myTangentSpaceMatrix = tangentSpaceMatrix;
    output.myUV = input.myUV;
    output.myUseFog = useFog;

    return output;
}

