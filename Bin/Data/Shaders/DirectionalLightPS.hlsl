#include "CommonStructs.hlsli"
#include "DeferredLightFunc.hlsli"

#define SHADOW_BUFFER_SIZE_X 4096.f
#define SHADOW_BUFFER_SIZE_Y 4096.f

SamplerState shadowSamplerState : register(s1);
Texture2D shadowMap : register(t8);

cbuffer LightData : register(b3)
{
    float4 toLightDirectionAndMipCount;
    float4 lightColor;
    float dirLightintensity;
}

cbuffer shadowData : register(b4)
{
    float4x4 depthProj;
    float4x4 depthView;
    float4x4 objInv;
	float2 resolution;
}

float4 ViewPosFromDepth(float depth, float2 TexCoord)
{
    float z = depth;
    float2 texpos = TexCoord * 2.0f - 1.0f;
    texpos.y *= -1.f;
    float4 clipSpacePosition = float4(texpos, z, 1.0f);
    float4 viewSpacePosition = mul(inverseCameraProjection, clipSpacePosition);
    viewSpacePosition /= viewSpacePosition.w;

    return viewSpacePosition;
}

float4 WorldPosFromDepth(float depth, float2 TexCoord)
{
    return mul(inverseCameraView, ViewPosFromDepth(depth, TexCoord));
}

static const float texelSizeH = 1.f / SHADOW_BUFFER_SIZE_X;
static const float texelSizeV = 1.f / SHADOW_BUFFER_SIZE_Y;
static const float2 texelOffset[8] = {  float2(0, -texelSizeV),
										float2(texelSizeH, 0),
										float2(0, texelSizeV),
										float2(-texelSizeH, 0),
										float2(texelSizeH, texelSizeV),
										float2(-texelSizeH, -texelSizeV),
										float2(-texelSizeH, texelSizeV),
										float2(texelSizeH, -texelSizeV) };
static const float bias = 0.00025f;
float DetermineShadow(float2 projectTexCoord)
{
    if (saturate(projectTexCoord.x) == projectTexCoord.x && saturate(projectTexCoord.y) == projectTexCoord.y)
    {
        float depthValue = 0.f;
        depthValue += shadowMap.Sample(shadowSamplerState, projectTexCoord).r;
        depthValue += shadowMap.Sample(shadowSamplerState, projectTexCoord + texelOffset[0]).r;
        depthValue += shadowMap.Sample(shadowSamplerState, projectTexCoord + texelOffset[1]).r;
        depthValue += shadowMap.Sample(shadowSamplerState, projectTexCoord + texelOffset[2]).r;
        depthValue += shadowMap.Sample(shadowSamplerState, projectTexCoord + texelOffset[3]).r;
        depthValue += shadowMap.Sample(shadowSamplerState, projectTexCoord + texelOffset[4]).r;
        depthValue += shadowMap.Sample(shadowSamplerState, projectTexCoord + texelOffset[5]).r;
        depthValue += shadowMap.Sample(shadowSamplerState, projectTexCoord + texelOffset[6]).r;
        depthValue += shadowMap.Sample(shadowSamplerState, projectTexCoord + texelOffset[7]).r;
    
        depthValue /= 9.0f;
        return depthValue;
    }
    else
    {
        return 99999.99f;
    }
}

PixelOutput PSMain(TexturedVertexInput input)
{
    PixelOutput output;
    Attributes attributes;
    attributes.ambientOcclusionAndFogFactor = ambientOcclusionTexture.Sample(samplerState, input.myUV);
        
    [branch]
    if (attributes.ambientOcclusionAndFogFactor.a <= 0.01f)
    {
        discard;
    }
    else if (UseGlobalIllumination == 2)
    {
        discard;
    }

    const float depth = depthTexture.Sample(samplerState, input.myUV).r;
    attributes.worldPosition = WorldPosFromDepth(depth, input.myUV);
    attributes.diffuseAndRoughness = diffuseAndRoughnessTexture.Sample(samplerState, input.myUV);
    attributes.metallic = emissiveAndMetallicTexture.Sample(samplerState, input.myUV).aaaa;
    attributes.objectNormal = normalTexture.Sample(samplerState, input.myUV) * 2.f - 1.f;
    attributes.objectNormal = Dither(attributes.objectNormal, input.myUV);
    attributes.toEye = normalize(cameraPosition - attributes.worldPosition);

    const float4 vertexPos = mul(depthProj, mul(depthView, attributes.worldPosition));
    float2 projectTexCoord;
    projectTexCoord.x = vertexPos.x / vertexPos.w / 2.0f + 0.5f;
    projectTexCoord.y = -vertexPos.y / vertexPos.w / 2.0f + 0.5f;

    const float lightDepthValue = ((vertexPos.z / vertexPos.w) - bias);
    const float depthValue = DetermineShadow(projectTexCoord);

    

    [branch]
    if (lightDepthValue > depthValue)
    {
        output.myColor = float4(0.f, 0.f, 0.f, 0.f);
        return output;
    }
    const float3 lightDir = normalize(toLightDirectionAndMipCount.xyz);
    const float3 lambert = GetLambert(attributes, lightDir);
    const float3 fresnel = GetFresnel(attributes, lightDir);
    const float distribution = GetDistribution(attributes, lightDir);
    const float visibility = GetVisibility(attributes, lightDir);

    PBRData pbrData;
    pbrData.lightColor = lightColor.rgb * dirLightintensity;
    pbrData.fresnel = fresnel;
    pbrData.lambert = lambert;
    pbrData.distribution = distribution;
    pbrData.visibility = visibility;
    
    const float3 directDiffuse = GetDirectDiffuse(attributes, pbrData);
    const float3 directSpecular = GetDirectSpecular(attributes, pbrData);
    
    output.myColor = float4((directDiffuse + directSpecular), 1.0f);
    return output;
}