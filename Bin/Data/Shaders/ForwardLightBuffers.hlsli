#include "DeferredLightFunc.hlsli"

#define MAX_LIGHTS 8
#define SHADOW_BUFFER_SIZE_X 4096.f
#define SHADOW_BUFFER_SIZE_Y 4096.f

Texture2D shadowMap : register(t8);
SamplerState shadowSamplerState : register(s1);

cbuffer DirectionalLightData : register(b5)
{
    float4 toLightDirectionAndMipCount;
    float4 lightColor;
    float dirLightintensity;
}
cbuffer DirectionalLightShadowData : register(b6)
{
    float4x4 depthProj;
    float4x4 depthView;
    float4x4 objInv;
    float2 resolution;
}

cbuffer PointLightData : register(b7)
{
    uint PLNumLights;
    uint3 __PLPADDING__;
    struct PL
    {
        float4 PLPosition;
        float3 PLColor;
        float PLRange;
        float PLIntensity;
        float3 __PLPADDING2__;
    } pointLights[MAX_LIGHTS];
}

cbuffer SpotLightData : register(b8)
{
    uint SLNumLights;
    uint3 __SLPADDING__;
    struct SL
    {
        float4 SLPosition;
        float3 SLColor;
        float SLAngle;
        float3 SLDirection;
        float SLRange;
        float SLIntensity;
        float3 __SLPADDING2__;
    } spotLights[MAX_LIGHTS];
}

float4 GetPointLightColorPBR(Attributes attributes)
{
    if (UseGlobalIllumination == 2)
    {
        float4(0.f, 0.f, 0.f, 0.f);
    }
    float4 returnColor = float4(0.f, 0.f, 0.f, 1.f);
    for (unsigned int index = 0; index < PLNumLights; ++index)
    {
        float3 toLight = pointLights[index].PLPosition.xyz - attributes.worldPosition.xyz;
        float toLightDistance = distance(pointLights[index].PLPosition.xyz, attributes.worldPosition.xyz);
        toLight = normalize(toLight);
        float lightRange = pointLights[index].PLRange * pointLights[index].PLRange;
        float toLightDist2 = toLightDistance * toLightDistance;

        float linearAttenuation = saturate(1.f - (toLightDist2 / lightRange));
        float lambertAttenuation = saturate(dot(attributes.objectNormal.xyz, toLight));
        float physicalAttenuation = 1.f / toLightDistance;

        float attenuation = lambertAttenuation * linearAttenuation * physicalAttenuation;

        float3 lightColor = (pointLights[index].PLColor.rgb * (pointLights[index].PLIntensity));
        float4 resColor = float4(attributes.diffuseAndRoughness.rgb * attenuation * lightColor, 1.0f);
        
        float3 lightDir = toLight;
        float3 lambert = GetLambert(attributes, lightDir);
        float3 fresnel = GetFresnel(attributes, lightDir);
        float visibility = GetVisibility(attributes, lightDir);
        float distribution = GetDistribution(attributes, lightDir);

        PBRData pbrData;
        pbrData.lightColor = lightColor;
        pbrData.fresnel = fresnel;
        pbrData.lambert = lambert;
        pbrData.distribution = distribution;
        pbrData.visibility = visibility;
    
        float3 directDiffuse = GetDirectDiffuse(attributes, pbrData);
        float3 directSpec = GetDirectSpecular(attributes, pbrData);
        resColor *= float4(directDiffuse + directSpec, 1.0f);
        returnColor.rgb += (resColor.rgb * 2.f);
    }

    return returnColor;
}
float4 GetPointLightContribution(Attributes attributes)
{
    if (UseGlobalIllumination == 2)
    {
        float4(0.f, 0.f, 0.f, 0.f);
    }
    float4 returnColor = float4(0.f, 0.f, 0.f, 1.f);
    for (unsigned int index = 0; index < PLNumLights; ++index)
    {
        float3 toLight = pointLights[index].PLPosition.xyz - attributes.worldPosition.xyz;
        float toLightDistance = distance(pointLights[index].PLPosition.xyz, attributes.worldPosition.xyz);
        toLight = normalize(toLight);
        float lightRange = pointLights[index].PLRange * pointLights[index].PLRange;
        float toLightDist2 = toLightDistance * toLightDistance;

        float linearAttenuation = saturate(1.f - (toLightDist2 / lightRange));
        float lambertAttenuation = saturate(dot(attributes.objectNormal.xyz, toLight));
        float physicalAttenuation = 1.f / toLightDistance;

        float attenuation = lambertAttenuation * linearAttenuation * physicalAttenuation;

        float3 lightColor = (pointLights[index].PLColor.rgb * (pointLights[index].PLIntensity));
        float4 resColor = float4(attributes.diffuseAndRoughness.rgb * attenuation * lightColor, 1.0f);
        returnColor.rgb += (resColor.rgb * 2.f);
    }

    return returnColor;
}

float4 GetSpotLightColorPBR(Attributes attributes)
{
    if (UseGlobalIllumination == 2)
    {
        float4(0.f, 0.f, 0.f, 0.f);
    }
    float4 returnColor = float4(0.f, 0.f, 0.f, 1.f);
	[unroll]
    for (unsigned int index = 0; index < SLNumLights; ++index)
    {
        float3 toLight = spotLights[index].SLPosition.xyz - attributes.worldPosition.xyz;
        float toLightDistance = distance(spotLights[index].SLPosition.xyz, attributes.worldPosition.xyz);
        toLight = normalize(toLight);
        float lightRange = spotLights[index].SLRange * spotLights[index].SLRange;
        float toLightDist2 = toLightDistance * toLightDistance;

        float lambertAttenuation = saturate(dot(attributes.objectNormal.xyz, toLight));
        float linearAttenuation = saturate(1.f - (toLightDist2 / lightRange));
        float physicalAttenuation = 1.f / toLightDistance;
    
        float cutPoint = 1.f - ((spotLights[index].SLAngle / 2.f) / 180.f);
        float a = saturate(dot(normalize(-toLight), normalize(spotLights[index].SLDirection)));
        float angleAttenuation = saturate((a - cutPoint) * (1.f / (1.f - cutPoint)));

        float attenuation = lambertAttenuation * linearAttenuation * physicalAttenuation * angleAttenuation;
        float3 lightColor = (spotLights[index].SLColor.rgb * (spotLights[index].SLIntensity));
        float4 resColor = float4(attributes.diffuseAndRoughness.rgb * attenuation * lightColor, 1.0f);
        
        float3 lightDir = toLight;
        float3 lambert = GetLambert(attributes, lightDir);
        float3 fresnel = GetFresnel(attributes, lightDir);
        float visibility = GetVisibility(attributes, lightDir);
        float distribution = GetDistribution(attributes, lightDir);

        PBRData pbrData;
        pbrData.lightColor = lightColor;
        pbrData.fresnel = fresnel;
        pbrData.lambert = lambert;
        pbrData.distribution = distribution;
        pbrData.visibility = visibility;
    
        float3 directDiffuse = GetDirectDiffuse(attributes, pbrData);
        float3 directSpec = GetDirectSpecular(attributes, pbrData);
        resColor *= float4(directDiffuse + directSpec, 1.0f);
        returnColor.rgb += (resColor.rgb * 5.0f);
    }

    return returnColor;
}
float4 GetSpotLightContribution(Attributes attributes)
{
    if (UseGlobalIllumination == 2)
    {
        float4(0.f, 0.f, 0.f, 0.f);
    }
    float4 returnColor = float4(0.f, 0.f, 0.f, 1.f);
	[unroll]
    for (unsigned int index = 0; index < SLNumLights; ++index)
    {
        float3 toLight = spotLights[index].SLPosition.xyz - attributes.worldPosition.xyz;
        float toLightDistance = distance(spotLights[index].SLPosition.xyz, attributes.worldPosition.xyz);
        toLight = normalize(toLight);
        float lightRange = spotLights[index].SLRange * spotLights[index].SLRange;
        float toLightDist2 = toLightDistance * toLightDistance;

        float lambertAttenuation = saturate(dot(attributes.objectNormal.xyz, toLight));
        float linearAttenuation = saturate(1.f - (toLightDist2 / lightRange));
        float physicalAttenuation = 1.f / toLightDistance;
    
        float cutPoint = 1.f - ((spotLights[index].SLAngle / 2.f) / 180.f);
        float a = saturate(dot(normalize(-toLight), normalize(spotLights[index].SLDirection)));
        float angleAttenuation = saturate((a - cutPoint) * (1.f / (1.f - cutPoint)));

        float attenuation = lambertAttenuation * linearAttenuation * physicalAttenuation * angleAttenuation;
        float3 lightColor = (spotLights[index].SLColor.rgb * (spotLights[index].SLIntensity));
        float4 resColor = float4(attributes.diffuseAndRoughness.rgb * attenuation * lightColor, 1.0f);
        returnColor.rgb += (resColor.rgb * 5.0f);
    }

    return returnColor;
}

static const float texelSizeH = 1.f / SHADOW_BUFFER_SIZE_X;
static const float texelSizeV = 1.f / SHADOW_BUFFER_SIZE_Y;
static const float2 texelOffset[8] =
{
    float2(0, -texelSizeV),
										float2(texelSizeH, 0),
										float2(0, texelSizeV),
										float2(-texelSizeH, 0),
										float2(texelSizeH, texelSizeV),
										float2(-texelSizeH, -texelSizeV),
										float2(-texelSizeH, texelSizeV),
										float2(texelSizeH, -texelSizeV)
};
static const float bias = 0.00025f;
float DetermineShadow(float2 projectTexCoord)
{
    if (saturate(projectTexCoord.x) == projectTexCoord.x && saturate(projectTexCoord.y) == projectTexCoord.y)
    {
        float depthValue = 0.f;
        depthValue += shadowMap.Sample(shadowSamplerState, projectTexCoord).r;
        depthValue += shadowMap.Sample(shadowSamplerState, projectTexCoord + texelOffset[0]).r;
        depthValue += shadowMap.Sample(shadowSamplerState, projectTexCoord + texelOffset[1]).r;
        depthValue += shadowMap.Sample(shadowSamplerState, projectTexCoord + texelOffset[2]).r;
        depthValue += shadowMap.Sample(shadowSamplerState, projectTexCoord + texelOffset[3]).r;
        depthValue += shadowMap.Sample(shadowSamplerState, projectTexCoord + texelOffset[4]).r;
        depthValue += shadowMap.Sample(shadowSamplerState, projectTexCoord + texelOffset[5]).r;
        depthValue += shadowMap.Sample(shadowSamplerState, projectTexCoord + texelOffset[6]).r;
        depthValue += shadowMap.Sample(shadowSamplerState, projectTexCoord + texelOffset[7]).r;
    
        depthValue /= 9.0f;
        return depthValue;
    }
    return 99999.99f;
}

float4 GetDirectionalLightContribution(in Attributes attributes)
{
    if (UseGlobalIllumination == 2)
    {
        return float4(0.f, 0.f, 0.f, 0.f);
    }
    const float4 vertexPos = mul(depthProj, mul(depthView, attributes.worldPosition));
    float2 projectTexCoord;
    projectTexCoord.x = vertexPos.x / vertexPos.w / 2.0f + 0.5f;
    projectTexCoord.y = -vertexPos.y / vertexPos.w / 2.0f + 0.5f;

    const float lightDepthValue = ((vertexPos.z / vertexPos.w) - bias);
    const float depthValue = DetermineShadow(projectTexCoord);
    
    [branch]
    if (lightDepthValue > depthValue)
    {
        return float4(0.f, 0.f, 0.f, 0.f);
    }

    float3 lightDir = normalize(toLightDirectionAndMipCount.xyz);
    float3 lambert = GetLambert(attributes, lightDir);
    float3 fresnel = GetFresnel(attributes, lightDir);
    float distribution = GetDistribution(attributes, lightDir);
    float visibility = GetVisibility(attributes, lightDir);

    PBRData pbrData;
    pbrData.lightColor = lightColor.rgb * dirLightintensity;
    pbrData.fresnel = fresnel;
    pbrData.lambert = lambert;
    pbrData.distribution = distribution;
    pbrData.visibility = visibility;
    
    float3 directDiffuse = GetDirectDiffuse(attributes, pbrData);
    float3 directSpecular = GetDirectSpecular(attributes, pbrData);
    return float4((directDiffuse + directSpecular), 1.0f);
}

