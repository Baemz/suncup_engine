#include "CommonStructs.hlsli"
#include "StandardBuffersPS.hlsli"

SamplerState samplerState : register(s0);

cbuffer Resolution : register(b3)
{
    float2 myResolution;
}
//#define FX_SSAO
#ifdef FX_SSAO

Texture2D posTexture : register(t0);
Texture2D normalTexture : register(t1);
Texture2D randomNormalTexture : register(t6);

static const float globalScale = 0.8f;
static const float globalIntensity = 4.0f;       
static const float globalBias = 0.65f;
static const float globalRadius = 0.05f;
static const float2 globalRandomTextureSize = float2(64.f, 64.f);
static const float2 vec[4] = { float2(1.f, 0.f), float2(-1.f, 0.f), float2(0.f, 1.f), float2(0.f, -1.f) };

float4 ViewPosFromDepth(float depth, float2 TexCoord)
{
    float z = depth;
    float2 texpos = TexCoord * 2.0f - 1.0f;
    texpos.y *= -1.f;
    float4 clipSpacePosition = float4(texpos, z, 1.0f);
    float4 viewSpacePosition = mul(inverseCameraProjection, clipSpacePosition);
    viewSpacePosition /= viewSpacePosition.w;

    return viewSpacePosition;
}

float DoAmbientOcclusion(float2 tcoord, float2 uv, float3 pos, float3 cnorm)
{
    float depth = posTexture.Sample(samplerState, tcoord + uv).r;
    float3 position = (ViewPosFromDepth(depth, tcoord + uv).xyz) - pos;

    const float rangeCheck = ((position.z - pos.z) < globalRadius) ? 1.f : 0.f;
    return max(0.0f, dot(cnorm, normalize(position)) - globalBias) * (1.0 / (1.0 + (length(position) * globalScale))) * globalIntensity * rangeCheck;
}

PixelOutput PS_FXSSAO(TexturedVertexToPixel input)
{
    discard;
    PixelOutput output;
    output.myColor.rgb = float3(1.f, 1.f, 1.f);

    float3 sNormal = (normalTexture.Sample(samplerState, input.myUV).xyz) * 2.f - 1.f;
    float3x3 viewWithoutTranslation = (float3x3)cameraView;
    float3 normal = mul(viewWithoutTranslation, sNormal).xyz;

    float depth = posTexture.Sample(samplerState, input.myUV).r;
    float3 position = ViewPosFromDepth(depth, input.myUV).xyz;
    float2 random = normalize(randomNormalTexture.Sample(samplerState, myResolution * input.myUV / globalRandomTextureSize).xy);

    float ambientOcclusion = 0.f;
    float sampleRadius = globalRadius / position.z;

    uint iterations = 4;
    //[unroll]
    for (uint i = 0; i < iterations; ++i)
    {
        float2 firstCoord = reflect(vec[i], random) * sampleRadius;
        float2 secondCoord = float2((firstCoord.x * 0.707f) - (firstCoord.y * 0.707f), (firstCoord.x * 0.707f) - (firstCoord.y * 0.707f));
       
        ambientOcclusion += DoAmbientOcclusion(input.myUV, firstCoord * 0.25f, position, normal);
        ambientOcclusion += DoAmbientOcclusion(input.myUV, secondCoord * 0.5f, position, normal);
        ambientOcclusion += DoAmbientOcclusion(input.myUV, firstCoord * 0.75f, position, normal);
        ambientOcclusion += DoAmbientOcclusion(input.myUV, secondCoord, position, normal);
    }

    ambientOcclusion /= (float) iterations * 4.f;
    output.myColor.r = 1.f - (ambientOcclusion);
    return output;
}
#endif

//#define FX_SSAOBLUR
#ifdef FX_SSAOBLUR
static const int globalBlurSize = 4;
static const float2 hlim = float2(float(-globalBlurSize) * 0.5f + 0.5f, float(-globalBlurSize) * 0.5f + 0.5f);
Texture2D ssaoTexture : register(t0);
static const float2 texelSize = float2((1.f / myResolution.x), (1.f / myResolution.y));
PixelOutput PS_FXSSAOBlur(TexturedVertexToPixel input)
{
discard;
    PixelOutput output;
    
    float result = 0.f;
    //[unroll]
    for (int j = 0; j < globalBlurSize; ++j)
    {
        for (int i = 0; i < globalBlurSize; ++i)
        {
            float2 offset = (hlim + float2(float(i), float(j))) * texelSize;
            result += ssaoTexture.Sample(samplerState, input.myUV + offset).r;
        }
    }
    output.myColor.r = result / float(globalBlurSize * globalBlurSize);
    return output;
}
#endif