#include "CommonStructs.hlsli"
#include "Dither.hlsli"

SamplerState samplerState : register(s0);
Texture2D sceneTexture : register(t0);
Texture2D aoAndFogTexture : register(t4);


PixelOutput PSMain(TexturedVertexInput input)
{
    PixelOutput output;
    float4 sceneColor = sceneTexture.Sample(samplerState, input.myUV);
    float fogFactor = aoAndFogTexture.Sample(samplerState, input.myUV).a;
    float4 fogColor = float4(0.3f, 0.3f, 0.3f, 1.f); //float4(0.603f, 0.270f, 0.764f, 1.0f);
    
    output.myColor = fogFactor * sceneColor + (1.0f - fogFactor) * fogColor;
    output.myColor = Dither(output.myColor, input.myUV);
    return output;
}