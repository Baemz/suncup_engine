
#define __VOXEL_DATABUFFER_SLOT b13
//#define VOXEL_CENTER float3(0.f, 0.f, 0.f)
#define VOXEL_EXTENT float3(0.f, 0.f, 0.f)
#define VOXEL_CONE_QUALITY 8
#define VOXEL_CONE_QUALITY_INVERSE (1.0f / VOXEL_CONE_QUALITY)

static const float __hdrRange = 10.0f;

struct VoxelizedEncodedData
{
    uint col;
    uint norm;
};

cbuffer VOXELDATA : register(__VOXEL_DATABUFFER_SLOT)
{
    float3 VoxelCenter;
    uint VoxelCenterChangedThisFrame;
    uint VoxelGridSize;
    float VoxelSize;
    uint UseGlobalIllumination;
    uint VoxelRadianceMips;
    float ConeAngleDiffuse;
    float ConeAngleSpecular;
    float VoxelSizeInversed;
    float VoxelGridSizeInversed;
}

uint EncodeColor(in float4 color)
{
	// normalize color to LDR
    float hdr = length(color.rgb);
    color.rgb /= hdr;

	// encode LDR color and HDR range
    uint3 iColor = uint3(color.rgb * 255.0f);
    uint iHDR = (uint) (saturate(hdr / __hdrRange) * 127);
    uint colorMask = (iHDR << 24u) | (iColor.r << 16u) | (iColor.g << 8u) | iColor.b;

	// encode alpha into highest bit
    uint iAlpha = (color.a > 0 ? 1u : 0u);
    colorMask |= iAlpha << 31u;

    return colorMask;
}

// Decode 32 bit uint into HDR color with 1 bit alpha
float4 DecodeColor(in uint colorMask)
{
    float hdr;
    float4 color;

    hdr = (colorMask >> 24u) & 0x0000007f;
    color.r = (colorMask >> 16u) & 0x000000ff;
    color.g = (colorMask >> 8u) & 0x000000ff;
    color.b = colorMask & 0x000000ff;

    hdr /= 127.0f;
    color.rgb /= 255.0f;

    color.rgb *= hdr * __hdrRange;

    color.a = (colorMask >> 31u) & 0x00000001;

    return color;
}

uint EncodeNormal(in float3 normal)
{
    int3 iNormal = int3(normal * 255.0f);
    uint3 iNormalSigns;
    iNormalSigns.x = (iNormal.x >> 5) & 0x04000000;
    iNormalSigns.y = (iNormal.y >> 14) & 0x00020000;
    iNormalSigns.z = (iNormal.z >> 23) & 0x00000100;
    iNormal = abs(iNormal);
    uint normalMask = iNormalSigns.x | (iNormal.x << 18) | iNormalSigns.y | (iNormal.y << 9) | iNormalSigns.z | iNormal.z;
    return normalMask;
}

float3 DecodeNormal(in uint normalMask)
{
    int3 iNormal;
    iNormal.x = (normalMask >> 18) & 0x000000ff;
    iNormal.y = (normalMask >> 9) & 0x000000ff;
    iNormal.z = normalMask & 0x000000ff;
    int3 iNormalSigns;
    iNormalSigns.x = (normalMask >> 25) & 0x00000002;
    iNormalSigns.y = (normalMask >> 16) & 0x00000002;
    iNormalSigns.z = (normalMask >> 7) & 0x00000002;
    iNormalSigns = 1 - iNormalSigns;
    float3 normal = float3(iNormal) / 255.0f;
    normal *= iNormalSigns;
    return normal;
}

inline uint Flatten3D(uint3 coord, uint3 dim)
{
    return (coord.z * dim.x * dim.y) + (coord.y * dim.x) + coord.x;
}
inline uint3 Unflatten3D(uint idx, uint3 dim)
{
    const uint z = idx / (dim.x * dim.y);
    idx -= (z * dim.x * dim.y);
    const uint y = idx / dim.x;
    const uint x = idx % dim.x;
    return uint3(x, y, z);
}

inline float3 CreateCube(in uint vertexID)
{
    const uint b = 1 << vertexID;
    return float3((0x287a & b) != 0, (0x02af & b) != 0, (0x31e3 & b) != 0);
}

struct GI_ModelVertexInput
{
    float4 myPosition : POSITION;
    float4 myNormals : NORMAL;
    float4 myTangents : TANGENT;
    float4 myBinormal : BINORMAL;
    float2 myUV : UV;
    float4 myBones : BONE;
    float4 myWeights : WEIGHT;
};

struct GI_ModelVertexToGeometry
{
    float3x3 myTangentSpaceMatrix : TANGENTSPACEMATRIX;
    float4 myNormal : NORMAL;
    float4 myWPosition : SV_POSITION;
    float2 myUV : UV;
};

struct GI_ModelGeometryToPixel
{
    float3x3 myTangentSpaceMatrix : TANGENTSPACEMATRIX;
    float4 myPosition : SV_POSITION;
    float4 myNormal : NORMAL;
    float3 myWPosition : WPOSITION;
    float2 myUV : UV;
};