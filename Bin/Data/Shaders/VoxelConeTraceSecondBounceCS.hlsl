#include "VoxelConeTrace.hlsli"
Texture3D<float4> voxelRadiance : register(t0);
StructuredBuffer<VoxelizedEncodedData> encodedRadiance : register(t1);
RWTexture3D<float4> outRadiance : register(u0);

[numthreads(64, 1, 1)]
void CSMain(uint3 dispThreadID : SV_DispatchThreadID)
{
    const uint3 writecoord = Unflatten3D(dispThreadID.x, VoxelGridSize);
    float4 currentRadiance = voxelRadiance[writecoord];

    if (currentRadiance.a > 0)
    {
        float3 normal = DecodeNormal(encodedRadiance[dispThreadID.x].norm);

        float3 position = ((float3) writecoord + 0.5f) * VoxelGridSizeInversed;
        position = position * 2 - 1;
        position.y *= -1;
        position *= VoxelSize;
        position *= VoxelGridSize;
        position += VoxelCenter;

        float4 radiance = ConeTraceRadiance(voxelRadiance, position, normal);

        outRadiance[writecoord] = currentRadiance + (float4(radiance.rgb, 0) * 0.75f);
    }
    else
    {
        outRadiance[writecoord] = 0;
    }
}