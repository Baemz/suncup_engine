﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;


public class ItemScroller : MonoBehaviour
{



    [MenuItem("ObjectScroller/Select Next %Q")]
    static void SelectNext()
    {
        Cycle(1);
    }

    [MenuItem("ObjectScroller/Select Previous %W")]
    static void SelectPrevious()
    {
        Cycle(-1);
    }

    static void Cycle(int aCycleAmount )
    {
        Object RootPrefab = PrefabUtility.FindPrefabRoot(Selection.activeTransform.gameObject);

        RootPrefab = PrefabUtility.GetPrefabParent(RootPrefab);

        string PrefabPath = AssetDatabase.GetAssetPath(RootPrefab);

        string PrefabFolderPath = Directory.GetParent(PrefabPath).ToString();

        //Debug.Log(PrefabFolderPath);

        List<string> prefabList = new List<string>(Directory.GetFiles(PrefabFolderPath));

        int ObjectIndex = -1;

        List<string> MetaFileRemoveList = new List<string>();

        foreach(string path in prefabList)
        {                       
            if(path.Contains("meta"))
            {
                MetaFileRemoveList.Add(path);
            }
        }

        foreach(string metaPath in MetaFileRemoveList)
        {
            prefabList.Remove(metaPath);
        }

        MetaFileRemoveList.Clear();

        foreach (string path in prefabList)
        {
            if (path.Contains(RootPrefab.name))
            {
                ObjectIndex = prefabList.IndexOf(path);
                Debug.Log(ObjectIndex);
            }
            //Debug.Log(path);
        }

        ObjectIndex += aCycleAmount;

        //Debug.Log(prefabList.Count);

        if(ObjectIndex > prefabList.Count - 1 )
        {
            ObjectIndex = 0;
            Debug.Log("OVER MAX");
        }

        if(ObjectIndex < 0)
        {
            ObjectIndex = prefabList.Count - 1;
            Debug.Log("LESS THAN ZERO");
        }

        //Debug.Log(ObjectIndex);

        //Debug.Log(prefabList[ObjectIndex]);

        Object newObjectPrefab = AssetDatabase.LoadAssetAtPath(prefabList[ObjectIndex], typeof(GameObject)) as GameObject;

        //Material newMat = AssetDatabase.LoadAssetAtPath(("ASSETS"), typeof(Material)) as Material;

        GameObject newObject = PrefabUtility.InstantiatePrefab(newObjectPrefab) as GameObject;

        newObject.transform.position = Selection.activeTransform.position;
        newObject.transform.rotation = Selection.activeTransform.rotation;
        newObject.transform.localScale = Selection.activeTransform.localScale;

        newObject.transform.parent = Selection.activeTransform.parent;

        DestroyImmediate(Selection.activeGameObject);

        Selection.activeGameObject = newObject;







    }
    
}    


