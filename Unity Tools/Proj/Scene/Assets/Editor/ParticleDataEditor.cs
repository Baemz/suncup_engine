﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(ParticleDataExport))]
public class ParticleDataEditor : Editor
{

    public override void OnInspectorGUI()
    {
        ParticleDataExport myScript = (ParticleDataExport)target;

        myScript.fileNameNoExt = EditorGUILayout.TextField("FileName (No extension)", myScript.fileNameNoExt);
        myScript.particleTexture = EditorGUILayout.TextField("Particle texture (No extension)", myScript.particleTexture);
        myScript.externalFilePath = EditorGUILayout.TextField("Folder Save-Path", myScript.externalFilePath);
        //myScript.MaxHealth = EditorGUILayout.IntField("Max Health", myScript.MaxHealth);
        //myScript.MaxSpeed = EditorGUILayout.FloatField("Max Speed", myScript.MaxSpeed);
        //myScript.TimeToAccelerate = EditorGUILayout.FloatField("Time To Accelerate", myScript.TimeToAccelerate);

        if (GUILayout.Button("Save as Json"))
        {
            myScript.CreateParticleFile();

        }

    }

}
