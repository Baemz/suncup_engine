﻿using System.IO;
using System.Text;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.AI;
using Houdini;

public class NavMeshWindow : EditorWindow
{
    public static NavMeshWindow curWindow;
    public static int subdivWindowParmInt = 0;
    public static Mesh mesh;

    public static GameObject curObj;
    public static bool navMeshEditMode = false;
    public static HoudiniApiAssetAccessor curAccessor = null;

    [MenuItem("NavMesh/Launch Nav Mesh Editor",false,5)]
    public static void InitWindow ()
    {
        curWindow = (NavMeshWindow)EditorWindow.GetWindow<NavMeshWindow>();

        GUIContent titleContent = new GUIContent("Nav Mesh Editor");
        curWindow.titleContent = titleContent;

	}

    void OnGUI()
    {
        mesh = new Mesh();
        //        EditorGUILayout.LabelField("vart visas denna");
        if (GUILayout.Button("EditNavMesh", GUILayout.Height(40)))
        {
            ExtractNavMesh();
            EditNavMesh();
            navMeshEditMode = true;
        }


        if(navMeshEditMode)
        {
            EditorGUILayout.LabelField("Change Subdivition in inspector window");
            //   Debug.Log("navMeshEditMode ->" + navMeshEditMode);
            //   EditorGUI.BeginChangeCheck();
            //   subdivWindowParmInt = EditorGUILayout.IntSlider("Subdivition", subdivWindowParmInt, 2, 10);
            //   if(EditorGUI.EndChangeCheck())
            //   {
            //       Debug.Log("EditorGUI.EndChangeCheck");
            //       Debug.Log(curAccessor.getParmIntValue("navMeshGeo", 0));
            //       subdivWindowParmInt = curAccessor.getParmIntValue("navMeshGeo", 0);
            //       Debug.Log("imellan");
            //       curAccessor.setParmIntValue("navMeshGeo", 0, subdivWindowParmInt);
            //       Debug.Log(string.Format("changed to {0}", subdivWindowParmInt));
            //   }
            if (GUILayout.Button("Export Mesh To Maya", GUILayout.Height(40)))
            {
                SaveEditedNavMesh();
                StartMaya();
                navMeshEditMode = false;
                this.Close();
            }


        }


        
    }

    static void ExtractNavMesh()
    {
        UnityEngine.AI.NavMeshTriangulation triangulatedNavMesh = UnityEngine.AI.NavMesh.CalculateTriangulation();
        //Mesh mesh = new Mesh();
        //mesh.name = "ExportedNavMesh";
        mesh = new Mesh();
        mesh.name = EditorSceneManager.GetActiveScene().name;
        mesh.vertices = triangulatedNavMesh.vertices;
        mesh.triangles = triangulatedNavMesh.indices;

        string filename = Application.dataPath + "/navMesh/" + Path.GetFileNameWithoutExtension(EditorSceneManager.GetActiveScene().name) + "navMesh.obj";
        MeshToFile(mesh, filename);
        Debug.Log("NavMesh exported as '" + filename + "'");
        AssetDatabase.Refresh();
    }

    static void EditNavMesh()
    {

        string assetPath = Application.dataPath + "Assets/Houdini/NavRemesher.otl";
        curObj = new GameObject("NavMeshTool");
        HoudiniAssetOTL asset = curObj.AddComponent<HoudiniAssetOTL>();
        curAccessor = HoudiniApiAssetAccessor.getAssetAccessor(curObj);
        asset.prAssetType = HoudiniAsset.AssetType.TYPE_OTL;
        asset.prAssetPath = assetPath;

        asset.buildAll();

        HoudiniParms curParms = asset.prParms;
        HAPI_ParmInfo[] parmInfos = curParms.prParms;




        if (parmInfos.Length > 0)
        {
            for (int i = 0; i < parmInfos.Length; i++)
            {
                if (parmInfos[i].type == HAPI_ParmType.HAPI_PARMTYPE_INT)
                {

                    //Debug.Log("parameter name-> " + parmInfos[i].name); //navMeshGeo
                    int subDivDefault = curAccessor.getParmIntValue(parmInfos[i].name, 0);
                    subDivDefault = 3;
                    curAccessor.setParmIntValue(parmInfos[i].name, 0, subDivDefault);


                }
                if (parmInfos[i].type == HAPI_ParmType.HAPI_PARMTYPE_PATH_FILE_GEO)
                {
                    // Debug.Log("parameter name-> " + parmInfos[i].name); //navMeshGeo
                    // Debug.Log("parameter type-> " + parmInfos[i].type);
                    //     Debug.Log("parameter label-> " + parmInfos[i].label);
                    //     Debug.Log("parameter getType-> " + parmInfos[i].GetType());
                    var navMeshGeoPath = curAccessor.getParmStringValue(parmInfos[i].name, 0);
                    navMeshGeoPath = Application.dataPath + "/navMesh/" + EditorSceneManager.GetActiveScene().name + "navMesh.obj";
                    curAccessor.setParmStringValue(parmInfos[i].name, 0, navMeshGeoPath);

                }
            }
        }
        Selection.activeObject = curObj;

    }

    static void SaveEditedNavMesh()
    {
        mesh = null;
        GameObject navMeshInScene = GameObject.Find(EditorSceneManager.GetActiveScene().name + "_0");
        Debug.Log("navMeshInScene :" + navMeshInScene);
        mesh = navMeshInScene.GetComponent<MeshFilter>().sharedMesh;
        Debug.Log("mesh :" + mesh);
        string filename = Application.dataPath + "/navMesh/" + Path.GetFileNameWithoutExtension(EditorSceneManager.GetActiveScene().name) + "navMesh.obj";
        MeshToFile(mesh, filename);
        Debug.Log("Edited NavMesh saved as '" + filename + "'");

        DestroyImmediate(curObj);


        AssetDatabase.Refresh();
    }

    static void StartMaya()
    {
        System.Diagnostics.Process exe = new System.Diagnostics.Process();
        exe.StartInfo.FileName = "C:/Program Files/Autodesk/Maya2016.5/bin/maya.exe";
        exe.StartInfo.Arguments = "-file "+ Application.dataPath+ "/navMesh/" + EditorSceneManager.GetActiveScene().name + "navMesh.obj -script \"navMeshLoadPymelScripts\"";
        exe.StartInfo.UseShellExecute = false;
        exe.Start();
    }

    static void StartMayaBatch()
    {
        System.Diagnostics.Process exe = new System.Diagnostics.Process();
        exe.StartInfo.FileName = "C:/Program Files/Autodesk/Maya2016.5/bin/mayabatch.exe";
        exe.StartInfo.Arguments = "-file "+ Application.dataPath+"/Assets/navMesh/" + EditorSceneManager.GetActiveScene().name + "navMesh.obj -script \"exportTest\"";
        exe.StartInfo.UseShellExecute = false;
        exe.Start();
    }

    static string MeshToString(Mesh mesh)
    {
        StringBuilder sb = new StringBuilder();

        sb.Append("g ").Append(mesh.name).Append("\n");
        foreach (Vector3 v in mesh.vertices)
        {
            sb.Append(string.Format("v {0} {1} {2}\n", -v.x, -v.y, v.z));
        }
        sb.Append("\n");
        foreach (Vector3 v in mesh.normals)
        {
            sb.Append(string.Format("vn {0} {1} {2}\n", v.x, v.y, v.z));
        }
        sb.Append("\n");
        foreach (Vector3 v in mesh.uv)
        {
            sb.Append(string.Format("vt {0} {1}\n", v.x, v.y));
        }

        for (int material = 0; material < mesh.subMeshCount; material++)
        {
            sb.Append("\n");
            //sb.Append("usemtl ").Append(mats[material].name).Append("\n");
            //sb.Append("usemap ").Append(mats[material].name).Append("\n");

            int[] triangles = mesh.GetTriangles(material);
            for (int i = 0; i < triangles.Length; i += 3)
            {
                sb.Append(string.Format("f {0}/{0}/{0} {1}/{1}/{1} {2}/{2}/{2}\n", triangles[i] + 1, triangles[i + 1] + 1, triangles[i + 2] + 1));
            }
        }

        return sb.ToString();
    }

    static void MeshToFile(Mesh mesh, string filename)
    {
        using (StreamWriter sw = new StreamWriter(filename))
        {
            sw.Write(MeshToString(mesh));
        }
    }

}
