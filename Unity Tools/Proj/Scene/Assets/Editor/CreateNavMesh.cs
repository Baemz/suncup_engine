﻿using System.IO;
using System.Text;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.AI;
using Houdini;
using System.Collections.Generic;
// Obj exporter component based on: http://wiki.unity3d.com/index.php?title=ObjExporter

public class CreateNavMesh : EditorWindow
{
    public static Mesh mesh = new Mesh();
    public static GameObject curObj;

    [MenuItem("SpelProjekt6/Create NavMesh")]
    static void ExtractNavMesh()
    {
        NavMeshTriangulation triangulatedNavMesh = NavMesh.CalculateTriangulation();
        //Mesh mesh = new Mesh();
        //mesh.name = "ExportedNavMesh";
        mesh = new Mesh();
        mesh.name = EditorSceneManager.GetActiveScene().name;
        mesh.vertices = triangulatedNavMesh.vertices;
        mesh.triangles = triangulatedNavMesh.indices;

        string filename = Application.dataPath + "/navMesh/" + Path.GetFileNameWithoutExtension(EditorSceneManager.GetActiveScene().name) + ".obj";
        MeshToFile(mesh, filename);
        Debug.Log("NavMesh exported as '" + filename + "'");
        AssetDatabase.Refresh();
    }

    [MenuItem("SpelProjekt6/Edit NavMesh")]
    static void EditNavMesh()
    {
        string assetPath = Application.dataPath + "Assets/Houdini/NavRemesher.otl";
        curObj = new GameObject("NavMeshTool");
        HoudiniAssetOTL asset = curObj.AddComponent<HoudiniAssetOTL>();
        HoudiniApiAssetAccessor curAccessor = HoudiniApiAssetAccessor.getAssetAccessor(curObj);
        asset.myMesh = mesh;
        asset.prAssetType = HoudiniAsset.AssetType.TYPE_OTL;
        asset.prAssetPath = assetPath;

        asset.buildAll();

        HoudiniParms curParms = asset.prParms;
        HAPI_ParmInfo[] parmInfos = curParms.prParms;

        if (parmInfos.Length > 0)
        {
            for (int i = 0; i < parmInfos.Length; i++)
            {
                if (parmInfos[i].type == HAPI_ParmType.HAPI_PARMTYPE_INT)
                {

                    //Debug.Log("parameter name-> " + parmInfos[i].name); //navMeshGeo
                    int subDivDefault = curAccessor.getParmIntValue(parmInfos[i].name, 0);
                    subDivDefault = 3;
                    curAccessor.setParmIntValue(parmInfos[i].name, 0, subDivDefault);


                }
                if (parmInfos[i].type == HAPI_ParmType.HAPI_PARMTYPE_PATH_FILE_GEO)
                {
                    // Debug.Log("parameter name-> " + parmInfos[i].name); //navMeshGeo
                    // Debug.Log("parameter type-> " + parmInfos[i].type);
                    //     Debug.Log("parameter label-> " + parmInfos[i].label);
                    //     Debug.Log("parameter getType-> " + parmInfos[i].GetType());
                    var navMeshGeoPath = curAccessor.getParmStringValue(parmInfos[i].name, 0);
                    navMeshGeoPath = Application.dataPath + "/navMesh/" + EditorSceneManager.GetActiveScene().name + ".obj";
                    curAccessor.setParmStringValue(parmInfos[i].name, 0, navMeshGeoPath);

                }
            }
        }
    }

    [MenuItem("SpelProjekt6/Save Edited NavMesh")]
    static void SaveEditedNavMesh()
    {
        mesh = null;
        GameObject navMeshInScene = GameObject.Find(EditorSceneManager.GetActiveScene().name + "_0");
        Debug.Log("navMeshInScene :" + navMeshInScene);
        mesh = navMeshInScene.GetComponent<MeshFilter>().sharedMesh;
        Debug.Log("mesh :" + mesh);
        string filename = Application.dataPath + "/navMesh/" + Path.GetFileNameWithoutExtension(EditorSceneManager.GetActiveScene().name) + ".obj";
        MeshToFile(mesh, filename);
        Debug.Log("Edited NavMesh saved as '" + filename + "'");

        DestroyImmediate(curObj);


        AssetDatabase.Refresh();
    }

    [MenuItem("SpelProjekt6/Export NavMesh to Maya")]
    static void StartMaya()
    {
        System.Diagnostics.Process exe = new System.Diagnostics.Process();
        exe.StartInfo.FileName = "C:/Program Files/Autodesk/Maya2016.5/bin/maya.exe";
        exe.StartInfo.Arguments = "-file C:/Unity Projects/ARPG/Project 6_Necrobyte/Assets/navMesh/" + EditorSceneManager.GetActiveScene().name + ".obj -script \"exportTest\"";
        exe.StartInfo.UseShellExecute = false;
        exe.Start();
    }

    [MenuItem("SpelProjekt6/Export NavMesh to MayaBatch")]
    static void StartMayaBatch()
    {
        System.Diagnostics.Process exe = new System.Diagnostics.Process();
        exe.StartInfo.FileName = "C:/Program Files/Autodesk/Maya2016.5/bin/mayabatch.exe";
        exe.StartInfo.Arguments = "-file C:/Unity Projects/ARPG/Project 6_Necrobyte/Assets/navMesh/" + EditorSceneManager.GetActiveScene().name + ".obj -script \"exportTest\"";
        exe.StartInfo.UseShellExecute = false;
        exe.Start();
    }

    static string MeshToString(Mesh mesh)
    {
        StringBuilder sb = new StringBuilder();

        sb.Append("g ").Append(mesh.name).Append("\n");
        foreach (Vector3 v in mesh.vertices)
        {
            sb.Append(string.Format("v {0} {1} {2}\n", -v.x, -v.y, v.z));
        }
        sb.Append("\n");
        foreach (Vector3 v in mesh.normals)
        {
            sb.Append(string.Format("vn {0} {1} {2}\n", v.x, v.y, v.z));
        }
        sb.Append("\n");
        foreach (Vector3 v in mesh.uv)
        {
            sb.Append(string.Format("vt {0} {1}\n", v.x, v.y));
        }

        for (int material = 0; material < mesh.subMeshCount; material++)
        {
            sb.Append("\n");
            //sb.Append("usemtl ").Append(mats[material].name).Append("\n");
            //sb.Append("usemap ").Append(mats[material].name).Append("\n");

            int[] triangles = mesh.GetTriangles(material);
            for (int i = 0; i < triangles.Length; i += 3)
            {
                sb.Append(string.Format("f {0}/{0}/{0} {1}/{1}/{1} {2}/{2}/{2}\n", triangles[i] + 1, triangles[i + 1] + 1, triangles[i + 2] + 1));
            }
        }

        return sb.ToString();
    }

    static void MeshToFile(Mesh mesh, string filename)
    {
        using (StreamWriter sw = new StreamWriter(filename))
        {
            sw.Write(MeshToString(mesh));
        }
    }
}
