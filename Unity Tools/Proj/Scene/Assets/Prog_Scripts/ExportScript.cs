﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;
using System;
using UnityEngine.AI;
using UnityEngine.SceneManagement;
using System.Linq;
using System.Reflection;

public class ExportScript : EditorWindow
{
    private enum eERROR_MESSAGES
    {
        MISSING_TRANSFORM_IN_PARENT,
        MISSING_TRANSFORM,
        UNSUPPORTED_LIGHT,
    }

    private enum eLightType : byte
    {
        NOT_SET,
        POINT,
        DIRECTIONAL,
        SPOT,
    }

    private enum eAreaType : byte
    {
        NOT_SET,
        WALK_DASH,
        DASH,
        NOTHING,
    }

    void AssertWithMessageCode(eERROR_MESSAGES aErrorCode)
    {
        Debug.Assert(false, "Failed to export a scene. Error code: " + aErrorCode);
    }

    const string DEFAULT_OUTPUT_DIRECTORY = "ExportedFiles/Scenes/";

    const string STOP_THIS = ("STOP_THIS" + "\0");

    static Texture2D myIcon;
    static Texture2D myBackground;

    [MenuItem("File/Export Opened Scene")]
    public static void ShowWindow()
    {
        EditorWindow thisWindow = GetWindow(typeof(ExportScript));
        thisWindow.minSize = new UnityEngine.Vector2(450, 300);
        thisWindow.maxSize = thisWindow.minSize;
        myIcon = new Texture2D(1, 1);
        myIcon.LoadImage(File.ReadAllBytes("Assets/Prog_Scripts/exportScriptIcon.png"));
        thisWindow.titleContent.image = myIcon;
    }

    public void Awake()
    {
        EditorSceneManager.sceneOpened += OnSceneOpened;
    }

    public void OnDestroy()
    {
        EditorSceneManager.sceneOpened -= OnSceneOpened;
    }

    public void OnSceneOpened(Scene aScene, OpenSceneMode aSceneMode)
    {
        OnDisable();
        OnEnable();
    }

    public void OnEnable()
    {
        EditorApplication.update += OnUpdate;

        SceneSetup[] loadedScenes = EditorSceneManager.GetSceneManagerSetup();
        if ((loadedScenes.Length > 0) && (loadedScenes[0] != null))
        {
            myCurrentScenePath = loadedScenes[0].path;

            string sceneName = Path.GetFileNameWithoutExtension(myCurrentScenePath);
            string productName = Application.productName;
            string prefix = "ExportScript_" + productName + "_" + sceneName;

            if (EditorPrefs.HasKey(prefix) == true)
            {
                myCustomName = EditorPrefs.GetString(prefix + "_CustomName");
                myOutputFolder = EditorPrefs.GetString(prefix + "_OutputFolder");
                myGenerateUnityNavMesh = EditorPrefs.GetBool(prefix + "_GenerateUnityNavMesh");
                myGenerateHoudiniNavMesh = EditorPrefs.GetBool(prefix + "_GenerateHoudiniNavMesh");
                myShowDebugOptions = EditorPrefs.GetBool(prefix + "_ShowDebugOptions");

                myPrintTransforms = EditorPrefs.GetBool(prefix + "_PrintTransforms");
            }
        }
    }

    public void OnDisable()
    {
        EditorApplication.update -= OnUpdate;

        if (myCurrentScenePath != null)
        {
            string sceneName = Path.GetFileNameWithoutExtension(myCurrentScenePath);
            string productName = Application.productName;
            string prefix = "ExportScript_" + productName + "_" + sceneName;

            EditorPrefs.SetBool(prefix, true);

            EditorPrefs.SetString(prefix + "_CustomName", myCustomName);
            EditorPrefs.SetString(prefix + "_OutputFolder", myOutputFolder);
            EditorPrefs.SetBool(prefix + "_GenerateUnityNavMesh", myGenerateUnityNavMesh);
            EditorPrefs.SetBool(prefix + "_GenerateHoudiniNavMesh", myGenerateHoudiniNavMesh);
            EditorPrefs.SetBool(prefix + "_ShowDebugOptions", myShowDebugOptions);

            EditorPrefs.SetBool(prefix + "_PrintTransforms", myPrintTransforms);
        }
    }

    string myCurrentScenePath = null;

    string myCustomName = "";
    string myOutputFolder = "";
    bool myShowDebugOptions = false;
    bool myGenerateUnityNavMesh = false;
    bool myGenerateHoudiniNavMesh = false;

    bool myPrintTransforms = true;

    BinaryWriter myWriter;

    Mesh myHoudiniMesh = null;
    List<GameObject> myNavMeshBoxes = null;

    Vector3 myMapSizeMin;
    Vector3 myMapSizeMax;

    float myExportedSuccesfully = -1.0f;
    float myExportedSuccesfullyTimer = 0.0f;
    const float myExportedSuccesfullyTimerMax = 5.0f;

    float myTimeLastUpdate = 0.0f;

    public void OnUpdate()
    {
        float deltaTime = 0.0f;

        float timeNow = Time.realtimeSinceStartup;
        if (myTimeLastUpdate != 0.0f)
        {
            deltaTime = timeNow - myTimeLastUpdate;
        }

        myTimeLastUpdate = timeNow;

        if (myExportedSuccesfullyTimer > 0.0f)
        {
            myExportedSuccesfullyTimer -= deltaTime;

            if (myExportedSuccesfullyTimer <= 0.0f)
            {
                myExportedSuccesfullyTimer = 0.0f;

                myExportedSuccesfully = -1.0f;
            }
        }
    }

    void OnGUI()
    {
        if (myBackground == null)
        {
            myBackground = new Texture2D(1, 1);
            myBackground.LoadImage(File.ReadAllBytes("Assets/Prog_Scripts/exportScriptBackground.png"));
        }
        GUI.DrawTexture(new Rect(0.0f, 0.0f, position.width, position.height), myBackground);

        if ((EditorSceneManager.GetSceneManagerSetup() != null) && (GUI.Button(new Rect(10, 10, 100, 32), "Export")))
        {
            try
            {
                ExportOpenedScenes();
                myExportedSuccesfullyTimer = myExportedSuccesfullyTimerMax;
                myExportedSuccesfully = 1.0f;
            }
            catch (Exception ex)
            {
                myExportedSuccesfullyTimer = myExportedSuccesfullyTimerMax;
                myExportedSuccesfully = 0.0f;
                throw ex;
            }
        }

        if (myExportedSuccesfully != -1.0f)
        {
            Rect rect = new Rect();
            rect.xMin = 120;
            rect.yMin = 10;
            rect.xMax = 152;
            rect.yMax = 42;
            Color color = new Color();
            color.r = 1.0f - myExportedSuccesfully;
            color.g = myExportedSuccesfully;
            color.b = 0.0f;
            color.a = 1.0f;
            EditorGUI.DrawRect(rect, color);
        }

        for (int spaceIndex = 0; spaceIndex < 7; ++spaceIndex)
        {
            EditorGUILayout.Space();
        }

        GUILayout.Label("Export Scene", EditorStyles.boldLabel);

        myCustomName = EditorGUILayout.TextField("Level name", myCustomName);
        EditorGUILayout.HelpBox("Leave empty for scene name", MessageType.Info);

        EditorGUILayout.Space();
        GUILayout.BeginHorizontal(GUILayout.Width(180.0f), GUILayout.ExpandWidth(false), GUILayout.ExpandHeight(false));
        GUILayout.Label("Output directory");

        if (GUILayout.Button("Select", GUILayout.Width(70.0f)))
        {
            string outputFolder = myOutputFolder;
            if (outputFolder == "")
            {
                outputFolder = Application.dataPath;
            }
            outputFolder = EditorUtility.SaveFolderPanel("Select export folder", outputFolder, "");

            if (outputFolder != "")
            {
                myOutputFolder = outputFolder;
            }
        }
        if (GUILayout.Button("Reset", GUILayout.Width(70.0f)))
        {
            myOutputFolder = "";
        }
        GUILayout.EndHorizontal();
        GUILayout.Label(myOutputFolder, EditorStyles.helpBox);
        EditorGUILayout.HelpBox("Leave empty for \".../" + DEFAULT_OUTPUT_DIRECTORY + "\"", MessageType.Info);

        const float toggleTextWidth = 170.0f;
        const float toggleToggleWidth = 60.0f;

        GUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("Generate Unity NavMesh", GUILayout.Width(toggleTextWidth));
        myGenerateUnityNavMesh = EditorGUILayout.Toggle("", myGenerateUnityNavMesh, GUILayout.Width(toggleToggleWidth));
        EditorGUILayout.LabelField("Generate Houdini NavMesh", GUILayout.Width(toggleTextWidth));
        myGenerateHoudiniNavMesh = EditorGUILayout.Toggle("", myGenerateHoudiniNavMesh, GUILayout.Width(toggleToggleWidth));
        GUILayout.EndHorizontal();

        EditorGUILayout.Space();
        EditorGUILayout.Space();

        GUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("Show debug options", GUILayout.Width(toggleTextWidth));
        myShowDebugOptions = EditorGUILayout.Toggle("", myShowDebugOptions, GUILayout.Width(toggleToggleWidth));
        GUILayout.EndHorizontal();
        if (myShowDebugOptions == true)
        {
            EditorGUILayout.HelpBox("Choose things to export", MessageType.None);

            myPrintTransforms = EditorGUILayout.Toggle("Transforms", myPrintTransforms);
        }
    }

    void ResetInternalData()
    {
        const float maxFloat = float.MaxValue;
        myMapSizeMin = new Vector3(maxFloat, maxFloat, maxFloat);
        const float minFloat = float.MinValue;
        myMapSizeMax = new Vector3(minFloat, minFloat, minFloat);
    }

    void ExportOpenedScenes()
    {
        SceneSetup[] loadedScenes = EditorSceneManager.GetSceneManagerSetup();

        if (loadedScenes.Length > 0)
        {
            ExportScene(loadedScenes[0].path);
        }
    }

    void ExportScene(string aScenePath)
    {
        string outputFolder = DEFAULT_OUTPUT_DIRECTORY;
        string outputSceneExtension = ".dataMap";

        if (myOutputFolder == "")
        {
            Directory.CreateDirectory(outputFolder);
        }
        else
        {
            outputFolder = myOutputFolder + "/";
        }

        UnityEngine.SceneManagement.Scene scene = EditorSceneManager.GetSceneByPath(aScenePath);

        if (scene.isLoaded == false)
        {
            return;
        }

        ResetInternalData();

        string outputName = Path.GetFileNameWithoutExtension(aScenePath);
        if (myCustomName != "")
        {
            outputName = myCustomName;
        }
        myWriter = new BinaryWriter(new FileStream(outputFolder + outputName + outputSceneExtension, FileMode.Create));

        try
        {
            GameObject[] gameObjects = scene.GetRootGameObjects();

            myHoudiniMesh = null;
            HoudiniAssetOTL houdiniAssetOTL = null;
            myNavMeshBoxes = new List<GameObject>();

            foreach (GameObject currentGameObject in gameObjects)
            {
                HoudiniAssetOTL tempHoudiniAssetOTL = currentGameObject.GetComponent<HoudiniAssetOTL>();
                string layerName = LayerMask.LayerToName(currentGameObject.layer);

                if (tempHoudiniAssetOTL != null)
                {
                    houdiniAssetOTL = tempHoudiniAssetOTL;
                }
                else if ((layerName == "OnlyDashBox")
                    || (layerName == "NothingBox"))
                {
                    myNavMeshBoxes.Add(currentGameObject);
                }
            }

            if (myGenerateUnityNavMesh == true)
            {
                UnityEditor.AI.NavMeshBuilder.BuildNavMesh();
            }

            if (myGenerateHoudiniNavMesh == true)
            {
                try
                {
                    Assembly editorAssembly = System.AppDomain.CurrentDomain.GetAssemblies().First(a => a.FullName.StartsWith("Assembly-CSharp-Editor,")); // ',' included to ignore  Assembly-CSharp-Editor-FirstPass
                    Type utilityType = editorAssembly.GetTypes().FirstOrDefault(t => t.FullName.Contains("CreateNavMesh"));
                    MethodInfo funcCreateNavMesh = utilityType.GetMethod("ExtractNavMesh", BindingFlags.NonPublic | BindingFlags.Static);
                    MethodInfo funcEditNavMesh = utilityType.GetMethod("EditNavMesh", BindingFlags.NonPublic | BindingFlags.Static);

                    if (houdiniAssetOTL != null)
                    {
                        GameObject objToDestroy = houdiniAssetOTL.gameObject;
                        houdiniAssetOTL = null;
                        DestroyImmediate(objToDestroy);
                    }

                    funcCreateNavMesh.Invoke(null, null);
                    funcEditNavMesh.Invoke(null, null);

                    FieldInfo fieldHoudiniAsset = utilityType.GetField("curObj", BindingFlags.Public | BindingFlags.Static);
                    FieldInfo areaNamesInfo = utilityType.GetField("myAreaNames", BindingFlags.Public | BindingFlags.Static);
                    
                    GameObject objHoudini = (GameObject)fieldHoudiniAsset.GetValue(null);

                    objHoudini.name = "DON'T TOUCH ME /Progg";
                    houdiniAssetOTL = objHoudini.GetComponent<HoudiniAssetOTL>();
                    Debug.Assert(houdiniAssetOTL != null);

                    MeshFilter meshFilter = houdiniAssetOTL.GetComponentInChildren<MeshFilter>();
                    myHoudiniMesh = meshFilter.sharedMesh;

                    Vector3[] vertices = myHoudiniMesh.vertices;

                    for (int vertexIndex = 0; vertexIndex < myHoudiniMesh.vertices.Length; ++vertexIndex)
                    {
                        vertices[vertexIndex].y = -myHoudiniMesh.vertices[vertexIndex].y;
                    }

                    myHoudiniMesh.vertices = vertices;

                    meshFilter.sharedMesh = myHoudiniMesh;

                    MeshRenderer meshRenderer = houdiniAssetOTL.GetComponentInChildren<MeshRenderer>();
                    meshRenderer.material = AssetDatabase.LoadAssetAtPath("Assets/Prog_Scripts/Wireframe/wireframeMaterial.mat", typeof(Material)) as Material;
                }
                catch (Exception ex)
                {
                    Debug.LogError("Failed to generate Houdini NavMesh in Editor. Tell a programmer!");
                    throw ex;
                }

                gameObjects = scene.GetRootGameObjects();
            }
            else if (houdiniAssetOTL != null)
            {
                myHoudiniMesh = houdiniAssetOTL.GetComponentInChildren<MeshFilter>().sharedMesh;
            }

            // ______________________________________
            // LEVEL INFO
            myWriter.Write(("LEVELINFO" + "\0").ToCharArray());

            WriteNavMesh();

            int dummyMapSizeMin = (int)myWriter.BaseStream.Position;
            myWriter.Write(dummyMapSizeMin);
            myWriter.Write((int)0);
            myWriter.Write((int)0);
            int dummyMapSizeMax = (int)myWriter.BaseStream.Position;
            myWriter.Write(dummyMapSizeMax);
            myWriter.Write((int)0);
            myWriter.Write((int)0);

            int dummyAmountOfRootObjects = (int)myWriter.BaseStream.Position;
            myWriter.Write(dummyAmountOfRootObjects);
            int dummyAmountOfTotalObjects = (int)myWriter.BaseStream.Position;
            myWriter.Write(dummyAmountOfTotalObjects);

            // LEVEL INFO
            // ______________________________________

            int rootAmountOfObjectsWritten = 0;
            int totalAmountOfObjectsWritten = 0;
            foreach (GameObject currentGameObject in gameObjects)
            {
                if ((currentGameObject.tag == "NoExport") || (currentGameObject.activeInHierarchy == false))
                {
                    continue;
                }

                int amountAdded = WriteGameObject(currentGameObject);
                if (amountAdded > 0)
                {
                    ++rootAmountOfObjectsWritten;
                    totalAmountOfObjectsWritten += amountAdded;
                }
            }

            myWriter.Seek(dummyMapSizeMin, SeekOrigin.Begin);
            WriteVector3(myMapSizeMin);

            myWriter.Seek(dummyMapSizeMax, SeekOrigin.Begin);
            WriteVector3(myMapSizeMax);

            myWriter.Seek(dummyAmountOfRootObjects, SeekOrigin.Begin);
            myWriter.Write(rootAmountOfObjectsWritten);

            myWriter.Seek(dummyAmountOfTotalObjects, SeekOrigin.Begin);
            myWriter.Write(totalAmountOfObjectsWritten);
        }
        catch (Exception ex)
        {
            myWriter.Close();
            throw ex;
        }

        myWriter.Close();
    }

    void WriteNavMesh()
    {
        Vector3[] vertices = null;
        int[] triangleIndices = null;

        bool validHoudiniNavMesh = false;
        if (myHoudiniMesh != null)
        {
            validHoudiniNavMesh = true;
            vertices = myHoudiniMesh.vertices;
            triangleIndices = myHoudiniMesh.triangles;
        }
        else
        {
            Debug.LogWarning("Found no Houdini NavMesh. Will use the Unity NavMesh");
        }

        if (validHoudiniNavMesh == false)
        {
            NavMeshTriangulation navMeshAsTriangulated = NavMesh.CalculateTriangulation();
            vertices = navMeshAsTriangulated.vertices;
            triangleIndices = navMeshAsTriangulated.indices;
        }
        
        // Duplicates removing
        List<Vector3> verticesToAdd = new List<Vector3>();
        for (int currentVertexIndex = 0; currentVertexIndex < vertices.Length; ++currentVertexIndex)
        {
            Vector3 currentVertex = vertices[currentVertexIndex];

            int foundIndex = -1;
            for (int currentOtherIndex = 0; currentOtherIndex < verticesToAdd.Count; ++currentOtherIndex)
            {
                Vector3 otherVertex = verticesToAdd[currentOtherIndex];

                if (currentVertex == otherVertex)
                {
                    foundIndex = currentOtherIndex;
                    break;
                }
            }

            if (foundIndex == -1)
            {
                foundIndex = verticesToAdd.Count;
                verticesToAdd.Add(currentVertex);
            }

            for (int currentIndexIndex = 0; currentIndexIndex < triangleIndices.Length; ++currentIndexIndex)
            {
                int currentIndex = triangleIndices[currentIndexIndex];

                if (currentVertexIndex == currentIndex)
                {
                    triangleIndices[currentIndexIndex] = foundIndex;
                }
            }
        }
        // END Duplicates removing
        
        List<KeyValuePair<int, eAreaType>> tempAreaTypes = new List<KeyValuePair<int, eAreaType>>();
        
        foreach (var gameObject in myNavMeshBoxes)
        {
            BoxCollider boxColldier = gameObject.GetComponent<BoxCollider>();
            if (boxColldier == null)
            {
                Debug.LogError("Object \"" + gameObject.name + "\" marked as navigation box but has no box collider!");
                continue;
            }

            Bounds bounds = gameObject.GetComponent<BoxCollider>().bounds;
            
            string layerName = LayerMask.LayerToName(gameObject.layer);
            for (int triangleIndex = 0; triangleIndex < triangleIndices.Length; triangleIndex += 3)
            {
                Vector3 vertex0 = verticesToAdd[triangleIndices[triangleIndex + 0]];
                Vector3 vertex1 = verticesToAdd[triangleIndices[triangleIndex + 1]];
                Vector3 vertex2 = verticesToAdd[triangleIndices[triangleIndex + 2]];

                if (bounds.Contains(vertex0) && bounds.Contains(vertex1) && bounds.Contains(vertex2))
                {
                    eAreaType areaType = eAreaType.NOT_SET;

                    if (layerName == "OnlyDashBox")
                    {
                        areaType = eAreaType.DASH;
                    }
                    else if (layerName == "NothingBox")
                    {
                        areaType = eAreaType.NOTHING;
                    }
                    else
                    {
                        Debug.LogWarning("Unrecognized layer \"" + layerName + "\" used, forgot to add it her as well?");
                    }

                    if (areaType != eAreaType.NOT_SET)
                    {
                        tempAreaTypes.Add(new KeyValuePair<int, eAreaType>((triangleIndex / 3), areaType));
                    }
                }
            }
        }

        tempAreaTypes.Sort((aPair0, aPair1) => aPair0.Key.CompareTo(aPair1.Key));

        List<eAreaType> areaTypes = new List<eAreaType>();

        int nextIndex = 0;
        foreach (var currentPair in tempAreaTypes)
        {
            while (nextIndex < currentPair.Key)
            {
                areaTypes.Add(eAreaType.WALK_DASH);
                ++nextIndex;
            }
            areaTypes.Add(currentPair.Value);
            ++nextIndex;
        }

        for (; nextIndex < (triangleIndices.Length / 3); ++nextIndex)
        {
            areaTypes.Add(eAreaType.WALK_DASH);
        }

        myWriter.Write(("NAVMESH" + "\0").ToCharArray());

        const int amountOfLong_AND_IntPaddings = 3;
        const int amountOfNormalInts = 5;

        int byteSize = 0;
        byteSize += sizeof(long) * amountOfLong_AND_IntPaddings;
        byteSize += sizeof(int) * amountOfNormalInts;
        byteSize += triangleIndices.Length * sizeof(int);
        byteSize += verticesToAdd.Count * (sizeof(float) * 3);
        byteSize += areaTypes.Count * sizeof(eAreaType);
        myWriter.Write(byteSize);

        myWriter.Write(new long());
        myWriter.Write((int)triangleIndices.Length);
        myWriter.Write(new int());
        myWriter.Write(new long());
        myWriter.Write((int)verticesToAdd.Count);
        myWriter.Write(new int());
        myWriter.Write(new long());
        myWriter.Write((int)areaTypes.Count);

        foreach (int currentIndex in triangleIndices)
        {
            myWriter.Write(currentIndex);
        }

        foreach (Vector3 currentVertex in verticesToAdd)
        {
            Vector3 positionFixed = currentVertex;
            positionFixed.x = -currentVertex.x;
            positionFixed.z = -currentVertex.z;
            WriteVector3(positionFixed);
        }
        
        foreach (var currentAreaType in areaTypes)
        {
            myWriter.Write((byte)currentAreaType);
        }
    }

    int WriteGameObject(GameObject aGameObject)
    {
        {
            MeshFilter meshFilter = aGameObject.GetComponent<MeshFilter>();

            if (meshFilter != null)
            {
                Mesh mesh = meshFilter.sharedMesh;
                if (mesh != null)
                {
                    if (mesh == myHoudiniMesh)
                    {
                        return 0;
                    }
                }
            }
        }

        myWriter.Write(("OBJECT" + "\0").ToCharArray());
        myWriter.Write(aGameObject.name.Length);
        myWriter.Write((aGameObject.name + "\0").ToCharArray());
        myWriter.Write(aGameObject.GetInstanceID());
        myWriter.Write(aGameObject.tag.Length);
        myWriter.Write((aGameObject.tag + "\0").ToCharArray());

        if ((GameObjectUtility.GetStaticEditorFlags(aGameObject) & StaticEditorFlags.NavigationStatic) != 0)
        {
            myWriter.Write(1);
        }
        else
        {
            myWriter.Write(0);
        }

        WriteTransform(aGameObject.GetComponent<Transform>());

        WriteMeshName(aGameObject.GetComponent<Animator>());

        WriteMaterialName(aGameObject.GetComponent<MeshRenderer>());

        WriteModelSize(aGameObject.GetComponent<MeshRenderer>(), aGameObject.GetComponent<SkinnedMeshRenderer>(), aGameObject.GetComponent<BoxCollider>());

        WriteScript(aGameObject.GetComponent<GameObjectScript>());

        WriteBoxCollider(aGameObject.GetComponent<BoxCollider>());

        WriteLights(aGameObject.GetComponents<Light>());

        WriteParticle(aGameObject.GetComponent<ParticleSystem>(), aGameObject.GetComponent<ParticleDataExport>());

        int amountOfChildrenWritten = WriteChildGameObjects(aGameObject);

        return (1 + amountOfChildrenWritten);
    }

    int WriteChildGameObjects(GameObject aGameObject)
    {
        int amountOfChildrenWritten = 0;

        Transform[] childAndThisTransforms = aGameObject.GetComponentsInChildren<Transform>();
        if (childAndThisTransforms.Length > 1)
        {
            Transform thisTransform = childAndThisTransforms[0];
            if (thisTransform.gameObject != aGameObject)
            {
                AssertWithMessageCode(eERROR_MESSAGES.MISSING_TRANSFORM_IN_PARENT);
                return 0;
            }

            List<GameObject> directChildren = new List<GameObject>();
            for (int transformIndex = 1; transformIndex < childAndThisTransforms.Length; ++transformIndex)
            {
                Transform currentTransform = childAndThisTransforms[transformIndex];
                if (currentTransform.parent == thisTransform)
                {
                    MeshFilter meshFilter = currentTransform.gameObject.GetComponent<MeshFilter>();

                    if (meshFilter != null)
                    {
                        Mesh mesh = meshFilter.sharedMesh;
                        if (mesh != null)
                        {
                            if (mesh == myHoudiniMesh)
                            {
                                continue;
                            }
                        }
                    }

                    directChildren.Add(currentTransform.gameObject);
                }
            }

            if (directChildren.Count != 0)
            {
                myWriter.Write(("CHILD" + "\0").ToCharArray());

                int beforeAmountOfChildrenPosition = (int)myWriter.BaseStream.Position;
                myWriter.Write(beforeAmountOfChildrenPosition);

                int amountOfRootChildrenWritten = 0;
                foreach (GameObject currentGameObject in directChildren)
                {
                    int amountWritten = WriteGameObject(currentGameObject);
                    if (amountWritten != 0)
                    {
                        ++amountOfRootChildrenWritten;
                        amountOfChildrenWritten += amountWritten;
                    }
                }

                int afterObjectsPosition = (int)myWriter.BaseStream.Position;
                myWriter.Seek(beforeAmountOfChildrenPosition, SeekOrigin.Begin);
                myWriter.Write(amountOfRootChildrenWritten);
                myWriter.Seek(afterObjectsPosition, SeekOrigin.Begin);
            }
        }

        return amountOfChildrenWritten;
    }

    void WriteMeshName(Animator aAnimator)
    {
        if (aAnimator == null)
        {
            return;
        }

        if (aAnimator.avatar == null)
        {
            Debug.LogWarning("GameObject \"" + aAnimator.gameObject.name + "\" has an animator with no avatar attached, this object won't be rendered in-engine!");
            return;
        }

        myWriter.Write(("MESHNAME" + "\0").ToCharArray());

        string meshName = aAnimator.avatar.name;

        meshName = meshName.Substring(0, meshName.LastIndexOf("Avatar"));
        myWriter.Write(meshName.Length);
        myWriter.Write((meshName + "\0").ToCharArray());
    }

    void WriteMaterialName(MeshRenderer aMeshRenderer)
    {
        if (aMeshRenderer == null)
        {
            return;
        }

        if (aMeshRenderer.sharedMaterial == null)
        {
            Debug.LogWarning("GameObject \"" + aMeshRenderer.gameObject.name + "\" has an mesh renderer with no material attached, this object won't be rendered in-engine!");
            return;
        }

        myWriter.Write(("MATERIALNAME" + "\0").ToCharArray());

        string materialName = aMeshRenderer.sharedMaterial.name;
        int compareIndex = materialName.LastIndexOf(' ');
        if (compareIndex < 0)
        {
            compareIndex = materialName.Length;
        }

        materialName = materialName.Substring(0, compareIndex);
        materialName = materialName + ".material";

        myWriter.Write(materialName.Length);
        myWriter.Write((materialName + "\0").ToCharArray());
    }

    void WriteModelSize(MeshRenderer aMeshRenderer, SkinnedMeshRenderer aSkinnedMeshRenderer, BoxCollider aBoxCollider)
    {
        myWriter.Write(("MODELSIZE" + "\0").ToCharArray());

        Vector3 size = new Vector3(1.0f, 1.0f, 1.0f);
        if (aMeshRenderer != null)
        {
            size = aMeshRenderer.bounds.extents;
        }
        else if (aSkinnedMeshRenderer != null)
        {
            size = aSkinnedMeshRenderer.bounds.extents;
        }
        else if (aBoxCollider != null)
        {
            size = aBoxCollider.bounds.extents;
        }

        WriteVector3(size);
    }

    void WriteScript(GameObjectScript aScript)
    {
        if (aScript == null)
        {
            return;
        }

        myWriter.Write(("SCRIPT" + "\0").ToCharArray());
        myWriter.Write(aScript.myScriptPath.Length);
        myWriter.Write((aScript.myScriptPath + "\0").ToCharArray());

        myWriter.Write(aScript.myScriptIDVariables.dictionary.Count);

        for (int index = 0; index < aScript.myScriptIDVariables.dictionary.Count; ++index)
        {
            myWriter.Write(new long());
            myWriter.Write(new long());
        }

        foreach (var variableNumberPair in aScript.myScriptIDVariables.dictionary)
        {
            myWriter.Write(variableNumberPair.Key.Length);
            myWriter.Write((variableNumberPair.Key + "\0").ToCharArray());

            if (variableNumberPair.Value == null)
            {
                myWriter.Write(Convert.ToInt32(false));
                myWriter.Write(0);
            }
            else
            {
                myWriter.Write(Convert.ToInt32(true));
                myWriter.Write(variableNumberPair.Value.GetInstanceID());
            }
        }

        if (aScript.myScriptNumberVariables.dictionary == null)
        {
            myWriter.Write(0);
        }
        else
        {
            myWriter.Write(aScript.myScriptNumberVariables.dictionary.Count);

            for (int index = 0; index < aScript.myScriptNumberVariables.dictionary.Count; ++index)
            {
                myWriter.Write(new long());
                myWriter.Write(new long());
            }

            foreach (var variableNumberPair in aScript.myScriptNumberVariables.dictionary)
            {
                myWriter.Write(variableNumberPair.Key.Length);
                myWriter.Write((variableNumberPair.Key + "\0").ToCharArray());

                if (variableNumberPair.Value == -1)
                {
                    myWriter.Write(Convert.ToInt32(false));
                    myWriter.Write(-1);
                }
                else
                {
                    myWriter.Write(Convert.ToInt32(true));
                    myWriter.Write(variableNumberPair.Value);
                }
            }
        }

        if (aScript.myScriptStringVariables.dictionary == null)
        {
            myWriter.Write(0);
        }
        else
        {
            myWriter.Write(aScript.myScriptStringVariables.dictionary.Count);

            for (int index = 0; index < aScript.myScriptStringVariables.dictionary.Count; ++index)
            {
                myWriter.Write(new long());
                myWriter.Write(new long());
            }

            foreach (var variableStringPair in aScript.myScriptStringVariables.dictionary)
            {
                myWriter.Write(variableStringPair.Key.Length);
                myWriter.Write((variableStringPair.Key + "\0").ToCharArray());

                if (variableStringPair.Value == "")
                {
                    myWriter.Write(Convert.ToInt32(false));
                    myWriter.Write(0);
                    myWriter.Write(new long());
                    myWriter.Write(("" + "\0").ToCharArray());
                }
                else
                {
                    myWriter.Write(Convert.ToInt32(true));
                    myWriter.Write(variableStringPair.Value.Length);
                    myWriter.Write(new long());
                    myWriter.Write((variableStringPair.Value + "\0").ToCharArray());
                }
            }
        }
    }

    void WriteBoxCollider(BoxCollider aBoxCollider)
    {
        if (aBoxCollider == null)
        {
            return;
        }

        Vector3 min = aBoxCollider.bounds.min;
        Vector3 max = aBoxCollider.bounds.max;

        min.x = -min.x;
        min.z = -min.z;

        max.x = -max.x;
        max.z = -max.z;

        Vector3 tempMin = min;
        if (max.x < min.x)
        {
            min.x = max.x;
            max.x = tempMin.x;
        }
        if (max.y < min.y)
        {
            min.y = max.y;
            max.y = tempMin.y;
        }
        if (max.z < min.z)
        {
            min.z = max.z;
            max.z = tempMin.z;
        }

        myWriter.Write(("BOXCOLLIDER" + "\0").ToCharArray());

        WriteVector3(min);
        WriteVector3(max);

        myWriter.Write(Convert.ToInt32(aBoxCollider.isTrigger));
    }

    void WriteLights(Light[] aLights)
    {
        if ((aLights == null) || (aLights.Length == 0))
        {
            return;
        }

        myWriter.Write(("LIGHT" + "\0").ToCharArray());
        myWriter.Write(aLights.Length);
        foreach (Light currentLight in aLights)
        {
            myWriter.Write(GetLightType(currentLight.type));
            WriteVector4(currentLight.color);
            myWriter.Write(currentLight.intensity);

            if (currentLight.type == LightType.Point)
            {
                WritePointLight(currentLight);
            }
            else if (currentLight.type == LightType.Directional)
            {
                WriteDirectionalLight(currentLight);
            }
            else if (currentLight.type == LightType.Spot)
            {
                WriteSpotLight(currentLight);
            }
            else
            {
                AssertWithMessageCode(eERROR_MESSAGES.UNSUPPORTED_LIGHT);
                return;
            }
        }
    }

    char GetLightType(LightType aLightType)
    {
        if (aLightType == LightType.Point)
        {
            return (char)eLightType.POINT;
        }
        else if (aLightType == LightType.Directional)
        {
            return (char)eLightType.DIRECTIONAL;
        }
        else if (aLightType == LightType.Spot)
        {
            return (char)eLightType.SPOT;
        }
        else
        {
            AssertWithMessageCode(eERROR_MESSAGES.UNSUPPORTED_LIGHT);
            return (char)eLightType.NOT_SET;
        }
    }

    void WritePointLight(Light aLight)
    {
        myWriter.Write(aLight.range);
    }

    void WriteDirectionalLight(Light aLight)
    {
        if (aLight.transform == null)
        {
            AssertWithMessageCode(eERROR_MESSAGES.MISSING_TRANSFORM);
        }
        Vector3 directionVector = aLight.transform.rotation * Vector3.forward;
        directionVector.x = -directionVector.x;
        directionVector.z = -directionVector.z;
        directionVector = -directionVector;
        WriteVector3(directionVector.normalized);
    }

    void WriteSpotLight(Light aLight)
    {
        Vector3 directionVector = aLight.transform.rotation * Vector3.forward;
        directionVector.x = -directionVector.x;
        directionVector.z = -directionVector.z;
        WriteVector3(directionVector.normalized);

        myWriter.Write(aLight.spotAngle);
        myWriter.Write(aLight.range);

    }

    void WriteParticle(ParticleSystem aParticleSystem, ParticleDataExport aParticleDataExport)
    {
        if (aParticleSystem == null)
        {
            return;
        }

        if (aParticleDataExport == null)
        {
            Debug.LogError("\"" + aParticleSystem.gameObject.name + "\" has a particle system (ID " + aParticleSystem.GetInstanceID() + ") but no particle data export. This particle won't work!\nThe export script will continue.");
            return;
        }

        myWriter.Write(("PARTICLE" + "\0").ToCharArray());

        aParticleDataExport.CreateParticleFile();
        string particleName = aParticleDataExport.fileNameNoExt;
        myWriter.Write(particleName.Length);
        myWriter.Write((particleName + "\0").ToCharArray());
    }

    void WriteVector2(Vector2 aVector2)
    {
        myWriter.Write(aVector2.x);
        myWriter.Write(aVector2.y);
    }

    void WriteVector3(Vector3 aVector3)
    {
        myWriter.Write(aVector3.x);
        myWriter.Write(aVector3.y);
        myWriter.Write(aVector3.z);
    }

    void WriteVector4(Vector4 aVector4)
    {
        myWriter.Write(aVector4.x);
        myWriter.Write(aVector4.y);
        myWriter.Write(aVector4.z);
        myWriter.Write(aVector4.w);
    }

    void WriteTransform(Transform aTransform)
    {
        if (myPrintTransforms == false)
        {
            return;
        }

        myWriter.Write(("TRANSFORM" + "\0").ToCharArray());

        Vector3 positionFixed = aTransform.position;
        positionFixed.x = -aTransform.position.x;
        positionFixed.z = -aTransform.position.z;
        WriteVector3(positionFixed);

        Vector3 rotationFixed = aTransform.eulerAngles;
        rotationFixed.x = -aTransform.eulerAngles.x;
        rotationFixed.z = -aTransform.eulerAngles.z;
        WriteVector3(rotationFixed * Mathf.Deg2Rad);
        WriteVector3(aTransform.lossyScale);

        if (positionFixed.x < myMapSizeMin.x)
        {
            myMapSizeMin.x = positionFixed.x;
        }
        if (positionFixed.y < myMapSizeMin.y)
        {
            myMapSizeMin.y = positionFixed.y;
        }
        if (positionFixed.z < myMapSizeMin.z)
        {
            myMapSizeMin.z = positionFixed.z;
        }

        if (positionFixed.x > myMapSizeMax.x)
        {
            myMapSizeMax.x = positionFixed.x;
        }
        if (positionFixed.y > myMapSizeMax.y)
        {
            myMapSizeMax.y = positionFixed.y;
        }
        if (positionFixed.z > myMapSizeMax.z)
        {
            myMapSizeMax.z = positionFixed.z;
        }
    }
}
