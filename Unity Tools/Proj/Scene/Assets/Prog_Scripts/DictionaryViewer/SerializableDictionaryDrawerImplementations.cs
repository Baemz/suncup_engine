using UnityEngine;
using UnityEngine.UI;

using UnityEditor;

[UnityEditor.CustomPropertyDrawer(typeof(StringGameObjectDictionary))]
public class StringGameObjectDictionaryDrawer : SerializableDictionaryDrawer<string, GameObject>
{
    protected override SerializableKeyValueTemplate<string, GameObject> GetTemplate()
    {
        return GetGenericTemplate<SerializableStringGameObjectTemplate>();
    }
}

[UnityEditor.CustomPropertyDrawer(typeof(StringIntDictionary))]
public class StringIntDictionaryDrawer : SerializableDictionaryDrawer<string, int>
{
    protected override SerializableKeyValueTemplate<string, int> GetTemplate()
    {
        return GetGenericTemplate<SerializableStringIntTemplate>();
    }
}

[UnityEditor.CustomPropertyDrawer(typeof(StringStringDictionary))]
public class StringStringDictionaryDrawer : SerializableDictionaryDrawer<string, string>
{
    protected override SerializableKeyValueTemplate<string, string> GetTemplate()
    {
        return GetGenericTemplate<SerializableStringStringTemplate>();
    }
}

internal class SerializableStringGameObjectTemplate : SerializableKeyValueTemplate<string, GameObject> { }

internal class SerializableStringIntTemplate : SerializableKeyValueTemplate<string, int> { }

internal class SerializableStringStringTemplate : SerializableKeyValueTemplate<string, string> { }
