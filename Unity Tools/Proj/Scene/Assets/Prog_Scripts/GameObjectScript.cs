﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(GameObjectScript))]
public class OpenFileButton : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        GameObjectScript myScript = (GameObjectScript)target;
        if (GUILayout.Button("Load Script"))
        {
            myScript.OpenDialog();
        }
    }
}

public class GameObjectScript : MonoBehaviour
{
    public string myScriptPath;
    [SerializeField]
    public StringGameObjectDictionary myScriptIDVariables;
    [SerializeField]
    public StringIntDictionary myScriptNumberVariables;
    [SerializeField]
    public StringStringDictionary myScriptStringVariables;

    public void OpenDialog()
    {
        string pathGotten = EditorUtility.OpenFilePanel(
            "Open Script",
            "",
            "lua");

        string extension = Path.GetExtension(pathGotten);

        if (pathGotten == "")
        {
            return;
        }

        if (extension != ".lua")
        {
            Debug.LogError("Scripts has to be of file type \".lua\"");
            return;
        }

        int startIndex = pathGotten.LastIndexOf("/Data/");
        if (startIndex == -1)
        {
            Debug.LogError("Scripts has to be in \".../Data/\" folder of the current game project");
            return;
        }
        myScriptPath = pathGotten.Substring(startIndex + 1);

        myScriptIDVariables = new StringGameObjectDictionary();
        myScriptIDVariables.dictionary = new Dictionary<string, GameObject>();
        myScriptNumberVariables = new StringIntDictionary();
        myScriptNumberVariables.dictionary = new Dictionary<string, int>();
        myScriptStringVariables = new StringStringDictionary();
        myScriptStringVariables.dictionary = new Dictionary<string, string>();

        StreamReader streamReader = new StreamReader(pathGotten);
        string currentLine = "";
        bool foundIndexLastTime = false;
        bool foundNumberLastTime = false;
        bool foundStringLastTime = false;
        while ((currentLine = streamReader.ReadLine()) != null)
        {
            if (foundIndexLastTime == true)
            {
                foundIndexLastTime = false;

                string changedLine = currentLine;

                const string variableNamePrefix = "local ";
                if (changedLine.StartsWith(variableNamePrefix))
                {
                    int variableNameStartIndex = variableNamePrefix.Length;
                    changedLine = changedLine.Substring(variableNameStartIndex);
                }

                int variableNameFirstSpaceIndex = changedLine.IndexOf(' ');

                changedLine = changedLine.Substring(0, variableNameFirstSpaceIndex);

                myScriptIDVariables.dictionary.Add(changedLine, null);
            }
            else
            {
                if (currentLine == "--[UNITY_INDEX]--")
                {
                    foundIndexLastTime = true;
                }
            }

            if (foundNumberLastTime == true)
            {
                foundNumberLastTime = false;

                string changedLine = currentLine;

                const string variableNamePrefix = "local ";
                if (changedLine.StartsWith(variableNamePrefix))
                {
                    int variableNameStartIndex = variableNamePrefix.Length;
                    changedLine = changedLine.Substring(variableNameStartIndex);
                }

                int variableNameFirstSpaceIndex = changedLine.IndexOf(' ');

                changedLine = changedLine.Substring(0, variableNameFirstSpaceIndex);

                myScriptNumberVariables.dictionary.Add(changedLine, -1);
            }
            else
            {
                if (currentLine == "--[UNITY_NUMBER]--")
                {
                    foundNumberLastTime = true;
                }
            }

            if (foundStringLastTime == true)
            {
                foundStringLastTime = false;

                string changedLine = currentLine;

                const string variableNamePrefix = "local ";
                if (changedLine.StartsWith(variableNamePrefix))
                {
                    int variableNameStartIndex = variableNamePrefix.Length;
                    changedLine = changedLine.Substring(variableNameStartIndex);
                }

                int variableNameFirstSpaceIndex = changedLine.IndexOf(' ');

                changedLine = changedLine.Substring(0, variableNameFirstSpaceIndex);

                myScriptStringVariables.dictionary.Add(changedLine, "");
            }
            else
            {
                if (currentLine == "--[UNITY_STRING]--")
                {
                    foundStringLastTime = true;
                }
            }
        }

        //         myScriptIDVariables.dictionary.Add("testingTest", null);
        //         myScriptIDVariables.dictionary.Add("testingTest number 2", null);
    }

    void Start()
    {

    }

    void Update()
    {

    }
}
