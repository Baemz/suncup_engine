﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class ParticleDataExport : MonoBehaviour {

    private new ParticleSystem particleSystem = null;
    public string fileNameNoExt;
    public string jsonData;
    public string externalFilePath;
    public string particleTexture;

    // Use this for initialization
    void Start () {

		
	}
	
    [System.Serializable]
    struct ParticleData
    {
        public float duration;
        public bool looping;
        public bool prewarm;
        public float startDelay;
        public float startLifetime;
        public float startSpeed;
        public float startSize;
        public bool useSizeOverLifetime;
        public bool useSizeCurve; 
        public float startSizeMin;
        public float startSizeMax;
        public float startSizeCurve;
        public float endSize; 
        public bool useRotationOverLifetime;
        public float startRotation;
        public float startRotationMin;
        public float startRotationMax;
        public float gravityModifier;
        public int maxParticles;

        public float emissionRateOverTime;
        public Color startColor;

        //Shape goes here

        public bool useBoxOffset;
        public Vector3 spawnPositionLimits;
        public Vector3 direction;

        public Vector3 velocityOverLife;

        // Colors
        public bool useColorOverLife;
        public bool randomTwoGradients;

        public Color[] colorgrad1;
        public Color[] colorgrad2;

        //public 
        
        //Size over life

        public Vector3 rotationOverLife;
        public Vector3 rotationOverLifeMin;
        public Vector3 rotationOverLifeMax;

        //INSERT NOISE SOLUTION

        //Trails

        //Renderer
        public string texturePath;
        public string materialName;
        public int myRenderProjectionMode;
    }

    public void CreateParticleFile()
    {
        particleSystem = gameObject.GetComponent<ParticleSystem>();
        if (particleSystem == null)
        {
            return;
        }

        ParticleData myParticleStats = new ParticleData();
        myParticleStats.texturePath = particleTexture;

        myParticleStats.duration = particleSystem.main.duration;
        myParticleStats.looping = particleSystem.main.loop;
        myParticleStats.prewarm = particleSystem.main.prewarm;
        myParticleStats.startDelay = particleSystem.main.startDelay.constant;
        myParticleStats.startLifetime = particleSystem.main.startLifetime.constant;
        myParticleStats.startSpeed = particleSystem.main.startSpeed.constant;
        myParticleStats.startSize = particleSystem.main.startSize.constant;

        myParticleStats.startRotation = particleSystem.main.startRotation.constant;
        myParticleStats.startRotationMin = particleSystem.main.startRotation.constantMin;
        myParticleStats.startRotationMax = particleSystem.main.startRotation.constantMax;
        myParticleStats.startColor = particleSystem.main.startColor.color.linear;
        myParticleStats.gravityModifier = particleSystem.main.gravityModifier.constant;
        myParticleStats.maxParticles = particleSystem.main.maxParticles;

        myParticleStats.emissionRateOverTime = particleSystem.emission.rateOverTime.constant;

        //insert shape here
        myParticleStats.direction = new Vector3(-particleSystem.shape.rotation.x, particleSystem.shape.rotation.y, -particleSystem.shape.rotation.z);
        myParticleStats.spawnPositionLimits = new Vector3(particleSystem.shape.scale.x, particleSystem.shape.scale.y, particleSystem.shape.scale.z);
        if (particleSystem.shape.shapeType == ParticleSystemShapeType.Box)
        {
            myParticleStats.useBoxOffset = true;
        }
        else
        {
            myParticleStats.useBoxOffset = false;
        }

        myParticleStats.velocityOverLife = new Vector3(particleSystem.velocityOverLifetime.x.constant, particleSystem.velocityOverLifetime.y.constant, particleSystem.velocityOverLifetime.z.constant);

        if (particleSystem.colorOverLifetime.enabled)
        {
            myParticleStats.useColorOverLife = true;

            if (particleSystem.colorOverLifetime.color.gradientMax != null && particleSystem.colorOverLifetime.color.gradientMin != null)
            {
                myParticleStats.colorgrad1 = new Color[100];
                myParticleStats.colorgrad2 = new Color[100];
                float lifetime = 0.0f;
                for (int i = 0; i < 100; ++i)
                {
                    myParticleStats.colorgrad1[i] = particleSystem.colorOverLifetime.color.gradientMin.Evaluate(lifetime);
                    myParticleStats.colorgrad2[i] = particleSystem.colorOverLifetime.color.gradientMax.Evaluate(lifetime);
                    lifetime += 0.01f;
                }
                myParticleStats.randomTwoGradients = true;
            }
            else
            {
                myParticleStats.colorgrad1 = new Color[100];
                float time = 0.0f;
                for (int i = 0; i < 100; ++i)
                {
                    myParticleStats.colorgrad1[i] = particleSystem.colorOverLifetime.color.gradient.Evaluate(time);
                    time += 0.01f;
                }
            }
        }
        else
        {
            myParticleStats.useColorOverLife = false;
        }
        

        //size over life goes here
        if (particleSystem.sizeOverLifetime.enabled == true)
        {
            myParticleStats.useSizeOverLifetime = true;

            if (particleSystem.sizeOverLifetime.size.curve == null)
            {
                myParticleStats.useSizeCurve = false;
                myParticleStats.startSizeMin = particleSystem.sizeOverLifetime.size.constantMin;
                myParticleStats.startSizeMax = particleSystem.sizeOverLifetime.size.constantMax;
            }
            else
            {
                myParticleStats.useSizeCurve = true;
                myParticleStats.startSizeCurve = particleSystem.sizeOverLifetime.size.curve.keys[0].value;
                myParticleStats.endSize = particleSystem.sizeOverLifetime.size.curve.keys[particleSystem.sizeOverLifetime.size.curve.keys.Length - 1].value;
            }
        }
        else
        {
            myParticleStats.useSizeOverLifetime = false;
        }

        if (particleSystem.rotationOverLifetime.enabled)
        {
            myParticleStats.useRotationOverLifetime = true;
            myParticleStats.rotationOverLife = new Vector3(particleSystem.rotationOverLifetime.x.constant, particleSystem.rotationOverLifetime.y.constant, particleSystem.rotationOverLifetime.z.constant);
            myParticleStats.rotationOverLifeMin = new Vector3(particleSystem.rotationOverLifetime.x.constantMin, particleSystem.rotationOverLifetime.y.constantMin, particleSystem.rotationOverLifetime.z.constantMin);
            myParticleStats.rotationOverLifeMax = new Vector3(particleSystem.rotationOverLifetime.x.constantMax, particleSystem.rotationOverLifetime.y.constantMax, particleSystem.rotationOverLifetime.z.constantMax);
        }
        else
        {
            myParticleStats.useRotationOverLifetime = false;
        }


        // Noise goes here


        //Trails

        //Renderer

        myParticleStats.materialName = particleSystem.GetComponent<Renderer>().sharedMaterial.name;
        if (particleSystem.GetComponent<ParticleSystemRenderer>().renderMode == ParticleSystemRenderMode.HorizontalBillboard)
        {
            myParticleStats.myRenderProjectionMode = 1;
        }
        else if (particleSystem.GetComponent<ParticleSystemRenderer>().renderMode == ParticleSystemRenderMode.VerticalBillboard)
        {
            myParticleStats.myRenderProjectionMode = 2;
        }
        else
        {
            myParticleStats.myRenderProjectionMode = 0;
        }

        jsonData = JsonUtility.ToJson(myParticleStats, true);
        string folderPath = Application.dataPath + "/Json/";
        Directory.CreateDirectory(folderPath);
        File.WriteAllText(folderPath + fileNameNoExt + ".particle", jsonData);
        Debug.Log("Saved file: " + fileNameNoExt + ".particle");
    }
}
